#ifndef TZBSPMPSSSFS_H
#define TZBSPMPSSSFS_H
/*===========================================================================

                    MPSS SFS and Header formats

DESCRIPTION
  This file contains definitions needed to support MPSS SFS Version Anti-Rollback
  feature.
  
 Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================

                      EDIT HISTORY FOR FILE
  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/src/tzbsp_mpss_sfs.h#1 $ 
  $DateTime: 2018/02/07 00:37:16 $ 
  $Author: mplp4svc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/18/2014 mr       Initial Version

===========================================================================*/

#include "comdef.h"

void tzbsp_mpss_sfs_feature_version_support(void);

#endif // TZBSPMPSSSFS_H
