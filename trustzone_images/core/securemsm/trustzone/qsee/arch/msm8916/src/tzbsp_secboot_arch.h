#ifndef TZBSP_SECBOOT_ARCH_H
#define TZBSP_SECBOOT_ARCH_H

#include <comdef.h>

void get_appsbl_entry_point_details_arch  (uint64* e_entry, uint32* ei_class);
void get_hyp_entry_point_details_arch     (uint64* e_entry, uint32* ei_class);

#endif /* #define TZBSP_SECBOOT_ARCH_H */
