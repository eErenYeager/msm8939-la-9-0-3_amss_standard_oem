#ifndef TZBSP_MEMORY_DUMP_H
#define TZBSP_MEMORY_DUMP_H

#include <stddef.h>
#include "aa64_ctx.h"
#include "tzbsp_target.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/

/* Delay before the WDT bites and resets. In milliseconds. */
#define TZBSP_WDT_DELAY_MS          500

/* Status bits for the CPU status field in \c tzbsp_dump_buf_t. */
#define TZ_SC_STATUS_NS_BIT      0x01
#define TZ_SC_STATUS_WDT         0x02
#define TZ_SC_STATUS_SGI         0x04
/* If this flag is set, WDT bark occured during TZ warm boot. */
#define TZ_SC_STATUS_WARM_BOOT   0x08
#define TZ_SC_STATUS_DBI_RSVD    0x10  /* Reserved for Debug Image */
#define TZ_SC_STATUS_CTX_BY_TZ   0x20

/* Timeout, in us, to wait for QDSS TMCs to flush */
#define TZBSP_QDSS_TMC_US_TIMEOUT   100

/* Macro to wait for ETR or ETB to finish flushing */
#define TZBSP_QDSS_TMC_WAIT(exp)            \
  do {                                      \
    uint32 timeout;                         \
    for(timeout = TZBSP_QDSS_TMC_US_TIMEOUT;\
        timeout > 0 && (exp);               \
        timeout--)                          \
    {                                       \
      busywait(1);                          \
    }                                       \
  } while(0);

#define GET_SHARED_IMEM_ADDR()  (TZBSP_APPS_WDT_CTX_BUFFER_ADDR)

/* Magic indicating that the IMEM dump information, for SBL, is valid;
 * [�T�, �Z�, �D�, �I�] */
#define TZBSP_IMEM_DUMP_MAGIC   0x49445a54

/* Cookie to indicate the XPU dump is valid; ['X', 'P', 'U', 'D'] */
#define XPU_DUMP_MAGIC    0x44555058

#define UPDATE_DEBUG_DUMP_FIELD(area_lc, area_uc, write_ptr, num_bytes) \
  if(num_bytes)                                                         \
  {                                                                     \
    tzbsp_debug_area->area_lc##_dump       = write_ptr;                 \
    tzbsp_debug_area->area_lc##_dump_size  = num_bytes;                 \
    tzbsp_debug_area->area_lc##_dump_magic = area_uc##_DUMP_MAGIC;      \
    write_ptr   += num_bytes;                                           \
    free_space  -= num_bytes;                                           \
    tzbsp_dsb();                                                        \
  }

/* Values to report the dump-ability of L1 and L2 to SDI; magic is ['L', 'C', 'V', 'S'] */
#define TZBSP_CACHE_DUMP_MAGIC      0x5356434C
#define TZBSP_L2_CACHE_VALID        0x1

extern void mon_dump_context(cpu_dbg_info_t *ctx_buf);

extern cpu_dbg_info_t cpu_dbg_info[TZBSP_CPU_COUNT];

/* Overlay structure \c tzbsp_mon_cpu_ctx_t starting from monitor's label
 * \c Workspace_N_Begin to access monitor's saved non-secure context. */
/* Overlay structure \c tzbsp_mon_cpu_ctx_t starting from monitor's label
 * \c Workspace_S_Begin to access monitor's saved secure context. */

extern void tzbsp_enable_qdss_clocks(void);

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/

extern void tzbsp_l1_dump(void);
extern void tzbsp_write_q6_nmis(void);
extern void tzbsp_get_pre_fiq_ctx(uint32 *);



/* Dump format for power status, spm and GICD registers */
typedef struct tzbsp_dump_gicd_spm_reg_s
{
  uint32 MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL;
  uint32 APCS_ALIASn_SAW2_SPM_STS[TZBSP_CPU_COUNT];
  uint32 APCS_ALIASn_SAW2_SPM_CTL[TZBSP_CPU_COUNT];
  uint32 APCS_ALIASn_APC_PWR_STATUS[TZBSP_CPU_COUNT];
  uint32 APCS_ALIASn_APC_PWR_GATE_STATUS[TZBSP_CPU_COUNT];
  uint32 APCS_L2_PWR_STATUS;
  uint32 APCS_L2_SAW2_SPM_STS;
  uint32 APCS_L2_SAW2_SPM_CTL;
  uint32 APCS_GICD_ISENABLER0_PRIV[TZBSP_CPU_COUNT];
  uint32 APCS_GICD_ISPENDR0_PRIV[TZBSP_CPU_COUNT];
  uint32 APCS_GICD_ISENABLERn_PUB[7];
  uint32 APCS_GICD_ISPENDRn_PUB[7];
} tzbsp_dump_gicd_spm_reg_t;

#endif /* TZBSP_MEMORY_DUMP_H */
