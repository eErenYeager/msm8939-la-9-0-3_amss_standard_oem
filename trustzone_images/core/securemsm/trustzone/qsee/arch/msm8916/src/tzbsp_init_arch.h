#ifndef TZBSP_INIT_ARCH_H
#define TZBSP_INIT_ARCH_H

#include <comdef.h>

void tzbsp_verify_sbl_info_arch(void *info);

#endif /* TZBSP_INIT_ARCH_H */
