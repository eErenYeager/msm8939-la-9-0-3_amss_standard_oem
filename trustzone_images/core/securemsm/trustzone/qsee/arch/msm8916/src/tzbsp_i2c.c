/**
@file tzbsp_i2c.c
@brief Wrapper functions for the I2CPD interface in TZBSP


*/
/*===========================================================================
Copyright (c) 2012-2016 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/src/tzbsp_i2c.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
09/20/16   dpk      Added I2C Exclusivity
11/10/12   th       Created

===========================================================================*/


/*===========================================================================

          INCLUDE FILES

============================================================================*/
#include "lkstring.h"
#include "kthread.h"
#include "tzbsp_errno.h"
#include "tzbsp_isr.h"
#include "tzbsp_sys.h"
#include "tzbsp_i2c.h"
#include "tzbsp_xpu.h"
#include "tzbsp_xpu_def.h"
#include "tzbsp_vmid_config.h"
#include "HALxpu2.h"

#include "I2cDevice.h"
#include "DALDeviceId.h"

#include <stringl/stringl.h>

typedef struct
{
  I2CDEV_HANDLE     ahI2c;
  uint8_t           tzAppId[TZBSP_APP_ID_LEN];
  uint8_t           tzAppIdLen;
  uint32            uDalDevId;

} i2cDevice;

/* Structure containing a reference to all the DAL I2C devices supported by this
 * interface and the I2C interrupt these devices are attached to.
 *
 * The values in this table need to be updated depending on the platform
 * configuration.
 */
static i2cDevice i2cDevices[] = {
  /* TS Controller - QUP1 */
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_1
  },
  /* TS Controller - QUP2 */
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_2
  },
  /* TS Controller - QUP3 */
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_3
  },
  /* TS Controller - QUP4 */
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_4
  },
  /* TS Controller - QUP5 */
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_5
  },
  /* TS Controller - QUP6 */
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_6
  },
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_7
  },
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_8
  },
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_9
  },
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_10
  },
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_11
  },
  {
    NULL,
    {0}, 0,
    DALDEVICEID_I2C_DEVICE_12
  }
};

/* @copydoc tzbsp_i2c_open */
int tzbsp_i2c_open(tzbsp_i2cpd_device_id_t device_id)
{
  thread_tz_app_id_t app_id    = {0};
  int                result    = E_SUCCESS;
  int32              i2cResult;

  if (device_id >= TZBSP_I2CPD_DEVICE_COUNT)
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_i2c_open() invalid device_id: %08x", device_id);
    return -E_FAILURE;
  }

  /* Check if there is already an open client. This wrapper supports
   * only one open client per device */
  if (i2cDevices[device_id].tzAppIdLen)
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_i2c_open() device_id already open: %08x", device_id);
    return -E_FAILURE;
  }

  do {
    /* Configure the I2C device if this has not been done before */
    if (NULL == i2cDevices[device_id].ahI2c)
    {
      i2cResult = I2CDEV_Init(i2cDevices[device_id].uDalDevId, &i2cDevices[device_id].ahI2c);
      if (I2C_RES_SUCCESS != i2cResult)
      {
        TZBSP_LOG(TZBSP_MSG_ERROR,
                  "tzbsp_i2c_open() init error: %08x", i2cResult);
        result = -E_FAILURE;
        break;
      }
    }

    /* Store the app_id for exclusivity checks */
    if ((E_SUCCESS        != thread_get_current_tz_app_id(&app_id)) || 
        (TZBSP_APP_ID_LEN != app_id.tz_app_id_len))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_open() failed to get app_id");
      result = -E_FAILURE;
      break;
    }

    i2cResult = I2CDEV_SetPowerState(i2cDevices[device_id].ahI2c, I2CDEV_POWER_STATE_2);
    if (I2C_RES_SUCCESS != i2cResult)
    {
        TZBSP_LOG(TZBSP_MSG_ERROR,
                  "tzbsp_i2c_open() set power state error: %08x", i2cResult);
        result = -E_FAILURE;
        break;
    }

    /* Device is now open */
    (void) memscpy(i2cDevices[device_id].tzAppId, TZBSP_APP_ID_LEN, 
                   app_id.tz_app_id,              app_id.tz_app_id_len);
    i2cDevices[device_id].tzAppIdLen = app_id.tz_app_id_len;

    /* Return results to caller */
  } while (0);

  if (E_SUCCESS != result) {
    /* Clean up everything that we can. Ignore the return value from
     * tzbsp_i2c_close() as we want to return the reason the tzbsp_i2c_open()
     * failed */
    (void) tzbsp_i2c_close(device_id);
  }

  return result;
}

/* @copydoc tzbsp_i2c_read */
int tzbsp_i2c_read(tzbsp_i2cpd_device_id_t        device_id,
                   const tzbsp_i2c_config_t*      p_config,
                   tzbsp_i2c_transaction_info_t*  p_read_info)
{
  I2CDEV_HANDLE      hI2c;
  I2cClientConfig    clntCfg = {400, 2500};
  thread_tz_app_id_t app_id  = {0};
  uint8              offset[2];
  I2cBuffDesc        rdIoVecs[2];
  I2cTransfer        rdTrans[2];
  I2cSequence        rdSeq;
  I2cIoResult        ioRes;
  int32              IoVecInd;
  int32              TransferInd;
  int32              nOffsetInd;
  int32              i2cResult;
  int                result = E_SUCCESS;

  do {
    /* Check for valid input parameters */
    if (((uint32)device_id        >= TZBSP_I2CPD_DEVICE_COUNT) ||
        (p_config                 == NULL)                     ||
        (p_config->p_slave_config == NULL)                     ||
        (p_read_info              == NULL))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_read() invalid input parameters");
      result = -E_FAILURE;
      break;
    }

    /* Fetch the current app id information */
    if ((E_SUCCESS        != thread_get_current_tz_app_id(&app_id)) || 
        (TZBSP_APP_ID_LEN != app_id.tz_app_id_len))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_read() failed to get app_id");
      result = -E_FAILURE;
      break;
    }

    /* Check the device is open for reading */
    if (memcmp(i2cDevices[device_id].tzAppId, app_id.tz_app_id, TZBSP_APP_ID_LEN))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_read() I2C device not owned/open");
      result = -E_FAILURE;
      break;
    }

    hI2c = i2cDevices[device_id].ahI2c;
    if (NULL != p_config->p_bus_config)
    {
      clntCfg.uBusFreqKhz = p_config->p_bus_config->fs_bus_freq_in_khz;
    }

    clntCfg.uByteTransferTimeoutUs =
      p_config->p_slave_config->byte_transfer_wait_time_usec;

    IoVecInd            = 0;
    TransferInd         = 0;
    rdSeq.pTransfer     = rdTrans;

    if ((TZBSP_I2C_MEMORY_ADDRESS_DEVICE ==
         p_config->p_slave_config->slave_device_type) ||
        (TZBSP_I2C_REGISTER_ADDRESS_DEVICE ==
         p_config->p_slave_config->slave_device_type))
    {
      /* Set the offset vectors and transfer */
      nOffsetInd            = 0;
      rdIoVecs[0].uBuffSize = 0;
      rdIoVecs[0].pBuff     = offset;

      if (TZBSP_I2C_MEMORY_ADDRESS_DEVICE ==
          p_config->p_slave_config->slave_device_type)
      {
        /* write offset high byte */
        offset[0]             = (p_read_info->start_addr >> 8) & 0xFF;
        rdIoVecs[0].uBuffSize = 1;
        nOffsetInd++;
      }

      /* write offset low byte */
      offset[nOffsetInd]     = p_read_info->start_addr & 0xFF;
      rdIoVecs[0].uBuffSize += 1;

      rdTrans[0].pI2cBuffDesc       = &rdIoVecs[0];
      rdTrans[0].uTrSize            = rdIoVecs[0].uBuffSize;
      rdTrans[0].tranCfg.uSlaveAddr = p_config->p_slave_config->slave_address;
      rdTrans[0].eTranDirection     = I2cTranDirOut;

      if ((TZBSP_I2C_READ_OPTION_DEFAULT ==
           p_config->p_slave_config->read_option) ||
          (TZBSP_I2C_GEN_START_BEFORE_READ ==
           p_config->p_slave_config->read_option))
      {
        rdTrans[0].eTranCtxt = I2cTrCtxSeqStart;
        rdTrans[1].eTranCtxt = I2cTrCtxSeqEnd;
      }
      else
      {
        /* separate transfers with a stop */
        rdTrans[0].eTranCtxt = I2cTrCtxNotASequence;
        rdTrans[1].eTranCtxt = I2cTrCtxNotASequence;
      }

      IoVecInd++;
      TransferInd++;
      rdSeq.uNumTransfers  = 2; // two transfers
    }
    else
    { /* default device. */
      rdTrans[0].eTranCtxt = I2cTrCtxNotASequence;
      rdSeq.uNumTransfers  = 1; // only 1 transfer
    }

    /* configure the actual read transfer.
     *
     * this could be the first or second transfer based on device type
     * config. */
   rdIoVecs[IoVecInd].pBuff          = p_read_info->p_buf;
   rdIoVecs[IoVecInd].uBuffSize      = p_read_info->buf_len;
   rdTrans[TransferInd].pI2cBuffDesc = &rdIoVecs[IoVecInd];
   rdTrans[TransferInd].uTrSize      = rdIoVecs[IoVecInd].uBuffSize;
   rdTrans[TransferInd].tranCfg.uSlaveAddr =
                                       p_config->p_slave_config->slave_address;
   rdTrans[TransferInd].eTranDirection =
                                       I2cTranDirIn;

   i2cResult = I2CDEV_BatchTransfer(hI2c, &rdSeq, &clntCfg, &ioRes);
   if(I2C_RES_SUCCESS != i2cResult)
   {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_read(): %08x", i2cResult);
      result = -E_FAILURE;
      break;
   }

   p_read_info->total_bytes = ioRes.uInByteCnt;

  } while (0);

  return result;
}

/* @copydoc tzbsp_i2c_write */
int tzbsp_i2c_write(tzbsp_i2cpd_device_id_t       device_id,
                    const tzbsp_i2c_config_t*     p_config,
                    tzbsp_i2c_transaction_info_t* p_write_info)
{
  I2CDEV_HANDLE      hI2c;
  thread_tz_app_id_t app_id  = {0};
  I2cClientConfig    clntCfg = {400, 2500};
  uint8              offset[2];
  I2cBuffDesc        wrIoVecs[2];
  I2cTransfer        wrTran;
  int32              IoVecInd;
  int32              i2cResult;
  int                result = E_SUCCESS;

  do {
    /* Check for valid input parameters */
    if (((uint32)device_id        >= TZBSP_I2CPD_DEVICE_COUNT) ||
        (p_config                 == NULL)                     ||
        (p_config->p_slave_config == NULL)                     ||
        (p_write_info             == NULL))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_write() invalid input parameters");
      result = -E_FAILURE;
      break;
    }

    /* Fetch the app id information */
    if ((E_SUCCESS        != thread_get_current_tz_app_id(&app_id)) || 
        (TZBSP_APP_ID_LEN != app_id.tz_app_id_len))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_write() failed to get app_id");
      result = -E_FAILURE;
      break;
    }

    if (memcmp(i2cDevices[device_id].tzAppId, app_id.tz_app_id, TZBSP_APP_ID_LEN))
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_write() I2C device not owned/open");
      result = -E_FAILURE;
      break;
    }

    hI2c = i2cDevices[device_id].ahI2c;
    if (NULL != p_config->p_bus_config)
    {
      clntCfg.uBusFreqKhz = p_config->p_bus_config->fs_bus_freq_in_khz;
    }

    clntCfg.uByteTransferTimeoutUs =
      p_config->p_slave_config->byte_transfer_wait_time_usec;

    IoVecInd       = 0;
    wrTran.uTrSize = 0;

    if (TZBSP_I2C_MEMORY_ADDRESS_DEVICE ==
        p_config->p_slave_config->slave_device_type)
    {
      /* write offset two bytes */
      offset[0]             = (p_write_info->start_addr >> 8) & 0xFF;
      offset[1]             = p_write_info->start_addr & 0xFF;

      /* the first buffer contains the write offset */
      wrIoVecs[0].pBuff     =  offset;
      wrIoVecs[0].uBuffSize =  2;

      /* total size reflects the size of first buffer */
      wrTran.uTrSize       +=  2;
      IoVecInd++;
    }
    else if (TZBSP_I2C_REGISTER_ADDRESS_DEVICE ==
             p_config->p_slave_config->slave_device_type)
    {
      /* write offset 1 byte */
      offset[0]             = p_write_info->start_addr & 0xFF;

      /* the first buffer contains the write offset */
      wrIoVecs[0].pBuff     = offset;
      wrIoVecs[0].uBuffSize = 1;

      /* total size reflects the size of first buffer */
      wrTran.uTrSize       += 1;
      IoVecInd++;
    }

    wrIoVecs[IoVecInd].pBuff     = p_write_info->p_buf;
    wrIoVecs[IoVecInd].uBuffSize = p_write_info->buf_len;
    wrTran.pI2cBuffDesc          = wrIoVecs;
    wrTran.uTrSize              += p_write_info->buf_len;
    wrTran.tranCfg.uSlaveAddr    = p_config->p_slave_config->slave_address;
    wrTran.eTranDirection        = I2cTranDirOut;
    wrTran.eTranCtxt             = I2cTrCtxNotASequence;
    i2cResult = I2CDEV_Write(hI2c,      &wrTran,
                             &clntCfg, &p_write_info->total_bytes);
    if (I2C_RES_SUCCESS != i2cResult)
    {
      TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_write(): %08x", i2cResult);
      result = -E_FAILURE;
      break;
    }
  } while (0);

  return result;
}

/* @copydoc tzbsp_i2c_close */
int tzbsp_i2c_close(tzbsp_i2cpd_device_id_t device_id)
{
  thread_tz_app_id_t app_id = {0};
  int32              i2cResult;

  /* Fetch the app id information */
  if ((E_SUCCESS        != thread_get_current_tz_app_id(&app_id)) || 
      (TZBSP_APP_ID_LEN != app_id.tz_app_id_len))
  {
    TZBSP_LOG(TZBSP_MSG_ERROR, "tzbsp_i2c_close() failed to get app_id");
    return -E_FAILURE;
  }

  /* Check for a valid device ID */
  if ((uint32)device_id >= TZBSP_I2CPD_DEVICE_COUNT)
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_i2c_close() invalid device_id: %08x", device_id);
    return -E_FAILURE;
  }

  /* Check if the caller is the open client */
  if (memcmp(i2cDevices[device_id].tzAppId, app_id.tz_app_id, TZBSP_APP_ID_LEN))
  {
    TZBSP_LOG(TZBSP_MSG_ERROR,
              "tzbsp_i2c_close() device_id not opened/owned: %08x", device_id);
    return -E_FAILURE;
  }

  i2cResult = I2CDEV_SetPowerState(i2cDevices[device_id].ahI2c, I2CDEV_POWER_STATE_0);
  if (I2C_RES_SUCCESS != i2cResult)
  {
     TZBSP_LOG(TZBSP_MSG_ERROR,
               "tzbsp_i2c_close() set power state error: %08x", i2cResult);
     return -E_FAILURE;
  }

  memset(i2cDevices[device_id].tzAppId, 0, TZBSP_APP_ID_LEN);
  i2cDevices[device_id].tzAppIdLen = 0;
  
  return E_SUCCESS;
}
