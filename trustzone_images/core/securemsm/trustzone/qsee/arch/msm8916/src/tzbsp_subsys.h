#ifndef TZBSP_SUBSYS_H
#define TZBSP_SUBSYS_H

#include <comdef.h>
#include "tzbsp_syscall.h"

void tzbsp_subsys_set_dump_segment(tz_pil_proc_e_type proc, void* base, size_t size);

#endif /* TZBSP_SUBSYS_H */
