/*===========================================================================
   Copyright (c) 2010-2013 by QUALCOMM Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/arch/msm8916/src/tzbsp_xpu_paged.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who     what, where, why
--------   ---     ------------------------------------------------------------
06/21/12   sg      Add functionality to add/remove vmid permissions & xpu error
                   status support
10/07/11   mm      Updated log string to include module information
09/28/10   tk      Initial version.
=============================================================================*/



/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <HALxpu2.h>

#include "tzbsp_errno.h"

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
 * Updates the Unmapped Read/Write VMIDs for a given XPU
 *
 * @param [in]  xpu   XPU to be updated
 * @param [in]  vmid  The VMID to be updated. This is the decimal value as
 *                    opposed to the binary value
 * @param [in]  read  TRUE to add read permissions, FALSE to remove
 * @param [in]  write TRUE to add write permissions, FALSE to remove
 *
 * @return \c E_SUCCESS on success, negative error code otherwise.
 */
int tzbsp_xpu_set_unmapped_vmid_perms(HAL_xpu2_XPU2Type xpu, uint32 vmid, boolean read, boolean write)
{
#ifdef FEATURE_NO_XPU
  return E_SUCCESS;
#else
  HAL_xpu2_ResourceGroupPermissionType acc;
  uint32 *perm;

  if(xpu >= HAL_XPU2_COUNT || !vmid || vmid > 31)
    return -E_INVALID_ARG;

  HAL_xpu2_GetUnmappedPartitionPermissions(xpu, &acc);

  if(vmid < 16)
    perm = &acc.Vmid_Type.multi.auVMIDPerm[0];
  else
  {
    vmid -= 16;
    perm = &acc.Vmid_Type.multi.auVMIDPerm[1];
  }

  if(read)
    *perm |= HAL_XPU2_RO_ACCESS << (vmid << 1);
  else
    *perm &= ~(HAL_XPU2_RO_ACCESS << (vmid << 1));

  if(write)
    *perm |= HAL_XPU2_WO_ACCESS << (vmid << 1);
  else
    *perm &= ~(HAL_XPU2_WO_ACCESS << (vmid << 1));

  return HAL_xpu2_ConfigUnmappedPartitionPermissions(xpu, &acc);
#endif
}

