# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/services/memzi/src/tzbsp_memzi.s"
# 1 "<built-in>" 1
# 1 "/local/mnt/workspace/CRMBuilds/TZ.BF.3.0.c3-00052-M8936AAAAANAZT-1_20180207_015142/b/trustzone_images/core/securemsm/trustzone/qsee/services/memzi/src/tzbsp_memzi.s" 2
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; T R U S T Z O N E M E M Z I
;
;
; GENERAL DESCRIPTION
; This file contains the TZ memory zeroing code
;
; Copyright (c) 2012 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; $Header:
; when who what, where, why
; -------- --- --------------------------------------------------------
; 09/10/12 sg Initial revision.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;============================================================================
;
; MODULE EXPORTS
;
;============================================================================

    EXPORT tzbsp_memzi

    PRESERVE8
    CODE32

;============================================================================
;
; MODULE CODE
;
;============================================================================

;======================================================================
;
; void tzbsp_memzi(void * dst, uint32 size)
;
; Initialize memory to zero
; Parameters:
; r0 contains the destination address
; r1 is the size to be set to zero
;======================================================================
    AREA TZBSP_MEMZI, CODE, READONLY, ALIGN=5
tzbsp_memzi FUNCTION
    bx lr
   ;Initialize
   ENDFUNC
   END
