#ifndef QSEE_MRM_H
#define QSEE_MRM_H

/**
@file qsee_mrm.h
@brief Memory Region Manager
*/

/*===========================================================================
   Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/services/memory_region_manager/inc/qsee_mrm.h#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $

when       who      what, where, why
--------   ---      ------------------------------------
01/20/13   pre      Initial version.

===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include <comdef.h>
#include "memory_defs.h"

p_addr_t* qsee_mrm_get_code_base_addrs(void);
size_t*   qsee_mrm_get_code_lens(void);
p_addr_t  qsee_mrm_get_demand_paging_code_base_addr(void);
size_t    qsee_mrm_get_demand_paging_code_len(void);

p_addr_t qsee_mrm_get_el1_tt_base_addr(void);
size_t qsee_mrm_get_el1_tt_len(void);

p_addr_t qsee_mrm_get_el3_tt_base_addr(void);
size_t qsee_mrm_get_el3_tt_len(void);
p_addr_t qsee_mrm_get_mink_shared_usr_code_base(void);
size_t qsee_mrm_get_mink_shared_usr_code_len(void);
p_addr_t qsee_mrm_get_ddr_stack_addr(void);
size_t qsee_mrm_get_ddr_stack_len(void);
mem_block_t* qsee_mrm_get_cold_init_map(void);
unsigned int qsee_mrm_get_cold_init_map_len(void);
int qsee_mrm_add_to_cold_init_map(v_addr_t, p_addr_t, size_t, uint32);

#endif /* #define QSEE_MRM_H */
