#ifndef TZBSP_SMC_H
#define TZBSP_SMC_H

typedef enum
{
  SMC_CMD_RETURN_TO_NS             =  0,
  SMC_CMD_ENTER_NS_OS              =  1,
  SMC_CMD_FIQ_DONE                 =  2,
  SMC_CMD_MILESTONE_SET            =  3,
  SMC_CMD_SET_BOOT_LOCK_ADDR       =  4,
  SMC_CMD_MMU_MAP                  =  5,
  SMC_CMD_MMU_MAP_BLOCK            =  6,
  SMC_CMD_GET_EL1_TT_MEM_INFO      =  7,
  SMC_CMD_GET_EL3_TT_MEM_INFO      =  8,

  /* Perform Mode switch for EL1 to A64 */
  SMC_CMD_DO_MODE_SWITCH           =  9,

  /* Configure Entry Address for Hypervisor */
  SMC_CONFIG_HYP_ENTRY             = 10,

  SMC_CMD_GET_NS_W30               = 11,
  SMC_CMD_RETURN_TO_NS_NO_REGS_MODIFIED = 12,
  SMC_CMD_SET_SMP_NOTIFIER          = 13,
  SMC_CMD_RETURN_FROM_PC_NOTIFIER  = 14,
  SMC_CMD_IRQ                      = 15,

  /* Instruct Monitor i.e. EL3 to dump context */
  SMC_CMD_DUMP_CONTEXT             = 16,

  SMC_CMD_SET_SYSDBG_ENTRY_AND_BUF = 17,
  SMC_CMD_SET_TIMER_FREQ           = 18,
  SMC_CMD_SET_DEBUG                = 19,
  SMC_CMD_ONESHOT_START_CPU_TIMER  = 20,
  SMC_CMD_SET_CPU_TIMER            = 21,
  SMC_CMD_GET_CPU_TIMER_COUNT      = 22,

  /* Dump calls */
  SMC_CMD_GET_A53_TLB_DUMP_SZ      = 23,
  SMC_CMD_DUMP_A53_TLB             = 24,
  SMC_CMD_GET_A53_ICACHE_DUMP_SZ   = 25,
  SMC_CMD_DUMP_A53_ICACHE          = 26,
  SMC_CMD_GET_A53_DCACHE_DUMP_SZ   = 27,
  SMC_CMD_DUMP_A53_DCACHE          = 28,

  /* Instruct Monitor i.e. EL3 to dump vfp context */
  SMC_CMD_DUMP_VFP_CONTEXT         = 29,

  /* set the is_ext_os_config to true */
  SMC_CMD_SET_EXT_OS_CONFIG        = 30,
  
  /* set this flag to have SDI work around */
  SMC_CMD_SET_SDI_WA_FLAG        = 31,

  /* Get Monitor Code Base Address and Length */
  SMC_CMD_GET_MON_CODE_INFO        = 34,
} mon_call_id_t;

/* Needs to be defined for AArch32/AArch64 expectations */
typedef uint32 reg_t;

uint32 tzbsp_smc_wrapper_1(mon_call_id_t r0);
uint32 tzbsp_smc_wrapper_2(mon_call_id_t r0, reg_t r1);
uint32 tzbsp_smc_wrapper_3(mon_call_id_t r0, reg_t r1, reg_t r2);
uint32 tzbsp_smc_wrapper_4(mon_call_id_t r0, reg_t r1, reg_t r2, reg_t r3);
uint32 tzbsp_smc_wrapper_5(mon_call_id_t r0, reg_t r1, reg_t r2, reg_t r3,
                           reg_t r4);
uint32 tzbsp_smc_return_2(mon_call_id_t r0, uint32* ret0, uint32* ret1);


#endif
