#ifndef TZ_MC_H
#define TZ_MC_H

#include "memory_defs.h"
#include "tzbsp.h"
#include "tz_syscall_pub.h"
//#include "aa64_ctx.h"
#define L2_GDHS_BMSK    3

typedef enum {
  SMC_INFO_GENERIC_TYPE        = 0x100,
  SMC_INFO_MODE_SWITCH_TYPE    = 0x101,  //SysCall = TZ_DO_MODE_SWITCH
} SMS_INFO_TYPE;


typedef struct mon_cpu_ctx_s
{
  uint32 mon_lr;
  uint32 mon_spsr;
  uint32 usr_r0;
  uint32 usr_r1;
  uint32 usr_r2;
  uint32 usr_r3;
  uint32 usr_r4;
  uint32 usr_r5;
  uint32 usr_r6;
  uint32 usr_r7;
  uint32 usr_r8;
  uint32 usr_r9;
  uint32 usr_r10;
  uint32 usr_r11;
  uint32 usr_r12;
  uint32 usr_r13;
  uint32 usr_r14;
  uint32 irq_spsr;
  uint32 irq_r13;
  uint32 irq_r14;
  uint32 svc_spsr;
  uint32 svc_r13;
  uint32 svc_r14;
  uint32 abt_spsr;
  uint32 abt_r13;
  uint32 abt_r14;
  uint32 und_spsr;
  uint32 und_r13;
  uint32 und_r14;
  uint32 fiq_spsr;
  uint32 fiq_r8;
  uint32 fiq_r9;
  uint32 fiq_r10;
  uint32 fiq_r11;
  uint32 fiq_r12;
  uint32 fiq_r13;
  uint32 fiq_r14;
} mon_cpu_ctx_t;

/* TODO: should move to a common area between qsee/monitor */
typedef enum {
  SMP_NOTIFIER_CMD_PC    = 0,
  SMP_NOTIFIER_CMD_SPMI  = 1
} el1_smp_notifier_cmd_t;

/* TODO: should move to a common area between qsee/monitor */
typedef enum {
  CPU_PC           = 0,
  CPU_AND_L2_PC    = 1,
  PC_FALLTHROUGH   = 2,
  CPU_AND_L2_GDHS  = 3,
} el1_pc_notifier_t;

typedef enum {
  SPMI_SUCCESS     = 0,
  SPMI_FAILURE     = 1
} el1_spmi_notifier_t;

typedef union {
  el1_pc_notifier_t el1_pc_notifier;
  el1_spmi_notifier_t el1_spmi_notifier;
} el1_smp_notifier_t;

#define SMC_EVENT_SYSCALL               0
#define SMC_EVENT_POWER_COLLAPSE_ENTRY  1
#define SMC_EVENT_POWER_COLLAPSE_EXIT   2
#define SMC_INVALID_VALUE               99

uint32 tzbsp_smc_return_to_ns(tzbsp_regs_t *regs);

void tzbsp_install_fiq_function( int (*pfn) (void) );
void tzbsp_clear_boot_lock(void);
uint32 tzbsp_get_scr_val_at_interrupt(void);
uint32 tzbsp_get_smc_lock_val(void);
void tzbsp_init_monitor(void);
int tzbsp_nfdbg_config(const uint32* vector, const uint32* stack,
                       uint32* ctx_buf, uint32 ctx_size, uint32 intnum,
                       uint32 flags);
int tzbsp_nfdbg_ctx_size(uint32* ctx_size, uint32 rsp_size);
int tzbsp_nfdbg_is_int_ok(uint32 intnum, uint32* verdict, uint32 rsp_size);

uint32 tzbsp_get_irq_val(void);
void tzbsp_fiq_set_stacked_ctx(void);
void tzbsp_l1_dump_buf_set_imem_addr(uint32 addr);

int tzbsp_enable_mmu(void);

int tzbsp_get_el3_tt_mem_info(p_addr_t* base_addr_ptr,
                              size_t*   len_ptr);

int tzbsp_get_el1_tt_mem_info(p_addr_t* base_addr_ptr,
                              size_t*   len_ptr);
/** 
 * Used when ext os is configured and to notify the monitor that is was
 * configured 
 */
void tzbsp_set_ext_os_config(void);
void mon_handle_mode_switch(hlos_boot_params_t* );

void *tzbsp_get_reset_vector(void);

void mon_config_hyp_entry(uint64 e_entry, uint32 ei_class);
uint32 tzbsp_smc_return_to_ns_no_regs_modified(tzbsp_regs_t *);
uint32 get_ns_lr(void);

void mon_register_el1_symmetric_multiprocessing_notifier(v_addr_t pfn);

void mon_oneshot_start_cpu_timer(uint64);

void mon_set_cpu_timer(uint64);

uint64 mon_get_cpu_timer_count(void);
//TODO-sg: Unable to include "aa64_ctx.h" even with MONITOR_API in sconscript, todo.c compiles
///* Monitor call to dump context */
//void mon_dump_context(aa_64_ctx_all_els_t *ctx_buf);
//
///* Configure Entry Address for Hypervisor */
void mon_set_timer_freq(uint32 ticks_per_second);


void mon_register_el1_sysdbg_entry_and_buf(v_addr_t pfn, v_addr_t buf);

void mon_set_timer_freq(uint32 ticks_per_second);

void mon_el1_fiq_processing_done(void);
void mon_el1_irq_processing_done(tzbsp_regs_t* regs);

void mon_set_debug(uint32 debug);
void mon_set_boot_lock_addr(v_addr_t boot_lock_addr);
void mon_set_oem_flag_for_sdi_wa(boolean sdi_wa);
void mon_handle_milestone(uint32 e_entry_l, uint32 e_entry_h, uint32 ei_class);

uint32 mon_get_a53_tlb_dump_sz(void);
void   mon_dump_a53_tlb(void* dump_ptr);

uint32 mon_get_a53_icache_dump_sz(void);
void   mon_dump_a53_icache(void* dump_ptr);
uint32 mon_get_a53_dcache_dump_sz(void);
void   mon_dump_a53_dcache(void* dump_ptr);
int tzbsp_config_hyp_entry(uint64 e_entry, uint32 ei_class);
int tzbsp_get_mon_code_info(p_addr_t* base_addr_ptr, uint32* len_ptr);


/****************************************************************************
   Stuff that will be entirely in monitor eventually, but need a stub
   for compilation now
 ****************************************************************************/
void tzbsp_restore_debug_regs(void);

// This function is call to monitor from EL1 to save NS VFP context
void mon_save_ns_vfp_context (int VFPStatusFlag);


#endif /* TZ_MC_H */
