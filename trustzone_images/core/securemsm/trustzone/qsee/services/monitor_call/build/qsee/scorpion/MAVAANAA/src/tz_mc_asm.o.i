;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; GENERAL DESCRIPTION
; This file contains the handlers for calling the monitor
;
; INITIALIZATION AND SEQUENCING REQUIREMENTS
;
; Copyright (c) 2013 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
; EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
;
; $Header:
; when who what, where, why
; -------- --- --------------------------------------------------------
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;============================================================================
;
; MODULE IMPORTS
;
;============================================================================
;============================================================================
;
; MODULE EXPORTS
;
;============================================================================
    EXPORT tzbsp_smc_wrapper_1
    EXPORT tzbsp_smc_wrapper_2
    EXPORT tzbsp_smc_wrapper_3
    EXPORT tzbsp_smc_wrapper_4
    EXPORT tzbsp_smc_wrapper_5
    EXPORT tzbsp_smc_return_2
;============================================================================
;
; MODULE CODE
;
;============================================================================
    PRESERVE8
    CODE32
    AREA TZBSP_SMC, CODE, READONLY, ALIGN=5
tzbsp_smc_wrapper_5
    push {r4,r5}
    ldr r4, [sp,#8]
    smc #0
    pop {r4,r5}
    bx lr
tzbsp_smc_wrapper_1
tzbsp_smc_wrapper_2
tzbsp_smc_wrapper_3
tzbsp_smc_wrapper_4
    smc #0
    bx lr
tzbsp_smc_return_2
    push {r1, r2}
    smc #0
    pop {r3, r12}
    str r1, [r3]
    str r2, [r12]
    bx lr
  END
