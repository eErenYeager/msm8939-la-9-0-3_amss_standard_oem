/**
* @file tzbsp_pil_xpu.c
* @brief Provide MPU protection for memory areas defined by PIL
*
* This file provides MPU locking and unlocking for ELF loaded images.
* It also adds MPU locked regions to the blaklist, which are considered
* secure memory areas from the perspecive of the HLOS.
*
*/


/*===========================================================================
   Copyright (c) 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                            EDIT HISTORY FOR FILE

  $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/core/securemsm/trustzone/qsee/services/img_auth/src/tzbsp_pil_xpu.c#1 $
  $DateTime: 2018/02/07 00:37:16 $
  $Author: mplp4svc $


when       who      what, where, why
--------   ---      ------------------------------------
22/10/10   tk       Created
===========================================================================*/

/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/

#include <string.h>

#include "tzbsp.h"
#include "tzbsp_chipset.h"
#include "tzbsp_target.h"
#include "tzbsp_errno.h"
#include "tzbsp_mem.h"
#include "tzbsp_pil.h"
#include "tzbsp_xpu.h"
#include "tzbsp_mpu.h"
#include "tzbsp_xpu_config.h"
#include "tzbsp_sys.h"
#include "secboot_hw.h"
#include "tzbsp_vmid_config.h"

#define IS_NOT_4K_ALIGNED(X) X&0xFFF
#define TRUE_MASK        0xFFFFFFFF

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/
boolean tzbsp_subsys_is_valid_area(tz_pil_proc_e_type proc, uint32 start,
                                   uint32 end);
int tzbsp_subsys_lock(tz_pil_proc_e_type proc);
int tzbsp_subsys_unlock(tz_pil_proc_e_type proc, boolean erase);

extern int tzbsp_subsys_bring_up(tz_pil_proc_e_type proc, uint32 start_addr);
extern int tzbsp_subsys_tear_down(tz_pil_proc_e_type proc,
                           tzbsp_tear_down_scope_et scope);
						   
/**
 * Checks whether the image entry and segments are in the memory areas
 * allocated for the subsystem.
 *
 * @param[in] proc    Subsystem ID.
 * @param[in] hdr     Pointer to the image ELF header.
 * @param[in] count   Number of segments in the image.
 * @param[in] entry   The entry address to the image.
 *
 * @return \c E_SUCCESS if the image entry and segments are in valid memory
 *         areas, error code otherwise.
 */
int tzbsp_validate_image_area(tz_pil_proc_e_type proc,
                              const Elf32_Phdr* hdr,
                              uint32 count, uint32 entry)
{
  int i;
  uint32 start, end;

  /* Also entry must be within the image area. */
  if(!tzbsp_subsys_is_valid_area(proc, entry, entry + sizeof(entry)))
  {
    TZBSP_ERROR_CODE(VALIDATE_IMAGE_AREA_INVALID_ENTRY);
    return -E_FAILURE;
  }

  /* All image segments must be within the image area (either EBI or internal
   * memory). */
  for(i = 0; i < count; i++, hdr++)
  {
    if(tzbsp_is_valid_segment(hdr))
    {
      start = hdr->p_paddr;
      end = hdr->p_paddr + hdr->p_memsz;

      if(!tzbsp_subsys_is_valid_area(proc, start, end))
      {
        TZBSP_ERROR_CODE(VALIDATE_IMAGE_AREA_INVALID_SEGMENT);
        return -E_FAILURE;
      }
    }
  }

  return E_SUCCESS;
}

/* @copydoc tzbsp_pil_lock_xpu */
int32 tzbsp_pil_lock_xpu(tz_pil_proc_e_type proc, const Elf32_Phdr* hdr,
                         uint32 count, uint32 entry)
{
  int ret = E_SUCCESS;

  do
  {
    if(tzbsp_validate_image_area(proc, hdr, count, entry) < 0)
    {
      TZBSP_ERROR_CODE(PIL_LOCK_XPU_IMAGE_AREA_INVALID);
      ret = -E_FAILURE;
      break;
    }

    if (tzbsp_subsys_lock(proc) < 0)
    {
      TZBSP_ERROR_CODE(PIL_LOCK_XPU_SUBSYS_LOCK_FAILED);
      ret = -E_FAILURE;
      break;
    }
  } while(0);

  return ret;
}

int32 tzbsp_pil_unlock_xpu(tz_pil_proc_e_type proc)
{
  if (tzbsp_subsys_tear_down(proc, TZBSP_TEAR_DOWN_CPU) < 0)
  {
    TZBSP_ERROR_CODE(PIL_UNLOCK_XPU_TEAR_DOWN_FAILED);
    return -E_FAILURE;
  }

  if (tzbsp_subsys_unlock(proc, FALSE) < 0)
  {
    TZBSP_ERROR_CODE(PIL_UNLOCK_XPU_UNLOCK_FAILED);
    return -E_FAILURE;
  }

  return E_SUCCESS;
}

int32 tzbsp_pil_unlock_xpu_zero(tz_pil_proc_e_type proc)
{
  if (tzbsp_subsys_tear_down(proc, TZBSP_TEAR_DOWN_CPU) < 0)
  {
    TZBSP_ERROR_CODE(PIL_UNLOCK_ZERO_XPU_TEAR_DOWN_FAILED);
    return -E_FAILURE;
  }

  if (tzbsp_subsys_unlock(proc, TRUE) < 0)
  {
    TZBSP_ERROR_CODE(PIL_UNLOCK_ZERO_XPU_UNLOCK_FAILED);
    return -E_FAILURE;
  }

  return E_SUCCESS;
}

/**
  * Fill tz_share_settings_t struct with settings for given proc and share_type
  * @return E_SUCCESS on success, negative value on failure.
  */
static int tzbsp_pil_get_share_settings (tz_share_settings_t* settings, uint32 proc, uint32 share_type)
{
  if(!settings)
    return -E_FAILURE;
  
  settings->rvmid = TZBSP_VMID_AP_BIT;
  settings->wvmid = TZBSP_VMID_AP_BIT;
  settings->mask = 0;
    
  uint32 re = 0;
  uint32 we = 0;
  //set vmid access based on share_type
  if(share_type)
  {
    if(share_type & TZ_PIL_SHARE_TYPE_READ) //Read access
    {
      re = TRUE_MASK;
    }
    if(share_type & TZ_PIL_SHARE_TYPE_WRITE) //Write access
    {
      we = TRUE_MASK;
    }
  }

  //check that proc is valid and assign vmid access
  switch(proc)
  {
    case TZ_PIL_AUTH_QDSP6_PROC:
      settings->xpu = HAL_XPU2_BIMC_MPU0;
      settings->mask |= TZBSP_VMID_LPASS_BIT;
      settings->rvmid |= re & (TZBSP_VMID_LPASS_BIT);
      settings->wvmid |= we & (TZBSP_VMID_LPASS_BIT);
      break;
    //add future procs here
    default:
      return -E_FAILURE;
  }
  return E_SUCCESS;  
}

int tzbsp_pil_share_memory (uint32 start, uint32 size, uint32 proc, uint32 share_type)
{ 

  int res;
  tz_share_settings_t settings = {0};

  res = tzbsp_pil_get_share_settings(&settings, proc, share_type);
  if(res != E_SUCCESS){
    return res;
  }
  
  if(start == 0 && size == 0){
    res = tzbsp_mpu_set_umr_vmid(settings.xpu, settings.mask, settings.rvmid, settings.wvmid);
  }
  else{

    if(IS_NOT_4K_ALIGNED(start) || IS_NOT_4K_ALIGNED(size)){
      return -E_FAILURE;
    }
    //lock/unlock the memory range
    if(!share_type)//unlock & revoke sharing
    {
      res = tzbsp_mpu_pil_unlock_memory (settings.xpu, start, size);
    } 
    else if(share_type & (~(TZ_PIL_SHARE_TYPE_WRITE | TZ_PIL_SHARE_TYPE_READ)))
    {
      return -E_FAILURE;
    }
    else // lock and share
    {
      res = tzbsp_mpu_pil_lock_memory (settings.xpu, start, size, 
        settings.rvmid, settings.wvmid);
    }
  }
  return res;
}


