#ifndef __CECL_EXT
#define __CECL_EXT

/*===========================================================================

                    Crypto Engine Chipset External Header

GENERAL DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2000-2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: 

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/10/14   amen    Initial version
===========================================================================*/

//Driver version, called in FVER, for HLOS to
//check
//0.0.0 no new support
#define CE_DRV_MAJ_REV   0
#define CE_DRV_MIN_REV   0
#define CE_DRV_STEP_REV  0 


#endif
