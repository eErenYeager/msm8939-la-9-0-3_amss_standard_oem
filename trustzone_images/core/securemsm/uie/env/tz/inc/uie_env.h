/*===========================================================================
              Unified Image Encryption (UIE) Environment Interface

GENERAL DESCRIPTION
  Definitions and types for UIE (document 80-NP914-1 Rev A) on TZ.

Copyright (c) 2014 QUALCOMM Technologies Incorporated.  All Rights Reserved.
===========================================================================*/
#ifndef _UIE_ENV_H
#define _UIE_ENV_H

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "tzbsp_log.h"       // TZBSP_LOG_DBG
#include "tzbsp_mem.h"       // tzbsp_malloc, tzbsp_free
#include <stringl/stringl.h> // memscpy
#include <IxErrno.h>

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

#define UIE_MALLOC  tzbsp_malloc
#define UIE_FREE    tzbsp_free
#define UIE_MEMSCPY memscpy
#define UIE_MEMSET  secure_memset

#define UIE_LOG_MSG(fmt, ...) TZBSP_LOG_DBG(fmt, ##__VA_ARGS__)
#define UIE_LOG_HEX(label, buf, buflen)              \
  do {                                               \
    if (uie_bin_to_hex_str(                          \
          uie_hex_log_buf, MAX_BIN_TO_STR_MSG_LEN,   \
          (uint8*)buf, buflen) == 0)                 \
    {                                                \
      UIE_LOG_MSG("%s: %s", label, uie_hex_log_buf); \
    }                                                \
  } while(0);

#define MAX_BIN_TO_STR_MSG_LEN 256
static char uie_hex_log_buf[MAX_BIN_TO_STR_MSG_LEN];

static inline int32 uie_bin_to_hex_str(char *str, uint32 len, uint8 *buf,
                                       uint32 buflen)
{
  uint32 i = 0;
  uint32 j = 0;
  int8 v;

  if (2*buflen + 1 > len)
  {
    buflen = len/2 -1;
  }

  for (i = 0; i < buflen; i++)
  {
    for (j = 0; j < 2; j++)
    {
      v = (j & 1) ? (buf[i] & 0x0f) : (buf[i] >> 4) & 0x0f;

      if (v >= 0 && v <= 9)
      {
        str[(i * 2) + j] = '0' + v;
      }
      else
      {
        str[(i * 2) + j] = 'a' + (v - 10);
      }
    }
  }

  str[2*buflen] = '\0';
  return 0;
}

extern void tzbsp_dtlb_inval_all(void) __attribute__((weak));
extern int tzbsp_mmu_map(uint32, uint32 , size_t, uint32 );

#define TZ_MEM_DEVICE     (1 << 0 )
#define TZ_READ_WRITE_PL1 (0 << 4 )
#define TZ_INR_SHAREABLE  (2 << 14)
#define TZ_NON_EXECUTABLE (1 << 6 )
#define TZ_TTE_INVALID    (1 << 11)

/**
 * @brief Map the device memory into the MMU for access
 *
 * @return - 0 on success; otherwise error
 */
static inline int uie_map_fuse_area(uint32 addr, uint32 len)
{
  int retval = 0;
  retval = tzbsp_mmu_map(addr, addr, len >> 10,
                         (TZ_MEM_DEVICE | TZ_READ_WRITE_PL1 |
                         TZ_INR_SHAREABLE | TZ_NON_EXECUTABLE));

  if (retval == E_SUCCESS)
  {
    tzbsp_dtlb_inval_all();
  }

  return retval;
}

/**
 * @brief Unmap the device memory into the MMU for access
 *
 * @return - 0 on success; otherwise error
 */
static inline void uie_unmap_fuse_area(uint32 addr, uint32 len)
{
  int retval = 0;
  retval = tzbsp_mmu_map(addr, addr, len >> 10, TZ_TTE_INVALID);

  if (retval == E_SUCCESS)
  {
    tzbsp_dtlb_inval_all();
  }
}
#endif
