#===============================================================================
#
# QSEE Secure UI Service
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
# Qualcomm Technologies Proprietary and Confidential.
#
#-------------------------------------------------------------------------------
#
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 25/06/14   rz      Added secure indicator support
# 05/09/13   sn      Initial version
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}/core/securemsm/sse/qsee/SecureUI/src"
env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

#-------------------------------------------------------------------------------
# Publish Private APIs
#-------------------------------------------------------------------------------

env.PublishPrivateApi('SECUREMSM', [
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureUI/inc",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureUI/src",
   "${INC_ROOT}/core/securemsm/trustzone/qsapps/libs/services/src",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureDisplay/libgd/include",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureDisplay/libpng/include",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureDisplay/zlib/include",
   "${INC_ROOT}/core/securemsm/sse/qsee/SecureUILib/include",
])

# Logging APIs
env.PublishPrivateApi('SSE_LOG_API', [
   '${INC_ROOT}/core/securemsm/sse/log/inc',
])

# Common includes
env.PublishPrivateApi('SSE_COMMON_API', [
   '${INC_ROOT}/core/securemsm/sse/common/include',
])

# Secure Touch includes
env.PublishPrivateApi('SSE_SECURE_TOUCH_API', [
   '${INC_ROOT}/core/securemsm/sse/SecureTouch/common/include',
   '${INC_ROOT}/core/securemsm/sse/SecureTouch/layout/include',
])
env.PublishPrivateApi('SSE_TOUCH_CONTROLLER_API', [
   '${INC_ROOT}/core/securemsm/sse/SecureTouch/controller/inc',
])
env.PublishPrivateApi('SSE_TOUCH_CONTROLLER_QSEE_API', [
   '${INC_ROOT}/core/securemsm/sse/qsee/SecureTouch/drTs/include',
])
env.PublishPrivateApi('SSE_TOUCH_SIDE_CHANNELS_QSEE_API', [
   '${INC_ROOT}/core/securemsm/sse/qsee/SecureTouch/sidechannels/include',
   '${INC_ROOT}/core/securemsm/sse/qsee/SecureTouch/sidechannels/modules/include',
])
env.PublishPrivateApi('SECURE_INDICATOR_QSEE_API', [
   '${INC_ROOT}/core/securemsm/trustzone/qsapps/secureindicator/inc',
])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------

INC_PATH_API = ["${INC_ROOT}/core/api/kernel/libstd/stringl"]
env.Append(CPPPATH = INC_PATH_API)

env.Append(CCFLAGS = " -DPNG_SEQUENTIAL_READ_SUPPORTED")

vars = Variables()
if 'form_factor_layout' not in env:
   vars.Add(EnumVariable('form_factor_layout', 'form factor', '8994_liquid',
                         allowed_values=('8994_fluid', '8994_liquid'),
                         map={}, ignorecase=2))
vars.Update(env)


SOURCES_LIB = [
  '${BUILDPATH}/SecureUI.c',
]

SOURCES_TUI = [
  '${BUILDPATH}/secure_display_renderer.c',
  '${BUILDPATH}/layout_manager.c',
  '${BUILDPATH}/qsee_tui_dialogs.c',
]

SOURCES_LAYOUT = [
  "${BUILDPATH}/load_layout_images_${form_factor_layout}.c",
  '${BUILDPATH}/layout_${form_factor_layout}.c',
]

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddBinaryLibrary([
   'SECUREUISAMPLE_IMAGE',
   'SECUREPIN_IMAGE',
   'PKCS11_IMAGE',
   ],
   '${BUILDPATH}/secure_ui',
   SOURCES_LIB)

env.AddBinaryLibrary([
   'SECUREUISAMPLE_IMAGE',
   'SECUREPIN_IMAGE',
   'PKCS11_IMAGE',
   ],
   '${BUILDPATH}/secure_ui_tui',
   SOURCES_TUI)

env.AddBinaryLibrary([
   'SECUREUISAMPLE_IMAGE',
   'SECUREPIN_IMAGE',
   'PKCS11_IMAGE',
   ],
   '${BUILDPATH}/secure_ui_layout',
   SOURCES_LAYOUT)
#-------------------------------------------------------------------------------
# Pack out files
#-------------------------------------------------------------------------------
NOSHIP_SOURCES = env.FindFiles("*", SRCPATH)
NOSHIP_SOURCES += env.FindFiles("*", "${BUILD_ROOT}/core/securemsm/sse/qsee/SecureUI/scripts")
env.CleanPack([
   'SECUREUISAMPLE_IMAGE',
   'SECUREPIN_IMAGE',
   'PKCS11_IMAGE'
   ], NOSHIP_SOURCES)
