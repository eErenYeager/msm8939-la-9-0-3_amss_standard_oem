#ifndef QSEE_TUI_H_
#define QSEE_TUI_H_

/** @file qsee_tui_dialogs.h
 * @brief
 * This file contains the interfaces to Secure UI GP dialog screens.
 */

/*===========================================================================
 Copyright (c) 2013-2014 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Confidential and Proprietary - Qualcomm Technologies, Inc.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
25/06/14   rz      Added secure indicator support
07/07/14   leo     (Tech Pubs) Edited/added Doxygen comments and markup.
31/10/13   sn      Initial Version.

===========================================================================*/

#include "SecureTouchLayout.h"

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/** @addtogroup qsee_gp_screens
@{ */

/** TUI dialog return values, i.e., status and error codes for the GP screens API. */
typedef enum {
	TUI_LAYOUT_ERROR                   = -4,  /**< Internal layout management error. */
	TUI_ILLEGAL_INPUT                  = -3,  /**< Illegal input was received. */
	TUI_SYS_EVENT_ABORTED              = -2,  /**< TUI session was aborted by some
                                                   external abort request, e.g., an
                                                   incoming call or a power down. */
	TUI_TIMEOUT_ABORTED                = -1,  /**< TUI session was aborted due to a timeout. */
	TUI_SUCCESS                        = 0,   /**< Success. */
	TUI_OK_PRESSED                     = 1,   /**< OK button was pressed by the user. */
	TUI_CANCEL_PRESSED                 = 2,   /**< Cancel button was pressed by the user. */
	TUI_RIGHT_PRESSED                  = 3,   /**< Right button of the message screen was
                                                   pressed by the user. */
	TUI_LEFT_PRESSED                   = 4,   /**< Left button of the message screen was
                                                   pressed by the user. */
	TUI_MIDDLE_PRESSED                 = 5,   /**< Middle button of the message screen was
                                                   pressed by the end user. */
	TUI_RETVAL_MAX                     = 0x7FFFFFFF
} qsee_tui_dialog_ret_val_t;

/** TUI GP screen type. */
typedef enum {
	TUI_DIALOG_MESSAGE,   /**< Message screen. */
	TUI_DIALOG_PIN,       /**< PIN entry screen. */
	TUI_DIALOG_PASSWORD,  /**< Login and password entry screen. */
	TUI_DIALOG_MAX = 0x7FFFFFFF
} qsee_tui_dialog_t;

/** TUI keypad mode. */
typedef enum {
	TUI_KEYPAD_NORMAL,      /**< Keypad mode is Normal. */
	TUI_KEYPAD_RANDOMIZED,  /**< Keypad mode is Randomized. */
	TUI_KEYPAD_MAX = 0x7FFFFFFF
} qsee_tui_keypad_mode_t;

/** Defines a general buffer parameter. */
typedef struct{
	uint8_t* pu8Buffer;  /**< Start address of the buffer. */
	uint32_t u32Len;     /**< Length of the buffer in bytes. */
} qsee_tui_buffer_t;

/** Defines the parameters for user input in a dialog screen. */
typedef struct{
	uint32_t u32MinLen;              /**< Minimum length in characters (UTF-8
                                          encoding may take more than one byte
                                          to represent a character). */
	uint32_t u32MaxLen;              /**< Maximum length in characters. */
	qsee_tui_buffer_t inpValue;      /**< Buffer parameter to be filled with
                                          user input. */
	qsee_tui_buffer_t defaultValue;  /**< Buffer parameter that contains the
                                          default value to be shown in the
                                          dialog screen. */
	qsee_tui_buffer_t label;         /**< Buffer parameter that contains the
                                          label text to be shown above the
                                          input object if it is empty. */
	uint32_t* pu32InpValueLen;       /**< Output parameter; the actual length
                                          of the user input in characters. */
} qsee_tui_input_params_t;

/** Common parameters for all GP screens. */
typedef struct{
	int32_t n32TimeOut;                 /**< Touch session timeout (in milliseconds),
                                               i.e., the maximum time to wait for
                                               user input. 0 means return
                                             immediately, and 1 means no
                                             timeout (wait indefinitely). */
	qsee_tui_buffer_t title;            /**< Buffer parameter that contains the
                                             title text of the screen. */
	qsee_tui_buffer_t description;      /**< Description label text. */ 
	qsee_tui_buffer_t logo;             /**< Logo image buffer encoded as PNG. */
	qsee_tui_buffer_t secureIndicator;  /**< Secure indicator image buffer encoded
                                             as PNG. */  
} qsee_tui_dialog_common_params_t;

/** Defines the parameters of a configurable button in a dialog screen.. */
typedef struct{
	qsee_tui_buffer_t btnLabel;  /**< Label to be shown on the button. */
	uint32_t bShowBtn;           /**< Boolean flag to show the button. */
	uint32_t bDisableBtn;        /**< Boolean flag to disable (gray out) the button. */
} qsee_tui_button_params_t;

/** Defines the parameters that are specific to the message screen. */
typedef struct{
	qsee_tui_buffer_t msg;                     /**< Buffer parameter that contains
                                                    the message to be shown on the screen. */
	qsee_tui_button_params_t rightBtnParams;   /**< Parameters of the right button
                                                    of the message screen. */
	qsee_tui_button_params_t leftBtnParams;    /**< Parameters of the left button
                                                    of the message screen. */
	qsee_tui_button_params_t middleBtnParams;  /**< Parameters of the middle button
                                                    of the message screen. */
} qsee_tui_dialog_msg_params_t;

/** Defines the parameters that are specific to the pin entry screen. */
typedef struct {
	qsee_tui_input_params_t pin;        /**< Buffer details for storing the user PIN. */
	uint32_t bHideInputBox;             /**< Boolean flag to to hide the user input on the screen. */
	LayoutInputDisplayMode_t inpMode;   /**< User input (for the PIN) display mode. */
	qsee_tui_keypad_mode_t keyPadMode;  /**< Keypad mode; randomized/normal. */
} qsee_tui_dialog_pin_params_t;

/** Defines the parameters that are specific to the login and password entry screen.

    @note1hang At least one of the flags in bUserNameInpShow and bPasswordInpShow
    must be set to TRUE; otherwise, it is considered an illegal input.
*/
typedef struct {
	qsee_tui_input_params_t username;  /**< User input parameters for the username . */
	uint32_t bUserNameInpShow;         /**< Boolean flag to show the username input object on the screen. */
	uint32_t bPasswordInpShow;         /**< Boolean flag to show the password input object on the screen. */
	qsee_tui_input_params_t password;  /**< User input parameters for password. */
	LayoutInputDisplayMode_t inpMode;  /**< Password display mode. */
} qsee_tui_dialog_pswd_params_t;

/** Defines the general parameter set for all screens. */
typedef struct qsee_tui_dialog_params_s {
	qsee_tui_dialog_common_params_t dialogCommonParams;  /**< Common parameters, e.g., timeout,
                                                              title, description, logo. */
	qsee_tui_dialog_t dialogType;                        /**< Dialog screen type. */
    /** Union of the parameters of the different screen types. */
	union {
		qsee_tui_dialog_msg_params_t  msgDialogParams;   /**< Message dialog parameters. */
		qsee_tui_dialog_pin_params_t  pinDialogParams;   /**< PIN dialog parameters. */
		qsee_tui_dialog_pswd_params_t pswdDialogParams;  /**< Password dialog parameters. */
	};
} qsee_tui_dialog_params_t;

typedef struct {
	uint8_t* pu8SecureIndicator;
	uint32_t u32IndicatorSize;
	uint32_t bIsValid;
} qsee_tui_secure_indicator_t;

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/*****************************************************************************************************************/

/**
   External function used to initialize the Secure UI GP screen service.

   @note1hang This function must be called before any other GP screen functions
              are called. For a description of the GP screen functions flow,
              see Section \xref{sec:GPScreenCallFlow}.

   @return
   TUI_SUCCESS      -- On successful initialization. \n
   TUI_LAYOUT_ERROR -- On an initialization failure. \n
   See #qsee_tui_dialog_ret_val_t for descriptions of return codes.

   @dependencies
   None.
*/
qsee_tui_dialog_ret_val_t qsee_tui_init(void);

/*****************************************************************************************************************/

/**
\xreflabel{sec:qseeTuiDialog}

   External function for using the three GP screens: LOGIN, GET_PIN, and
   SHOW_MESSAGE. For a description of the GP screens, see Section @xref{sec:GpScreens}.

   This function does not start the screen operation. It only sets up the
   screen so that when the touch session starts, the required screen appears.
   The operation comletes during the touch session.

   @note1hang This function starts a secure display session (if there is no
   active session of the previous screen). So, until the touch session starts,
   a black screen is displayed. 
   @newpage
   @caution1hang The status parameter, as well as other output parameters that
   are contained in the dialogParams parameter, e.g., the buffers that are to
   be filled with the user input, <i>must be read only after the touch session
   ends</i>. A notification about the touch session ending is received from
   the HLOS after the return of UseSecureTouch().

   @param[in,out]  dialogParams  General parameters of the dialog, including
                                 the dialog type and the relevant input/output
                                 parameters. \n
                                 @note1 See Section @xref{sec:GPScreenParamLim}
                                 for a description of parameter limitations for
                                 this parameter.
   @param[out]     status        Final status of the dialog execution, which is
                                 to be read only after the touch session is
                                 finished.

   @return
   TUI_SUCCESS       -- On successful setup. \n
   TUI_LAYOUT_ERROR  -- On a layout management error. \n
   TUI_ILLEGAL_INPUT -- On an illegal/NULL input. \n
   See #sec_ui_err_t for descriptions of return codes.

   @note1hang The return value represents the status of setting up the
   screen, as opposed to the status parameter, which reflects the final status
   of the operation of the GP screen.

   @dependencies
   Must be called after qsee_tui_init.
*/
qsee_tui_dialog_ret_val_t qsee_tui_dialog(qsee_tui_dialog_params_t* dialogParams, qsee_tui_dialog_ret_val_t* status);

/*****************************************************************************************************************/

/**
   @brief
   External function for retrieving the secure indicator struct, which holds a pointer to the secure indicator buffer.
   When calling this function the buffer is allocated and the pointer parameter is set to point to the struct saved in the service.
   This struct can then be used by the requesting app in order to decapsulate the secure indicator into the internal buffer and set its size.
   After a successful decapsulation, the valid field should be set to 1 by the requesting app.

   @param[in/out]  indicator  - Secure indicator struct. Set to point to the struct saved in the service.

   @return
   TUI_SUCCESS              on successful operation
   TUI_LAYOUT_ERROR         on memory allocation failure

   @dependencies
   None.

   @sideeffects
   Allocates a buffer large enough to hold the secure indicator

*/
qsee_tui_dialog_ret_val_t qsee_tui_get_secure_indicator_buffer(qsee_tui_secure_indicator_t** indicator);

/*****************************************************************************************************************/

/**
   @brief
   External function for releasing the secure indicator buffer and clearing the valid and size fields.
   This function should be called when the app no longer needs the secure indicator (usually on shut down).

   @return
   TUI_SUCCESS              on any result (nothing to do if releasing the buffer failed...)

   @dependencies
   None.

   @sideeffects
   The secure indicator will not be available anymore (unless it is set again).

*/
qsee_tui_dialog_ret_val_t qsee_tui_release_secure_indicator(void);

/*****************************************************************************************************************/

/**
   External function for closing the Secure UI GP screen service.

   @note1hang If several GP screens must be called one after
   another, call this function only after the final one.

   @return
   TUI_SUCCESS      -- On successful teardown. \n
   TUI_LAYOUT_ERROR -- On a display/touch teardown error. \n
   See #qsee_tui_dialog_ret_val_t for descriptions of return codes.

   @dependencies
   None.

   @sideeffects
   Stops the secure display session.
*/
qsee_tui_dialog_ret_val_t qsee_tui_tear_down(void);

/** @} */ /* end_addtogroup qsee_gp_screens */

#endif /* QSEE_TUI_H_ */
