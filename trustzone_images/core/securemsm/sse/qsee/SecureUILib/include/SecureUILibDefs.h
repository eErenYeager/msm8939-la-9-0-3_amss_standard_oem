/*===========================================================================
 * Copyright(c) 2013 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

#pragma once

#include <stdint.h>

/** Commands indication from QSEE App to Android Lib layer */
typedef enum {
  SEC_TOUCH_CMD_CONTINUE      = 0,
  SEC_TOUCH_CMD_CANCELLED     = 1,
  SEC_TOUCH_CMD_COMPLETED     = 2,
  SEC_TOUCH_CMD_MAX           = SEC_TOUCH_CMD_COMPLETED,
  SEC_TOUCH_CMD_INVALID       = 0x7FFFFFFF
} sec_touch_cmd_t;

/** Error indication from Android Lib layer to QSEE App */
typedef enum {
  SEC_TOUCH_ERR_STARTED       = 0, /** Secure touch started */
  SEC_TOUCH_ERR_STOPPED       = 1, /** Secure touch started */
  SEC_TOUCH_ERR_DATA          = 2, /** Interrupt fired */
  SEC_TOUCH_ERR_TIMEOUT       = 3, /** Timeout */
  SEC_TOUCH_ERR_POWER         = 4, /** Abort request to service a power event */
  SEC_TOUCH_ERR_ABORT         = 5, /** Abort request */
  SEC_TOUCH_ERR_MAX           = SEC_TOUCH_ERR_ABORT,
  SEC_TOUCH_ERR_INVALID       = 0x7FFFFFFF
} sec_touch_err_t;
