/** @file SecureUILib.h
 * @brief
 * This file contains the definitions of the constants, data structures
 * and interfaces to the QSEE Secure User Interface library
 */

/*===========================================================================
 Copyright (c) 2013 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Qualcomm Technologies Proprietary and Confidential.
===========================================================================*/

#pragma once
#include <TouchApiDefs.h>
#include <SecureUILibDefs.h>


/**
  @brief Application callback called in case of touch events.

  @param[in]   err     - Error code from the operation
  @param[in]   fingers - Fingers related to the event
  @param[out]  timeout - Amount of milliseconds to wait for the next touch

  @return
  Command for the next touch operation
 * */

typedef sec_touch_cmd_t (*sec_touch_callback_t)(sec_touch_err_t err, struct tsFingerData *fingers, int32_t *timeout);

/*----------------------------------------------------------------------------
 * Function Declarations and Documentation
 * -------------------------------------------------------------------------*/

/**
  @brief Determines if a command is intended for Secure UI and if so processes it.
  This function returns TRUE, no further processing is required by the calling
  application.

  @param[in]   req  - Request from QSApp Client.
  @param[in]   cmdlen   - Length of the request buffer in bytes.
  @param[out]  rsp  - Response buffer
  @param[in]   rsplen   - Length of the request buffer in bytes.

  @return
  TRUE 	- if the command was intended for Secure UI and has been processed
  FALSE - otherwise
*/
boolean secUiProcessCmd(void* req, uint32 cmdlen, void* rsp, uint32 rsplen);

/**
  @brief Determines if a command is intended for Secure UI.

  @return
  TRUE 	- if the command is intended for Secure UI
  FALSE - otherwise
 */
boolean secUiCmd(void* req, uint32 cmdlen);

/**
  @brief Registers the Secure Touch callback.

  @param[in]   touch_callback  - Callback function, NULL to unregister
*/
void secUiRegisterTouchCallback(sec_touch_callback_t callback);

/**
  @brief Secure Touch is active

  @return TRUE if active, FALSE otherwise.
*/
boolean secUiTouchIsActive(void);

/**
  @brief Stop Secure Touch
*/
boolean secUiTouchStop(void);

/**
  @brief Set touch panel size

  @param[in] width    Width in pixels
  @param[in] height   Height in pixels
*/
void secUiSetPanelSize(uint32_t width, uint32_t height);
