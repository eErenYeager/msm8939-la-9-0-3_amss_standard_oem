/*
 * Copyright (c) 2013 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */
#pragma once

#include "qsee_i2c.h"

#define I2C_ADDRESS 0x20

#define I2C_DEVICE  QSEE_I2CPD_DEVICE_2 /* BLSP 1 QUP1 */


