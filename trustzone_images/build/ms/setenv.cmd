@echo off
rem ==========================================================================
rem
rem  CBSP Buils system
rem
rem  General Description
rem     SetEnv batch file.
rem
rem Copyright (c) 2011 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem
rem --------------------------------------------------------------------------
rem
rem $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/build/ms/setenv.cmd#1 $
rem $DateTime: 2018/02/07 00:37:16 $
rem $Author: mplp4svc $
rem $Change $
rem                      EDIT HISTORY FOR FILE 
rem
rem  This section contains comments describing changes made to the module.
rem  Notice that changes are listed in reverse chronological order.
rem
rem when       who     what, where, why
rem --------   ---     ---------------------------------------------------------
rem  09/19/11  sy    Initial revision
rem 
rem ==========================================================================

rem ========== Call the RVCT version ==========================
IF EXIST C:\Apps\ARMCT6.01\48 (
rem    CALL \\stone\aswcrm\smart\NT\Bin\ARMCT600B6_64BIT.cmd
  ) ELSE (
      ECHO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ECHO ARM Compiler 6 not found!  Please install "ARMUpdater - ARMUpdater ARMCT6.01 install" in "Run Advertised Programs" under "Control Panel"
      ECHO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  )
)

rem ======== Set Klocwork =============================
SET BUILDSPEC=KLOCWORK

set ROOT_DRIVE=C:
set APPS_DIR=Apps
set APPS_PATH=%ROOT_DRIVE%\%APPS_DIR%
set UTILROOT=%ROOT_DRIVE%\utils

rem
rem GNU, Perl and Python paths
rem
set GNUPATH=%UTILROOT%\cygwin\bin
set CRMPERL=C:\CRMApps\apps\Perl\bin
set PERLPATH=C:\perl\bin
set PYTHON_PATH=C:\CRMApps\apps\Python262

rem
rem ARM General Definitions used by Qualcomm build goo
rem
set ARMTOOLS=ARMCT6

rem
rem ARM Include, Lib and Bin Variables (used by compiler toolchain)
rem
set COMPILER_VER=6.01
set ARMROOT=C:\Apps\ARMCT6.01\48
set ARMCC5BIN=%ARMROOT%\bin
set ARMCC5INC=%ARMROOT%\include
set ARMCC5LIB=%ARMROOT%\lib

set ARMBIN=%ARMCC5BIN%
set ARMINC=%ARMCC5INC%
set ARMLIB=%ARMCC5LIB%

set LLVMROOT==C:\Apps\ARMCT6.01\48
set LLVMBIN=C:\Apps\ARMCT6.01\48\bin

rem
rem Path for ARM, GNU, Python, and Perl tools
rem
set path=%LLVMBIN%;%PERLPATH%;%CRMPERL%;%PYTHON_PATH%;%GNUPATH%;%ARMCC5BIN%;%UTILROOT%;%path%
rem set ARMLMD_LICENSE_FILE=C:\Apps\ARMCT6.00\21\sw\info\license.dat
set ARMLMD_LICENSE_FILE=7117@license-wan-arm1;7117@license-hyd1;7117@license-hyd2;7117@license-hyd3
rem Dump environment to stdout so that calling scripts can read it.
set



