#!/bin/bash

cd `dirname $0`
sh build.sh CHIPSET=msm8952 tz hyp
sh build_qsapps.sh CHIPSET=msm8952 fingerprint chamomile commonlib fidocrypto isdbtmm keymaster pkcs11 playready sampleap secure_ui_sample secureindicator securemm securepin tbase tqs widevine USES_FLAGS=USES_PLAYREADY_PACK
sh build.sh CHIPSET=msm8952 tz hyp USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
sh build_qsapps.sh CHIPSET=msm8952 chamomile commonlib fidocrypto isdbtmm keymaster pkcs11 playready sampleap secure_ui_sample secureindicator securemm securepin tbase tqs widevine USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
sh build.sh CHIPSET=msm8952 tz hyp -c --implicit-deps-unchanged --cleanpack
sh build_qsapps.sh CHIPSET=msm8952 chamomile commonlib fidocrypto isdbtmm keymaster pkcs11 playready sampleap secure_ui_sample secureindicator securemm securepin tbase tqs widevine -c --implicit-deps-unchanged --cleanpack
sh build.sh CHIPSET=msm8952 tz hyp
sh build_qsapps.sh CHIPSET=msm8952 chamomile commonlib fidocrypto isdbtmm keymaster pkcs11 playready sampleap secure_ui_sample secureindicator securemm securepin tbase tqs widevine
