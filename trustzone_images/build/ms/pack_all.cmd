@echo off
rem ==========================================================================
rem
rem  CBSP Buils system
rem
rem  General Description
rem     build batch file.
rem
rem Copyright (c) 2009-2009 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem
rem --------------------------------------------------------------------------
rem
rem $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/build/ms/pack_all.cmd#1 $
rem $DateTime: 2018/02/07 00:37:16 $
rem $Author: mplp4svc $
rem $Change: 15409075 $
rem                      EDIT HISTORY FOR FILE
rem
rem  This section contains comments describing changes made to the module.
rem  Notice that changes are listed in reverse chronological order.
rem
rem when       who     what, where, why
rem --------   ---     ---------------------------------------------------------
rem 07/08/13   ab      Add support for securemm for msm8994
rem 10/17/13   ib      Add support for securemm for apq8084
rem ==========================================================================

setlocal

rem ===== Setup Paths=====================================================
SET BUILD_ROOT=%~dp0..\..
SET PACK_ROOT=%~dp0..\..\..

IF "%1"=="CHIPSET" (
  IF "%2" == "mpq8092" (
    del %PACK_ROOT%\HY11_8092
    goto PACK_8092_build
  )
  IF "%2" == "apq8084" (
    del %PACK_ROOT%\HY11_8084
    goto PACK_8084_build
  )
  IF "%2" == "msm8962" (
    del %PACK_ROOT%\HY11_8962
    goto PACK_8962_build
  )
  IF "%2" == "mdm9x35" (
    del %PACK_ROOT%\HY11_9x35
    goto PACK_9x35_build
  )
  IF "%2" == "msm8916" (
    del %PACK_ROOT%\HY11_8916
    goto PACK_8916_build
  )
  IF "%2" == "msm8994" (
    del %PACK_ROOT%\HY11_8994
    goto PACK_8994_build
  )
  echo Unknown chipset...
  EXIT /B
)
IF "%1" == "" (
  del %PACK_ROOT%\HY11_8092
  del %PACK_ROOT%\HY11_8084
  del %PACK_ROOT%\HY11_8962
  del %PACK_ROOT%\HY11_9x35
  del %PACK_ROOT%\HY11_8916
  del %PACK_ROOT%\HY11_8994
  goto PACK_all
)

echo Please specify a valid chipset e.g. CHIPSET=msm8x26
EXIT /B

:PACK_8092_build
CD %BUILD_ROOT%\build\ms
CALL build.cmd CHIPSET=mpq8092 tz sampleapp commonlib
IF EXIST %BUILD_ROOT%\build\ms\bin\LABAANAA\cmnlib.mbn GOTO PACK_8092_deepclean
echo **Build errors...
echo One or more images failed to build for mpq8092. Please refer to build log for compilation errors.
EXIT /B

:PACK_8092_deepclean
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=mpq8092 tz sampleapp commonlib USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
GOTO PACK_8092_copy
echo **Build errors...
echo Deepclean failed for mpq8092. Please refer to build log for errors.
EXIT /B

:PACK_8092_copy
CALL xcopy %PACK_ROOT%\trustzone_images %PACK_ROOT%\HY11_8092\trustzone_images /s/e/i
IF EXIST %PACK_ROOT%\HY11_8092 GOTO PACK_8092_cleanpack
echo **Build errors...
echo HY11_8092 creation failed for mpq8092. Please refer to build log for errors.
EXIT /B

:PACK_8092_cleanpack
CD %PACK_ROOT%\HY11_8092\trustzone_images\build\ms
CALL build.cmd CHIPSET=mpq8092 tz sampleapp commonlib -c --implicit-deps-unchanged --cleanpack
GOTO PACK_8092_HY11CompileTest
echo **Build errors...
echo Clean pack failed for mpq8092. Please refer to build log for errors.
EXIT /B

:PACK_8092_HY11CompileTest
CALL %PACK_ROOT%\HY11_8092\trustzone_images\build\ms\build.cmd CHIPSET=mpq8092 tz sampleapp commonlib
IF EXIST %PACK_ROOT%\HY11_8092\trustzone_images\build\ms\bin\LABAANAA\cmnlib.mbn GOTO PACK_8092_success
echo **Build errors...
echo One or more images failed to build for mpq8092. Please refer to build log for compilation errors.
EXIT /B

:PACK_8084_build
CD %BUILD_ROOT%\build\ms
CALL build.cmd CHIPSET=apq8084 tz sampleapp commonlib
IF EXIST %BUILD_ROOT%\build\ms\bin\GAAAANAA\cmnlib.mbn GOTO PACK_8084_deepclean
echo **Build errors...
echo One or more images failed to build for apq8084. Please refer to build log for compilation errors.
EXIT /B

:PACK_8084_deepclean
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=apq8084 tz sampleapp commonlib USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
GOTO PACK_8084_copy
echo **Build errors...
echo Deepclean failed for apq8084. Please refer to build log for errors.
EXIT /B

:PACK_8084_copy
CALL xcopy %PACK_ROOT%\trustzone_images %PACK_ROOT%\HY11_8084\trustzone_images /s/e/i
IF EXIST %PACK_ROOT%\HY11_8084 GOTO PACK_8084_cleanpack
echo **Build errors...
echo HY11_8084 creation failed for apq8084. Please refer to build log for errors.
EXIT /B

:PACK_8084_cleanpack
CD %PACK_ROOT%\HY11_8084\trustzone_images\build\ms
CALL build.cmd CHIPSET=apq8084 tz sampleapp commonlib -c --implicit-deps-unchanged --cleanpack
GOTO PACK_8084_HY11CompileTest
echo **Build errors...
echo Clean pack failed for apq8084. Please refer to build log for errors.
EXIT /B

:PACK_8084_HY11CompileTest
CALL %PACK_ROOT%\HY11_8084\trustzone_images\build\ms\build.cmd CHIPSET=apq8084 tz sampleapp commonlib
IF EXIST %PACK_ROOT%\HY11_8084\trustzone_images\build\ms\bin\GAAAANAA\cmnlib.mbn GOTO PACK_8084_success
echo **Build errors...
echo One or more images failed to build for apq8084. Please refer to build log for compilation errors.
EXIT /B

:PACK_8962_build
CD %BUILD_ROOT%\build\ms
CALL build.cmd CHIPSET=msm8962 tz sampleapp commonlib
IF EXIST %BUILD_ROOT%\build\ms\bin\FASAANBA\cmnlib.mbn GOTO PACK_8962_deepclean
echo **Build errors...
echo One or more images failed to build for msm8962. Please refer to build log for compilation errors.
EXIT /B

:PACK_8962_deepclean
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=msm8962 tz sampleapp commonlib USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
GOTO PACK_8962_copy
echo **Build errors...
echo Deepclean failed for msm8962. Please refer to build log for errors.
EXIT /B

:PACK_8962_copy
CALL xcopy %PACK_ROOT%\trustzone_images %PACK_ROOT%\HY11_8962\trustzone_images /s/e/i
IF EXIST %PACK_ROOT%\HY11_8962 GOTO PACK_8962_cleanpack
echo **Build errors...
echo HY11_8962 creation failed for msm8962. Please refer to build log for errors.
EXIT /B

:PACK_8962_cleanpack
CD %PACK_ROOT%\HY11_8962\trustzone_images\build\ms
CALL build.cmd CHIPSET=msm8962 tz sampleapp commonlib -c --implicit-deps-unchanged --cleanpack
GOTO PACK_8962_HY11CompileTest
echo **Build errors...
echo Clean pack failed for msm8962. Please refer to build log for errors.
EXIT /B

:PACK_8962_HY11CompileTest
CALL %PACK_ROOT%\HY11_8962\trustzone_images\build\ms\build.cmd CHIPSET=msm8962 tz sampleapp commonlib
IF EXIST %PACK_ROOT%\HY11_8962\trustzone_images\build\ms\bin\FASAANBA\cmnlib.mbn GOTO PACK_8962_success
echo **Build errors...
echo One or more images failed to build for msm8962. Please refer to build log for compilation errors.
EXIT /B

:PACK_9x35_build
CD %BUILD_ROOT%\build\ms
CALL build.cmd CHIPSET=mdm9x35 tz 
IF EXIST %BUILD_ROOT%\build\ms\bin\JAFAANBA\tz.mbn GOTO PACK_9x35_deepclean
echo **Build errors...
echo One or more images failed to build for mdm9x35. Please refer to build log for compilation errors.
EXIT /B

:PACK_9x35_deepclean
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=mdm9x35 tz USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
GOTO PACK_9x35_copy
echo **Build errors...
echo Deepclean failed for mdm9x35. Please refer to build log for errors.
EXIT /B

:PACK_9x35_copy
CALL xcopy %PACK_ROOT%\trustzone_images %PACK_ROOT%\HY11_9x35\trustzone_images /s/e/i
IF EXIST %PACK_ROOT%\HY11_9x35 GOTO PACK_9x35_cleanpack
echo **Build errors...
echo HY11_9x35 creation failed for mdm9x35. Please refer to build log for errors.
EXIT /B

:PACK_9x35_cleanpack
CD %PACK_ROOT%\HY11_9x35\trustzone_images\build\ms
CALL build.cmd CHIPSET=mdm9x35 tz -c --implicit-deps-unchanged --cleanpack
GOTO PACK_9x35_HY11CompileTest
echo **Build errors...
echo Clean pack failed for mdm9x35. Please refer to build log for errors.
EXIT /B

:PACK_9x35_HY11CompileTest
CALL %PACK_ROOT%\HY11_9x35\trustzone_images\build\ms\build.cmd CHIPSET=mdm9x35 tz
IF EXIST %PACK_ROOT%\HY11_9x35\trustzone_images\build\ms\bin\JAFAANBA\tz.mbn GOTO PACK_9x35_success
echo **Build errors...
echo One or more images failed to build for mdm9x35. Please refer to build log for compilation errors.
EXIT /B

:PACK_8916_build
CD %BUILD_ROOT%\build\ms
CALL build.cmd CHIPSET=msm8916 tz sampleapp commonlib securemm
IF EXIST %BUILD_ROOT%\build\ms\bin\MABAANAA\cmnlib.mbn GOTO PACK_8916_deepclean
echo **Build errors...
echo One or more images failed to build for msm8916. Please refer to build log for compilation errors.
EXIT /B

:PACK_8916_deepclean
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=msm8916 tz sampleapp commonlib securemm USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
GOTO PACK_8916_copy
echo **Build errors...
echo Deepclean failed for msm8916. Please refer to build log for errors.
EXIT /B

:PACK_8916_copy
CALL xcopy %PACK_ROOT%\trustzone_images %PACK_ROOT%\HY11_8916\trustzone_images /s/e/i
IF EXIST %PACK_ROOT%\HY11_8916 GOTO PACK_8916_cleanpack
echo **Build errors...
echo HY11_8916 creation failed for msm8916. Please refer to build log for errors.
EXIT /B

:PACK_8916_cleanpack
CD %PACK_ROOT%\HY11_8916\trustzone_images\build\ms
CALL build.cmd CHIPSET=msm8916 tz sampleapp commonlib securemm -c --implicit-deps-unchanged --cleanpack
GOTO PACK_8916_HY11CompileTest
echo **Build errors...
echo Clean pack failed for msm8916. Please refer to build log for errors.
EXIT /B

:PACK_8916_HY11CompileTest
CALL %PACK_ROOT%\HY11_8916\trustzone_images\build\ms\build.cmd CHIPSET=msm8916 tz sampleapp commonlib securemm
IF EXIST %PACK_ROOT%\HY11_8916\trustzone_images\build\ms\bin\MABAANAA\cmnlib.mbn GOTO PACK_8916_success
echo **Build errors...
echo One or more images failed to build for msm8916. Please refer to build log for compilation errors.
EXIT /B

:PACK_8994_build
CD %BUILD_ROOT%\build\ms
CALL build.cmd CHIPSET=msm8994 tz sampleapp commonlib
IF EXIST %BUILD_ROOT%\build\ms\bin\GASAANBA\cmnlib.mbn GOTO PACK_8994_deepclean
echo **Build errors...
echo One or more images failed to build for msm8994. Please refer to build log for compilation errors.
EXIT /B

:PACK_8994_deepclean
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=msm8994 tz sampleapp commonlib USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
GOTO PACK_8994_copy
echo **Build errors...
echo Deepclean failed for msm8994. Please refer to build log for errors.
EXIT /B

:PACK_8994_copy
CALL xcopy %PACK_ROOT%\trustzone_images %PACK_ROOT%\HY11_8994\trustzone_images /s/e/i
IF EXIST %PACK_ROOT%\HY11_8994 GOTO PACK_8994_cleanpack
echo **Build errors...
echo HY11_8994 creation failed for msm8994. Please refer to build log for errors.
EXIT /B

:PACK_8994_cleanpack
CD %PACK_ROOT%\HY11_8994\trustzone_images\build\ms
CALL build.cmd CHIPSET=msm8994 tz sampleapp commonlib -c --implicit-deps-unchanged --cleanpack
GOTO PACK_8994_HY11CompileTest
echo **Build errors...
echo Clean pack failed for msm8994. Please refer to build log for errors.
EXIT /B

:PACK_8994_HY11CompileTest
CALL %PACK_ROOT%\HY11_8994\trustzone_images\build\ms\build.cmd CHIPSET=msm8994 tz sampleapp commonlib
IF EXIST %PACK_ROOT%\HY11_8994\trustzone_images\build\ms\bin\GASAANBA\cmnlib.mbn GOTO PACK_8994_success
echo **Build errors...
echo One or more images failed to build for msm8994. Please refer to build log for compilation errors.
EXIT /B

:PACK_8092_SUCCESS
echo ==============================================================================
echo PACK_ALL.cmd build summary for 8092
echo ==============================================================================
echo. 2>PackSuccess_8092.txt
GOTO DONE

:PACK_8084_SUCCESS
echo ==============================================================================
echo PACK_ALL.cmd build summary for 8084
echo ==============================================================================
echo. 2>PackSuccess_8084.txt
GOTO DONE

:PACK_8962_SUCCESS
echo ==============================================================================
echo PACK_ALL.cmd build summary for 8962
echo ==============================================================================
echo. 2>PackSuccess_8962.txt
GOTO DONE

:PACK_9x35_SUCCESS
echo ==============================================================================
echo PACK_ALL.cmd build summary for 9x35
echo ==============================================================================
echo. 2>PackSuccess_9x35.txt
GOTO DONE

:PACK_8916_SUCCESS
echo ==============================================================================
echo PACK_ALL.cmd build summary for 8916
echo ==============================================================================
echo. 2>PackSuccess_8916.txt
GOTO DONE

:PACK_8994_SUCCESS
echo ==============================================================================
echo PACK_ALL.cmd build summary for 8994
echo ==============================================================================
echo. 2>PackSuccess_8994.txt
GOTO DONE

:PACK_all
CALL %BUILD_ROOT%\build\ms\pack_all.cmd CHIPSET=mpq8092
IF NOT EXIST %PACK_ROOT%\HY11_8092\trustzone_images\build\ms\bin\LABAANAA\cmnlib.mbn GOTO PACK_all_error
CALL %BUILD_ROOT%\build\ms\pack_all.cmd CHIPSET=apq8084
IF NOT EXIST %PACK_ROOT%\HY11_8084\trustzone_images\build\ms\bin\GAAAANAA\cmnlib.mbn GOTO PACK_all_error
CALL %BUILD_ROOT%\build\ms\pack_all.cmd CHIPSET=msm8962
IF NOT EXIST %PACK_ROOT%\HY11_8962\trustzone_images\build\ms\bin\FASAANBA\cmnlib.mbn GOTO PACK_all_error
CALL %BUILD_ROOT%\build\ms\pack_all.cmd CHIPSET=mdm9x35
IF NOT EXIST %PACK_ROOT%\HY11_9x35\trustzone_images\build\ms\bin\JAFAANBA\tz.mbn GOTO PACK_all_error
CALL %BUILD_ROOT%\build\ms\pack_all.cmd CHIPSET=msm8916
IF NOT EXIST %PACK_ROOT%\HY11_8916\trustzone_images\build\ms\bin\MABAANBA\cmnlib.mbn GOTO PACK_all_error
CALL %BUILD_ROOT%\build\ms\pack_all.cmd CHIPSET=msm8994
IF NOT EXIST %PACK_ROOT%\HY11_8994\trustzone_images\build\ms\bin\GASAANBA\cmnlib.mbn GOTO PACK_all_error
GOTO DONE_all

:DONE
echo ***PACKING COMPLETE***
endlocal
EXIT /B

:DONE_all
echo ***PACKING COMPLETE FOR ALL TARGETS***
echo. 2>PackAllTargetsSuccess.txt
endlocal
EXIT /B

:BUILD_all_error
echo **Build errors...
echo Please refer to build log for build errors and check the image bin folder(s).
endlocal
EXIT /B
