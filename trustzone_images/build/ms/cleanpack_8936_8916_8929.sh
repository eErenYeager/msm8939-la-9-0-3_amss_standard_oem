#!/bin/bash

cd `dirname $0`
sh build.sh CHIPSET=msm8929 tz hyp
sh build_qsapps.sh CHIPSET=msm8929 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto USES_FLAGS=USES_PLAYREADY_PACK
sh build.sh CHIPSET=msm8936 tz hyp
sh build_qsapps.sh CHIPSET=msm8936 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto USES_FLAGS=USES_PLAYREADY_PACK
sh build.sh CHIPSET=msm8916 tz hyp
sh build_qsapps.sh CHIPSET=msm8916 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto USES_FLAGS=USES_PLAYREADY_PACK
sh build.sh CHIPSET=msm8929 tz hyp USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
sh build_qsapps.sh CHIPSET=msm8929 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
sh build.sh CHIPSET=msm8936 tz hyp USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
sh build_qsapps.sh CHIPSET=msm8936 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
sh build.sh CHIPSET=msm8916 tz hyp USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
sh build_qsapps.sh CHIPSET=msm8916 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
sh build.sh CHIPSET=msm8929 tz hyp -c --implicit-deps-unchanged --cleanpack
sh build_qsapps.sh CHIPSET=msm8929 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto -c --implicit-deps-unchanged --cleanpack
sh build.sh CHIPSET=msm8936 tz hyp -c --implicit-deps-unchanged --cleanpack
sh build_qsapps.sh CHIPSET=msm8936 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto -c --implicit-deps-unchanged --cleanpack
sh build.sh CHIPSET=msm8916 tz hyp -c --implicit-deps-unchanged --cleanpack
sh build_qsapps.sh CHIPSET=msm8916 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto -c --implicit-deps-unchanged --cleanpack
sh build.sh CHIPSET=msm8929 tz hyp
sh build_qsapps.sh CHIPSET=msm8929 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto
sh build.sh CHIPSET=msm8936 tz hyp
sh build_qsapps.sh CHIPSET=msm8936 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto
sh build.sh CHIPSET=msm8916 tz hyp
sh build_qsapps.sh CHIPSET=msm8916 commonlib isdbtmm keymaster playready sampleapp securemm widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto
