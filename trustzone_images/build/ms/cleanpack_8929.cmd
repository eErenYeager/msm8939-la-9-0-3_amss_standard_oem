@echo off
rem First build to ensure no error
call build.cmd CHIPSET=msm8929 tz hyp
call build_qsapps.cmd CHIPSET=msm8929 commonlib isdbtmm keymaster playready sampleapp widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto USES_FLAGS=USES_PLAYREADY_PACK
rem IF  %ERRORLEVEL% GTR 0 GOTO ERR
rem Clean build except binary libraries
call build.cmd CHIPSET=msm8929 tz hyp USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
call build_qsapps.cmd CHIPSET=msm8929 atlm commonlib isdbtmm keymaster playready sampleapp widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged
rem Remove the files
call build.cmd CHIPSET=msm8929 tz hyp -c --implicit-deps-unchanged --cleanpack
call build_qsapps.cmd CHIPSET=msm8929 commonlib isdbtmm keymaster playready sampleapp widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto -c --implicit-deps-unchanged --cleanpack
rem Recompile
call build.cmd CHIPSET=msm8929 tz hyp
call build_qsapps.cmd CHIPSET=msm8929 commonlib isdbtmm keymaster playready sampleapp widevine secure_ui_sample tbase_32 securepin pkcs11 secureindicator chamomile fidocrypto
goto END
rem Copy the build

:ERR
echo ---- ERR: Error in Pack build. Compilation terminated --

:END
echo ---- Complete Local Clean pack. Moving to HY11 ---
