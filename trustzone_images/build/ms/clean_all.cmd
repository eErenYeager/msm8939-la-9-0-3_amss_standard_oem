@echo off
rem ==========================================================================
rem
rem  CBSP Buils system
rem
rem  General Description
rem     build batch file.
rem
rem Copyright (c) 2012 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem
rem --------------------------------------------------------------------------
rem
rem $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/build/ms/clean_all.cmd#1 $
rem $DateTime: 2018/02/07 00:37:16 $
rem $Author: mplp4svc $
rem $Change: 15409075 $
rem                      EDIT HISTORY FOR FILE
rem
rem  This section contains comments describing changes made to the module.
rem  Notice that changes are listed in reverse chronological order.
rem
rem when       who     what, where, why
rem --------   ---     ---------------------------------------------------------
rem 10/17/13   ib      Add support for securemm for apq8084
rem ==========================================================================

setlocal

rem ===== Setup Paths=====================================================
SET BUILD_ROOT=%~dp0..\..

IF "%1"=="CHIPSET" (
  IF "%2" == "mpq8092" (
    goto CLEAN_8092_tz
  )
  IF "%2" == "apq8084" (
    goto CLEAN_8084_tz
  )
  IF "%2" == "msm8962" (
    goto CLEAN_8962_tz
  )
  IF "%2" == "mdm9x35" (
    goto CLEAN_9x35_tz
  )
  IF "%2" == "msm8916" (
    goto CLEAN_8916_tz
  )
  IF "%2" == "msm8994" (
    goto CLEAN_8994_tz
  )
  echo Unknown chipset...
  EXIT /B
)
IF "%1" == "" (
  goto CLEAN_all
)

echo Please specify a valid chipset e.g. CHIPSET=msm8974
EXIT /B

:CLEAN_8092_tz
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=mpq8092 tz -c
GOTO CLEAN_8092_SUCCESS

:CLEAN_8084_tz
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=apq8084 tz -c
GOTO CLEAN_8084_SUCCESS

:CLEAN_8962_tz
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=msm8962 tz -c
GOTO CLEAN_8962_SUCCESS

:CLEAN_9x35_tz
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=mdm9x35 tz -c
GOTO CLEAN_9x35_SUCCESS

:CLEAN_8916_tz
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=msm8916 tz -c
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=msm8916 securemm -c
GOTO CLEAN_8916_SUCCESS

:CLEAN_8994_tz
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=msm8994 tz -c
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=msm8994 tqs -c
GOTO CLEAN_8994_SUCCESS

:CLEAN_8092_SUCCESS
echo ==============================================================================
echo CLEAN_ALL.cmd summary for 8092
echo ==============================================================================
echo CLEAN SUCCESS - The following images were removed:
echo LABAANAA\tz.mbn
echo. 2>CleanAllSuccess_8092.txt
GOTO DONE

:CLEAN_8084_SUCCESS
echo ==============================================================================
echo CLEAN_ALL.cmd summary for 8084
echo ==============================================================================
echo CLEAN SUCCESS - The following images were removed:
echo GAAAANAA\tz.mbn
echo. 2>CleanAllSuccess_8084.txt
GOTO DONE

:CLEAN_8962_SUCCESS
echo ==============================================================================
echo CLEAN_ALL.cmd summary for 8962
echo ==============================================================================
echo CLEAN SUCCESS - The following images were removed:
echo FASAANBA\tz.mbn
echo. 2>CleanAllSuccess_8962.txt
GOTO DONE

:CLEAN_9x35_SUCCESS
echo ==============================================================================
echo CLEAN_ALL.cmd summary for 9x35
echo ==============================================================================
echo CLEAN SUCCESS - The following images were removed:
echo JAFAANBA\tz.mbn
echo. 2>CleanAllSuccess_9x35.txt
GOTO DONE

:CLEAN_8916_SUCCESS
echo ==============================================================================
echo CLEAN_ALL.cmd summary for 8916
echo ==============================================================================
echo CLEAN SUCCESS - The following images were removed:
echo MABAANAA\tz.mbn
echo MABAANAA\securemm.mbn
echo. 2>CleanAllSuccess_8916.txt
GOTO DONE

:CLEAN_8994_SUCCESS
echo ==============================================================================
echo CLEAN_ALL.cmd summary for 8994
echo ==============================================================================
echo CLEAN SUCCESS - The following images were removed:
echo GASAANBA\tz.mbn
echo GASAANBA\tqs.mbn
echo. 2>CleanAllSuccess_8994.txt
GOTO DONE

:CLEAN_all
CALL %BUILD_ROOT%\build\ms\clean_all.cmd CHIPSET=mpq8092
CALL %BUILD_ROOT%\build\ms\clean_all.cmd CHIPSET=apq8084
CALL %BUILD_ROOT%\build\ms\clean_all.cmd CHIPSET=msm8962
CALL %BUILD_ROOT%\build\ms\clean_all.cmd CHIPSET=mdm9x35
CALL %BUILD_ROOT%\build\ms\clean_all.cmd CHIPSET=msm8916
CALL %BUILD_ROOT%\build\ms\clean_all.cmd CHIPSET=msm8994
GOTO DONE_all

:DONE
echo ***CLEANING COMPLETE***
endlocal
EXIT /B

:DONE_all
echo ***CLEANING COMPLETE FOR ALL TARGETS***
echo. 2>CleanAllTargetsSuccess.txt
endlocal
EXIT /B
