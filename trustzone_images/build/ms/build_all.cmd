@echo off
rem ==========================================================================
rem
rem  CBSP Builds system
rem
rem  General Description
rem     build batch file.
rem
rem Copyright (c) 2009-2009 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem
rem --------------------------------------------------------------------------
rem
rem $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/build/ms/build_all.cmd#1 $
rem $DateTime: 2018/02/07 00:37:16 $
rem $Author: mplp4svc $
rem $Change: 15409075 $
rem                      EDIT HISTORY FOR FILE
rem
rem  This section contains comments describing changes made to the module.
rem  Notice that changes are listed in reverse chronological order.
rem
rem when       who     what, where, why
rem --------   ---     ---------------------------------------------------------
rem 10/17/13   ib      Added securemm for apq8084
rem ==========================================================================

setlocal

rem ===== Setup Paths=====================================================
SET BUILD_ROOT=%~dp0..\..

IF "%1"=="CHIPSET" (
  IF "%2" == "msm8936" (
    del %BUILD_ROOT%\build\ms\bin\MAXAANAA\tz.mbn
    del %BUILD_ROOT%\build\ms\bin\MAXAANAA\appsbl.mbn
    del %BUILD_ROOT%\build\ms\bin\MAXAANAA\dummy_venus.mbn
    del %BUILD_ROOT%\build\ms\bin\MAXAANAA\qsee_with_test.mbn
    del %BUILD_ROOT%\build\ms\bin\MAXAANAA\tzbsptest.mbn
    del %BUILD_ROOT%\build\ms\bin\MAXAANAA\tztestexec.mbn
    del %BUILD_ROOT%\build\ms\bin\MAXAANAA\tztimage.img
    goto BUILD_8936_tz
  )
  echo Unknown chipset...
  EXIT /B
)
IF "%1" == "" (
  del %BUILD_ROOT%\build\ms\bin\FARAANBA\tz.mbn
  del %BUILD_ROOT%\build\ms\bin\FARAANBA\tzbsp_no_xpu.mbn
  del %BUILD_ROOT%\build\ms\bin\DATAANAA\tz.mbn
  del %BUILD_ROOT%\build\ms\bin\DATAANAA\tzbsp_no_xpu.mbn
  del %BUILD_ROOT%\build\ms\bin\LABAANAA\tz.mbn
  del %BUILD_ROOT%\build\ms\bin\GASAANBA\tz.mbn
  goto BUILD_all
)

IF "%1" == "-c" (
  CALL %BUILD_ROOT%\build\ms\clean_all.cmd
  endlocal
  EXIT /B
)

echo Please specify a valid chipset e.g. CHIPSET=msm8936
EXIT /B

:BUILD_8936_tz
CALL %BUILD_ROOT%\build\ms\build.cmd CHIPSET=msm8936 tz tz_with_test tzbsptest
IF NOT EXIST %BUILD_ROOT%\build\ms\bin\MAXAANAA\tz.mbn GOTO BUILD_8936_TZ_FAILED
IF NOT EXIST %BUILD_ROOT%\build\ms\bin\MAXAANAA\tz_with_test.mbn GOTO BUILD_8936_TZ_WITH_TEST_FAILED
IF NOT EXIST %BUILD_ROOT%\build\ms\bin\MAXAANAA\tzbsptest.mbn GOTO BUILD_8936_TZBSPTEST
GOTO BUILD_8936_SUCCESS

:BUILD_8936_TZ_FAILED
echo **Build errors...
echo tz.mbn file not found for msm8936. Please refer to build log for build errors.
EXIT /B

:BUILD_8936_TZ_WITH_TEST_FAILED
echo **Build errors...
echo tz_with_test.mbn file not found for msm8936. Please refer to build log for build errors.
EXIT /B

:BUILD_8936_TZBSPTEST_FAILED
echo **Build errors...
echo tzbsptest.mbn file not found for msm8936. Please refer to build log for build errors.
EXIT /B

:BUILD_8916_SUCCESS
echo ==============================================================================
echo BUILD_ALL.cmd build summary for 8916
echo ==============================================================================
echo BUILD SUCCESS - The following images were generated:
echo MABAANAA\tz.mbn
echo. 2>BuildAllSuccess_8916.txt
GOTO DONE

:BUILD_8936_SUCCESS
echo ==============================================================================
echo BUILD_ALL.cmd build summary for 8936
echo ==============================================================================
echo BUILD SUCCESS - The following images were generated:
echo MAXAANAA\tz.mbn
echo MAXAANAA\tz_with_test.mbn
echo MAXAANAA\tzbsptest.mbn
echo MAXAANAA\tztimage.img
echo. 2>BuildAllSuccess_8936.txt
GOTO DONE

:BUILD_all
CALL %BUILD_ROOT%\build\ms\build_all.cmd CHIPSET=msm8916
IF NOT EXIST %BUILD_ROOT%\build\ms\bin\MABAANAA\tz.mbn GOTO BUILD_all_error
CALL %BUILD_ROOT%\build\ms\build_all.cmd CHIPSET=msm8936
IF NOT EXIST %BUILD_ROOT%\build\ms\bin\GASAANBA\tz.mbn GOTO BUILD_all_error
GOTO DONE_all

:DONE
echo ***BUILDING COMPLETE***
endlocal
EXIT /B

:DONE_all
echo ***BUILDING COMPLETE FOR ALL TARGETS***
echo. 2>BuildAllTargetsSuccess.txt
endlocal
EXIT /B

:BUILD_all_error
echo **Build errors...
echo Please refer to build log for build errors and check the image bin folder(s).
endlocal
EXIT /B
