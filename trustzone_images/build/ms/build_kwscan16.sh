#!/bin/bash

cd `dirname $0`
sh build.sh CHIPSET=msm8916 tz hyp
sh build.sh CHIPSET=msm8916 tzbsp_no_xpu tz_with_test tztestexec dummy_venus tzbsptest -j1
sh build_qsapps.sh CHIPSET=msm8916 secureindicator pkcs11 securepin secure_ui_sample apttestapp isdbtmm assurancetest commonlib sampleapp qmpsecapp keymaster dxhdcp2 dxhdcp2dbg playready widevine USES_FLAGS=USES_NO_CP -j1