#===============================================================================
#
# CBSP Buils system
#
# General Description
#    build shell script file.
#
# Copyright (c) 2009-2012 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
# $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/build/ms/build.sh#1 $
# $DateTime: 2018/02/07 00:37:16 $
# $Author: mplp4svc $
# $Change: 15409075 $
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to the module.
# Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     -----------------------------------------------------------
#
#===============================================================================
#===============================================================================
# Set up default paths
# Need to get working on Linux
#===============================================================================


build_dir=`dirname $0`
cd $build_dir
#export OKL4_SDK=../../core/kernel/sdk
export BUILD_ROOT=../..
export CORE_BSP_ROOT=$BUILD_ROOT/core/bsp
export TOOLS_SCONS_ROOT=$BUILD_ROOT/tools/build/scons

if [ -e setenv.sh ]; then
source setenv.sh
fi


#===============================================================================
# Set target enviroment
#===============================================================================

chipset="$1"

if [ "$chipset" = "CHIPSET=mpq8092" ]; then
  export BUILD_ID=LABAANAA
  export BUILD_VER=40000000
  export BUILDSPEC=KLOCWORK

  export BUILD_ASIC=8092
  export MSM_ID=8092
  export HAL_PLATFORM=8092
  export TARGET_FAMILY=8092
  export CHIPSET=mpq8092
  export IMAGES=""
elif [ "$chipset" = "CHIPSET=apq8084" ]; then
  export BUILD_ID=GAAAANAA
  export BUILD_VER=40000000
  export BUILDSPEC=KLOCWORK

  export BUILD_ASIC=8084
  export MSM_ID=8084
  export HAL_PLATFORM=8084
  export TARGET_FAMILY=8084
  export CHIPSET=apq8084
  export IMAGES=""
elif [ "$chipset" = "CHIPSET=msm8962" ]; then
  export BUILD_ID=FASAANBA
  export BUILD_VER=40000000
  export BUILDSPEC=KLOCWORK

  export BUILD_ASIC=8962
  export MSM_ID=8962
  export HAL_PLATFORM=8962
  export TARGET_FAMILY=8962
  export CHIPSET=msm8962
  export IMAGES=""
elif [ "$chipset" = "CHIPSET=mdm9x35" ]; then
  export BUILD_ID=JAFAANBA
  export BUILD_VER=40000000
  export BUILDSPEC=KLOCWORK

  export BUILD_ASIC=9x35
  export MSM_ID=9x35
  export HAL_PLATFORM=9x35
  export TARGET_FAMILY=9x35
  export CHIPSET=mdm9x35
  export IMAGES=""
elif [ "$chipset" = "CHIPSET=msm8916" ]; then
  export BUILD_ID=MAVAANAA
  export BUILD_VER=40000000
  export BUILDSPEC=KLOCWORK

  export BUILD_ASIC=8916
  export MSM_ID=8916
  export HAL_PLATFORM=8916
  export TARGET_FAMILY=8916
  export CHIPSET=msm8916
  export IMAGES=""
elif [ "$chipset" = "CHIPSET=msm8936" ]; then
  export BUILD_ID=MAXAANAA
  export BUILD_VER=40000000
  export BUILDSPEC=KLOCWORK

  export BUILD_ASIC=8936
  export MSM_ID=8936
  export HAL_PLATFORM=8936
  export TARGET_FAMILY=8936
  export CHIPSET=msm8936
  export IMAGES=""
  elif [ "$chipset" = "CHIPSET=msm8929" ]; then
  export BUILD_ID=MAFAANAA
  export BUILD_VER=40000000
  export BUILDSPEC=KLOCWORK

  export BUILD_ASIC=8929
  export MSM_ID=8929
  export HAL_PLATFORM=8929
  export TARGET_FAMILY=8929
  export CHIPSET=msm8929
  export IMAGES=""
elif [ "$chipset" = "CHIPSET=msm8994" ]; then
  export BUILD_ID=EACAANAA
  export BUILD_VER=40000000
  export BUILDSPEC=KLOCWORK

  export BUILD_ASIC=8994
  export MSM_ID=8994
  export HAL_PLATFORM=8994
  export TARGET_FAMILY=8994
  export CHIPSET=msm8994
  export IMAGES=""
elif [ "$chipset" = "CHIPSET=msm8992" ]; then
  export BUILD_ID=BAWAANAA
  export BUILD_VER=40000000
  export BUILDSPEC=KLOCWORK

  export BUILD_ASIC=8994
  export MSM_ID=8994
  export HAL_PLATFORM=8994
  export TARGET_FAMILY=8994
  export CHIPSET=msm8994
  export IMAGES=""
else
  echo "Please enter a valid chipset"
fi

shift

export BUILD_CMD="BUILD_ID=$BUILD_ID BUILD_VER=$BUILD_VER MSM_ID=$MSM_ID HAL_PLATFORM=$HAL_PLATFORM TARGET_FAMILY=$TARGET_FAMILY BUILD_ASIC=$BUILD_ASIC CHIPSET=$CHIPSET $IMAGES $*"
$TOOLS_SCONS_ROOT/build/rename-log.sh
$TOOLS_SCONS_ROOT/build/build.sh -f target.scons $BUILD_CMD
