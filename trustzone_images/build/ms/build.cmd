@echo off
rem ==========================================================================
rem
rem  CBSP Buils system
rem
rem  General Description
rem     build batch file.
rem
rem Copyright (c) 2009-2009 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem
rem --------------------------------------------------------------------------
rem
rem $Header: //components/rel/tz.bf/3.0.c3.r165.1/trustzone_images/build/ms/build.cmd#1 $
rem $DateTime: 2018/02/07 00:37:16 $
rem $Author: mplp4svc $
rem $Change: 15409075 $
rem                      EDIT HISTORY FOR FILE
rem
rem  This section contains comments describing changes made to the module.
rem  Notice that changes are listed in reverse chronological order.
rem
rem when       who     what, where, why
rem --------   ---     ---------------------------------------------------------
rem
rem ==========================================================================

rem
rem PLEASE note that this script MUST be called with the target and options at
rem then end of command.
rem Syntax: build.cmd BUILD_ID=XYZ CHIPSET=ABC tz --verbose=2
rem We first parse out the chipset build options then everything else must come
rem later.
rem

setlocal

rem ============== Call the set environment batch file ===================
CALL setenv.cmd

rem ===== Setup Paths=====================================================
SET BUILD_ROOT=%~dp0..\..
SET CORE_BSP_ROOT=%BUILD_ROOT%\core\bsp
SET TOOLS_SCONS_ROOT=%BUILD_ROOT%\tools\build\scons
SET CMD_ARGS=%*

setlocal
:LOOP

IF /I "%1" equ "" GOTO CONTINUE

IF /I "%1" equ "BUILD_ID" (
 SET BUILD_ID=%2
 GOTO SHIFT2
)
IF /I "%1" equ "BUILD_VER" (
  SET BUILD_VER=%2
  GOTO SHIFT2
)
IF /I "%1" equ "MSM_ID" (
  SET MSM_ID=%2
  GOTO SHIFT2
)
IF /I "%1" equ "HAL_PLATFORM" (
  SET  HAL_PLATFORM=%2
  GOTO SHIFT2
)
IF /I "%1" equ "TARGET_FAMILY" (
  SET TARGET_FAMILY=%2
  GOTO SHIFT2
)
IF /I "%1" equ "BUILD_ASIC" (
  SET BUILD_ASIC=%2
  GOTO SHIFT2
)
IF /I "%1" equ "CHIPSET" (
  SET CHIPSET=%2
  GOTO SHIFT2
)

SHIFT
GOTO LOOP

:SHIFT2
SHIFT
SHIFT
GOTO LOOP

:CONTINUE
IF /I "%MSM_ID%" equ "8916" (
  set _result=%CMD_ARGS:*8916=%
  goto SET_8916
)
IF /I "%CHIPSET%" equ "msm8916" (
  set _result=%CMD_ARGS:*msm8916=%
  goto SET_8916
)
IF /I "%HAL_PLATFORM%" equ "8916" (
  set _result=%CMD_ARGS:*8916=%
  goto SET_8916
)
IF /I "%TARGET_FAMILY%" equ "8916" (
  set _result=%CMD_ARGS:*8916=%
  goto SET_8916
)
IF /I "%MSM_ID%" equ "8936" (
  set _result=%CMD_ARGS:*8936=%
  goto SET_8936
)
IF /I "%CHIPSET%" equ "msm8936" (
  set _result=%CMD_ARGS:*msm8936=%
  goto SET_8936
)
IF /I "%HAL_PLATFORM%" equ "8936" (
  set _result=%CMD_ARGS:*8936=%
  goto SET_8936
)
IF /I "%TARGET_FAMILY%" equ "8936" (
  set _result=%CMD_ARGS:*8936=%
  goto SET_8936
)
IF /I "%MSM_ID%" equ "8929" (
  set _result=%CMD_ARGS:*8929=%
  goto SET_8929
)
IF /I "%CHIPSET%" equ "msm8929" (
  set _result=%CMD_ARGS:*msm8929=%
  goto SET_8929
)
IF /I "%HAL_PLATFORM%" equ "8929" (
  set _result=%CMD_ARGS:*8929=%
  goto SET_8929
)
IF /I "%TARGET_FAMILY%" equ "8929" (
  set _result=%CMD_ARGS:*8929=%
  goto SET_8929
)

echo "Please Specify the CHIPSET to build"
EXIT /B

:SET_8916
rem Below BUILD_ID is temporary until we have full details on 8916 mem config
SET BUILD_ID=MAVAANAA
SET BUILD_VER=40000000
SET BUILD_ASIC=8916A
SET MSM_ID=8916
SET HAL_PLATFORM=8916
SET TARGET_FAMILY=8916
SET CHIPSET=msm8916
SET _tempresult=%_result:hdcpapp=%

IF NOT "%_result%" == "%_tempresult%" (
  SET _tempresult=%_result:-c=%
  IF NOT "%_result%" == "%_tempresult%" (
    DEL %BUILD_ROOT%\build\ms\bin\PIL_IMAGES\SPLITBINS_%BUILD_ID%\dxhdcp2*"
  ) ELSE (
    MKDIR %BUILD_ROOT%\build\ms\bin\PIL_IMAGES\SPLITBINS_%BUILD_ID%
    COPY "%CORE_BSP_ROOT%\trustzone\qsapps\hdcpapp\build\*.*" "%BUILD_ROOT%\build\ms\bin\PIL_IMAGES\SPLITBINS_%BUILD_ID%\*.*"
  )
)

rem temporarily added for compiling 9x35 tz without abt
rem remove after abt is checked in
rem SET _result=%_result% tzbsp_no_abt=1

SET _result=%_result:hdcpapp=%
goto DO_BUILD




:SET_8936
rem Below BUILD_ID is temporary until we have full details on 8936 mem config
SET BUILD_ID=MAXAANAA
SET BUILD_VER=40000000
SET BUILD_ASIC=8936A
SET MSM_ID=8936
SET HAL_PLATFORM=8936
SET TARGET_FAMILY=8936
SET CHIPSET=msm8936
SET _tempresult=%_result:hdcpapp=%

IF NOT "%_result%" == "%_tempresult%" (
  SET _tempresult=%_result:-c=%
  IF NOT "%_result%" == "%_tempresult%" (
    DEL %BUILD_ROOT%\build\ms\bin\PIL_IMAGES\SPLITBINS_%BUILD_ID%\dxhdcp2*"
  ) ELSE (
    MKDIR %BUILD_ROOT%\build\ms\bin\PIL_IMAGES\SPLITBINS_%BUILD_ID%
    COPY "%CORE_BSP_ROOT%\trustzone\qsapps\hdcpapp\build\*.*" "%BUILD_ROOT%\build\ms\bin\PIL_IMAGES\SPLITBINS_%BUILD_ID%\*.*"
  )
)

SET _result=%_result:hdcpapp=%
goto DO_BUILD



:SET_8929
rem Below BUILD_ID is temporary until we have full details on 8929 mem config
SET BUILD_ID=MAFAANAA
SET BUILD_VER=40000000
SET BUILD_ASIC=8929A
SET MSM_ID=8929
SET HAL_PLATFORM=8929
SET TARGET_FAMILY=8929
SET CHIPSET=msm8929
SET _tempresult=%_result:hdcpapp=%

IF NOT "%_result%" == "%_tempresult%" (
  SET _tempresult=%_result:-c=%
  IF NOT "%_result%" == "%_tempresult%" (
    DEL %BUILD_ROOT%\build\ms\bin\PIL_IMAGES\SPLITBINS_%BUILD_ID%\dxhdcp2*"
  ) ELSE (
    MKDIR %BUILD_ROOT%\build\ms\bin\PIL_IMAGES\SPLITBINS_%BUILD_ID%
    COPY "%CORE_BSP_ROOT%\trustzone\qsapps\hdcpapp\build\*.*" "%BUILD_ROOT%\build\ms\bin\PIL_IMAGES\SPLITBINS_%BUILD_ID%\*.*"
  )
)

SET _result=%_result:hdcpapp=%
goto DO_BUILD


:DO_END
EXIT /B

SET IMAGES=%1
REM SET BUILDSPEC=KLOCWORK

:DO_BUILD
SET BUILD_CMD=BUILD_ID=%BUILD_ID% BUILD_VER=%BUILD_VER% MSM_ID=%MSM_ID% HAL_PLATFORM=%HAL_PLATFORM% TARGET_FAMILY=%TARGET_FAMILY% BUILD_ASIC=%BUILD_ASIC% CHIPSET=%CHIPSET% %_result%

CALL %TOOLS_SCONS_ROOT%\build\rename-log.cmd

@echo CALL %TOOLS_SCONS_ROOT%\build\build.cmd -f target.scons %BUILD_CMD%

CALL %TOOLS_SCONS_ROOT%\build\build.cmd -f target.scons %BUILD_CMD%

