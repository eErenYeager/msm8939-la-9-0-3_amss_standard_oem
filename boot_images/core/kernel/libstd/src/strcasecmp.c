/*===============================================================================================
 * FILE:        strcasecmp.c
 *
 * DESCRIPTION: Implementation of standard string API strcasecmp.
 *
 *              Copyright (c) 1999 - 2010 Qualcomm Technologies Incorporated.
 *              All Rights Reserved. QUALCOMM Proprietary and Confidential.
 *===============================================================================================*/
 
/*===============================================================================================
 *
 *                            Edit History
 * $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/kernel/libstd/src/strcasecmp.c#1 $ 
 * $DateTime: 2015/03/19 01:58:37 $
 *
 *===============================================================================================*/
#include <limits.h> 
#include <stringl/stringl.h>

int strcasecmp(const char * s1, const char * s2)
{
    return strncasecmp(s1, s2, UINT_MAX);
}
