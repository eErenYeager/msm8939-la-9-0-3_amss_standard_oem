/*===============================================================================================
 * FILE:        libstd_std_scanul.c
 *
 * DESCRIPTION: Implementation of a secure API memsmove - Size bounded memory move.
 *
 *              Copyright (c) 2012 Qualcomm Technologies Incorporated.
 *              All Rights Reserved. QUALCOMM Proprietary and Confidential.
 *===============================================================================================*/
 
/*===============================================================================================
 *
 *                            Edit History
 * $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/kernel/libstd/src/memsmove.c#1 $ 
 * $DateTime: 2015/03/19 01:58:37 $
 *
 *===============================================================================================*/
 
#include <stringl/stringl.h>

size_t memsmove(
          void        *dst,
          size_t      dst_size,
          const void  *src,
          size_t      src_size
          )
{
  size_t  copy_size = (dst_size <= src_size)? dst_size : src_size;

  memmove(dst, src, copy_size);

  return copy_size;
}

