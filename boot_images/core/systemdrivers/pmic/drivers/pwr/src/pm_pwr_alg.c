/*! \file pm_pwr_alg.c 
*  \n
*  \brief  
*  \n  
*  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/pwr/src/pm_pwr_alg.c#1 $

when        who     what, where, why
--------    ---     ----------------------------------------------------------
10/08/13   rk      VOLTAGE_CTL1 reg supported only for CLK LDOs
12/06/12    hw      Rearchitecturing module driver to peripheral driver
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pwr_alg.h"

/*===========================================================================

                     FUNCTION IMPLEMENTATION 

===========================================================================*/

pm_err_flag_type pm_pwr_pull_down_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, pm_on_off_type on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    
    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (on_off >= PM_INVALID)
    {
        err_flag = PM_ERR_FLAG__MACRO_NOT_RECOGNIZED;
    }
    else
    {
        pm_register_address_type          reg = 0xFF;
        pm_register_data_type             data = 0xFF;
    
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->PD_CTL);
    
        data = (PM_OFF == on_off) ? 0 : 0x80;

        err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x80, data, 0);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_sw_mode_status_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, pm_sw_mode_type *sw_mode)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
                                                          
    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (sw_mode == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0xFF;
            
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->MODE_CTL);

        err_flag = comm_ptr->ReadByte(comm_ptr, reg, &data, 0);
        
        if (err_flag == PM_ERR_FLAG__SUCCESS)
        {
            /* BYPASS supercedes NPM & PFM */
            /* Only valid for LDO's */
            if (data & 0x20)
            {
                *sw_mode = PM_SW_MODE_BYPASS;
            }
            /* NPM supercedes AUTO & PFM */
            else if (data & 0x80)
            {
                *sw_mode = PM_SW_MODE_NPM;
            }
            /* AUTO supercedes PFM */
            /* only valid for SMPS */
            else if (data & 0x40)
            {
                *sw_mode = PM_SW_MODE_AUTO;
            }
            else /* IT MIGHT BE PFM DEPENDING ON pc_mode bits */
            {
                *sw_mode = PM_SW_MODE_LPM;
            }
        }
        else
        {
            *sw_mode = PM_SW_MODE_INVALID;
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_pin_ctrl_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, uint8 select_pin)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index < pwr_data->num_of_peripherals)
    {
        pm_register_address_type           reg = 0x0;

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->EN_CTL);

        err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x0F, (pm_register_data_type)select_pin, 1); // bit<0:3>
    }
    else
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_sw_enable_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, pm_on_off_type on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (on_off >= PM_INVALID)
    {
        err_flag = PM_ERR_FLAG__MACRO_NOT_RECOGNIZED;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
            
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->EN_CTL);

        if (PM_OFF == on_off)
        {
            err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x80, 0, 0);
        }
        else
        {
            err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x80, 0x80, 0);
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_volt_level_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, pm_volt_level_type  volt_level)
{
    pm_err_flag_type                  err_flag = PM_ERR_FLAG__SUCCESS;
    uint32                            vmin = 0;  // microvolt
    uint32                            vStep = 0; // microvolt
    uint32                            range = 0; // 0 to 4
    pm_register_data_type             vset = 0;
    pm_register_data_type             data = 0;
    pm_register_data_type             curr_range = 0;
    pm_register_address_type          status_reg = 0;
    pm_register_address_type          reg = 0;
    uint8                             mask = 0xFF;
    pm_device_info_type               pmic_device_info;

    err_flag = pm_get_pmic_info(0, &pmic_device_info);
    if (PM_ERR_FLAG__SUCCESS == err_flag)
    {
        /* In PM8916 version 1.0  some of the regulators (L4 to L18 and S4) are not regulating properly. So avoiding to program the VSET for those LDOs. Instead it is configured in PBS */
        if ((PMIC_IS_PM8916 == pm_get_pmic_model(0)) && (1 == pmic_device_info.nPmicAllLayerRevision) && (0 == pmic_device_info.nPmicMetalRevision) && (peripheral_index >= 3) && (peripheral_index <= 17))
        {
            err_flag = PM_ERR_FLAG__SUCCESS ;
            return err_flag;
        }
    }
    else
    {
        return err_flag;
    }


    if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED ;
    }
    else
    {
        // Set Range
        reg = pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->VOLTAGE_CTRL1;

        //Read the current range
        err_flag = comm_ptr->ReadByte(comm_ptr, reg, &curr_range, 0);
        curr_range = pwr_data->pwr_specific_info[peripheral_index].volt_range;

        if(PM_ERR_FLAG__SUCCESS != err_flag)
        {
            return err_flag;
        }

        if( (volt_level <= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[curr_range].RangeMax) &&
            (volt_level >= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[curr_range].RangeMin) )
        {
            vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[curr_range].RangeMin;
            vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[curr_range].VStep;
            range = curr_range;
        }
        else
        {
        for(range = pwr_data->pwr_specific_info[peripheral_index].pwr_range->start; range <= pwr_data->pwr_specific_info[peripheral_index].pwr_range->end; range++)
        {
            if( (volt_level <= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMax) &&
                (volt_level >= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin) )
            {
                vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin;
                vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].VStep;
                break;
            }
        }
        }
       
        if (vStep > 0)
        {
            //calculate VSET
            vset = (pm_register_data_type)((volt_level - vmin)/vStep);

            // Set the range first, only if the current range is not equal to requested range
            if(curr_range != ((pm_register_data_type)range))
            {
                status_reg = pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->STATUS;

                //Read the Vreg OK status
                err_flag = comm_ptr->ReadByte(comm_ptr, status_reg, &data, 0);

                if(PM_ERR_FLAG__SUCCESS != err_flag)
                {
                    return err_flag;
                }

                //New range cannot be set if Power rail is ON
                if(data & 0x80)
                {
                    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
                }

                //Set the new range
                err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x07, (pm_register_data_type)range , 0) ; // 0:2

                if(PM_ERR_FLAG__SUCCESS != err_flag)
                {
                    return err_flag;
                }
                curr_range = range;
            }

             // Set VSET
             reg =  pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->VOLTAGE_CTRL2;
             if ((1 == curr_range) && (PM_HW_MODULE_ULT_BUCK == pwr_data->pwr_specific_info[peripheral_index].periph_type))
             {
                 mask = 0x1F; // since bit<5> and bit<6> implies range which shouldn�t be changed, bit <7> is unused.
             }
             // Set the vset then
             err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, mask, vset , 0) ; // 0:7
        }
        else
        {
            err_flag = PM_ERR_FLAG__VLEVEL_OUT_OF_RANGE;
        }
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_volt_level_status_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8  peripheral_index, pm_volt_level_type *volt_level)
{
    pm_err_flag_type                 err_flag = PM_ERR_FLAG__SUCCESS;
    uint32                           vmin = 0; // microvolt
    uint32                           vStep = 0; //microvolt
    uint32                           range = 0; // 0 to 4
    pm_register_data_type            vset = 0;
    pm_register_address_type         reg = 0;
    pm_register_data_type            reg_data[2];

    if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (volt_level == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        // get the voltage level LUT
        reg =  (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->VOLTAGE_CTRL2);

        err_flag = comm_ptr->ReadByteArray(comm_ptr, reg, 1, reg_data, 0);

        if ( err_flag == PM_ERR_FLAG__SUCCESS )
        {
            range = pwr_data->pwr_specific_info[peripheral_index].volt_range;
            vset = reg_data[0];
            if ((1 == range) && (PM_HW_MODULE_ULT_BUCK == pwr_data->pwr_specific_info[peripheral_index].periph_type))
            {
                vset &= 0x1F;
            }
            vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin; // microvolt
            vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].VStep; //microvolt

            *volt_level = vmin + vset * vStep;
        }
    }

    return err_flag;
}
