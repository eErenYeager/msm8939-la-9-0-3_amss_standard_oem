/*! \file pm_lbc_driver.c
 *  \n
 *  \brief  
 *  \details  
 *  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/lbc/src/pm_lbc_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/17/13   aab     Creation
========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_lbc_driver.h"
#include "CoreVerify.h"


/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the MPP data */
static pm_lbc_data_type pm_lbc_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/


void pm_lbc_driver_init( pmiC_IComm *comm_ptr )
{
    pm_lbc_data_type* lbc_ptr = NULL;
	uint32 prop_id_arr[] = {PM_PROP_LBCA_NUM, PM_PROP_LBCB_NUM};

	DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);

	lbc_ptr = &pm_lbc_data_arr[pmic_index];
	
	if (lbc_ptr->periph_exists == FALSE)
    {
        lbc_ptr->periph_exists = TRUE;
                                                    
        /* Assign Comm ptr */
        lbc_ptr->comm_ptr = comm_ptr;
		
		/* LBC Register Info - Obtaining Data through dal config */
        lbc_ptr->lbc_reg_table = (pm_lbc_register_info_type*)pm_target_information_get_common_info(PM_PROP_LBC_REG);
        CORE_VERIFY_PTR(lbc_ptr->lbc_reg_table);
		
        /* LBC Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));
        		
	
        lbc_ptr->num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);
        CORE_VERIFY(lbc_ptr->num_of_peripherals  != 0);
		
		//lbc_ptr->lbc_specific = (pm_lbc_specific_info_type*)pm_target_information_get_specific_info(PM_PROP_LBC_SPECIFIC_DATA);
		//CORE_VERIFY_PTR(lbc_ptr->lbc_specific);
		
    }
}

pm_lbc_data_type* pm_lbc_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_lbc_data_arr[pmic_index];
    }

    return NULL;
}


