/*! \file
*
*  \brief  pm_smb.c
*  \details Implementation file for SMB resourece type.
*
*  &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

when       who     what, where, why
---------- ---     ----------------------------------------------------------
08/19/2014 pxm     Creation
===========================================================================*/

#include "DALDeviceId.h"
#include "I2cDevice.h"
#include "DalDevice.h"
#include "DALStdErr.h"
#include "DDITlmm.h"

#include "pm_smb.h"

#define I2C_SLAVE_ADDR 0x14
#define I2C_BUS_FREQ   400
#define TRANSFER_TIMEOUT_US 2500

#define BIT(n) (1 << (n))

#define _SMB_MASK(BITS, POS) \
    ((unsigned char)(((1 << (BITS)) - 1) << (POS)))

#define SMB_MASK(LEFT_BIT_POS, RIGHT_BIT_POS) \
        _SMB_MASK((LEFT_BIT_POS) - (RIGHT_BIT_POS) + 1, \
                (RIGHT_BIT_POS))

#define I2C_VERIFY(result) \
    do \
    { \
        if(PM_ERR_FLAG__SUCCESS != (result)) \
            return PM_ERR_FLAG__I2C_OPT_ERR; \
    } while(0)

/* Charger Registers */
#define CFG_BATT_CHG_ICL_REG            0x05
#define AC_INPUT_ICL_PIN_BIT            BIT(7)
#define AC_INPUT_PIN_HIGH_BIT           BIT(6)
#define RESET_STATE_USB_500             BIT(5)
#define INPUT_CURR_LIM_MASK             SMB_MASK(3, 0)
#define INPUT_CURR_LIM_300MA            0x0

#define CFG_CHG_MISC_REG                0x7
#define CHG_EN_BY_PIN_BIT               BIT(7)
#define CHG_EN_ACTIVE_LOW_BIT           BIT(6)
#define PRE_TO_FAST_REQ_CMD_BIT         BIT(5)
#define CHG_CURR_TERM_DIS_BIT           BIT(3)
#define CFG_AUTO_RECHG_DIS_BIT          BIT(2)
#define CFG_CHG_INHIBIT_EN_BIT          BIT(0)

#define CHG_CURRENT_REG                 0x13
#define FASTCHG_CURR_MASK               SMB_MASK(4, 2)
#define FASTCHG_CURR_SHIFT              2

#define CMD_IL_REG                      0x41
#define USB_CTRL_MASK                   SMB_MASK(1 , 0)
#define USB_100_BIT                     0x01
#define USB_500_BIT                     0x00
#define USB_AC_BIT                      0x02
#define SHDN_CMD_BIT                    BIT(7)

#define CMD_CHG_REG                     0x42
#define CMD_CHG_EN                      BIT(1)
#define CMD_OTG_EN_BIT                  BIT(0)

typedef enum 
{
    IL_USB100,
    IL_USB500,
    IL_AC,
} InputLimitMode;

static uint32 device_id = DALDEVICEID_I2C_DEVICE_4; // Will place into config file
static I2cClientConfig config = {I2C_BUS_FREQ, TRANSFER_TIMEOUT_US};
static I2CDEV_HANDLE  handle = NULL;

static pm_err_flag_type smb_write(uint8 offset, uint8  pWrBuff );
static pm_err_flag_type smb_read(uint8 offset, uint8* data );
static pm_err_flag_type smb_write_mask (uint8 offset, uint8  pWrBuff, uint8 mask);

/*
 * Read a byte from SMB with given offset/address.
 * @offset: The internal address of SMB to read a byte.
 * @data: The buffer to receive the reading result.
 *
 * @return PM_ERR_FLAG__I2C_OPT_ERR if I2C error happened
 *         PM_ERR_FLAG__SUCCESS if success
 */
static pm_err_flag_type smb_read(uint8 offset, uint8* data )
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    int32 i2cResult = I2C_RES_SUCCESS;

    // I2C read sequence need 2 transfer operation
    // Start | SMB address(W) | Register address | Start | SMB address(R) | DATA
    I2cBuffDesc rdIoVecs[2]; // 0 for register, 1 for buffer to receive data
    I2cTransfer rdTrans[2]; // 0 for write sequence writing register address
                            // 1 for read sequence to receive data
    I2cSequence rdSeq;
    I2cIoResult ioRes;

    rdIoVecs[0].pBuff             = &offset;
    rdIoVecs[0].uBuffSize         = 1;                  
    rdIoVecs[1].pBuff             = data;
    rdIoVecs[1].uBuffSize         = 1;

    rdTrans[0].pI2cBuffDesc       = &rdIoVecs[0];
    rdTrans[0].uTrSize            = rdIoVecs[0].uBuffSize;
    rdTrans[0].tranCfg.uSlaveAddr = I2C_SLAVE_ADDR; 
    rdTrans[0].eTranDirection     = I2cTranDirOut;
    rdTrans[0].eTranCtxt          = I2cTrCtxSeqStart;

    rdTrans[1].pI2cBuffDesc       = &rdIoVecs[1];
    rdTrans[1].uTrSize            = rdIoVecs[1].uBuffSize;
    rdTrans[1].tranCfg.uSlaveAddr = I2C_SLAVE_ADDR; 
    rdTrans[1].eTranDirection     = I2cTranDirIn;
    rdTrans[1].eTranCtxt          = I2cTrCtxSeqEnd;

    rdSeq.pTransfer      = rdTrans;
    rdSeq.uNumTransfers  = 2;

    i2cResult = I2CDEV_BatchTransfer(handle, &rdSeq, &config, &ioRes);
    if(i2cResult != I2C_RES_SUCCESS)
    {
        err_flag = PM_ERR_FLAG__I2C_OPT_ERR;
    }
    return err_flag;
}

/*
 * Write a byte into SMB in given offset/address.
 * @offset: The internal address of SMB to write the data.
 * @data: The data to be written into SMB.
 *
 * @return PM_ERR_FLAG__I2C_OPT_ERR if I2C error happened
 *         PM_ERR_FLAG__SUCCESS if success
 */
static pm_err_flag_type smb_write(uint8 offset, uint8 data)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    int32 i2cResult = I2C_RES_SUCCESS;

    I2cBuffDesc wrIoVecs[2];
    I2cTransfer wrTrans;
    uint32 uWriteSize;

    wrIoVecs[0].pBuff          = &offset;
    wrIoVecs[0].uBuffSize      = 1; // 1 byte register offset
    wrIoVecs[1].pBuff          = &data;
    wrIoVecs[1].uBuffSize      = 1; // 1 byte data would be written

    wrTrans.pI2cBuffDesc       = wrIoVecs;
    wrTrans.uTrSize            = wrIoVecs[0].uBuffSize + wrIoVecs[1].uBuffSize;
    wrTrans.tranCfg.uSlaveAddr = I2C_SLAVE_ADDR;
    wrTrans.eTranDirection     = I2cTranDirOut;
    wrTrans.eTranCtxt          = I2cTrCtxNotASequence;

    i2cResult = I2CDEV_Write(handle, &wrTrans, &config, &uWriteSize);
    if(i2cResult != I2C_RES_SUCCESS)
    {
        err_flag = PM_ERR_FLAG__I2C_OPT_ERR;
    }

    return err_flag;
}

/*
 * Write a byte into SMB in given offset/address with mask.
 * With mask the unnecessary bits would keep the old value.
 * @offset: The internal address of SMB to write the data.
 * @data: The data to be written into SMB.
 * @mask:
 *
 * @return PM_ERR_FLAG__I2C_OPT_ERR if I2C error happened
 *         PM_ERR_FLAG__SUCCESS if success
 */
static pm_err_flag_type smb_write_mask (uint8 offset, uint8  data, uint8 mask)
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   uint8 rdata;

   err_flag = smb_read(offset, &rdata);
   if(PM_ERR_FLAG__SUCCESS != err_flag)
   {
        return PM_ERR_FLAG__I2C_OPT_ERR;
   }

   rdata = (rdata & (~mask)) | data;

   err_flag = smb_write(offset, rdata);

   return err_flag;
}

static pm_err_flag_type smb_set_input_limit_mode(InputLimitMode mode)
{
    uint8 data = 0;

    switch(mode)
    {
        case IL_AC:
            data = USB_AC_BIT;
            break;
        case IL_USB100:
            data = USB_100_BIT;
            break;
        case IL_USB500:
            data = USB_500_BIT;
            break;
    }

    I2C_VERIFY(smb_write_mask(CMD_IL_REG, data, USB_CTRL_MASK));

    return PM_ERR_FLAG__SUCCESS;
}

static pm_err_flag_type smb_charging_enable(boolean enable)
{
    uint8 data = 0;

    I2C_VERIFY(smb_read(CFG_CHG_MISC_REG, &data));

    if(CHG_EN_ACTIVE_LOW_BIT == (CHG_EN_ACTIVE_LOW_BIT & data))
    {
        data = enable ? 0 : CMD_CHG_EN;
    }
    else
    {
        data = enable ? CMD_CHG_EN : 0;
    }

    I2C_VERIFY(smb_write_mask(CMD_CHG_REG, data, CMD_CHG_EN));

    // Configure registers to enable the charging at first, otherwise charging will be disabled
    // momentarily
    I2C_VERIFY(smb_write_mask(CFG_CHG_MISC_REG, 0, CHG_EN_BY_PIN_BIT));

    return PM_ERR_FLAG__SUCCESS;
}

pm_err_flag_type pm_smb_enable_charging(boolean enable)
{
    uint32 result = 0;

    result = I2CDEV_Init(device_id, &handle);
    if(I2C_RES_SUCCESS != result)
    {
        return PM_ERR_FLAG__I2C_OPT_ERR;
    }

    // Bus on, clock on
    result = I2CDEV_SetPowerState(handle, I2CDEV_POWER_STATE_2);
    if(I2C_RES_SUCCESS != result)
    {
        return PM_ERR_FLAG__I2C_OPT_ERR;
    }

    I2C_VERIFY(smb_charging_enable(enable));

    // AC input current limit controlled by register (bit7=0)
    // For register 41h bit[1:0], 00b is USB500 mode, 01b is USB100 mode (bit5=1)
    I2C_VERIFY(smb_write_mask(CFG_BATT_CHG_ICL_REG,
        RESET_STATE_USB_500,
        AC_INPUT_ICL_PIN_BIT | RESET_STATE_USB_500));
    I2C_VERIFY(smb_set_input_limit_mode(IL_USB500));

    result = I2CDEV_SetPowerState(handle, I2CDEV_POWER_STATE_0);
    if(I2C_RES_SUCCESS != result)
    {
        return PM_ERR_FLAG__I2C_OPT_ERR;
    }

    result = I2CDEV_DeInit(handle);

    if(I2C_RES_SUCCESS != result)
    {
        return PM_ERR_FLAG__I2C_OPT_ERR;
    }
    else
    {
        return PM_ERR_FLAG__SUCCESS;
    }
}

/*
 * Get the index of a given current. If the value doesn't exist in the list,
 * return the index of a closet current which less than the give current.
 * For example, for both 600ma and 749ma, you'll get index 1.
 * For current < 450ma, return 0, For current > 1500, return 7
 */
static uint8 smb_get_ibat_index(uint32 current)
{
    uint8 ind = 1;
    uint32 fast_current[] = {450, 600, 750, 900, 1050, 1200, 1350, 1500};
    uint8 size = sizeof(fast_current) / sizeof(uint32);

    if(current < fast_current[0])
    {
        return 0;
    }

    while(ind < size - 1)
    {
        if(current < fast_current[ind])
        {
            return ind - 1;
        }
        else
        {
            ++ind;
        }
    }

    return size - 1;
}

pm_err_flag_type smb_set_ibat_fast(uint32 current)
{
    uint8 ind = smb_get_ibat_index(current);
    return smb_write_mask(CHG_CURRENT_REG, ind << FASTCHG_CURR_SHIFT, FASTCHG_CURR_MASK);
}

/*
 * Get the index of a given current. If the value doesn't exist in the list,
 * return the index of a closet current which less than the give current.
 * For example, for both 400ma and 449ma, you'll get index 1.
 * For current < 300ma, return 0, For current > 1500, return 0xF
 */
static uint8 smb_get_icl_index(uint32 current)
{
    uint8 ind = 1;
    uint32 input_current_limit[] = 
        {300, 400, 450, 500, 600, 700, 800, 850, 900, 950, 
         1000, 1100, 1200, 1300, 1400, 1500,};
    uint8 size = sizeof(input_current_limit) / sizeof(uint32);

    if(current < input_current_limit[0])
    {
        return 0;
    }

    while(ind < size - 1)
    {
        if(current < input_current_limit[ind])
        {
            return ind - 1;
        }
        else
        {
            ++ind;
        }
    }

    return size - 1;
}

pm_err_flag_type smb_set_icl(uint32 current)
{
    uint8 ind = smb_get_icl_index(current);
    return smb_write_mask(CFG_BATT_CHG_ICL_REG, ind, INPUT_CURR_LIM_MASK);
}


