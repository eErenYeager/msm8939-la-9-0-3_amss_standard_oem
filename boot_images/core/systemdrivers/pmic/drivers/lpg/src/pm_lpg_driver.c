/*! \file pm_lpg_driver.c 
*  \n
*  \brief This file contains LPG peripheral driver initialization during which the driver data is stored.
*  \n
*  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/lpg/src/pm_lpg_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/27/13   aab     Creation
========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_lpg_driver.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the LPG driver data */
static pm_lpg_data_type pm_lpg_data_arr[PM_MAX_NUM_DEVICES];
/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_lpg_driver_init(pmiC_IComm *comm_ptr)
{
    pm_lpg_data_type *lpg_ptr = NULL;
    uint32 prop_id_arr[] = {PM_PROP_LPGA_NUM, PM_PROP_LPGB_NUM}; 

    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);

    lpg_ptr = &pm_lpg_data_arr[pmic_index];
    if (lpg_ptr->periph_exists == FALSE)
    {
        lpg_ptr->periph_exists = TRUE;
                                                    
        /* Assign Comm ptr */
        lpg_ptr->comm_ptr = comm_ptr;

        /* LPG Register Info - Obtaining Data through dal config */
        lpg_ptr->lpg_reg_table = (pm_lpg_register_info_type*)pm_target_information_get_common_info((uint32)PM_PROP_LPG_REG);
        CORE_VERIFY_PTR(lpg_ptr->lpg_reg_table);

        /* LPG Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));

		lpg_ptr->num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);
        CORE_VERIFY(lpg_ptr->num_of_peripherals  != 0);

    }
}

pm_lpg_data_type* pm_lpg_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_lpg_data_arr[pmic_index];
    }
    return NULL;
}
