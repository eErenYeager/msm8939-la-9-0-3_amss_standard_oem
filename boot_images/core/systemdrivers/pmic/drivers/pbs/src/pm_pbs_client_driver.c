/*! \file pm_pbs_client_driver.c
*  \n
*  \brief PBS Client driver initialization.
*  \n  
*  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
                            Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/pbs/src/pm_pbs_client_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/13/13   kt      Initial version
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pbs_client_driver.h"
#include "hw_module_type.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/
/* Static global variable to store the RGB driver data */
static pm_pbs_client_data_type pm_pbs_client_data_arr[PM_MAX_NUM_DEVICES];  

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_pbs_client_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
    pm_pbs_client_data_type *pbs_client_ptr = NULL;
    uint32 prop_id_arr[] = {PM_PROP_PBS_CLIENTA_NUM, PM_PROP_PBS_CLIENTB_NUM}; 

    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);

    if (peripheral_info->peripheral_subtype != PM_HW_MODULE_PBS_CLIENT)
    {
        return;
    }

    pbs_client_ptr = &pm_pbs_client_data_arr[pmic_index];
    if (pbs_client_ptr->periph_exists == FALSE)
    {
        pbs_client_ptr->periph_exists = TRUE;

        /* Assign Comm ptr */
        pbs_client_ptr->comm_ptr = comm_ptr;

        /* PBS Client Register Info - Obtaining Data through dal config */
        pbs_client_ptr->pbs_client_reg_table = (pm_pbs_client_register_info_type*)pm_target_information_get_common_info((uint32)PM_PROP_PBS_CLIENT_REG);
        CORE_VERIFY_PTR(pbs_client_ptr->pbs_client_reg_table);

        /* PBS Client Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));
                                                              
		pbs_client_ptr->num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);
        CORE_VERIFY(pbs_client_ptr->num_of_peripherals != 0);
        }

    }

pm_pbs_client_data_type* pm_pbs_client_get_data(uint8 pmic_index)
    {
    if(pmic_index < PM_MAX_NUM_DEVICES)
       {
        return &pm_pbs_client_data_arr[pmic_index];
    }
    return NULL;
}

