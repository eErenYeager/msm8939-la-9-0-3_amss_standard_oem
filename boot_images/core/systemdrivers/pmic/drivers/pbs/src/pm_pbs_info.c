/*! \file  pm_pbs_info.c
 *
 *  \brief  This file contains the pmic PBS info driver implementation.
 *  \details  This file contains the pm_pbs_info_init & pm_pbs_info_store_glb_ctxt
 *  API implementations.
 *
 *  &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/pbs/src/pm_pbs_info.c#1 $
$DateTime: 2015/03/19 01:58:37 $  $Author: pwbldsvc $

when         who     what, where, why
----------   ---     ----------------------------------------------------------
06/17/2014   mr      Added support for PM8916 v2.0 FAB_ID read (CR-681200)
01/15/2014   rk      Added test pgm rev to pbs_info struct (increment struct format)
03/19/2013   kt      Added pm_pbs_info_add_ram_sequence API.
10/19/2012   umr     PBS Core Driver.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "pm_pbs_driver.h"
#include "pm_pbs_info.h"
#include "pm_version.h"
#include "DALGlbCtxt.h"

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/* Format of the pm_pbs_info_smem_type structure */
#define PM_PBS_INFO_SMEM_FORMAT              3

/* PBS ROM (256 word) info start address */
#define PM_PBS_INFO_ROM_START_ADDR           0x03E4

/* PBS RAM (256 word) info start address */
#define PM_PBS_INFO_RAM_START_ADDR           0x07FC

/* PM8916 PBS ROM (128 word) Info start address */
#define PM_PBS_INFO_PM8916_ROM_START_ADDR        0x0
/* PM8916 PBS RAM (128 word) Info start address */
#define PM_PBS_INFO_PM8916_RAM_START_ADDR        0x0400

/* For, PM8916 v1.1 - FAB_ID, ROM Version, Lot ID and other info stored at
   0->122th line in PBS ROM (121 * 4 = 484 = 0x01E4) */
#define PM_PBS_INFO_PM8916_ROM_INFO_START_ADDR   (PM_PBS_INFO_PM8916_ROM_START_ADDR + 0x01E4)

/* PBS ROM-RAM Version stored at 0->128th line (127 * 4 = 508 = 0x1FC) */
#define PM8916_ROM_VERSION_READ_ADDR             (PM_PBS_INFO_PM8916_ROM_START_ADDR + 0x01FC)
#define PM8916_RAM_VERSION_READ_ADDR             (PM_PBS_INFO_PM8916_RAM_START_ADDR + 0x01FC)

/**
  @struct pm_pbs_info_smem_type
  @brief Structure for the shared memory location which is used
         to store PMIC PBS related information such as PBS Lot
         ID, ROM Version, RAM Version, Fab Id, Wafer Id, X
         coord, Y coord and Test PGM Rev. PBS ROM/RAM Revision
         id and Variant (or Branch) id are stored in last 16
         bits(upper and lower 8 bits) of rom_version and
         ram_version.
 */
typedef struct
{
  uint32         format;                           /* Starts from 1 and increments if we add more data */
  uint8          lot_id[PM_PBS_INFO_NUM_LOT_IDS];  /* PBS Lot Id */
  uint32         rom_version;                      /* PBS ROM Version number */
  uint32         ram_version;                      /* PBS RAM Version number */
  uint32         fab_id;                           /* PBS Fab Id */
  uint32         wafer_id;                         /* PBS Wafer Id */
  uint32         x_coord;                          /* PBS X Coord */
  uint32         y_coord;                          /* PBS Y Coord */
  uint32         test_pgm_rev;                     /* PBS Test PGM Rev */
} pm_pbs_info_smem_type;

/**
  @struct pm_pbs_info_glb_ctxt_type
  @brief Global context data structure for sharing the pbs info
         across processors.
 */
typedef struct
{
  DALGLB_HEADER             dal_glb_header;
  pm_pbs_info_smem_type     pbs_info_glb_arr[PM_MAX_NUM_DEVICES];
} pm_pbs_info_glb_ctxt_type;

/* Static global variables to store the pbs info */
static pm_pbs_info_smem_type pm_pbs_info_arr[PM_MAX_NUM_DEVICES];

/* Flag to check if PBS Info driver is initialized */
static boolean pm_pbs_info_initialized = FALSE;

static pm_device_info_type PmicDeviceInfo;

/*=========================================================================
      Function Definitions
==========================================================================*/
/**
 * @name pm_pbs_info_read
 *
 * @brief This is an internal helper function for reading
 *        PBS info for PBS Peripheral with 256 word ROM and
 *        256 word RAM support. This function reads the PBS
 *        ROM/RAM addresses for PMIC PBS Manufacturing IDs
 *        and foundry information such as PBS Lot ID, ROM
 *        Version, RAM Version, Fab Id, Wafer Id, X coord
 *        and Y coord. This function internally calls
 *        pm_pbs_enable/disable_access to enable/disable
 *        PBS ROM/RAM access and pm_pbs_config_access to
 *        configure READ BURST mode access to PBS ROM/RAM.
 *
 * @param[in]  slave_id. PMIC chip's slave id value.
 * @param[out] pbs_info_ptr: Variable to return to the caller with
 *             PBS info. Please refer to pm_pbs_info_smem_type structure
 *             above for more info on this structure.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          else SPMI ERROR.
 *
 * @dependencies None.
 */
static pm_err_flag_type pm_pbs_info_read (uint32 slave_id, pm_pbs_info_smem_type *pbs_info_ptr)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  uint32           read_info_temp = 0;
  uint8            lot_info_cnt = PM_PBS_INFO_NUM_LOT_IDS;
  uint8            lot_info_reg_reads_cnt = (PM_PBS_INFO_NUM_LOT_IDS/3);

  pbs_info_ptr->format = PM_PBS_INFO_SMEM_FORMAT;

  /* Configuring to enable PBS core access for ROM reads */
  err_flag = pm_pbs_enable_access(slave_id);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Configuring the base address for reading PBS ROM info */
  if (PMIC_IS_PM8916 == PmicDeviceInfo.ePmicModel)
  {
    err_flag = pm_pbs_config_access(slave_id, PM_PBS_INFO_PM8916_ROM_INFO_START_ADDR, PM_PBS_ACCESS_READ_BURST);
  }
  else
  {
    err_flag = pm_pbs_config_access(slave_id, PM_PBS_INFO_ROM_START_ADDR, PM_PBS_ACCESS_READ_BURST);
  }
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Reading the Fab id and storing it in temp variable */
  err_flag = pm_spmi_lite_read_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_RD_DATA0_ADDR, 4, (uint8*)&read_info_temp, 1);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
     return err_flag;
  }

  /* Masking the opcode and reserved bits from temp variable
     storing the Fab id and TEST_PGM_REV in the pbs info struct */
  pbs_info_ptr->test_pgm_rev = (uint32)(read_info_temp & 0x000000FF);
  pbs_info_ptr->fab_id = (uint32)((read_info_temp>>8) & 0x000000FF);
  read_info_temp = 0;

  /* Reading the Wafer ID, X and Y coords and storing it in temp variable */
  err_flag = pm_spmi_lite_read_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_RD_DATA0_ADDR, 4, (uint8*)&read_info_temp, 1);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
     return err_flag;
  }

  /* Masking the opcode bits from temp variable storing the Wafer ID, X and
     Y coords along with the existing Fab ID in the pbs info struct */
  pbs_info_ptr->wafer_id = (uint32)(read_info_temp & 0x000000FF);
  pbs_info_ptr->x_coord = (uint32)((read_info_temp>>8) & 0x000000FF);
  pbs_info_ptr->y_coord = (uint32)((read_info_temp>>16) & 0x000000FF);
  read_info_temp = 0;

  /* Reading the LOT info and storing it in the pbs info struct */
  while((lot_info_reg_reads_cnt != 0) && (lot_info_cnt > 2))
  {
    /* Reading the LOT info and storing it in temp variable */
    err_flag = pm_spmi_lite_read_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_RD_DATA0_ADDR, 4, (uint8*)&read_info_temp, 1);
    if (err_flag != PM_ERR_FLAG__SUCCESS)
    {
      return err_flag;
    }

    pbs_info_ptr->lot_id[--lot_info_cnt] = (uint8)(read_info_temp & 0x000000FF);
    pbs_info_ptr->lot_id[--lot_info_cnt] = (uint8)((read_info_temp >> 8) & 0x000000FF);
    pbs_info_ptr->lot_id[--lot_info_cnt] = (uint8)((read_info_temp >> 16) & 0x000000FF);

    read_info_temp = 0;

    lot_info_reg_reads_cnt--;
  }

  /* Reading the ROM version info and storing it in temp variable */
  err_flag = pm_spmi_lite_read_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_RD_DATA0_ADDR, 4, (uint8*)&read_info_temp, 1);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Masking the opcode and reserved bits from temp variable
     storing the ROM version info in the PBS info struct */
  /* If rom_version = 0x0100 (PDM Label 2p0_1_0_1), then ROM Version : (Info branch = 0, Revision = 1) */
  pbs_info_ptr->rom_version = (uint32)(read_info_temp & 0x0000FFFF);
  read_info_temp = 0;

  /* Configuring the base address for reading PBS RAM version info */
  if (PMIC_IS_PM8916 == PmicDeviceInfo.ePmicModel)
  {
    err_flag = pm_pbs_config_access(slave_id, PM8916_RAM_VERSION_READ_ADDR, PM_PBS_ACCESS_READ_BURST);
  }
  else
  {
    err_flag = pm_pbs_config_access(slave_id, PM_PBS_INFO_RAM_START_ADDR, PM_PBS_ACCESS_READ_BURST);
  }
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Reading the RAM version info and storing it in temp variable */
  err_flag = pm_spmi_lite_read_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_RD_DATA0_ADDR, 4, (uint8*)&read_info_temp, 1);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Masking the opcode and reserved bits from temp variable
     storing the RAM version info in the pbs info struct */
  /* If ram_version = 0x0200 (PDM Label 2p0_1_0_2), then RAM Version : (Info branch = 0, Revision = 2) */
  pbs_info_ptr->ram_version = (uint32)(read_info_temp & 0x0000FFFF);
  read_info_temp = 0;

  /* Configuring to disable PBS core read access */
  err_flag = pm_pbs_disable_access(slave_id);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  return err_flag;
}

/**
 * @name pm_pbs_info_pm8916_read
 *
 * @brief This is an internal helper function is used for
 *        reading PBS info for PBS peripheral with 256 word ROM
 *        support. This function reads the PBS ROM address and
 *        MISC module registers for PMIC PBS Manufacturing IDs
 *        and foundry information such as PBS Lot ID, ROM Version,
 *        RAM version, Fab Id, Wafer Id, X coord and Y coord.
 *
 * @param[in]  slave_id. PMIC chip's slave id value.
 * @param[out] pbs_info_ptr: Variable to return to the caller with
 *             PBS info. Please refer to pm_pbs_info_smem_type
 *             structure above for more info on this structure.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          else SPMI ERROR.
 *
 * @dependencies None.
 */
static pm_err_flag_type pm_pbs_info_pm8916_read (uint32 slave_id, pm_pbs_info_smem_type *pbs_info_ptr)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  uint32           read_info_temp = 0;
  uint16           lot_info_addr = 0;
  uint8            lot_info_cnt = 0;
  uint8            lot_info_temp1 = 0;
  uint8            lot_info_temp2 = 0;
  uint8            tp_rev_temp = 0;

  pbs_info_ptr->format = PM_PBS_INFO_SMEM_FORMAT;

  /* Read MISC Peripheral registers to get the PBS info */

  /* Reading the TEST_PGM_REV and storing it in temp variable */
  err_flag = pm_spmi_lite_read_byte(slave_id, PMIO_MISC_TP_REV_ADDR, &tp_rev_temp, 1);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  pbs_info_ptr->test_pgm_rev = (uint32)tp_rev_temp;

  /* Reading the Fab id, Wafer id, X/Y coords and storing it in temp variable */
  err_flag = pm_spmi_lite_read_byte_array(slave_id, PMIO_MISC_FAB_ID_ADDR, 4, (uint8*)&read_info_temp, 1);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Storing the Fab ID, Wafer ID, X and Y coords in the pbs info struct */
  pbs_info_ptr->fab_id = (uint32)(read_info_temp & 0x000000FF);
  pbs_info_ptr->wafer_id = (uint32)((read_info_temp>>8) & 0x000000FF);
  pbs_info_ptr->x_coord = (uint32)((read_info_temp>>16) & 0x000000FF);
  pbs_info_ptr->y_coord = (uint32)((read_info_temp>>24) & 0x000000FF);
  read_info_temp = 0;

  /* Reading the Lot info stored in MISC peripheral registers. 12 Lot Ids are
     stored in 9 registers with 6 bits assigned for each lot id. Since the bit
     mask for the lot ids would be the same for every 3 register reads, below
     logic is implemented in while loop. The logic is to read and store 4 lot ids
     spread across 3 registers and repeat the same logic thrice in the loop for
     9 registers */

  /* Starting to read from LOT_ID_01_00 register and decrement to the next register
     addr after every read */

  lot_info_addr = PMIO_MISC_LOT_ID_01_00_ADDR;

  /* The third and final loop will start the register read from LOT_ID_09_08 register
     and the lot info count should be < 9 since we increment the count 4 times per
     loop and the number of lot ids are 12 */

  while ((lot_info_addr > PMIO_MISC_LOT_ID_10_09_ADDR) && (lot_info_cnt < 9))
  {
     /* Since this WHILE loop runs thrice and 4 lot ids are stored per loop, consider
        'X' in the below comments to be 0, 4 and 8 corresponding to each loop */

     /* Reading the LOT_ID_01_00 (+ X) reg and storing it in first temp variable and then
        decrement the register address for the next read */
     err_flag = pm_spmi_lite_read_byte(slave_id, lot_info_addr--, &lot_info_temp1, 1);
     if (err_flag != PM_ERR_FLAG__SUCCESS)
     {
        return err_flag;
     }

     /* Storing the Lot id 0 (+ X) LSB 6 bits in the pbs info struct */
     pbs_info_ptr->lot_id[lot_info_cnt++] = (uint8)(lot_info_temp1 & 0x3F);

     /* Storing the Lot id 1 (+ X) MSB 2 bits in the second temp variable */
     lot_info_temp2 = (uint8)((lot_info_temp1>>6) & 0x03);

     /* Resetting the first temp variable */
     lot_info_temp1 = 0;

     /* Reading the LOT_ID_02_01 (+ X) reg and storing it in first temp variable and then
        decrement the register address for the next read */
     err_flag = pm_spmi_lite_read_byte(slave_id, lot_info_addr--, &lot_info_temp1, 1);
     if (err_flag != PM_ERR_FLAG__SUCCESS)
     {
        return err_flag;
     }

     /* Storing the Lot id 1 (+ X) in the pbs info struct */
     pbs_info_ptr->lot_id[lot_info_cnt++] = (uint8)(((lot_info_temp1<<2)& 0x3F)|(lot_info_temp2));

     /* Resetting the second temp variable */
     lot_info_temp2 = 0;

     /* Storing the Lot id 2 (+ X) bits in the second temp variable */
     lot_info_temp2 = (uint8)((lot_info_temp1>>4) & 0x0F);

     /* Resetting the first temp variable */
     lot_info_temp1 = 0;

     /* Reading the LOT_ID_03_02 (+ X) reg and storing it in first temp variable and then
        decrement the register address for the next read */
     err_flag = pm_spmi_lite_read_byte(slave_id, lot_info_addr--, &lot_info_temp1, 1);

     if (err_flag != PM_ERR_FLAG__SUCCESS)
     {
        return err_flag;
     }

     /* Storing the Lot id 2 (+ X) in the pbs info struct */
     pbs_info_ptr->lot_id[lot_info_cnt++] = (uint8)(((lot_info_temp1<<4)& 0x3F)|(lot_info_temp2));

     /* Storing the Lot id 3 (+ X) in the pbs info struct */
     pbs_info_ptr->lot_id[lot_info_cnt++] = (uint8)((lot_info_temp1>>2) & 0x3F);

     /* Resetting the second temp variable */
     lot_info_temp2 = 0;

     /* Resetting the first temp variable */
     lot_info_temp1 = 0;
  }

  /* Configuring to enable PBS core access for ROM reads */
  err_flag = pm_pbs_enable_access(slave_id);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Configuring the base address for reading PBS ROM version info */
  err_flag = pm_pbs_config_access(slave_id, PM8916_ROM_VERSION_READ_ADDR, PM_PBS_ACCESS_READ_BURST);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Reading the ROM version info and storing it in temp variable */
  err_flag = pm_spmi_lite_read_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_RD_DATA0_ADDR, 4, (uint8*)&read_info_temp, 1);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Masking the opcode and reserved bits from temp variable
     storing the ROM version info in the pbs info struct */
  pbs_info_ptr->rom_version = (uint32)(read_info_temp & 0x0000FFFF);
  read_info_temp = 0;

  /* Configuring the base address for reading PBS RAM version info */
  err_flag = pm_pbs_config_access(slave_id, PM8916_RAM_VERSION_READ_ADDR, PM_PBS_ACCESS_READ_BURST);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Reading the RAM version info and storing it in temp variable */
  err_flag = pm_spmi_lite_read_byte_array(slave_id, PMIO_PBS_CORE_MEM_INTF_RD_DATA0_ADDR, 4, (uint8*)&read_info_temp, 1);
  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  /* Masking the opcode and reserved bits from temp variable
     storing the RAM version info in the pbs info struct */
  pbs_info_ptr->ram_version = (uint32)(read_info_temp & 0x0000FFFF);
  read_info_temp = 0;

  /* Configuring to disable PBS core read access */
  err_flag = pm_pbs_disable_access(slave_id);

  if (err_flag != PM_ERR_FLAG__SUCCESS)
  {
    return err_flag;
  }

  return err_flag;
}

/**
 * @name pm_pbs_info_init
 *
 * @brief Please refer to pm_pbs_info.h file for info regarding
 *        this function.
 */
pm_err_flag_type pm_pbs_info_init (void)
{
  pm_err_flag_type   err_flag = PM_ERR_FLAG__SUCCESS;
  uint32             slave_id = 0;
  uint8              pmic_index = 0;
  uint8              data = 0;

  DALSYS_memset(pm_pbs_info_arr, 0, sizeof(pm_pbs_info_arr));

  for (pmic_index = 0; pmic_index < PM_MAX_NUM_DEVICES; pmic_index++)
  {
    /* Get PMIC device primary slave id */
    err_flag = pm_get_slave_id(pmic_index, 0, &slave_id);
    if (err_flag != PM_ERR_FLAG__SUCCESS)
    {
      /* Second or third PMIC doesn't exist */
      err_flag = PM_ERR_FLAG__SUCCESS;
      break;
    }

    /* Read PBS Peripheral info and check if PBS peripheral exists */
    err_flag = pm_spmi_lite_read_byte(slave_id, PMIO_PBS_CORE_PERPH_TYPE_ADDR, &data, 0);
    if (err_flag != PM_ERR_FLAG__SUCCESS)
    {
       return err_flag;
    }

    if (data != PM_HW_MODULE_PBS)
    {
       return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    if (PM_ERR_FLAG__SUCCESS != pm_get_pmic_info(0, &PmicDeviceInfo))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    /* PBS info is read differently for PM8916 v2.0 PMIC model. The PBS info
     * (except for ROM version info) is stored in MISC peripheral registers. */
    if(PMIC_IS_PM8916 == PmicDeviceInfo.ePmicModel && PmicDeviceInfo.nPmicAllLayerRevision >= 2)
    {
       /* Read all the PMIC's PBS Manufacturing ids and store it in static global var */
       err_flag = pm_pbs_info_pm8916_read(slave_id, &(pm_pbs_info_arr[pmic_index]));
    }
    else
    {
       /* Read all the PMIC's PBS Manufacturing ids and store it in static global var */
       err_flag = pm_pbs_info_read(slave_id, &(pm_pbs_info_arr[pmic_index]));
    }

    if (err_flag != PM_ERR_FLAG__SUCCESS)
    {
      /* Configuring to disable PBS core read access */
      pm_pbs_disable_access(slave_id);

      return err_flag;
    }
  }

  pm_pbs_info_initialized = TRUE;
  return err_flag;
}

/**
 * @name pm_pbs_info_store_glb_ctxt
 *
 * @brief Please refer to pm_pbs_info.h file for info regarding
 *        this function.
 */
pm_err_flag_type pm_pbs_info_store_glb_ctxt (void)
{
  pm_pbs_info_glb_ctxt_type*  pbs_glb_ctxt_ptr = NULL;
  uint8                       pmic_index = 0;

  /* Return error if PBS driver is not already initialized */
  if (!pm_pbs_info_initialized)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }

  /* Ensure global context has been initialized */
  if(DAL_SUCCESS != DALGLBCTXT_Init())
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }

  /* Allocate the context */
  if(DAL_SUCCESS != DALGLBCTXT_AllocCtxt("PM_PBS_INFO", sizeof(pm_pbs_info_glb_ctxt_type),
                                         DALGLBCTXT_LOCK_TYPE_SPINLOCK, (void **)&pbs_glb_ctxt_ptr))
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }

  if (pbs_glb_ctxt_ptr == NULL)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }

  for (pmic_index = 0; pmic_index < PM_MAX_NUM_DEVICES; pmic_index++)
  {
    /* Fill in the global context with PMIC's PBS info */
    DALSYS_memscpy(&(pbs_glb_ctxt_ptr->pbs_info_glb_arr[pmic_index]), sizeof(pm_pbs_info_smem_type), &(pm_pbs_info_arr[pmic_index]), sizeof(pm_pbs_info_smem_type));
  }

  return PM_ERR_FLAG__SUCCESS;
}

pm_err_flag_type pm_get_pbs_info(uint8 pmic_device_index, pm_pbs_info_type* pbs_info_ptr)
{
   /* Return error if PBS driver is not already initialized or invalid arguments are passed */
  if((NULL == pbs_info_ptr) || (pmic_device_index >= PM_MAX_NUM_DEVICES) || (!pm_pbs_info_initialized))
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }

  if (pm_pbs_info_arr[pmic_device_index].format != PM_PBS_INFO_SMEM_FORMAT)
  {
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
  }

  //get the PBS info from static global variable and return it
  DALSYS_memscpy(pbs_info_ptr->lot_id, sizeof(pbs_info_ptr->lot_id), pm_pbs_info_arr[pmic_device_index].lot_id, sizeof(pbs_info_ptr->lot_id));
  pbs_info_ptr->rom_version = pm_pbs_info_arr[pmic_device_index].rom_version;
  pbs_info_ptr->ram_version = pm_pbs_info_arr[pmic_device_index].ram_version;
  pbs_info_ptr->fab_id = pm_pbs_info_arr[pmic_device_index].fab_id;
  pbs_info_ptr->wafer_id = pm_pbs_info_arr[pmic_device_index].wafer_id;
  pbs_info_ptr->x_coord = pm_pbs_info_arr[pmic_device_index].x_coord;
  pbs_info_ptr->y_coord = pm_pbs_info_arr[pmic_device_index].y_coord;
  pbs_info_ptr->test_pgm_rev = pm_pbs_info_arr[pmic_device_index].test_pgm_rev;

  return PM_ERR_FLAG__SUCCESS;
}
