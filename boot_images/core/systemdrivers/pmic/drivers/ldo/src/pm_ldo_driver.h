#ifndef PM_LDO_DRIVER__H
#define PM_LDO_DRIVER__H

/*! \file pm_ldo_driver.h
 *  \n
 *  \brief This file contains LDO peripheral driver related function prototypes,
 *         enums and driver data structure type.   
 *  \n  
 *  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
 */
/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/ldo/src/pm_ldo_driver.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
12/06/12   hw      Rearchitecturing module driver to peripheral driver
========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_target_information.h"
#include "pm_pwr_alg.h"

/*===========================================================================

                     LDO TYPES AND STRUCTURES 

===========================================================================*/
typedef struct
{
    boolean            periph_exists;
    pmiC_IComm        *comm_ptr;
    pm_pwr_data_type   pm_pwr_data;
} pm_ldo_data_type;

/*===========================================================================

                     FUNCTION DECLARATION 

===========================================================================*/
void pm_ldo_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info);

pm_ldo_data_type* pm_ldo_get_data(uint8 pmic_index);

#endif /* PM_LDO_DRIVER__H */
