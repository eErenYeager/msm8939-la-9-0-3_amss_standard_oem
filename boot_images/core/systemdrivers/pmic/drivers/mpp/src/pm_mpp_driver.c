/*! \file pm_mpp_driver.c 
*  \n
*  \brief This file contains MPP peripheral driver initialization during which the driver data is stored.
*  \n
*  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/mpp/src/pm_mpp_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/18/14   mr      Removed KW errors (CR-643316)
10/08/13   rk      Remove Dig Inout support for MPPs
04/05/11   hs      Updated for Badger.
04/05/11   hs      Refactored the code.
12/20/10   wra     Initial Creation
========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_mpp_driver.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the MPP data */
static pm_mpp_data_type pm_mpp_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_mpp_driver_init(pmiC_IComm *comm_ptr)
{
	pm_mpp_data_type *mpp_ptr = NULL;
	uint32 prop_id_arr[] = {PM_PROP_MPPA_NUM, PM_PROP_MPPB_NUM};
	uint8 index;

	DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);
	
	mpp_ptr = &pm_mpp_data_arr[pmic_index];
    if (mpp_ptr->periph_exists == FALSE)
    {
        mpp_ptr->periph_exists = TRUE;
                                                    
        /* Assign Comm ptr */
        mpp_ptr->comm_ptr = comm_ptr;
		
		/* MPP Register Info - Obtaining Data through dal config */
        mpp_ptr->mpp_reg_table = (pm_mpp_register_info_type*)pm_target_information_get_common_info(PM_PROP_MPP_REG);
        CORE_VERIFY_PTR(mpp_ptr->mpp_reg_table);
		
        /* MPP Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));
        		
	
        mpp_ptr->num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);
        CORE_VERIFY(mpp_ptr->num_of_peripherals  != 0);
		
		mpp_ptr->mpp_specific = (pm_mpp_specific_info_type*)pm_target_information_get_specific_info(PM_PROP_MPP_SPECIFIC_DATA);
		CORE_VERIFY_PTR(mpp_ptr->mpp_specific);
		
		pm_malloc(sizeof(uint32)* (mpp_ptr->num_of_peripherals), (void**)&(mpp_ptr->m_mpp_using_abus)); 
		for(index = 0; index < mpp_ptr->num_of_peripherals; index++)
		{
			mpp_ptr->m_mpp_using_abus[index] = 0;
		}

		pm_malloc(sizeof(int)* (mpp_ptr->mpp_specific->num_of_dbus), (void**)&(mpp_ptr->m_dbus_busy)); 
		for(index = 0; index < mpp_ptr->mpp_specific->num_of_dbus; index++)
		{
			mpp_ptr->m_dbus_busy[index] = mpp_ptr->num_of_peripherals;
		}
    }
}

pm_mpp_data_type* pm_mpp_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_mpp_data_arr[pmic_index];
    }

    return NULL;
}

