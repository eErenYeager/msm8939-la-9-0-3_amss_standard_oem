/*! \file pm_rtc_driver.c
*  \n
*  \brief This file contains PON peripheral driver initialization during which the driver
*         driver data is stored.
*  \n
*  &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: 

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/31/14   akm     Updated to the latest PMIC driver architecture   
01/20/14   kt      Created.
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_rtc_driver.h"
#include "hw_module_type.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the RTC data */
static pm_rtc_data_type pm_rtc_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/

void pm_rtc_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
   DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);

   CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);

   /* Currently the API added only supports RTC_RW peripheral subtype */
   if (peripheral_info->peripheral_subtype == PM_HW_MODULE_RTC_RTC_RW)
   {
      pm_rtc_data_arr[pmic_index].periph_exists = TRUE;
      pm_rtc_data_arr[pmic_index].comm_ptr = comm_ptr;
   }
}

pm_rtc_data_type* pm_rtc_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_rtc_data_arr[pmic_index];
    }

    return NULL;
}

