/*! \file pm_rtc.c
*  \n
*  \brief This file contains the implementation of the public APIs for PMIC RTC module.
*  \n
*  &copy; Copyright 2013-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/rtc/src/pm_rtc.c#1 $ 

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
11/08/13   kt      Created.
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_rtc.h"
#include "pm_rtc_driver.h"
#include "hw_module_type.h"

/*===========================================================================

                     API IMPLEMENTATION 

===========================================================================*/

pm_err_flag_type pm_rtc_get_time(uint8 pmic_index, uint32 *time_ptr)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_rtc_data_type *rtc_ptr = pm_rtc_get_data(pmic_index);
    pm_register_data_type rtc_enable = 0;
    pmiC_IComm *local_comm = NULL;

    if ((rtc_ptr == NULL) || (rtc_ptr->periph_exists == FALSE))
    { 
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if (time_ptr == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
       local_comm = rtc_ptr->comm_ptr;

       err_flag = local_comm->ReadByteMask(local_comm, PMIO_RTC_WR_EN_CTL1_ADDR, 0x80, &rtc_enable);

       if (err_flag == PM_ERR_FLAG__SUCCESS)
       {
          if (rtc_enable == 0)
          {
             err_flag = PM_ERR_FLAG__RTC_HALTED;
          }
       }

       err_flag |= local_comm->ReadByteArray(local_comm, PMIO_RTC_WR_RDATA0_ADDR, 4, (uint8*)time_ptr, 0);
    }

    return err_flag; 
}

