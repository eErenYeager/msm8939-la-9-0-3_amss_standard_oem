/*! \file pm_smbb_driver.c
 *  \n
 *  \brief  
 *  \details  
 *  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/smbb/src/pm_smbb_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/06/13   aab     Added pm_smbb_chg_get_chg_gone_rt_status() 
06/17/13   aab     Creation
========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_smbb_driver.h"
#include "CoreVerify.h"


/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the MPP data */
static pm_smbb_data_type pm_smbb_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/


void pm_smbb_driver_init(pmiC_IComm *comm_ptr)
{
    pm_smbb_data_type* smbb_ptr = NULL;
	uint32 prop_id_arr[] = {PM_PROP_SMBBA_NUM, PM_PROP_SMBBB_NUM};

	DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);
 	smbb_ptr = &pm_smbb_data_arr[pmic_index];

	if (smbb_ptr->periph_exists == FALSE)
    {
        smbb_ptr->periph_exists = TRUE;
                                                    
        /* Assign Comm ptr */
        smbb_ptr->comm_ptr = comm_ptr;
		
		/* SMBB Register Info - Obtaining Data through dal config */
        smbb_ptr->smbb_reg_table = (pm_smbb_register_info_type*)pm_target_information_get_common_info(PM_PROP_SMBB_REG);
        CORE_VERIFY_PTR(smbb_ptr->smbb_reg_table);
		
        /* SMBB Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));
		
        smbb_ptr->num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);
        CORE_VERIFY(smbb_ptr->num_of_peripherals  != 0);
		
		smbb_ptr->smbb_specific = (pm_smbb_specific_info_type*)pm_target_information_get_specific_info(PM_PROP_SMBB_SPECIFIC_DATA);
		CORE_VERIFY_PTR(smbb_ptr->smbb_specific);
		
    }
}


pm_smbb_data_type* pm_smbb_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_smbb_data_arr[pmic_index];
    }

    return NULL;
}


