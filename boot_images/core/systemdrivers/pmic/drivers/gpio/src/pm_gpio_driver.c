/*! \file pm_gpio_driver.c
 *  \n
 *  \brief  
 *  \details  
 *  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/gpio/src/pm_gpio_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/25/13   aab     Creation
========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_gpio_driver.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the GPIO data */
static pm_gpio_data_type pm_gpio_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/
void pm_gpio_driver_init(pmiC_IComm *comm_ptr)
{
	pm_gpio_data_type *gpio_ptr = NULL;
	uint32 prop_id_arr[] = {PM_PROP_GPIOA_NUM, PM_PROP_GPIOB_NUM};
	
    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);	
    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);
	
	gpio_ptr = &pm_gpio_data_arr[pmic_index];
    if (gpio_ptr->periph_exists == FALSE)
    {
        gpio_ptr->periph_exists = TRUE;
                                                    
        /* Assign Comm ptr */
        gpio_ptr->comm_ptr = comm_ptr;
		
		/* GPIO Register Info - Obtaining Data through dal config */
        gpio_ptr->gpio_reg_table = (pm_gpio_register_info_type*)pm_target_information_get_common_info(PM_PROP_GPIO_REG);
        CORE_VERIFY_PTR(gpio_ptr->gpio_reg_table);

        /* GPIO Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));
		
        gpio_ptr->num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);
        CORE_VERIFY(gpio_ptr->num_of_peripherals  != 0);
    }
}

pm_gpio_data_type* pm_gpio_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_gpio_data_arr[pmic_index];
    }
    return NULL;
}
