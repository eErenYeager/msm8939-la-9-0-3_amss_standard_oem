#ifndef PM_VIB_DRIVER__H
#define PM_VIB_DRIVER__H

/*! \file
*  \n
*  \brief  pm_vib_driver.h 
*  \details  This file contains functions prototypes and variable/type/constant
*  declarations for supporting vib peripheral 
*  
*  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/vib/src/pm_vib_driver.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/04/13   aab     Created
========================================================================== */

/*===========================================================================
						INCLUDE FILES
===========================================================================*/

#include "pm_target_information.h"

/*===========================================================================

                     VIB TYPES AND STRUCTURES 

===========================================================================*/
typedef struct
{                  
  pm_register_address_type base_address;  
  pm_register_address_type peripheral_offset;
  pm_register_address_type status1;       //0x008
  pm_register_address_type voltage_ctl2;  //0x041
  pm_register_address_type en_ctl;        //0x046
} pm_vib_register_info_type;

typedef struct
{
    uint16 MinVoltage;
    uint16 MaxVoltage;
} pm_vib_specific_data_type;

typedef struct
{
    boolean                     periph_exists;
    pmiC_IComm                 *comm_ptr;
    uint32                      num_of_peripherals;
    pm_vib_register_info_type  *vib_reg_table;
    pm_vib_specific_data_type  *vib_data;
} pm_vib_data_type;




/*===========================================================================

                     FUNCTION DECLARATION 

===========================================================================*/
void pm_vib_driver_init(pmiC_IComm *comm_ptr);

pm_vib_data_type* pm_vib_get_data(uint8 pmic_index);
#endif // PM_VIB_DRIVER__H

