
/*! \file vib_api.c
*  \n
*  \details This file contains the top-level API wrappers for the Vib
*           peripheral.
*
*  \n &copy; Copyright 2012 Qualcomm Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/drivers/vib/src/pm_vib.c#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $
========================================================================== */

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "pm_resource_manager.h"
#include "pm_vib.h"
#include "pm_vib_driver.h"

pm_err_flag_type pm_vib_enable
(
  unsigned          pmic_chip,
  pm_vib_which_type vib,
  pm_vib_mode_type  mode,
  boolean           enable
)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type reg_vib_en_ctl = 0;
    pm_register_address_type periph_index = (pm_register_address_type)vib;
    pm_vib_data_type *vib_ptr = pm_vib_get_data(pmic_chip);

    if ((vib_ptr == NULL) || (vib_ptr->periph_exists == FALSE))
    {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    if (periph_index >= vib_ptr->num_of_peripherals)
    {
      return PM_ERR_FLAG__INVALID_VIBRATOR_INDEXED;
    }

    reg_vib_en_ctl = vib_ptr->vib_reg_table->base_address + (vib_ptr->vib_reg_table->peripheral_offset*periph_index) + vib_ptr->vib_reg_table->en_ctl;

    switch (mode)
    {
      case PM_VIB_MODE__MANUAL:
        err_flag = vib_ptr->comm_ptr->WriteByteMask(vib_ptr->comm_ptr, reg_vib_en_ctl, 0x80, (pm_register_data_type)(enable << 7), 0);
        break;
      case PM_VIB_MODE__DBUS1:
        err_flag = vib_ptr->comm_ptr->WriteByteMask(vib_ptr->comm_ptr, reg_vib_en_ctl, 0x01, (pm_register_data_type)(enable), 0);
        break;
      case PM_VIB_MODE__DBUS2:
        err_flag = vib_ptr->comm_ptr->WriteByteMask(vib_ptr->comm_ptr, reg_vib_en_ctl, 0x02, (pm_register_data_type)(enable << 1), 0);
        break;
      case PM_VIB_MODE__DBUS3:
        err_flag = vib_ptr->comm_ptr->WriteByteMask(vib_ptr->comm_ptr, reg_vib_en_ctl, 0x04, (pm_register_data_type)(enable << 2), 0);
        break;
      default:
        err_flag = PM_ERR_FLAG__PAR3_OUT_OF_RANGE;
    }

    return err_flag;
}


pm_err_flag_type pm_vib_set_volt
(
  unsigned           pmic_chip,
  pm_vib_which_type  vib,
  uint16             voltage
)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type reg_vib_voltage_ctl2 = 0;
    pm_register_address_type periph_index = (pm_register_address_type)vib;
    pm_vib_data_type *vib_ptr = pm_vib_get_data(pmic_chip);

    if ((vib_ptr == NULL) || (vib_ptr->periph_exists == FALSE))
    {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    if (periph_index >= vib_ptr->num_of_peripherals)
    {
      return PM_ERR_FLAG__INVALID_VIBRATOR_INDEXED;
    }

    reg_vib_voltage_ctl2 = vib_ptr->vib_reg_table->base_address + (vib_ptr->vib_reg_table->peripheral_offset*periph_index) + vib_ptr->vib_reg_table->voltage_ctl2;

    if (voltage < vib_ptr->vib_data->MinVoltage || 
        voltage > vib_ptr->vib_data->MaxVoltage)
    {
      err_flag = PM_ERR_FLAG__PAR3_OUT_OF_RANGE;
    }
    else
    {
       err_flag = vib_ptr->comm_ptr->WriteByteMask(vib_ptr->comm_ptr, reg_vib_voltage_ctl2, 0x1F, (pm_register_data_type)(voltage/100), 0); 
    }

    return err_flag;
}

pm_err_flag_type pm_vib_set_polarity
(
  unsigned          pmic_chip,
  pm_vib_which_type vib,
  pm_vib_pol_type   polarity
)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type reg_vib_en_ctl = 0;
    pm_register_address_type periph_index = (pm_register_address_type)vib;
    pm_vib_data_type *vib_ptr = pm_vib_get_data(pmic_chip);
    boolean inv_dtest;

    if ((vib_ptr == NULL) || (vib_ptr->periph_exists == FALSE))
    {
      return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    if (periph_index >= vib_ptr->num_of_peripherals)
    {
      return PM_ERR_FLAG__INVALID_VIBRATOR_INDEXED;
    }

    reg_vib_en_ctl = vib_ptr->vib_reg_table->base_address + (vib_ptr->vib_reg_table->peripheral_offset*periph_index) + vib_ptr->vib_reg_table->en_ctl;

    switch (polarity)
    {
      case PM_VIB_POL__ACTIVE_HIGH: inv_dtest = 0; break;
      case PM_VIB_POL__ACTIVE_LOW:  inv_dtest = 1; break;
      default:
        return PM_ERR_FLAG__PAR3_OUT_OF_RANGE;
    }

    err_flag = vib_ptr->comm_ptr->WriteByteMask(vib_ptr->comm_ptr, reg_vib_en_ctl, 0x10, (pm_register_data_type)(inv_dtest << 4), 0); 
  
    return err_flag;
}


pm_err_flag_type pm_vib_get_status
(
  unsigned             pmic_chip,
  pm_vib_which_type    vib,
  pm_vib_status_type  *status
)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type reg_vib_en_ctl = 0;
    pm_register_address_type reg_vib_voltage_ctl2 = 0;
    pm_register_data_type en_ctl_val;
    pm_register_data_type voltage_ctl2_val;
    pm_register_address_type periph_index = (pm_register_address_type)vib;
    pm_vib_data_type *vib_ptr = pm_vib_get_data(pmic_chip);

    if ((vib_ptr == NULL) || (vib_ptr->periph_exists == FALSE))
    {
        return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }

    if (periph_index >= vib_ptr->num_of_peripherals)
    {
      return PM_ERR_FLAG__INVALID_VIBRATOR_INDEXED;
    }

    reg_vib_en_ctl       = vib_ptr->vib_reg_table->base_address + (vib_ptr->vib_reg_table->peripheral_offset*periph_index) + vib_ptr->vib_reg_table->en_ctl;
    reg_vib_voltage_ctl2 = vib_ptr->vib_reg_table->base_address + (vib_ptr->vib_reg_table->peripheral_offset*periph_index) + vib_ptr->vib_reg_table->voltage_ctl2;

    err_flag  = vib_ptr->comm_ptr->ReadByte(vib_ptr->comm_ptr, reg_vib_en_ctl, &en_ctl_val, 0);
    err_flag |= vib_ptr->comm_ptr->ReadByte(vib_ptr->comm_ptr, reg_vib_voltage_ctl2, &voltage_ctl2_val, 0);

    status->enabled[PM_VIB_MODE__MANUAL] = (en_ctl_val & 0x80)? TRUE : FALSE;
    status->enabled[PM_VIB_MODE__DBUS1]  = (en_ctl_val & 0x1) ? TRUE : FALSE;
    status->enabled[PM_VIB_MODE__DBUS2]  = (en_ctl_val & 0x2) ? TRUE : FALSE;
    status->enabled[PM_VIB_MODE__DBUS3]  = (en_ctl_val & 0x4) ? TRUE : FALSE;

    status->voltage =  (voltage_ctl2_val & 0x1F) * 100;
    status->polarity = (en_ctl_val & 0x10) ? PM_VIB_POL__ACTIVE_LOW : PM_VIB_POL__ACTIVE_HIGH;

    return err_flag;
}

