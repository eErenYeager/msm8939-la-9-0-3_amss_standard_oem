/*! \file
*  
*  \brief  pm_app_rgb_led.c
*  \details Implementation file for rgb led resourece type.
*    
*  &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/app/rgb/src/pm_app_rgb.c#1 $ 
when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
04/25/14   pxm    Fix bug that LED is not blinking in 8916 during weak charging. CR648720
10/08/13   rk        No RGB support
07/15/13   aab     Added support for 8x10 ATC LED
03/13/13   aab     Creation
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_rgb.h"
#include "pm_lpg.h"
#include "pm_err_flags.h"
#include "pm_mpp.h"
#include "pm_app_chg.h"

pm_err_flag_type pm_rgb_led_config_8x26 (uint32 rgb_mask, uint8  dim_level, boolean  enable_rgb );

pm_err_flag_type pm_rgb_led_config
(
   unsigned                   device_index,
   pm_rgb_which_type          rgb_peripheral,
   uint32                     rgb_mask,
   pm_rgb_voltage_source_type source_type,
   uint8                      dim_level,
   boolean                    enable_rgb )
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_lpg_chan_type lpg_channel = PM_LPG_CHAN_5;
    pm_model_type pm_model;
    
    if(rgb_peripheral >= PM_RGB_INVALID)
    {
        err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
    }
    else if( !((rgb_mask & PM_RGB_SEGMENT_R) || (rgb_mask & PM_RGB_SEGMENT_G) || (rgb_mask & PM_RGB_SEGMENT_B)) )
    {
        err_flag = PM_ERR_FLAG__PAR3_OUT_OF_RANGE;
    }
    else if(source_type >= PM_RGB_VOLTAGE_SOURCE_INVALID)
    {
        err_flag = PM_ERR_FLAG__PAR4_OUT_OF_RANGE;
    }
    else if(dim_level > PM_RGB_DIM_LEVEL_MAX)
    {
        err_flag = PM_ERR_FLAG__PAR5_OUT_OF_RANGE;
    }
    else
    {
        pm_model = pm_get_pmic_model(device_index);
        
        if( (PMIC_IS_PM8916 == pm_model) || (PMIC_IS_PM8110 == pm_model) ) //If target is 8916 or 8110
        {
            return pm_chg_set_atc_led(device_index, enable_rgb);
        }
        if( PMIC_IS_PM8026 == pm_model ) //If target is 8x26 
        {
            err_flag = pm_rgb_led_config_8x26 (rgb_mask, dim_level, enable_rgb );  //Call 8x26 target specific LED configuration

            return err_flag;
        }
        else if( PMIC_IS_PM8941 == pm_model ) //If Target is MSM8974/MSM8974Pro AA/AB/AC
        {
	        err_flag = pm_rgb_set_voltage_source(device_index, rgb_peripheral,source_type ); //Select RGB voltage source
	        if(err_flag == PM_ERR_FLAG__SUCCESS)
	        {
	            if( enable_rgb == TRUE)
	            {
	                if(rgb_mask & PM_RGB_SEGMENT_R)
	                {
	                     lpg_channel = PM_LPG_CHAN_7;
	                }
	                else if(rgb_mask & PM_RGB_SEGMENT_G)
	                {
	                     lpg_channel = PM_LPG_CHAN_6;
	                }
	                else if(rgb_mask & PM_RGB_SEGMENT_B)
	                {
	                    lpg_channel = PM_LPG_CHAN_5;
	                }
	     

	                //LPG Configuration                            
	                err_flag = pm_lpg_pattern_config(device_index, lpg_channel, FALSE, FALSE, FALSE, FALSE, FALSE); //Configure for no pattern
	                if(err_flag != PM_ERR_FLAG__SUCCESS){ return err_flag;}

	                err_flag = pm_lpg_set_pwm_bit_size( device_index, lpg_channel,PM_LPG_PWM_7BIT );    //Configure 7 bit mode
	                if(err_flag != PM_ERR_FLAG__SUCCESS){ return err_flag;}

	                err_flag = pm_lpg_pwm_clock_sel( device_index, lpg_channel, PM_LPG_PWM_19_2_MHZ );  //Select 19.2 MHz clock
	                if(err_flag != PM_ERR_FLAG__SUCCESS){ return err_flag;}

	                err_flag = pm_lpg_pwm_set_pre_divide(device_index, lpg_channel, PM_LPG_PWM_PRE_DIV_5, PM_LPG_PWM_FREQ_EXPO_7 ); //Configure 390 Hz PWM frequency
	                if(err_flag != PM_ERR_FLAG__SUCCESS){ return err_flag;}

	                err_flag = pm_lpg_config_pwm_type(device_index, lpg_channel, FALSE, FALSE, FALSE, PM_LPG_PHASE_STAG_SHIFT_0_DEG);
	                if(err_flag != PM_ERR_FLAG__SUCCESS){ return err_flag;}

	                err_flag = pm_lpg_pwm_set_pwm_value( device_index, lpg_channel,dim_level );  //Configure DIM level
	                if(err_flag != PM_ERR_FLAG__SUCCESS){ return err_flag;}

	                err_flag = pm_lpg_pwm_enable(device_index, lpg_channel, TRUE ); //enable LPG
	                if(err_flag != PM_ERR_FLAG__SUCCESS){ return err_flag;}
	            }

	            err_flag = pm_rgb_enable(device_index, rgb_peripheral, rgb_mask, enable_rgb, FALSE); //Turn ON/OFF RGB LED
	        }
    	}
        else
        {
            err_flag = PM_ERR_FLAG__PMIC_NOT_SUPPORTED; //not supported
        }
    }
    return err_flag;
}




pm_err_flag_type pm_rgb_led_config_8x26 (uint32 rgb_mask, uint8  dim_level, boolean  enable_rgb )
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_mpp_i_sink_level_type isink_level  = PM_MPP__I_SINK__LEVEL_5mA;
    pm_mpp_perph_index which_mpp = PM_MPP_6;

    if(rgb_mask & PM_RGB_SEGMENT_R)
    {
        which_mpp = PM_MPP_6;  //For RED LED
    }
    else if(rgb_mask & PM_RGB_SEGMENT_G)
    {
        which_mpp = PM_MPP_4;  //For GREEN LED
    }
    else
    {
        //Blue LED is NOT supported on 8x26
        err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
    }
    if(err_flag != PM_ERR_FLAG__SUCCESS){ return err_flag;}


   
    switch(dim_level)  //Match Dim level configuration
    {
        case PM_RGB_DIM_LEVEL_MIN:
            isink_level = PM_MPP__I_SINK__LEVEL_5mA;
            break;
        case PM_RGB_DIM_LEVEL_LOW:
                isink_level = PM_MPP__I_SINK__LEVEL_10mA;
            break;
        case PM_RGB_DIM_LEVEL_MID:
                isink_level = PM_MPP__I_SINK__LEVEL_20mA;
            break;
        case PM_RGB_DIM_LEVEL_HIGH:
                isink_level = PM_MPP__I_SINK__LEVEL_30mA;
            break;
        case PM_RGB_DIM_LEVEL_MAX:
                isink_level = PM_MPP__I_SINK__LEVEL_40mA;
            break;
        default:
            err_flag |= PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
            break;
    }

    if(err_flag != PM_ERR_FLAG__SUCCESS){ return err_flag;}


    //Program MPP to turn ON/OFF LED
    if(enable_rgb)
    {
        err_flag = pm_mpp_config_i_sink(0, which_mpp, isink_level, PM_MPP__I_SINK__SWITCH_ENA);
    }
    else
    {
        err_flag = pm_mpp_enable(0, which_mpp, FALSE); 
    }

    return err_flag;
}

