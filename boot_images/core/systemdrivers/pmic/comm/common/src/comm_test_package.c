/*! \file 
 *  \n
 *  \brief  CommTestPackage.cpp ---- CommTestPackage RELATED DEFINITION
 *  \n
 *  \n This header file contains class implementation of the CommTestPackage
 *  \n 
 *  \n
 *  \n &copy; Copyright 2009 Qualcomm Technologies Incorporated, All Rights Reserved
 */
/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/comm/common/src/comm_test_package.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
1/25/2010  wra     File created

========================================================================== */

/*===========================================================================

                           INCLUDE FILES FOR MODULE

===========================================================================*/

#include "comm_test_package.h"    /* CommTestPackage class definition file               */

//namespace PmicObjects
//{
//   namespace COMM
//   {
      //CommTestPackage::CommTestPackage(PmicDevices::DeviceType deviceType, uint32 busIndex, CommType commType)
      //{
      //}

      //PmicDevices::DeviceType CommTestPackage::GetDeviceType()
      //{
      //   return mDeviceType;
      //}
//   }
//}
