/*! \file comm_info.c
*  \n
*  \brief  Comm Info related function definitions
*  \n
*  \n This file contains CommInfo initialization function definitions.
*  \n
*  \n &copy; Copyright 2009-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/comm/common/src/comm_info.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/15/13   aab     Fixed KW error
03/20/12   hs      Added a member vailable for DAL SPMI handle.       
08/19/11   jtn     Fix Klocwork warnings, refactor spelling error
12/08/10   wra     Optimizing for RPM footprint 
11/15/09   wra     File created

========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "comm_info.h"
#include "pm_utils.h"

/*=========================================================================
      Function Definitions
==========================================================================*/

void pmiC_CommInfo_Init(pmiC_CommInfo *commInfo, pmiC_DeviceInfo *deviceInfo, CommType commType, uint8 slaveIdIndex)
{
    commInfo->mDeviceInfo = NULL;
    commInfo->mDeviceInitialized = FALSE;
    commInfo->mCommType = commType;
    commInfo->mBusIndex = 0;

    /* Each PMIC device has 2 slave ids so slave id index can be only 0 and 1 */
    if((deviceInfo == NULL) || (slaveIdIndex > 1))
    {
        return;
    }

    commInfo->mDeviceInfo = deviceInfo;
    commInfo->mDeviceInitialized = TRUE;

    /* Slave Id index is 0 for PMIC device Primary Slave Id
       Slave Id index is 1 for PMIC device Secondary Slave Id */
    commInfo->mBusIndex = deviceInfo->mSlaveId[slaveIdIndex];
}


/* Deep copy constructor */
void pmiC_CommInfo_InitCopy(pmiC_CommInfo *commInfo, const pmiC_CommInfo *rhs)
{
	commInfo->mDeviceInfo = NULL;
    commInfo->mBusIndex = rhs->mBusIndex;
    commInfo->mCommType = rhs->mCommType;

    if(commInfo->mDeviceInfo == NULL)
    {
        pm_malloc(sizeof(pmiC_DeviceInfo), (void**)&(commInfo->mDeviceInfo));
    }

    commInfo->mDeviceInfo->mDeviceType = rhs->mDeviceInfo->mDeviceType;
    commInfo->mDeviceInfo->mMaxRegisterSize = rhs->mDeviceInfo->mMaxRegisterSize;
    commInfo->mDeviceInfo->mPmicModel = rhs->mDeviceInfo->mPmicModel;
    commInfo->mDeviceInfo->mPmicAllLayerRevision = rhs->mDeviceInfo->mPmicAllLayerRevision;
    commInfo->mDeviceInfo->mDeviceIndex = rhs->mDeviceInfo->mDeviceIndex;
    commInfo->mDeviceInfo->mSlaveId[0] = rhs->mDeviceInfo->mSlaveId[0];
    commInfo->mDeviceInfo->mSlaveId[1] = rhs->mDeviceInfo->mSlaveId[1];

    commInfo->mDeviceInitialized = TRUE;
}
