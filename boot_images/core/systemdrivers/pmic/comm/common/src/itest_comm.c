/*! \file 
*  \n
*  \brief  pmiC_IComm.cpp ---- pmiC_IComm RELATED DEFINITION
*  \n
*  \n This header file contains class implementation of the pmiC_IComm
*  \n messaging interface
*  \n
*  \n &copy; Copyright 2009 Qualcomm Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/comm/common/src/itest_comm.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/15/12   hs     File created

========================================================================== */

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "itest_comm.h"    /* pmiC_IComm class definition file   */

/*************************************
NOTE: Initializer IMPLEMENTATION
**************************************/
void pmiC_ITestComm_Init(pmiC_ITestComm *me, pmiC_IComm  **innerComm, CommType  type)
{
    pmiC_IComm_Init((pmiC_IComm*)me, innerComm, type);    
}

