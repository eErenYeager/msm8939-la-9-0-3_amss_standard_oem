<driver name="NULL">
<device id="/boot/pmic/common">
<props id=25 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
gpio_reg
</props>
<props id=26 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
mpp_reg
</props>
<props id=28 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
lpg_reg
</props>
<props id=29 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
vib_reg
</props>
<props id=53 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
smbb_reg
</props>
<props id=50 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
lbc_reg
</props>
<props id=2 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
smps_reg 
</props>
<props id=3 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
ldo_reg
</props>
<props id=22 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
clk_sleep_reg
</props>
<props id=23 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
clk_xo_reg
</props>
<props id=24 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
clk_common
</props>
<props id=31 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
pbs_client_reg 
</props>
<props id=9 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
nmos_volt
</props>
<props id=12 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
pmos_volt 
</props>
<props id=17 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
nmos_range 
</props>
<props id=20 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
pmos_range 
</props>
<props id=44 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
ult_buck_volt_1
</props>
<props id=43 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
ult_buck_range_1
</props>
<props id=46 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
ult_buck_volt_2
</props>
<props id=45 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
ult_buck_range_2
</props>
<props id=47 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
clk_ldo_range 
</props>
<props id=48 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
clk_ldo_volt 
</props>
<props id=1 type=DALPROP_ATTR_TYPE_STRUCT_PTR>
clk_reg 
</props>
</device> 
</driver>
