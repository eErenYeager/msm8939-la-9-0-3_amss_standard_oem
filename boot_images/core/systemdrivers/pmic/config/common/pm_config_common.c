/*! \file
 *  
 *  \brief  pm_common_config.c ----This file contains peripheral related settings that are common across PMICs.
 *  \details This file contains peripheral related settings that are common across PMICs.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Resource Setting Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Software Register Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Processor Allocation Information Version: VU.Please Provide Valid Label - Not Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2013 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/config/common/pm_config_common.c#1 $ 

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
01/15/14   rk      Updated the smps_reg struct with SEC_ACCESS reg address.
10/08/13   rk      New voltage ranges for ULT bucks
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_gpio_driver.h"
#include "pm_mpp_driver.h"
#include "pm_rgb_driver.h"
#include "pm_lpg_driver.h"
#include "pm_vib_driver.h"
#include "pm_smbb_driver.h"
#include "pm_lbc_driver.h"
#include "pm_pwr_alg.h"
#include "pm_clk_driver.h"
#include "pm_pbs_client_driver.h"



pm_pwr_range_info_type ult_buck_range_1[1] = 
{
    {0, 1}
};
pm_pwr_volt_info_type ult_buck_volt_1[2] = 
{
    { 375000,   1562500,         12500},  //ULT Buck1,2 3 - Range 0
    { 750000,   1525000,         25000},  //ULT Buck1,2 3    Range 1
};

pm_pwr_range_info_type ult_buck_range_2[1] = 
{
    {0, 0}
};
pm_pwr_volt_info_type ult_buck_volt_2[1] = 
{
    { 1550000,   3125000,         25000},  //ULT Buck 4
};

pm_pwr_range_info_type pmos_range[1] = 
{
    {0, 0}
};
pm_pwr_volt_info_type pmos_volt[1] = 
{
    { 1750000,  3337500,         12500},  //same for P50, P150, P300 and P600 PLDO

};
pm_pwr_range_info_type nmos_range[1] = 
{
    {0, 0}
};
pm_pwr_volt_info_type nmos_volt[1] = 
{
    { 375000,   1537500,         12500},  //same for N300, N600 and N1200 NLDO
};

pm_pwr_range_info_type clk_ldo_range[1] = 
{
    {0, 1}
};
pm_pwr_volt_info_type clk_ldo_volt[2] = 
{
    { 1380000,  2220000,         120000},//CLK LDO for VREG_XO and VREG_RF
    { 690000,   1110000,         60000},
};

pm_clk_register_info_type clk_reg[1] = 
{
    {0x5000, 0x100, 0x044, 0x046, 0x048}
};

pm_clk_sleep_register_info_type clk_sleep_reg[1] = 
{
//  1.smp_ctl1 - 2.cal_rc4
    {0x048, 0x05B}
};

pm_clk_xo_register_info_type clk_xo_reg[1] =
{
//	1.model_ctl1 - 2.xo_adj_fine
	{0x044, 0x05C}
};

pm_pbs_client_register_info_type pbs_client_reg[1] = 
{
    {0x7100, 0x100, 0x0010, 0x0011, 0x0012, 0x0013, 0x0014, 0x0015, 0x0016, 0x0018, 0x0019, 0x0040, 0x0046}
};

pm_clk_type clk_common[PM_MAX_NUM_CLKS] = 
{
   PM_CLK_XO,
   PM_CLK_BB_1, 
   PM_CLK_BB_2, 
   PM_CLK_INVALID, 
   PM_CLK_RF_1, 
   PM_CLK_RF_2, 
   PM_CLK_RF_3, 
   PM_CLK_DIFF_1, 
   PM_CLK_LN_BB, 
   PM_CLK_DIST, 
   PM_CLK_SLEEP, 
   PM_CLK_DIV_1, 
   PM_CLK_DIV_2, 
   PM_CLK_DIV_3,
};



pm_gpio_register_info_type gpio_reg[1] = 
{
    {0xC000, 0x100, 0x008, 0x010, 0x011, 0x012, 0x013, 0x014, 0x015, 0x016, 0x018, 0x019, 0x01A, 0x01B, 0x040, 0x042, 0x043, 0x045, 0x041, 0x046}
};

pm_mpp_register_info_type mpp_reg[1] = 
{
    {0xA000, 0x100, 0x008, 0x010, 0x011, 0x012, 0x013, 0x014, 0x015, 0x016, 0x018, 0x019, 0x01A, 0x01B, 0x040, 0x00, 0x043, 0x041, 0x046, 0x04C, 0x04A, 0x048}
};

pm_pwr_register_info_type smps_reg[1] = 
{
    {0x1400, 0x300, 0x008, 0x045, 0x048, 0x046, 0x000, 0x041, 0x000, 0x04A, 0x050, 0x000, 0x060, 0x000, 0x005, 0x0D0}
};

pm_pwr_register_info_type ldo_reg[1] = 
{
    {0x4000, 0x100, 0x008, 0x045, 0x048, 0x046, 0x040, 0x041, 0x000, 0x000, 0x000, 0x000, 0x04C, 0x000, 0x005, 0x0D0}
};
pm_lpg_register_info_type lpg_reg[1] = 
{
    {0xB000, 0x100, 0x040, 0x041, 0x042, 0x043, 0x044, 0x045, 0x046, 0x050, 0x051, 0x052, 0x053, 0x054, 0x055, 0x056, 0x057,0x040,0x041,0x0C8}
};

pm_vib_register_info_type vib_reg[1] = 
{
    {0xC000, 0x100, 0x008, 0x041, 0x046}
};

pm_smbb_register_info_type smbb_reg[1] = 
{
   {0x1000, 0x100, 0x00B, 0x044, 0x049, 0x04B, 0x04D, 0x050, 0x052, 0x054, 0x064, 0x0D0, 0x0EE, 0x008, 0x09, 0x010,0x043, 0x044, 0x051, 0x00A, 0x042}
};

pm_lbc_register_info_type lbc_reg[1] = 
{
   {0x1000, 0x100, 0x008, 0x040, 0x041, 0x044, 0x045, 0x047, 0x049, 0x04D, 0x050, 0x052, 0x055, 0x064, 0x065, 0x0D0, 0x0EE, 0x008, 0x09, 0x047, 0x04E, 0x042}
};

