/*! \file
 *  
 *  \brief  
 *   pm_config_registers.c
 *  \details 
 *   This file contains customizable target specific 
 *   driver settings & PMIC registers. This file is generated from database functional
 *   configuration information that is maintained for each of the targets.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Resource Setting Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Software Register Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Processor Allocation Information Version: VU.Please Provide Valid Label - Not Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2010-2013,2014 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/config/msm8929/pm_config_target.c#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/19/15   mr      Updated 8929 target specific configurations (CR-783265)
11/14/14   sk      Added 8929 target specific configurations
05/17/14   rk      Modify XPU permission and Int Ownership for PMIC Peripherals (CR - 666063)
12/19/13   rk      Added spmi channel config setings
10/08/13   rk      Added target specific configurations
07/15/13   aab     Added support for SMBB driver
05/09/13   aab     Updated MPP driver support 
03/26/13   aab     Updated GPIO driver support  
03/07/13   aab     Added VIB driver support
01/18/13   aab     Cleanup
01/09/13   aab     Added support for sleep clk
12/06/12   hw      Rearchitecturing module driver to peripheral driver
08/07/12   hs      Added support for 5V boost.

===========================================================================*/
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include"pm_gpio_driver.h"
#include"pm_mpp_driver.h"
#include"pm_clk_driver.h"
#include"pm_vib_driver.h"
#include"pm_smbb_driver.h"
//#include "PmicArbCfg.h"
#include "SpmiCfg.h"


 /************************************************ DRIVER DATA *********************************/
/*
 * pm_periph_intr_owner
 *
 * A type to choose the interrupt owner for the peripheral.
 */
typedef enum {
  PM_APPS_HLOS_OWNER = 0,
  PM_APPS_TZ_OWNER,
  PM_MSS_OWNER,
  PM_LPASS_OWNER,
  PM_RPM_OWNER,
  PM_WCONNECT_OWNER
} pm_periph_intr_owner;


//===== LUT ====================================================

unsigned smps_phase_lut_a[ ]= {0,0,0,0};


pm_mpp_specific_info_type
mpp_specific[1] =
{
   {0x9E0, 4}
};

pm_vib_specific_data_type
vib_specific[1] = 
{
  {1200, 3100}
};

uint16 chg_app_ds[] = 
{
   3400,  // PM_CHG_FLCB_OS_BOOT_UP_THRESHOLD
   3206,  // PM_CHG_FLCB_WEAK_BATTERY_THRESHOLD, in the unit of mv
      0,  // PM_CHG_FLCB_FAST_CHG_THRESHOLD, not applicable       
   2796,  // PM_CHG_FLCB_DEAD_BATTERY_THRESHOLD, in the unit of mv
      0,  // PM_CHG_DC_CHARGER_IMAX, not applicable
   1440,  // PM_CHG_IMAX_DEFAULT
   1440,  // PM_CHG_USB_IDEV_DCHG
   1440,  // PM_CHG_USB_IDEV_FLOAT
    500,  // PM_CHG_USB_IDEV_SDP
     90,  // PM_CHG_USB_IUNIT
   4775,  // PM_CHG_VBAT_MAX_MAX
   4000,  // PM_CHG_VBAT_MAX_MIN
     25,  // PM_CHG_VBAT_MAX_STEP
   1440,  // PM_CHG_IBAT_MAX_MAX
     90,  // PM_CHG_IBAT_MAX_MIN
     90,  // PM_CHG_IBAT_MAX_STEP
   1440,  // PM_CHG_IBAT_SAFE
   4200,  // PM_CHG_VIN_MIN
   2500,  // PM_CHG_VBAT_TRKL_MIN_THRESHOLD, in the unit of mv
   2985,  // PM_CHG_VBAT_TRKL_MAX_THRESHOLD, in the unit of mv
   3000,  // PM_CHG_VBAT_WEAK_MIN_THRESHOLD, in the unit of mv
   3582,  // PM_CHG_VBAT_WEAK_MAX_THRESHOLD, in the unit of mv
   3500,  // PM_CHG_FLCB_OS_BOOTUP_AFTER_USB_SUSPEND_THRESHOLD.  Used when PC is in suspend state to insure that the next boot up can make it to UEFI/LK
      0   // PM_CHG_PMIC_DEVICE_INDEX
};



/*-------------------------------------------------------------------------
* Static Variable Definitions
* ----------------------------------------------------------------------*/

const SpmiCfg_ChannelCfg  pm_spmi_periph_cfg_pm8936[] =
{

    {0xF, 0x00, 116, PM_APPS_TZ_OWNER},     /* GID            */
    {0x2, 0x06, 57, PM_RPM_OWNER},         /* SPMI_OPTIONS   */
    {0x1, 0xF3, 24,  PM_APPS_HLOS_OWNER},   /* CDC_NCP_FREQ    */
    {0x1, 0xF2, 23,  PM_APPS_HLOS_OWNER},   /* CDC_BOOST_FREQ    */
    {0x1, 0xF1, 22,  PM_APPS_HLOS_OWNER},   /* CDC_A    */
    {0x1, 0xF0, 21,  PM_APPS_HLOS_OWNER},   /* CDC_D    */
    {0x1, 0xC0, 20,  PM_APPS_HLOS_OWNER},   /* VIB1     */
    {0x1, 0xBC, 19,  PM_APPS_HLOS_OWNER},   /* PWM_3D         */
    {0x1, 0x53, 56, PM_RPM_OWNER},         /* LDO20          */
    {0x1, 0x52, 55, PM_RPM_OWNER},         /* LDO19          */
    {0x1, 0x51, 54, PM_RPM_OWNER},         /* LDO18          */
    {0x1, 0x50, 60, PM_RPM_OWNER},         /* LDO17          */
    {0x1, 0x4F, 86, PM_RPM_OWNER},         /* LDO16          */
    {0x1, 0x4E, 85, PM_RPM_OWNER},         /* LDO15          */
    {0x1, 0x4D, 84, PM_RPM_OWNER},         /* LDO14          */
    {0x1, 0x4C, 59, PM_RPM_OWNER},         /* LDO13          */
    {0x1, 0x4B, 53, PM_RPM_OWNER},         /* LDO12          */
    {0x1, 0x4A, 52, PM_RPM_OWNER},         /* LDO11          */
    {0x1, 0x49, 51, PM_RPM_OWNER},         /* LDO10          */
    {0x1, 0x48, 50, PM_RPM_OWNER},         /* LDO9           */
    {0x1, 0x47, 49, PM_RPM_OWNER},         /* LDO8           */
    {0x1, 0x46, 48, PM_RPM_OWNER},         /* LDO7           */
    {0x1, 0x45, 58, PM_RPM_OWNER},         /* LDO6           */
    {0x1, 0x44, 47, PM_RPM_OWNER},         /* LDO5           */
    {0x1, 0x43, 46, PM_RPM_OWNER},         /* LDO4           */
    {0x1, 0x42, 45, PM_RPM_OWNER},         /* LDO3           */
    {0x1, 0x41, 44, PM_RPM_OWNER},         /* LDO2           */
    {0x1, 0x40, 43, PM_RPM_OWNER},         /* LDO1           */
    {0x1, 0x1F, 66, PM_RPM_OWNER},         /* S4_FREQ        */
    {0x1, 0x1E, 65, PM_RPM_OWNER},         /* S4_PS          */
    {0x1, 0x1D, 64, PM_RPM_OWNER},         /* S4_CTRL        */
    {0x1, 0x1C, 63, PM_RPM_OWNER},         /* S3_FREQ        */
    {0x1, 0x1B, 62, PM_RPM_OWNER},         /* S3_PS          */
    {0x1, 0x1A, 61, PM_RPM_OWNER},         /* S3_CTRL        */
    {0x1, 0x19, 18 , PM_APPS_HLOS_OWNER},         /* S2_FREQ        */
    {0x1, 0x18, 17 , PM_APPS_HLOS_OWNER},         /* S2_PS          */
    {0x1, 0x17, 16 , PM_APPS_HLOS_OWNER},         /* S2_CTRL        */
    {0x1, 0x16, 68,  PM_RPM_OWNER},         /* S1_FREQ        */
    {0x1, 0x15, 67, PM_RPM_OWNER},         /* S1_PS          */
    {0x1, 0x14, 87, PM_RPM_OWNER},         /* S1_CTRL        */
    {0x1, 0x10, 42,  PM_RPM_OWNER},   /* BCLK_GEN_MAIN  */
    {0x0, 0xFE, 115, PM_RPM_OWNER},         /* TRIM           */
    {0x0, 0xC3, 98 , PM_APPS_HLOS_OWNER},   /* GPIO4          */
    {0x0, 0xC2, 97 , PM_APPS_HLOS_OWNER},   /* GPIO3          */
    {0x0, 0xC1, 96,  PM_WCONNECT_OWNER},   /* GPIO2          */
    {0x0, 0xC0, 95, PM_MSS_OWNER},         /* GPIO1          */
    {0x0, 0xA3, 94 , PM_APPS_HLOS_OWNER},   /* MPP4           */
    {0x0, 0xA2, 93 , PM_APPS_HLOS_OWNER},   /* MPP3           */
    {0x0, 0xA1, 92 , PM_APPS_HLOS_OWNER},   /* MPP2           */
    {0x0, 0xA0, 91 , PM_APPS_HLOS_OWNER},   /* MPP1           */
    {0x0, 0x77, 83 , PM_APPS_HLOS_OWNER},   /* PBS_CLIENT6    */
    {0x0, 0x76, 82 , PM_APPS_HLOS_OWNER},   /* PBS_CLIENT5    */
    {0x0, 0x75, 81 , PM_APPS_HLOS_OWNER},   /* PBS_CLIENT4    */
    {0x0, 0x74, 80, PM_MSS_OWNER},         /* PBS_CLIENT3    */
    {0x0, 0x73, 41 , PM_APPS_HLOS_OWNER},   /* PBS_CLIENT2    */
    {0x0, 0x72, 40 , PM_APPS_HLOS_OWNER},   /* PBS_CLIENT1    */
    {0x0, 0x71, 39 , PM_APPS_HLOS_OWNER},   /* PBS_CLIENT0    */
    {0x0, 0x70, 38 , PM_RPM_OWNER},         /* PBS_CORE       */
    {0x0, 0x62, 15 , PM_APPS_HLOS_OWNER},   /* RTC_TIMER      */
    {0x0, 0x61, 14 , PM_APPS_HLOS_OWNER},   /* RTC_ALARM      */
    {0x0, 0x60, 79, PM_MSS_OWNER},         /* RTC_WR         */
    {0x0, 0x5A, 37 , PM_RPM_OWNER},   /* SLEEP_CLK1     */
    {0x0, 0x59, 36 , PM_RPM_OWNER},   /* CLK_DIST       */
    {0x0, 0x55, 75 , PM_RPM_OWNER},   /* RF_CLK2        */
    {0x0, 0x54, 74 , PM_RPM_OWNER},   /* RF_CLK1        */
    {0x0, 0x52, 35 , PM_RPM_OWNER},   /* BB_CLK2        */
    {0x0, 0x51, 34,  PM_RPM_OWNER},   /* BB_CLK1        */
    {0x0, 0x50, 78, PM_MSS_OWNER},   /* XO             */
    {0x0, 0x40, 13 , PM_APPS_HLOS_OWNER},   /* BMS_VM       */
    {0x0, 0x35, 12 , PM_APPS_HLOS_OWNER},   /* VADC4_LC_VBAT        */
    {0x0, 0x34, 11 , PM_APPS_HLOS_OWNER},   /* VADC2_BTM      */
    {0x0, 0x33, 10, PM_MSS_OWNER},         /* VADC3_LC_VBMS      */
    {0x0, 0x32, 77,  PM_RPM_OWNER},         /* VADC3_MDM      */
    {0x0, 0x31, 9 , PM_APPS_HLOS_OWNER},   /* VADC1_USR      */
    {0x0, 0x2C, 33,  PM_RPM_OWNER},   /* MBG1           */
    {0x0, 0x28, 8  , PM_APPS_HLOS_OWNER},   /* COIN           */
    {0x0, 0x24, 7  , PM_APPS_HLOS_OWNER},   /* TEMP_ALARM     */
    {0x0, 0x1C, 76, PM_MSS_OWNER},         /* BUA_EXT_CHARGER*/
    {0x0, 0x16, 6  , PM_APPS_HLOS_OWNER},   /* LBC_MISC      */
    {0x0, 0x13, 5  , PM_APPS_HLOS_OWNER},   /* LBC_USB      */
    {0x0, 0x12, 4  , PM_APPS_HLOS_OWNER},   /* LBC_BAT_IF      */
    {0x0, 0x10, 3  , PM_APPS_HLOS_OWNER},   /* LBC_CHGR      */
    {0x0, 0x0A, 2  , PM_APPS_HLOS_OWNER},   /* VREFLPDDR      */
    {0x0, 0x09, 0,   PM_RPM_OWNER},   /* MISC           */
    {0x0, 0x08, 1  , PM_APPS_HLOS_OWNER},   /* PON            */
    {0x0, 0x06, 114,  PM_RPM_OWNER},         /* SPMI           */
    {0x0, 0x05, 113, PM_RPM_OWNER},         /* INT            */
    {0x0, 0x04, 112, PM_RPM_OWNER},         /* BUS            */
    {0x0, 0x01, 111, PM_RPM_OWNER},         /* REVID          */
};

/* Number of peripherals. */
const uint32 pm_num_periph_pm8936 = sizeof(pm_spmi_periph_cfg_pm8936) / sizeof(SpmiCfg_ChannelCfg);


