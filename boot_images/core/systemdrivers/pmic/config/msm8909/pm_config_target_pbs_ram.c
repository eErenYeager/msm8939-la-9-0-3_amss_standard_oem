/*! \file  pm_config_target_pbs_ram.c
 *  
 *  \brief  File Contains the PMIC Set Mode Driver Implementation
 *  \details Set Mode Driver implementation is responsible for setting and getting 
 *  all mode settings such as Register values, memory values, etc.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Locked Version: PM8916-x.x-OTPx.x-MSM8916-05262014v2 - Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2014 Qualcomm Technologies, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/config/msm8909/pm_config_target_pbs_ram.c#1 $ 
$DateTime: 2015/03/19 01:58:37 $  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "pm_target_information.h"
#include "pm_config_sbl.h"
#include "pm_pbs_driver.h"

/*========================== PBS RAM LUT =============================*/

// To handle multiple PBS RAM configuration for different rev of the same PMIC or for multiple PMICs,
// a double dimension array of PBS RAM data is used. The data field of the specific command (in pm_config_target_sbl_sequence.c)
// to program PBS RAM will hold the index to the PBS RAM that needs to be used. during programming.
// Example:
// 1.sid; 2.data; 3.base_address; 4.offset;  5.reg_operation; 6.rev_id_operation; 7.rev_id;
// {0, 0x00, 0x0000, 0x000, PM_SBL_PBS_RAM, EQUAL, REV_ID_2_0},  //data = 0:  Use the 1st set of PBS RAM data if PMIC Rev ID = REV_ID_2_0
// {0, 0x01, 0x0000, 0x000, PM_SBL_PBS_RAM, EQUAL, REV_ID_1_0},  //data = 1:  Use the 2nd set of PBS RAM data if PMIC Rev ID = REV_ID_1_0

pm_pbs_ram_data_type
pm_pbs_seq [ ][PBS_RAM_DATA_SIZE] =
{
   // PBS_RAM_MSM8916.PMIC.HW.PM8916_2p0_1_0_0
   {
      //data  offset  base_addr  sid
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#0	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#1	-	
      { 0xE4,	0x04,	0x00,	0x80},	// W#2	-	POFF PBS-RAM Trigger
      { 0x24,	0x04,	0x00,	0x80},	// W#3	-	BUA PBS-RAM Trigger
      { 0x6C,	0x04,	0x00,	0x80},	// W#4	-	Sleep_Wake PBS-RAM trigger
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#5	-	
      { 0xC4,	0x04,	0x00,	0x80},	// W#6	-	WARM_RESET PBS-RAM Trigger
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#7	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#8	-	
      { 0x00,	0x18,	0x1C,	0x40},	// W#9	-	Read BUA_4UICC_INT_LATCHED_STS in BUA
      { 0x01,	0x01,	0x01,	0x90},	// W#10	-	Check within value of Data (in Read-Buffer): Bit-0 for BATT_ALARM. Skip Next line if Value is EQUAL to 0x01
      { 0x3C,	0x04,	0x00,	0x80},	// W#11	-	Read Data != 0x01 => NO BATT_GONE Alarm. Jump to the code that will check for each UIM Removal Status individually
      { 0x58,	0x04,	0x4D,	0x89},	// W#12	-	BATT_GONE Alarm Detected. Disable LDO14 - UIM1
      { 0x58,	0x04,	0x4E,	0x89},	// W#13	-	BATT_GONE Alarm Detected. Disable LDO15 - UIM2
      { 0x58,	0x04,	0x4F,	0x89},	// W#14	-	BATT_GONE Alarm Detected. Disable LDO16 - UIM3
      { 0x02,	0x00,	0x02,	0x90},	// W#15	-	Check within value of Data (in Read-Buffer): Bit-1 for UICC1_ALARM. Skip Next line if Value is NOT EQUAL to 0x02
      { 0x58,	0x04,	0x4D,	0x89},	// W#16	-	UICC1_ALARM Detected. Disable UIM1 LDO14
      { 0x04,	0x00,	0x04,	0x90},	// W#17	-	Check within value of Data (in Read-Buffer): Bit-2 for UICC2_ALARM. Skip Next line if Value is NOT EQUAL to 0x04
      { 0x58,	0x04,	0x4E,	0x89},	// W#18	-	UICC2_ALARM Detected. Disable UIM2 LDO15
      { 0x08,	0x00,	0x08,	0x90},	// W#19	-	Check within value of Data (in Read-Buffer): Bit-3 for UICC3_ALARM. Skip Next line if Value is NOT EQUAL to 0x08
      { 0x58,	0x04,	0x4F,	0x89},	// W#20	-	UICC3_ALARM Detected. Disable UIM3 LDO16
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#21	-	EOS (End of BUA_RAM_SEQ)
      { 0xA5,	0xD0,	0x00,	0x04},	// W#22	-	Unlock SEC_ACCESS
      { 0x01,	0xDB,	0x00,	0x04},	// W#23	-	Perform LOCAL_SOFT_RESET on LDO (to reset all other voltage settings in addition to disabling it)
      { 0x00,	0xFF,	0xFF,	0x8C},	// W#24	-	Return to the calling line
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#25	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#26	-	
      { 0x00,	0x8A,	0x08,	0x40},	// W#27	-	Read PON:AVDD_VPH Register to determine if we are Entering-into or Exiting-from Sleep
      { 0x20,	0x01,	0x20,	0x90},	// W#28	-	IF AVDD_VPH:HPM_EN (Bit 5) == 1 => System is Awake => SKIP Next Line to ENTER_SLEEP
      { 0x9C,	0x04,	0x00,	0x80},	// W#29	-	Goto: BEGIN_RAM_WAKE_SEQ
      { 0x01,	0x46,	0x51,	0x00},	// W#30	-	Configure BBCLK1 to (only) follow BBCLK1_EN (CXO_EN) Pin-Control
      { 0x80,	0x46,	0x46,	0x01},	// W#31	-	Enable LDO7 (Preserved if already ON, Turned-ON if previously OFF)
      { 0x00,	0x45,	0x14,	0x01},	// W#32	-	Force PFM Mode on S1
      { 0x00,	0x45,	0x1A,	0x01},	// W#33	-	Force PFM Mode on S3
      { 0x00,	0x45,	0x1D,	0x01},	// W#34	-	Force PFM Mode on S4
      { 0x02,	0x40,	0x50,	0x00},	// W#35	-	CR-0000163990: Force VMUX to LDO5 (SW-Workaround)
      { 0x11,	0x40,	0xA0,	0x00},	// W#36	-	Change MPP1 Mode from 1.25V Analog Output to Digital Output Mode (for low power consumption) [DIG_VIN already configured for 1.2V in SBL]
      { 0x00,	0x8A,	0x08,	0x00},	// W#37	-	Put AVDD Regulator into LPM
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#38	-	EOS (End on RAM_SLEEP_SEQ)
      { 0x00,	0x40,	0x50,	0x00},	// W#39	-	CR-0000163990: Force VMUX back to Auto (SW-Workaround)
      { 0x81,	0x46,	0x51,	0x00},	// W#40	-	Configure BBCLK1 to also get SW-Enabled (it has already been enabled by BBCLK1_EN (CXO_EN) Pin that got asserted-high before starting Wake-Up)
      { 0x20,	0x8A,	0x08,	0x00},	// W#41	-	Put AVDD Regulator back to HPM
      { 0x51,	0x40,	0xA0,	0x00},	// W#42	-	Revert MPP1 Mode from 1.20V Digital Output to 1.25V Analog Output Mode [DIG_VIN already configured for 1.2V in SBL]
      { 0x80,	0x45,	0x14,	0x01},	// W#43	-	Force PWM Mode on S1
      { 0x80,	0x45,	0x1A,	0x01},	// W#44	-	Force PWM Mode on S3
      { 0x80,	0x45,	0x1D,	0x01},	// W#45	-	Force PWM Mode on S4
      { 0x80,	0x46,	0x46,	0x01},	// W#46	-	Enable LDO7 (Preserved if already ON) - line ideally not necessary
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#47	-	EOS (End of RAM_WAKE_SEQ)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#48	-	
      { 0x00,	0x08,	0x01,	0x40},	// W#49	-	REVID_STATUS1: Check for Option-2 Pin
      { 0x00,	0x00,	0x0C,	0x90},	// W#50	-	Skip Next Line IF OPT2 != 0 i.e. Data is NOT EQUAL to 0x00 (mask 0x0C)
      { 0x11,	0x40,	0xC3,	0x00},	// W#51	-	If OPT2 ==0 => Ext S5 APC Buck Present. Configure GPIO4 as Digital Output with HIGH (Default Dig VIN0)
      { 0x54,	0x03,	0x01,	0xC0},	// W#52	-	Wait for 26ms (= 852 cycles of 32kHz clock)
      { 0x40,	0x91,	0x08,	0x00},	// W#53	-	Write Sequence Complete ACK to PON
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#54	-	EOS (Finish Warm-Reset-Sequence)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#55	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#56	-	
      { 0x29,	0xB2,	0xF1,	0x01},	// W#57	-	CDC_A_SPKR_DRV_CTL: Disable SPKR Drive from Tombak-Analog Codec Module
      { 0x1F,	0xC3,	0xF1,	0x01},	// W#58	-	CDC_A_BOOST_EN_CTL: Disable Boost Regulator from Tombak-Analog Codec Module
      { 0x00,	0x48,	0x5A,	0x00},	// W#59	-	Disable SMPL
      { 0x54,	0x05,	0x4A,	0x89},	// W#60	-	[19] Disable LDO11
      { 0x54,	0x05,	0x4B,	0x89},	// W#61	-	[18] Disable LDO12
      { 0x54,	0x05,	0x47,	0x89},	// W#62	-	[17] Disable LDO8
      { 0x54,	0x05,	0x4C,	0x89},	// W#63	-	[16] Disable LDO13
      { 0x54,	0x05,	0x0A,	0x88},	// W#64	-	[15] Disable VREF_LPDDR
      { 0x54,	0x05,	0x41,	0x89},	// W#65	-	[14] Disable LDO2
      { 0x54,	0x05,	0x45,	0x89},	// W#66	-	[9] Disable L6 (earlier, to ensure safe QFPROM shutdown before turning off MSM APC, MX, CX rails)
      { 0x54,	0x05,	0x5A,	0x88},	// W#67	-	[11] Disable Sleep Clock (before L5)
      { 0x54,	0x05,	0x51,	0x88},	// W#68	-	[13] Remove SW-Enable to disable BB_CLK1 (Actual disable will happen only after BBCLK1_EN Pin is Low AND SW-Control is also disabled [internal OR-gate for enabling])
      { 0x54,	0x05,	0x44,	0x89},	// W#69	-	[12] Disable LDO5 (This will also pull BBCLK1_EN Pin Low)
      { 0x54,	0x05,	0x50,	0x88},	// W#70	-	Disable XO (19.2MHz Clock)
      { 0x54,	0x05,	0x52,	0x89},	// W#71	-	Disable LDO_XO
      { 0x54,	0x05,	0x46,	0x89},	// W#72	-	[10] Disable LDO7
      { 0x54,	0x05,	0xA0,	0x88},	// W#73	-	[8] Disable MPP1
      { 0x54,	0x05,	0xC3,	0x88},	// W#74	-	[7] Disable GPIO4 (Goes Low, for Ext S5 Buck Disable)
      { 0x54,	0x05,	0x17,	0x89},	// W#75	-	[6] Disable S2
      { 0x54,	0x05,	0x14,	0x89},	// W#76	-	[5] Disable S1
      { 0x54,	0x05,	0x42,	0x89},	// W#77	-	[4] Disable LDO3
      { 0x54,	0x05,	0x1A,	0x89},	// W#78	-	[3] Disable S3
      { 0x54,	0x05,	0x40,	0x88},	// W#79	-	[2] Disable BMS (Can be left enabled & thus automatically killed after POFF, for slightly more BMS-On-Time & marginally improved SoC calculation - KKGK)
      { 0x54,	0x05,	0x1D,	0x89},	// W#80	-	[1] Disable S4
      { 0x40,	0x91,	0x08,	0x00},	// W#81	-	Write Sequence Complete ACK to PON
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#82	-	EOS (Finish POFF-Sequence)
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#83	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#84	-	
      { 0x00,	0x46,	0x00,	0x04},	// W#85	-	Write 0x00 to 0x46 (EN_CTL register in the peripheral)
      { 0x20,	0x00,	0x01,	0xC0},	// W#86	-	Wait 1ms (= 32 cycles of 32kHz clock)
      { 0x00,	0xFF,	0xFF,	0x8C},	// W#87	-	RETURN: Go back to next line of the calling function
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#88	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#89	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#90	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#91	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#92	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#93	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#94	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#95	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#96	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#97	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#98	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#99	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#100	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#101	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#102	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#103	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#104	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#105	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#106	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#107	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#108	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#109	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#110	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#111	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#112	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#113	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#114	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#115	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#116	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#117	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#118	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#119	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#120	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#121	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#122	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#123	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#124	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#125	-	
      { 0xFF,	0xFF,	0xFF,	0xFF},	// W#126	-	
      { 0x00,	0x00,	0xFF,	0xF8},	// W#127	-	
   },
};
