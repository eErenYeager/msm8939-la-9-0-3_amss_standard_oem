#ifndef __PM_SBL_BOOT__H__
#define __PM_SBL_BOOT__H__

/*! \file pm_sbl_boot.h
*  \n
*  \brief This header file contains enums and helper function declarations needed
*  \n during PMIC device initialization and during initial PMIC SBL settings configured
*  \n through the PDM auto-generated code.
*  \n
*  \n &copy; Copyright 2010-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/target/msm8909_pm8909/system/src/pm_sbl_boot.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/14/13   akm     Updating PMIC revision API
 
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "pm_err_flags.h"
#include "pm_version.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

typedef uint32 pm_rpm_rev_type ;

/*===========================================================================

                        FUNCTION PROTOTYPES 

===========================================================================*/

unsigned int pm_clk_busy_wait ( unsigned int mS );
unsigned int pm_rpm_resolve_chip_revision ( unsigned pmic_dev_index, pm_rpm_rev_type* pmic_rev );
pm_err_flag_type pm_driver_post_init (void);
pm_err_flag_type pm_pon_init(void);



#endif // __PM_SBL_BOOT__H__
