/*! \file pm_sbl_boot.c
*  \n
*  \brief This file contains PMIC device initialization function where initial PMIC
*  \n SBL settings are configured through the PDM auto-generated code.
*  \n
*  \n &copy; Copyright 2010-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/target/msm8909_pm8909/system/src/pm_sbl_boot.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/24/14   rk      remove pm_validate_pon as it is used for SMBC
12/19/13   rk      Added spmi channel config setings
11/22/13   rk      Added optimization of SBL and PBS generated code
07/15/13   aab     Enabled SBL charging
02/26/13   kt      Added PMIC SMEM Init function
02/14/13   kt      Added PBS Info reads and stored the info in Glb Ctxt
01/15/13   aab     Fixed KW error/Clean
12/06/12   hw      replace comdef.h with com_dtypes.h
07/26/12   umr     Add Chip Info Chip ID and Rev.
04/09/12   umr     Add PON reason API for SBL to return pon data.
10/03/11   hs      Added code check to ensure the power rail does get turned 
                   on/off before returning.
09/28/11   dy      Replace delay loop with DALSYS_BusyWait
08/14/11   dy      Check for more PMIC revisions
11/09/10   wra     File created for 8960

========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "com_dtypes.h"
#include "spmi_1_0_lite.h"
#include "pm_sbl_boot.h"
#include "pm_config_sbl.h"
#include "pm_pbs_info.h"
#include "device_info.h"
#include "DALSys.h" /* For DALSYS_BusyWait */
#include "pm_app_chg.h"
#include "boothw_target.h"
#include "pm_target_information.h"

extern boolean boot_dload_is_dload_mode_set( void );

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

/* PMIC Revision Id addresses */
#define PM_IO_REVID_BASE                    0x00000100

#define PM_IO_REVID_REVISION1_OFFSET     0x00000000
#define PM_IO_REVID_REVISION2_OFFSET     0x00000001
#define PM_IO_REVID_REVISION3_OFFSET     0x00000002
#define PM_IO_REVID_REVISION4_OFFSET     0x00000003
#define PM_IO_REVID_PERPH_TYPE_OFFSET    0x00000004
#define PM_IO_REVID_PERPH_SUBTYPE_OFFSET 0x00000005
#define PM_IO_REVID_COUNT                0x00000006
#define PM_IS_QC_PMIC                    0x00000051


static byte pmic_rev_info_a[PM_IO_REVID_COUNT];
static pm_rpm_rev_type pmic_a;

/* PMIC SlaveIDs */
static uint32 primary_slave_id_a = 0;
static uint32 secondary_slave_id_a = 0;

/*===========================================================================

                        FUNCTION DEFINITIONS 

===========================================================================*/

unsigned int
pm_clk_busy_wait ( unsigned int uS )
{
  unsigned int failure = TRUE ;

  if ( uS > 0 )
  {  
     failure = FALSE ;
     (void) DALSYS_BusyWait(uS);
  }

  return failure;
}

static pm_err_flag_type
pm_get_chip_info (void) 
{
  pm_err_flag_type  errFlag = PM_ERR_FLAG__SUCCESS;

  /* Read revision info of PMIC A */
  errFlag = pm_spmi_lite_read_byte_array(primary_slave_id_a, PM_IO_REVID_BASE, PM_IO_REVID_COUNT, 
                                             (uint8*) pmic_rev_info_a, 0);

  
  pmic_a = (pmic_rev_info_a[3] << 24)| (pmic_rev_info_a[2] << 16)| (pmic_rev_info_a[1] << 8) | pmic_rev_info_a[0];	// using only:	REvID:	All layer | metal layer 

  return errFlag;
}

unsigned int pm_rpm_resolve_chip_revision ( unsigned pmic_dev_index, pm_rpm_rev_type* pmic_rev )
{

  if (0 == pmic_dev_index)
  {
    *pmic_rev = pmic_a;
  }
  else
  {
    return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
  }

  return PM_ERR_FLAG__SUCCESS;

}

pm_err_flag_type
pm_driver_post_init (void)
{
  pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
  err = pm_chg_sbl_charging_state_entry();  //Enable SBL Dead/Weak battery Charging
  return err;
}

static pm_err_flag_type
pm_device_post_init(void)
{
  pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
  
  //PON KPDPWR configuration:  s1_timer=10256ms, S2_timer=100ms,  Warm Reset
  err |= pm_pon_reset_source_cfg(0, PM_PON_RESET_SOURCE_KPDPWR, 10256, 2000, PM_PON_RESET_CFG_WARM_RESET);

  //PON RESIN_AND_KPDPWR configuration:  s1_timer=6720ms, S2_timer=100ms,  Hard Reset
  err |= pm_pon_reset_source_cfg(0, PM_PON_RESET_SOURCE_RESIN_AND_KPDPWR, 10256, 2000, PM_PON_RESET_CFG_HARD_RESET);

  return err;
}
pm_err_flag_type pm_device_init ( void )
{
    pm_err_flag_type error = PM_ERR_FLAG__SUCCESS ;

    /* PMIC peripheralto SPMI Channel mapping */
    error |= pm_target_information_spmi_chnl_cfg(PROP_DEVICE_NAME_PM8909);
    error |= pm_version_detect();
    error |= pm_pon_init();
    /* Get Primary and Secondary Slave IDs of PMIC A */
    pm_get_slave_id(0, 0, &primary_slave_id_a);
    pm_get_slave_id(0, 1, &secondary_slave_id_a);

    pm_get_chip_info ();

    error |= pm_sbl_config(); /*SBL Configuration*/ 

    /* Read PBS INFO for the pmic devices*/
    error |= pm_pbs_info_init ();
    
    /* Initialize PMIC with the ones PDM can not perform */
    error |= pm_device_post_init(); 

    /* NON ZERO return means an ERROR */
    return error ; 
}
