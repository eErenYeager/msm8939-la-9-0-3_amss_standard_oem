/*! \file pm_sbl_boot.c
*  \n
*  \brief This file contains PMIC device initialization function where initial PMIC
*  \n SBL settings are configured through the PDM auto-generated code.
*  \n
*  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/target/mdm9x45_pmd9x45/system/src/pm_sbl_boot.c#1 $

when         who     what, where, why
--------     ---     ----------------------------------------------------------
06/03/14     akm     Initial Creation

========================================================================== */
/*===========================================================================

                     INCLUDE FILES

===========================================================================*/

#include "com_dtypes.h"
#include "spmi_1_0_lite.h"
#include "pm_sbl_boot.h"
#include "pm_config_sbl.h"
#include "pm_pbs_info.h"
#include "device_info.h"
#include "pm_target_information.h"
#include "pm_ldo.h"
#include "DALSys.h" /* For DALSYS_BusyWait */
#include "DDIChipInfo.h"

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

/* PMIC Revision Id addresses */
#define PM_IO_REVID_BASE                    0x00000100

#define PM_IO_REVID_REVISION1_OFFSET     0x00000000
#define PM_IO_REVID_REVISION2_OFFSET     0x00000001
#define PM_IO_REVID_REVISION3_OFFSET     0x00000002
#define PM_IO_REVID_REVISION4_OFFSET     0x00000003
#define PM_IO_REVID_PERPH_TYPE_OFFSET    0x00000004
#define PM_IO_REVID_PERPH_SUBTYPE_OFFSET 0x00000005
#define PM_IO_REVID_COUNT                0x00000006
#define PM_IS_QC_PMIC                    0x00000051

/* PMIC SlaveIDs */
#define PMIC_A_SLAVEID_PRIM                    0x0


static byte pmic_rev_info_a[PM_IO_REVID_COUNT];
static pm_rpm_rev_type pmic_a;



/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/

unsigned int
pm_clk_busy_wait ( unsigned int uS )
{
  unsigned int failure = TRUE ;

  if ( uS > 0 )
  {
     failure = FALSE ;
     (void) DALSYS_BusyWait(uS);
  }

  return failure;
}



static pm_err_flag_type
pm_get_chip_info (void)
{
  pm_err_flag_type  err_flag = PM_ERR_FLAG__SUCCESS;

  /* Read revision info of PMIC A */
  err_flag = pm_spmi_lite_read_byte_array(PMIC_A_SLAVEID_PRIM, PM_IO_REVID_BASE, PM_IO_REVID_COUNT,
                                             (uint8*) pmic_rev_info_a, 0);

  pmic_a = (pmic_rev_info_a[3] << 24)| (pmic_rev_info_a[2] << 16)| (pmic_rev_info_a[1] << 8) | pmic_rev_info_a[0];  // using only:  REvID:  All layer | metal layer

  return err_flag;
}

unsigned int pm_rpm_resolve_chip_revision ( unsigned pmic_dev_index, pm_rpm_rev_type* pmic_rev )
{

  if (0 == pmic_dev_index)
  {
    *pmic_rev = pmic_a;
  }
  else
  {
    return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
  }

  return PM_ERR_FLAG__SUCCESS;

}

pm_err_flag_type
pm_driver_post_init (void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    /* On MDM9x35, we have the below requirement for L4/L16 to work-around a H/W issue
       MDMv2.0 and newer: L4/L16 will be at default 0.95V whenever enabled
       Other older MDM versions: L4/L16 will be at 1V whenever enabled */

    /* The work-around is to change LDO4 and LDO16 voltage to 1V on
       older MDM versions < v2.0 */
  if ((DalChipInfo_ChipFamily() == DALCHIPINFO_FAMILY_MDM9x35) && (DalChipInfo_ChipVersion() < DALCHIPINFO_VERSION(2,0)))
  {
     err_flag |= pm_ldo_volt_level(0, PM_LDO_4, 1000000);
     err_flag |= pm_ldo_volt_level(0, PM_LDO_16, 1000000);
  }

  return err_flag;
}

static pm_err_flag_type
pm_device_post_init(void)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

  //PON KPDPWR configuration:  s1_timer=10256ms, S2_timer=100ms,  Warm Reset
  err_flag |= pm_pon_reset_source_cfg(0, PM_PON_RESET_SOURCE_KPDPWR, 10256, 2000, PM_PON_RESET_CFG_WARM_RESET);

  //PON RESIN_AND_KPDPWR configuration:  s1_timer=6720ms, S2_timer=100ms,  Hard Reset
  err_flag |= pm_pon_reset_source_cfg(0, PM_PON_RESET_SOURCE_RESIN_AND_KPDPWR, 10256, 2000, PM_PON_RESET_CFG_HARD_RESET);

  return err_flag;
}

pm_err_flag_type pm_device_init ( void )
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    /* PMIC peripheral to SPMI Channel mapping.
       This function must be called before calling any SPMI R/W. */
    err_flag |= pm_target_information_spmi_chnl_cfg(PROP_DEVICE_NAME_PDM9635);

    err_flag |= pm_version_detect();

    //pm_target_information_init();

    err_flag |= pm_pon_init();

    err_flag |= pm_get_chip_info();

    err_flag |= pm_sbl_config(); /* SBL Configuration */

    err_flag |= pm_pbs_info_init();  /* Read PBS INFO for the pmic devices */

    err_flag |= pm_device_post_init(); /* Initialize PMIC with the ones PDM can not perform */

    return err_flag; /* NON ZERO return means an ERROR */
}
