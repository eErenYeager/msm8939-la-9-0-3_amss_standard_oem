#ifndef __PM_CHG_H__
#define __PM_CHG_H__

/*! \file  pm_chg.h
 *  
 *  \brief  This file contains the pmic boot charger definitions.
 *  \details  This file contains the pmic boot charger definitions.
 *  
 *  &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/framework/inc/pm_chg.h#1 $ 
$DateTime: 2015/03/19 01:58:37 $  $Author: pwbldsvc $

when         who     what, where, why
----------   ---     ---------------------------------------------------------- 
10/19/2012   umr     Created.
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_err_flags.h"
#include "AdcBoot.h"

/*===========================================================================

                     FUNCTION DEFINITIONS

===========================================================================*/

pm_err_flag_type
pm_boot_adc_get_mv(const char *pszInputName, uint32 *battery_voltage);

#endif /* __PM_CHG_H__ */


