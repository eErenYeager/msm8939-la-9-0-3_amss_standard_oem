#ifndef __PMIC_SBL_CONFIG_H__
#define __PMIC_SBL_CONFIG_H__

/*! \file  pm_sbl_config.h
 *  
 *  \brief  Contains PMIC Set Mode driver declaration
 *  \details  This file contains the Set Mode driver API. These interfaces 
 *  should not be called directly. All entry into these interfaces should 
 *  from the application level set mode interfaces.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Locked Version: PM8026-x.x-OTPx.x-MSM8x26-07012013v1 - Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2013 Qualcomm Technologies, All Rights Reserved
 */

/*===========================================================================

                EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module over time.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/framework/inc/pm_config_sbl.h#1 $ 
$DateTime: 2015/03/19 01:58:37 $  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
08/07/13   aab     Creation
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "pm_qc_pmic.h"
#include "com_dtypes.h"         /* Containse basic type definitions */
#include "pm_err_flags.h"     /* Containse the error definitions */
#define PM_SBL_16BITADD_8BITDATA_REGULAR 2
#define PBS_RAM_DATA_SIZE 256
#define REV_ID_COMMON 0xFFFFFFFF

/*=============================================================================

DEFINITIONS

=============================================================================*/

typedef enum
{
   PM_SBL_WRITE,
   PM_SBL_DELAY,
   PM_SBL_PBS_RAM,
   PM_SBL_NOP,
   PM_SBL_OPERATION_INVALID
}pm_sbl_reg_operation_type;

 

typedef enum  
{
   EQUAL,
   GREATER,
   GREATER_OR_EQUAL,
   LESS,
   LESS_OR_EQUAL,
   REV_ID_OPERATION_INVALID
}pm_sbl_rev_id_operation_type;



typedef struct
{
   uint8  sid;
   uint8  data;
   uint16 base_address;
   uint16 offset;
   pm_sbl_reg_operation_type reg_operation;
   pm_sbl_rev_id_operation_type rev_id_operation;
   uint32  rev_id;
}pm_sbl_seq_type;


/*=============================================================================

                              FUNCTION PROTOTYPES

=============================================================================*/

/*Generic function that reads SBL Config data and PBS RAM data from DAL Config and writes it to PMIC */
pm_err_flag_type pm_sbl_config(void);




#endif // __PMIC_SBL_CONFIG_H__

