#ifndef PM_PROCESSOR_H
#define PM_PROCESSOR_H

/*! \file pm_processor.h
 *  \n
 *  \brief
 *  \n  
 *  &copy; Copyright 2010-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/framework/inc/pm_processor.h#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/30/11   jtn     Fix RPM compiler warnings
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#include "icomm.h"
#include "pm_resources_and_types.h"

/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/

/*! \brief This function initializes the processor related information, such as Comm.
 *  \param[in] None
 *
 *  \return None.
 *
 *  <b>Dependencies</b>
 *  \li DAL framework.
 */
void pm_processor_init(void);

/*! \brief This function resolves the parameter specified peripheral information.
 *  \param[in] comm_ptr The Comm Channel.
 *
 *  \param[in] peripheral_info A pointer to where the peripheral information is going to be saved.
 *
 *  \return Error flag.
 *
 *  <b>Dependencies</b>
 *  \li pm_processor_init() .
 */
pm_err_flag_type pm_processor_resolve_peripheral_version(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info);

/*! \brief This function creates the comm channels.
 *  \param[in] comm_info The Comm info.
 *
 *  \param[in] current_comm The current comm object.
 *
 *  \param[in] new_comm The new comm object to be created.
 *
 *  \return Error flag.
 *
 *  <b>Dependencies</b>
 *  \li pm_processor_init() .
 */
pm_err_flag_type pm_processor_create_comm_channel(pmiC_CommInfo *comm_info, pmiC_IComm **current_comm, pmiC_IComm **new_comm);

#endif /* PM_PROCESSOR_H */

