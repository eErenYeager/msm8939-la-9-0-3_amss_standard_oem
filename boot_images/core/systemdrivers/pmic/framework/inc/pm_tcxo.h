#ifndef __PM_TCXO_H__
#define __PM_TCXO_H__

/*! \file  pm_tcxo.h
 *  
 *  \brief  This file contains the pmic tcxo definitions.
 *  
 *  &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/framework/inc/pm_tcxo.h#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/11/13   umr      Created.
===========================================================================*/

/*===========================================================================

                     FUNCTION DEFINITIONS

===========================================================================*/

pm_err_flag_type pm_boot_tcxo_config (void);

pm_err_flag_type pm_clk_tcxo_config(void);

#endif //__PM_TCXO_H__




