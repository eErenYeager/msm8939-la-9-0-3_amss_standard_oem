/*! \file  pmic_sbl_gen_driver.c
 *
 *  \brief  File Contains the PMIC Set Mode Implementation
 *  \details Set Mode implementation is responsible for setting
 *  all mode settings such as Register values, memory values, etc.
 *
 *    This file contains code for Target specific settings and modes.
 *
 *  &copy; Copyright 2013 Qualcomm Technologies, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/framework/src/pm_config_sbl.c#1 $
$DateTime: 2015/03/19 01:58:37 $  $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/18/14   mr      Removed KW errors (CR-643316)
08/07/13   aab     Creation
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES

===========================================================================*/

#include "spmi_1_0_lite.h"              /* Needed for SSBI communications */
#include "pm_sbl_boot.h"                /* Needed for pm_clk_busy_wait and sbi_mini initialization */
#include "pm_pbs_driver.h"              /* Needed for pm_clk_busy_wait and sbi_mini initialization */
#include "pm_config_sbl.h"                          /* Contains the type definitions and static register tables */
#include "pm_target_information.h"


boolean
pm_sbl_validate_reg_config( uint32                        rev_id,       //Generated from PDM
                             pm_rpm_rev_type              rev_id_read,  //Read from PMIC RevID
                             pm_sbl_reg_operation_type    reg_operation,
                             pm_sbl_rev_id_operation_type rev_id_operation
                           )
{
   boolean valid_config_flag = FALSE;

   if(rev_id == REV_ID_COMMON)
   {
      valid_config_flag = TRUE;
   }
   else
   {
      switch(rev_id_operation)
      {
         case EQUAL:
            valid_config_flag = (rev_id_read == rev_id)?TRUE:FALSE;
            break;
         case GREATER:
            valid_config_flag = (rev_id_read > rev_id)?TRUE:FALSE;
            break;
         case GREATER_OR_EQUAL:
            valid_config_flag = (rev_id_read >= rev_id)?TRUE:FALSE;
            break;
         case LESS:
            valid_config_flag = (rev_id_read < rev_id)?TRUE:FALSE;
            break;
         case LESS_OR_EQUAL:
            valid_config_flag = (rev_id_read <= rev_id)?TRUE:FALSE;
            break;
         default:
            valid_config_flag = FALSE;
            break;
      }
   }
   return valid_config_flag;
}



pm_err_flag_type
pm_sbl_config()
{
   pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
   uint16 pbs_data_index = 0;
   pm_pbs_ram_data_type *pbs_ram_config_ds = NULL;
   pm_sbl_seq_type *sbl_reg_config_ds = NULL;
   pm_sbl_seq_type *rc = NULL;  //reg config data ptr
   pm_rpm_rev_type rev_id_read;
   boolean primary_pmic_rev_read_flag   = FALSE;
   boolean secondary_pmic_rev_read_flag = FALSE;

   pm_target_information_init();
   sbl_reg_config_ds = (pm_sbl_seq_type*)pm_target_information_get_specific_info(PM_PROP_SBL_REG_CONFIG_LUT); //get handle for sbl config data
   if (NULL == sbl_reg_config_ds)
   {
      return PM_ERR_FLAG__INVALID_POINTER;
   }

   for(rc = sbl_reg_config_ds; (rc->reg_operation != PM_SBL_OPERATION_INVALID); rc++)
   {
      if( (rc->sid <= 1) && (primary_pmic_rev_read_flag == FALSE) )
      {
         pm_rpm_resolve_chip_revision (0, &rev_id_read ); //read PMIC chip revision from primary PMIC
         primary_pmic_rev_read_flag   = TRUE;
         secondary_pmic_rev_read_flag = FALSE;
      }
      else if( (rc->sid > 1) && (secondary_pmic_rev_read_flag == FALSE) )
      {
         pm_rpm_resolve_chip_revision (1, &rev_id_read ); //read PMIC chip revision from secondary PMIC
         primary_pmic_rev_read_flag   = FALSE;
         secondary_pmic_rev_read_flag = TRUE;
      }


     if( pm_sbl_validate_reg_config(rc->rev_id, rev_id_read, rc->reg_operation, rc->rev_id_operation)== TRUE )
     {
        switch(rc->reg_operation)
        {
           case PM_SBL_WRITE:
              {
                 rc->base_address += rc->offset;
                 err_flag = pm_spmi_lite_write_byte(rc->sid, rc->base_address, rc->data, 1);
              }
              break;
           case PM_SBL_DELAY:
              {
                 pm_clk_busy_wait(rc->offset);
              }
              break;
           case PM_SBL_PBS_RAM:
              {
                 pbs_data_index = rc->data*PBS_RAM_DATA_SIZE;
                 pbs_ram_config_ds = (pm_pbs_ram_data_type*)pm_target_information_get_specific_info( PM_PROP_PBS_RAM_CONFIG_LUT); 
                 err_flag = pm_pbs_ram_image_load(rc->sid, &pbs_ram_config_ds[pbs_data_index], PBS_RAM_DATA_SIZE);
              }
              break;
           case PM_SBL_NOP:
              break;
           default:
                 err_flag = PM_ERR_FLAG__SBL_CONFIG_REG_OPERATION_NOT_SUPPORTED;
              break;
        }
     }
   }

   return err_flag;
}


