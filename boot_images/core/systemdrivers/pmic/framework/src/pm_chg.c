/*! \file 
*  \n
*  \brief  pm_boot_chg.c ---- FLCB CHG RELATED DEFINITION
*  \n
*  \n This header file contains class implementation of the IComm
*  \n messaging interface
*  \n
*  \n Copyright 2011-2013 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  \n Qualcomm Technologies Proprietary and Confidential.
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/framework/src/pm_chg.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/23/13   aab     Added API pm_boot_flag_sbl_chg_event_in_bms() 
07/15/13   aab     Moved charger related APIs to SMBB driver
07/03/13   ps      Adding API to disable bat_therm pin CR#492767   
04/19/13   aab     Adding API to disable charging 
03/15/13   umr     Add support for broken battery detection. 
03/01/13   aab     Updated pm_boot_chg_config_rsense()
02/12/13   aab     Added more update on pm_boot_chg_get_attached_charger()
02/06/13   aab     Updated pm_boot_chg_get_attached_charger()
01/20/13   dy      Added API to configure Rsense
01/09/13   aab     Added API to control Red LED (CR-421406)
12/04/12   umr     Add Batt_ID and Batt_Therm to Batt present API
09/05/12   hs      Updated for Badger.
03/22/12   hs      Replaced SBI calls with SPMI calls
========================================================================== */

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include "pm_boot_chg.h"
#include "spmi_1_0_lite.h"            /* Needed for SPMI communications */
#include "AdcBoot.h"
#include "busywait.h"
#include "pm_chg.h"
pm_err_flag_type
pm_boot_adc_get_mv(const char *pszInputName, uint32 *battery_voltage)
{
  pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
  AdcBootDeviceChannelType channel;
  AdcBootResultType adc_result;
  uint32 error = 0x0;
  DALResult adc_err;

  /*Initialize the ADC*/
  error = AdcBoot_Init();

  if (0 != error)
  {
    err_flag |= PM_ERR_FLAG__ADC_NOT_READY;
  }
  else
  {
    /*Get the channel from where the data is needed, Like ADC_INPUT_VBATT, ADC_INPUT_XO_THERM */
    adc_err = AdcBoot_GetChannel(pszInputName, &channel);
    if (DAL_SUCCESS != adc_err)
    {
      err_flag |= PM_ERR_FLAG__ADC_NOT_READY;
    }
    else
    {
      /*Read the Voltage of the Battery*/
      adc_err = AdcBoot_Read(&channel, &adc_result);
      if (DAL_SUCCESS != error)
      {
        err_flag |= PM_ERR_FLAG__ADC_NOT_READY;
      }
      /*Check for the result*/
      if (0 != adc_result.eStatus)
      {
        *battery_voltage = (uint32)(adc_result.nMicrovolts / 1000);
      }
      else
      {
        err_flag |= PM_ERR_FLAG__ADC_NOT_READY;
      }
    }
  }
  return err_flag;
}


pm_err_flag_type pm_boot_chg_enable_led (boolean enable, pm_boot_chg_led_source_type type)
{
    if(type > PM_BOOT_CHG_LED_SRC__INTERNAL)
    {
        return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }

    if(enable == TRUE)
    {
        /********************************* Select the LED source ******************************/
        //- 00 = ground (no power)
        //- 01 = VPH_PWR / VSYS
        //- 10 : 5 V supply
        //- 11 : min(VCHG, 5V), internally clamped by charger module

        /* Set the new data in LED_SRC_SEL bit 1:0 */
        if ( pm_spmi_lite_write_byte_mask(1, 0xD045, 0x03, (uint8)type, 0 ) != PM_ERR_FLAG__SUCCESS )
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }

        /********************************* Enable the LED  ******************************/
        /* 4mA current + blue in EN_CTL_ATC bit 5 */
        if ( pm_spmi_lite_write_byte_mask(1, 0xD047, 0x20, 0x20, 0 ) != PM_ERR_FLAG__SUCCESS )
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }
    }
    else
    {
        /* clear EN_CTL_ATC bit 5 */
        if ( pm_spmi_lite_write_byte_mask(1, 0xD047, 0x20, 0, 0 ) != PM_ERR_FLAG__SUCCESS )
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }
    }

    return PM_ERR_FLAG__SUCCESS;
}


pm_err_flag_type pm_boot_chg_enable_led_with_dim_ctrl(boolean enable, pm_boot_chg_led_source_type type, pm_boot_chg_led_dim_level_type dim_level)
{
    
    if(type > PM_BOOT_CHG_LED_SRC__INTERNAL)
    {
        return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }

	
    if(enable == TRUE)
    {
        /********************************* Select the LED source ******************************/
        //- 00 = ground (no power)
        //- 01 = VPH_PWR / VSYS
        //- 10 : 5 V supply
        //- 11 : min(VCHG, 5V), internally clamped by charger module

        /* Set the new data in LED_SRC_SEL bit 1:0 */
        if ( pm_spmi_lite_write_byte_mask(1, 0xD045, 0x03, (uint8)type, 0 ) != PM_ERR_FLAG__SUCCESS )
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }

        //Enable RED LED
        /* Set the new data in LED_SRC_SEL bit 1:0 */
        if ( pm_spmi_lite_write_byte_mask(1, 0xD046, 0x80, 0x80, 0 ) != PM_ERR_FLAG__SUCCESS )
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }


        /********************************* Program LPG ******************************/
        if ( pm_spmi_lite_write_byte_mask(1, 0xB740, 0xFF, 0x00, 0 ) != PM_ERR_FLAG__SUCCESS ) //No pattern is being set here
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }

        if ( pm_spmi_lite_write_byte_mask(1, 0xB741, 0xFF, 0x13, 0 ) != PM_ERR_FLAG__SUCCESS ) //7 bit mode, 19.2 MHz clock
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }

        if ( pm_spmi_lite_write_byte_mask(1, 0xB742, 0xFF, 0x47, 0 ) != PM_ERR_FLAG__SUCCESS ) //Pre divide = 5, Exponent = 7 , This gives a 390 Hz PWM frequency
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }

        if ( pm_spmi_lite_write_byte_mask(1, 0xB743, 0xFF, 0x00, 0 ) != PM_ERR_FLAG__SUCCESS ) 
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }

        //Configure DIM level
        if (LOW == dim_level)
        {
            if ( pm_spmi_lite_write_byte_mask(1, 0xB744, 0xFF, 0x2A, 0 ) != PM_ERR_FLAG__SUCCESS )
            { 
                return PM_ERR_FLAG__SBI_OPT_ERR ; 
            }
        }
        else if(MID == dim_level)
        {
            if ( pm_spmi_lite_write_byte_mask(1, 0xB744, 0xFF, 0x54, 0 ) != PM_ERR_FLAG__SUCCESS )
            { 
                return PM_ERR_FLAG__SBI_OPT_ERR ; 
            }
        }
        else if(HIGH == dim_level)
        {
            if ( pm_spmi_lite_write_byte_mask(1, 0xB744, 0xFF, 0x80, 0 ) != PM_ERR_FLAG__SUCCESS ) //fixed 4mA
            { 
                return PM_ERR_FLAG__SBI_OPT_ERR ; 
            }
        }


        if ( pm_spmi_lite_write_byte_mask(1, 0xB745, 0xFF, 0x00, 0 ) != PM_ERR_FLAG__SUCCESS ) 
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }

        if ( pm_spmi_lite_write_byte_mask(1, 0xB746, 0xFF, 0xE4, 0 ) != PM_ERR_FLAG__SUCCESS ) //Enable PWM
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }


    }
    else
    {
        //Turn OFF Red LED
        if ( pm_spmi_lite_write_byte_mask(1, 0xD046, 0x80, 0x00, 0 ) != PM_ERR_FLAG__SUCCESS )
        { 
            return PM_ERR_FLAG__SBI_OPT_ERR ; 
        }

    }

    return PM_ERR_FLAG__SUCCESS;
}


pm_err_flag_type pm_boot_chg_config_rsense(pm_boot_chg_rsense_type rsense_cfg)
{
    uint8 data = 0;
    
    data = ( rsense_cfg == PM_BOOT_CHG_RSENSE_EXTERNAL ) ? 1 : 0;
    
    if ( pm_spmi_lite_write_byte_mask(0, 0x3848, 0x07, (uint8)data, 0 ) != PM_ERR_FLAG__SUCCESS )
    {
        return PM_ERR_FLAG__SBI_OPT_ERR ; 
    }
    else
    {
        return PM_ERR_FLAG__SUCCESS;
    }
}




pm_err_flag_type pm_smbb_disable_bat_therm(void)
{
    if ( pm_spmi_lite_write_byte_mask(0, 0x1248, 0x07, 0x05, 0 ) != PM_ERR_FLAG__SUCCESS )
    {
        return PM_ERR_FLAG__SBI_OPT_ERR ; 
    }

    if ( pm_spmi_lite_write_byte_mask(0, 0x1249, 0x80, 0x00, 0 ) != PM_ERR_FLAG__SUCCESS )
    {
        return PM_ERR_FLAG__SBI_OPT_ERR ; 
    }

    return PM_ERR_FLAG__SUCCESS;
}



pm_err_flag_type pm_boot_flag_sbl_chg_event_in_bms(boolean set_flag)
{
    uint8 data = 0;

    data = ( set_flag == TRUE ) ? 1 : 0;

    //BMS register BMS_DATA_REG_0 : 0x40B0:  set Bit 0 = 1  if we go through SBL charging. (V<3.4)
    if ( pm_spmi_lite_write_byte_mask(0, 0x40B0, 0x01, (uint8)data, 0 ) != PM_ERR_FLAG__SUCCESS )
    {
        return PM_ERR_FLAG__SBI_OPT_ERR ; 
    }
    else
    {
        return PM_ERR_FLAG__SUCCESS;
    }
}

