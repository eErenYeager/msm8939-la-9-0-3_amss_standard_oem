/*! \file 
*  \n
*  \brief  pm_clk_tcxo.c
*  \n
*  \n
*  \n Copyright 2013 Qualcomm Technologies, Inc.  All Rights Reserved. 
*  \n Qualcomm Technologies Proprietary and Confidential.
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/pmic/framework/protected/pm_clk_tcxo.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/15/13   umr     Add support for TCXO config. 
========================================================================== */

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include "com_dtypes.h"
#include "spmi_1_0_lite.h"

/* Configure for TCXO and external sleep xtal */

pm_err_flag_type
pm_clk_tcxo_config(void)
{
  pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;

  /* Disable CalRC clock */
  err |= pm_spmi_lite_write_byte_mask(0, 0x5A5B, 0x80, 0x00, 1);

  /* Set the XO trim code to 0 for TCXO */
  err |= pm_spmi_lite_write_byte(0, 0x505C, 0x00, 1);

  /* Set TCXO Warm up time and this is currently over-written
     to 0x82 in RPM sleep_init which should be fine */
  err |= pm_spmi_lite_write_byte(0, 0x504C, 0x64, 1);
  err |= pm_spmi_lite_write_byte(0, 0x504D, 0x00, 1);

  /* Enables the 32KHz crystal oscillator */
  err |= pm_spmi_lite_write_byte(0, 0x5A49, 0x80, 1);

  /* <5:4> = 00 Follow osc halt and XO_timer for SMPS clock
     <3:2> = 00 Follow osc halt and XO_timer for GP clock
     <1:0> = 00 Follow osc halt and XO_timer for sleep clock */
  err |= pm_spmi_lite_write_byte(0, 0x5940, 0x00, 1);

  /* Not selecting 19.2MHz XO as the sleep clock source */
  err |= pm_spmi_lite_write_byte_mask(0, 0x5941, 0x01, 0x00, 1);

  /* Not selecting 19.2MHz XO as the sleep clock for RTC */
  err |= pm_spmi_lite_write_byte_mask(0, 0x5942, 0x01, 0x00, 1);

  /* Bit:1 selects the low frequency clock source when phone is off without battery */
  /* Bit:0 selects the 32KHz XO or external source */
  err |= pm_spmi_lite_write_byte_mask(0, 0x5943, 0x03, 0x01, 1);

  return err;
}

