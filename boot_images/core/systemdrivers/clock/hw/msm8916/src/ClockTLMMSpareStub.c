/** @file ClockTlmmSpare.c
   
  This is a stub file to avoid compilation for EMMC.

  Copyright (c) 2014, Qualcomm Technologies Inc. All rights reserved.
  
**/
/*=============================================================================
                              EDIT HISTORY

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/msm8916/src/ClockTLMMSpareStub.c#2 $ 
  $DateTime: 2015/07/20 00:34:35 $ 
  $Author: pwbldsvc $ 

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 18/02/14   mkumari     Initial version

=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/
#include "com_dtypes.h"
/* ========================================================================
**  Function : Clock_ExtBuck_GPIO_Misc
** ======================================================================*/
/*
    Description: This is a stub API to avoid compilation for EMMC.

    @param NONE
    @return
    TRUE  -- Configuration was success.
    FALSE -- Configuration failed.

    @dependencies
    None.

    @sa None
*/

boolean Clock_ExtBuckGPIOMisc(void)
{
  
    return TRUE;

}

boolean Is_DDR_Type_LPDDR3( void )
{
  return FALSE;
}
