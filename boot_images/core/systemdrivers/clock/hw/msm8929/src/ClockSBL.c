/*
===========================================================================
  @file ClockSBL.c

  This file provides clock initialization for the Apps SBL.
===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/msm8929/src/ClockSBL.c#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  =========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/
#include "ClockSBL.h"
#include "ClockHWIO.h"
#include "ClockSBLConfig.h"
#include "ddr_drivers.h" 
#include "ddr_common.h"
#include "railway.h"
#include "busywait.h"
/*=========================================================================
      Macro Definitions
==========================================================================*/
/*
 * HALF_DIVIDER
 *
 * Macro to return the normalized half divider for a given mux structure.
 * NOTE: Expecting (2 * divider) value as input.
 */
#define HALF_DIVIDER(mux)  ((mux)->nDiv2x ? (((mux)->nDiv2x) - 1) : 0)

/* Definitions for generalizing clock configuration */
#define CLOCK_CMD_CFG_UPDATE_FMSK   HWIO_FMSK(GCC_SDCC2_APPS_CMD_RCGR, UPDATE)

#define CLOCK_CFG_REG_OFFSET        (HWIO_ADDR(GCC_SDCC2_APPS_CFG_RCGR)-HWIO_ADDR(GCC_SDCC2_APPS_CMD_RCGR))
#define CLOCK_CFG_CGR_SRC_SEL_FMSK  HWIO_FMSK(GCC_SDCC2_APPS_CFG_RCGR, SRC_SEL)
#define CLOCK_CFG_CGR_SRC_SEL_SHFT  HWIO_SHFT(GCC_SDCC2_APPS_CFG_RCGR, SRC_SEL)
#define CLOCK_CFG_CGR_SRC_DIV_FMSK  HWIO_FMSK(GCC_SDCC2_APPS_CFG_RCGR, SRC_DIV)
#define CLOCK_CFG_CGR_SRC_DIV_SHFT  HWIO_SHFT(GCC_SDCC2_APPS_CFG_RCGR, SRC_DIV)

#define CLOCK_CFG_CGR_MODE_FMSK     HWIO_FMSK(GCC_SDCC2_APPS_CFG_RCGR, MODE)
#define CLOCK_CFG_CGR_MODE_SHFT     HWIO_SHFT(GCC_SDCC2_APPS_CFG_RCGR, MODE)
#define CLOCK_CFG_CGR_MODE_DUAL_EDGE_VAL  0x2
#define CLOCK_CFG_CGR_MODE_BYPASS_VAL     0x0

#define HWIO_OUTF2(a, b, c, b1, c1) \
HWIO_OUTF(a, b, b1); \
HWIO_OUTF(a, c, c1);
/*=========================================================================
     Externs
==========================================================================*/

/*=========================================================================
      Function Prototypes
==========================================================================*/

boolean Clock_InitCrypto(void);
boolean Clock_ConfigBIMCMux (BimcMuxType* pBIMC);

/*=========================================================================
      Function Definitions
==========================================================================*/

/*=========================================================================
      Data
==========================================================================*/
struct Clock_I2CRegType 
{
  uint32 cmd;
  uint32 cbcr;
};

const struct Clock_I2CRegType Clock_I2CRegs[CLK_BLSP_QUP_I2C_NUM_CLKS] = 
{ 
  { 0,0 },
  { HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CBCR) },
  { HWIO_ADDR(GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP2_I2C_APPS_CBCR) },
  { HWIO_ADDR(GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP3_I2C_APPS_CBCR) },
  { HWIO_ADDR(GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP4_I2C_APPS_CBCR) },
  { HWIO_ADDR(GCC_BLSP1_QUP5_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP5_I2C_APPS_CBCR) },
  { HWIO_ADDR(GCC_BLSP1_QUP6_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP6_I2C_APPS_CBCR) },
};

/*=========================================================================
      Function Definitions
=========================================================================*/
/* ============================================================================
**	Function : Clock_SetUARTClockFrequency
** ============================================================================
*/
/*!
	Configure UART clock to a specific perf level.

	@param    ePerfLevel    -	[IN] UART Clock perf level
		      eClock	-	[IN] UART clock to configure
	@return
	TRUE -- Initialization was successful.
	FALSE -- Initialization failed.

	@dependencies
	None.

	@sa None
*/
boolean Clock_SetUARTClockFrequency
(
  ClockBootPerfLevelType ePerfLevel,
  ClockUARTType eClock
)
{
  ClockConfigMuxType UARTCfg = {0};
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();

  if(ePerfLevel >= CLOCK_BOOT_PERF_NUM) return FALSE;

  /* Copy the config to the local so the nCMDCGRAddr can be modified. */
  UARTCfg = cfg->UART_Cfg[ePerfLevel];

  if( ! Clock_EnableSource( UARTCfg.eSource )) return FALSE;

  switch (eClock)
  {
    case CLK_BLSP1_UART1_APPS:
      UARTCfg.nCMDCGRAddr = HWIO_ADDR(GCC_BLSP1_UART1_APPS_CMD_RCGR);
      if( ! Clock_ConfigMux(&UARTCfg)) return FALSE;
      Clock_ToggleClock(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR, CLK_TOGGLE_ENABLE);
      break;
	  
    case CLK_BLSP1_UART2_APPS:
      UARTCfg.nCMDCGRAddr = HWIO_ADDR(GCC_BLSP1_UART2_APPS_CMD_RCGR);
      if( ! Clock_ConfigMux(&UARTCfg)) return FALSE;
      Clock_ToggleClock(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR, CLK_TOGGLE_ENABLE);
      break;
	  
    default:
      return FALSE;
  }

  /* Enable BLSP1 AHB CLK */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_AHB_CLK_ENA, 1);

  return TRUE;

} /* END Clock_SetUARTClockFrequency */



/* ============================================================================
**	Function : Clock_DisableUARTClock
** ============================================================================
*/
/*!
	Disable specific UART clock.

	@param    eClock       -	[IN] UART clock to Disable
	@return
	TRUE -- Clock disable was successful.
	FALSE -- Clock disable failed.

	@dependencies
	None.

	@sa None
*/
boolean Clock_DisableUARTClock(ClockUARTType eClock)
{

  switch (eClock)
  {
    case CLK_BLSP1_UART1_APPS:
      Clock_ToggleClock(HWIO_GCC_BLSP1_UART1_APPS_CBCR_ADDR, CLK_TOGGLE_DISABLE);
      break;

    case CLK_BLSP1_UART2_APPS:
      Clock_ToggleClock(HWIO_GCC_BLSP1_UART2_APPS_CBCR_ADDR, CLK_TOGGLE_DISABLE);
      break;

     default:
      return FALSE;
  }

  return TRUE;
}

/* ============================================================================
**	Function : Clock_SetI2CClockFrequency
** ============================================================================
*/
/*!
	Configure QUP_I2C clock to a specific perf level.

	@param      ePerfLevel	 -	[IN] UART Clock perf level
		        eClock	 -	[IN] UART clock to configure
	@return
	TRUE -- Initialization was successful.
	FALSE -- Initialization failed.

	@dependencies
	None.

	@sa None
*/
boolean Clock_SetI2CClockFrequency
(
  ClockBootPerfLevelType ePerfLevel,
  ClockQUPI2CType eClock
)
{
  ClockConfigMuxType I2CCfg = {0};
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();

  if(ePerfLevel >= CLOCK_BOOT_PERF_NUM) return FALSE;
  if(eClock >= CLK_BLSP_QUP_I2C_NUM_CLKS) return FALSE;

  /* Copy the config to the local so the nCMDCGRAddr can be modified. */
  I2CCfg = cfg->I2C_Cfg[ePerfLevel];

  if( ! Clock_EnableSource( I2CCfg.eSource )) return FALSE;

  I2CCfg.nCMDCGRAddr = Clock_I2CRegs[eClock].cmd;
  if( ! Clock_ConfigMux(&I2CCfg)) return FALSE;
  Clock_ToggleClock(Clock_I2CRegs[eClock].cbcr, CLK_TOGGLE_ENABLE);

  /* Enable BLSP1 AHB CLK */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_AHB_CLK_ENA, 1);


 return TRUE;
} /* END Clock_SetI2CClockFrequency */



/* ============================================================================
**	Function : Clock_DisableI2CClock
** ============================================================================
*/
/*!
	Disable  specific I2C clock.

	@param    eClock	-	[IN] I2C clock to Disable
	@return
	TRUE -- Clock disable was successful.
	FALSE -- Clock disable failed.

	@dependencies
	None.

	@sa None
*/
boolean Clock_DisableI2CClock(ClockQUPI2CType eClock)
{
  if(eClock >= CLK_BLSP_QUP_I2C_NUM_CLKS) return FALSE;
  Clock_ToggleClock(Clock_I2CRegs[eClock].cbcr, CLK_TOGGLE_DISABLE);
  
  return TRUE;
}

/* ============================================================================
**  Function : Clock_DebugInit
** ============================================================================
*/
/*!

    This function is called very early in boot.  It is used for work-arounds that
    need to be done before JTAG attaches at the SBL "hold focus button" spin loop.

    @return
    TRUE -- Clean up was successful.
    FALSE -- Clean up failed.

   @dependencies
    You cannot use busywait in this function.

*/
boolean Clock_DebugInit(void)
{
  /* Enable QDSS register access */
  Clock_ToggleClock(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* Needed for Random Stack Canary */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, PRNG_AHB_CLK_ENA, 1);
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA, 1);

  /*Setting these bits to default value in case warm reset does not clear it*/
  HWIO_OUT(GCC_MSS_RESTART, 0x0);
  HWIO_OUT(GCC_VENUS_RESTART, 0x0);
  HWIO_OUT(GCC_WCSS_RESTART, 0x0);

  return TRUE;
}

/* ========================================================================
**  Function : Clock_I2CInit
** ======================================================================*/
/*
    Description: Configure all clocks needed for EEPROM to be used Pre DDR.

    @param None
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_I2CInit( void )
{
  /* Enabled QUP5 I2C Apps clk (fixed at 19.2) and vote for the AHB */
  /* NOTE:  The I2C hardware block numbers their QUPS from 0-5, but 
   * clock control uses 1-6.  We are having off-by-one errors so be
   * wary and clear in communicating about these serial devices. */

  // 8x26 removed BLSP2
  // TODO HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP2_AHB_CLK_ENA, 1);
  // TODO HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP2_SLEEP_CLK_ENA, 1);
  // TODO HWIO_OUTF(GCC_BLSP2_QUP5_I2C_APPS_CBCR, CLK_ENABLE, 1);

  /* These clocks can be disabled at Clock_ExitBoot, so keep that function
   * in sync */
  return TRUE;
}

/* ========================================================================
**  Function : Clock_InitVotes
** ======================================================================*/
/*
    Description: This function serves two purposes.  It clears 
    unwanted votes that may be left after watchdog or JTAG reset, which 
    does not actually reset all of GCC.

    @param None
    @return None

    @dependencies
    None.

    @sa None
*/
void Clock_InitVotes( void )
{
  uint32 rpm_gpll_votes = 0;

  /* 
   * Correct any left over votes from a watchdog or JTAG induced reset 
   */


  /* RPM needs GPLL0 and maybe GPLL1 if it was configured in PBL */
  if(HWIO_INF(GCC_GPLL0_MODE, PLL_VOTE_FSM_ENA ) > 0) {
    rpm_gpll_votes |= HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL0);
  }

  HWIO_OUT(GCC_RPM_GPLL_ENA_VOTE, rpm_gpll_votes);

  /* APCS requires the same PLLs */
  HWIO_OUT(GCC_APCS_GPLL_ENA_VOTE, rpm_gpll_votes);

  /* Everybody else has not booted yet */
  HWIO_OUT(GCC_APCS_TZ_GPLL_ENA_VOTE, 0);
  HWIO_OUT(GCC_MSS_Q6_GPLL_ENA_VOTE, 0);
  HWIO_OUT(GCC_WCSS_GPLL_ENA_VOTE, 0);
  HWIO_OUT(GCC_SPARE_GPLL_ENA_VOTE, 0);

}

/* ============================================================================
**  Function : Clock_DDRSpeed
** ============================================================================
*/
/*!
    Return the DDR clock rate in kilohertz.  This clock rate is the bus speed.
    It is not the controller clock (2X) clock for targets that use Legacy mode.

    @param None.
    @return
      The speed configured in Clock_PreDDRInit() of the BIMC/DDR clock.

    @dependencies
    None.

    @sa None
*/

static uint32 bimc_speed_khz = 172800;
static uint32 ddr_speed_khz  = 345600;

uint32 Clock_DDRSpeed()
{
  return ddr_speed_khz;
}

/* ========================================================================
**  Function : Clock_PreDDRInitEx
** ======================================================================*/
/*
    Description: Configure all clocks needed for DDR configuration.

    @param None
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_PreDDRInitEx( uint32 ddr_type )
{
  uint32 mask;
 
  Clock_SBLConfigType *cfg = Clock_SBLConfig();
  const ClockConfigMuxType BIMCclkCfg =    {HWIO_ADDR(GCC_BIMC_DDR_CMD_RCGR),  MUX_BIMC, SRC_GPLL5,  4, 0, 0, 0};
  const ClockConfigMuxType DDRclkCfg  =    {HWIO_ADDR(GCC_DDR_CMD_RCGR),       MUX_BIMC, SRC_GPLL5,  2, 0, 0, 0};
  const ClockConfigMuxType bimcgpuclkCfg = {HWIO_ADDR(GCC_BIMC_GPU_CMD_RCGR),  MUX_GCC,  SRC_GPLL0,  10, 0, 0, 0};
  
  /* Clean up from watchdog/JTAG reset */
  Clock_InitVotes();

  /* Enable Imem and Msg RAM clock */
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA, 1);
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA, 1);

  /* Enable the necessary voteable KPSS and BIMC related clocks */
  mask = 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, SYS_NOC_APSS_AHB_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BIMC_APSS_AXI_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, APSS_AHB_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, APSS_AXI_CLK_ENA);
   
    

  HWIO_OUTM(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, mask, mask);
  HWIO_OUTF(GCC_APCS_CLOCK_SLEEP_ENA_VOTE, BIMC_APSS_AXI_CLK_SLEEP_ENA, 1);
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE , BIMC_APSS_AXI_CLK_ENA, 0);
  
  /* SPMI clocks are already enabled by default on reset */
  /* gcc_spmi_ser_clk default on */
  /* gcc_spmi_cnoc_ahb_clk default on */
  /* gcc_spmi_ahb_clk default on */

  /* Enable all possible BIMC and DDR clocks, (K) means KPSS_Boot_Clk_CTL On.
     bimc_ddr_xo_clk_src, root auto enables. */

  HWIO_OUT(GCC_BIMC_MISC , 0x8150);
  
  Clock_ToggleClock(HWIO_GCC_PCNOC_DDR_CFG_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  
  /* gcc_ddr_dim_cfg_clk */
  Clock_ToggleClock(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* gcc_ddr_dim_sleep_clk */
  Clock_ToggleClock(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* gcc_bimc_ddr_jcpll0_clk */ 
  Clock_ToggleClock(HWIO_GCC_BIMC_DDR_CPLL0_CBCR_ADDR, CLK_TOGGLE_DISABLE);
  /* gcc_bimc_ddr_jcpll1_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_DDR_CPLL1_CBCR_ADDR, CLK_TOGGLE_DISABLE);
  /* gcc_bimc_ddr_jcpll0_clk */
   Clock_ToggleClock(HWIO_GCC_BIMC_DDR_CPLL0_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  /* gcc_bimc_ddr_jcpll1_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_DDR_CPLL1_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  
  /* gcc_bimc_ddr_ch0_clk : only CHO present on Shere */
  Clock_ToggleClock(HWIO_ADDR(GCC_BIMC_DDR_CH0_CBCR), CLK_TOGGLE_ENABLE);

  /* gcc_bimc_xo_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_XO_CBCR_ADDR, CLK_TOGGLE_ENABLE); 
  /* gcc_bimc_cfg_ahb_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE); 
  /* gcc_bimc_sleep_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE); 
  /* (K)gcc_bimc_sysnoc_axi_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE); 
  /* (K)gcc_bimc_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_CBCR_ADDR, CLK_TOGGLE_ENABLE); 

  /* Perform a BIMC clock switch to 166.8MHZ  (from GPLL5 ) with the 
   * FSM disabled, because DDR is not ready for switching yet for Bring-up */
  if( ! Clock_EnableSource( BIMCclkCfg.eSource )) return FALSE;
  if( ! Clock_ConfigMux(&BIMCclkCfg)) return FALSE;

  if( ! Clock_EnableSource( DDRclkCfg.eSource )) return FALSE;
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS, 1);
  

  if( ! Clock_ConfigMux(&DDRclkCfg)) return FALSE;

  /*
   * Trigger an update again so that the JCPLL selection stays in sync with 
   * the FSM state.
   */
  if(! Clock_TriggerUpdate(DDRclkCfg.nCMDCGRAddr, 
                           HWIO_FMSK(GCC_DDR_CMD_RCGR, UPDATE))) return FALSE;

  /*
   * Trigger the FSM update manually and wait for the frequency to switch.
   */
  if(! Clock_TriggerUpdate(HWIO_ADDR(GCC_BIMC_MISC),
                           HWIO_FMSK(GCC_BIMC_MISC, BIMC_FSM_DIS_DDR_UPDATE))) return FALSE;
  
  HWIO_OUTF(GCC_BIMC_GDSCR, RETAIN_FF_ENABLE, 1);
  /* gcc_ocmem_sys_noc_axi_clk : voted on previously */
  /* gcc_ocmem_noc_cfg_ahb_clk */
  // 8x26 removed HWIO_OUTF(GCC_OCMEM_NOC_CFG_AHB_CBCR, CLK_ENABLE, 1);

  
  /*Configuring gcc_bimc_gpu_clk to be atleast half of gcc_bimc_clk*/
  if( ! Clock_EnableSource( bimcgpuclkCfg.eSource )) return FALSE;
  if( ! Clock_ConfigMux(&bimcgpuclkCfg)) return FALSE;

 
  
  Clock_I2CInit();

  return TRUE;
}

/* ========================================================================
**  Function : Clock_PreDDRInit
** ======================================================================*/
/*
    Description: Clock_PreDDRInitEx() was added in 8974 to provide a separate API
    for emergency download (EDL) without including Mxdroop work around. Adding wrapper 
    function to avoid compilation erros in 8x26 and 8x10.

    @param None
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_PreDDRInit( uint32 ddr_type )
{
  Clock_SBLRailwayType *pClockRailway;
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();

  /* Initialize CX Rail ID */
  pClockRailway = Clock_RailwayConfig();
  pClockRailway->nCxRailId = rail_id(pClockRailway->CxRail);

  if(RAIL_NOT_SUPPORTED_BY_RAILWAY != pClockRailway->nCxRailId)
  {
    pClockRailway->CxVoter = railway_create_voter(pClockRailway->nCxRailId,RAILWAY_CLOCK_DRIVER_VOTER_ID);                                                            
  }
  else
  {
    return FALSE;
  }
  return Clock_PreDDRInitEx( ddr_type );
}

/* ========================================================================
**  Function : Clock_BIMCConfigFSM
** ======================================================================*/
/*
    Description: Configure BIMC to enable the DDR FSM.

    @param None
    @return None

    @dependencies
    None.

    @sa None
*/
void Clock_BIMCConfigFSM( void )
{
  /* Enable the FSM */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS, 0);

  HWIO_OUTF(GCC_BIMC_MISC, JCPLL_CLK_POST_DIV, 0xF);

  /* JCPLL is in bypass mode by default . We are using JCPLL 
   * For DDR at 667.2MHZ(nominal max). Hence pulling out JCPLL from Bypass mode */
  /* For Bring-up DDR@333.6MHZ JCPLL in bypass mode */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_JCPLL_BYPASS, 0x0);
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_DYN_LEGACY_MODE_EN, 0x0);  

  /* JCPLL clocks should be disabled after the FSM is turned on.
   * This gives the FSM control over these clocks */
  Clock_ToggleClock(HWIO_GCC_BIMC_DDR_CPLL0_CBCR_ADDR, CLK_TOGGLE_DISABLE);
  Clock_ToggleClock(HWIO_GCC_BIMC_DDR_CPLL1_CBCR_ADDR, CLK_TOGGLE_DISABLE);


  /* Enable DEHR, which is a data mover style device that saves and 
   * restores BIMC state. */
  Clock_ToggleClock(HWIO_GCC_DEHR_CBCR_ADDR, CLK_TOGGLE_ENABLE); 
}



/* ========================================================================
**  Function : Clock_BIMCIsFSMConfigured
** ======================================================================*/
/*
    Description: Find out if BIMC is configured enable the DDR FSM.

    @param None
    @return TRUE if FSM is enabled and FALSE otherwise.

    @dependencies
    None.

    @sa None
*/
boolean Clock_BIMCIsFSMConfigured( void )
{
  uint32 nVal;

  /* Enable the FSM */
  nVal = HWIO_INF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS);

  if (nVal == 0) 
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

/* ========================================================================
**  Function : Clock_ReplaceUSBBootClockEnable
** ======================================================================*/
/*
    Description: The register GCC_USB_BOOT_CLOCK_CTL turns on all the clocks
    necessary to access USB from the APPS.  This function must be ported
    on every new MSM.  It replaces that single register with all the clock
    enables that it replaced.  This allows for fine grained control of these
    clocks on an individual basis.

    @param None
    @return None

    @dependencies
    None.

    @sa None
*/
void Clock_ReplaceUSBBootClockEnable()
{
  /* Set the CLK_ENABLE for each clock that is enabled by GCC_USB_BOOT_CLOCK_CTL */
  Clock_ToggleClock(HWIO_GCC_SNOC_PCNOC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_PCNOC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_USB_HS_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_SYS_MM_NOC_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  
  /* Now disable the master boot control, allowing each of those above to replace */
  HWIO_OUTF(GCC_USB_BOOT_CLOCK_CTL, CLK_ENABLE, 0);
}

/* ============================================================================
**  Function : Clock_Init
** ============================================================================
*/
/*!

    This function turns on the required clocks and configures
    Fabric and Scorpion speeds depending on the System Fabric and
    CPU boot performance level.

    @param eSysPerfLevel   -  [in] Fabric and DDR performance level to initialize.
    @param eCPUPerfLevel   -  [in] Scropion CPU performance level to initialize.

    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

   @dependencies
    None.

*/

boolean Clock_Init
(
  ClockBootPerfLevelType eSysPerfLevel,
  ClockBootPerfLevelType eCPUPerfLevel
)
{
  Clock_SBLConfigType *cfg = Clock_SBLConfig();
  ClockConfigMuxType *RPMCfg;
  RPMCfg = &cfg->RPM_Cfg;
  
  if( ! Clock_SetCPUPerfLevel(eCPUPerfLevel)) return FALSE;
  //L2 is in sync in each cluster. CCI is for Cache coherency. Configure CCI perf level*/
  //if( ! Clock_SetL2PerfLevel(eCPUPerfLevel)) return FALSE; 
  if( ! Clock_SetSysPerfLevel(eSysPerfLevel)) return FALSE;
  if( ! Clock_InitCrypto()) return FALSE;

  /* SBL should configure GPLL0 ,GPLL1, GPLL2, BIMC_PLL, GPLL6 and A53 related PLLs */
  /* Configuring A53PLL and enabling early output but not using it in SBL as APPS gets configured to 800MHZ from GPLL0*/
  (void)Clock_ConfigureSource(SRC_GPLL0);
  (void)Clock_ConfigureSource(SRC_GPLL1);
  (void)Clock_ConfigureSource(SRC_GPLL2);
  (void)Clock_ConfigureSource(SRC_GPLL5); 
  (void)Clock_ConfigureSource(SRC_BIMCPLL);
  (void)Clock_ConfigureSource(SRC_GPLL6); 
  //(void)Clock_ConfigureSource(SRC_A53PWRPLL);
  (void)Clock_ConfigureSource(SRC_A53PERFPLL); 
  (void)Clock_ConfigureSource(SRC_A53CCIPLL); 

  Clock_ReplaceUSBBootClockEnable();

  /*
   * Configure RPM to 100MHz in case its source is 
   * configured to XO
   */
  if(HWIO_INF(GCC_RPM_CFG_RCGR, SRC_SEL) == 0x0)
  {
    if (!Clock_EnableSource(RPMCfg->eSource)) return FALSE;
    if (!Clock_ConfigMux(RPMCfg))  return FALSE;
  }

  return TRUE;
}

/* ============================================================================
**  Function : Clock_SetSysPerfLevel
** ============================================================================
*/
/**
  Configure System and Application Fabric to a perf level.

  @param eSysPerfLevel [in]  -  System NOC and DDR performance level to configure.

  @return
  TRUE -- Fabric was configured to perf level successful.
  FALSE -- Configuration failed.

  @dependencies
  None.

  @sa
  Clock_Init.
*/

boolean Clock_SetSysPerfLevel
(
  ClockBootPerfLevelType eSysPerfLevel
)
{
  /* Configure SysNOC, PCNOC */
  /* Configure BIMC FSM. BIMC already configured in Clock_PreDDRinit*/
  const ClockConfigMuxType *clkCfg;
  Clock_SBLConfigType *cfg = Clock_SBLConfig();

  if(eSysPerfLevel >= CLOCK_BOOT_PERF_NUM) return FALSE;

  /* Configure SNOC */
  clkCfg = &cfg->SNOC_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

   /* Configure SYSTEM MMNOC */
  clkCfg = &cfg->SYSMMNOC_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  if (!Clock_BIMCIsFSMConfigured()) 
  {
    /* Configure BIMC */
    Clock_BIMCConfigFSM();
  }
      

  /* Configure PCNOC */
  clkCfg = &cfg->PCNOC_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /*Configure Q6_TBU  and enable gcc_mss_q6_bimc_axi_clk which is derived from this mux*/
  clkCfg = &cfg->Q6TBU_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;
  
  
   /* Configure APSS TCU*/
  clkCfg = &cfg->APSSTCU_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /* Configuring APSS_AXI and providing GFMUX switch for gcc_bimc_apss_axi_clk */
  clkCfg = &cfg->APSSAXI_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;
  HWIO_OUTF(GCC_GCC_SPARE2_REG , BIMC_APSS_AXI_CLK_SRC_SEL , 0x1);

  return TRUE;
}

/* ============================================================================
**  Function : Clock_InitCrypto
** ============================================================================
*/
/*!
    Configure Crypto clocks.

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/

boolean Clock_InitCrypto(void)
{
  const ClockConfigMuxType *CECfg;
  Clock_SBLConfigType *cfg = Clock_SBLConfig();
  CECfg = &cfg->CE_Cfg;

  if( ! Clock_EnableSource( CECfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(CECfg)) return FALSE;

  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_CLK_ENA, 1);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_AXI_CLK_ENA, 1);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_AHB_CLK_ENA, 1);

  return TRUE;

} /* END Clock_InitCrypto */

/* ============================================================================
**  Function : Clock_InitForDownloadMode
** ============================================================================
*/
/*!
    Configure clocks for download.  Enable every RAM we want to dump.

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
void Clock_InitForDownloadMode(void)
{
  uint32 mask;

  /* Enable the necessary voteable KPSS and BIMC related clocks */
  mask = 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, SYS_NOC_APSS_AHB_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BIMC_APSS_AXI_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, APSS_AHB_CLK_ENA) | 
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, APSS_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, GMEM_SYS_NOC_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BOOT_ROM_AHB_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, SPMI_PCNOC_AHB_CLK_ENA);
    

  HWIO_OUTM(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, mask, mask);

   /* Enable Imem and Msg RAM clock */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA, 1);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA, 1);

  // Pronto TCM
  HWIO_OUTF(WCSS_A_PMU_COMMON_GDSCR, SW_COLLAPSE, 0);
  Clock_ToggleClock(HWIO_WCSS_A_PMU_COMMON_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  // LPASS LPM
  /* No separate LPASS power domain and No separate LPASS subsystem on 8916.*/
  //HWIO_OUTF(LPASS_AUDIO_CORE_GDSCR, SW_COLLAPSE, 0);
  Clock_ToggleClock(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_ADDR, CLK_TOGGLE_ENABLE);

}

/* ============================================================================
**  Function : Clock_ExitBoot
** ============================================================================
*/
/*!

    This function turns off clocks that were used during boot, but are not
    needed after boot.  This should be called at the end of boot.

    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

   @dependencies
    None.

*/
boolean Clock_ExitBoot(void)
{
  /* Calling it here because if done in Clock_Init then the configurations get overridden 
   * with sleep configs during DalTlmm_PostInit() in gpio_init().
   */ 
  Clock_ExtBuckGPIOMisc();  

  /* Disable UART related clocks */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_AHB_CLK_ENA, 0);

  /* Disable I2C related clocks */
  // 8x26 removed BLSP2
  //HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP2_AHB_CLK_ENA, 0);
  //HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP2_SLEEP_CLK_ENA, 0);
  //HWIO_OUTF(GCC_BLSP2_QUP4_I2C_APPS_CBCR, CLK_ENABLE, 0);

  /* Disable CE1 */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_CLK_ENA, 0);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_AXI_CLK_ENA, 0);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_AHB_CLK_ENA, 0);

  /* Disable PRNG */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, PRNG_AHB_CLK_ENA, 0);

  return TRUE;
}


/* ============================================================================
**  Function : Clock_BIMCQuery
** ============================================================================
*/
/*!
 
  This function lets the client query the BIMC frequency plan and the number 
  of supported frequencies.
  @return
  NONE
  @dependencies
  None.

*/


void Clock_BIMCQuery(ClockQueryType nQuery,void* pResource)
{
  uint32 nIdx,nCount;
  uint32 *pData;
  ClockPlanType *pBimcClkPlan;

  const Clock_SBLConfigType *cfg = Clock_SBLConfig();
  switch ( nQuery )
  {
    case CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS:
      pData = (uint32 *)pResource;
      if (pData == NULL) 
      {
        return;
      }
      *pData = 0;
      nCount = 0;
      for ( nIdx =0;(cfg->BIMC_Cfg[nIdx].nFrequency !=0); nIdx++ )
      {
        nCount ++;
      }
      
      *pData = nCount;
      break;
    case CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ:
      pBimcClkPlan = (ClockPlanType*)pResource;
      if (pBimcClkPlan == NULL) 
      {
        return;
      }
      for ( nIdx =0;(cfg->BIMC_Cfg[nIdx].nFrequency !=0); nIdx++ )
      {
        pBimcClkPlan[nIdx].nFreqKHz   = cfg->BIMC_Cfg[nIdx].nFrequency/1000;
        pBimcClkPlan[nIdx].eVRegLevel = cfg->BIMC_Cfg[nIdx].eVRegLevel;
      }
      break;
    default:
      return ;
  }
}
 
/* =========================================================================
**  Function : Clock_SwitchBusVoltage
** =========================================================================*/
/*

  This function is used for switching all the buses to different 
  voltage level for the ddr driver (LOW and HIGH) for DDR SITE
  training. 

  @param 
    eVoltageLevel - Clock Voltage Level (LOW and HIGH)

  @return
    None

  @dependencies
    None

  @sa
    None
*/


void Clock_SwitchBusVoltage( ClockVRegLevelType eVoltageLevel )
{

  Clock_SBLRailwayType *pClockRailway;

  pClockRailway = Clock_RailwayConfig();
  if (eVoltageLevel == CLOCK_VREG_LEVEL_LOW) 
  {
    Clock_SetCPUPerfLevel(CLOCK_BOOT_PERF_MIN);
    Clock_SetSysPerfLevel(CLOCK_BOOT_PERF_MIN);
    
    // setup the vote for corner voltage with railway.
    railway_corner_vote(pClockRailway->CxVoter, (railway_corner)eVoltageLevel);

    // Always need to transition the rail after updating the votes for them to be effective.
    railway_transition_rails();

  }
  else if (eVoltageLevel == CLOCK_VREG_LEVEL_HIGH) 
  {
    // setup the vote for corner voltage with railway.
    railway_corner_vote(pClockRailway->CxVoter, (railway_corner)eVoltageLevel);

    // Always need to transition the rail after updating the votes for them to be effective.
    railway_transition_rails();

    Clock_SetCPUPerfLevel(CLOCK_BOOT_PERF_MAX);
    Clock_SetSysPerfLevel(CLOCK_BOOT_PERF_MAX);
    
  } 

}

/* =========================================================================
**  Function : Clock_ConfigBIMCMux
** =========================================================================*/
/*!
    Configure a BIMC clock mux. BIMC clocks are configured differently
    compared to the other clocks.

    @param pConfig -  [IN] Clock mux config structure
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/

boolean Clock_ConfigBIMCMux (BimcMuxType* pBIMC)
{
  uint32 nCmdCGRAddr, nCmdCGRVal, nCfgCGRAddr, nCfgCGRVal;
  uint32 nSource, nPostDiv;
  const ClockConfigMuxType *pConfig;
  uint32 nTimeout = 0;
 
  /* Switch BIMC clock */
  pConfig = (const ClockConfigMuxType *)&pBIMC->BimcMux;
  if( ! Clock_ConfigMux(pConfig)) return FALSE;

  /* Switch BIMC GPU clock */
  pConfig = (const ClockConfigMuxType *)&pBIMC->BimcGPUMux;
  if( ! Clock_ConfigMux(pConfig)) return FALSE;

  /********************/
  /* Switch DDR clock */
  /********************/
  pConfig = (const ClockConfigMuxType *)&pBIMC->DDRMux;

  nSource = 0;

  if( ! Clock_SourceMapToMux( pConfig, &nSource)) return FALSE;
  
  // HW non-legacy mode
  if ( HWIO_INF(GCC_BIMC_MISC, BIMC_DDR_LEGACY_MODE_EN) != 1)
  {
  /*
   * BIMC config uses N value for RCG post-div.  Configure Post-Div
   */
    nPostDiv = (pConfig->nN > 0) ? pConfig->nN - 1 : 0;
    HWIO_OUTF(GCC_BIMC_MISC, JCPLL_CLK_POST_DIV, nPostDiv);

    /*
     * Use M value for Bypass
     */
    HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_JCPLL_BYPASS, pConfig->nM);
    HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_DYN_LEGACY_MODE_EN,pConfig->nM);
   }

  /*
   * now configure the DDR Mux
   */
  nCmdCGRAddr = pConfig->nCMDCGRAddr;
  nCmdCGRVal  = inpdw(nCmdCGRAddr);
  nCfgCGRAddr = pConfig->nCMDCGRAddr + CLOCK_CFG_REG_OFFSET; 
  nCfgCGRVal  = inpdw(nCfgCGRAddr);

  /*
   * Clear the fields
   */
  nCfgCGRVal &= ~(CLOCK_CFG_CGR_SRC_SEL_FMSK |
                  CLOCK_CFG_CGR_SRC_DIV_FMSK |
                  CLOCK_CFG_CGR_MODE_FMSK);


  /*
   * Program the source and divider values.
   */
  nCfgCGRVal |= (nSource << CLOCK_CFG_CGR_SRC_SEL_SHFT)
                  & CLOCK_CFG_CGR_SRC_SEL_FMSK;
  nCfgCGRVal |= ((HALF_DIVIDER(pConfig) << CLOCK_CFG_CGR_SRC_DIV_SHFT)
                  & CLOCK_CFG_CGR_SRC_DIV_FMSK);

  /*
   * Write the final CFG register value
   */
  outpdw(nCfgCGRAddr, nCfgCGRVal);

  /*
   * Trigger the update
   */
  nCmdCGRVal |= CLOCK_CMD_CFG_UPDATE_FMSK;
  outpdw(nCmdCGRAddr, nCmdCGRVal);


  /*
   * Wait until update finishes
   */
  while(inpdw(nCmdCGRAddr) & CLOCK_CMD_CFG_UPDATE_FMSK)
  {
    if (nTimeout++ >= CLOCK_UPDATE_TIMEOUT_US)
    {
      return FALSE;
    }

    busywait(1);
  }

  return TRUE;

} /* END Clock_ConfigBIMCMux */

/* =========================================================================
**  Function : Clock_SetBIMCSpeed
** =========================================================================*/
/*!
    Toggle between GPLL5/BIMC PLL if required and select the correct BIMC MUX configurations

    @param nFreqKHz - gcc_bimc_clk freq to be configured
    @return
    TRUE -- Configuration was successful.
    FALSE -- Configuration failed.

    @dependencies
    None.

    @sa None
*/


boolean Clock_SetBIMCSpeed(uint32 nFreqKHz )
{
  uint32 nIdx;
  uint32 nFreqHz;
  boolean bResult;
  Clock_SBLRailwayType *pClockRailway;
  ClockConfigMuxType CurrentCfg;
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();
  const ClockConfigPLLType *pCurrentPLLCfg;
  BIMCClockCfgType BimcCfg;
  ClockSourceConfigType NewSrcCfg;
  pClockRailway = Clock_RailwayConfig();
  nFreqHz = nFreqKHz*1000;
  pCurrentPLLCfg = NULL;
  bResult = TRUE;
  
  for ( nIdx =0;(cfg->BIMC_Cfg[nIdx].nFrequency !=0); nIdx++ )
  { 
    if(nFreqHz == cfg->BIMC_Cfg[nIdx].nFrequency)
    {
      /* 
       * Vote for higher voltage before the switch if the new frequency is more
       * than the current frequency.
       */
      if (bimc_speed_khz < nFreqKHz) 
      {

        // setup the vote for corner voltage with railway.
       railway_corner_vote(pClockRailway->CxVoter, (railway_corner)cfg->BIMC_Cfg[nIdx].eVRegLevel);

        // Always need to transition the rail after updating the votes for them to be effective.
       railway_transition_rails();

      }

      BimcCfg = cfg->BIMC_Cfg[nIdx];
      
      CurrentCfg.nCMDCGRAddr = HWIO_ADDR(GCC_BIMC_DDR_CMD_RCGR);
      if(!Clock_DetectMuxCfg(&(cfg->BIMC_Cfg[nIdx].Mux.BimcMux),&CurrentCfg))
      {
        return FALSE;
      }

      /* If frequency to be configured is not GPLL0 then we need to toggle between GPLL2 and GPLL3 */
      if ((cfg->BIMC_Cfg[nIdx].pSourceConfig->ePLL != SRC_GPLL0) && (cfg->BIMC_Cfg[nIdx].pSourceConfig->ePLL != SRC_CXO))
      {
        /* 
         * We want to configure the source based on which pll is configured for bimc.
         */
        if(CurrentCfg.eSource == SRC_GPLL5)
        {
          /* 
           * New config is SRC_BIMCPLL
           */
          NewSrcCfg.PLLCfg               =  cfg->BIMC_Cfg[nIdx].pSourceConfig->PLLCfg;
          NewSrcCfg.ePLL                 =  SRC_BIMCPLL;
          NewSrcCfg.PLLCfg.nPLLModeAddr  =  HWIO_ADDR(GCC_BIMC_PLL_MODE); 
          NewSrcCfg.PLLCfg.nVoteAddr     =  0; 
          NewSrcCfg.PLLCfg.nVoteMask     =  0;
          BimcCfg.pSourceConfig          =  &NewSrcCfg;
          BimcCfg.Mux.BimcMux.eSource    =  SRC_BIMCPLL;
          BimcCfg.Mux.DDRMux.eSource     =  SRC_BIMCPLL;
          pCurrentPLLCfg = &cfg->PLL5_Cfg;
        }
        else
        {
          /* 
           * New config is GPLL5
           */
          NewSrcCfg.PLLCfg               =  cfg->BIMC_Cfg[nIdx].pSourceConfig->PLLCfg;
          NewSrcCfg.ePLL                 =  SRC_GPLL5;
          NewSrcCfg.PLLCfg.nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL5_MODE); 
          NewSrcCfg.PLLCfg.nVoteAddr     =  0; 
          NewSrcCfg.PLLCfg.nVoteMask     =  0;
          BimcCfg.pSourceConfig          =  &NewSrcCfg;
          BimcCfg.Mux.BimcMux.eSource    =  SRC_GPLL5;
          BimcCfg.Mux.DDRMux.eSource     =  SRC_GPLL5;
          pCurrentPLLCfg = &cfg->BIMCPLL_Cfg;
        }
      }
      if(CurrentCfg.eSource == SRC_GPLL0)
      {
        pCurrentPLLCfg = &cfg->PLL0_Cfg;
      }
      
      if(!Clock_IsPLLEnabled(&BimcCfg.pSourceConfig->PLLCfg))
      {
        bResult = Clock_ConfigurePLL(&BimcCfg.pSourceConfig->PLLCfg); 
        if (bResult == TRUE) 
        {
          bResult = Clock_EnablePLL(&BimcCfg.pSourceConfig->PLLCfg);   
        }  
      }
      
      if (bResult == FALSE) 
      {
        return bResult;
      }
      if (!Clock_BIMCIsFSMConfigured()) 
      {
        /* Configure BIMC */
        Clock_BIMCConfigFSM();
      }
      

      //ddr_pre_clock_switch( ddr_speed_khz, nFreqKHz, SDRAM_INTERFACE_0 );
      //ddr_pre_clock_switch( ddr_speed_khz, nFreqKHz, SDRAM_INTERFACE_1 );
       
      HWIO_OUTF2(GCC_DLL_TIMER,DLL_LOCK_TIMER_VAL,DLL_PER_TIMER_VAL,0x60,0x60);
      HWIO_OUTF2(GCC_DLL_TIMER,DLL_LOCK_TIMER_EN,DLL_PER_TIMER_EN,0x1,0x1); 

       
      if(!Clock_ConfigBIMCMux(&BimcCfg.Mux)) 
      {
        return FALSE;
      }
      if (((CurrentCfg.eSource != SRC_GPLL0) && (CurrentCfg.eSource != SRC_CXO)) && (pCurrentPLLCfg != NULL))
      {
        Clock_DisablePLL(pCurrentPLLCfg);
      }
         
      /* Inform BIMC that new clock already switched */
      //ddr_post_clock_switch( ddr_speed_khz, nFreqKHz, SDRAM_INTERFACE_0 );
      //ddr_post_clock_switch( ddr_speed_khz, nFreqKHz, SDRAM_INTERFACE_1 );
     
      /* 
       * Vote for lower voltage after the switch if the new frequency is less
       * than the current frequency.
       */
      if (bimc_speed_khz > nFreqKHz) 
      {

        // setup the vote for corner voltage with railway.
       railway_corner_vote(pClockRailway->CxVoter, (railway_corner)cfg->BIMC_Cfg[nIdx].eVRegLevel);

        // Always need to transition the rail after updating the votes for them to be effective.
       railway_transition_rails();

      }
      bimc_speed_khz = nFreqKHz;
      ddr_speed_khz =  (bimc_speed_khz * 2);
      return TRUE;
    }
  }
  return FALSE;
}
