#ifndef CLOCKCONFIG_H
#define CLOCKCONFIG_H
/*
===========================================================================
*/
/**
  @file ClockSBLConfig.h

  Internal header file for the SBL configuration data structures.
*/
/*
  ====================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/msm8929/src/ClockSBLConfig.h#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  when       who     what, where, why
  --------   ---     -------------------------------------------------

  ====================================================================
*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBoot.h"
#include "ClockSBLCommon.h"
#include "railway.h"

/*=========================================================================
      References
==========================================================================*/

/*=========================================================================
      Definitions
==========================================================================*/


/*=========================================================================
      Type Definitions
==========================================================================*/

/* Data structure for SBL configuration data */
typedef struct
{
  /* PLL configurations */
  ClockConfigPLLType PLL0_Cfg;
  ClockConfigPLLType PLL1_Cfg;
  ClockConfigPLLType PLL2_Cfg;
  ClockConfigPLLType PLL5_Cfg;
  ClockConfigPLLType BIMCPLL_Cfg;
  ClockConfigPLLType PLL6_Cfg;
  //ClockConfigPLLType A53PWRPLL_Cfg;
  ClockConfigPLLType A53PERFPLL_Cfg;
  ClockConfigPLLType A53CCIPLL_Cfg;
  /* Configurations for CPU */
  ClockConfigMuxType CPU_Cfg[CLOCK_BOOT_PERF_NUM];

 /* Configurations for CCI */
  ClockConfigMuxType CCI_Cfg[CLOCK_BOOT_PERF_NUM]; 
                    
  /* System NOC config data */
  ClockConfigMuxType SNOC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* System MMNOC config data */
  ClockConfigMuxType SYSMMNOC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* PCNOC config data */
  ClockConfigMuxType PCNOC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* SDC table (for backwards compatibility) */
  uint32 SDC_Cfg[CLOCK_BOOT_PERF_NUM];
  
  /* SDC extended configurations */
  ClockSDCCfgType SDC_Ext_Cfg[6];

  /* Crypto clock config */
  ClockConfigMuxType CE_Cfg;

  /* USB_HS clock config */
  ClockConfigMuxType USBHS_Cfg;
  
  /* UART clock config */
  ClockConfigMuxType UART_Cfg[CLOCK_BOOT_PERF_NUM];
  
  /*RPM clock config*/
  ClockConfigMuxType RPM_Cfg;

  /* I2C clock config */
  ClockConfigMuxType I2C_Cfg[CLOCK_BOOT_PERF_NUM];

  /*Q6TBU clock config*/
  ClockConfigMuxType Q6TBU_Cfg[CLOCK_BOOT_PERF_NUM];

  /*APSS_AXI clock config*/
  ClockConfigMuxType APSSAXI_Cfg[CLOCK_BOOT_PERF_NUM];

  /* APSS_TCU configuration */
  ClockConfigMuxType APSSTCU_Cfg[CLOCK_BOOT_PERF_NUM];

  /* BIMC clock config
   * At the very minimum we need 2 entries.
   */
  BIMCClockCfgType BIMC_Cfg[9];

} Clock_SBLConfigType;

/* Data structure for Railway data */
typedef struct
{
  char*             CxRail;
  int               nCxRailId;
  railway_voter_t   CxVoter;
}Clock_SBLRailwayType;

extern Clock_SBLConfigType *Clock_SBLConfig( void );

extern Clock_SBLRailwayType *Clock_RailwayConfig( void );
extern boolean Clock_EnableSource( ClockSourceType eSource );
extern boolean Clock_ConfigureSource( ClockSourceType eSource );

boolean Clock_SourceMapToMux
(
  const ClockConfigMuxType *pConfig,
  uint32 *nMuxValue
);

boolean Clock_MuxMapToSource
(
  ClockConfigMuxType *pConfig,
  uint32 nSource
);

#endif /* !CLOCKCONFIG_H */

