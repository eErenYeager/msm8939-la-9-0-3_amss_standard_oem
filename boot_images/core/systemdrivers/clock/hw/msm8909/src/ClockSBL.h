#ifndef CLOCKSBL_H
#define CLOCKSBL_H

/*
===========================================================================
*/
/**
  @file ClockSBL.h

  Internal header file for the clock device driver for the Secondary Boot Loader.
*/
/*
  ====================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/msm8909/src/ClockSBL.h#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  when        who     what, where, why
  --------    ---     -------------------------------------------------
  2012/03/13  bc      Initial revision.
  ====================================================================
*/

#include "ClockSBLConfig.h"
#include "ClockSBLCommon.h"

#endif

