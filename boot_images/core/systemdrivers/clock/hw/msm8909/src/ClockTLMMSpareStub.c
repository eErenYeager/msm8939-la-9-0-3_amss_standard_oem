/** @file ClockTlmmSpare.c
   
  This is a stub file to avoid compilation for EMMC.

  Copyright (c) 2014, Qualcomm Technologies Inc. All rights reserved.
  
**/
/*=============================================================================
                              EDIT HISTORY

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/msm8909/src/ClockTLMMSpareStub.c#1 $ 
  $DateTime: 2015/03/19 01:58:37 $ 
  $Author: pwbldsvc $ 

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------
 18/02/14   mkumari     Initial version

=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/
#include "com_dtypes.h"
/* ========================================================================
**  Function : Clock_ExtBuck_GPIO_Misc
** ======================================================================*/
/*
    Description: This is a stub API to avoid compilation for EMMC.

    @param NONE
    @return
    TRUE  -- Configuration was success.
    FALSE -- Configuration failed.

    @dependencies
    None.

    @sa None
*/

boolean Clock_ExtBuckGPIOMisc(void)
{
  
    return TRUE;

}
