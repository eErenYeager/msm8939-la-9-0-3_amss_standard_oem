/*
===========================================================================
  @file ClockSBLCPU.c

  This file provides clock initialization for the Apps SBL.
===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/mdm9x45/src/ClockSBLCPU.c#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  =========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/
#include "ClockSBL.h"
#include "ClockHWIO.h"
#include "ClockSBLConfig.h"
#include "busywait.h"
#include "ClockSBLCommon.h"

/*=========================================================================
      Macro Definitions
==========================================================================*/

/*
 *
 * RCGR update timeout.
 */
#define RCGR_UPDATE_TIMEOUT_US 1000

/*
 *
 * Init time in microseconds after de-asserting PLL reset (normal mode).
 */
#define PLL_INIT_TIME_US 5

/*
 *
 * Warm-up time in microseconds after turning on the PLL.
 */
#define PLL_WARMUP_TIME_US 50

/*
 * PLL lock timeout (2x max locking time).
 */
#define PLL_LOCK_TIMEOUT_US 200

/*
 * PLL lock time after turning on PLL output.
 */
#define PLL_LOCKING_TIME_US 60

/*
 * Lock det status (APCS_CPU_PLL_STATUS bit0).
 */
#define PLL_LOCK_DET_STATUS 0x1


#define A7SSPLL_CONFIG_CTL_VALUE 0x40008529


/*=========================================================================
     Externs
==========================================================================*/

/*=========================================================================
      Function Prototypes
==========================================================================*/

/*=========================================================================
      Function Definitions
==========================================================================*/

/*=========================================================================
      Data
==========================================================================*/

/* ========================================================================
**  Function : Clock_CPUMuxSelect
** ======================================================================*/
/*
    Description: Configure CPU/L2 clock source

    @param
      nSource - source to be selected.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
static boolean Clock_CPUMuxSelect(uint32 nSource )
{
  uint32 nTimeout = 0;

  /* if APCS clock is on GPLL0, set APCS_CPU_PWR_CTL:b18
   * work around for HW clock gating bug before swicth clock.
   */
  HWIO_OUTF(APCS_CPU_PWR_CTL, CLK_EN, 0x4);

  HWIO_OUTF(APCS_CFG_RCGR, SRC_SEL, nSource);

  /*  Divider value is 2N-1. */
  HWIO_OUTF(APCS_CFG_RCGR, SRC_DIV, 1);

  /*
   * Trigger the update
   */
  HWIO_OUTF(APCS_CMD_RCGR, UPDATE, 1);

  /*
   * Wait until update finishes
   */
  while( HWIO_INF(APCS_CMD_RCGR, UPDATE))
  {
    if (nTimeout++ >= CLOCK_UPDATE_TIMEOUT_US)
    {
      return FALSE;
    }

    busywait(1);
  }
  return TRUE;

} /* END Clock_CPUMuxSelect */


/* ============================================================================
**  Function : Clock_SetCPUPerfLevel
** ============================================================================
*/
/**
  Configure Krait CPU to a specific perf level.

  @param eCPUPerfLevel [in] - CPU performance level.

  @return
  TRUE -- CPU was configured to perf level successful.
  FALSE -- Configuration failed.

  @dependencies
  None.

  @sa
  Clock_Init.
*/

boolean Clock_SetCPUPerfLevel(ClockBootPerfLevelType eCPUPerfLevel)
{
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();
  const ClockConfigCPUL2Type *CPU_cfg;
  ClockConfigPLLType A7SSPLL_cfg = {0};
  boolean bRetVal = FALSE;

  if( eCPUPerfLevel >= CLOCK_BOOT_PERF_NUM ) 
  {
      return bRetVal;
  }

  CPU_cfg = &cfg->CPU_Cfg[eCPUPerfLevel];


    /* Enable A7SS PLL. */
    A7SSPLL_cfg.nPLLModeAddr = HWIO_ADDR(APCS_CPU_PLL_MODE);
    A7SSPLL_cfg.nL = CPU_cfg->nL;
    A7SSPLL_cfg.nAlpha = CPU_cfg->nAlpha;
    A7SSPLL_cfg.nConfigCtl = A7SSPLL_CONFIG_CTL_VALUE ;
    if( ! Clock_EnablePLL(&A7SSPLL_cfg) )
    {
      return FALSE;
    }

  /*Configure HM RCG register to nSource and divider ratio.  */
  bRetVal = Clock_CPUMuxSelect(CPU_cfg->nSource );

  return bRetVal;
}/* END Clock_SetCPUPerfLevel */

/* ============================================================================
**  Function : Clock_SetL2PerfLevel
** ============================================================================
*/
/*!
    Configure L2 cache to a specific perf level.

    @param eL2PerfLevel   -  [IN] CPU performance level

    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_SetL2PerfLevel(ClockBootPerfLevelType eL2PerfLevel)
{

  const Clock_SBLConfigType *cfg = Clock_SBLConfig();
  const ClockConfigCPUL2Type *L2_cfg;
  ClockConfigPLLType A7SSPLL_cfg = {0};

  if( eL2PerfLevel >= CLOCK_BOOT_PERF_NUM ) return FALSE;

  L2_cfg = &cfg->L2_Cfg[eL2PerfLevel];

  /* Enable A7SS PLL if needed */
  if( L2_cfg->nL > 0)
  {
    A7SSPLL_cfg.nPLLModeAddr = HWIO_ADDR(APCS_CPU_PLL_MODE);
    A7SSPLL_cfg.nL = L2_cfg->nL;
    if( ! Clock_EnablePLL(&A7SSPLL_cfg) )
    {
      return FALSE;
    }
  }

  /* Configure HM RCG register to nSource and divider ratio. */
  Clock_CPUMuxSelect( L2_cfg->nSource );

  return TRUE;
}

