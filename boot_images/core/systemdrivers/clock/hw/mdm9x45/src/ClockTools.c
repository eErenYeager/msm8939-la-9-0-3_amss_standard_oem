/*
===========================================================================
  @file ClockSBLSDCC.c

  This file provides clock initialization for starting SDCC clocks at boot.
===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/mdm9x45/src/ClockTools.c#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  =========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/
#include "ClockSBL.h"
#include "ClockHWIO.h"
#include "ClockSBLConfig.h"

/* ============================================================================
**  Function : Clock_SetSDCClockFrequency
** ============================================================================
*/
/*!
    Configure SDC clock to a specific perf level.

    @param eClockPerfLevel   -  [IN] SDC Clock perf level
           eClock            -  [IN] SDC clock to configure
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_SetSDCClockFrequency
(
  ClockBootPerfLevelType ePerfLevel,
  ClockSDCType eClock
)
{
  ClockConfigMuxType SDCCCfg = {0};
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();

  if(ePerfLevel >= CLOCK_BOOT_PERF_NUM) return FALSE;

  /* Copy the config to the local so the nCMDCGRAddr can be modified. */
  SDCCCfg = cfg->SDC_Cfg[ePerfLevel];

  if( ! Clock_EnableSource( SDCCCfg.eSource )) return FALSE;

  /*
   * Enable access to the Peripheral NOC.
   */
  Clock_ToggleClock(HWIO_GCC_PCNOC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  switch (eClock)
  {
    case CLK_SDC1:
      SDCCCfg.nCMDCGRAddr = HWIO_ADDR(GCC_SDCC1_APPS_CMD_RCGR);
      if( ! Clock_ConfigMux(&SDCCCfg)) return FALSE;
      Clock_ToggleClock(HWIO_GCC_SDCC1_APPS_CBCR_ADDR, CLK_TOGGLE_ENABLE);
      Clock_ToggleClock(HWIO_GCC_SDCC1_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
      break;

    default:
      return FALSE;
  }

  return TRUE;

} /* END Clock_SetSDCClockFrequency */

/* ============================================================================
**  Function : Clock_InitUSB
** ============================================================================
*/
/*!
    Configure USB clocks.

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_InitUSB(void)
{
  if( HWIO_INF(GCC_SYS_NOC_USB3_AXI_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }
  if( HWIO_INF(GCC_USB30_MASTER_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB30_MASTER_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }
  if( HWIO_INF(GCC_USB30_SLEEP_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB30_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }
  if( HWIO_INF(GCC_USB30_MOCK_UTMI_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }
  if( HWIO_INF(GCC_USB_PHY_CFG_AHB_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }
  if( HWIO_INF(GCC_USB3_AUX_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB3_AUX_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }
   if( HWIO_INF(GCC_USB3_PIPE_CBCR, CLK_OFF))
  {
    if( ! Clock_ToggleClock(HWIO_GCC_USB3_PIPE_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;    
  }

  return TRUE;

} /* END Clock_InitUSB */

/* ============================================================================
**  Function : Clock_DisableUSB
** ============================================================================
*/
/*!
    Disable USB clocks.

    @param None.
    @return
    TRUE -- Disable was successful.
    FALSE -- Disable failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_DisableUSB(void)
{
  
  if( ! Clock_ToggleClock(HWIO_GCC_USB3_PIPE_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;  
  if( ! Clock_ToggleClock(HWIO_GCC_USB3_AUX_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;  
  if( ! Clock_ToggleClock(HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;    
  if( ! Clock_ToggleClock(HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;    
  if( ! Clock_ToggleClock(HWIO_GCC_USB30_SLEEP_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;    
  if( ! Clock_ToggleClock(HWIO_GCC_USB30_MASTER_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;    
  if( ! Clock_ToggleClock(HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_ADDR, CLK_TOGGLE_DISABLE)) return FALSE;    

  return TRUE;

}/* END Clock_DisableUSB */


/* ============================================================================
**  Function : Clock_Usb30SwitchPipeClk
** ============================================================================
*/
/*!
    Switch to USB3_PIPE_CLK.

    @param None.
    @return
    TRUE -- Switch was successful.
    FALSE -- Switch failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_Usb30SwitchPipeClk(void)
{

  /* A mux config for USB to PIPE Clk */
  ClockConfigMuxType USB30_PIPE_Cfg =
  {
    .nCMDCGRAddr = HWIO_ADDR(GCC_USB3_PIPE_CMD_RCGR),
    .eMux = MUX_GCC,
    .eSource = SRC_USB3_PIPE_CLK,
    .nDiv2x = 2,
    .nM = 0,
    .nN = 1,
    .n2D = 0
  };

  /* Swicth USB clock to CXO */
  if( ! Clock_ConfigMux(&USB30_PIPE_Cfg)) return FALSE;
  return TRUE;
}


/* ============================================================================
**  Function : Clock_Usb30DisableSWCollapse
** ============================================================================
*/
/*!
    Disable SW Collapse for USB30

    @param None.
    @return
    TRUE always

    @dependencies
    None.

    @sa None
   */
void Clock_Usb30DisableSWCollapse(boolean enable)
{
/* Disable SW PowerCollapse for USB30 */
  if(enable)
  {
    HWIO_OUTF(GCC_USB_30_GDSCR, SW_COLLAPSE, 0x0);
  }
  else
  {
    HWIO_OUTF(GCC_USB_30_GDSCR, SW_COLLAPSE, 0x1);
  }
}


/* ========================================================================
**  Function : Clock_EnableQPICForDownloadMode
** ======================================================================*/
/*
    Description: Configure QPIC(NAND) clocks at 100MHz on GPLL0 and enable
                 QPIC clocks.
                 The function is used in Download Mode where QPIC clocks are
                 not enabeld by PBL.

    @param None
    @return TRUE on success.

    @dependencies
    None.

    @sa None
*/
boolean Clock_EnableQPICForDownloadMode( void )
{

  /*
   * Clocks access to the Peripheral NOC enabled in Clock_ReplaceUSBBootClockEnable.
   */

  /*
   * Enable the QPIC clocks.
   */
  if( ! Clock_ToggleClock(HWIO_GCC_QPIC_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;
  if( ! Clock_ToggleClock(HWIO_GCC_QPIC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;
  if( ! Clock_ToggleClock(HWIO_GCC_QPIC_SYSTEM_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;

  return TRUE;

}
