/** @file ClockPIL.c

  This file implements clock device driver for second boot
  loader (SBL) stage.  The driver is responsible for configuring
  processor core and bus clocks.

  Copyright (c) 2012, Qualcomm Technologies Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/mdm9x45/src/ClockPIL.c#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

 when       who     what, where, why
 --------   ---     -----------------------------------------------------------

 08/06/12   vtw     Ported from TZ image.

=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/

#include <busywait.h>

#include "ClockPIL.h"
#include "ClockHWIO.h"
#include "ClockBoot.h"
#include "ClockSBLCommon.h"
#include "ClockSBL.h"

/*=========================================================================
      Macros
==========================================================================*/
#define PLL_TESLA_CONFIG_CTL 0x00004309

/*
 * LPASS_HSFS_MASK
 *
 * Power configuration for memories.
 */
#define HSFS_MASK \
  (HWIO_MSS_QDSP6SS_PWR_CTL_SLP_RET_N_BMSK | \
   HWIO_MSS_QDSP6SS_PWR_CTL_L2DATA_STBY_N_BMSK | \
   HWIO_MSS_QDSP6SS_PWR_CTL_ETB_SLP_NRET_N_BMSK | \
   HWIO_MSS_QDSP6SS_PWR_CTL_L2TAG_SLP_NRET_N_BMSK| \
   HWIO_MSS_QDSP6SS_PWR_CTL_L1IU_SLP_NRET_N_BMSK | \
   HWIO_MSS_QDSP6SS_PWR_CTL_L1DU_SLP_NRET_N_BMSK | \
   HWIO_MSS_QDSP6SS_PWR_CTL_L2PLRU_SLP_NRET_N_BMSK )

#define L2DATA_SLP_NRET_N_7_BMSK                    0x80
#define L2DATA_SLP_NRET_N_6_BMSK                    0x40
#define L2DATA_SLP_NRET_N_5_BMSK                    0x20
#define L2DATA_SLP_NRET_N_4_BMSK                    0x10
#define L2DATA_SLP_NRET_N_3_BMSK                    0x8
#define L2DATA_SLP_NRET_N_2_BMSK                    0x4
#define L2DATA_SLP_NRET_N_1_BMSK                    0x2
#define L2DATA_SLP_NRET_N_0_BMSK                    0x1

/*=========================================================================
      Function Prototypes
==========================================================================*/

static boolean Setup_MSSProcessor(void);

/*=========================================================================
      Functions
==========================================================================*/

/* =========================================================================
**  Function : Clock_SetupProcessor
** =========================================================================*/
/*
  See ClockPIL.h
*/

boolean Clock_SetupProcessor(ClockProcessorType eProcessorType)
{
  boolean bRetVal = FALSE;

  switch(eProcessorType)
  {
     case CLOCK_PROCESSOR_LPASS:
      break;
     case CLOCK_PROCESSOR_MODEM:
      bRetVal = Setup_MSSProcessor();
      break;

    default:
      break;
  }

  return bRetVal;

} /* END Clock_SetupProcessor */


/* =========================================================================
**  Function : Clock_EnableProcessor
** =========================================================================*/
/*
  See ClockPIL.h
*/

boolean Clock_EnableProcessor(ClockProcessorType eProcessorType)
{
  boolean bRetVal = FALSE;

  switch(eProcessorType)
  {
     case CLOCK_PROCESSOR_LPASS:
      bRetVal = FALSE;
      break;

     case CLOCK_PROCESSOR_MODEM:

     /* 
      * Change duData write ACC from default to relaxed.
      */
      HWIO_OUTF(MSS_QDSP6SS_STRAP_ACC, DATA, 0x20);
      HWIO_OUTF(MSS_QDSP6SS_RESET, STOP_CORE , 0x0);
      /*
       * Wait for core to come out of reset
       */
      busywait(5);
  
      
       HWIO_OUT(MSS_QDSP6SS_GFMUX_CTL,
           (HWIO_MSS_QDSP6SS_GFMUX_CTL_SRC_SWITCH_CLK_OVR_BMSK |\
            HWIO_MSS_QDSP6SS_GFMUX_CTL_CLK_ENA_BMSK ) );

      bRetVal = TRUE; 
       break;

      default:
       break;
  }

  return bRetVal;

} /* END Clock_EnableProcessor */

/* =========================================================================
**  Function : Setup_MSSProcessor
** =========================================================================*/
/**
  This function sets up clocks and bring MSS QDSP6 out of reset.

  @return
  TRUE  -- Operation is successful.\n
  FALSE -- Operation is unsuccessful.

  @dependencies
  None.
*/

static boolean Setup_MSSProcessor(void)
{

  uint32 nQ6ACKTimeout = 20000;

    /*
   * Enable MSS AHB.
   */
  HWIO_OUTF(GCC_MSS_CFG_AHB_CBCR, CLK_ENABLE, 0x1);
  while((HWIO_INF(GCC_MSS_CFG_AHB_CBCR, CLK_OFF) == 1) &&
          (nQ6ACKTimeout > 0))
  {
    busywait(5);
    nQ6ACKTimeout--;
  }

  /*
   * Failed to turn on MSS AHB clock, stop.
   */
  if(!nQ6ACKTimeout)
  {
    return FALSE;
  }

   /*
   * Enable Q6 BIMC AXI clock.
   */
  HWIO_OUTF(GCC_MSS_Q6_BIMC_AXI_CBCR, CLK_ENABLE, 1);

  /*
   *  Turn gcc_mss_gpll0_clk_src ON. This is required for Apps to access MSS registers.
   */
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSS_GPLL0_CLK_SRC_ENA, 1);
  /*
   * Restart Modem.
   */

  /*
   * Turn on XO clock. XO clock is required for BHS operation.
   */
   HWIO_OUTF(MSS_QDSP6SS_XO_CBCR, CLKEN, 0x1);

  /*
   * Turn-on BHS, clamp QDSP6 custom memory word line, clamp QDSP6 core IO.
   */
  HWIO_OUT(MSS_QDSP6SS_PWR_CTL, HWIO_MSS_QDSP6SS_PWR_CTL_BHS_ON_BMSK |\
                                HWIO_MSS_QDSP6SS_PWR_CTL_CLAMP_WL_BMSK|\
                                HWIO_MSS_QDSP6SS_PWR_CTL_CLAMP_IO_BMSK);
  busywait(1);
  /* 
   * Put LDO in bypass mode
   */
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, HWIO_MSS_QDSP6SS_PWR_CTL_LDO_BYP_BMSK,
            HWIO_MSS_QDSP6SS_PWR_CTL_LDO_BYP_BMSK);

  /*
   * Enable memories.First turn on QDSP6 memory except L2.
   */
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, HSFS_MASK, HSFS_MASK);
  busywait(2);

  /*
   * To avoid in-rush current, QDSP6 L2 memory should be turned on 256kB bank at a time.
   */
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, L2DATA_SLP_NRET_N_0_BMSK, L2DATA_SLP_NRET_N_0_BMSK);
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, L2DATA_SLP_NRET_N_1_BMSK, L2DATA_SLP_NRET_N_1_BMSK);
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, L2DATA_SLP_NRET_N_2_BMSK, L2DATA_SLP_NRET_N_2_BMSK);
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, L2DATA_SLP_NRET_N_3_BMSK, L2DATA_SLP_NRET_N_3_BMSK);
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, L2DATA_SLP_NRET_N_4_BMSK, L2DATA_SLP_NRET_N_4_BMSK);
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, L2DATA_SLP_NRET_N_5_BMSK, L2DATA_SLP_NRET_N_5_BMSK);
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, L2DATA_SLP_NRET_N_6_BMSK, L2DATA_SLP_NRET_N_6_BMSK);
  HWIO_OUTM(MSS_QDSP6SS_PWR_CTL, L2DATA_SLP_NRET_N_7_BMSK, L2DATA_SLP_NRET_N_7_BMSK);

  /*
   * Remove QDSP6 memory word line clamp.
   */
  HWIO_OUTF(MSS_QDSP6SS_PWR_CTL, CLAMP_WL, 0);
  /*
   * Remove QDSP6 I/O clamp.
   */
  HWIO_OUTF(MSS_QDSP6SS_PWR_CTL, CLAMP_IO, 0);

  /*
   * Release core out of reset.
   */
  HWIO_OUTF(MSS_QDSP6SS_RESET, BUS_ARES_ENA, 1 );
  HWIO_OUTF(MSS_QDSP6SS_RESET, STOP_CORE, 1 );
  HWIO_OUTF(MSS_QDSP6SS_RESET, CORE_ARES, 0 );

  busywait(1);

  return TRUE;

} /* END Setup_MSSProcessor */


