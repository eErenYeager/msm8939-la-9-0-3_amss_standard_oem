/*
===========================================================================
  @file ClockSBL.c

  This file provides clock initialization for the Apps SBL.
===========================================================================

  Copyright (c) 2012 QUALCOMM Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  =========================================================================

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/mdm9x45/src/ClockSBL.c#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $
  $Author: pwbldsvc $

  when           who     what, where, why
 --------    ---     -----------------------------------------------------------

07/22/13     pbitra    Created from 9x25.
11/21/13     pbitra    Removed the check in QPIC_Config to see if the clocks were turned on by PBL.
                       Reason:ehostdl download mode with boot config in forced usb option finds PBL 
                       not configuring the clock, and hence when enabled in SBL, finds them runing at XO.

  =========================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/
#include "ClockSBL.h"
#include "ClockHWIO.h"
#include "ClockSBLConfig.h"
#include "busywait.h"

/*=========================================================================
      Macro Definitions
==========================================================================*/

/*=========================================================================
     Externs
==========================================================================*/
/*
 * Variable to enable Dynamic Clock Divide.
 */
boolean Boot_Clock_DCD_DCG = TRUE;
/*=========================================================================
      Function Prototypes
==========================================================================*/

boolean Clock_InitCrypto(void);
boolean Clock_I2CInit( void );
void Clock_EnableDCDAndDCG(void);

/*=========================================================================
      Function Definitions
==========================================================================*/

/*=========================================================================
      Data
==========================================================================*/
struct Clock_I2CRegType 
{
  uint32 cmd;
  uint32 cbcr;
};

const struct Clock_I2CRegType Clock_I2CRegs[CLK_BLSP_QUP_I2C_NUM_CLKS] = 
{ 
  { 0,0 },
  { HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CBCR) },
  { HWIO_ADDR(GCC_BLSP1_QUP2_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP2_I2C_APPS_CBCR) },
  { HWIO_ADDR(GCC_BLSP1_QUP3_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP3_I2C_APPS_CBCR) },
  { HWIO_ADDR(GCC_BLSP1_QUP4_I2C_APPS_CMD_RCGR),  HWIO_ADDR(GCC_BLSP1_QUP4_I2C_APPS_CBCR) },
};

/*=========================================================================
      Function Definitions
=========================================================================*/

/* ============================================================================
**	Function : Clock_SetI2CClockFrequency
** ============================================================================
*/
/*!
	Configure QUP_I2C clock to a specific perf level.

	@param      ePerfLevel	 -	[IN] UART Clock perf level
		        eClock	 -	[IN] UART clock to configure
	@return
	TRUE -- Initialization was successful.
	FALSE -- Initialization failed.

	@dependencies
	None.

	@sa None
*/
boolean Clock_SetI2CClockFrequency
(
  ClockBootPerfLevelType ePerfLevel,
  ClockQUPI2CType eClock
)
{
  ClockConfigMuxType I2CCfg = {0};
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();

  if(ePerfLevel >= CLOCK_BOOT_PERF_NUM) return FALSE;
  if(eClock >= CLK_BLSP_QUP_I2C_NUM_CLKS) return FALSE;

  /* Copy the config to the local so the nCMDCGRAddr can be modified. */
  I2CCfg = cfg->I2C_Cfg[ePerfLevel];

  if( ! Clock_EnableSource( I2CCfg.eSource )) return FALSE;

  I2CCfg.nCMDCGRAddr = Clock_I2CRegs[eClock].cmd;
  if( ! Clock_ConfigMux(&I2CCfg)) return FALSE;
  Clock_ToggleClock(Clock_I2CRegs[eClock].cbcr, CLK_TOGGLE_ENABLE);

  /* Enable BLSP1 and BLSP2 (if present) AHB CLK */
  if (eClock <= CLK_BLSP1_QUP6_I2C_APPS)
  {
    HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_AHB_CLK_ENA, 1);
  }
 
 return TRUE;
} /* END Clock_SetI2CClockFrequency */

/* A utility function to get the sleep timetick */
static uint32 Clock_GetSclkTimetick()
{
  uint32 curr_count;
  uint32 last_count;

  /*Grab current time count*/
  curr_count = HWIO_IN(MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL);

  /*Keep grabbing the time until a stable count is given*/
  do
  {
    last_count = curr_count;
    curr_count = HWIO_IN(MPM2_MPM_SLEEP_TIMETICK_COUNT_VAL);
  } while (curr_count != last_count);

  return curr_count;
}

static void Clock_BusywaitSleepClk(uint32 sclk_ticks)
{
  uint32 now;

  now = Clock_GetSclkTimetick();
  while( Clock_GetSclkTimetick() - now <= sclk_ticks);
}

/* ============================================================================
**  Function : Clock_DDRSpeed
** ============================================================================
*/
/*!
    Return the DDR clock rate in kilohertz.  This clock rate is the bus speed.
    It is not the controller clock (2X) clock for targets that use Legacy mode.

    @param None.
    @return
      The speed configured in Clock_PreDDRInit() of the BIMC/DDR clock.

    @dependencies
    None.

    @sa None
*/

static uint32 ddr_speed_khz = 333333;

uint32 Clock_DDRSpeed()
{
  return ddr_speed_khz;
}

/* ========================================================================
**  Function : Clock_PreDDRInit
** ======================================================================*/
/*
    Description: Configure all clocks needed for DDR configuration.

    @param None
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_PreDDRInit( uint32 ddr_type)
{
  uint32 mask;

  /* A mux config for BIMC clock to 808.4 MHz */
  const ClockConfigMuxType  clkCfg =
  {
    .nCMDCGRAddr = HWIO_ADDR(GCC_BIMC_DDR_CMD_RCGR),
    .eMux = MUX_GCC,
    .eSource = SRC_GPLL2,
    .nDiv2x = 2,
    .nM = 0,
    .nN = 0,
    .n2D = 0
  };

  /* A mux config for RPM clock to 171.43 MHz*/
  const ClockConfigMuxType RPMCfg =
  {
    .nCMDCGRAddr = HWIO_ADDR(GCC_RPM_CMD_RCGR),
    .eMux = MUX_GCC,
    .eSource = SRC_GPLL0,
    .nDiv2x = 7,
    .nM = 0,
    .nN = 1,
    .n2D = 0
  };

  /* Temporary workaround for
     bootup issue by enabling QDSS_AT clock. */
//  Clock_ToggleClock(HWIO_GCC_SYS_NOC_AT_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* Enable sleep clock. */
  Clock_ToggleClock(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_BusywaitSleepClk(3);

  /* Configure RPM clock to 171MHz from GPLL0. */
  if( !Clock_EnableSource( RPMCfg.eSource ) )
  {
    return FALSE;
  }

  /* Set GPLL0 LV_AUX output. Required by APCS CPU clock to run on GPLL0. */
  HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX, 1);

  if( !Clock_ConfigMux( &RPMCfg ) )
  {
    return FALSE;
  }

  /* Enable the necessary voteable APSS and BIMC related clocks */
  mask =
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, SYS_NOC_APSS_AHB_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BIMC_APSS_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, APSS_AHB_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, APSS_AXI_CLK_ENA) ;

  HWIO_OUTM(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, mask, mask);

  /* Enable Imem and Msg RAM clock */
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA, 1);
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA, 1);
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSS_GPLL0_CLK_SRC_ENA, 1);

  /* SPMI clocks are already enabled by default on reset */
  /* gcc_spmi_ser_clk default on */
  /* gcc_spmi_cnoc_ahb_clk default on */
  /* gcc_spmi_ahb_clk default on */

  /* Enable all possible BIMC and DDR clocks, (K) means KPSS_Boot_Clk_CTL On.
     bimc_ddr_xo_clk_src, root auto enables. */

  Clock_ToggleClock(HWIO_GCC_PCNOC_DDR_CFG_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* gcc_ddr_dim_cfg_clk */
  Clock_ToggleClock(HWIO_GCC_DDR_DIM_CFG_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* gcc_ddr_dim_sleep_clk */
  Clock_ToggleClock(HWIO_GCC_DDR_DIM_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* gcc_bimc_xo_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_XO_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  /* gcc_bimc_cfg_ahb_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  /* gcc_bimc_sleep_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  /* (K)gcc_bimc_sysnoc_axi_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_SYSNOC_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  /* (K)gcc_bimc_clk */
  Clock_ToggleClock(HWIO_GCC_BIMC_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* Enable legacy mode */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_DDR_LEGACY_2X_MODE_EN, 1);

  /* Perform a BIMC clock switch to 333 MHz from GPLL3 with the
   * FSM disabled, because DDR is not ready for switching yet */
  if( ! Clock_EnableSource( clkCfg.eSource )) return FALSE;
  /* apc0 Aux Clock runs from AUX */
  HWIO_OUTF(GCC_GPLL2_USER_CTL, PLLOUT_LV_AUX, 1);
  
   /* Disbale BIMC FSM.  */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS, 1);
  if( ! Clock_ConfigMux(&clkCfg)) return FALSE;

  /*
   * Trigger an update again so that the JCPLL selection stays in sync with
   * the FSM state.
   */
  if(! Clock_TriggerUpdate(HWIO_ADDR(GCC_BIMC_DDR_CMD_RCGR),
                           HWIO_FMSK(GCC_BIMC_DDR_CMD_RCGR, UPDATE))) return FALSE;

  /*
   * Trigger the FSM update manually and wait for the frequency to switch.
   */
  if(! Clock_TriggerUpdate(HWIO_ADDR(GCC_BIMC_MISC),
                           HWIO_FMSK(GCC_BIMC_MISC, BIMC_FSM_DIS_DDR_UPDATE))) return FALSE;

  return TRUE;
}

/* ========================================================================
**  Function : Clock_QPICConfig
** ======================================================================*/
/*
    Description: Configure QPIC(NAND) clocks at 100MHz on GPLL0.

    @param None
    @return None

    @dependencies
    None.

    @sa None
*/
boolean Clock_QPICConfig( void )
{
  /* A mux config for QPIC clock to 100MHz*/
  const ClockConfigMuxType QPICCfg =
  {
    .nCMDCGRAddr = HWIO_ADDR(GCC_QPIC_CMD_RCGR),
    .eMux = MUX_GCC,
    .eSource = SRC_GPLL0,
    .nDiv2x = 12,  /* div6*/
    .nM = 0,
    .nN = 1,
    .n2D = 0
  };

  /* Configure QPIC clock to 100MHz */
  return Clock_ConfigMux( &QPICCfg ) ;

}



/* ========================================================================
**  Function : Clock_IPAConfig
** ======================================================================*/
/*
    Description: Configure IPA clocks at 150MHz on GPLL0.

    @param None
    @return None

    @dependencies
    None.

    @sa None
*/
boolean Clock_IPAConfig( void )
{
  /* A mux config for IPA clock to 150MHz*/
  const ClockConfigMuxType IPACfg =
  {
    .nCMDCGRAddr = HWIO_ADDR(GCC_IPA_CMD_RCGR),
    .eMux = MUX_GCC,
    .eSource = SRC_GPLL0,
    .nDiv2x = 8,  /* div4*/
    .nM = 0,
    .nN = 0,
    .n2D = 0
  };


  /* Configure IPA clock to 150MHz */
  if( ! Clock_ConfigMux(&IPACfg)) return FALSE;

  if( ! Clock_ToggleClock(HWIO_GCC_IPA_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;
  if( ! Clock_ToggleClock(HWIO_GCC_SYS_NOC_IPA_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;
  if( ! Clock_ToggleClock(HWIO_GCC_IPA_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;
  if( ! Clock_ToggleClock(HWIO_GCC_IPA_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE)) return FALSE;

  return TRUE;
}


/* ========================================================================
**  Function : Clock_BIMCConfigFSM
** ======================================================================*/
/*
    Description: Configure BIMC to enable the DDR FSM.

    @param None
    @return None

    @dependencies
    None.

    @sa None
*/
void Clock_BIMCConfigFSM( void )
{
  /* Enable the FSM */
  HWIO_OUTF(GCC_BIMC_MISC, BIMC_FRQSW_FSM_DIS, 0);

  /* Enable DEHR, which is a data mover style device that saves and
   * restores BIMC state. */
  Clock_ToggleClock(HWIO_GCC_DEHR_CBCR_ADDR, CLK_TOGGLE_ENABLE);
}

/* ========================================================================
**  Function : Clock_ReplaceUSBBootClockEnable
** ======================================================================*/
/*
    Description: The register GCC_USB_BOOT_CLOCK_CTL turns on all the clocks
    necessary to access USB from the APPS.  This function must be ported
    on every new MSM.  It replaces that single register with all the clock
    enables that it replaced.  This allows for fine grained control of these
    clocks on an individual basis.

    @param None
    @return None

    @dependencies
    None.

    @sa None
*/
void Clock_ReplaceUSBBootClockEnable()
{
  /* Set the CLK_ENABLE for each clock that is enabled by GCC_USB_BOOT_CLOCK_CTL */
  Clock_ToggleClock(HWIO_GCC_SYS_NOC_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_SNOC_PCNOC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_PCNOC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_SYS_NOC_USB3_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_USB30_MASTER_CBCR_ADDR, CLK_TOGGLE_ENABLE);  
  Clock_ToggleClock(HWIO_GCC_USB30_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);   
  Clock_ToggleClock(HWIO_GCC_USB30_MOCK_UTMI_CBCR_ADDR, CLK_TOGGLE_ENABLE);  
  Clock_ToggleClock(HWIO_GCC_USB_PHY_CFG_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);  
  Clock_ToggleClock(HWIO_GCC_USB3_PIPE_CBCR_ADDR, CLK_TOGGLE_ENABLE); 
  Clock_ToggleClock(HWIO_GCC_USB3_AUX_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* Now disable the master boot control, allowing each of those above to replace */
 HWIO_OUTF(GCC_USB_BOOT_CLOCK_CTL, CLK_ENABLE, 0);
}

/* ============================================================================
**  Function : Clock_Init
** ============================================================================
*/
/*!

    This function turns on the required clocks and configures
    Fabric and Scorpion speeds depending on the System Fabric and
    CPU boot performance level.

    @param eSysPerfLevel   -  [in] Fabric and DDR performance level to initialize.
    @param eCPUPerfLevel   -  [in] Scropion CPU performance level to initialize.

    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

   @dependencies
    None.

*/

boolean Clock_Init
(
  ClockBootPerfLevelType eSysPerfLevel,
  ClockBootPerfLevelType eCPUPerfLevel
)
{
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();

  if( ! Clock_SetCPUPerfLevel(eCPUPerfLevel)) return FALSE;

  if( ! Clock_SetL2PerfLevel(eCPUPerfLevel)) return FALSE;
  if( ! Clock_SetSysPerfLevel(eSysPerfLevel)) return FALSE;
  if( ! Clock_InitCrypto()) return FALSE;

  (void)Clock_ConfigureSource(SRC_GPLL0);
  /* apc0 Aux clock runs from  AUX */
  HWIO_OUTF(GCC_GPLL0_USER_CTL, PLLOUT_LV_AUX, 1);

  (void)Clock_ConfigureSource(SRC_GPLL1);
  /* apc0 Aux clock runs from  AUX */
  HWIO_OUTF(GCC_GPLL1_USER_CTL, PLLOUT_LV_AUX, 1);
  
  (void)Clock_ConfigureSource(SRC_GPLL2);
  /* apc0 Aux clock runs from AUX */
  HWIO_OUTF(GCC_GPLL2_USER_CTL, PLLOUT_LV_AUX, 1);

  (void)Clock_ConfigureSource(SRC_GPLL3);
  /* apc0 Aux clock runs from AUX */
  HWIO_OUTF(GCC_GPLL3_USER_CTL, PLLOUT_LV_AUX, 1);

  Clock_ReplaceUSBBootClockEnable();
 
  if( ! Clock_QPICConfig()) return FALSE;
  if( ! Clock_IPAConfig()) return FALSE;

  /*
   * Enable HW clock gating.
   */
  if (Boot_Clock_DCD_DCG == TRUE)
  {
    Clock_EnableDCDAndDCG();
  }
  return TRUE;

}

/* ============================================================================
**  Function : Clock_SetSysPerfLevel
** ============================================================================
*/
/**
  Configure System and Application Fabric to a perf level.

  @param eSysPerfLevel [in]  -  System NOC and DDR performance level to configure.

  @return
  TRUE -- Fabric was configured to perf level successful.
  FALSE -- Configuration failed.

  @dependencies
  None.

  @sa
  Clock_Init.
*/

boolean Clock_SetSysPerfLevel
(
  ClockBootPerfLevelType eSysPerfLevel
)
{
  /* Configure SysNOC, PCNOC */
  /* Configure BIMC FSM. BIMC already configured in Clock_PreDDRinit*/
  const ClockConfigMuxType *clkCfg;
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();

  if(eSysPerfLevel >= CLOCK_BOOT_PERF_NUM) return FALSE;

  /* Configure SNOC */
  clkCfg = &cfg->SNOC_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  /* Configure BIMC */
  Clock_BIMCConfigFSM();

  /* Configure PCNOC */
  clkCfg = &cfg->PCNOC_Cfg[eSysPerfLevel];
  if( ! Clock_EnableSource( clkCfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(clkCfg)) return FALSE;

  return TRUE;
}

/* ============================================================================
**  Function : Clock_InitCrypto
** ============================================================================
*/
/*!
    Configure Crypto clocks.

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/

boolean Clock_InitCrypto(void)
{
  const ClockConfigMuxType *CECfg;
  const Clock_SBLConfigType *cfg = Clock_SBLConfig();
  CECfg = &cfg->CE_Cfg;

  if( ! Clock_EnableSource( CECfg->eSource )) return FALSE;
  if( ! Clock_ConfigMux(CECfg)) return FALSE;

  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_CLK_ENA, 1);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_AXI_CLK_ENA, 1);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_AHB_CLK_ENA, 1);

  return TRUE;

} /* END Clock_InitCrypto */


/* ============================================================================
**  Function : Clock_InitForDownloadMode
** ============================================================================
*/
/*!
    Configure clocks for download.  Enable every RAM we want to dump.

    @param None.
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
void Clock_InitForDownloadMode(void)
{
  uint32 mask;

  /* Enable the necessary voteable KPSS and BIMC related clocks */
  mask =
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, SYS_NOC_APSS_AHB_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BIMC_APSS_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, APSS_AHB_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, APSS_AXI_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BOOT_ROM_AHB_CLK_ENA) |
    HWIO_FMSK(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, SPMI_PCNOC_AHB_CLK_ENA);

  HWIO_OUTM(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, mask, mask);

  /* Enable Imem and Msg RAM clock */
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, IMEM_AXI_CLK_ENA, 1);
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA, 1);

  // LPASS LPM
  /* No separate LPASS power domain and No separate LPASS subsystem on 9x45.*/
  Clock_ToggleClock(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_ULTAUDIO_AHBFABRIC_IXFABRIC_LPM_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  /* Enable IMEM AXI clock. For V1, set CLK_ENABLE(bit0). */
  Clock_ToggleClock(HWIO_GCC_IMEM_AXI_CBCR_ADDR, CLK_TOGGLE_ENABLE);

}

/* ========================================================================
**  Function : Clock_I2CInit
** ======================================================================*/
/*
    Description: Configure all clocks needed for EEPROM to be used Pre DDR.

    @param None
    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

    @dependencies
    None.

    @sa None
*/
boolean Clock_I2CInit( void )
{
  /* Enabled QUP3 I2C Apps clk (fixed at 19.2) and vote for the AHB */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_AHB_CLK_ENA, 1);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_SLEEP_CLK_ENA, 1);
  Clock_ToggleClock(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  // Turn  on the GCC_BLSP1_QUP2_I2C_APPS_CBCR
  HWIO_OUTF(GCC_BLSP1_QUP2_I2C_APPS_CBCR, CLK_ENABLE, 1);

  return TRUE;
}

/* ============================================================================
**  Function : Clock_ExitBoot
** ============================================================================
*/
/*!

    This function turns off clocks that were used during boot, but are not
    needed after boot.  This should be called at the end of boot.

    @return
    TRUE -- Initialization was successful.
    FALSE -- Initialization failed.

   @dependencies
    None.

*/
boolean Clock_ExitBoot(void)
{
  // Turn  on the GCC_BLSP1_QUP2_I2C_APPS_CBCR
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_AHB_CLK_ENA, 0);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, BLSP1_SLEEP_CLK_ENA, 0);
  Clock_ToggleClock(HWIO_GCC_BLSP1_QUP3_I2C_APPS_CBCR_ADDR, CLK_TOGGLE_DISABLE);
  HWIO_OUTF(GCC_BLSP1_QUP2_I2C_APPS_CBCR, CLK_ENABLE, 0);

  /* Disable CE1 */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_CLK_ENA, 0);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_AXI_CLK_ENA, 0);
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, CRYPTO_AHB_CLK_ENA, 0);

  /* Disable PRNG */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, PRNG_AHB_CLK_ENA, 0);

  return TRUE;
}

/* ============================================================================
**  Function : Clock_DebugInit
** ============================================================================
*/
/*!

    This function is called very early in boot.  It is used for work-arounds that
    need to be done before JTAG attaches at the SBL "hold focus button" spin loop.

    @return
    TRUE -- Clean up was successful.
    FALSE -- Clean up failed.

   @dependencies
    You cannot use busywait in this function.

*/
boolean Clock_DebugInit(void)
{
  /* Ensure that the debug interface is reset upon IPA block power up */
  /* Disable SW PowerCollapse for IPA */
//  HWIO_OUTF(GCC_IPA_GDSCR, SW_COLLAPSE, 0);
 // while(HWIO_INF(GCC_IPA_GDSCR, PWR_ON) == 0);  // loop while off

  Clock_ToggleClock(HWIO_GCC_GCC_IM_SLEEP_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_BusywaitSleepClk(3);

  /* Reset QDSS very early in the boot sequence */
  HWIO_OUT(GCC_QDSS_BCR, 0x1);
  Clock_BusywaitSleepClk(3);
  HWIO_OUT(GCC_QDSS_BCR, 0x0);
  Clock_BusywaitSleepClk(3);

 /* Needed for Random Stack Canary */
  HWIO_OUTF(GCC_APCS_CLOCK_BRANCH_ENA_VOTE, PRNG_AHB_CLK_ENA, 1);
  HWIO_OUTF(GCC_RPM_CLOCK_BRANCH_ENA_VOTE, MSG_RAM_AHB_CLK_ENA, 1);

  /*Required for PcIE Boot*/
  Clock_ToggleClock(HWIO_GCC_SNOC_PCNOC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);
  Clock_ToggleClock(HWIO_GCC_PCNOC_AHB_CBCR_ADDR, CLK_TOGGLE_ENABLE);

  return TRUE;
}

/* ============================================================================
**  Function : Clock_BIMCQuery
** ============================================================================
*/
/*!
 
  This function lets the client query the BIMC frequency plan and the number 
  of supported frequencies.
  @return
  NONE
  @dependencies
  None.

*/
void Clock_BIMCQuery(ClockQueryType nQuery,void* pResource)
{
  return ;
 
}

/**
  This function is used for switching all the buses to different 
  voltage level for the ddr driver (LOW and HIGH) for DDR SITE
  training. 

  @param 
    eVoltageLevel - Clock Voltage Level (LOW and HIGH)

  @return
    None

  @dependencies
    None

  @sa
    None
*/
void Clock_SwitchBusVoltage( ClockVRegLevelType eVoltageLevel )
{
  return;
}

boolean Clock_SetBIMCSpeed(uint32 nFreqKHz )
{
  return TRUE;
}

/* ============================================================================
**	Function : Clock_DisableI2CClock
** ============================================================================
*/
/*!
	Disable  specific I2C clock.

	@param    eClock	-	[IN] I2C clock to Disable
	@return
	TRUE -- Clock disable was successful.
	FALSE -- Clock disable failed.

	@dependencies
	None.

	@sa None
*/
boolean Clock_DisableI2CClock(ClockQUPI2CType eClock)
{
  if(eClock >= CLK_BLSP_QUP_I2C_NUM_CLKS) return FALSE;
  Clock_ToggleClock(Clock_I2CRegs[eClock].cbcr, CLK_TOGGLE_DISABLE);
  
  return TRUE;
}

void Clock_EnableDCDAndDCG (void)
{
  /* Enable Dynamic Clock Divide */
  HWIO_OUTF(GCC_SNOC_DCD_CONFIG,ALLOWED_DIV,1);
  HWIO_OUTF(GCC_SNOC_DCD_CONFIG,DCD_ENABLE,1);
  HWIO_OUTF(GCC_PCNOC_DCD_CONFIG,ALLOWED_DIV,1);
  HWIO_OUTF(GCC_PCNOC_DCD_CONFIG,DCD_ENABLE,1);

  /* Enable HW Dynamic Clock Gating */
  HWIO_OUTF(GCC_MSS_Q6_BIMC_AXI_CBCR,HW_CTL,0x1);
  HWIO_OUTF(GCC_RPM_TIMER_CBCR,HW_CTL,0x1);
  HWIO_OUTF(GCC_QPIC_AHB_CBCR,HW_CTL,0x1);

  return;
}

