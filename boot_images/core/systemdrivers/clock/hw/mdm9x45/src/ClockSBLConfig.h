#ifndef CLOCKCONFIG_H
#define CLOCKCONFIG_H
/*
===========================================================================
*/
/**
  @file ClockSBLConfig.h

  Internal header file for the SBL configuration data structures.
*/
/*
  ====================================================================

  Copyright (c) 2011 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/mdm9x45/src/ClockSBLConfig.h#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  when       who     what, where, why
  --------   ---     -------------------------------------------------

  ====================================================================
*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockBoot.h"
#include "ClockSBLCommon.h"

/*=========================================================================
      References
==========================================================================*/

/*=========================================================================
      Definitions
==========================================================================*/

#define SRC_A7SSPLL                        0x5


/*=========================================================================
      Type Definitions
==========================================================================*/

/*
 * ClockConfigCPUL2Type
 *
 * Parameters used for configuring a standard clock multiplexer.
 *
 *  nSource     - The source to use.
 *  nL          - The L value of the HFPLL, 0 indicates not a PLL
 */
typedef struct sbl_clock_l2cpu_cfg
{
  uint32 nSource;
  uint32 nL;
  uint64 nAlpha;
  uint32 nFrequency;
} ClockConfigCPUL2Type;

/* Data structure for SBL configuration data */
typedef struct
{
  /* PLL configurations */
  ClockConfigPLLType PLL0_Cfg;
  ClockConfigPLLType PLL1_Cfg;
  ClockConfigPLLType PLL2_Cfg;
  ClockConfigPLLType PLL3_Cfg;

  /* Configurations for CPU */
  ClockConfigCPUL2Type CPU_Cfg[CLOCK_BOOT_PERF_NUM];
  ClockConfigCPUL2Type L2_Cfg[CLOCK_BOOT_PERF_NUM];

  /* PCNOC config data */
  ClockConfigMuxType PCNOC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* System NOC config data */
  ClockConfigMuxType SNOC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* BIMC config data */
  ClockConfigMuxType BIMC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* SDC clock config data */
  ClockConfigMuxType SDC_Cfg[CLOCK_BOOT_PERF_NUM];

  /* Crypto clock config */
  ClockConfigMuxType CE_Cfg;

  /* I2C clock config */
  ClockConfigMuxType I2C_Cfg[CLOCK_BOOT_PERF_NUM];


} Clock_SBLConfigType;

extern const Clock_SBLConfigType *Clock_SBLConfig( void );

extern boolean Clock_EnableSource( ClockSourceType eSource );
extern boolean Clock_ConfigureSource( ClockSourceType eSource );

boolean Clock_SourceMapToMux
(
  const ClockConfigMuxType *pConfig,
  uint32 *nMuxValue
);

boolean Clock_MuxMapToSource
(
  ClockConfigMuxType *pConfig,
  uint32 nSource
);

#endif /* !CLOCKCONFIG_H */

