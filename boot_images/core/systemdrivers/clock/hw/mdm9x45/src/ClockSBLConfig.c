/** @file ClockSBLConfig.c

  Definitions of the support clock perf level for the 8974.

  Copyright (c) 2012 - 2014, Qualcomm Technologies Inc. All rights reserved.

**/

/*=============================================================================
                              EDIT HISTORY

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/clock/hw/mdm9x45/src/ClockSBLConfig.c#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

 when        who     what, where, why
 --------    ---     -----------------------------------------------------------
02/18/14     vph     Change A7 frequency from 800MHz to 787.2MHz
01/09/13     vph     Change GPLL2 max at 500MHz for V1
06/05/12     vtw     Copied from 8974.

=============================================================================*/


/*=========================================================================
      Include Files
==========================================================================*/

#include "ClockSBLConfig.h"
#include "ClockSBLCommon.h"
#include "ClockHWIO.h"

/*=========================================================================
      Prototypes
==========================================================================*/
boolean Clock_SourceMapToGCC(ClockSourceType eSource, uint32 *nMuxValue);

/*=========================================================================
      Data
==========================================================================*/


static const Clock_SBLConfigType Clock_SBLConfigData =
{

  /* PLL0 @ 600MHz  */
  .PLL0_Cfg =
  {
     .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL0_MODE),
     .nVoteAddr     =  HWIO_ADDR(GCC_APCS_GPLL_ENA_VOTE),
     .nVoteMask     =  HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL0),
     .nVCO          =  0,
     .nPreDiv       =  1,
     .nPostDiv      =  1,
     .nL            =  0x1F,
     .nM            =  0,
     .nN            =  0,
     .nAlpha        =  0x4000000000,
     .nConfigCtl    =  0x4001051B 

  },

  /* PLL1 @ 614.4MHz  */
  .PLL1_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL1_MODE),
    .nVoteAddr     =  HWIO_ADDR(GCC_APCS_GPLL_ENA_VOTE),
    .nVoteMask     =  HWIO_FMSK(GCC_APCS_GPLL_ENA_VOTE, GPLL1),
    .nVCO          =  0,
    .nPreDiv       =  1,
    .nPostDiv      =  2,
    .nL            =  0x20,
    .nM            =  0,
    .nN            =  0,
    .nAlpha        =  0,
    .nConfigCtl    =  0x00004309
  },

  /* PLL2 @ 808.4MHz  */
  .PLL2_Cfg =
  {
     .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL2_MODE),
     .nVoteAddr     =  HWIO_ADDR(GCC_RPM_GPLL_ENA_VOTE),
     .nVoteMask     =  HWIO_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL2),
     .nVCO          =  0,
     .nPreDiv       =  1,
     .nPostDiv      =  1,
     .nL            =  0x2A,
     .nM            =  0,
     .nN            =  0,
     .nAlpha        =  0x1AAAAAAAAA,
     .nConfigCtl    =  0x4001051B
  },

  /* PLL3 @ 808.4MHz  */
  .PLL3_Cfg =
  {
    .nPLLModeAddr  =  HWIO_ADDR(GCC_GPLL3_MODE),
    .nVoteAddr     =  HWIO_ADDR(GCC_RPM_GPLL_ENA_VOTE),
    .nVoteMask     =  HWIO_FMSK(GCC_RPM_GPLL_ENA_VOTE, GPLL3),
    .nVCO          =  0,
    .nPreDiv       =  1,
    .nPostDiv      =  1,
    .nL            =  0x2A,
    .nM            =  0,
    .nN            =  0,
    .nAlpha        =  0x1AAAAAAAAA,
    .nConfigCtl    =  0x4001051B
  },

  .CPU_Cfg =
  {
    /* { eSource, nL, nAlpha, freq }, */
    { 0x0, 0, 0, 0},
    { 0x0, 0, 0, 19200}, /* Min: CXO 19.2 MHz */
    { SRC_A7SSPLL, 42,  0x0000000000,  80640}, /* Nom: 806.4 MHz */
    { SRC_A7SSPLL, 62,  0x0000000000, 11904}, /* Max: 1190.4 MHz  */
    { SRC_A7SSPLL, 42,  0x0000000000,  80640}, /* Def: 806.4 MHz */
  },

  .L2_Cfg =
  {
    /* { eSource, nL, nAlpha, freq }, */
    { 0x0, 0, 0, 0},
    { 0x0, 0, 0, 19200}, /* Min: CXO 19.2 MHz */
    { SRC_A7SSPLL, 42,  0x0000000000,  80640}, /* Nom: 806.4 MHz */
    { SRC_A7SSPLL, 62,  0x0000000000, 11904}, /* Max: 1190.4 MHz  */
    { SRC_A7SSPLL, 42,  0x0000000000,  80640}, /* Def: 806.4 MHz */
  },

 .SNOC_Cfg =
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                          /* PERF NONE */
    {HWIO_ADDR(GCC_SYSTEM_NOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_CXO,  1, 0, 0, 0},  /* MIN - 19.2 MHz*/
    {HWIO_ADDR(GCC_SYSTEM_NOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_GPLL0, 8, 0, 0, 0},  /* NOM - 150 MHz*/
    {HWIO_ADDR(GCC_SYSTEM_NOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_GPLL0,  6, 0, 0, 0},  /* MAX - 200 MHz*/
    {HWIO_ADDR(GCC_SYSTEM_NOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_GPLL0, 8, 0, 0, 0}   /* DEFAULT - SAME AS NOMINAL */
  },

 .PCNOC_Cfg =
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                          /* PERF NONE */
    {HWIO_ADDR(GCC_PCNOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_CXO,  1, 0, 0, 0},  /* MIN - 19.2 MHz*/
    {HWIO_ADDR(GCC_PCNOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_GPLL0, 12, 0, 0, 0},  /* NOM - 100 MHz*/
    {HWIO_ADDR(GCC_PCNOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_GPLL0, 9, 0, 0, 0},  /* MAX - 133.3 MHz*/
    {HWIO_ADDR(GCC_PCNOC_BFDCD_CMD_RCGR), MUX_GCC, SRC_GPLL0, 12, 0, 0, 0}   /* DEFAULT - 100 (Max Nom) */
  },

  /* SDC configuration */
  .SDC_Cfg =
  {
    {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                           /* PERF NONE */
    {HWIO_ADDR(GCC_SDCC1_APPS_CMD_RCGR), MUX_GCC, SRC_CXO, 24, 1, 4, 4},  /* MIN - 400KHz*/
    {HWIO_ADDR(GCC_SDCC1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 24, 1, 2, 2},  /* NOMINAL - 25MHz*/
    {HWIO_ADDR(GCC_SDCC1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 24, 0, 0, 0},  /* MAX - 50MHz*/
    {HWIO_ADDR(GCC_SDCC1_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 24, 1, 2, 2}   /* DEFAULT - SAME AS NOMINAL */
  }, /* END SDC config */

  /* Crypto configuration CE_Cfg : 171.43 MHz */
  .CE_Cfg =
  {
    HWIO_ADDR(GCC_CRYPTO_CMD_RCGR),
    MUX_GCC, SRC_GPLL0,  /* eSource */
    7, /* nDiv2x */
    0,0,0 /* M/N:D */
  },

  .I2C_Cfg = 
   {
      {0, MUX_GCC, SRC_CXO, 0, 0, 0, 0},                                              /* PERF-NONE */
      {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0}, /* MIN - 19.2 MHz */
      {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 32, 0, 0, 0}, /* NOMINAL - 37.5 MHz */
      {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_GPLL0, 24, 0, 0, 0}, /* MAX - 50 MHz */
      {HWIO_ADDR(GCC_BLSP1_QUP1_I2C_APPS_CMD_RCGR), MUX_GCC, SRC_CXO,    1, 0, 0, 0}, /* DEFAULT - 19.2 MHz */
    },

};


boolean (*Clock_SBL_MuxMap[NUM_MUX_TYPES])(ClockSourceType, uint32 *) =
{
  Clock_SourceMapToGCC
};

/*=========================================================================
      Functions
==========================================================================*/

/* ============================================================================
**  Function : Clock_SBLConfig
** ============================================================================
*/
/*!
    Return a pointer to the SBL configuration data.

   @param  None

   @retval a pointer to the SBL configuration data

*/
const Clock_SBLConfigType *Clock_SBLConfig( void )
{
  return &Clock_SBLConfigData;
}

/* ============================================================================
**  Function : Clock_SourceMapToMux
** ============================================================================
*/
boolean Clock_SourceMapToMux
(
  const ClockConfigMuxType *pConfig,
  uint32 *nMuxValue
)
{
  if( (pConfig == NULL) ||
      (nMuxValue == NULL) ||
      pConfig->eMux >= NUM_MUX_TYPES )
  {
    return FALSE;
  }

  return Clock_SBL_MuxMap[pConfig->eMux](pConfig->eSource, nMuxValue);
}


/* ============================================================================
**  Function : Clock_MuxMapToSource
** ============================================================================
*/
boolean Clock_MuxMapToSource
(
  ClockConfigMuxType *pConfig,
  uint32 nSource
)
{
  return TRUE;
}


/* ============================================================================
**  Function : Clock_SourceMapToGCC
** ============================================================================
*/
/*!
   Map the source enumeration to a physical mux setting for GCC.

   @param  eSource : The source enumeration to map.
   @param  nMuxValue : output parameter.

   @retval a pointer to the SBL configuration data

*/
boolean Clock_SourceMapToGCC(ClockSourceType eSource, uint32 *nMuxValue)
{
  switch( eSource )
  {
    case SRC_CXO:
      *nMuxValue = 0;
      break;
    case SRC_GPLL0:
      *nMuxValue = 1;
      break;
    case SRC_GPLL2:
      *nMuxValue = 2;
      break;
    case SRC_GPLL3:
      *nMuxValue = 3;
      break;
    default:
      return FALSE;
  }
  return TRUE;
}

/* ============================================================================
**  Function : Clock_EnableSource
** ============================================================================
*/

boolean Clock_EnableSource( ClockSourceType eSource )
{
  boolean rtrn = FALSE;

  switch( eSource )
  {
    case SRC_CXO:
      rtrn = TRUE;
      break;
    case SRC_GPLL0:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.PLL0_Cfg);
      break;
    case SRC_GPLL1:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.PLL1_Cfg);
      break;
    case SRC_GPLL2:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.PLL2_Cfg);
      break;
    case SRC_GPLL3:
      rtrn = Clock_EnablePLL(&Clock_SBLConfigData.PLL3_Cfg);
      break;
    default:
      break;
  }
  return rtrn;
}

/* ============================================================================
**  Function : Clock_ConfigureSource
** ============================================================================
*/
boolean Clock_ConfigureSource( ClockSourceType eSource )
{
  boolean rtrn = FALSE;

  switch( eSource )
  {
    case SRC_CXO:
      rtrn = TRUE;
      break;
    case SRC_GPLL0:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.PLL0_Cfg);
      break;
    case SRC_GPLL1:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.PLL1_Cfg);
      break;
    case SRC_GPLL2:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.PLL2_Cfg);
      break;
    case SRC_GPLL3:
      rtrn = Clock_ConfigurePLL(&Clock_SBLConfigData.PLL3_Cfg);
      break;

    default:
      break;
  }
  return rtrn;
}

