#===========================================================================
#
#  @file hwio_msm8936.py
#  @brief HWIO config file for the HWIO generation scripts for MSM8916.
#
#  This file can be invoked by calling:
#
#    HWIOGen.py --cfg=hwio_msm8936.py --flat=..\..\..\api\systemdrivers\hwio\msm8936\ARM_ADDRESS_FILE.FLAT
#
#  ===========================================================================
#
#  Copyright (c) 2012 Qualcomm Technologies Incorporated.
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ===========================================================================
#
#  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/hwio/build/hwio_msm8936.py#1 $
#  $DateTime: 2015/03/19 01:58:37 $
#  $Author: pwbldsvc $
#
#  ===========================================================================

CHIPSET = 'msm8936'

# ============================================================================
# HWIO_BASE_FILES
# ============================================================================

bases = [
  'BOOT_ROM',
  'RPM',
  'RPM_SS_MSG_RAM_START_ADDRESS',
  'SECURITY_CONTROL',
  'SPDM_WRAPPER_TOP',
  'MPM2_MPM',
  'MSS_TOP',
  'PMIC_ARB',
  'QDSS_QDSS',
  'CLK_CTL',
  'CORE_TOP_CSR',
  'CRYPTO0_CRYPTO_TOP',
  'SYSTEM_NOC',
  'TLMM',
  'BIMC',
  'WCSS_WCSS',
  'A53SS',
  'ULTAUDIO_CORE',
  'PRNG_PRNG',
  'PC_NOC',
  'BLSP1_BLSP',
  'PDM_PERPH_WEB',
  'DEHR_BIMC_WRAPPER',
  'SDC1_SDCC5_TOP',
  'SDC2_SDCC5_TOP',
  'QDSS_WRAPPER_TOP'
]

base_resize = {
    'A53SS':                           0x1d6000,
    'BIMC':                            0x6a000,
    'BLSP1_BLSP':                      0x3b000,
    'BOOT_ROM':                        0x100000,
    'RPM_SS_MSG_RAM_START_ADDRESS':    0x5000,
    'CLK_CTL':                         0x82000,
    'CORE_TOP_CSR':                    0x58000,
    'CRYPTO0_CRYPTO_TOP':              0x40000,
    'DEHR_BIMC_WRAPPER':               0x4000,
    'MPM2_MPM':                        0xc000,
    'MSS_TOP':                         0x3e0000,
    'PC_NOC':                          0x11000,
    'PDM_PERPH_WEB':                   0x4000,
    'PMIC_ARB':                        0x1906000,
    'PRNG_PRNG':                       0x2000,
    'QDSS_QDSS':                       0x99000,
    'QDSS_WRAPPER_TOP':                0x6000,
    'RPM':                             0x89000,
    'SDC1_SDCC5_TOP':                  0x25000,
    'SDC2_SDCC5_TOP':                  0x25000,
    'SECURITY_CONTROL':                0x8000,
    'SPDM_WRAPPER_TOP':                0x5000,
    'SYSTEM_NOC':                      0x15000,
    'TLMM':                            0x301000,
    'ULTAUDIO_CORE':                   0xe4000,
    'WCSS_WCSS':                       0x4c1000
}

HWIO_BASE_FILES = [
  {
    'filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/msmhwiobase.h',
    'bases': bases,
    'map-type': 'physical',
    'resize': base_resize,
    'map-filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/msmhwiomap.h',
    'check-sizes': True,
    'check-for-overlaps': True,
    #'ignore-overlaps': [ ('LPASS_CSR', 'LPASS_M2VMT'),
    #                     ('LPASS_CSR', 'LPASS_M2VMT_Q6SS'),
    #                     ('LPASS_CSR', 'LPASS_AHBTM') ],
  }
]


# ============================================================================
# HWIO_T32VIEW_FILES
# ============================================================================

HWIO_T32VIEW_FILES = [
  {
    'symbol-filename': '../scripts/' + CHIPSET + '/hwio.cmm',
    'limit-array-size': [ 10, 4 ],
    'per-filename': '../scripts/' + CHIPSET + '/hwioreg',
    'filter-exclude': ['RESERVED', 'DUMMY']
  },
]


# ============================================================================
# Main
#
# Entry point when invoking this directly.
# ============================================================================

if __name__ == "__main__":
  from subprocess import Popen
  hwiogen = Popen(["\\\\ben\\corebsp_labdata_0001\\sysdrv\\hwio\\HWIOGen.py", "--cfg=hwio_" + CHIPSET + ".py", "--flat=../../../api/systemdrivers/hwio/" + CHIPSET + "/ARM_ADDRESS_FILE.FLAT"], shell=True)
  hwiogen.wait()

