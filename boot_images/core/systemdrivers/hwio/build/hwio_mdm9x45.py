#===========================================================================
#
#  @file hwio_mdm9x45.py
#  @brief HWIO config file for the HWIO generation scripts for MDM9x45.
#
#  This file can be invoked by calling:
#
#    HWIOGen.py --cfg=hwio_mdm9x45.py --flat=..\..\..\api\systemdrivers\hwio\mdm9x45\ARM_ADDRESS_FILE.FLAT
#
#  ===========================================================================
#
#  Copyright (c) 2014 Qualcomm Technologies Incorporated.
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ===========================================================================
#
#  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/systemdrivers/hwio/build/hwio_mdm9x45.py#1 $
#  $DateTime: 2015/03/19 01:58:37 $
#  $Author: pwbldsvc $
#
#  ===========================================================================

CHIPSET = 'mdm9x45'

# ============================================================================
# HWIO_BASE_FILES
# ============================================================================

bases = [
  'BOOT_ROM',
  'RPM',
  'RPM_SS_MSG_RAM_START_ADDRESS',
  'RPM_SS_MSG_RAM_END_ADDRESS',
  'SECURITY_CONTROL',
  'SPDM_WRAPPER_TOP',
  'MPM2_MPM',
  'MODEM_TOP',
  'SDC1_SDCC5_TOP',
  'PMIC_ARB',
  'QDSS_QDSS',
  'CLK_CTL',
  'PC_NOC',
  'CORE_TOP_CSR',
  'CRYPTO0_CRYPTO_TOP',
  'SYSTEM_NOC',
  'TLMM',
  'BIMC',
  'A7SS',
  'QDSS_WRAPPER_TOP',
  'EBI1_PHY_CFG',
  'QPIC_QPIC',
  'PRNG_PRNG'
]

base_resize = {
  'PC_NOC':                         0x11000,
  'SYSTEM_NOC':                     0x14000,
  'MPM2_MPM':                        0xC000,
  'RPM':                            0x90000,
  'BOOT_ROM':                    0x00100000,
  'RPM_SS_MSG_RAM_START_ADDRESS':    0x4000,
  'RPM_SS_MSG_RAM_END_ADDRESS':         0x0,
  'CLK_CTL':                        0x90000,
  'PMIC_ARB':                     0x2000000,
  'MODEM_TOP':                    0x2000000,
  'SDC1_SDCC5_TOP':                 0x30000,
  'QDSS_QDSS':                     0x100000,
  'CORE_TOP_CSR':                   0x60000,
  'CRYPTO0_CRYPTO_TOP':             0x40000,
  'SPDM_WRAPPER_TOP':                0x6000,
  'TLMM':                          0x400000,
  'BIMC':                           0x70000,
  'A7SS':                           0x50000,
  'QDSS_WRAPPER_TOP':                0x8000,
  'EBI1_PHY_CFG' :                  0x20000,
  'QPIC_QPIC' :                     0x40000,
  'PRNG_PRNG' :                       0x140,
}


HWIO_BASE_FILES = [
  {
    'filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/msmhwiobase.h',
    'bases': bases,
    'map-type': 'physical',
    'resize': base_resize,
    'map-filename': '../../../api/systemdrivers/hwio/' + CHIPSET + '/msmhwiomap.h',
    'check-sizes': True,
    'check-for-overlaps': True,
    #'ignore-overlaps': [ ('LPASS_CSR', 'LPASS_M2VMT'),
    #                     ('LPASS_CSR', 'LPASS_M2VMT_Q6SS'),
    #                     ('LPASS_CSR', 'LPASS_AHBTM') ],
  }
]


# ============================================================================
# HWIO_T32VIEW_FILES
# ============================================================================

HWIO_T32VIEW_FILES = [
  {
    'symbol-filename': '../scripts/' + CHIPSET + '/hwio.cmm',
    'limit-array-size': [ 10, 4 ],
    'per-filename': '../scripts/' + CHIPSET + '/hwioreg',
    'filter-exclude': ['RESERVED', 'DUMMY']
  },
]


# ============================================================================
# Main
#
# Entry point when invoking this directly.
# ============================================================================

if __name__ == "__main__":
  from subprocess import Popen
  Popen(["\\\\ben\\corebsp_labdata_0001\\sysdrv\\hwio\\HWIOGen.py", "--cfg=hwio_" + CHIPSET + ".py", "--flat=../../../api/systemdrivers/hwio/" + CHIPSET + "/ARM_ADDRESS_FILE.FLAT"], shell=True)



