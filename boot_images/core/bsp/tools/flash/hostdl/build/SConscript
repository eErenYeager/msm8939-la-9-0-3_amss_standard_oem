#===============================================================================
#
# HOSTDL Tools build script
#
# GENERAL DESCRIPTION
#    HOSTDL image build script
#
# Copyright (c) 2009-2014 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Confidential and Proprietary
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/core/pkg/bootloaders/rel/1.2/boot_images/core/bsp/tools/flash/hostdl/build/SConscript#9 $
#  $DateTime: 2012/03/08 01:29:17 $
#  $Author: coresvc $
#  $Change: 2256571 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 06/22/14   sb      Add support for 9x45, 8909
# 09/03/13   eo      Add support for 9x35
# 10/31/12   sv      Update macro name for flash tools heap size
# 10/19/12   sv      Hostdl support for 9x25
# 09/28/12   sy      Replace MBN_ROOT directory
# 09/17/12   eo      Updated to support 9x25
# 03/08/12   vm      Updated proc value for 8K targets
# 03/06/12   vm      Updated to use new image template and functions
# 12/29/11   sv      Disable hostdl compilation for 8960, 8064, 8930
# 11/16/11   eo      Reverted code start address for hostdl hex image to 0x0
# 10/31/11   eo      Update code start address for hostdl hex image
# 10/26/11   sy      Calling buildProduct library to add compiled hex into BuildProduct.txt
# 09/22/11   sv      Ehostdl support for 9x15
# 06/22/11   eo      Disable hostdl compilation 9x15 apps
# 01/31/11   jz      Added aliases to support modem build
# 12/01/10   bn      Enable hostdl compilation for 6615
# 11/18/10   eo      Fix code alignment
# 11/04/10   sv      Disable hostdl compilation for 6615
# 10/28/10   sb      HOSTDL HDR ADDR to be derived from .builds file for SCMM
# 10/26/10   bn      Fixed build errors for 8660 build
# 10/19/10   bn      Removed hostdl from 8660 build
# 09/20/10   sb      Remove referring to a1std.lib of rvct flavor
# 08/18/10   bb      .scl file clean for e-hostdl/hostdl
# 08/12/10   nr      Replaced nand_log.h with flash_log.h
# 08/08/10   bb      Fix for RVCT 4.0 build failure
# 07/19/10   bb      Added support RVCT 4.0
# 07/13/10   bb      Added support for 6615
# 07/12/10   jz      Fixed for interleaved support
# 06/18/10   sb      Fixing compilation warnings on MDM9K
# 05/07/10   sb      MBN builder changes for secure/nonsecure build
# 04/23/10   sb      Fixing indentation error
# 04/15/10   sb      Change for Memory Map XML on 7x30
# 04/14/10   sv      Update aliases to build hostdl on corebsp image
# 03/22/10   sb      Including msm.h as FLASH_WATCHDOG_H for Voyager
# 03/11/10   jz      Added alias amss_secure for secure signing
# 02/18/10   op      Use hostdl_7x30.scl for halcyon.
# 12/08/09   eo      Update aliases to build flash tools on arm9
# 11/25/09   jz      Fixed to handle case sensitive on Linux
# 11/24/09   jz      Fixed compiler warnings
# 11/17/09   wd/jz   Fixed for Halcyon HOSTDL support
# 11/01/09   ng      Added support for secure boot authentication 
# 10/16/09   eo      Updates to support Hostdl on Voyager
# 09/18/09   wd      Updates to sync with Halcyon and Voyager
# 09/21/09   jz      Moved the handling of 'USES_PRINTF' flag here
# 09/16/09   jz      Fixed the logic to build nandprg.hex
# 09/08/09   eo      Update MDM9K SCONS scripts to new SCONS framework
# 09/02/09   jz      Fixed the sconscript to install the elf in the right place
# 08/12/09   mh      Branched and updated to generate image only
#===============================================================================
import os
import re
Import('env')

env = env.Clone()
#------------------------------------------------------------------------------
# Add new options/arguments to build system
# Needs to be done before check aliases otherwise help won't be updated
#------------------------------------------------------------------------------
vars = Variables(None)

# Defining this to enable PRINTF through command line
vars.Add(
   EnumVariable('HOSTDL_USES_PRINTF',
      'Overide USES_PRINTF flag used in HOSTDL', 'no',
      allowed_values=('yes', 'no'), map={}, ignorecase=0
   )
)

# update environment and read command line variables
vars.Update(env)

Help(vars.GenerateHelpText(env))

#------------------------------------------------------------------------------
# Aliases: Determines when to build hostdl
#------------------------------------------------------------------------------
# aliases:  first alias is always the target then the other possibles aliases
# amss      : Alias for AMSS; anything that needs to be build for amss  
#             as a whole target (componenets from modem + apps)
# amss_secure: Alias for building images with secure signing process
# arm11     : Build components that is for ARM11 only 
# modem     : Only the modem (not applicable to single processor targets)  
# amss_arm11: amss for arm11 processor 
# arm9      : Build components that is for ARM9 only
# amss_arm9 : amss for arm9 processor 
# all       : Build all components 
# cbsp      : Build components for building corebsp images

if env['MSM_ID'] in ['9x00', '9x15', '9x25', '9x35', '9x45', '8909']:
   aliases = [
      'hostdl', 'nandprg', 'tools', 'flashtools',
      'modem', 'amss', 'amss_secure', 'amss_arm11',
      'arm11', 'amss_arm9', 'arm9', 'all', 'cbsp',
      'coreimg_arm11', 'coreimage_modem',
      'cbsp_arm11', 'cbsp_modem',
   ]
else:
   aliases = [
      'not_supported',
   ]

if env['MSM_ID'] in ['9x00']:
   proc = 'arm9'
elif env['MSM_ID'] in ['9x15', '9x25', '9x35', '9x45', '8909']:
   proc = 'sparrow'
else:
   proc = 'not_supported'
   
env.InitImageVars(
   alias_list = aliases,                           # list of aliases, unique name index [0]
   proc = proc,                                    # proc settings
   config = 'apps',                                # config settings
   build_tags = [                                  # list of build tags for sub lib scripts
   'HOSTDL_IMAGE',
   'DAL_DEVCFG_TOOLS_IMG'],
   tools = [
   '${BUILD_ROOT}/core/bsp/build/scripts/mbn_builder.py', 
   '${BUILD_ROOT}/core/bsp/build/scripts/bin_builder.py', 
   '${BUILD_ROOT}/core/bsp/build/scripts/hex_builder.py', 
   '${BUILD_ROOT}/core/bsp/build/scripts/scl_builder.py', 
   '${BUILD_ROOT}/core/bsp/build/scripts/devcfg_builder.py',
   'buildspec_builder.py']
)

if not env.CheckAlias():
   Return()

#------------------------------------------------------------------------------
# Init default values this PROC/Image
#------------------------------------------------------------------------------

# add environment variables
env.Replace(BUILD_TOOL_CHAIN = 'yes')
env.Replace(USES_DEVCFG = 'yes')

#===============================================================================
# HOSTDL build rules
#===============================================================================

#------------------------------------------------------------------------------
# Configure and load in CBSP uses and path variables
#------------------------------------------------------------------------------
env.InitBuildConfig()

#------------------------------------------------------------------------------
# Load in tools scripts, and re define our CCCOM string
#------------------------------------------------------------------------------
env.LoadToolScript('arm', toolpath = ['${BUILD_SCRIPTS_ROOT}'])
env.LoadToolScript('hostdl_gen_hex', toolpath = ['${BUILD_ROOT}/core/bsp/tools/flash/build'])

if env['MSM_ID'] in ['9x00', '9x15', '9x25', '9x35', '9x45', '8909']:
   env.LoadToolScript('modem_defs', toolpath = ['${BUILD_SCRIPTS_ROOT}'])   

#change the MBN_ROOT to copy all the binaries to a meaningful directory
if env['MSM_ID'] in ['9x25', '9x35', '9x45', '8909']:
   env.Replace(MBN_ROOT = "${BUILD_ROOT}/build/ms/bin/${TARGET_FAMILY}/nand")

# Force debug and symbols for flash tools, required by T32 swbp interface
env.Replace(ARM_DBG = "-g --dwarf2")            # Enable DWARF2 format debug tables

# After loading default rules, make some optimization changes
env.Append(ARMCC_CODE = " ${ARM_SPLIT_SECTIONS}")
env.Replace(ARMLD_NOREMOVE_CMD = "")

# Compile thumb code
env.Replace(CC = env['TCC'])

#disable unaligned access for c/c++ compiler and assembler
env.Replace(ARMCC_CODE = "${ARM_END} ${ARM_NO_UNALIGNED_ACCESS}")
env.Append(ARMCXX_CODE = " ${ARM_NO_UNALIGNED_ACCESS}")
env.Append(ASFLAGS = " --keep ${ARM_NO_UNALIGNED_ACCESS}")

# Define processor instruction set
env.Replace(ARM_CPU_KRAIT = "7-A.security")
env.Replace(ARM_CPU = "${ARM_CPU_CMD} ${ARM_CPU_KRAIT}")

#----------------------------------------------------------------------------
# Define the PRINT SOURCES.PRINT LOGS will be enabled only if USES_PRINTF is
# defined on the scons environment, to disable it DON'T define it. As the
# scripts check to see if the key is present and not the value!!
#----------------------------------------------------------------------------
env.Replace(USES_PRINTF = "no")
env.Replace(TNOISYLEVEL = "3")

#----------------------------------------------------------------------------
# Define macro NAND_PAGE_SIZE used to pass a value to macro 'MAX_PAGE_SIZE',
#  which is used by nand tools and flash drivers (when compiled with tools)
# to size the static buffers.
#----------------------------------------------------------------------------
NAND_PAGE_SIZE = "4096"

# ---------------------------------------------------------------------------
# Determine the platform
# Please see //source/qcom/qct/core/storage/flash/main/latest/src/README.txt
# for details on flash configurations.
# ---------------------------------------------------------------------------
FLASH_CFG_ID = "UNKNOWN"

# The following for FLASH_7K_CFG is not used but is place holder for
# future targets that shoudl use this configurations for hal/dal/tools
if env['MSM_ID'] in ['9x00', '9x15']:
   FLASH_CFG_ID = "FLASH_9K_CFG"
   FLASH_DAL_CFG = "config_v1"
   FLASH_HAL_CFG = "config_v1"
   FLASH_NAND_TOOLS_CFG = "config_v2"
   FLASH_NOR_TOOLS_CFG = "config_v1"
   FLASH_HOSTDL_CFG = "config_v2"
   SRCROOT = "../../../../../.."

if env['MSM_ID'] in ['9x25', '9x35']:
   if env['MSM_ID'] in ['9x25']:
      FLASH_CFG_ID = "FLASH_9K_CORTEXA5_CFG"
      MSMID_TYPE = "M9x25"
   else:
      FLASH_CFG_ID = "FLASH_9K_CORTEXA7_CFG"
      MSMID_TYPE = "M9x35"
   FLASH_DAL_CFG = "config_v2"
   FLASH_HAL_CFG = "config_v1"
   FLASH_NAND_TOOLS_CFG = "config_v2"
   FLASH_NOR_TOOLS_CFG = "config_v1"
   FLASH_HOSTDL_CFG = "config_v1"
   SRCROOT = "../../../../../.."

if env['MSM_ID'] in ['9x45']:
   FLASH_CFG_ID = "FLASH_9K_CORTEXA7_CFG"
   MSMID_TYPE = "M9x45"
   FLASH_DAL_CFG = "config_v2"
   FLASH_HAL_CFG = "config_v1"
   FLASH_NAND_TOOLS_CFG = "config_v2"
   FLASH_NOR_TOOLS_CFG = "config_v1"
   FLASH_HOSTDL_CFG = "config_v1"
   SRCROOT = "../../../../../.."

if env['MSM_ID'] in ['8909']:
   FLASH_CFG_ID = "FLASH_8K_CORTEXA7_CFG"
   MSMID_TYPE = "M8909"
   FLASH_DAL_CFG = "config_v2"
   FLASH_HAL_CFG = "config_v1"
   FLASH_NAND_TOOLS_CFG = "config_v2"
   FLASH_NOR_TOOLS_CFG = "config_v1"
   FLASH_HOSTDL_CFG = "config_v1"
   SRCROOT = "../../../../../.."

if env['MSM_ID'] in ['9x00', '9x25', '9x35', '9x45', '8909']: 
   env.Append(CPPDEFINES = [
      "FLASH_WATCHDOG_H=\\\"msm.h\\\"",
   ])
   
if env['MSM_ID'] in ['9x45']:
   env.Replace(DAL_CONFIG_TARGET_ID = '0x9045')
else:
  env.Replace(DAL_CONFIG_TARGET_ID = '0x${MSM_ID}')
  
env.Append(CPPDEFINES = [
  "DAL_CONFIG_TARGET_ID=${DAL_CONFIG_TARGET_ID}",
])
   
#----------------------------------------------------------------------------
# Overide uses_print setting from cmd line
#----------------------------------------------------------------------------
if env['HOSTDL_USES_PRINTF'] == "yes":
   env.Replace(USES_PRINTF = "yes")

#----------------------------------------------------------------------------
# Define the PRINT SOURCES.PRINT LOGS will be enabled only if USES_PRINTF is
# set to YES in the make file
#----------------------------------------------------------------------------
if env.has_key('USES_PRINTF') and env['USES_PRINTF'] == "yes": 
   env.Append(CPPDEFINES = [
      "FLASH_LOG_H=\\\"flash_log.h\\\"",
      "NOISY",
      "TNOISY=" + str(env['TNOISYLEVEL']),
   ])

#print env.Dump()

if env['MSM_ID'] in ['9x15']:
   CODE_START_ADDR = "0x41700028"
   CODE_HDR_ADDR = "0x41700000"
elif env['MSM_ID'] in ['9x25', '9x35']:
   CODE_START_ADDR = "0x02A00028"
   CODE_HDR_ADDR = "0x02A00000"
   HOSTDL_ZI_BASE = "+0"
elif env['MSM_ID'] in ['9x45', '8909']:
   CODE_START_ADDR = "0xA0000028"
   CODE_HDR_ADDR = "0xA0000000"
   HOSTDL_ZI_BASE = "+0"

DATA_ADDR = "+0x0"

# add extension flags for HOSTDL
env.Append(CPPDEFINES = [
   "BUILD_TOOL_CHAIN",
   "MAX_PAGE_SIZE=" + NAND_PAGE_SIZE,
   "FLASH_SINGLE_THREADED",
   "ENDIAN_LITTLE",
   "BUILD_HOSTDL",
   "BUILD_NANDPRG",
   "HOSTDL_PROCESS_USB_BUFFERS",
   "EXTENDED_NAND_CMDS",
   "DAL_CONFIG_MMU_SCHEME=DAL_CONFIG_MMU_SCHEME_NONE",
   "DAL_IMAGE_FLASH_TOOLS",
   "CODE_START_ADDR=" + CODE_START_ADDR,
   "CODE_HDR_ADDR=" + CODE_HDR_ADDR,
   "DATA_ADDR=" + DATA_ADDR,
   "ZI_ADDR=" + HOSTDL_ZI_BASE,
   "FEATURE_HS_USB_BASIC",
])

if env['MSM_ID'] in ['9x15', '9x25', '9x35', '9x45', '8909']:
   env.Append(CPPDEFINES = [
   "FLASH_TOOLS_HEAP_SIZE=512000",
   ])
else:
   env.Append(CPPDEFINES = [
      "BOOT_HEAP_SIZE=512000",
   ])
   
# For 7x30 and 9x00, FEATURE_STANDALONE_MODEM will be defined in .builds file.
# So no need to define it in Sconscript file

if env['MSM_ID'] in ['9x00', '9x15', '9x25', '9x35', '9x45']:
   env.Append(CPPDEFINES = [
      "TARGET_ID_9K",
      "USE_USB_ONLY",
])

if env['MSM_ID'] in ['8909']:
   env.Append(CPPDEFINES = [
      "TARGET_ID_8K",
      "USE_USB_ONLY",
])

if env['MSM_ID'] in ['9x15', '9x25', '9x35', '9x45', '8909']:
   env.Append(CPPDEFINES = [
   "FLASH_ENABLE_WDOG_CFG"
   ])

env.Append(CPPDEFINES = [
"EHOSTDL_STARTUP_SOURCE=\\\"hostdl_startup.o\\\"",   
   ])
   
#------------------------------------------------------------------------------
# Decide which build steps to perform
#------------------------------------------------------------------------------
# Regular build steps (no filter) is build everything, once a user starts
# using filters we have to make decisions depending on user input.
#
# The LoadAUSoftwareUnits function will take care of filtering subsystem, units, 
# etc.  This is to take care of what steps to enable disable from the top level
# script, such as building files specify in this script i.e. quartz, stubs, etc.

do_local_files = True
do_link = True
do_post_link = True

# Get user input from command line
filter_opt = env.get('FILTER_OPT')

# Limit build processing based on filter option
if filter_opt is not None:
   do_link = False
   do_post_link = False

   if not env.FilterMatch(os.getcwd()):
      do_local_files = False

#-------------------------------------------------------------------------------
# Libraries Section
#-------------------------------------------------------------------------------
core_libs, core_objs = env.LoadAUSoftwareUnits('core')
hostdl_units = [core_libs, core_objs,]

if do_local_files:
#-------------------------------------------------------------------------------
# HOSTDL Environment
#-------------------------------------------------------------------------------
   env.RequireRestrictedApi('BOOT')

#-------------------------------------------------------------------------------
# Begin building HOSTDL
#-------------------------------------------------------------------------------
   env.Replace(TARGET_NAME = 'NANDPRG_${MSM_ID}')

#------------------------------------------------------------------------------
# Generate Scatter Load File (SCL)
#-------------------------------------------------------------------------------
   target_scl = env.SclBuilder('${SHORT_BUILDPATH}/${TARGET_NAME}', 
      '${BUILD_ROOT}/core/storage/flash/tools/src/hostdl/hostdl_in.scl')

if do_link:
#-------------------------------------------------------------------------------
# Generate nandprg.elf
#-------------------------------------------------------------------------------
   libs_path = env['INSTALL_LIBPATH']

   nandprg_elf = env.Program('${SHORT_BUILDPATH}/${TARGET_NAME}', 
      source=[core_objs], LIBS=[core_libs], LIBPATH=libs_path)
	
   env.Depends(nandprg_elf, target_scl)

   Clean(nandprg_elf, env.subst('${SHORT_BUILDPATH}/${TARGET_NAME}.map'))
   Clean(nandprg_elf, env.subst('${SHORT_BUILDPATH}/${TARGET_NAME}.sym'))

# copy elf to needed locations for AMSS tools to load on target
   install_nandprg_elf = env.Install("${BUILD_ROOT}/core/storage/flash/tools/src/hostdl", 
      nandprg_elf)
   
if do_post_link: 
#----------------------------------------------------------------------------
# Generate HOSTDL BIN
#----------------------------------------------------------------------------
   target_bin = env.BinBuilder('${SHORT_BUILDPATH}/${TARGET_NAME}', nandprg_elf)

#----------------------------------------------------------------------------
# Generate HOSTDL mbn
#----------------------------------------------------------------------------
   # MbnBuilder will make a copy of the mbn in the build binaries directory
   env.Replace(MBN_FILE = '${MBN_ROOT}/NPRG${MSM_ID}')

   target_mbn = env.MbnBuilder('${SHORT_BUILDPATH}/${TARGET_NAME}', target_bin, 
      IMAGE_TYPE="hostdl", FLASH_TYPE="nand", MBN_TYPE="bin")

   nprg_product = env.BuildProductsFile('${SHORT_BUILDPATH}/${TARGET_NAME}',target_mbn)

#-------------------------------------------------------------------------------
# Generate nandprg.hex
#-------------------------------------------------------------------------------
   if env['MSM_ID'] not in ['9x25', '9x35', '9x45', '8909']:
      nprg_hex = env.NPRGHexBuilder('NPRG${MSM_ID}_${BUILD_ID}', target_mbn,
         HOSTDL_ADDR=CODE_HDR_ADDR, CODE_START_OFFSET=0x0)

      # Make a copy of the hex in the build binaries directory
      install_nandprg_hex = env.InstallAs('${MBN_ROOT}/NPRG${MSM_ID}.hex', nprg_hex)

      nprg_product = env.BuildProductsFile('${SHORT_BUILDPATH}/${TARGET_NAME}',install_nandprg_hex)

#===============================================================================
# Define units that will be built
#===============================================================================

   hostdl_units = [nprg_product,]

# add aliases
for alias in aliases:
   env.Alias(alias, hostdl_units)
