Logging to /local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/bsp/tools/emmcbld/build/SAAAANAZ/sign/SecImage_log.txt
Config path is set to: /local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/tools/build/scons/sectools/config/integration/secimage.xml
Output dir is set to: /local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/bsp/tools/emmcbld/build/SAAAANAZ/sign
------------------------------------------------------
Processing 1/1: /local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/build/ms/bin/8936/unsigned/MPRG8936.mbn

Signing image: /local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/build/ms/bin/8936/unsigned/MPRG8936.mbn
Generating new Root certificate and a random key
Generating new Attestation CA certificate and a random key
Generating new Attestation certificate and a random key

Attestation Certificate Properties:
| SW_ID     | 0x0000000000000003  |
| HW_ID     | 0x0000000000000000  |
| DEBUG     | 0x0000000000000002  |
| OEM_ID    | 0x0000              |
| SW_SIZE   | 136                 |
| MODEL_ID  | 0x0000              |
| SHA256    | True                |
| APP_ID    | None                |
| CRASH_DUMP| None                |
| ROT_EN    | None                |
| Exponent  | 3                   |
| TCG_MIN   | None                |
| TCG_MAX   | None                |

Signed image is stored at /local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/bsp/tools/emmcbld/build/SAAAANAZ/sign/default/emmcbld/MPRG8936.mbn

Base Properties: 
| Integrity Check             | True  |
| Signed                      | True  |
| Encrypted                   | False |
| Size of signature           | 256   |
| Size of one cert            | 2048  |
| Num of certs in cert chain  | 3     |
| Number of root certs        | 1     |
| Cert chain size             | 6144  |

ELF Properties: 
Elf Header: 
| Magic                      | ELF                           |
| Class                      | ELF32                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | Advanced RISC Machines ARM     |
| Version                    | 0x1                            |
| Entry address              | 0x08005050                     |
| Program headers offset     | 0x00000034                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x05000002                     |
| ELF header size            | 52                             |
| Program headers size       | 32                             |
| Number of program headers  | 1                              |
| Section headers size       | 40                             |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Elf Program Headers: 
| S.No | Type | Offset | VirtAddr | PhysAddr | FileSize | MemSize |   Flags    | Align |
|------|------|--------|----------|----------|----------|---------|------------|-------|
|  1   | LOAD | 0x3000 |0x8005050 |0x8005050 |  0xbe08  | 0x18598 | 0x80000007 | 0x800 |

Hash Segment Properties: 
| Header Size  | 40B  |
| Has Preamble | False|
| Preamble Size| None |
| Has Magic Num| False|
| Page Size    | None |
| Num Pages    | None |
| Ota Enabled  | False|
| Ota Min Size | None |

Header: 
| cert_chain_ptr  | 0x0801e188  |
| cert_chain_size | 0x00001800  |
| code_size       | 0x00000060  |
| flash_parti_ver | 0x00000003  |
| image_dest_ptr  | 0x0801e028  |
| image_id        | 0x0000000d  |
| image_size      | 0x00001960  |
| image_src       | 0x00000000  |
| sig_ptr         | 0x0801e088  |
| sig_size        | 0x00000100  |

SecElf Properties: 
| image_type         | 0     |
| testsig_serialnum  | None  |

------------------------------------------------------

SUMMARY:
Following actions were performed: "sign"
Output is saved at: /local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/bsp/tools/emmcbld/build/SAAAANAZ/sign

| Idx | SignId | Parse | Integrity | Sign | Encrypt |              Validate              |
|     |        |       |           |      |         | Parse | Integrity | Sign | Encrypt |
|-----|--------|-------|-----------|------|---------|-------|-----------|------|---------|
|  1. |emmcbld |   T   |     NA    |  T   |    NA   |   NA  |     NA    |  NA  |    NA   |

