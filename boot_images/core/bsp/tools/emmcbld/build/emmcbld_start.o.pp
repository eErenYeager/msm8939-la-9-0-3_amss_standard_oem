#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/emmcbld_start.s"
;























 
















 

#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"




 










 














 

#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/hwio/msm8929/msmhwiobase.h"




 



 


























 



 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 






#line 34 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"





 



#line 55 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"

#line 62 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"

#line 71 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"

#line 86 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"

#line 99 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"

#line 112 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"

#line 121 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"

#line 128 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"

#line 141 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"



 



#line 160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/msmhwioreg.h"


#line 45 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/storage/tools/emmcbld/src/emmcbld_start.s"

;



 

; CPSR Control Masks 
PSR_Fiq_Mask    EQU     0x40
PSR_Irq_Mask    EQU     0x80

; Processor mode definitions 
PSR_Supervisor  EQU     0x13


;



 


; Stack sizes
;If this value has to be changed, please also change in emmcbld_main.c
SVC_Stack_Size  EQU     0x4000
    

;



 


        IMPORT  main_c
        ;IMPORT  svc_stack
        IMPORT  memory_init
        IMPORT  zero_init_needed
        IMPORT  emmcbld_bsp_hw_init
        IMPORT  |Image$$ZI_DATA$$ZI$$Base|
        IMPORT  |Image$$ZI_DATA$$ZI$$Limit|

        EXPORT  __main
        EXPORT  __emmcbld_entry

        PRESERVE8
        AREA    EMMCBLD_ENTRY_POINT, CODE, READONLY
        CODE32


;=======================================================================
; MACRO mmu_set_default_cfg
; ARGS
;   NONE
; DESCRIPTION
;   Sets a default value into the mmu control register
;   we do not really need to do this, but we do it to
;   know exactly the state of the processor
;=======================================================================
   MACRO
   mmu_set_default_cfg
   ldr    r0, = 0x00050078
   MCR    p15, 0, r0, c1, c0, 0
   MEND
;=======================================================================


;=======================================================================
; MACRO mmu_enable_i_cache
; ARGS
;  NONE
; DESCRIPTION
;  Enables the I Cache
;  Does a read modify write, so no other bits in the control register a
;  affected
;=======================================================================
   MACRO
   mmu_enable_i_cache
   mrc     p15, 0 , r0, c1, c0, 0
   orr     r0, r0, # (1 << 12 )
   mcr     p15, 0 , r0, c1, c0, 0
   MEND
;=======================================================================

;=======================================================================
; MACRO init_zi
; ARGS
;  NONE
; DESCRIPTION
;  Initializes the zero-init region (.bss) to zero
;  r0  = start of ZI
;  r1  = end of ZI
;  r2  = 0 (for initializing memory) 
;  r3  = watchdog register
;  r4  = 1 (for kicking the dog)
;=======================================================================
  MACRO
    init_zi
    ldr     r0, =|Image$$ZI_DATA$$ZI$$Base|
    ldr     r1, =|Image$$ZI_DATA$$ZI$$Limit|



    mov     r4, #0x1                   
    mov     r2,#0
init_zi_label
    cmp     r0,r1          ; unsigned compare, clear c on borrow
    strcc   r2,[r0],#4     ; str 0 in [r0] if r0 < r1



    bcc     init_zi_label  ; loop while ro < r1
  MEND
;=======================================================================


;=======================================================================
; MACRO reloc_data
; ARGS
;  NONE
; DESCRIPTION
;  Relocates the RW data for eHOSTDL
;  r0  = start of dest mem
;  r1  = end of dest mem (length of data to be relocated)
;  r2  = start of src mem
;  r3  = watchdog register
;  r4  = 1 (for kicking the dog)
;  r5  = tmp data holder for data relocation
;=======================================================================
  MACRO
    reloc_data
    ldr     r0, =|Image$$APP_RAM$$Base|
    ldr     r1, =|Image$$APP_RAM$$ZI$$Base|



    mov     r4, #0x1                   
    ldr     r2, =|Load$$APP_RAM$$Base|
reloc_data_label
    cmp     r0,r1            ; unsigned compare, clear c on borrow
    ldrcc   r5,[r2],#4       ; Load [r2] to r5, increment [r2] if r0 < r1
    strcc   r5,[r0],#4       ; str r5 at [r0],  increment [r0] if r0 < r1



    bcc     reloc_data_label ; loop while ro < r1
  MEND
;=======================================================================


;




















 
__main
__emmcbld_entry


        ENTRY

; Supervisor Mode
; Set up the Supervisor stack pointer.
        msr     CPSR_c, #PSR_Supervisor:OR:PSR_Irq_Mask:OR:PSR_Fiq_Mask
        ldr     r13, =0x08035000


        init_zi






; ======================================================================
; Enable the instruction cache
; ======================================================================
   ;mmu_set_default_cfg
   ;mmu_enable_i_cache

; ======================================================================
; Initialize memory for C only once
;   The test/set of the global variable must be done here in assembly
;   because if we access a global variable in the C function, the
;   compiler will construct a PUSH/POP of registers and since we will
;   have just zeroed the stack, we will pop zero into R14 and then
;   branch to zero.  With no use of globals in the C function,
;   the compiler will generate a bx r14 for the return and all will
;   work correctly.
; ======================================================================

; indicate zero init completed
        ldr   r2, =zero_init_needed
        mov   r0, #0x0
        str   r0, [r2]

; Enter C code execution
        ;ldr     r13, =svc_stack+SVC_Stack_Size
;;loop_forever
;;		b loop_forever
        ldr     a1, =main_c
        bx      a1

        END
