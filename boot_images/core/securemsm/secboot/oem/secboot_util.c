/*===========================================================================

                     S E C U R I T Y    S E R V I C E S

                S E C U R E  B O O T   X.5 0 9  P A R S E R

                               M O D U L E

FILE:  secboot_util.c

DESCRIPTION:
  Secure Boot wraper function

EXTERNALIZED FUNCTIONS


Copyright (c) 2013 by Qualcomm Technologies, Inc. All Rights Reserved.
===========================================================================*/

/*=========================================================================

                          EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/secboot/oem/secboot_util.c#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $

when         who                what, where, why
--------   ----               ------------------------------------------- 
01/21/14   hw                  enable mrc signed image (with root 0) on non-secure device
01/20/14   hw                  add parm define to enable unsigned image on 8092
01/10/14   hw                  disallow unsigned image to be loaded
11/06/13   hw                  Init version.
===========================================================================*/


/*==========================================================================

           Include Files for Module

==========================================================================*/
#include "secboot_util.h"
#include "secboot_i.h"
#include "secboot_x509.h"

// check data + len is always smaller than the bound and no overflow
#define DATA_IN_BOUND(data, len, bound) ((data + len > data) && (data + len < bound))

/**
 * @brief See documentation in the header
 *
 */
static boolean secboot_is_signed_image(secboot_image_info_type *image_info)
{
  if (image_info != NULL)
  {
    if ( (image_info->signature_ptr != NULL) &&
         (image_info->signature_len != 0) && 
         (image_info->x509_chain_ptr != NULL) && 
         (image_info->x509_chain_len != 0))
    {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * @brief in-place convert char to integer (supporting hex format)
 *
 * @param[in/out] data_ptr the data pointer
 *
 * @return TRUE for no error, return FALSE for null pointer of if not a hex integer ascii code
 */
static boolean secboot_ctoi(uint8* data_ptr) 
{
  uint32 value = 0;

  if (data_ptr == NULL) 
  {
    return FALSE;
  }

  value = BLOAD8(data_ptr);
  if ((value >= 0x41) && (value <= 0x46)) // A: 0x41 (ascii)
  {
    value -= 55;
  }
  else if ((value >= 0x30) && (value <= 0x39))  // 0: 0x30 (ascii)
  {
    value -= 48;
  }
  else
  {
    return FALSE;
  }

  BSTOR8(data_ptr , value&0x0F);

  return TRUE;
}

/**
 * @brief convert string to long long int (supporting hex format). 
 *        it returns 0 if the conversion is not successful
 *
 * @param[in/out] data_ptr  the data pointer
 *                data_len  the data length
 *
 */
static uint64 secboot_strtoll(uint8* data_ptr, uint64 data_len)
{
  uint32 lshift = 0, j = 0;
  uint64 value = 0;
  
  if (NULL == data_ptr)
  {
    return 0;
  }

  lshift = 60; // shifting length, init: 4*16-4
  for (j = 0; j < data_len; j ++)
  {
    // in-place convert the ascii of 0-9 and A-F to binary
    if (TRUE != secboot_ctoi(&data_ptr[j]))
    {
      value = 0;
      break;
    }

    value += ((uint64)data_ptr[j])<<lshift;
    lshift -=4;
  }

  return value;
}

/**
 * search ou field from the cert chain
 *
 * @param[in/out]  ou_field_value_ptr    the ou field value
 * @param[in]     ou_field_string_ptr    the pointer to the searched string
 *                ou_field_string_size   the size of the searched string
 *                data_ptr         the pointer to the searched data
 *                data_len         the length of the searched data
 *
 * @return  \c E_SECBOOT_SUCCESS if the OU value is available,
 *          \c E_SECBOOT_INVALID_PARAM for invalid input argument
 */
#define OU_FIELD_VALUE_STRING_SIZE (16)
static secboot_error_type secboot_search_OU_field_data(uint64* ou_field_value_ptr, 
                                                const uint8* ou_field_string_ptr, 
                                                uint64 ou_field_string_size, 
                                                const uint8* data_ptr, 
                                                uint64 data_len)
{
  uint64 i = 0; 

  uint64 ou_field_value = 0;
  uint8 ou_field_value_string[OU_FIELD_VALUE_STRING_SIZE+1];
  uint8* ou_field_value_string_ptr = NULL;

  if (ou_field_value_ptr == NULL || ou_field_string_ptr == NULL 
      || data_ptr == NULL || data_len == 0)
  {
    return E_SECBOOT_INVALID_PARAM;
  }

  // search for the ou field string.
  for (; i < data_len; i++)
  {
    // no need to check before the 17th byte, as the length of 
    // searched OU_FIELD_VALUE_STRING_SIZE is 16, plus one space (1 byte)
    if (i >= OU_FIELD_VALUE_STRING_SIZE + 1)
    {
      if (DATA_IN_BOUND(i, ou_field_string_size, data_len) &&
          MEMCMP((data_ptr+i), ou_field_string_ptr, ou_field_string_size) == 0)
      {
        // Found it, get the starting position of the ou field value string, 
        // 1 for the space after the ou field value string
        ou_field_value_string_ptr = (uint8*)(data_ptr + i - (OU_FIELD_VALUE_STRING_SIZE + 1));
        break;
      }
    }
  }

  // ou_field_value_string_ptr should not be NULL, still pointing to cert data region 
  // and not underflow
  if ( ou_field_value_string_ptr != NULL &&
      ((uint32)ou_field_value_string_ptr >= (uint32)data_ptr) &&
      ((uint32)ou_field_value_string_ptr < (uint32)(data_ptr + i)) )
  {
    // convert hex str to integer
    MEMSET(ou_field_value_string, 0, (OU_FIELD_VALUE_STRING_SIZE+1));
    MEMCPY(ou_field_value_string, ou_field_value_string_ptr, OU_FIELD_VALUE_STRING_SIZE);
    ou_field_value = secboot_strtoll(ou_field_value_string, OU_FIELD_VALUE_STRING_SIZE);
  }

  *ou_field_value_ptr = ou_field_value;

  return E_SECBOOT_SUCCESS;
}

/**
 * @brief See documentation in the header
 *
 */
static const uint8 hw_id_string[]="HW_ID";
static secboot_error_type secboot_get_fuse_info_from_image(secboot_fuse_info_type *fuse_info, 
                                                    secboot_image_info_type* image_info)
{
  secboot_error_type status = E_SECBOOT_FAILURE;
  secx509_errno_enum_type x509_status = E_X509_FAILURE;
  uint64 msm_hw_id = 0;
  uint32 total_cert_num = 0;
  
  /* return failure, if the image pointer is NULL */
  if ( (image_info == NULL) || 
     (image_info->x509_chain_ptr == NULL) ||
     (image_info->signature_ptr == NULL) )
  {
    return E_SECBOOT_INVALID_PARAM;
  }
  
  /* default specific fuse information here: */
  MEMSET(fuse_info->root_of_trust, 0, sizeof(fuse_info->root_of_trust));
  
  fuse_info->use_root_of_trust_only = FALSE;
  fuse_info->auth_use_serial_num = 0;
  fuse_info->serial_num = 0;

  /* init multiple root cert fuse value  for default one root qpsa signed image */
  fuse_info->root_sel_info.is_root_sel_enabled = SECBOOT_ROOT_CERT_SEL_DISABLED;
  fuse_info->root_sel_info.root_cert_sel = 0;
  fuse_info->root_sel_info.num_root_certs = 0;

  /* MRC signed image needs to setup additional fuses. 
     MRC cert has more than one root, plus attest and subca cert. The total cert number is larger than 3.
     (This only works for the case whose cert chain has the attest-subca-root(s) 3-level structure)*/
  x509_status = secboot_cal_cert_num( image_info->x509_chain_ptr, image_info->x509_chain_len, 
                                      &total_cert_num);
  if (E_X509_SUCCESS != x509_status) 
  {
    return E_SECBOOT_FAILURE;
  }

  if ( total_cert_num > 3) 
  {
    fuse_info->root_sel_info.is_root_sel_enabled = SECBOOT_ROOT_CERT_SEL_ENABLED;
    /* assign the total number of root cert */
    fuse_info->root_sel_info.num_root_certs = total_cert_num - 2; 
  }

  /* search MSM_HW_ID from the image */
  status = secboot_search_OU_field_data(&msm_hw_id, hw_id_string, (sizeof(hw_id_string)-1), 
                                     image_info->x509_chain_ptr,image_info->x509_chain_len);
  if ( E_SECBOOT_SUCCESS != status)
  {
    return status;
  }
  
  fuse_info->msm_hw_id = msm_hw_id;

  return E_SECBOOT_SUCCESS;
}

/**
 * @brief See documentation in the header
 *
 */
secboot_error_type secboot_authenticate_image(uint32 code_segment,
                                     secboot_image_info_type* image_info,
                                     secboot_verified_info_type* verified_info,
                                     secboot_ftbl_type* secboot_ftbl_ptr,
                                     secboot_hw_ftbl_type* secboot_hw_ftbl_ptr,
                                     secboot_crypto_hash_ftbl_type* crypto_hash_ftbl_ptr)
{
  secboot_error_type status = E_SECBOOT_FAILURE;
  secboot_error_type deinit_status = E_SECBOOT_FAILURE;
  secboot_hw_etype   hw_status = E_SECBOOT_HW_FAILURE;
       

  secboot_handle_type  sec_handle = {0};
  uint32         is_auth_enabled = FALSE;
  uint32         is_auth_required = FALSE;
  secboot_fuse_info_type image_fuse_info;
   
  MEMSET(&image_fuse_info, 0, sizeof(image_fuse_info));
   
  if ((secboot_ftbl_ptr == NULL) 
       || (secboot_hw_ftbl_ptr == NULL))
  {
    return E_SECBOOT_INVALID_PARAM;
  }

  if ((secboot_hw_ftbl_ptr->secboot_hw_is_auth_enabled == NULL) 
       || (secboot_ftbl_ptr->secboot_init == NULL)
       || (secboot_ftbl_ptr->secboot_init_fuses == NULL)
       || (secboot_ftbl_ptr->secboot_authenticate == NULL)
       || (secboot_ftbl_ptr->secboot_deinit == NULL))
  {
    return E_SECBOOT_INVALID_PARAM;
  }


  hw_status = secboot_hw_ftbl_ptr->secboot_hw_is_auth_enabled(code_segment, &is_auth_enabled);  
  if(E_SECBOOT_HW_SUCCESS == hw_status)
  {
    /* secure device, auth is required */
    if (TRUE == is_auth_enabled)
    {
      is_auth_required = TRUE;
    }
    /* non-secure device: auth signed image */
    else if ((FALSE == is_auth_enabled) && 
             (TRUE == secboot_is_signed_image(image_info)))
    {
      is_auth_required = TRUE;
    }
    else
    {
#ifdef FEATURE_BOOT_ALLOW_UNSIGNED_IMAGE
      is_auth_required = FALSE;
      status = E_SECBOOT_SUCCESS;
#else
      /* unsigned image in non-secure device is not allowed.*/
      return E_SECBOOT_UNSUPPORTED;
#endif
    }
  }
  else
  {
    return E_SECBOOT_HW_FAIL;
  }    

  if(is_auth_required)
  {
    /* Initializes the secboot handle and Crypto HW engine */
    status = secboot_ftbl_ptr->secboot_init( crypto_hash_ftbl_ptr, &sec_handle);
    if( E_SECBOOT_SUCCESS == status )
    {
      do
      {
        /* auth signed image for non-secure device. fuse info is extracted from the image. */
        /* for secure device, fuse info is always from the qfprom hardware. */
        if ( FALSE == is_auth_enabled )
        {
          status = secboot_get_fuse_info_from_image(&image_fuse_info, image_info);
          if(E_SECBOOT_SUCCESS != status)
          {
            break;
          }

          status = secboot_ftbl_ptr->secboot_init_fuses(&sec_handle, image_fuse_info);
          if (E_SECBOOT_SUCCESS != status)
          {
            break;
          }
        }

        status = secboot_ftbl_ptr->secboot_authenticate( &sec_handle,
                                                         code_segment,
                                                         image_info,
                                                         verified_info);
        /* Check the result of authentication */ 
        if(E_SECBOOT_SUCCESS != status)
        {
         /* for non-sec device: ignore the untrusted root, as oem might use oem-signed image */
          if ( (is_auth_enabled == FALSE) && (status == E_SECBOOT_UNTRUSTED_ROOT) )
          {
            status = E_SECBOOT_SUCCESS;
          }
        }
      }
      while(0);
        
      /* Deinitializes the secboot handle and Crypto HW engine*/
      /* deinit always be run, and don't report the status if already an error from previous function */
      deinit_status = secboot_ftbl_ptr->secboot_deinit(&sec_handle);
      if (E_SECBOOT_SUCCESS == status)
      {
        status = deinit_status;
      }
    }
  }
    
  return status;  
}

