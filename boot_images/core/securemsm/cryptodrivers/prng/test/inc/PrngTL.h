#ifndef _PrngTL_
#define _PrngTL_

/**
@file PrngTL.h
     The test API file prng.
*/

/*===========================================================================

                    PRNG Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/21/12   amen     initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

#define MAX_NUM_SAMPLES (256)
#define NUM_OF_RUNS 2

/*===========================================================================
                 FUNCTION DECLARATIONS
===========================================================================*/

uint16 PrngTL_hardware_prng( void );

#endif /* _PrngTL_ */


