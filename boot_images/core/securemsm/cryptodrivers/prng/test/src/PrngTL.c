
#include "math.h"
#include "PrngML.h"
#include "PrngCL.h"
#include "PrngTL.h"
#include "string.h"


/*===========================================================================

            REGIONAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/

int distribute[MAX_NUM_SAMPLES]={0,};
float prob[MAX_NUM_SAMPLES];
float results[NUM_OF_RUNS]={0.0};

void PrngTL_hw_prng_cleanup( void );
void PrngTL_hw_prng_record_distribute(unsigned char num);
float PrngTL_hw_prng_calculate_entrophy (int i);



void PrngTL_hw_prng_cleanup( void )
{
  memset(distribute, 0x0 , sizeof(distribute));
  memset(prob, 0x0 , sizeof(prob));
}

void PrngTL_hw_prng_record_distribute(unsigned char num)
{
  distribute[num]++;
}

float PrngTL_hw_prng_calculate_entrophy (int i)
{
  int n;
  float h=0.0;

  for (n=0; n<MAX_NUM_SAMPLES; n++)
  {
    prob[n] =  (float)(distribute[n])/(float) i;
  }

  for (n=0; n<MAX_NUM_SAMPLES; n++)
  {
    h = h + prob[n] * (-log(prob[n])/log(2)); 
  }
  //MSG_HIGH ("Number of data = %d, Entropy = %10.7f \n", i, h, 0);
  return h;
}



uint16 PrngTL_hardware_prng( void )
{
   uint32 rng_random_number = 0;
   uint32 iter = 0;
   uint32 bytes = 1; // Length of Random Number Sample
   uint16 errors = 0;
   PrngML_Result_Type result_prng = (PrngML_Result_Type)0;
   uint32 num_result = 0;
   uint32 samples = 10240; // total number of Random Samples
   
  /* Entropy calculator */ 
  // Cleanup
  PrngTL_hw_prng_cleanup();
  //Clean results array 
  memset(results, 0x0, sizeof(results));

  while (num_result < NUM_OF_RUNS)   //k value
  {
    //Klocwork fix
    result_prng = PrngML_getdata_lite((uint8*)&rng_random_number, bytes);
    if(result_prng != 0)
    {
      errors++;
      break;
    }

    PrngTL_hw_prng_record_distribute((uint8)rng_random_number);

    iter++;
    if (0 == (iter % samples))
    {
      results[num_result++] = PrngTL_hw_prng_calculate_entrophy(iter);
      PrngTL_hw_prng_cleanup();
      iter = 0;
    }
  }
  
  return errors;
} /* PrngTL_hardware_prng() */
