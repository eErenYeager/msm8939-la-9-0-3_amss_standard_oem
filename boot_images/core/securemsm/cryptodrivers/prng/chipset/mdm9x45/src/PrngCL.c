/**
@file PrngCL.c 
@brief PRNG Engine source file 
*/

/*===========================================================================

                     P R N G E n g i n e D r i v e r

DESCRIPTION
  This file contains declarations and definitions for the
  interface between PRNG engine api and the PRNG hardware

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2009 - 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

 $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/prng/chipset/mdm9x45/src/PrngCL.c#1 $
 $DateTime: 2015/03/19 01:58:37 $
 $Author: pwbldsvc $ 

when         who     what, where, why
--------     ---     ----------------------------------------------------------
11/03/2013   nk       Cleaned up deinit routine write and increased random buffer placeholder  
9/20/2012    amen     PRNG_LITE 
============================================================================*/


#include "comdef.h"

#include "PrngCL.h"
#include "PrngEL.h"
#include "PrngCL_target.h"
#include "string.h"

/**
 * @brief This function initializes PRNG Engine.
 *
 * @param None
 * @param None
 *
 * @return None
 *
 * @see PrngCL_getdata
 *
 */
PrngCL_Result_Type PrngCL_lite_init(void)
{
   PrngCL_Result_Type stat = PRNGCL_ERROR_NONE;
   /* Enable clock for random number generator */
   stat = (PrngCL_Result_Type)PrngEL_ClkEnable();

   HWIO_OUT(SEC_PRNG_CONFIG,
        ((1 << HWIO_SHFT(SEC_PRNG_CONFIG, SEC_PRNG_EN)) ));

   HWIO_OUT(SEC_PRNG_LFSR_CFG,
        (0x5                                                                                 | //Set RING_OSC0_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR0_EN)) | //Enable LFSR0_EN
        0x50                                                                                 | //Set RING_OSC1_CFG to 101, which is feedback point 1 
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR1_EN)) | //Enable LFSR1_EN
        0x500                                                                               | //Set RING_OSC2_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR2_EN)) | //Enable LFSR2_EN
        0x5000                                                                             | //Set RING_OSC3_CFG to 101, which is feedback point 1
        (1 << HWIO_SHFT(SEC_PRNG_LFSR_CFG, SEC_LFSR3_EN))    //Enable LFSR3_EN
        ));

   return stat;
}
/**
 * @brief  This function returns the contents of the PRNG_DATA_OUT register.
 *
 * @param random_ptr [in]pointer to random number
 * @param random_len [in]length of random number
 *
 * @return PrngCL_Resut_Type
 *
 * @see PrngCL_init
 *
 */
PrngCL_Result_Type PrngCL_getdata(uint8*  random_ptr,  uint16  random_len)
{
  PrngCL_Result_Type ret_val = PRNGCL_ERROR_NONE;
  uint32 tmp_iv;
  uint32 i;
  uint32 tmp_random_len = random_len;
  const uint16 unit_random_len = 4;
   
  if(!random_ptr || !random_len){
      return PRNGCL_ERROR_INVALID_PARAM; 
   }

  if(random_len%unit_random_len !=0){
  	tmp_random_len += (unit_random_len - random_len%unit_random_len);
  	}
  
  /* Generate random numbers. Unit length of PRNG is 4 Bytes. 
  *  Hence, multiple PRNG random numbers are generated if random_len > 4 */
  for (i=0; i<(tmp_random_len/unit_random_len); i++){

      /* check PRNG_STATUS */
      while(1)
        {
             uint32 prng_status = HWIO_IN(SEC_PRNG_STATUS);

             if(prng_status &  SEC_HWIO_PRNG_STATUS_DATA_AVAIL_BMSK)	{
                break;
             }
        }


       /* Get the randum number from PRNG_DATA_OUT and check if it is not 0 */
       while(1)
           {
                tmp_iv = HWIO_IN(SEC_PRNG_DATA_OUT);

                if (tmp_iv != 0) break;
  	     }
	   
       /* Generate mutiple of 4 bytes random numbers. Then, if necessary, adjust total random
        *  number to random_len size */
       if( i < (tmp_random_len/unit_random_len -1) ){
           memcpy(random_ptr, &tmp_iv, unit_random_len);
           random_ptr += unit_random_len;
          }
	   
       else {
           if ( (random_len%unit_random_len) !=0){
	        memcpy(random_ptr, &tmp_iv, random_len%unit_random_len);
           	}
           else {
               memcpy(random_ptr, &tmp_iv, unit_random_len);
		}
           }
        }

  return ret_val;
}

/**
 * @brief This function de-initializes PRNG Engine.
 *
 * @param None
 * @param None
 *
 * @return None
 *
 * @see PrngCL_init
 *
 */
PrngCL_Result_Type PrngCL_lite_deinit(void)
{
   PrngCL_Result_Type stat = PRNGCL_ERROR_NONE;

   /* Disable clock for random number generator */
   stat = (PrngCL_Result_Type) PrngEL_ClkDisable();
   return stat;
}
