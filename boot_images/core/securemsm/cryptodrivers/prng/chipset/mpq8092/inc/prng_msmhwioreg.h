#ifndef __PRNG_MSMHWIOREG_H__
#define __PRNG_MSMHWIOREG_H__
/*
===========================================================================
*/
/**
  @file prng_msmhwioreg.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    PERIPH_SS_PRNG_PRNG

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/prng/chipset/mpq8092/inc/prng_msmhwioreg.h#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  ===========================================================================
*/

#include "msmhwiobase.h"

/*----------------------------------------------------------------------------
 * MODULE: PERIPH_SS_PRNG_PRNG
 *--------------------------------------------------------------------------*/

#define PERIPH_SS_PRNG_PRNG_REG_BASE                                                         (PERIPH_SS_BASE      + 0x003ff000)

#define HWIO_PERIPH_SS_PRNG_PRNG_DATA_OUT_ADDR                                               (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000000)
#define HWIO_PERIPH_SS_PRNG_PRNG_DATA_OUT_RMSK                                               0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_DATA_OUT_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_DATA_OUT_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_DATA_OUT_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_DATA_OUT_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_DATA_OUT_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_DATA_OUT_PRNG_DATA_OUT_BMSK                                 0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_DATA_OUT_PRNG_DATA_OUT_SHFT                                        0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_ADDR                                                 (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000004)
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RMSK                                                      0x3ff
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_STATUS_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_STATUS_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_CURRENT_OPERATION_BMSK                                    0x300
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_CURRENT_OPERATION_SHFT                                      0x8
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_GENERATE_REQUIRED_BMSK                                     0x80
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_GENERATE_REQUIRED_SHFT                                      0x7
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RESEED_REQUIRED_BMSK                                       0x40
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RESEED_REQUIRED_SHFT                                        0x6
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_INSTANTIATE_REQUIRED_BMSK                                  0x20
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_INSTANTIATE_REQUIRED_SHFT                                   0x5
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RING_OSC3_HEALTHY_BMSK                                     0x10
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RING_OSC3_HEALTHY_SHFT                                      0x4
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RING_OSC2_HEALTHY_BMSK                                      0x8
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RING_OSC2_HEALTHY_SHFT                                      0x3
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RING_OSC1_HEALTHY_BMSK                                      0x4
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RING_OSC1_HEALTHY_SHFT                                      0x2
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RING_OSC0_HEALTHY_BMSK                                      0x2
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_RING_OSC0_HEALTHY_SHFT                                      0x1
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_DATA_AVAIL_BMSK                                             0x1
#define HWIO_PERIPH_SS_PRNG_PRNG_STATUS_DATA_AVAIL_SHFT                                             0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_ADDR                                             (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000008)
#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_RMSK                                             0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_MAJOR_VERSION_BMSK                               0xffff0000
#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_MAJOR_VERSION_SHFT                                     0x10
#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_MINOR_VERSION_BMSK                                   0xff00
#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_MINOR_VERSION_SHFT                                      0x8
#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_STEP_BMSK                                              0xff
#define HWIO_PERIPH_SS_PRNG_PRNG_HW_VERSION_STEP_SHFT                                               0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_ENTROPY_CNTR_ADDR                                           (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000010)
#define HWIO_PERIPH_SS_PRNG_PRNG_ENTROPY_CNTR_RMSK                                           0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_ENTROPY_CNTR_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_ENTROPY_CNTR_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_ENTROPY_CNTR_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_ENTROPY_CNTR_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_ENTROPY_CNTR_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_ENTROPY_CNTR_ENTROPY_CNTR_BMSK                              0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_ENTROPY_CNTR_ENTROPY_CNTR_SHFT                                     0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_CNTR_ADDR                                            (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000014)
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_CNTR_RMSK                                            0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_CNTR_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_CNTR_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_CNTR_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_CNTR_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_CNTR_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_CNTR_SAMPLE_CNTR_BMSK                                0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_CNTR_SAMPLE_CNTR_SHFT                                       0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_CNTR_ADDR                                               (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000018)
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_CNTR_RMSK                                               0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_CNTR_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_GEN_CNTR_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_GEN_CNTR_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_CNTR_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_GEN_CNTR_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_CNTR_PRNG_GEN_CNTR_BMSK                                 0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_CNTR_PRNG_GEN_CNTR_SHFT                                        0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_CNTR_ADDR                                            (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x0000001c)
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_CNTR_RMSK                                            0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_CNTR_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_RESEED_CNTR_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_RESEED_CNTR_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_CNTR_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_RESEED_CNTR_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_CNTR_RESEED_CNTR_BMSK                                0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_CNTR_RESEED_CNTR_SHFT                                       0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_CNTR_ADDR                                       (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000020)
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_CNTR_RMSK                                       0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_CNTR_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_CNTR_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_CNTR_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_CNTR_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_CNTR_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_CNTR_INSTANTIATE_CNTR_BMSK                      0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_CNTR_INSTANTIATE_CNTR_SHFT                             0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_ADDR                                                  (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000024)
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_RMSK                                                      0x3fff
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_RNG_STATE_BMSK                                            0x3c00
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_RNG_STATE_SHFT                                               0xa
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_HASHGEN_STATE_BMSK                                         0x380
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_HASHGEN_STATE_SHFT                                           0x7
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_HASH_DF_STATE_BMSK                                          0x70
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_HASH_DF_STATE_SHFT                                           0x4
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_PAD_STATE_BMSK                                               0xf
#define HWIO_PERIPH_SS_PRNG_PRNG_DEBUG_PAD_STATE_SHFT                                               0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_ADDR                                               (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000100)
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RMSK                                                  0x1ffff
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_OUT(v)      \
        out_dword(HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_ADDR,v)
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_ADDR,m,v,HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_IN)
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR_OVRID_ON_BMSK                                    0x10000
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR_OVRID_ON_SHFT                                       0x10
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR3_EN_BMSK                                          0x8000
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR3_EN_SHFT                                             0xf
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RING_OSC3_CFG_BMSK                                     0x7000
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RING_OSC3_CFG_SHFT                                        0xc
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR2_EN_BMSK                                           0x800
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR2_EN_SHFT                                             0xb
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RING_OSC2_CFG_BMSK                                      0x700
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RING_OSC2_CFG_SHFT                                        0x8
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR1_EN_BMSK                                            0x80
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR1_EN_SHFT                                             0x7
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RING_OSC1_CFG_BMSK                                       0x70
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RING_OSC1_CFG_SHFT                                        0x4
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR0_EN_BMSK                                             0x8
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_LFSR0_EN_SHFT                                             0x3
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RING_OSC0_CFG_BMSK                                        0x7
#define HWIO_PERIPH_SS_PRNG_PRNG_LFSR_CFG_RING_OSC0_CFG_SHFT                                        0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_ADDR                                                 (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000104)
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_RMSK                                                       0x3f
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_OUT(v)      \
        out_dword(HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_ADDR,v)
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_ADDR,m,v,HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_IN)
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_TEST_OUT_SEL_BMSK                                          0x3c
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_TEST_OUT_SEL_SHFT                                           0x2
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_PRNG_EN_BMSK                                                0x2
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_PRNG_EN_SHFT                                                0x1
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_SW_RESET_BMSK                                               0x1
#define HWIO_PERIPH_SS_PRNG_PRNG_CONFIG_SW_RESET_SHFT                                               0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_ADDR                                 (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000108)
#define HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_RMSK                                 0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_OUT(v)      \
        out_dword(HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_ADDR,v)
#define HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_ADDR,m,v,HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_IN)
#define HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_PERSONALIZATION_STRING_BMSK          0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_PERSONALIZATION_STRING_PERSONALIZATION_STRING_SHFT                 0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_ADDR                                            (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000110)
#define HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_RMSK                                            0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_OUT(v)      \
        out_dword(HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_ADDR,v)
#define HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_ADDR,m,v,HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_IN)
#define HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_NUM_ENTROPY_BYTES_BMSK                          0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_NUM_ENTROPY_NUM_ENTROPY_BYTES_SHFT                                 0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_ADDR                                            (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000114)
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_RMSK                                            0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_OUT(v)      \
        out_dword(HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_ADDR,v)
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_ADDR,m,v,HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_IN)
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_SAMPLE_FREQ_BMSK                                0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_SAMPLE_FREQ_SAMPLE_FREQ_SHFT                                       0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_ADDR                                              (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000118)
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_RMSK                                              0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_OUT(v)      \
        out_dword(HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_ADDR,v)
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_ADDR,m,v,HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_IN)
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_REQUESTED_WORDS_BMSK                              0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_GEN_WORDS_REQUESTED_WORDS_SHFT                                     0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_ADDR                                            (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x0000011c)
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_RMSK                                            0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_OUT(v)      \
        out_dword(HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_ADDR,v)
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_ADDR,m,v,HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_IN)
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_RESEED_FREQ_BMSK                                0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_RESEED_FREQ_RESEED_FREQ_SHFT                                       0x0

#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_ADDR                                       (PERIPH_SS_PRNG_PRNG_REG_BASE      + 0x00000120)
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_RMSK                                       0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_IN          \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_ADDR, HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_RMSK)
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_INM(m)      \
        in_dword_masked(HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_ADDR, m)
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_OUT(v)      \
        out_dword(HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_ADDR,v)
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_OUTM(m,v) \
        out_dword_masked_ns(HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_ADDR,m,v,HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_IN)
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_INSTANTIATE_FREQ_BMSK                      0xffffffff
#define HWIO_PERIPH_SS_PRNG_PRNG_INSTANTIATE_FREQ_INSTANTIATE_FREQ_SHFT                             0x0

#endif /* __PRNG_MSMHWIOREG_H__ */
