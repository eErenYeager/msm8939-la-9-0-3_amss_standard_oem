#ifndef _CeTL_HWKEY_
#define _CeTL_HWKEY_

/**
@file CeTL_HWKey.h
     The test API file for Hardware Key test case.
*/

/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/inc/CeTL_HWKey.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/17/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeTL_Comdef.h"
#include "CeML.h"

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

typedef enum
{
  CETL_HWKEY_NULL                    = 0,
  CETL_HWKEY_DUMMY                   = 1,
  CETL_HWKEY_PRIMARY                 = 2,
  CETL_HWKEY_SECONDARY               = 3,
  CETL_HWKEY_SOFTWARE                = 4,
  CETL_HWKEY_FAILURE                 = 5
} CeTLHWKeyType;

/*--------------------------------------------------------------------------
Hardware key vectors
---------------------------------------------------------------------------*/
/* NULL HW key: CE uses device hw key or dummy hw key 
   when passed with NULL as key param
*/
static uint8 cetl_hwkey_null[] = {0};

/*
  Dummy HW Key: This is msm exposed key to the crypto engine
  when JTAG is enabled. usually this remains const.
  Reference Document: Shelby HDD 80-N3880-11 Rev. B page 766
  Dummy HW key size for AES256 = 32 bytes
  */
static uint8 cetl_hwkey_dummy[] =
{
  0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11,
  0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff,
  0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11,
  0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff
};
/*=============================================================
   EFUSE list to be blown to enable (Primary/Secondary)HW key
1. tic_debug_disable
2. rpm_arm_disable 
3. sc_spiden_disable
4. sc_dbgen_disable
5. dap_spiden_disable
6. dap_dbgen_disable
 Note: This might differ from PLs to PLS, 
 Refer the HDD document 
==============================================================*/
/*
  Primary HW Key: This should be the key that is to be blown 
  on the msm device. before executing this test case.
  We need to also blow effuses corresponding to 
  jtag disable etc. The exact information of the fuses 
  to be blown can be found HDD documents for a PL
  Primary HW key size for AES256 = 32 bytes
*/
static uint8 cetl_hwkey_primary[] =
{
  0x0f, 0x0e, 0x0d, 0x0c, 0x0b, 0x0a, 0x09, 0x08,
  0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00,
  0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6,
  0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c
};

/*
  Secondary HW Key: This should be the key that is to be blown 
  on the msm device oem key. before executing this test case.
  We need to also blow effuses corresponding to 
  jtag disable etc. The exact information of the fuses 
  to be blown can be found HDD documents for a PL
  Secondary HW key size for AES256 = 32 bytes
*/
static uint8 cetl_hwkey_secondary[] =
{
  0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6,
  0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c,
  0x0f, 0x0e, 0x0d, 0x0c, 0x0b, 0x0a, 0x09, 0x08,
  0x07, 0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00
};

/* Dummy software key  size for AES256 = 32 bytes */
static uint8 cetl_hwkey_software[] =
{
  0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
  0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
  0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c,
  0x0f, 0x0e, 0x0d, 0x0c, 0x0b, 0x0a, 0x09, 0x08
};

/* Plain text Data size = 32 bytes */
static uint8 cetl_aes256_plain_text[] =
{
  0xed, 0x09, 0x0a, 0x0b, 0x0d, 0x0e, 0x0f, 0x10, 
  0x12, 0x13, 0x14, 0x15, 0x17, 0x18, 0x19, 0x1a, 
  0x1c, 0x1d, 0x1e, 0x1f, 0x21, 0x22, 0x23, 0x24, 
  0x26, 0x27, 0x28, 0x29, 0x2b, 0x2c, 0x2d, 0x2e
};

/* Place holder dummy key for cipher text 
   Data size = 32 bytes 
*/
static uint8 cetl_aes256_cipher_text_dummy[] =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/* Place holder null key for cipher text 
   Data size = 32 bytes 
*/
static uint8 cetl_aes256_cipher_text2_null[] =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/* Place holder real key for cipher text 
   Data size = 32 bytes 
*/
static uint8 cetl_aes256_cipher_text3_real[] =
{
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

typedef struct
{
  CeMLCipherModeType mode;
  uint8 *plain_text;
  uint32 plain_text_len;
  uint8 *key_ptr;
  uint32 key_len;
  uint8 *iv;
  uint8 *cipher_text;
} ce_aes_hwkey_vector_type;

/* AES-256 hwkey test vectors array */
static ce_aes_hwkey_vector_type ce_aes_hwkey_vectors[] = 
{
  {CEML_CIPHER_MODE_ECB, cetl_aes256_plain_text,      //AEA256 & Use HW key
   sizeof(cetl_aes256_plain_text), NULL, 
   32, NULL, cetl_aes256_cipher_text2_null}
  ,
  {CEML_CIPHER_MODE_ECB, cetl_aes256_plain_text,      //AES256 & Hardware dummy key
   sizeof(cetl_aes256_plain_text), cetl_hwkey_dummy,
   32, NULL, cetl_aes256_cipher_text_dummy}
  ,
  {CEML_CIPHER_MODE_ECB, cetl_aes256_plain_text,      //AES256 & Msm blown primary key
   sizeof(cetl_aes256_plain_text), cetl_hwkey_primary,
   32, NULL, cetl_aes256_cipher_text3_real}
  ,
   {CEML_CIPHER_MODE_ECB, cetl_aes256_plain_text,      //AES256 & Msm blown secondary key
   sizeof(cetl_aes256_plain_text), cetl_hwkey_secondary,
   32, NULL, cetl_aes256_cipher_text3_real}
};

/**
 * @brief This function will encrypt the input data
 *        structure and compute the final digest hash value.
 *
 * @param input_data  [in] Pointer to input data
 * @param input_len   [in] Input data length
 * @param output_data [Out] Pointer to output data
 * @param output_len_ptr [in] Pointer to output data length
 * @param encrypt [in] encrypt flag
 * @param mode [Out] cipher mode
 *
 * @return CeTLErrorType
 *
 * @see CeTLHWKeyCompare, CeTLHWKey
 *
 */
CeTLErrorType CeTLHWKeyEncrypt(
  uint8                *plain_text,
  uint32               plain_text_len,
  uint8                *cipher_text,
  uint8                *key_ptr,
  uint32               key_len,
  uint8                *iv,
  CeMLCipherModeType   mode
  );

/**
 * @brief This function will compare two data buffers, and populate 
 *        comparision result in result variable.
 *
 * @param data1  [in] Pointer to data1 
 * @param data2  [in] Pointer to data2 
 * @param result [Out] Pointer to compare result
 *
 * @return CeTLErrorType  
 *
 * @see CeTLHWKey, CeTLHWKeyEncrypt
 *
 */
CeTLErrorType CeTLHWKeyCompare(
  uint8        *data1,
  uint8        *data2,
  uint8        data_size,
  uint8        *result
  );

/**
 * @brief This function will determine if hw key is used by CE.
 *
 * @return CeTLErrorType  
 *
 * @see CeTLHWKeyCompare, CeTLHWKeyEncrypt
 *
 */
CeTLErrorType CeTLHWKeyTests( void );

#endif /* _CeTL_HWKEY_ */
