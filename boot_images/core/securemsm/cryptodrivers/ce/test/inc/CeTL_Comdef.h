#ifndef _CeTL_Comdef_
#define _CeTL_Comdef_
/*===========================================================================

                    Crypto Engine Test Library Core API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/inc/CeTL_Comdef.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/19/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "boot_comdef.h"

/* -------------------------------------------------------------------------
Crypto Engine Test Library Error codes
-------------------------------------------------------------------------- */
typedef enum
{
  CETL_ERROR_SUCCESS                = 0x0,
  CETL_ERROR_FAILURE                = 0x1,
  CETL_ERROR_INVALID_PARAM          = 0x2,
  CETL_ERROR_NOT_SUPPORTED          = 0x3
} CeTLErrorType;

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

#endif /* _CeTL_Comdef_ */




