
#ifndef _CeTL_kdf
#define _CeTL_kdf

/**
@file CeTL_kdf.h
     The KDF test api
*/
/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


KDF based testing

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/inc/CeTL_kdf.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/18/13   amen     Initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeTL_Comdef.h"
#include "CeTL.h"


/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

void CeTLKDFTests(void);

#endif
