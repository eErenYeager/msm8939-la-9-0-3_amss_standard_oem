#ifndef _CeTL_
#define _CeTL_

/**
@file CeTL.h
     The test API file crypto engine.
*/

/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/inc/CeTL.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/07/11   nk      Added test config macros
09/17/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

/* Test config control */
#define HASHTEST              0
#define HASHPROFILETEST       0
#define HASHREGRESSIONTEST    0
#define CIPHERTEST            0
#define CIPHERPROFILETEST     0
#define CIPHERREGRESSIONTEST  0
#define HWKEYTEST             0
#define KDFTEST               1

/*===========================================================================
                 FUNCTION DECLARATIONS
===========================================================================*/
/* The Crypto Engine Test Library execute function */
void CeTL_Execute( void );

#endif /* _CeTL_ */
