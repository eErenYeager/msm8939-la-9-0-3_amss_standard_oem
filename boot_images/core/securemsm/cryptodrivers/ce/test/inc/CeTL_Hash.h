#ifndef _CeTL_HASH_
#define _CeTL_HASH_

/**
@file CeTL_Hash.h
     The test API file for hash.
*/

/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/


/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/inc/CeTL_Hash.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/17/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeTL_Comdef.h"
#include "CeML.h"

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/


/*--------------------------------------------------------------------------
SHA1 vectors
---------------------------------------------------------------------------*/

/* SHA1/SHA256 input data */
static uint8 cetl_sha_plain_text[] = 
{
  "Test vector from febooti.com"
};

/* SHA 1 test vectors */
static uint8 cetl_sha1_digest_text[] = 
{
  0xa7, 0x63, 0x17, 0x95, 0xf6, 0xd5, 0x9c, 0xd6,
  0xd1, 0x4e, 0xbd, 0x00, 0x58, 0xa6, 0x39, 0x4a,
  0x4b, 0x93, 0xd8, 0x68
};

/* SHA 256 test vectors */
static uint8 cetl_sha256_digest_text[] = 
{
  0x07, 0x7b, 0x18, 0xfe, 0x29, 0x03, 0x6a, 0xda,
  0x48, 0x90, 0xbd, 0xec, 0x19, 0x21, 0x86, 0xe1,
  0x06, 0x78, 0x59, 0x7a, 0x67, 0x88, 0x02, 0x90,
  0x52, 0x1d, 0xf7, 0x0d, 0xf4, 0xba, 0xc9, 0xab
};

typedef struct
{
  CeMLHashAlgoType algo;
  uint8 *msg;
  uint16 msg_len;
  uint8 *digest;
} ce_sha_vector_type;

/* SHA test vectors array */
static ce_sha_vector_type ce_sha_tests_vectors[] = 
{
  {CEML_HASH_ALGO_SHA1, cetl_sha_plain_text,
   sizeof(cetl_sha_plain_text) - 1, cetl_sha1_digest_text} 
  ,
  {CEML_HASH_ALGO_SHA256, cetl_sha_plain_text,
   sizeof(cetl_sha_plain_text) - 1, cetl_sha256_digest_text} 
};

/* Caluculate Hash */
int CeTLHashCalculate(
                       CeMLHashAlgoType algo, 
                       uint8* msg,
                       uint32 msg_len, 
                       uint8* digest );

/* Execute Crypto Engine HASH test vectors */
void CeTLHashTests( void );


#endif /* _CeTL_HASH_ */
