#ifndef _CeTL_Env_
#define _CeTL_Env_
/*===========================================================================

                    Crypto Engine Test Library Core API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/inc/CeTL_Env.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/19/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include <string.h>
#include <stdlib.h>
#include "CeEL.h"
#include "boot_logger.h"
#include "boot_logger_jtag.h"

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/
void CeTL_Message_Logger( void  *buffer);

/* Message defines */
#define CR "\n"
#define CETL_MSG_HIGH(A, B, C, D) \
   CeTL_Message_Logger(A);

#define CETL_MSG_MED(A, B, C, D) CETL_MSG_HIGH(A, B, C, D)
#define CETL_MSG_LOW(A, B, C, D) CETL_MSG_HIGH(A, B, C, D)
#define CETL_MSG_ERROR(A, B, C, D) CETL_MSG_HIGH(A, B, C, D)
#define CETL_MSG_FATAL(A, B, C, D) CETL_MSG_HIGH(A, B, C, D)

/* Memory operation defines */
#define CETLMEMCMP(A, B, C) memcmp(A, B, C)
#define CETLMEMCPY(A, B, C) CeElMemcpy (A, B, C)
#define CETLMEMSET(A, B, C) CeElMemset (A, B, C)

/* CE version 1 or 2*/
#define CE_VERSION() 1

// used only in boot environment
/*Stringizing Operator */
#define QUOTEME(x) #x

/* Boot messages log */
#define CE_BOOT_LOG_MSG(x) \
        boot_log_message(x);  // -> this will be logged as a message

/* Start the timer here */
#define CE_BOOT_LOG_MSG_START_TIMER() \
        boot_log_start_timer();  // -> start timere

/* End the timer here */
#define CE_BOOT_LOG_MSG_END(x) \
        boot_log_stop_timer(x); //-> this will also be logged

#endif /* _CeTL_Env_ */

