/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/src/CeTL_HWKey.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/01/11   nk      Added the Positive messages 
09/18/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeTL_Comdef.h"
#include "CeML.h"
#include "CeTL_HWKey.h"
#include "CeTL_Env.h"

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

/**
 * @brief This function will encrypt the input data
 *        structure and compute the final digest hash value.
 *
 * @param input_data  [in] Pointer to input data
 * @param input_len   [in] Input data length
 * @param output_data [Out] Pointer to output data
 * @param output_len_ptr [in] Pointer to output data length
 * @param encrypt [in] encrypt flag
 * @param mode [Out] cipher mode
 *
 * @return CeTLErrorType
 *
 * @see CeTLHWKeyCompare, CeTLHWKey
 *
 */
CeTLErrorType CeTLHWKeyEncrypt(
  uint8               *plain_text,
  uint32              plain_text_len,
  uint8               *cipher_text,
  uint8               *key_ptr,
  uint32              key_len,
  uint8               *iv,
  CeMLCipherModeType  mode
  )
{
  CeMLCipherDir       cipher_direction;
  CeMLCipherModeType  cipher_mode;
  CeMLCntxHandle*     cntx = NULL;
  CEMLIovecListType   ioVecIn;
  CEMLIovecListType   ioVecOut;
  CEMLIovecType       IovecIn;
  CEMLIovecType       IovecOut;


  //Clear cipher buffer
  CETLMEMSET(cipher_text, 0, plain_text_len);

  /* Input IOVEC */
  ioVecIn.size = 1;
  ioVecIn.iov = &IovecIn;
  ioVecIn.iov[0].dwLen = plain_text_len;
  ioVecIn.iov[0].pvBase = plain_text; 

  /* Output IOVEC */
  ioVecOut.size = 1;
  ioVecOut.iov = &IovecOut;
  ioVecOut.iov[0].dwLen = plain_text_len;
  ioVecOut.iov[0].pvBase = cipher_text;

  CeMLInit();

  // Use AES256 by default
  CeMLCipherInit(&cntx, CEML_CIPHER_ALG_AES256);

  //Set parameters so we can do the encrypt
  cipher_direction = CEML_CIPHER_ENCRYPT;
  CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_DIRECTION, &cipher_direction, sizeof(CeMLCipherDir));

  cipher_mode = mode;
  CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_MODE, &cipher_mode, sizeof(CeMLCipherModeType));

  CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_KEY, key_ptr, key_len);

  //Check for NULL IV pointer
  if (iv != NULL)
  {
    CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_IV, iv, CEML_AES128_IV_SIZE);
  }

  //Now encrypt the data
  CeMLCipherData(cntx, ioVecIn, &ioVecOut);

  //Free ctx
  CeMLCipherDeInit(&cntx);

  CeMLDeInit();

  return CETL_ERROR_SUCCESS;
}

/**
 * @brief This function will compare two data buffers, and populate 
 *        comparision result in result variable.
 *
 * @param data1  [in] Pointer to data1 
 * @param data2  [in] Pointer to data2 
 * @param result [Out] Pointer to compare result
 *
 * @return CeTLErrorType  
 *
 * @see CeTLHWKey, CeTLHWKeyEncrypt
 *
 */
CeTLErrorType CeTLHWKeyCompare(
  uint8        *data1,
  uint8        *data2,
  uint8        data_size,
  uint8        *result
  )
{
  *result = CETLMEMCMP(data1, data2, data_size);
  return CETL_ERROR_SUCCESS;
}

/**
 * @brief This function will determine if hw key is used by CE.
 *
 * @return CeTLErrorType  
 *
 * @see CeTLHWKeyCompare, CeTLHWKeyEncrypt
 *
 */
CeTLErrorType CeTLHWKeyTests( void )
{
    int result = 0;
    CeTLErrorType tlerror =  CETL_ERROR_FAILURE;

    CETL_MSG_HIGH(" ==================================================",0,0,0);
    CETL_MSG_HIGH(" TESTID: 0003 HWKEYTEST: Hardware key test case ",0,0,0);
    CETL_MSG_HIGH(" ==================================================",0,0,0);
    CETL_MSG_HIGH(" Test Prequistics:  The actual key corresponding to the",0,0,0);
    CETL_MSG_HIGH(" Hardware key vector should be blown, the corresponding",0,0,0);
    CETL_MSG_HIGH(" debug disable fuses should be blown, to enable the actual",0,0,0);


    tlerror = CeTLHWKeyEncrypt(
               ce_aes_hwkey_vectors[CETL_HWKEY_NULL].plain_text,
               ce_aes_hwkey_vectors[CETL_HWKEY_NULL].plain_text_len,
               ce_aes_hwkey_vectors[CETL_HWKEY_NULL].cipher_text,
               ce_aes_hwkey_vectors[CETL_HWKEY_NULL].key_ptr,
               ce_aes_hwkey_vectors[CETL_HWKEY_NULL].key_len,
               ce_aes_hwkey_vectors[CETL_HWKEY_NULL].iv,
               ce_aes_hwkey_vectors[CETL_HWKEY_NULL].mode
              );

     if( tlerror == CETL_ERROR_FAILURE)
     {
       CETL_MSG_ERROR("TESTID: 0003 HWKEYTEST: NULL Hardware key encryption failed!!! ",0,0,0);
       return tlerror;
     }

     tlerror = CeTLHWKeyEncrypt(
                ce_aes_hwkey_vectors[CETL_HWKEY_DUMMY].plain_text,
                ce_aes_hwkey_vectors[CETL_HWKEY_DUMMY].plain_text_len,
                ce_aes_hwkey_vectors[CETL_HWKEY_DUMMY].cipher_text,
                ce_aes_hwkey_vectors[CETL_HWKEY_DUMMY].key_ptr,
                ce_aes_hwkey_vectors[CETL_HWKEY_DUMMY].key_len,
                ce_aes_hwkey_vectors[CETL_HWKEY_DUMMY].iv,
                ce_aes_hwkey_vectors[CETL_HWKEY_DUMMY].mode
               );

     if( tlerror == CETL_ERROR_FAILURE)
     {
       CETL_MSG_ERROR("TESTID: 0003 HWKEYTEST: Dummy Hardware key encryption failed!!! ",0,0,0);
       return tlerror;
     }

     tlerror = CeTLHWKeyCompare(
                ce_aes_hwkey_vectors[CETL_HWKEY_DUMMY].cipher_text, 
                ce_aes_hwkey_vectors[CETL_HWKEY_NULL].cipher_text,
                ce_aes_hwkey_vectors[CETL_HWKEY_NULL].plain_text_len,
                &result
               );
     if( tlerror == CETL_ERROR_FAILURE)
     {
        CETL_MSG_ERROR("TESTID: 0003 HWKEYTEST: Comparision operation failed!!! ",0,0,0);
        return tlerror;
     }

     if(result != 0)
     {
        CETL_MSG_HIGH("TESTID: 0003 HWKEYTEST: Likely Hardware Key is Used!!! ",0,0,0);
        CETL_MSG_HIGH("TESTID: 0003 HWKEYTEST: negative comparision case to be done to confirm!!! ",0,0,0);
     }
     else
     {
       CETL_MSG_ERROR("TESTID: 0003 HWKEYTEST: Comparision with the Dummy Key Passed!!! ",0,0,0);
       CETL_MSG_HIGH("TESTID: 0003 HWKEYTEST: Dummy key used !!! ",0,0,0);
     }

     /* negative comparision */
     if(result != 0)
     {
        CETL_MSG_HIGH(" ==================================================",0,0,0);
        CETL_MSG_HIGH(" TESTID: 0003 HWKEYTEST: Dummy key cipher mismatch, dummy key not used ",0,0,0);
        CETL_MSG_HIGH(" ==================================================",0,0,0);

        if( 1 == CE_VERSION())
        {
          tlerror = CeTLHWKeyEncrypt(
                     ce_aes_hwkey_vectors[CETL_HWKEY_PRIMARY].plain_text,
                     ce_aes_hwkey_vectors[CETL_HWKEY_PRIMARY].plain_text_len,
                     ce_aes_hwkey_vectors[CETL_HWKEY_PRIMARY].cipher_text,
                     ce_aes_hwkey_vectors[CETL_HWKEY_PRIMARY].key_ptr,
                     ce_aes_hwkey_vectors[CETL_HWKEY_PRIMARY].key_len,
                     ce_aes_hwkey_vectors[CETL_HWKEY_PRIMARY].iv,
                     ce_aes_hwkey_vectors[CETL_HWKEY_PRIMARY].mode
                    );
          if( tlerror == CETL_ERROR_FAILURE )
          {
            CETL_MSG_ERROR("TESTID: 0003 HWKEYTEST: Real Primary Hardware key encryption failed!!! ",0,0,0);
            return tlerror;
          }

          /* reset result */
          result = 0;
          CeTLHWKeyCompare(
           ce_aes_hwkey_vectors[CETL_HWKEY_PRIMARY].cipher_text, 
           ce_aes_hwkey_vectors[CETL_HWKEY_NULL].cipher_text,
           ce_aes_hwkey_vectors[CETL_HWKEY_NULL].plain_text_len,
           &result
          );

          if( tlerror == CETL_ERROR_FAILURE)
          {
             CETL_MSG_ERROR("TESTID: 0003 HWKEYTEST: Comparision operation failed!!! ",0,0,0);
             return tlerror;
          }

          /* positive comparision */
          if( result == 0) 
          {
             CETL_MSG_HIGH(" ==================================================",0,0,0);
             CETL_MSG_HIGH(" TESTID: 0003 HWKEYTEST: Actual/Real Primary Hardware Key used ",0,0,0);
             CETL_MSG_HIGH(" ==================================================",0,0,0);
          }
        }/* if CE_VERSION 1 */
        else if( 2 == CE_VERSION() )
        {
          tlerror = CeTLHWKeyEncrypt(
                     ce_aes_hwkey_vectors[CETL_HWKEY_SECONDARY].plain_text,
                     ce_aes_hwkey_vectors[CETL_HWKEY_SECONDARY].plain_text_len,
                     ce_aes_hwkey_vectors[CETL_HWKEY_SECONDARY].cipher_text,
                     ce_aes_hwkey_vectors[CETL_HWKEY_SECONDARY].key_ptr,
                     ce_aes_hwkey_vectors[CETL_HWKEY_SECONDARY].key_len,
                     ce_aes_hwkey_vectors[CETL_HWKEY_SECONDARY].iv,
                     ce_aes_hwkey_vectors[CETL_HWKEY_SECONDARY].mode
                    );
          if( tlerror == CETL_ERROR_FAILURE )
          {
            CETL_MSG_ERROR(" TESTID: 0003 HWKEYTEST: Real Secondary Hardware key encryption failed!!! ",0,0,0);
            return tlerror;
          }

          /* reset result */
          result = 0;
          CeTLHWKeyCompare(
           ce_aes_hwkey_vectors[CETL_HWKEY_SECONDARY].cipher_text, 
           ce_aes_hwkey_vectors[CETL_HWKEY_NULL].cipher_text,
           ce_aes_hwkey_vectors[CETL_HWKEY_NULL].plain_text_len,
           &result
          );

          if( tlerror == CETL_ERROR_FAILURE)
          {
             CETL_MSG_ERROR("TESTID: 0003 HWKEYTEST: Comparision operation failed!!! ",0,0,0);
             return tlerror;
          }

          /* positive comparision */
          if( result == 0) 
          {
             CETL_MSG_HIGH(" ==================================================",0,0,0);
             CETL_MSG_HIGH(" TESTID: 0003 HWKEYTEST: Actual/Real Secondary Hardware Key used ",0,0,0);
             CETL_MSG_HIGH(" ==================================================",0,0,0);
          }
        } /* else CE_VERSION 2*/
  
     }/* negative comparision */

  return CETL_ERROR_SUCCESS;
}

