/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/src/CeTL_Cipher.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/18/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeTL_Comdef.h"
#include "CeML.h"
#include "CeTL_Cipher.h"
#include "CeTL_Env.h"



/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

/* Execute Crypto AES-128 and AES-256 test vectors */
int CeTLAESOperation( CeMLCipherModeType mode, 
                      uint8 *pt,
                      uint32 pt_len,
                      uint8 *key, 
                      uint32 key_len, 
                      uint8 *iv, 
                      uint8 *ct)
{

  CeMLCipherDir       cipher_direction;
  CeMLCipherModeType  cipher_mode;
  CeMLCntxHandle*     cntx = NULL;
  CEMLIovecListType   ioVecIn;
  CEMLIovecListType   ioVecOut;
  CEMLIovecType       IovecIn;
  CEMLIovecType       IovecOut;
  uint8               *ct_tmp = NULL;
  uint8               *pt_tmp = NULL;
  uint8               array_ct[100] = {0};
  uint8               array_pt[100] = {0};
  int                 errors = 0;

  //Allocate temp ct buffer
  if(pt_len < 100)
  {
    ct_tmp = array_ct;
  }
  else
  {
    CETL_MSG_HIGH("TESTID:0002 AESTEST memory allocation failed",0,0,0);
  }

  //Clear temp buffer
  CETLMEMSET(ct_tmp, 0, pt_len);

  /* Input IOVEC */
  ioVecIn.size = 1;
  ioVecIn.iov = &IovecIn;
  ioVecIn.iov[0].dwLen = pt_len;
  ioVecIn.iov[0].pvBase = pt; 

  /* Output IOVEC */
  ioVecOut.size = 1;
  ioVecOut.iov = &IovecOut;
  ioVecOut.iov[0].dwLen = pt_len;
  ioVecOut.iov[0].pvBase = ct_tmp;

  CeMLInit();

  if (key_len == CEML_AES128_IV_SIZE)
    CeMLCipherInit(&cntx, CEML_CIPHER_ALG_AES128);
  else
    CeMLCipherInit(&cntx, CEML_CIPHER_ALG_AES256);

  //Set parameters so we can do the encrypt
  cipher_direction = CEML_CIPHER_ENCRYPT;
  CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_DIRECTION, &cipher_direction, sizeof(CeMLCipherDir));

  cipher_mode = mode;
  CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_MODE, &cipher_mode, sizeof(CeMLCipherModeType));

  CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_KEY, key, key_len);

  //Check for NULL IV pointer
  if (iv != NULL)
  {
    CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_IV, iv, CEML_AES128_IV_SIZE);
  }
  
  //Now encrypt the data
  CeMLCipherData(cntx, ioVecIn, &ioVecOut);

  //If NULL key pointer we are using HW key so don't compare encrypted result
  if (key != NULL)
  {
    //Now compare encrypted results
    if(CETLMEMCMP(ct, ct_tmp, pt_len) != 0) 
    {
      errors++;
    }
  }

  //Allocate temp pt buffer
  if(pt_len < 100)
  {
    pt_tmp = array_pt;
  }
  else
  {
    CETL_MSG_HIGH("TESTID:0002 AESTEST memory allocation failed",0,0,0);
  }

  //Clear temp buffer
  CETLMEMSET(pt_tmp, 0, pt_len);

  /* Input IOVEC */
  ioVecIn.size = 1;
  ioVecIn.iov = &IovecIn;
  ioVecIn.iov[0].dwLen = pt_len;
  ioVecIn.iov[0].pvBase = ct_tmp; 

  /* Output IOVEC */
  ioVecOut.size = 1;
  ioVecOut.iov = &IovecOut;
  ioVecOut.iov[0].dwLen = pt_len;
  ioVecOut.iov[0].pvBase = pt_tmp;

  //We must set parameters again so we can do the decrypt
  cipher_direction = CEML_CIPHER_DECRYPT;
  CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_DIRECTION, &cipher_direction, sizeof(CeMLCipherDir));

  cipher_mode = mode;
  CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_MODE, &cipher_mode, sizeof(CeMLCipherModeType));

  CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_KEY, key, key_len);

  //Check for NULL IV pointer
  if (iv != NULL)
  {
    CeMLCipherSetParam(cntx, CEML_CIPHER_PARAM_IV, iv, CEML_AES128_IV_SIZE);
  }

  //Now decrypt the data 
  CeMLCipherData(cntx, ioVecIn, &ioVecOut);

  //Now compare decrypted results to original plaintext
  if(CETLMEMCMP(pt, pt_tmp, pt_len) != 0) 
  {
    errors++;
  }

  //Free ctx
  CeMLCipherDeInit(&cntx);

  CeMLDeInit();

  return errors;
}

void CeTLAESTests( void )
{
  uint32 num = 0;
  uint32 i = 0;
  uint32 stat = 0;
  uint8  total_errors = 0;

   /* Run AES test vectors */
  num = sizeof(ce_aes_test_vectors) / sizeof(ce_aes_vector_type);

  /* Run through AES128 and AES256 ECB/CBC/CTR test vectors */
  for (i = 0; i < num; i++)
  {
    stat = CeTLAESOperation(ce_aes_test_vectors[i].mode,
                            ce_aes_test_vectors[i].pt,
                            ce_aes_test_vectors[i].pt_len,
                            ce_aes_test_vectors[i].key,
                            ce_aes_test_vectors[i].key_len,
                            ce_aes_test_vectors[i].iv,
                            ce_aes_test_vectors[i].ct);

    if(stat)
    {
      total_errors++;
    }
  }

  if(total_errors == 0) 
  {
    CETL_MSG_HIGH("TESTID:0002 AESTEST < AES test passed >",0,0,0);
  }
  else
  {
    CETL_MSG_ERROR("TESTID:0002 AESTEST < AES test failed !!! Error: %d >",total_errors,0,0);
  }

}

