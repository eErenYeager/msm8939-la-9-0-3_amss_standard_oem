/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/src/CeTL_Hash.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/18/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeTL_Comdef.h"
#include "CeML.h"
#include "CeTL_Hash.h"
#include "CeTL_Env.h"


/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/


/* Execute Crypto HASH test vectors */
int CeTLHashCalculate(
                       CeMLHashAlgoType algo, 
                       uint8* msg,
                       uint32 msg_len, 
                       uint8* digest )
{
  uint8               result_digest[CEML_HASH_DIGEST_SIZE_SHA256] = {0x00};
  uint32              digest_len = CEML_HASH_DIGEST_SIZE_SHA1;
  int                 errors = 0;
  CeMLCntxHandle*     cntx = NULL;
  CEMLIovecListType   ioVecIn;
  CEMLIovecListType   ioVecOut;
  CEMLIovecType       IovecIn;
  CEMLIovecType       IovecOut;

  CETLMEMSET(result_digest, 0, sizeof(result_digest));

  //Set digest len
  if (algo == CEML_HASH_ALGO_SHA256)
  {
    digest_len = CEML_HASH_DIGEST_SIZE_SHA256;
  }

  /* Input IOVEC */
  ioVecIn.size = 1;
  ioVecIn.iov = &IovecIn;
  ioVecIn.iov[0].dwLen = msg_len;
  ioVecIn.iov[0].pvBase = msg; 

  /* Output IOVEC */
  ioVecOut.size = 1;
  ioVecOut.iov = &IovecOut;
  ioVecOut.iov[0].dwLen = digest_len;
  ioVecOut.iov[0].pvBase = result_digest;

  CeMLInit();
  
  CeMLHashInit(&cntx, algo);

  CeMLHashUpdate(cntx, ioVecIn);

  CeMLHashFinal(cntx, &ioVecOut);

  CeMLHashDeInit(&cntx);

  CeMLDeInit();

  /* Compare rsults */
  if (CETLMEMCMP(result_digest, digest, digest_len) != 0)
  {
    errors++;
  }

  return errors;
}

void CeTLHashTests( void )
{
  uint32 num = 0;
  uint32 i = 0;
  uint32 stat = 0;
  uint8  total_errors = 0;

    /* Run HASH test vectors */
  num = sizeof(ce_sha_tests_vectors) / sizeof(ce_sha_vector_type);

  /* Logger */
  CE_BOOT_LOG_MSG("CeTLHashTests, Start");

  /* Start timmer */
  CE_BOOT_LOG_MSG_START_TIMER();


  /* Run through SHA1 & SHA256 test vectors */
  for (i = 0; i < num; i++)
  {
    stat = CeTLHashCalculate(ce_sha_tests_vectors[i].algo,
                             ce_sha_tests_vectors[i].msg,
                             ce_sha_tests_vectors[i].msg_len,
                             ce_sha_tests_vectors[i].digest);
    if(stat)
    {
      total_errors++;
    }
  }

  /* End timmer */
  CE_BOOT_LOG_MSG_END("CeTLHashTests, Delta");

  if(total_errors == 0) 
  {
    CETL_MSG_HIGH("TESTID:0001 SHATEST < SHA test passed >",0,0,0);
  }
  else
  {
    CETL_MSG_ERROR("TESTID:0001 SHATEST < SHA test failed !!! Error: %d >",total_errors,0,0);
  }

}
  

