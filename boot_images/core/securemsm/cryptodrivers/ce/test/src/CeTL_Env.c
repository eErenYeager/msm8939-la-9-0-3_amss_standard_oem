/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/src/CeTL_Env.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/18/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeTL_Comdef.h"
#include "CeTL_Env.h"



/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

/* The Crypto Engine Test Library execute function */
void CeTL_Message_Logger( void  *buffer)
{
  char msg_buf[100] = "";
  uint8 msg_size = 0;
  msg_size = strlen(buffer) + 1;
  CETLMEMCPY(msg_buf, buffer, msg_size); 
  boot_log_jtag_dump_buffer(msg_buf, msg_size);
}
