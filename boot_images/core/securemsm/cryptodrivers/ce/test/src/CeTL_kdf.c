
/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


KDF based testing

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/src/CeTL_kdf.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/18/13   amen     Initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeTL_Comdef.h"
#include "CeTL.h"
#include "CeML_kdf.h"
#include <string.h>


/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

volatile uint32 key_check = 1;

void CeTLKDFTests(void)
{
  uint32 status = CETL_ERROR_FAILURE;
  uint32 i = 0;
  uint32 key_sizes[] = {16,32};
  uint8 derived_key[32], derived_key_ver[32];
  uint32 num = sizeof(key_sizes)/4;
  static char key_label[] = {"I am deriving a key"};
  static char key_context[] = {"To verify the kdf working"};
  uint8 key_input[] = {0x12,0x23,0x56,0x73,
                       0xA3,0xff,0xEf,0xAB,
                       0xCD,0x1F,0x56,0xEF,
                       0xA3,0xff,0xEf,0xAB,
                       0xCD,0x1F,0x56,0xEF,
                       0x12,0x23,0x56,0x73,
                       0xCD,0x1F,0x56,0xEF,
                       0xCD,0x1F,0x56,0xEF,
                       0x12,0x23,0x56,0x73,
                       0xCD,0x1F,0x56,0xEF,
                       0xCD,0x1F,0x56,0xEF,
                       0xA3,0xff,0xEf,0xAB};
  volatile uint8* address = (uint8*)(0xfe805040); //shared imem to communicate the derived key from SBL
																								  //to TZ, to print to logs. for testing purpose

  //expected derived key for a non fuse blown chip
  uint32 expected_derived_key[2][8] = { {0x9295C5BA, 0xD1524116, 0x25E77AC1, 0x20BE4A59,
                                         0x0       , 0x0       , 0x0       , 0x0       },
                                        {0x132540A1, 0xEA6D8712, 0x54D50521, 0x485544CB,
                                         0x8286E798, 0xD1A2E1DD, 0x52EBEB97, 0x780CDAE9},
                                      };

  //Derive off SW key
  for(i = 0; i < num; i++)
  {
    status = CeML_kdf((void*)key_input, key_sizes[i],                           
                      (void*)(&key_label), strlen(key_label),
                      (void*)(&key_context), strlen(key_context),
                      derived_key, key_sizes[i]);
    if (status != CETL_ERROR_SUCCESS) 
    {
      status = CETL_ERROR_FAILURE;
      goto err;
    }

    status = CeML_kdf((void*)key_input, key_sizes[i],                           
                      (void*)(&key_label), strlen(key_label),
                      (void*)(&key_context), strlen(key_context),
                      derived_key_ver, key_sizes[i]);
    if (status != CETL_ERROR_SUCCESS) 
    {
      status = CETL_ERROR_FAILURE;
      goto err;
    }

    //key is changing per run
    if(CETL_ERROR_SUCCESS != memcmp(derived_key,derived_key_ver,key_sizes[i]))
    {
      status = CETL_ERROR_FAILURE;
      goto err;
    }

  }


  //Derive off HW key
  for(i = 0; i < 1; i++)
  {
    status = CeML_kdf(NULL, key_sizes[i],                           
                     (void*)(&key_label), strlen(key_label),
                     (void*)(&key_context), strlen(key_context),
                     derived_key, key_sizes[i]);
    if (status != CETL_ERROR_SUCCESS) 
    {
      status = CETL_ERROR_FAILURE;
      goto err;
    }

    status = CeML_kdf(NULL, key_sizes[i],                           
                      (void*)(&key_label), strlen(key_label),
                      (void*)(&key_context), strlen(key_context),
                      derived_key_ver, key_sizes[i]);
    if (status != CETL_ERROR_SUCCESS) 
    {
      status = CETL_ERROR_FAILURE;
      goto err;
    }
    //key is changing per run
    if(CETL_ERROR_SUCCESS != memcmp(derived_key,derived_key_ver,key_sizes[i]))
    {
      status = CETL_ERROR_FAILURE;
      goto err;
    }

		*address = 0xAB;
		address++;

		{
			uint32 j = 0;
			for(j = 0; j < key_sizes[i]; j++)
			{
				*address =  derived_key[j];
			  address++;
			}
		}

    //key is different from TZ key
    if(CETL_ERROR_SUCCESS != memcmp(expected_derived_key[i], derived_key, key_sizes[i]))
    {
      status = CETL_ERROR_FAILURE;
      goto err;
    }

  }
err:
  return;
}
