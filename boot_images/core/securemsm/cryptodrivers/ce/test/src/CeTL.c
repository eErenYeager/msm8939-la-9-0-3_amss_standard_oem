/*===========================================================================

                    Crypto Engine Core Test Library API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/test/src/CeTL.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/07/11   nk      Cleanup test calls
09/18/11   nk      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeTL_Comdef.h"
#include "CeTL.h"
#include "CeTL_Hash.h"
//#include "CeTL_HashProfile.h"
//#include "CeTL_HashRegression.h"
#include "CeTL_Cipher.h"
#include "CeTL_HWKey.h"
#include "CeTL_kdf.h"
#include "CeTL_Cipher.h"


/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

/* The Crypto Engine Test Library execute function */
void CeTL_Execute( void )
{
    /* Hash Test case */
    #if HASHTEST
       CeTLHashTests();
    #endif

    /* Hash Profile Test case */
    #if HASHPROFILETEST
       CeTLHashProfileTests();
    #endif

    /* Hash Regression Test case */
    #if HASHREGRESSIONTEST
       CeTL_cev2_sha_all_tests();
    #endif

    /* AES Test case */
    #if CIPHERTEST
       CeTLAESTests();
    #endif

    /* HW Key Test case */
    #if HWKEYTEST
       CeTLHWKeyTests();
    #endif

    #if KDFTEST
       CeTLKDFTests();
    #endif

}
