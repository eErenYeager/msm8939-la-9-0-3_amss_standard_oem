/**
@file CeMl.c 
@brief Crypto Engine Module source file 
*/

/**********************************************************************
 * Copyright (c) 2009 - 2012 Qualcomm Technologies Incorporated. All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 **********************************************************************/
/*======================================================================

                        EDIT HISTORY FOR MODULE
 
 $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/shared/src/CeML.c#1 $
 $DateTime: 2015/03/19 01:58:37 $
 $Author: pwbldsvc $ 

when         who     what, where, why
--------     ---     --------------------------------------------------- 
2013-11-09   amen    Non-blocking Hash
2013-01-08   ejt     Fix klocwork warnings
2012-03-09   yk      Modification for Crypto5
2010-06-10   bm      Initial Version
=======================================================================*/

#include "boot_comdef.h"
#include "comdef.h"
#include "CeML.h"
#include "CeEL.h"
#include "CeCL.h"
#include "CeCL_Env.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include "DALSysTypes.h"
#include "DDIHWIO.h" 

/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/
#define CEML_SHA_BLOCK_SIZE        64
// Max data size that crypto engine can handle
#define CEML_MAX_BLOCK_SIZE        0xFFFFFFFC

typedef PACKED struct
{
  CeCLHashAlgoCntxType  ctx;
  uint8                 saved_buff[CEML_SHA_BLOCK_SIZE];
  uint32                saved_buff_index;
}CeMLHashAlgoCntxType;

typedef PACKED struct
{
  CeCLCipherCntxType    ctx;
}CeMLCipherAlgoCntxType;

//Define MAX cipher/Hash block size we can work with in CE/BAM environment
static uint32 ceml_max_block_size = CEML_MAX_BLOCK_SIZE;
static boolean ceel_init_done = FALSE; 

//Array to hold pointers for non-blocking hash api
#define NON_BLOCKING_POINTER_LIST_SIZE 4
typedef struct pointer_list
{
  uint32 input[NON_BLOCKING_POINTER_LIST_SIZE];
  uint32 input_size[NON_BLOCKING_POINTER_LIST_SIZE];
  uint32 pointer_location;
  uint32 counter;
  uint32 last_sec_copied;
}pointer_list;

static pointer_list non_blocking_ptr = {0};


/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeMLErrorType CeMLInit(void)
{
  CeElXferModeType xferMode;
  CeMLErrorType    ret_val = CEML_ERROR_SUCCESS;

  do
  {
    //If BAM supported change max cipher block size
    if(CECL_BAM_IS_SUPPORTED())
    {
      ceml_max_block_size = CECL_MAX_BAM_BLOCK_SIZE;
    }

    //Enable clock
    ret_val = (CeMLErrorType)CeElEnableClock();
    if (CEML_ERROR_SUCCESS != ret_val) 
    {
      break;
    } 

    //Enable BAM if needed    
    if (!ceel_init_done)   
    {
      ceel_init_done = TRUE;

      // Enable BAM
      ret_val = (CeMLErrorType) CeElInit();
      if (CEML_ERROR_SUCCESS != ret_val) 
      {
        break;
      }      
    } 
  
    //Get Xfer mode
    ret_val = (CeMLErrorType) CeElGetXferModeList(&xferMode);
    if (CEML_ERROR_SUCCESS != ret_val)
    {
      break;
    }

    //Decide which mode to use for intializating of CE config register
    ret_val = (CeMLErrorType) CeClInit((CeCLXferModeType)xferMode);
    if (CEML_ERROR_SUCCESS != ret_val)
    {
      break;
    }
  }while (0);

  return ret_val;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeMLErrorType CeMLDeInit(void)
{
  CeMLErrorType ret_val = CEML_ERROR_SUCCESS;

  /* Disable CE clock */
  ret_val = (CeMLErrorType)CeElDisableClock();

  return ret_val;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeMLErrorType CeMlHashUpdate(CeMLHashAlgoCntxType *ctx_ptr, 
                             uint8 *buff_ptr, 
                             uint32 buff_size,
                             boolean lastBlock)
{
  CeMLErrorType        ret_val = CEML_ERROR_SUCCESS;
  uint32               pdwActualOut = 0;
  uint32               tmp_size = 0;
  CeElXferModeType     xferMode;
  CeElXferFunctionType xferFunc;
  CEELIovecListType    inIoVec;
  CEELIovecListType    outIoVec;
  CEELIovecType        inIoVecT;
  CEELIovecType        outIoVecT;

  /* Sanity check inputs */
  if (!ctx_ptr || !buff_ptr)
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  if(lastBlock == TRUE)
  {
    ctx_ptr->ctx.lastBlock = TRUE;
  }
  else
  {
    ctx_ptr->ctx.lastBlock = FALSE;
  }

  do
  {
    ctx_ptr->ctx.dataLn = buff_size;
    ret_val = (CeMLErrorType) CeClIOCtlHash(CECL_IOCTL_SET_HASH_CNTXT, 
              (uint8 *)(&(ctx_ptr->ctx)), 
              sizeof(CeCLHashAlgoCntxType), 
              NULL, 
              0, 
              &pdwActualOut);
    if (CEML_ERROR_SUCCESS != ret_val) 
    {
       break;
    }
  
    tmp_size = buff_size;
    if (buff_size % 16)
    {
      tmp_size = buff_size + (16 - (buff_size % 16));  
    }
  
    inIoVec.size = 1;
    inIoVec.iov = &inIoVecT;
    inIoVec.iov->pvBase = buff_ptr;
    inIoVec.iov->dwLen = tmp_size;

    outIoVec.size = 1;
    outIoVec.iov = &outIoVecT;
    outIoVec.iov->pvBase = (void *) ctx_ptr->ctx.auth_iv;
    if(ctx_ptr->ctx.algo == CECL_HASH_ALGO_SHA1)
    {
      outIoVec.iov->dwLen = CECL_HASH_SHA_IV_LEN;
    }
    else if (ctx_ptr->ctx.algo == CECL_HASH_ALGO_SHA256)
    {
      outIoVec.iov->dwLen = CECL_HASH_SHA256_IV_LEN;
    }
  
    //Get Xfer mode
    ret_val = (CeMLErrorType) CeElGetXferModeList(&xferMode);
    if (CEML_ERROR_SUCCESS != ret_val) 
    {
       break;
    }
  
    //Decide how to set function pointer
    ret_val = (CeMLErrorType) CeElGetXferfunction(xferMode, (CeElXferFunctionType*)&xferFunc);
    if (CEML_ERROR_SUCCESS != ret_val) 
    {
       break;
    }

    //Execute function pointer
    ret_val = (CeMLErrorType) xferFunc(&inIoVec, &outIoVec, lastBlock, (uint8 *)(&(ctx_ptr->ctx)), CEEL_DATA_HASH); 
    if (CEML_ERROR_SUCCESS != ret_val) 
    {
       break;
    }
  
    //Read HASH context
    pdwActualOut = sizeof(CeCLHashAlgoCntxType);
    if(ctx_ptr->ctx.auth_nonblock_mode == 0 || ctx_ptr->ctx.lastBlock == 1)
    {
      ret_val = (CeMLErrorType) CeClIOCtlHash(CECL_IOCTL_GET_HASH_CNTXT, 
                                              NULL, 
                                              0, 
                                              (uint8 *)(&(ctx_ptr->ctx)), 
                                              pdwActualOut, 
                                              &pdwActualOut);
      if (CEML_ERROR_SUCCESS != ret_val) 
      {
         break;
      }
    }
  } while(0);

  return ret_val;
}                                                                         

/**
 * @brief 
 *
 * @param
 *
 * @return 
 *
 * @see 
 *
 */                     
CeMLErrorType CeMLHashInit(CeMLCntxHandle** ceMlHandle, CeMLHashAlgoType pAlgo)
{
  CeMLErrorType ret_val = CEML_ERROR_SUCCESS;

  /* Sanity check inputs */
  if ((CEML_HASH_ALGO_SHA1 != pAlgo) && (CEML_HASH_ALGO_SHA256 != pAlgo))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  *ceMlHandle = NULL;

  /* Allocate memory and check for errors */
  CeElmalloc((void**) &(*ceMlHandle), sizeof(CeMLCntxHandle));
  if(!*ceMlHandle)
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  (*ceMlHandle)->pClientCtxt = NULL;

  /* Allocate memory and check for errors */
  CeElmalloc(&((*ceMlHandle)->pClientCtxt), sizeof(CeMLHashAlgoCntxType));
  if (!(*ceMlHandle)->pClientCtxt)
  {
    CeElfree((*ceMlHandle));
    return CEML_ERROR_INVALID_PARAM; 
  }

  //Clear HASH context
  CeElMemset(((*ceMlHandle)->pClientCtxt), 0, sizeof(CeMLHashAlgoCntxType));

  /* Sanity check inputs */
  if (CEML_HASH_ALGO_SHA1 != pAlgo && CEML_HASH_ALGO_SHA256 != pAlgo)
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  do 
  {
    CeMLHashAlgoCntxType* hashCntx = (CeMLHashAlgoCntxType*) ((*ceMlHandle)->pClientCtxt);

    hashCntx->ctx.auth_bytecnt[0] = 0;
    hashCntx->ctx.auth_bytecnt[1] = 0;

    hashCntx->saved_buff_index = 0;
    CeElMemset((void*)hashCntx, 0, sizeof(CeMLHashAlgoCntxType));

    hashCntx->ctx.auth_nonblock_mode = 0;
    hashCntx->ctx.auth_no_context = 0;
    non_blocking_ptr.last_sec_copied = 0;

    hashCntx->ctx.firstBlock = 1; 

    CeElMemset((void*)&non_blocking_ptr, 0, sizeof(non_blocking_ptr));

    /* load initial IV */
    if(CEML_HASH_ALGO_SHA1 == pAlgo) 
    {
       hashCntx->ctx.algo = CECL_HASH_ALGO_SHA1;
       /* standard initialization vector for SHA-1, source: FIPS 180-2 */
       /* changed to little-endian format */
       hashCntx->ctx.auth_iv[0] = 0x01234567;
       hashCntx->ctx.auth_iv[1] = 0x89ABCDEF;
       hashCntx->ctx.auth_iv[2] = 0xFEDCBA98;
       hashCntx->ctx.auth_iv[3] = 0x76543210;
       hashCntx->ctx.auth_iv[4] = 0xF0E1D2C3;

    }
    else if(CEML_HASH_ALGO_SHA256 == pAlgo) 
    {
       hashCntx->ctx.algo = CECL_HASH_ALGO_SHA256;
       /* standard initialization vector for SHA-256, source: FIPS 180-2 */
       /* changed to little-endian format */
       hashCntx->ctx.auth_iv[0] = 0x67E6096A;
       hashCntx->ctx.auth_iv[1] = 0x85AE67BB;
       hashCntx->ctx.auth_iv[2] = 0x72F36E3C;
       hashCntx->ctx.auth_iv[3] = 0x3AF54FA5;
       hashCntx->ctx.auth_iv[4] = 0x7F520E51;
       hashCntx->ctx.auth_iv[5] = 0x8C68059B;
       hashCntx->ctx.auth_iv[6] = 0xABD9831F;
       hashCntx->ctx.auth_iv[7] = 0x19CDE05B;
    }
    /* Initialise the mode to HASH mode. */
    //hashCntx->ctx.mode = CECL_HASH_MODE_HASH;
  } while(0);

 return ret_val;
}

/**
 * @brief 
 *
 * @param
 *
 * @return 
 *
 * @see 
 *
 */                     
CeMLErrorType CeMLHashDeInit(CeMLCntxHandle** ceMlHandle)
{
  CeMLErrorType ret_val = CEML_ERROR_SUCCESS;

  if ((!ceMlHandle) || (!*ceMlHandle))
  {  
    return CEML_ERROR_INVALID_PARAM;
  }

  CeElfree((*ceMlHandle)->pClientCtxt);
  (*ceMlHandle)->pClientCtxt = NULL;
  CeElfree(*ceMlHandle);
  *ceMlHandle = NULL;

  return ret_val;
}

/**
 * @brief 
 *
 * @param 
 *
 * @return 
 *
 * @see  
 *
 */
CeMLErrorType CeMLHashUpdate(CeMLCntxHandle* ceMlHandle, 
                             CEMLIovecListType ioVecIn)
{
   CeMLHashAlgoCntxType* hashCntx;   
   volatile uint32 ce_status;
   uint32 total_data_len = 0;
   uint32 bytes_remaining = 0;
   uint32 bytes_to_write = 0;
   uint8* pData = NULL;
   uint32 nDataLen = 0;
   uint32 bytes_filled_into_saved_buff = 0;
   CeMLErrorType ret_val = CEML_ERROR_SUCCESS;
   uint32 temp_address = NULL;
   uint32 temp_len = 0;
   uint32 temp_counter;

   /* Sanity check inputs */
   if ((!ceMlHandle) || (!ceMlHandle->pClientCtxt))
   {
     return CEML_ERROR_INVALID_PARAM;
   }

   if ((ioVecIn.size!= 1) || (!ioVecIn.iov))
   {
     return CEML_ERROR_INVALID_PARAM;
   }

   pData = (uint8*) ioVecIn.iov[0].pvBase;
   nDataLen = ioVecIn.iov[0].dwLen;

   //Set up context pointer
   hashCntx = (CeMLHashAlgoCntxType*) (ceMlHandle->pClientCtxt);

   if(hashCntx->ctx.auth_nonblock_mode == 1)
   {
     hashCntx->ctx.auth_no_context = 1;

     if(non_blocking_ptr.counter >= NON_BLOCKING_POINTER_LIST_SIZE)
     {
       while(1)
       {
         ce_status = HWIO_IN(CECL_CE_STATUS);
         CeElMemoryBarrier();
         if(ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK)
         {
           break;
         }
       }

       hashCntx->ctx.auth_nonblock_mode = 0;

       pData = (uint8*)non_blocking_ptr.input[non_blocking_ptr.pointer_location];
       nDataLen = non_blocking_ptr.input_size[non_blocking_ptr.pointer_location];

       non_blocking_ptr.input[non_blocking_ptr.pointer_location] = 
         (uint32)ioVecIn.iov[0].pvBase;

       non_blocking_ptr.input_size[non_blocking_ptr.pointer_location] = 
         ioVecIn.iov[0].dwLen;

       non_blocking_ptr.pointer_location = 
         (non_blocking_ptr.pointer_location+1)%(NON_BLOCKING_POINTER_LIST_SIZE);
     }
     else
   {
       if(non_blocking_ptr.counter != 0)
       {
         temp_address = 
           non_blocking_ptr.input[non_blocking_ptr.pointer_location];

         temp_len = 
           non_blocking_ptr.input_size[non_blocking_ptr.pointer_location];
       }
       else
       {
         non_blocking_ptr.pointer_location = 0;
       }

       temp_counter = 
         (non_blocking_ptr.pointer_location+non_blocking_ptr.counter)%
         (NON_BLOCKING_POINTER_LIST_SIZE);

       non_blocking_ptr.input[temp_counter] = (uint32)pData;

       non_blocking_ptr.input_size[temp_counter] = nDataLen;

       if(non_blocking_ptr.counter != 0)
       {
         pData = (uint8*)temp_address;
         nDataLen = temp_len;
       }
       non_blocking_ptr.counter++;
     }
   }

   do 
   {
      total_data_len = hashCntx->saved_buff_index + nDataLen;

      //ensure there was no integer overflow for total_data_len
      if (total_data_len < nDataLen)
      {
         ret_val = CEML_ERROR_FAILURE;
         break;
      }

      if((total_data_len <= CEML_SHA_BLOCK_SIZE) && (non_blocking_ptr.last_sec_copied == 0)) 
      {
         /* not enough bytes for a full block */
         ret_val = (CeMLErrorType) CeElMemcpy(&hashCntx->saved_buff[hashCntx->saved_buff_index], 
                                              pData, nDataLen );
         if (CEML_ERROR_SUCCESS != ret_val) 
         {
            break;
         }

         hashCntx->saved_buff_index += nDataLen;

        if(hashCntx->ctx.auth_nonblock_mode == 1)
        {
          non_blocking_ptr.pointer_location = 
            (non_blocking_ptr.pointer_location+1)%(NON_BLOCKING_POINTER_LIST_SIZE);
          non_blocking_ptr.counter--;
        }
         break;
      }
      else 
      {
          if(non_blocking_ptr.last_sec_copied == 0) 
          {
            // fill in the saved_buff with the data and then CeMlHashUpdate 
            // and then save remaining data from pData into saved_buff
            bytes_filled_into_saved_buff =  CEML_SHA_BLOCK_SIZE - hashCntx->saved_buff_index;

           //integer underflow check for bytes_filled_into_saved_buff
           //Added check for Klocwork
           if (hashCntx->saved_buff_index > CEML_SHA_BLOCK_SIZE ||
               bytes_filled_into_saved_buff > CEML_SHA_BLOCK_SIZE)
           {
             ret_val = CEML_ERROR_FAILURE;
             break;
           }
  
            if(hashCntx->ctx.auth_nonblock_mode == 1)
            {
              ce_status = HWIO_IN(CECL_CE_STATUS);
              CeElMemoryBarrier();
              if(((ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK) != 0x2) && 
                 (hashCntx->ctx.firstBlock == 0))
              {
                return CEML_ERROR_SUCCESS;
              }
            }

           if (hashCntx->saved_buff_index < CEML_SHA_BLOCK_SIZE)
           {
             ret_val = (CeMLErrorType) CeElMemcpy(&hashCntx->saved_buff[hashCntx->saved_buff_index], 
                                                  pData, bytes_filled_into_saved_buff);
             if (CEML_ERROR_SUCCESS != ret_val) 
             {
                break;
             }
           }

           hashCntx->saved_buff_index = CEML_SHA_BLOCK_SIZE;
           ret_val = CeMlHashUpdate(hashCntx, hashCntx->saved_buff, 
                                    hashCntx->saved_buff_index, FALSE);
           if (CEML_ERROR_SUCCESS != ret_val) 
           {
              break;
           }

            if(hashCntx->ctx.auth_nonblock_mode == 1)
            {
              non_blocking_ptr.input[non_blocking_ptr.pointer_location] = 
                (uint32)pData+bytes_filled_into_saved_buff;

              non_blocking_ptr.input_size[non_blocking_ptr.pointer_location] = 
                nDataLen - bytes_filled_into_saved_buff;

            }

           //integer underflow check for bytes_to_write and bytes_remaining
           if (bytes_filled_into_saved_buff > nDataLen)
           {
             ret_val = CEML_ERROR_FAILURE;
             break;
           }
         }
         else
         {
           bytes_filled_into_saved_buff = 0;
         }

         /* nothing saved - just send the new data */
         bytes_to_write  = (nDataLen - bytes_filled_into_saved_buff)/CEML_SHA_BLOCK_SIZE;
         bytes_to_write *= CEML_SHA_BLOCK_SIZE;
         bytes_remaining = (nDataLen - bytes_filled_into_saved_buff)%CEML_SHA_BLOCK_SIZE;

         if(non_blocking_ptr.last_sec_copied == 0) 
         {
           /* even if we have full blocks, we need the final 64-bytes
            * processed with CE_Hash_Final() */
           if(0 == bytes_remaining)
           {
              bytes_remaining = CEML_SHA_BLOCK_SIZE;
              bytes_to_write -= CEML_SHA_BLOCK_SIZE;
           }
  
           ret_val = (CeMLErrorType) CeElMemcpy(&hashCntx->saved_buff[0], &pData[(nDataLen - bytes_remaining)], 
                                                bytes_remaining);
           if (CEML_ERROR_SUCCESS != ret_val) 
           {
              break;
           }

           hashCntx->saved_buff_index = bytes_remaining;
  
           if(hashCntx->ctx.auth_nonblock_mode == 1)
           {
             non_blocking_ptr.last_sec_copied = 1;

             non_blocking_ptr.input[non_blocking_ptr.pointer_location] = 
               (uint32)pData + bytes_filled_into_saved_buff;

             non_blocking_ptr.input_size[non_blocking_ptr.pointer_location] = 
               bytes_to_write;
           }
         }

         if(hashCntx->ctx.auth_nonblock_mode == 1)
         {
           ce_status = HWIO_IN(CECL_CE_STATUS);
           CeElMemoryBarrier();
           if((ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK) != 0x2)
           {
             return CEML_ERROR_SUCCESS;
           }
         }

         if(bytes_to_write)
         {
            /* Crypto Engine can handle maximum of 4G in one shot, so if
             * the data is more than 4G then split them into less
             * than 4G chunks and the process them
             */
            uint8* data_ptr = pData + bytes_filled_into_saved_buff;
            uint32 bytes_pending = bytes_to_write;
            do
            {
              if(bytes_pending > ceml_max_block_size)
              {
                ret_val = CeMlHashUpdate(hashCntx, data_ptr, ceml_max_block_size, FALSE);
                if (CEML_ERROR_SUCCESS != ret_val) 
                {
                   break;
                }

                bytes_pending -= ceml_max_block_size;
                data_ptr += ceml_max_block_size;

                if(hashCntx->ctx.auth_nonblock_mode == 1)
                {
                  non_blocking_ptr.input[non_blocking_ptr.pointer_location] = 
                    (uint32)data_ptr;

                  non_blocking_ptr.input_size[non_blocking_ptr.pointer_location] = 
                    bytes_pending;

                  ce_status = HWIO_IN(CECL_CE_STATUS);
                  CeElMemoryBarrier();
                  if((ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK) != 0x2)
                  {
                    return CEML_ERROR_SUCCESS;
                  }
                }
              }
              else
              {
                ret_val = CeMlHashUpdate(hashCntx, data_ptr, bytes_pending, FALSE);
                if (CEML_ERROR_SUCCESS != ret_val) 
                {
                   break;
                }

                bytes_pending = 0;
              }
            }while (bytes_pending > 0);
         }

         if(hashCntx->ctx.auth_nonblock_mode == 1)
         {
           non_blocking_ptr.counter--;
           non_blocking_ptr.pointer_location = 
             (non_blocking_ptr.pointer_location+1)%(NON_BLOCKING_POINTER_LIST_SIZE);
           non_blocking_ptr.last_sec_copied = 0;
         }
      }
   } while(0);


   if(hashCntx->ctx.auth_no_context == 1)
   {
     hashCntx->ctx.auth_nonblock_mode = 1;
   }

   return ret_val;
}

/**
 * @brief 
 *
 * @param 
 * 
  @return 
 *
 * @see 
 *
 */
CeMLErrorType CeMLHashFinal(CeMLCntxHandle* ceMlHandle, 
                            CEMLIovecListType *ioVecOut)
{
  CeMLErrorType         ret_val = CEML_ERROR_SUCCESS;    
  uint32                hashLen = 0;
  CEMLIovecListType     iovec;
  CEMLIovecType         iovec_buf;
  CeMLHashAlgoCntxType* hashCntx = NULL; 
  uint32                i = 0;
  volatile uint32       ce_status;

  iovec.size  = 1;
  iovec.iov   = &iovec_buf;

  /* Sanity check inputs */
  if ((!ceMlHandle) || (!ceMlHandle->pClientCtxt))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  if ((!ioVecOut) || (ioVecOut->size!= 1) || (!ioVecOut->iov))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  hashCntx = (CeMLHashAlgoCntxType*) (ceMlHandle->pClientCtxt);

  if(hashCntx->ctx.algo == CECL_HASH_ALGO_SHA1)
  {
    hashLen = CEML_HASH_DIGEST_SIZE_SHA1;
  }
  else if (hashCntx->ctx.algo == CECL_HASH_ALGO_SHA256)
  {
    hashLen = CEML_HASH_DIGEST_SIZE_SHA256;
  }
  else
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  if (ioVecOut->iov[0].dwLen < hashLen)
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  if(hashCntx->ctx.auth_nonblock_mode == 1)
  {
    while(1)
    {
      ce_status = HWIO_IN(CECL_CE_STATUS);
      CeElMemoryBarrier();
      if(ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK)
      {
        break;
      }
    }
    for(i = 0; i < non_blocking_ptr.counter; i++)
    {
      hashCntx->ctx.auth_nonblock_mode = 0;
      hashCntx->ctx.auth_no_context = 1;
      iovec.iov[0].pvBase = (void *)non_blocking_ptr.input[non_blocking_ptr.pointer_location];
      iovec.iov[0].dwLen = non_blocking_ptr.input_size[non_blocking_ptr.pointer_location];
      if(CEML_ERROR_SUCCESS != CeMLHashUpdate(ceMlHandle, iovec))
      {
        return CEML_ERROR_FAILURE;
      }
      non_blocking_ptr.pointer_location = 
        (non_blocking_ptr.pointer_location++)%(NON_BLOCKING_POINTER_LIST_SIZE);
    }
    non_blocking_ptr.counter = 0;
    hashCntx->ctx.auth_nonblock_mode = 0;
    hashCntx->ctx.auth_no_context = 1;
  }

  do 
  {    
    if(hashCntx->saved_buff_index > 0) 
    {
      //check for integer underflow for memset operation
      if (hashCntx->saved_buff_index > CEML_SHA_BLOCK_SIZE)
      {
        ret_val = CEML_ERROR_FAILURE;
        break;
      }

      /* we only have the saved buffer left */          
      if (hashCntx->saved_buff_index < CEML_SHA_BLOCK_SIZE)
      {
        ret_val = (CeMLErrorType) CeElMemset(hashCntx->saved_buff+ (hashCntx->saved_buff_index), 0, 
                                            (CEML_SHA_BLOCK_SIZE - hashCntx->saved_buff_index)); 
        if (CEML_ERROR_SUCCESS != ret_val) 
        {
           break;
        }
      }

      ret_val = CeMlHashUpdate(hashCntx, 
                              hashCntx->saved_buff,
                              hashCntx->saved_buff_index, 
                              TRUE);
      if (CEML_ERROR_SUCCESS != ret_val) 
      {
         break;
      }
    }

    //copy the hash result into ioVecOut
    ret_val = (CeMLErrorType) CeElMemcpy(ioVecOut->iov[0].pvBase,  (void *) hashCntx->ctx.auth_iv, hashLen);
    if (CEML_ERROR_SUCCESS != ret_val) 
    {
       break;
    }

    ioVecOut->size = 1;
    ioVecOut->iov[0].dwLen = hashLen;

  } while(0);

 return ret_val;
}  

/**
 * @brief 
 *
 * @param 
 *
 * @return 
 *
 * @see
 *
 */
CeMLErrorType CeMLHashAtomic(CeMLCntxHandle*    ceMlHandle, 
                             CEMLIovecListType  ioVecIn,
                             CEMLIovecListType* ioVecOut)
{
  CeMLErrorType         ret_val = CEML_ERROR_SUCCESS;    
  uint32                hashLen = 0;
  CeMLHashAlgoCntxType* hashCntx = NULL; 
  uint8*                data_ptr = NULL;
  uint32                bytes_pending = 0;

  /* Sanity check inputs */
  if ((!ceMlHandle) || (!ceMlHandle->pClientCtxt) || (!ioVecOut))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  if ( ((ioVecOut->size!= 1) || (!ioVecOut->iov)) 
       || ((ioVecIn.size!= 1) || (!ioVecIn.iov)))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  hashCntx = (CeMLHashAlgoCntxType*) (ceMlHandle->pClientCtxt);

  if(hashCntx->ctx.algo == CECL_HASH_ALGO_SHA1)
  {
    hashLen = CEML_HASH_DIGEST_SIZE_SHA1;
  }
  else if (hashCntx->ctx.algo == CECL_HASH_ALGO_SHA256)
  {
    hashLen = CEML_HASH_DIGEST_SIZE_SHA256;
  }
  else
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  if (ioVecOut->iov[0].dwLen < hashLen)
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  data_ptr      = (uint8*) (ioVecIn.iov[0].pvBase);
  bytes_pending = ioVecIn.iov[0].dwLen;

  do 
  {
    do
    {
      if(bytes_pending > ceml_max_block_size)
      {
        ret_val = CeMlHashUpdate(hashCntx, data_ptr, ceml_max_block_size, FALSE);
        if (CEML_ERROR_SUCCESS != ret_val) 
        {
           break;
        }

        bytes_pending -= ceml_max_block_size;
        data_ptr += ceml_max_block_size;
      }
      else
      {
        ret_val = CeMlHashUpdate(hashCntx, data_ptr, bytes_pending, TRUE);
        if (CEML_ERROR_SUCCESS != ret_val) 
        {
           break;
        }

        bytes_pending = 0;
      }
    }while (bytes_pending > 0);

    //copy the hash result into ioVecOut
    ret_val = (CeMLErrorType) CeElMemcpy(ioVecOut->iov[0].pvBase,  (void *) hashCntx->ctx.auth_iv, hashLen);
    if (CEML_ERROR_SUCCESS != ret_val) 
    {
       break;
    }

    ioVecOut->size = 1;
    ioVecOut->iov[0].dwLen = hashLen;
  } while(0); 

  return ret_val;
}


/**
 * @brief This function will create a Hmac message digest using 
 *        the algorithm specified.
 *
 * @param hmac_key      [in]  Pointer to key 
 * @param hmac_key_len  [in]  Length of input key in bytes
 * @param ioVecIn       [in]  Pointer to input data to hash
 * @param ioVecOut      [out] Pointer to output data 
 * @param palgo         [in]  Algorithm type
 *
 * @return CeMLErrorType
 *
 * @see 
 *
 */
CeMLErrorType CeMLHmac(uint8              *key_ptr, 
                       uint32              keylen, 
                       CEMLIovecListType   ioVecIn,
                       CEMLIovecListType  *ioVecOut,
                       CeMLHashAlgoType    pAlgo)
{
  CeMLCntxHandle*   cntx = NULL;
  CeMLErrorType     ret = CEML_ERROR_SUCCESS;    
  uint8             ipad[CEML_HASH_DIGEST_BLOCK_SIZE];
  uint8             opad[CEML_HASH_DIGEST_BLOCK_SIZE];
  uint8             key[CEML_HASH_DIGEST_BLOCK_SIZE];
  uint32            key_len = CEML_HASH_DIGEST_BLOCK_SIZE;
  uint32            hmac[CEML_HASH_DIGEST_SIZE_SHA256];
  uint32            i = 0;
  CEMLIovecListType ioVecInTmp;
  CEMLIovecListType ioVecOutTmp;
  CEMLIovecType     IovecInTmp;
  CEMLIovecType     IovecOutTmp;
  uint32            hash_size = 0;

  if ((pAlgo != CEML_HASH_ALGO_SHA256) && (pAlgo != CEML_HASH_ALGO_SHA1))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  if ((key_ptr == NULL) || (keylen == 0) ||
      (ioVecIn.size != 1)  || ( ioVecIn.iov[0].pvBase == NULL) || ( ioVecIn.iov[0].dwLen == 0) ||
      (ioVecOut == NULL) || ( ioVecOut->size != 1) || 
      (ioVecOut->iov[0].pvBase == NULL))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  if (((pAlgo == CEML_HASH_ALGO_SHA256) &&
       (ioVecOut->iov[0].dwLen < CEML_HASH_DIGEST_SIZE_SHA256)) ||
       ((pAlgo == CEML_HASH_ALGO_SHA1) &&
        (ioVecOut->iov[0].dwLen < CEML_HASH_DIGEST_SIZE_SHA1)) )
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  ioVecInTmp.size = 1;
  ioVecInTmp.iov = &IovecInTmp;

  ioVecOutTmp.size = 1;
  ioVecOutTmp.iov = &IovecOutTmp; 

  /* Update the hash size */
  switch(pAlgo)
  {
    case CEML_HASH_ALGO_SHA256:
      hash_size = CEML_HASH_DIGEST_SIZE_SHA256;
      break;
    case CEML_HASH_ALGO_SHA1:
      hash_size = CEML_HASH_DIGEST_SIZE_SHA1;
      break;
    default:
      /* Validation on pAlgo has already been done */
      break;
  }

  if (keylen > CEML_HASH_DIGEST_BLOCK_SIZE)
  {

    ret = CeMLHashInit(&cntx, pAlgo);

    if (ret != CEML_ERROR_SUCCESS)
    {
      CeMLHashDeInit (&cntx);
      return ret;
    }

    ioVecInTmp.iov[0].pvBase = key_ptr;
    ioVecInTmp.iov[0].dwLen  = keylen;

    ioVecOutTmp.iov[0].pvBase = key;
    ioVecOutTmp.iov[0].dwLen  = hash_size;

    ret = CeMLHashUpdate(cntx, ioVecInTmp); 
    if (ret != CEML_ERROR_SUCCESS)
    {
      CeMLHashDeInit (&cntx);
      return ret;
    }

    ret = CeMLHashFinal(cntx, &ioVecOutTmp);
    if (ret != CEML_ERROR_SUCCESS)
    {
      CeMLHashDeInit (&cntx);
      return ret;
    }

    CeMLHashDeInit(&cntx);

    CeElMemset(key+hash_size, 0, (key_len-hash_size));

  }
  else
  {
    CeElMemcpy(key, key_ptr, keylen);
    //Added check for Klocwork
    if(keylen < CEML_HASH_DIGEST_BLOCK_SIZE && (key_len-keylen) <= CEML_HASH_DIGEST_BLOCK_SIZE)
      CeElMemset(key+keylen, 0, (key_len-keylen));
  }

  CeElMemset(ipad, 0, CEML_HASH_DIGEST_BLOCK_SIZE);
  CeElMemset(opad, 0, CEML_HASH_DIGEST_BLOCK_SIZE);
  CeElMemcpy(ipad, key, key_len);
  CeElMemcpy(opad, key, key_len);

  for (i=0; i < CEML_HASH_DIGEST_BLOCK_SIZE; i++) 
  {
    ipad[i] ^= 0x36;
    opad[i] ^= 0x5C;
  }

  /* Inner Hash */
  ret = CeMLHashInit(&cntx, pAlgo);
  if (ret != CEML_ERROR_SUCCESS)
  {
    return ret;
  }

  ioVecInTmp.iov[0].pvBase = ipad;
  ioVecInTmp.iov[0].dwLen  = CEML_HASH_DIGEST_BLOCK_SIZE;

  ioVecOutTmp.iov[0].pvBase = hmac;
  ioVecOutTmp.iov[0].dwLen  = hash_size;

  ret = CeMLHashUpdate(cntx, ioVecInTmp); 
  if (ret != CEML_ERROR_SUCCESS)
  {
    CeMLHashDeInit(&cntx);
    return ret;
  }
 
  ioVecInTmp.iov[0].pvBase = ioVecIn.iov[0].pvBase;
  ioVecInTmp.iov[0].dwLen  = ioVecIn.iov[0].dwLen;
  ret = CeMLHashUpdate(cntx, ioVecInTmp);
  if (ret != CEML_ERROR_SUCCESS)
  {
      CeMLHashDeInit(&cntx);
    return ret;
  }

  ret = CeMLHashFinal(cntx, &ioVecOutTmp);
  if (ret != CEML_ERROR_SUCCESS)
  {
    CeMLHashDeInit(&cntx);
    return ret;
  }

  CeMLHashDeInit(&cntx);

  /* Outer Hash */
  ret = CeMLHashInit(&cntx, pAlgo);
  if (ret != CEML_ERROR_SUCCESS)
  {
    return ret;
  }

  ioVecInTmp.iov[0].pvBase = opad;
  ioVecInTmp.iov[0].dwLen  = CEML_HASH_DIGEST_BLOCK_SIZE;
  ret = CeMLHashUpdate(cntx, ioVecInTmp);
  if (ret != CEML_ERROR_SUCCESS)
  {
    CeMLHashDeInit(&cntx);
    return ret;
  }

  ioVecInTmp.iov[0].pvBase = hmac;
  ioVecInTmp.iov[0].dwLen  = hash_size;
  ret = CeMLHashUpdate(cntx, ioVecInTmp);
  if (ret != CEML_ERROR_SUCCESS)
  {
    CeMLHashDeInit(&cntx);
    return ret;
  }

  ret = CeMLHashFinal(cntx, ioVecOut);
  if (ret != CEML_ERROR_SUCCESS)
  {
    CeMLHashDeInit(&cntx);
    return ret;
  }

  CeMLHashDeInit(&cntx);

  return ret;
} 

/**
 * @brief This functions sets the Hash paramaters - Mode and Key for HMAC
 *
 * @param ceMlHandle [in] Pointer to cipher context handle
 * @param nParamID   [in] Cipher parameter id to set
 * @param pParam     [in] Pointer to parameter data 
 * @param cParam     [in] Size of parameter data in bytes
 * @param palgo      [in] Algorithm type
 *
 * @return CeMLErrorType
 *
 */ 
CeMLErrorType CeMLHashSetParam (CeMLCntxHandle     *ceMlHandle,
                                CeMLHashParamType  nParamID, 
                                const void         *pParam, 
                                uint32             cParam,
                                CeMLHashAlgoType   pAlgo)
{
  CeMLErrorType     retVal = CEML_ERROR_SUCCESS;
  CeMLCntxHandle*   cntx = NULL;
  CEMLIovecListType ioVecInTmp;
  CEMLIovecListType ioVecOutTmp;
  CEMLIovecType     IovecInTmp;
  CEMLIovecType     IovecOutTmp;
  uint32            hash_size = 0;
  uint8             key[CEML_HASH_DIGEST_BLOCK_SIZE];
  uint32            key_len = CEML_HASH_DIGEST_BLOCK_SIZE;


  if (!pParam)
  {
    return CEML_ERROR_INVALID_PARAM;
  } 

  /* Update the hash size */
  switch(pAlgo)
  {
    case CEML_HASH_ALGO_SHA256:
      hash_size = CEML_HASH_DIGEST_SIZE_SHA256;
      break;
    case CEML_HASH_ALGO_SHA1:
      hash_size = CEML_HASH_DIGEST_SIZE_SHA1;
      break;
    default:
      return CEML_ERROR_FAILURE;
  }

  switch (nParamID)
  {
    case CEML_HASH_PARAM_MODE:
      if (sizeof(CeMLHashModeType) != cParam)
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }
      ((CeMLHashAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.mode = 
        (CeCLHashModeType)(*((CeMLHashModeType*)pParam));
      break;

    case CEML_HASH_PARAM_HMAC_KEY:
      if (((CeMLHashAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.mode == CEML_HASH_MODE_HMAC)
      {
        /* If the Key length is not 64, truncate or extend the key as per FIPS 198-1 */
        if (cParam > CEML_HASH_DIGEST_BLOCK_SIZE)
        {
          ioVecInTmp.size = 1;
          ioVecInTmp.iov = &IovecInTmp;

          ioVecOutTmp.size = 1;
          ioVecOutTmp.iov = &IovecOutTmp; 

          retVal = CeMLHashInit(&cntx, pAlgo);
          if (retVal != CEML_ERROR_SUCCESS)
          {
            CeMLHashDeInit(&cntx);
            break;
          }

          ioVecInTmp.iov[0].pvBase = (void*) pParam;
          ioVecInTmp.iov[0].dwLen  = cParam;
          ioVecOutTmp.iov[0].pvBase = key;
          ioVecOutTmp.iov[0].dwLen  = hash_size;

          retVal = CeMLHashUpdate(cntx, ioVecInTmp); 
          if (retVal != CEML_ERROR_SUCCESS)
          {
            break;
          }

          retVal = CeMLHashFinal(cntx, &ioVecOutTmp);
          if (retVal != CEML_ERROR_SUCCESS)
          {
            break;
          }

          CeMLHashDeInit(&cntx);

          CeElMemset(key+hash_size, 0, (key_len-hash_size));
        }
        else
        {
          CeElMemcpy(key, (void *)pParam, cParam);
          //Added check for Klocwork
          if(cParam < CEML_HASH_DIGEST_BLOCK_SIZE && (key_len-cParam) <= CEML_HASH_DIGEST_BLOCK_SIZE)
            CeElMemset(key+cParam, 0, (key_len-cParam));
        }
          CeElMemcpy(((CeMLHashAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.hmac_key,
                       (void*) key, key_len);

      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;        
      }
      break;

     case CEML_HASH_NONBLOCK_MODE:
        if(!pParam)
        {
          return CEML_ERROR_INVALID_PARAM;
        }

        //Check key size 
       if (cParam == 0 || cParam > sizeof(uint8))
       {
         return CEML_ERROR_INVALID_PARAM;
       }

       if(!CECL_NON_BLOCKING_HASH_IS_SUPPORTED())
       {
         return CEML_ERROR_NOT_SUPPORTED;
       }
       else
       {
         //enable/disable non blocking mode
         ((CeMLHashAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.auth_nonblock_mode = (uint8*)pParam;
       }

       break;


    default:
      retVal = CEML_ERROR_INVALID_PARAM;
      break;
  }

  return retVal;

} /* CeMLHashSetParam() */  

/**
 * @brief 
 *
 * @param 
 * 
  @return 
 *
 * @see 
 *
 */
CeMLErrorType CeMLCipherInit (CeMLCntxHandle     ** ceMlHandle, 
                              CeMLCipherAlgType  pAlgo)
{
  /* Sanity check inputs */
  if ((CEML_CIPHER_ALG_AES128 != pAlgo) && (CEML_CIPHER_ALG_AES256 != pAlgo)) 
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  *ceMlHandle = NULL;

  /* Allocate memory and check for error */
  CeElmalloc((void**) &(*ceMlHandle), sizeof(CeMLCntxHandle));
  if(!*ceMlHandle)
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  (*ceMlHandle)->pClientCtxt = NULL;

  /* Allocate memory and check for error */
  CeElmalloc(&((*ceMlHandle)->pClientCtxt), sizeof(CeMLCipherAlgoCntxType));
  if (!(*ceMlHandle)->pClientCtxt)
  {
    CeElfree(*ceMlHandle);
    return CEML_ERROR_INVALID_PARAM;
  }

  ((CeMLCipherAlgoCntxType*)((*ceMlHandle)->pClientCtxt))->ctx.algo = 
    (CeCLCipherAlgType) pAlgo;

  return CEML_ERROR_SUCCESS;
}

/**
 * @brief 
 *
 * @param 
 * 
  @return 
 *
 * @see 
 *
 */
CeMLErrorType CeMLCipherDeInit (CeMLCntxHandle  ** ceMlHandle)
{
  /* Check pointer then free data */
  if ((*ceMlHandle)->pClientCtxt)
  {
    CeElfree((*ceMlHandle)->pClientCtxt);
    (*ceMlHandle)->pClientCtxt = NULL;
    CeElfree(*ceMlHandle);
    *ceMlHandle = NULL;  
  }

  return CEML_ERROR_SUCCESS;
}


/**
 * @brief 
 *
 * @param 
 * 
  @return 
 *
 * @see 
 *
 */
CeMLErrorType CeMLCipherSetParam (CeMLCntxHandle       *ceMlHandle,
                                  CeMLCipherParamType  nParamID, 
                                  const void           *pParam, 
                                  uint32               cParam )
{
  CeMLErrorType retVal = CEML_ERROR_SUCCESS;

  switch (nParamID)
  {
    case CEML_CIPHER_PARAM_DIRECTION:
      if ((sizeof(CeMLCipherDir) != cParam) || (!pParam))
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }
      ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.dir = 
        (CeCLCipherDir)(*((CeCLCipherDir*)pParam));
      break;

    case CEML_CIPHER_PARAM_KEY: 
      if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128)
      {
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = FALSE;

        //If NULL key pointer use HW key
        if(!pParam)
        {
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = TRUE;
          break;
        }

        if((CEML_AES128_KEY_SIZE != cParam) && (pParam != 0))
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256)
      {
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = FALSE;

        //If NULL key pointer use HW key
        if(!pParam)
        {
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = TRUE;
          break;
        }

        if((CEML_AES256_KEY_SIZE != cParam) && (pParam != 0))
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }
      
      if(((CEML_AES128_KEY_SIZE == cParam) || (CEML_AES256_KEY_SIZE == cParam)) && (pParam != 0))
      { 
        //Write key to context structure
        CeElMemcpy(((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.aes_key, (void*) pParam, cParam);
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }
      break;

    case CEML_CIPHER_PARAM_XTS_KEY: 

      /* Check if we are using CRYPTO Engine 3 or 4 */
      if ( ((((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.mode) != CEML_CIPHER_MODE_XTS) )
      {
        retVal = CEML_ERROR_NOT_SUPPORTED;
        break;
      }

      if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128)
      {
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = FALSE;

        //If NULL key pointer use HW key
        if(!pParam)
        {
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = TRUE;
          break;
        }

        if((CEML_AES128_KEY_SIZE != cParam) && (pParam != 0))
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256)
      {
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = FALSE;

        //If NULL key pointer use HW key
        if(!pParam)
        {
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = TRUE;
          break;
        }

        if((CEML_AES256_KEY_SIZE != cParam) && (pParam != 0))
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }
      
      if(((CEML_AES128_KEY_SIZE == cParam) || (CEML_AES256_KEY_SIZE == cParam)) && (pParam != 0))
      { 
        //Write key to context structure
        CeElMemcpy(((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.aes_xts_key, (void*) pParam, cParam);
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }
      break;

    case CEML_CIPHER_PARAM_XTS_DU_SIZE: 

      /* Check if we are using CRYPTO Engine 3 or 4 */
      if (((((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.mode) != CEML_CIPHER_MODE_XTS) )
      {
        retVal = CEML_ERROR_NOT_SUPPORTED;
        break;
      }

      //Set the Data Unit Size
      if ( (pParam !=0) && (sizeof(uint32) == cParam) )
      {
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.xts_du_size = *((uint32 *)pParam);
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }
      break;

    case CEML_CIPHER_PARAM_IV:
      if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128 || 
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256)
      {
        if (CEML_AES128_IV_SIZE != cParam)
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }

      if (pParam)
      {
        //Copy IV vector to context
        CeElMemcpy(((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.iv, (void*) pParam, cParam);
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }
      break;

    case CEML_CIPHER_PARAM_MODE:
      if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128 ||
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256)
      {
        if (sizeof(CeMLCipherModeType) != cParam)
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }

        /* Set context mode */
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.mode = 
         (CeCLCipherModeType) (*((CeCLCipherModeType*)pParam));
      break;

    case CEML_CIPHER_PARAM_NONCE:
      if ((((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128 || 
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256) &&
          ((pParam) && (cParam <= CECL_AES_NONCE_SIZE_BYTES)))
      {
          //Copy nonce data to context
          CeElMemcpy(((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.nonce, (void*) pParam, cParam);
          //Set nonce data size in bytes
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.nonceLn = cParam;
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }
      break; 

    default:
      retVal = CEML_ERROR_INVALID_PARAM;
      break;
  }
  return retVal;
}

/**
 * @brief 
 *
 * @param 
 * 
  @return 
 *
 * @see 
 *
 */
CeMLErrorType CeMLCipherGetParam (CeMLCntxHandle       * ceMlHandle,
                                  CeMLCipherParamType  nParamID, 
                                  const void           *pParam, 
                                  uint32               *cParam)
{
  CeMLErrorType retVal = CEML_ERROR_SUCCESS;

  switch (nParamID)
  {
    case CEML_CIPHER_PARAM_DIRECTION:
      if ((sizeof(CeMLCipherDir) != *cParam) || (!pParam))
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }
      (*((CeMLCipherDir*)pParam)) = (CeMLCipherDir)
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.dir;
      break;

    case CEML_CIPHER_PARAM_KEY:
      if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128)
      {
        if((cParam != 0) && (CEML_AES128_KEY_SIZE != *cParam))
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256)
      {
        if((cParam != 0) && (CEML_AES256_KEY_SIZE != *cParam))
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }

      /* Check if we are using HW key */
      if((cParam == 0) && (!pParam))
      {
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = TRUE;
        break;
      }

      if(cParam != 0) 
      {
        if(((CEML_AES128_KEY_SIZE != *cParam) || (CEML_AES256_KEY_SIZE != *cParam)) && (pParam != 0))
        {
          //Get the key
          CeElMemcpy((void*) pParam,((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.aes_key, 
                    *cParam);
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }

      break;

    case CEML_CIPHER_PARAM_IV:
      if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128 || 
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256)
      {
        if ((cParam != 0) && (CEML_AES128_IV_SIZE != *cParam))
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }

      if(cParam != 0) 
      {
        if((CEML_AES128_IV_SIZE != *cParam) && (pParam != 0))
        {
          //Get IV vector
          CeElMemcpy((void*) pParam, ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.iv, *cParam);
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }
      break;

    case CEML_CIPHER_PARAM_MODE:
      if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128 ||
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256)
      {
        if (sizeof(CeMLCipherModeType) != *cParam)
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }

      (*((CeCLCipherModeType*)pParam)) = 
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.mode;
      break;

    case CEML_CIPHER_PARAM_NONCE:
      if ((((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128 || 
          ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256) &&
          ((pParam) && (*cParam <= CECL_AES_NONCE_SIZE_BYTES)))
      {
          //Get Nonce data
          CeElMemcpy((void*) pParam, ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.nonce, *cParam);
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }
      break;

    case CEML_CIPHER_PARAM_XTS_KEY:
      if ( ((((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.mode) != CEML_CIPHER_MODE_XTS) )
      {
        retVal = CEML_ERROR_NOT_SUPPORTED;
        break;
      }

      if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES128)
      {
        if((cParam != 0) && (CEML_AES128_KEY_SIZE != *cParam))
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else if (((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.algo == CEML_CIPHER_ALG_AES256)
      {
        if((cParam != 0) && (CEML_AES256_KEY_SIZE != *cParam))
        {
          retVal = CEML_ERROR_INVALID_PARAM;
          break;
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
        break;
      }

      /* Check if we are using HW key */
      if((cParam == 0) && (!pParam))
      {
        ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.bAESUseHWKey = TRUE;
        break;
      }

      if(cParam != 0) 
      {
        if(((CEML_AES128_KEY_SIZE != *cParam) || (CEML_AES256_KEY_SIZE != *cParam)) && (pParam != 0))
        {
          //Get the key
          CeElMemcpy((void*) pParam,((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.aes_xts_key, *cParam);
        }
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }
      break;

    case CEML_CIPHER_PARAM_XTS_DU_SIZE: 
      if (((((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.mode) != CEML_CIPHER_MODE_XTS))
      {
        retVal = CEML_ERROR_NOT_SUPPORTED;
        break;
      }

      //Get the Data Unit Size
      if ((pParam !=0) && (sizeof(uint32) == *cParam))
      {
         (*((uint32 *)pParam)) = ((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx.xts_du_size;
      }
      else
      {
        retVal = CEML_ERROR_INVALID_PARAM;
      }
      break;

    default:
      retVal = CEML_ERROR_INVALID_PARAM;
      break;
  }
  return retVal;
}

/**
 * @brief 
 *
 * @param 
 * 
  @return 
 *
 * @see 
 *
 */
CeMLErrorType CeMLCipherData (CeMLCntxHandle     *ceMlHandle, 
                              CEMLIovecListType  ioVecIn,
                              CEMLIovecListType  * ioVecOut)
{
  CeMLErrorType        ret = CEML_ERROR_SUCCESS;
  CeCLCipherCntxType*  ctx_ptr = NULL;
  uint32               pdwActualOut;
  CeElXferModeType     xferMode;
  CeElXferFunctionType xferFunc; 
  uint32               curr_blk_size;
  uint32               num_blks;
  CEMLIovecListType    ioVecIn_tmp;
  CEMLIovecListType    ioVecOut_tmp;
  uint32               input_len;
  uint32               input_len2;
  uint8*               input_ptr;
  uint8*               output_ptr;

  /* Sanity check inputs */
  if ((!ceMlHandle) || (!ceMlHandle->pClientCtxt))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  //Create context pointer
  ctx_ptr = (CeCLCipherCntxType*) &(((CeMLCipherAlgoCntxType*)((ceMlHandle)->pClientCtxt))->ctx);

  if (((ioVecOut->size!= 1)  || (!ioVecOut->iov)) 
       ||((ioVecIn.size!= 1) || (!ioVecIn.iov))
       ||((ctx_ptr->mode != CECL_CIPHER_MODE_CTR)
        &&(ioVecIn.iov->dwLen % 16 != 0)))
  {
    return CEML_ERROR_INVALID_PARAM;
  }

  CeElMutexEnter();

  //Init temp IoVect pointers
  ioVecIn_tmp  = ioVecIn;
  ioVecOut_tmp = *ioVecOut;
  input_len    = ioVecIn.iov->dwLen;
  input_len2   = ioVecIn.iov->dwLen;
  input_ptr    = ioVecIn_tmp.iov->pvBase;
  output_ptr   = ioVecOut_tmp.iov->pvBase;

  //Calculate block size based on input data length
  num_blks = ioVecIn_tmp.iov->dwLen / ceml_max_block_size; 
  if(ioVecIn_tmp.iov->dwLen % ceml_max_block_size)
  {
    ++num_blks;
  }

  //Set up loop to process all input data in blks of max size of 32K bytes
  while(num_blks--)
  {
    //Calculate current blk size
    curr_blk_size = ceml_max_block_size;
    if(num_blks == 0)
    {
      curr_blk_size = (ioVecIn_tmp.iov->dwLen % ceml_max_block_size);
      if(curr_blk_size == 0)
      {
        curr_blk_size = ceml_max_block_size;
      }
    }  

    //Set sizes
    ctx_ptr->dataLn         = curr_blk_size;
    ioVecIn_tmp.iov->dwLen  = curr_blk_size;
    ioVecOut_tmp.iov->dwLen = curr_blk_size;

    pdwActualOut = 0;
    ret = (CeMLErrorType) CeClIOCtlCipher(CECL_IOCTL_SET_CIPHER_CNTXT, 
                                         (uint8 *)(ctx_ptr), 
                                          sizeof(CeCLCipherCntxType), 
                                          NULL, 
                                          0, 
                                          &pdwActualOut);
    if (ret != CEML_ERROR_SUCCESS)
    {
      break;
    }

    //Get Xfer mode    
    ret = (CeMLErrorType) CeElGetXferModeList(&xferMode);
    if (CEML_ERROR_SUCCESS != ret) 
    {
      break;
    }

    //Decide how to set function pointer
    ret = (CeMLErrorType) CeElGetXferfunction(xferMode, (CeElXferFunctionType*)&xferFunc);
    if (CEML_ERROR_SUCCESS != ret)
    {
      break;
    }

    //Execute function pointer    
    ret = (CeMLErrorType) xferFunc((CEELIovecListType*) &ioVecIn_tmp, (CEELIovecListType*) &ioVecOut_tmp, 
                                    FALSE, (uint8 *)(ctx_ptr), CEEL_DATA_CIPHER); 
    if (CEML_ERROR_SUCCESS != ret) 
    {
      break;
    }

    pdwActualOut = sizeof(CeCLCipherCntxType);
    ret = (CeMLErrorType) CeClIOCtlCipher(CECL_IOCTL_GET_CIPHER_CNTXT, 
                                          NULL, 
                                          0, 
                                          (uint8 *)(ctx_ptr), 
                                          pdwActualOut, 
                                          &pdwActualOut);
    if (CEML_ERROR_SUCCESS != ret) 
    {
      break;
    }

    //Increment IoVect Tmp pointers
    input_ptr  += curr_blk_size;
    output_ptr += curr_blk_size;
    ioVecIn_tmp.iov->pvBase  = input_ptr;
    ioVecOut_tmp.iov->pvBase = output_ptr;
    
    //Decrement Input size
    input_len2 -= curr_blk_size;
    ioVecIn_tmp.iov->dwLen  = input_len2;
  } /* while */

  //Make sure we set in/out lengths again
  ioVecIn.iov->dwLen   = input_len;
  ioVecOut->iov->dwLen = input_len;

  CeElMutexExit();

  return ret;
}
