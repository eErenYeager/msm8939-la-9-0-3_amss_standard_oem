/**
@file CeML_kdf.c 
@brief Crypto Engine Module source file
  This is an implementation of the key derivation algorithm based on
  AES_CMAC from NIST SP 800-38B.

  NIST SP 800-38B is more of a recommendation than a standard, as it leaves 
  several details unspecified. In particular the following
  decisions/interpretations were made.
  1.  Where integers are passed to AES_CMAC, the integers are 
      presented (to the crypto algorithm) as 4 bytes in network (big-endian)
      order.  This limits the maximum length of the output to 2^32 - 1
      bits.  
  2.  NIST SP 800-38B calls for the requested output length to be specified in
      bits.  Thus it is possible to request output that ends in a 
      partial bytes. I chose to pad such output with zeros on the right
      (least significant bits) out to a byte boundary.
*/

/**********************************************************************
 * Copyright (c) 2011 Qualcomm Technologies Incorporated. All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 **********************************************************************/
/*======================================================================

                        EDIT HISTORY FOR MODULE
 
 $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/shared/src/CeML_kdf.c#2 $
 $DateTime: 2017/07/27 05:16:37 $
 $Author: pwbldsvc $ 

 when         who     what, where, why
 --------     ---     --------------------------------------------------- 
=======================================================================*/

/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "IxErrno.h"
#include "CeML_kdf.h"
#include "CeEL.h"
/*===========================================================================

            REGIONAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/
#define AES128_KEY_SIZE        16
#define AES_BLOCK_SZ (0x10)
#define AES_MAXNR 14
#define R (4) /* width of counter in bytes.  Like r in SP800-108, but in bytes rather than bits. */
#define AES_BLOCK_SIZE_BITS (AES_BLOCK_SZ * 8)
#define AES_BLOCK_LONGS     (AES_BLOCK_SZ / sizeof (uint32))
#define CEML_KEY_MAX_KEY_LEN      512
/*
 * multiplies a polynomial of degree AES_BLOCK_SZ*8 by x mod x^128 +
 * x^7 + x^2 + x + 1.  srcpoly and destpoly can be the same.
 */
#define MODPOLY 0x87 /* modulus tail in binary */

typedef union {
      uint8  bytes[AES_BLOCK_SZ];
      uint32 longs[AES_BLOCK_SZ / sizeof(uint32)];
} aes_aligned_block;

typedef struct {
      const void*             key;
      int                     key_len;
      aes_aligned_block   K1;
      aes_aligned_block   K2;
} AES_CMAC_keystruct;

typedef struct {
      const AES_CMAC_keystruct *ks;
      aes_aligned_block         C;
      aes_aligned_block         buffer;
      int                           buffer_fill;
} AES_CMAC_ctx;

int CeMLAesEncryptUsingHWKey(  const void  *key,  
	uint32       key_len,  
	uint8       *pt,  
	uint32       pt_len,  
	uint8       *ct,  
	uint32       ct_len);

/*===========================================================================

                     Local Functions FOR MODULE

===========================================================================*/
static void mpy_x
(
 aes_aligned_block *destpoly, 
 aes_aligned_block *srcpoly
) 
{
   int i;
   int r = (srcpoly->bytes[0] & 0x80) ? MODPOLY : 0;

   for (i = 0; i < AES_BLOCK_SZ - 1; ++i) 
   {
      destpoly->bytes[i] = (srcpoly->bytes[i] << 1) | ((srcpoly->bytes[i+1] >> 7) & 1);
   }
   destpoly->bytes[AES_BLOCK_SZ - 1] =
      (srcpoly->bytes[AES_BLOCK_SZ - 1] << 1) ^ r;
}


/* Builds the key schedule and the subkeys in keystruct. keylen is in bytes. */
int aes_cmac_setup
(
 AES_CMAC_keystruct *keystruct,
 const void             *key, 
 uint32                  keylen
)
{
  aes_aligned_block      zero;
  aes_aligned_block      L;
  int                        i;
  int                        result;

  keystruct->key = key;
  keystruct->key_len = keylen;  
  
  /* Construct all zero block.   Avoid initialized static for maximum compatibility */
  for (i = 0; i < AES_BLOCK_SZ; i++) 
  {
    zero.bytes[i] = 0;
  }
  
  result = CeMLAesEncryptUsingHWKey (keystruct->key, keystruct->key_len,
                                   zero.bytes, AES_BLOCK_SZ, 
                                   L.bytes, AES_BLOCK_SZ);
  if(E_SUCCESS == result )
  {
    /* build the subkeys K1 and K2 */
    mpy_x(&(keystruct->K1), &L);
    mpy_x(&(keystruct->K2), &(keystruct->K1));
  }
  
  return result;

}

/*
 * Zeroizes *keystruct.   Call before freeing or abandoning the
 * keystruct.  The keystruct contains sensitve information.
 */
void aes_cmac_zeroize_key
(
 AES_CMAC_keystruct *keystruct
)
{

   /* Depend on aes_key_st being int aligned and size being
      a multiple of int size.  Don't use longs, because that
      is not guaranteed by aes.h.  Yes, aes.h should be used
      as opaque spec.
    */

    CeElSecMemset((void *)keystruct,0,sizeof(AES_CMAC_keystruct));
   
}


/*
 * Initialized the ctx structure.  The ctx structure will contain a
 * pointer to keystruct.  The keystruct must not be modified or freed
 * while ctx is live.
 * !!! As a consquence, the caller has the responsibilty to make sure 
 * that the key is not    deallocated or modifed from the aes_cmac_Init 
 * call through the aes_cmac_Final call.
 */
void aes_cmac_Init
(
 AES_CMAC_ctx             *ctx,
 const AES_CMAC_keystruct *keystruct
)
{
   int i;

   ctx->ks = keystruct;
   ctx->buffer_fill = 0;
   for (i = 0; i < AES_BLOCK_LONGS; ++i) {
      ctx->C.longs[i] = 0;
   }
}

/* overwrites *data */
static void aes_cmac_update_whole_block
(
 AES_CMAC_ctx             *ctx, 
 aes_aligned_block        *data,
 const aes_aligned_block  *xor_block
)
{
   int i;

   /* Indeed, we don't know if this is running on a big-ending or
      little-endian machine, but we do know that xor_block and data
      are the same endianness, and xor does not depend on endianness
      so it doesn't matter */
   if (xor_block != 0) {
      for (i = 0; i < AES_BLOCK_LONGS; ++i) {
   data->longs[i] ^= ctx->C.longs[i] ^ xor_block->longs[i];
      }
   } else {
      for (i = 0; i < AES_BLOCK_LONGS; ++i) {
   data->longs[i] ^= ctx->C.longs[i];
      }
   }

   CeMLAesEncryptUsingHWKey ( ctx->ks->key, ctx->ks->key_len,
                            data->bytes, AES_BLOCK_SZ,
                            ctx->C.bytes, ctx->ks->key_len );
}
/*
 * According to the NIST SP 800-38B, AES_CMAC can accept messages that are
 * not a mulitple of 8 bits.  This is not supported.  (It is easy to
 * add, but it adds testing and will probably never be used.) len is
 * in bytes.
 */
void aes_cmac_Update
(
 AES_CMAC_ctx *ctx, 
 const uint8 *data, 
 uint32 len
)
{
    uint32 i;

    for (i = 0; i < len; ++i) {
      if (ctx->buffer_fill >= AES_BLOCK_SZ) {
    /* A complete block is in the buffer and it is not the last
      block (or control wouldn't be here).  Process the block.
      The 0 indicates not last block.  Note: this overwrites
      ctx->buffer, but that's okay. */
    aes_cmac_update_whole_block(ctx, &ctx->buffer, 0);
    ctx->buffer_fill = 0;
      }

      ctx->buffer.bytes[ctx->buffer_fill++] = data[i];
    }
}

/*
 * len is the number of bytes of digest to generate.  If len exceeds
 * the AES block size (16 bytes, i.e. 128 bits), the remaining space
 * is zeroed.  The ctx structure is cleared to avoid leaking sensitive
 * information into memory.
 */
void aes_cmac_Final
(
 AES_CMAC_ctx *ctx, 
 uint8 *md, uint32 len
)
{
   uint32 i;

   if (ctx->buffer_fill < AES_BLOCK_SZ) {
      /* Partial block: Add padding, use K2 */
      ctx->buffer.bytes[ctx->buffer_fill++] = 0x80;
      while (ctx->buffer_fill < AES_BLOCK_SZ) {
   ctx->buffer.bytes[ctx->buffer_fill++] = 0;
      }
      aes_cmac_update_whole_block(ctx, &ctx->buffer, &ctx->ks->K2);
   } else {
      /* whole block: no padding, use K1 */
      aes_cmac_update_whole_block(ctx, &ctx->buffer, &ctx->ks->K1);
   }
   ctx->buffer_fill = 0;  /* keep invariants correct, even if dead */
   ctx->ks = 0; /* null pointer will induce fault if dead ctx used */

   /* Copy out digest. */

   for (i = 0; i < len && i < AES_BLOCK_SZ; ++i) {
      md[i] = ctx->C.bytes[i];
   }

   /*
    * Store zeros past AES block size if user asks for more than
    * AES_BLOCK_SZ.
    */
   /* Arguably this is an error on the part of the caller, but then we
      would have to make this return an error code and callers would
      have to check it.   It is easier to just define it do something
      reasonable. */

   for (; i < len; ++i) {
      md[i] = 0;
   }

   /*
    * Zeroize possibly sensitive data.  The digest is in C.  With
    * single-block messages, the buffer contains the message XORd with
    * K1 or K2.
    */

   for (i = 0; i < AES_BLOCK_LONGS; ++i) {
      ctx->buffer.longs[i] = 0;
      ctx->C.longs[i] = 0;
   }
}

#define BITS_TO_BYTES(n) (((n) + 7) / 8)  /* round up */


/* converts v to bytes in network (big endian) order, which is then
   stored in dat_len bytes in dat.  returns 0 on success, -1 if dat_len
   bytes cannot represent v.  v is a uint32 (a rather unexpected type),
   since in this program it is always called with a value that is, or
   is up to, the size of an object.  to_net_bytes could have been done
   with htonl(), but that function is not available in the ARM
   library.  It could also be done with the ARM REV instruction.  This
   was chosen for simplicity portability, and generality.
*/
int
to_net_bytes(uint32 v, uint8 *dat, uint32 dat_len)
{
   uint32 i;
   for (i = 0; i < dat_len; ++i) {
      dat[dat_len - 1 - i] = v & 0xff;
      v >>= 8;
   }
   return (v == 0 ? 0 : -1);
}


/**
  Derives a Key using known label, context strings and using either 
  the Hardware Key [Primary or Secondary] or a user provided Key.

  This is an implementation of the key derivation algorithm based on
  AES_CMAC from FIPS Special Publication 800-38B.

  @param[in]  input_key        Pointer to the input Key.
                               This should be NULL if Primary or Secondary
                               HW Key are to be used for Ciphering.
			       
  @param[in]  input_key_len    Length of the 'input_key' in bytes.
                               The value of this is not acted on if the
                               'input_key' is NULL.

  @param[in]  label            Pointer to a string constant.

  @param[in]  label_len        Length of the string constant 'label'.

  @param[in]  context          Pointer to a string constant.

  @param[in]  context_len      Length of the string constant 'context'.

  @param[out]  output_key      Pointer to the generated Key.
                               The Memory for this should be provided by the caller.
  @param[in]   output_key_len  Length of the Key to be generated in bytes.
                               This is size of the buffer that the 'output_key'
                               points to.
       
  @return
  CEML_ERROR_SUCCESS - Successful completion.
  CEML_ERROR_FAILURE - Any other failure.

  @dependencies
  The memory for the 'output_key' should be allocated by the caller
  according to the 'output_key_len'.
*/
CeMLErrorType CeML_kdf
(
  const void  *input_key,
  uint32       input_key_len,
  const void  *label,
  uint32       label_len,
  const void  *context,
  uint32       context_len,
  void        *output_key,
  uint32       output_key_len
)
{
  uint32 nblks = (output_key_len * 8 + AES_BLOCK_SIZE_BITS-1)/AES_BLOCK_SIZE_BITS;
  uint32 i;
  int    rv;
  const uint8 zero = 0; 
  uint8 outlen_net_order[R];
  AES_CMAC_keystruct ks;
  AES_CMAC_ctx       ctx;
  uint32  output_len_bits = 0;

  /*
   * input validation
   */
   
  /*
   * SP 800-108 only requires nblks <= 2**32-1.  This code requires
   * output_key_len <= (2**32-1)/8, which is a stronger condition.  
   */
  if (output_key_len > (0xffffffff/8) ) 
  {
    return CEML_ERROR_FAILURE;
  }
  output_len_bits = output_key_len * 8;

  /* NULL pointer validations */
  if (label_len > 0 && label == NULL) 
  {
    return CEML_ERROR_FAILURE;
  }
  if (context_len > 0 && context == NULL) 
  {
    return CEML_ERROR_FAILURE;
  }
  if (output_len_bits > 0 && output_key == NULL) 
  {
    return CEML_ERROR_FAILURE;
  }

  /* This check is valid if the rollover happens from 0xFFFFFFFF
     to 0 hence check for arithmetic overflow */
  if ((uintnt)label + label_len < (uintnt)label)
  {
    /* address would wrap */
    return CEML_ERROR_FAILURE;
  }
  if ((uintnt)context + context_len < (uintnt)context)
  {
    /* address would wrap */
    return CEML_ERROR_FAILURE;
  }
  if ((uintnt)output_key + BITS_TO_BYTES(output_len_bits) < (uintnt)output_key) 
  {
    /* address would wrap */
    return CEML_ERROR_FAILURE;
  }

  /*
   * aes_cmac_setup (called below) does input validation on input_key and
   * input_key_len, so we don't need to validate input_key and input_key_len here.
   */

  /* This can't fail because of the check against 0xffffffff/8 above. */
  rv = to_net_bytes(output_len_bits, outlen_net_order, R);

  /*
   * aes_cmac_setup validates input_key and input_key_len and fills in the 
   * key structure (ks)
   */
  rv = aes_cmac_setup(&ks, input_key, input_key_len);
  if (rv)
  {
    return CEML_ERROR_FAILURE;
  }

  /* From here on, we have to zeroise ks before returning */
  /* Now do the CMAC calculation for each output block */
  for (i = 0; i < nblks; ++i) 
  {
    uint8 i_net_order[R];
    rv = to_net_bytes(i+1, i_net_order, R);
    /* Now do the AES_CMAC calculation */
    aes_cmac_Init(&ctx, &ks);
    aes_cmac_Update(&ctx, i_net_order, R);
    aes_cmac_Update(&ctx, label, label_len);
    aes_cmac_Update(&ctx, &zero, 1);
    aes_cmac_Update(&ctx, context, context_len);
    aes_cmac_Update(&ctx, outlen_net_order, R);
    aes_cmac_Final( &ctx,
                    (uint8 *)output_key + i * AES_BLOCK_SZ,
                    (i < nblks -1 || output_len_bits % AES_BLOCK_SIZE_BITS == 0 ?
                    (AES_BLOCK_SZ) : /* normal case */
                    BITS_TO_BYTES(output_len_bits % AES_BLOCK_SIZE_BITS)) );
                    /* last block and it is a fractional block  */
    /* aes_cmac_Final zeroizes the ctx structure, so we don't need to. */
  }

  /* zero out trailing bits of last byte, if necessary */
  if ((output_len_bits % 8) != 0)
  {
    ((uint8 *)output_key)[BITS_TO_BYTES(output_len_bits) - 1] &= 
      (0xff00 >> (output_len_bits % 8));
  }

  /* zeroize key structure and return */
  aes_cmac_zeroize_key(&ks);

  return CEML_ERROR_SUCCESS;
} /* CeML_kdf() */

int CeMLAesEncryptUsingHWKey
(
  const void  *key,
  uint32       key_len,
  uint8        *pt,
  uint32       pt_len,
  uint8       *ct,
  uint32       ct_len
)
{
  CeMLErrorType       ret_val = CEML_ERROR_SUCCESS;
  CeMLCipherDir       cipher_direction;
  CeMLCipherModeType  cipher_mode;
  CeMLCntxHandle*     cntx = NULL;
  CEMLIovecListType   ioVecListIn;
  CEMLIovecListType   ioVecListOut;
  CEMLIovecType       IovecIn;
  CEMLIovecType       IovecOut;
  int                 result = E_SUCCESS;

  /* Validate input parameters */
  if ( (key_len > CEML_KEY_MAX_KEY_LEN) ||
       (NULL == ct) || (NULL == pt) )
  {
    return E_FAILURE;
  }

  do
  {

    /* Input IOVEC */
    ioVecListIn.size          = 1;
    ioVecListIn.iov           = &IovecIn;
    ioVecListIn.iov[0].dwLen  = pt_len;
    ioVecListIn.iov[0].pvBase = pt; 

    /* Output IOVEC */
    ioVecListOut.size          = 1;
    ioVecListOut.iov           = &IovecOut;
    ioVecListOut.iov[0].dwLen  = ct_len;
    ioVecListOut.iov[0].pvBase = ct;


    ret_val = CeMLInit();
    if ( ret_val != CEML_ERROR_SUCCESS )
    {
      result = E_FAILURE;
      break;
    }

    //Init ctx
    if(CEML_AES128_KEY_SIZE == key_len)
    {
      ret_val = CeMLCipherInit( &cntx, CEML_CIPHER_ALG_AES128 );
      if ( ret_val != CEML_ERROR_SUCCESS )
      {
        result = E_FAILURE;
        break;
      }
      ret_val = CeMLCipherSetParam( cntx, CEML_CIPHER_PARAM_KEY, key, key_len );
      if ( ret_val != CEML_ERROR_SUCCESS )
      {
        result = E_FAILURE;
        break;
      }
    }
    else
    {
      ret_val = CeMLCipherInit( &cntx, CEML_CIPHER_ALG_AES256 );
      if ( ret_val != CEML_ERROR_SUCCESS )
      {
        result = E_FAILURE;
        break;
      }
		
      ret_val = CeMLCipherSetParam( cntx, CEML_CIPHER_PARAM_KEY, key, key_len );
      if ( ret_val != CEML_ERROR_SUCCESS )
      {
        result = E_FAILURE;
        break;
      }
    }
  
    cipher_direction = CEML_CIPHER_ENCRYPT;
    ret_val = CeMLCipherSetParam( cntx, CEML_CIPHER_PARAM_DIRECTION,
                                  &cipher_direction, sizeof(CeMLCipherDir) );
    if ( ret_val != CEML_ERROR_SUCCESS )
    {
      result = E_FAILURE;
      break;
    }

    cipher_mode = CEML_CIPHER_MODE_ECB;
    ret_val = CeMLCipherSetParam( cntx, CEML_CIPHER_PARAM_MODE, &cipher_mode,
                                  sizeof(CeMLCipherModeType) );
    if ( ret_val != CEML_ERROR_SUCCESS )
    {
      result = E_FAILURE;
      break;
    }

    ret_val = CeMLCipherData( cntx, ioVecListIn, &ioVecListOut );
    if ( ret_val != CEML_ERROR_SUCCESS )
    {
      result = E_FAILURE;
      break;
    }

  } while(0);

  if ( cntx )
  { 
    ret_val = CeMLCipherDeInit( &cntx );
    if ( ret_val != CEML_ERROR_SUCCESS )
    {   
      result = E_FAILURE;
    }
  }

  ret_val = CeMLDeInit();
  if ( ret_val != CEML_ERROR_SUCCESS )
  {
    result = E_FAILURE;
  }

  return result;

} /* CeMLAesEncryptUsingCE1() */

