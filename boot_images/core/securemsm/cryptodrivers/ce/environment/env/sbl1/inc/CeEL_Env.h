/*===========================================================================

                    Crypto Engine Environment API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2000-2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/environment/env/sbl1/inc/CeEL_Env.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
 03/10/12   yk      Modification for Crypto5
 04/29/10   bm      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "comdef.h"
#include "string.h"
#include "boot_comdef.h"
#include "boot_cache_mmu.h"  

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

//Set up clocks
#define CeElClkEnable()   CeClClockEnable()
#define CeElClkDisable()  CeClClockDisable()

#define CEEL_RETAILMSG(a, b, c, d)  MSG_ERROR(a, b, c, d)

#define CEEL_ASSERT() ASSERT(0)

#define CEEL_PMEM_GET_PHYS_ADDR(a) (a) 

#define CEEL_MUTEX_TYPE 

#define CEEL_MUTEX_ENTER()

#define CEEL_MUTEX_EXIT() 

#define CEEL_MEMORY_BARRIER() memory_barrier()

typedef enum
{
  CEEL_ENV_ERROR_SUCCESS                = 0x0,
  CEEL_ENV_ERROR_FAILURE                = 0x1,
  CEEL_ENV_ERROR_INVALID_PARAM          = 0x2
} CeEL_EnvErrorType;

CeEL_EnvErrorType CeElmalloc_Env(void** ptr, uint32 ptrLen);

CeEL_EnvErrorType CeElfree_Env(void* ptr);



