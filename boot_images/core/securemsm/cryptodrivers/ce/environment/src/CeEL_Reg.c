/**
@file CeEL_Reg.c 
@brief Crypto Engine REG source file 
*/

/*===========================================================================

                     

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2009- 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/environment/src/CeEL_Reg.c#1 $   
  $DateTime: 2015/03/19 01:58:37 $ 
  $Author: pwbldsvc $ 

when         who     what, where, why
--------     ---     ---------------------------------------------------------- 
2012-07-31   nk      Boot version
2012-03-09   yk      Modification for Crypto5
2010-08-23   bm      Initial Version
============================================================================*/


/*===========================================================================
 
                           INCLUDE FILES

===========================================================================*/
#include "boot_comdef.h"
#include "CeEL.h"
#include "CeEL_Env.h"
#include "CeCL.h"

/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/

/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/**
 * @brief 
 *
 * @return none
 *
 * @see 
 *
 */

CeELErrorType CeElRegInit (void)
{
  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief  
 *
 * @param
 *  
 * @return 
 *
 * @see 
 *
 */

CeELErrorType CeELHashRegXfer( uint8 *buff_ptr, 
                               uint32 buff_len,
                               uint8 *digest_ptr,
                               uint32 digest_len)
{
  CeELErrorType ret_val = CEEL_ERROR_SUCCESS;
  CeCLHashXferType xferInfo;
  uint32  pdwActualOut = sizeof(CeCLHashXferType);

  xferInfo.buff_ptr = buff_ptr;
  xferInfo.buff_len = buff_len;

  ret_val = (CeELErrorType) CeClIOCtlHash(CECL_IOCTL_HASH_XFER, 
            (uint8 *)(&xferInfo), 
            sizeof(CeCLHashXferType), 
            NULL, 
            0, 
            &pdwActualOut);

  return ret_val;
}

  /**
 * @brief  
 *
 * @param 
 *
 * @return
 *
 * @see 
 *
 */
CeELErrorType CeELCipherRegXfer( uint8 *datain_ptr, 
                                 uint32 nDataLen,
                                 uint8 *dataout_ptr)
{
  CeELErrorType ret = CEEL_ERROR_SUCCESS;
  CeCLCipherXferType xferInfo;
  uint32  pdwActualOut = sizeof(CeCLCipherXferType);

  xferInfo.pDataIn  = datain_ptr;
  xferInfo.nDataLen = nDataLen;
  xferInfo.pDataOut = dataout_ptr;

  ret = (CeELErrorType) CeClIOCtlCipher(CECL_IOCTL_CIPHER_XFER, 
                                        (uint8 *)(&xferInfo), 
                                        sizeof(CeCLCipherXferType), 
                                        NULL, 
                                        0, 
                                        &pdwActualOut);

  return ret;
}


