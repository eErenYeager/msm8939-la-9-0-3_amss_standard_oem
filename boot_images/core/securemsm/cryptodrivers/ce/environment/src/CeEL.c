/**
@file CeEL.c 
@brief Crypto Engine Environment Library source file 
*/

/*===========================================================================

                     Crypto Engine Environment Library 

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2010 - 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

 $Header: 
 $DateTime: 2017/07/27 05:16:37 $
 $Author: pwbldsvc $ 

when         who     what, where, why
--------     ---     ---------------------------------------------------------- 
2012-03-09   yk      Modification for Crypto5
2010-05-24   bm      Initial version
============================================================================*/
#include <stringl/stringl.h>
#include "CeEL.h"
#include "CeCL_Env.h"
#include "CeEL_Env.h"
#include "CeEL_Reg.h"
#include "CeEL_Bam.h"
#include "CeCL.h"

CeELErrorType CeElBamXferFunction (CEELIovecListType* inData, 
                                   CEELIovecListType* outData,
                                   boolean lastBlock,
                                   uint8* cntx,
                                   CEELDataType dataType);

CeELErrorType CeElRegXferFunction (CEELIovecListType* inData, 
                                   CEELIovecListType* outData,
                                   boolean lastBlock, 
                                   uint8* cntx,
                                   CEELDataType dataType);


/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMemoryBarrier(void)
{
  CEEL_MEMORY_BARRIER();

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElInit(void)
{
  CeELErrorType stat = CEEL_ERROR_SUCCESS;

  if(CECL_BAM_IS_SUPPORTED())
  {
    stat = CeElBamInit();
  }
  
  return stat;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElDeinit(void)
{
  CeELErrorType stat = CEEL_ERROR_SUCCESS;

  if(CECL_BAM_IS_SUPPORTED())
  {
    stat = CeElBamDeInit();
  }  

  return stat;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElEnableClock(void)
{
  CeELErrorType stat = CEEL_ERROR_SUCCESS;

  stat = (CeELErrorType) CeElClkEnable();

  return stat;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElDisableClock(void)
{
  CeELErrorType stat = CEEL_ERROR_SUCCESS;

  stat = (CeELErrorType) CeElClkDisable();

  return stat;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElmalloc(void** ptr, uint32 ptrLen)
{ 
  CeElmalloc_Env(ptr, ptrLen);

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElfree(void* ptr)
{
  CeElfree_Env(ptr);

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMemcpy(void* dst, void* src, uint32 len)
{
  memcpy(dst, src, len);

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMemset(void* src, uint32 val, uint32 len)
{
  memset(src, val, len);

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElSecMemset(void* src, uint32 val, uint32 len)
{
  secure_memset(src, val, len);

  return CEEL_ERROR_SUCCESS;
}


/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElGetXferModeList(CeElXferModeType* xferMode)
{
  //Set XferMode to register
  *xferMode = CEEL_XFER_MODE_REG;

  //Set XferMode to BAM if supported
  if(CECL_BAM_IS_SUPPORTED())
  {
    *xferMode = CEEL_XFER_MODE_BAM; 
  }

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElGetXferfunction(CeElXferModeType xferMode, CeElXferFunctionType* xferFunc)
{
  //Check mode and set xfer function pointer
  if(xferMode == CEEL_XFER_MODE_REG)
    {
      *xferFunc = CeElRegXferFunction;
      return CEEL_ERROR_SUCCESS;
    }

  if(xferMode == CEEL_XFER_MODE_BAM)
  {      
    *xferFunc = CeElBamXferFunction;
    return CEEL_ERROR_SUCCESS;
  }  
  
  return CEEL_ERROR_FAILURE;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMutexEnter (void)
{
  CEEL_MUTEX_ENTER();

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElMutexExit (void)
{
  CEEL_MUTEX_EXIT();

  return CEEL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElBamXferFunction(CEELIovecListType* inData, 
                                  CEELIovecListType* outData, 
                                  boolean lastBlock, 
                                  uint8* cntx,
                                  CEELDataType dataType)
{
  CeELErrorType ret = CEEL_ERROR_FAILURE;
   
  if(CECL_BAM_IS_SUPPORTED())
  {
    if(dataType == CEEL_DATA_HASH) 
    {
      ret = CeELBamHashXfer(inData->iov->pvBase, inData->iov->dwLen, outData->iov->pvBase, outData->iov->dwLen, lastBlock, cntx); 
      return ret;
    }

    if(dataType == CEEL_DATA_CIPHER) 
    {
      ret = CeELBamCipherXfer(inData->iov->pvBase, inData->iov->dwLen, outData->iov->pvBase); 
      return ret;
    }
  }

  return ret;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeELErrorType CeElRegXferFunction (CEELIovecListType* inData, 
                                   CEELIovecListType* outData, 
                                   boolean lastBlock, 
                                   uint8* cntx,
                                   CEELDataType dataType)
{
  CeELErrorType ret = CEEL_ERROR_FAILURE;

  if(dataType == CEEL_DATA_HASH) 
  {
    ret = CeELHashRegXfer(inData->iov->pvBase, inData->iov->dwLen, outData->iov->pvBase, outData->iov->dwLen); 
    return ret;
  }

  if(dataType == CEEL_DATA_CIPHER) 
  {
      ret = CeELCipherRegXfer(inData->iov->pvBase, inData->iov->dwLen, outData->iov->pvBase); 
      return ret;
  }

  return ret;

}

