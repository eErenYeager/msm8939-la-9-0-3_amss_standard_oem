/**
@file CeEL_Bam.h 
@brief Crypto Engine BAM source file 
*/

/*===========================================================================

                     

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/environment/src/CeEL_Bam.h#1 $   
  $DateTime: 2015/03/19 01:58:37 $ 
  $Author: pwbldsvc $ 

when         who     what, where, why
--------     ---     ---------------------------------------------------------- 
2012-07-31    nk     Boot version
2012-04-25   ejt     Initial Version
============================================================================*/


/*===========================================================================
 
                           INCLUDE FILES

===========================================================================*/
#include "boot_comdef.h"
#include "comdef.h"


/*===========================================================================

            LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

===========================================================================*/
/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS

===========================================================================*/

/**
 * @brief Initialize the Data mover 
 *
 * @return none
 *
 * @see 
 *
 */

CeELErrorType CeElBamInit (void);

/**
 * @brief Initialize the Data mover 
 *
 * @return none
 *
 * @see 
 *
 */

CeELErrorType CeElBamDeInit (void);

/**
 * @brief  
 *
 * @param
 *  
 * @return 
 *
 * @see 
 *
 */

CeELErrorType CeELBamHashXfer(uint8 *buff_ptr, 
                              uint32 buff_len,
                              uint8 *digest_ptr,
                              uint32 digest_len,
                              boolean lastBlock,
                              uint8 *cntx);


/**
 * @brief  
 *
 * @param
 *  
 * @return 
 *
 * @see 
 *
 */

CeELErrorType CeELBamCipherXfer(uint8 *datain_ptr, 
                                uint32 nDataLen,
                                uint8 *dataout_ptr);

