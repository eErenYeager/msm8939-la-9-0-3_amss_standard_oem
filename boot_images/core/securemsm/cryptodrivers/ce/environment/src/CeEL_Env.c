/**
@file CeEL_Env.c 
@brief Crypto Engine Environment Library source file 
*/

/*===========================================================================

                     Crypto Engine Environment Library 

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2010 - 2010 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

 $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/environment/src/CeEL_Env.c#1 $
 $DateTime: 2015/03/19 01:58:37 $
 $Author: pwbldsvc $ 

when         who     what, where, why
--------     ---     ----------------------------------------------------------
2010-05-24   bm      Initial version
============================================================================*/
#include "CeEL_Env.h"
#include "boot_cache_mmu.h"

uint8 CeMLCntxHandleBuff[5]; 

typedef struct {
	uint8 CeMLIovecTypeBuff[15]; 
	boolean used;
} CeMLIovecType;
CeMLIovecType CeMLIovecTypeArray[6];

uint8 CeMLHashCipherAlgoCntxTypeBuff[320]; 

CeEL_EnvErrorType CeElmalloc_Env(void** ptr, uint32 ptrLen)
{  
  uint32 i = 0;
  
  if(ptrLen<5) {
        *ptr = CeMLCntxHandleBuff;

  }
  else if(ptrLen >= 5  && ptrLen <15) {
      for(i=0;i<6;i++){		
          if (CeMLIovecTypeArray[i].used  == 1) continue;
          else{
                 *ptr = CeMLIovecTypeArray[i].CeMLIovecTypeBuff;
  		   CeMLIovecTypeArray[i].used = 1;
                  break;
           }
      }
  }
  else if (ptrLen >= 15 && ptrLen <320) {
        *ptr = CeMLHashCipherAlgoCntxTypeBuff;
  }
  else {
  	return CEEL_ENV_ERROR_FAILURE;
  }
  	
  if (i >= 5) return CEEL_ENV_ERROR_FAILURE;
  
  return CEEL_ENV_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeEL_EnvErrorType CeElfree_Env(void* ptr)
{
uint32 j;


  for (j=0;j<6;j++)
  {
    if(ptr == CeMLIovecTypeArray[j].CeMLIovecTypeBuff)
    {
        CeMLIovecTypeArray[j].used = 0;
        break;
    }
    else
    {
        continue;
    }
  }
  return CEEL_ENV_ERROR_SUCCESS;
}

