/*===========================================================================

                    Crypto Engine Environment API

GENERAL DESCRIPTION


EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2010-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/environment/inc/CeEL.h#2 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/31/12   nk      Boot version
11/02/11   sm      update for crypto5 register hash interface 
04/29/10   bm      initial version
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "comdef.h"

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/
typedef enum
{
  CEEL_XFER_MODE_REG   = 0x0,
  CEEL_XFER_MODE_BAM   = 0x1,
  CEEL_XFER_MODE_MAX   = 0x2
}CeElXferModeType;

typedef PACKED struct 
{
  void                      *pvBase;
  uint32                     dwLen;
} CEELIovecType;

typedef PACKED struct 
{
  CEELIovecType             *iov;
  uint32                     size;
} CEELIovecListType;

typedef enum
{
  CEEL_DATA_HASH                    = 0x1,
  CEEL_DATA_CIPHER                  = 0x2
}CEELDataType;

typedef enum
{
  CEEL_ERROR_SUCCESS                = 0x0,
  CEEL_ERROR_FAILURE                = 0x1,
  CEEL_ERROR_INVALID_PARAM          = 0x2
} CeELErrorType;

/* the function pointer representing the data transfer function available in the given
 * environment. "direction" determines if the transfer is into the crypto core or out of 
 * the crypto core. */
typedef CeELErrorType (*CeElXferFunctionType) (CEELIovecListType*, CEELIovecListType*, boolean, uint8*, CEELDataType);

/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/

CeELErrorType CeElMemoryBarrier(void);

/*===========================================================================
*
* FUNCTION: CeElInit()
*
* DESCRIPTION:
* This function initializes all the needed dependencies, also making sure
* if they are available. For example, these include DataMover, ClockRegimes,
* Interrupt Handlers, Memory Map
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
CeELErrorType CeElInit(void);

/*===========================================================================
*
* FUNCTION: CeElDenit()
*
* DESCRIPTION:
* This function removes any registrations made with the dependencies
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
CeELErrorType CeElDeinit(void);

/*===========================================================================
*
* FUNCTION: CeElEnableClock()
*
* DESCRIPTION:
* This function turns on needed clocks for ce operation in the given environment
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
CeELErrorType CeElEnableClock(void);

/*===========================================================================
*
* FUNCTION: CeElDisableClock()
*
* DESCRIPTION:
* This function turns off needed clocks for ce operation in the given environment
* 
* DEPENDENCIES:
* 
* RETURN VALUE: TRUE if successful and FALSE otherwise
*
===========================================================================*/
CeELErrorType CeElDisableClock(void);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElmalloc(void** ptr, uint32 preLen);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElfree(void* ptr);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElMemcpy (void* dst, void* src, uint32 len);
  
/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElMemset (void* src, uint32 val, uint32 len);
/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
*  
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElSecMemset (void* src, uint32 val, uint32 len);

/*===========================================================================
*
* FUNCTION: CeElGetXferModeList()
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE:
*
===========================================================================*/
CeELErrorType CeElGetXferModeList(CeElXferModeType* xferMode);

/*===========================================================================
*
* FUNCTION: CeElGetXferfunction()
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE:
*
===========================================================================*/
CeELErrorType CeElGetXferfunction(CeElXferModeType, CeElXferFunctionType* );

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElMutexEnter (void);

/*===========================================================================
*
* FUNCTION: 
*
* DESCRIPTION:
* 
* DEPENDENCIES:
* 
* RETURN VALUE: 
*
===========================================================================*/
CeELErrorType CeElMutexExit (void);
