/**
@file CeCL.c 
@brief Crypto Engine Core Library source file 
*/

/*===========================================================================

                     Crypto Engine Core Library 

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2011 - 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

 $Header: 
 $DateTime: 2015/03/19 01:58:37 $
 $Author: pwbldsvc $ 

when         who     what, where, why
--------     ---     ----------------------------------------------------- 
2012-07-31   nk      Boot version
2012-07-08   nk      Added memory barriers
2012-03-09   yk      Modification for Crypto5
2010-08-21   bm      added support for reg interface xfers
2010-05-24   bm      Initial version
============================================================================*/
#include "CeCL.h"
#include "CeCL_Target.h"
#include "CeEL.h"
#include "CeCL_Env.h"

CeCLErrorType CeCLIOCtlSetHashCntx(CeCLHashAlgoCntxType *ctx_ptr);
CeCLErrorType CeCLIOCtlGetHashCntx(CeCLHashAlgoCntxType *ctx_ptr);
CeCLErrorType CeCLIOCtlHashRegXfer(CeCLHashXferType     *pBufOut);
CeCLErrorType CeCLIOCtlSetCipherCntx(CeCLCipherCntxType *ctx_ptr);
CeCLErrorType CeCLIOCtlGetCipherCntx(CeCLCipherCntxType *ctx_ptr);
CeCLErrorType CeCLIOCtlCipherRegXfer(CeCLCipherXferType *pBufOut);

#define CECL_HW_SEC_ACC_DIN_SIZE 16
#define CECL_HW_SEC_CE_STATUS_DIN_RDY (HWIO_IN(CECL_CE_STATUS) & (1<<CECL_CE_STATUS_DIN_RDY_SHFT))
#define CECL_HW_SEC_CE_STATUS_DOUT_RDY (HWIO_IN(CECL_CE_STATUS) & (1<<CECL_CE_STATUS_DOUT_RDY_SHFT))  

#define CECL_CE_CIPHER_KEY_SIZE_AES128  0x0
#define CECL_CE_CIPHER_KEY_SIZE_AES256  0x2
#define CECL_BLOCK_SIZE_NON_BAM         16
#define CECL_BLOCK_SIZE_BAM             64

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClGetCeVersion(CeCLCeVersionType* CeEngVersion)
{
  if (CeEngVersion == NULL)
  {
    return CECL_ERROR_FAILURE;
  }

  *CeEngVersion = (CeCLCeVersionType) HWIO_INF(CECL_CE_VERSION, MAJ_VER);

  return CECL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClInit(CeCLXferModeType mode)
{
  if (mode == CECL_XFER_MODE_REG)
  {
    //Set the bits that we need in Register mode
    HWIO_OUT(CECL_CE_CONFIG,
         ((0 << HWIO_SHFT(CECL_CE_CONFIG, MASK_ERR_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_OP_DONE_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_DIN_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_DOUT_INTR)) |
          (1 << HWIO_SHFT(CECL_CE_CONFIG, HIGH_SPD_DATA_EN_N)) |
          (1 << HWIO_SHFT(CECL_CE_CONFIG, LITTLE_ENDIAN_MODE))));
  }
  else if (mode == CECL_XFER_MODE_BAM)
  {
    //Set the bits that we need in BAM mode
    HWIO_OUT(CECL_CE_CONFIG,
         ((0 << HWIO_SHFT(CECL_CE_CONFIG, MASK_ERR_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_OP_DONE_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_DIN_INTR)) | 
          (1 << HWIO_SHFT(CECL_CE_CONFIG, MASK_DOUT_INTR)) |
          (0 << HWIO_SHFT(CECL_CE_CONFIG, HIGH_SPD_DATA_EN_N)) |
          (1 << HWIO_SHFT(CECL_CE_CONFIG, PIPE_SET_SELECT)) |
          (1 << HWIO_SHFT(CECL_CE_CONFIG, LITTLE_ENDIAN_MODE))));

    //Set the REQ_SIZE field to 8 beats * 8 = 64 bytes in BAM mode
    HWIO_OUTM(CECL_CE_CONFIG, HWIO_CRYPTO0_CRYPTO_CONFIG_REQ_SIZE_BMSK, 0xE0000); 
  }
  else
  {
    return CECL_ERROR_FAILURE;
  }

  CeElMemoryBarrier();

  return CECL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClDeinit(void)
{
  return CECL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClReset(void)
{
  /* pulling out of reset with default configuration */
  /* Reset is no longer there in a crypto register */
  /* It is controlled at the chip level clock block */

  return CECL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClIOCtlHash (CeCLIoCtlHashType ioCtlVal, 
                         uint8* pBufIn, 
                         uint32 dwLenIn, 
                         uint8* pBufOut, 
                         uint32 dwLenOut, 
                         uint32* pdwActualOut)
{
  CeCLErrorType retVal = CECL_ERROR_SUCCESS;

  switch (ioCtlVal)
  {
    case CECL_IOCTL_HASH_VERSION_NUM:
      if ((pBufOut!= NULL) && (dwLenOut >= CECL_VERSION_NUM_SIZE))
      {
        *pBufOut = 1;
        *pdwActualOut = CECL_VERSION_NUM_SIZE;
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }
      break;

    case CECL_IOCTL_SET_HASH_CNTXT:
      if ((pBufIn!= NULL) && (dwLenIn == sizeof(CeCLHashAlgoCntxType)))
      { 
        retVal = CeCLIOCtlSetHashCntx((CeCLHashAlgoCntxType*) pBufIn);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }
      break;

    case CECL_IOCTL_GET_HASH_CNTXT:
      
      if ((pBufOut!= NULL) && (dwLenOut >= sizeof(CeCLHashAlgoCntxType)))
      { 
        retVal = CeCLIOCtlGetHashCntx ((CeCLHashAlgoCntxType*) pBufOut);
        *pdwActualOut = sizeof(CeCLHashAlgoCntxType);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }      
      break;

    case CECL_IOCTL_HASH_XFER:

      if ((pBufIn!= NULL) && (dwLenIn >= sizeof(CeCLHashXferType)))
      { 
        retVal = CeCLIOCtlHashRegXfer ((CeCLHashXferType*) pBufIn);
        *pdwActualOut = sizeof(CeCLHashXferType);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }      
      break;

    default:
      retVal = CECL_ERROR_FAILURE;
      break;
  }

  return retVal;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClIOCtlCipher (CeCLIoCtlCipherType ioCtlVal, 
                               uint8* pBufIn, 
                               uint32 dwLenIn, 
                               uint8* pBufOut, 
                               uint32 dwLenOut, 
                               uint32* pdwActualOut)
{
  CeCLErrorType retVal = CECL_ERROR_SUCCESS;

  switch (ioCtlVal)
  {
    case CECL_IOCTL_CIPHER_VERSION_NUM:
      if ((pBufOut!= NULL) && (dwLenOut >= CECL_VERSION_NUM_SIZE))
      {
        *pBufOut = 1;
        *pdwActualOut = CECL_VERSION_NUM_SIZE;
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }
      break;

    case CECL_IOCTL_SET_CIPHER_CNTXT:
      if ((pBufIn!= NULL) && (dwLenIn == sizeof(CeCLCipherCntxType)))
      { 
        retVal = CeCLIOCtlSetCipherCntx((CeCLCipherCntxType*) pBufIn);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }      
      break;

    case CECL_IOCTL_GET_CIPHER_CNTXT:
      if ((pBufOut!= NULL) && (dwLenOut >= sizeof(CeCLCipherCntxType)))
      { 
        retVal = CeCLIOCtlGetCipherCntx ((CeCLCipherCntxType*) pBufOut);
        *pdwActualOut = sizeof(CeCLCipherCntxType);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }            
      break;

    case CECL_IOCTL_CIPHER_XFER:
      if ((pBufIn!= NULL) && (dwLenIn >= sizeof(CeCLCipherXferType)))
      { 
        retVal = CeCLIOCtlCipherRegXfer ((CeCLCipherXferType*) pBufIn);
        *pdwActualOut = sizeof(CeCLCipherXferType);
      }
      else
      {
        retVal = CECL_ERROR_FAILURE;
      }            
      break;

    default:
      retVal = CECL_ERROR_FAILURE;
      break;
  }

  return retVal;
}

/**
 * @brief This function sets various SHAx registers 
 *
 * @param
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlSetHashCntx(CeCLHashAlgoCntxType *ctx_ptr)
{
  uint32 i                    = 0;
  uint32 iv_length_in_words   = 0;
  uint32 seg_cfg_val          = 0;
  uint32 temp_len;
  uint32 ce_block_size        = CECL_BLOCK_SIZE_NON_BAM;
  CeCLErrorType retVal        = CECL_ERROR_SUCCESS;

  /* Sanity check inputs */
  if (!ctx_ptr)
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  //Clear encryption and authentication seg config registers
  HWIO_OUT(CECL_CE_ENCR_SEG_CFG, 0);
  CeElMemoryBarrier();

  HWIO_OUT(CECL_CE_AUTH_SEG_CFG, 0);
  CeElMemoryBarrier();

  //If BAM supported set ce block size to multiple of 64
  if(CECL_BAM_IS_SUPPORTED())
  {
    ce_block_size = CECL_BLOCK_SIZE_BAM;
  }

  if (CECL_HASH_ALGO_SHA1 == ctx_ptr->algo)
  {
     seg_cfg_val = CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHA << CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHFT |
                   CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHA1 << CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHFT;

    iv_length_in_words = CECL_SHA1_INIT_VECTOR_SIZE;

  }
  else if (CECL_HASH_ALGO_SHA256 == ctx_ptr->algo)
  {
     seg_cfg_val = CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHA << CECL_CE_AUTH_SEG_CFG_AUTH_ALG_SHFT |
                   CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHA256 << CECL_CE_AUTH_SEG_CFG_AUTH_SIZE_SHFT;

     iv_length_in_words = CECL_SHA256_INIT_VECTOR_SIZE;
  }
  else
  {
     return CECL_ERROR_INVALID_PARAM;
  }

  if (ctx_ptr->lastBlock) 
  {
     seg_cfg_val |= (1 << HWIO_SHFT(CECL_CE_AUTH_SEG_CFG, LAST)); 
  }

  /* Write Initialization Vector */
  for (i=0; i < iv_length_in_words; i++) 
  {
     HWIO_OUTI(CECL_CE_AUTH_IVn, i,   *(ctx_ptr->auth_iv + i));
  }
  CeElMemoryBarrier();

  HWIO_OUT( CECL_CE_AUTH_SEG_CFG,  seg_cfg_val );
  CeElMemoryBarrier();

  /* Write the AUTH_BYTECNT[0,1] registers, only 2 are valid at this time */
  HWIO_OUT(CECL_CE_AUTH_BYTECNT0, (ctx_ptr->auth_bytecnt[0]));
  HWIO_OUT(CECL_CE_AUTH_BYTECNT1, (ctx_ptr->auth_bytecnt[1]));
  CeElMemoryBarrier();

  /* Write the CRYPTO_CE_AUTH_SEG_SIZE register. AUTH_SIZE should be set
   * to buff_size */
  HWIO_OUT(CECL_CE_AUTH_SEG_SIZE, ctx_ptr->dataLn);
  CeElMemoryBarrier();

  /* Set the SEG START to 0 */
  HWIO_OUT(CECL_CE_AUTH_SEG_START, 0);
  CeElMemoryBarrier();

  /* Write the CRYPTO_CE_SEG_SIZE register: Multiple of 16 or 64 bytes depending on BAM
     or no BAM. For 1-pass SHA1, this value should be same as the total length */
  if (ctx_ptr->dataLn % ce_block_size)
  {
    /* Not a multiple of 16/64 bytes */
    temp_len = ctx_ptr->dataLn + (ce_block_size - (ctx_ptr->dataLn % ce_block_size));  
    HWIO_OUT(CECL_CE_SEG_SIZE, temp_len);
  }
  else 
  {
    HWIO_OUT(CECL_CE_SEG_SIZE, ctx_ptr->dataLn);
  }
  CeElMemoryBarrier();

  /* kick-off the crypto operation, the GOPROC is to be set once all the 
   * config registers are set. It has nothing to do with data */
  HWIO_OUT(CECL_CE_GOPROC, CECL_CE_GOPROC_GO_BMSK);
  CeElMemoryBarrier();

  while(1)
  {
    uint32 ce_state = HWIO_INF(CECL_CE_STATUS, CRYPTO_STATE);
    CeElMemoryBarrier();
    //We can change this to detect whether the PROCESSING bit is turned on. Currently
    //we do what the original drivers are doing i.e look for any bit set to indicate it's
    //not in idle state
    if(ce_state)
    {
      break;
    }
  }

  return retVal;
}

/**
 * @brief Get the SHA digest data from the Crypto HW registers
 *
 * @param 
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlGetHashDigest(uint32 *digest_ptr, uint32 digest_len)
{
   uint32 i = 0;
   uint32 ce_status;
   CeCLErrorType retVal = CECL_ERROR_SUCCESS;

   //Sanity check inputs 
   if (!digest_ptr || !digest_len)
   {
     return CECL_ERROR_INVALID_PARAM;
   }

   //Wait for operation done bit before we read HASH digest
   while(1)
   {
     ce_status = HWIO_IN(CECL_CE_STATUS);
     CeElMemoryBarrier();
     if(ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK)
     {
       break;
     }
   }

   //Get HASH digest data from CE registers 
   for(i = 0; i < digest_len / 4; i++) 
   {
     digest_ptr[i] = HWIO_INI(CECL_CE_AUTH_IVn, i);
   }
   CeElMemoryBarrier();

   return retVal;
}

/**
 * @brief Get the SHA context from the Crypto HW registers
 *
 * @param 
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlGetHashCntx(CeCLHashAlgoCntxType *ctx_ptr)
{
   uint32 i = 0;
   CeCLErrorType retVal        = CECL_ERROR_SUCCESS;
   uint32 ce_status;

   /* Sanity check inputs */
   if ((!ctx_ptr) || (((CECL_HASH_ALGO_SHA1 != ctx_ptr->algo)) && 
      ((CECL_HASH_ALGO_SHA256 != ctx_ptr->algo))))
   {
     return CECL_ERROR_INVALID_PARAM;
   }

   //Wait for operation done bit before we read HASH context
   while(1)
   {
     ce_status = HWIO_IN(CECL_CE_STATUS);
     CeElMemoryBarrier();
     if(ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK)
     {
       break;
     }
   }

   ctx_ptr->auth_bytecnt[0] = HWIO_IN(CECL_CE_AUTH_BYTECNT0); 
   ctx_ptr->auth_bytecnt[1] = HWIO_IN(CECL_CE_AUTH_BYTECNT1);
   CeElMemoryBarrier();

   /* Get HASH digest data */
   for(i = 0; i < 8; i++) 
   {
      ctx_ptr->auth_iv[i] = HWIO_INI(CECL_CE_AUTH_IVn, i);
   }
   CeElMemoryBarrier();

   return retVal;
}

/**
 * @brief  This function prepares the command buffer and then 
 *
 * @param ctx_ptr        [in]  Pointer to current context
 * @param buff_ptr       [in]  Pointer to input buffer
 * @param bytes_to_write [in]  Size of the input buffet
 * @param digest_ptr     [out] Pointer to output buffer
 * @param auth_alg       [in]  Algorithm typr 
 *  
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlHashRegXfer(CeCLHashXferType     *pBufOut)
{
  uint32 counter_1 = 0;
  uint32 last_seg_size = 0;
  uint32 segment_count = 0;
  uint8  last_segment[CECL_HW_SEC_ACC_DIN_SIZE];
  uint32 data_write = 0;
  uint8* data_ptr = NULL;
  CeCLErrorType ret_val = CECL_ERROR_FAILURE;
  uint32 is_data_aligned = 0;

  /* Sanity check inputs */
  if ((!pBufOut) || ( !pBufOut->buff_ptr ))
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  data_ptr = pBufOut->buff_ptr;

  segment_count = (pBufOut->buff_len) / CECL_HW_SEC_ACC_DIN_SIZE;
  last_seg_size = (pBufOut->buff_len) % CECL_HW_SEC_ACC_DIN_SIZE;

  //copy the last bytes into last_segment
  if(0 != last_seg_size)
  {
    uint8 i = 0;
    for(i=0; i<last_seg_size; i++)
    {
      last_segment[i] = (uint8)*((pBufOut->buff_ptr+(segment_count * CECL_HW_SEC_ACC_DIN_SIZE)) + i);
    }
  }

  // Check if the data is at an aligned address
  is_data_aligned = (((uint32)(data_ptr) & 0x03) == 0);

  // each segment has 16 bytes which is multiple of 4 bytes that can be copied to CE_DATA_IN
  for (counter_1=0; counter_1<(segment_count*4); counter_1++)
  { 
    while (!CECL_HW_SEC_CE_STATUS_DIN_RDY);

    /* Feed data into DATA_IN. We set crypto engine to little endian mode so don't need to */
    /* reverse the bytes */
    if (is_data_aligned)
    {
      //As the data is aligned at a 32 bit boundry we can just write it to the hw register
      HWIO_OUTI(CECL_CE_DATA_INn, 0, *((uint32*)data_ptr));          
    }
    else
    {
      //Copy the data to an aligned address and then write it to the hw register
      CeElMemcpy(&data_write, data_ptr, 4);  
      HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);    
    }
    CeElMemoryBarrier();

    data_ptr = data_ptr + 4;
  }

  //fill  last segment
  if(0 != last_seg_size)
  {  
    for (counter_1=0; counter_1 < 4; counter_1++)
    {
      while (!CECL_HW_SEC_CE_STATUS_DIN_RDY);

      /* Feed data into DATA_IN */
      CeElMemcpy(&data_write, (uint8*)(last_segment+(counter_1*4)), 4);
      HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);
      CeElMemoryBarrier();
    }
  }

  //wait for the hashing to complete
  while(1)
  {
      uint32 ce_status = HWIO_IN(CECL_CE_STATUS);
      CeElMemoryBarrier();

      if(ce_status & CECL_CE_STATUS_OPERATION_DONE_BMSK)
      {
        break;
      }
  }  

  /* Check for CE HW error */
  if (!(HWIO_IN(CECL_CE_STATUS) & 1))
  {
    ret_val = CECL_ERROR_SUCCESS;
  }
  CeElMemoryBarrier();

  return ret_val;
}

/**
 * @brief This function sets various SHAx registers 
 *
 * @param
 *
 * @return 
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlSetCipherCntx(CeCLCipherCntxType *ctx_ptr)
{
  CeCLErrorType retVal = CECL_ERROR_SUCCESS;
  uint32 seg_cfg_val = 0;
  uint32 temp_len;
  uint32 ce_block_size = CECL_BLOCK_SIZE_NON_BAM;

  /* Sanity check inputs */
  if (!ctx_ptr)
  {
    return CECL_ERROR_INVALID_PARAM;
  }
    
  //Clear encryption and authentication seg config registers
  HWIO_OUT(CECL_CE_ENCR_SEG_CFG, 0);
  CeElMemoryBarrier();

  HWIO_OUT(CECL_CE_AUTH_SEG_CFG, 0);
  CeElMemoryBarrier();

  //If BAM supported set ce block size to multiple of 64
  if(CECL_BAM_IS_SUPPORTED())
  {
    ce_block_size = CECL_BLOCK_SIZE_BAM;
  } 

  seg_cfg_val = ctx_ptr->mode << CECL_CE_ENCR_SEG_CFG_ENCR_MODE_SHFT;

  /* Set algo to AES */
  seg_cfg_val |= CECL_CE_CIPHER_AES_ALGO_VAL << CECL_CE_ENCR_SEG_CFG_ENCR_ALG_SHFT;
    
  /* this bit should be set for encryption and clear otherwise */
  if (ctx_ptr->dir == CECL_CIPHER_ENCRYPT)
  {
    seg_cfg_val |= (1 << CECL_CE_ENCR_SEG_CFG_ENCODE_SHFT);
  }
       
  /* For counter mode operation we need to run the engine in encrypt mode
     the XOR �undoes" the encrypted data into decrypted data.*/
  if (ctx_ptr->mode == CECL_CIPHER_MODE_CTR) 
  {
    seg_cfg_val |= (1 << CECL_CE_ENCR_SEG_CFG_ENCODE_SHFT);
  }
    
  /* Set AES key size */
  if(ctx_ptr->algo == CECL_CIPHER_ALG_AES128)
  {
    seg_cfg_val |= (CECL_CE_CIPHER_KEY_SIZE_AES128 << CECL_CE_ENCR_SEG_CFG_ENCR_KEY_SZ_SHFT);
  }
  else if(ctx_ptr->algo == CECL_CIPHER_ALG_AES256)
  {
    seg_cfg_val |= (CECL_CE_CIPHER_KEY_SIZE_AES256 << CECL_CE_ENCR_SEG_CFG_ENCR_KEY_SZ_SHFT);
  }
    
  /* Use HW key ? */
  if (ctx_ptr->bAESUseHWKey) 
  {     
    seg_cfg_val |= (1 << CECL_CE_ENCR_SEG_CFG_USE_HW_KEY_ENCR_SHFT);
    CECL_SYS_SEC_CRYPTO_CFG();
  }
  else 
  {
    HWIO_OUT( CECL_CE_ENCR_SEG_CFG, seg_cfg_val );
  }

  /* encryption/decryption segment configuration */
  HWIO_OUT(CECL_CE_ENCR_SEG_SIZE, ctx_ptr->dataLn);
  CeElMemoryBarrier();
  HWIO_OUT(CECL_CE_ENCR_SEG_START, 0);
  CeElMemoryBarrier();

  //Write the CRYPTO_CE_SEG_SIZE register: Multiple of 16 or 64 bytes depending on BAM or no BAM 
  if ((ctx_ptr->dataLn) % ce_block_size)
  {
    /* Not a multiple of 16/64 bytes */
    temp_len = ctx_ptr->dataLn + (ce_block_size - (ctx_ptr->dataLn % ce_block_size));  
    HWIO_OUT(CECL_CE_SEG_SIZE, temp_len);
  }
  else 
  {
    HWIO_OUT(CECL_CE_SEG_SIZE, ctx_ptr->dataLn);
  }
  CeElMemoryBarrier();

  if (seg_cfg_val & (CECL_CE_CIPHER_AES_ALGO_VAL << CECL_CE_ENCR_SEG_CFG_ENCR_ALG_SHFT)) 
  {
    HWIO_OUT (CECL_CE_ENCR_CNTR0_IV0, ctx_ptr->iv[0]);
    HWIO_OUT (CECL_CE_ENCR_CNTR1_IV1, ctx_ptr->iv[1]);
    HWIO_OUT (CECL_CE_ENCR_CNTR2_IV2, ctx_ptr->iv[2]);
    HWIO_OUT (CECL_CE_ENCR_CNTR3_IV3, ctx_ptr->iv[3]);
    HWIO_OUT (CECL_CE_ENCR_CNTR_MASK, 0xFFFFFFFF);
    CeElMemoryBarrier();   
     
    // write to aes round key RAM                                                              
    if(!ctx_ptr->bAESUseHWKey) 
    {
      uint32 i = 0;
      for (i=0; i < CECL_AES_MAX_KEY_SIZE/4; i++) 
      {
        HWIO_OUTI(CECL_CE_ENCR_KEYn, i, ctx_ptr->aes_key[i]);
      }      
      CeElMemoryBarrier();
    }
  }
 
  /* kick-off the crypto operation, the GOPROC is to be set once all the 
   * config registers are set. It has nothing to do with data */
  if (ctx_ptr->bAESUseHWKey)       
  {    
    CECL_SYS_SEC_CRYPTO_GOPROC_OEM_KEY();
  }
  else 
  {
    HWIO_OUT(CECL_CE_GOPROC, CECL_CE_GOPROC_GO_BMSK);
  }
  CeElMemoryBarrier();
   
  while(1)
  {
    uint32 ce_state = HWIO_INF(CECL_CE_STATUS, CRYPTO_STATE);
    CeElMemoryBarrier();
    //We can change this to detect whether the PROCESSING bit is turned on. Currently
    //we do what the original drivers are doing i.e look for any bit set to indicate it's
    //not in idle state
    if(ce_state)
    {
      break;
    }
  }
 
  return retVal;
}
    
/** 
 * @brief Get the AES context from the HW registers
 *
 * @param ctx_ptr    [in] Pointer to current context
 * @param auth_alg   [in] Algorithm type 
 *
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlGetCipherCntx(CeCLCipherCntxType *ctx_ptr)
{
  CeCLErrorType retVal = CECL_ERROR_SUCCESS;
 
  /* Sanity check inputs */
  if (!ctx_ptr)
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  ctx_ptr->iv[0] = HWIO_IN(CECL_CE_ENCR_CNTR0_IV0);
  ctx_ptr->iv[1] = HWIO_IN(CECL_CE_ENCR_CNTR1_IV1);
  ctx_ptr->iv[2] = HWIO_IN(CECL_CE_ENCR_CNTR2_IV2);
  ctx_ptr->iv[3] = HWIO_IN(CECL_CE_ENCR_CNTR3_IV3);
  CeElMemoryBarrier(); 

  return retVal;
}

/**
 * @brief  This function prepares the command buffer and then 
 *
 * @param ctx_ptr        [in]  Pointer to current context
 * @param buff_ptr       [in]  Pointer to input buffer
 * @param bytes_to_write [in]  Size of the input buffet
 * @param digest_ptr     [out] Pointer to output buffer
 * @param auth_alg       [in]  Algorithm typr 
 *  
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeCLErrorType CeCLIOCtlCipherRegXfer(CeCLCipherXferType *pBufOut)
{
  uint32 counter_1 = 0;
  uint32 counter_2 = 0;
  uint32 counter_3 = 0;
  uint32 segment_count=0;  
  CeCLErrorType ret_val = CECL_ERROR_FAILURE;
  uint8* data_in = pBufOut->pDataIn;
  uint8* data_out = pBufOut->pDataOut;  
  uint32 data_write = 0;
  uint32 is_data_aligned = 0;  

  /* Sanity check inputs */
  if (!pBufOut->pDataIn || !pBufOut->nDataLen || !pBufOut->pDataOut)
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  is_data_aligned = (((uint32)(data_in) & 0x03) == 0);
  segment_count = (pBufOut->nDataLen  / CECL_HW_SEC_ACC_DIN_SIZE);

  for (counter_1=0; counter_1<segment_count;counter_1++)
  {
    /* feed data in 4 bytes at a time */

    for (counter_2=0; counter_2<4; counter_2++)
    {
      while (!CECL_HW_SEC_CE_STATUS_DIN_RDY); 

      if (is_data_aligned)
      {
        //As the data is aligned at a 32 bit boundry we can just write it to the hw register
        HWIO_OUTI(CECL_CE_DATA_INn, 0, *((uint32*)data_in));          
      }
      else
      {
        //Copy the data to an aligned address and then write it to the hw register
        CeElMemcpy(&data_write, data_in, 4);  
        HWIO_OUTI(CECL_CE_DATA_INn, 0, data_write);    
      }	  
      CeElMemoryBarrier();

      data_in += 4;
    }

    /* store data out 4 bytes at a time */
    for (counter_3=0; counter_3<4; counter_3++)
    {
      while (!CECL_HW_SEC_CE_STATUS_DOUT_RDY); 

      *((uint32*)data_out) = HWIO_INI(CECL_CE_DATA_OUTn, counter_3);
      CeElMemoryBarrier();
      data_out += 4;
    }
  }

  if (!((HWIO_IN(CECL_CE_STATUS) & 1)))
  {
    ret_val = CECL_ERROR_SUCCESS;
  }

  CeElMemoryBarrier();

  return ret_val;
}

/**
 * @brief This function reverses the bytes for each four byte 
 *        set in the input data
 *
 * @param out_bytes_ptr  [out] Pointer to output data
 * @param in_bytes_ptr   [in]  Pointer to input data
 * @param byte_count     [in]  Length of data in bytes. 
 *
 * @return CE_Result_Type
 *
 * @see 
 *
 */
CeCLErrorType CeCLReverseBytes(uint8 *out_bytes_ptr, 
                               uint8 *in_bytes_ptr, 
                               uint32 byte_count)
{
  uint32 n = 0;
  uint8  tmp_byte = 0;

  /* Sanity check inputs */
  if (!out_bytes_ptr || !in_bytes_ptr || !byte_count)
  {
    return CECL_ERROR_INVALID_PARAM;
  }

  for (n = 0; n < byte_count / 4; n++) 
  {
     //swap first byte and fourth byte
     tmp_byte = in_bytes_ptr[4*n + 0];
     out_bytes_ptr[4*n + 0] =  in_bytes_ptr[4*n + 3];
     out_bytes_ptr[4*n + 3] =  tmp_byte;

     //swap second byte and third byte
     tmp_byte = in_bytes_ptr[4*n + 1];
     out_bytes_ptr[4*n + 1] =  in_bytes_ptr[4*n + 2];
     out_bytes_ptr[4*n + 2] =  tmp_byte;
  }

  return CECL_ERROR_SUCCESS;
}

