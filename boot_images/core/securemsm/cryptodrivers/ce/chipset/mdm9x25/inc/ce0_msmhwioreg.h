#ifndef __CE0_MSMHWIOREG_H__
#define __CE0_MSMHWIOREG_H__
/*
===========================================================================
*/
/**
  @file ce0_msmhwioreg.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    CRYPTO0_CRYPTO
    CRYPTO0_CRYPTO_AXI_VMIDMT_AXI
    CRYPTO0_CRYPTO_BAM
    CRYPTO0_CRYPTO_BAM_VMIDMT_BAM
    CRYPTO0_CRYPTO_BAM_XPU2_BAM

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/chipset/mdm9x25/inc/ce0_msmhwioreg.h#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  ===========================================================================
*/
#include "msmhwiobase.h"

//#define CRYPTO0_CRYPTO_TOP_BASE              CRYPTO0_ELAN_SYSNOC_CRYPTO_WRAPPER_BASE
//#define CRYPTO0_CRYPTO_TOP_BASE_PHYS         CRYPTO0_ELAN_SYSNOC_CRYPTO_WRAPPER_BASE_PHYS

/*----------------------------------------------------------------------------
 * MODULE: CRYPTO0_CRYPTO
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_REG_BASE                                                        (CRYPTO0_CRYPTO_TOP_BASE      + 0x0001a000)
#define CRYPTO0_CRYPTO_REG_BASE_PHYS                                                   (CRYPTO0_CRYPTO_TOP_BASE_PHYS + 0x0001a000)

#define HWIO_CRYPTO0_CRYPTO_VERSION_ADDR                                               (CRYPTO0_CRYPTO_REG_BASE      + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_VERSION_PHYS                                               (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_VERSION_RMSK                                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_VERSION_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_VERSION_ADDR, HWIO_CRYPTO0_CRYPTO_VERSION_RMSK)
#define HWIO_CRYPTO0_CRYPTO_VERSION_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_VERSION_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_VERSION_MAJ_VER_BMSK                                       0xff000000
#define HWIO_CRYPTO0_CRYPTO_VERSION_MAJ_VER_SHFT                                             0x18
#define HWIO_CRYPTO0_CRYPTO_VERSION_MIN_VER_BMSK                                         0xff0000
#define HWIO_CRYPTO0_CRYPTO_VERSION_MIN_VER_SHFT                                             0x10
#define HWIO_CRYPTO0_CRYPTO_VERSION_STEP_VER_BMSK                                          0xffff
#define HWIO_CRYPTO0_CRYPTO_VERSION_STEP_VER_SHFT                                             0x0

#define HWIO_CRYPTO0_CRYPTO_DATA_INn_ADDR(n)                                           (CRYPTO0_CRYPTO_REG_BASE      + 0x00000010 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_DATA_INn_PHYS(n)                                           (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000010 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_DATA_INn_RMSK                                              0xffffffff
#define HWIO_CRYPTO0_CRYPTO_DATA_INn_MAXn                                                       3
#define HWIO_CRYPTO0_CRYPTO_DATA_INn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_DATA_INn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_DATA_INn_DATA_IN_BMSK                                      0xffffffff
#define HWIO_CRYPTO0_CRYPTO_DATA_INn_DATA_IN_SHFT                                             0x0

#define HWIO_CRYPTO0_CRYPTO_DATA_OUTn_ADDR(n)                                          (CRYPTO0_CRYPTO_REG_BASE      + 0x00000020 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_DATA_OUTn_PHYS(n)                                          (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000020 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_DATA_OUTn_RMSK                                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_DATA_OUTn_MAXn                                                      3
#define HWIO_CRYPTO0_CRYPTO_DATA_OUTn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_DATA_OUTn_ADDR(n), HWIO_CRYPTO0_CRYPTO_DATA_OUTn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_DATA_OUTn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_DATA_OUTn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_DATA_OUTn_DATA_OUT_BMSK                                    0xffffffff
#define HWIO_CRYPTO0_CRYPTO_DATA_OUTn_DATA_OUT_SHFT                                           0x0

#define HWIO_CRYPTO0_CRYPTO_STATUS_ADDR                                                (CRYPTO0_CRYPTO_REG_BASE      + 0x00000100)
#define HWIO_CRYPTO0_CRYPTO_STATUS_PHYS                                                (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000100)
#define HWIO_CRYPTO0_CRYPTO_STATUS_RMSK                                                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_STATUS_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_STATUS_ADDR, HWIO_CRYPTO0_CRYPTO_STATUS_RMSK)
#define HWIO_CRYPTO0_CRYPTO_STATUS_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_STATUS_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_STATUS_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_STATUS_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_STATUS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_STATUS_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_STATUS_IN)
#define HWIO_CRYPTO0_CRYPTO_STATUS_MAC_FAILED_BMSK                                     0x80000000
#define HWIO_CRYPTO0_CRYPTO_STATUS_MAC_FAILED_SHFT                                           0x1f
#define HWIO_CRYPTO0_CRYPTO_STATUS_DOUT_SIZE_AVAIL_BMSK                                0x7c000000
#define HWIO_CRYPTO0_CRYPTO_STATUS_DOUT_SIZE_AVAIL_SHFT                                      0x1a
#define HWIO_CRYPTO0_CRYPTO_STATUS_DIN_SIZE_AVAIL_BMSK                                  0x3e00000
#define HWIO_CRYPTO0_CRYPTO_STATUS_DIN_SIZE_AVAIL_SHFT                                       0x15
#define HWIO_CRYPTO0_CRYPTO_STATUS_HSD_ERR_BMSK                                          0x100000
#define HWIO_CRYPTO0_CRYPTO_STATUS_HSD_ERR_SHFT                                              0x14
#define HWIO_CRYPTO0_CRYPTO_STATUS_ACCESS_VIOL_BMSK                                       0x80000
#define HWIO_CRYPTO0_CRYPTO_STATUS_ACCESS_VIOL_SHFT                                          0x13
#define HWIO_CRYPTO0_CRYPTO_STATUS_PIPE_ACTIVE_ERR_BMSK                                   0x40000
#define HWIO_CRYPTO0_CRYPTO_STATUS_PIPE_ACTIVE_ERR_SHFT                                      0x12
#define HWIO_CRYPTO0_CRYPTO_STATUS_CFG_CHNG_ERR_BMSK                                      0x20000
#define HWIO_CRYPTO0_CRYPTO_STATUS_CFG_CHNG_ERR_SHFT                                         0x11
#define HWIO_CRYPTO0_CRYPTO_STATUS_DOUT_ERR_BMSK                                          0x10000
#define HWIO_CRYPTO0_CRYPTO_STATUS_DOUT_ERR_SHFT                                             0x10
#define HWIO_CRYPTO0_CRYPTO_STATUS_DIN_ERR_BMSK                                            0x8000
#define HWIO_CRYPTO0_CRYPTO_STATUS_DIN_ERR_SHFT                                               0xf
#define HWIO_CRYPTO0_CRYPTO_STATUS_AXI_ERR_BMSK                                            0x4000
#define HWIO_CRYPTO0_CRYPTO_STATUS_AXI_ERR_SHFT                                               0xe
#define HWIO_CRYPTO0_CRYPTO_STATUS_CRYPTO_STATE_BMSK                                       0x3c00
#define HWIO_CRYPTO0_CRYPTO_STATUS_CRYPTO_STATE_SHFT                                          0xa
#define HWIO_CRYPTO0_CRYPTO_STATUS_ENCR_BUSY_BMSK                                           0x200
#define HWIO_CRYPTO0_CRYPTO_STATUS_ENCR_BUSY_SHFT                                             0x9
#define HWIO_CRYPTO0_CRYPTO_STATUS_AUTH_BUSY_BMSK                                           0x100
#define HWIO_CRYPTO0_CRYPTO_STATUS_AUTH_BUSY_SHFT                                             0x8
#define HWIO_CRYPTO0_CRYPTO_STATUS_DOUT_INTR_BMSK                                            0x80
#define HWIO_CRYPTO0_CRYPTO_STATUS_DOUT_INTR_SHFT                                             0x7
#define HWIO_CRYPTO0_CRYPTO_STATUS_DIN_INTR_BMSK                                             0x40
#define HWIO_CRYPTO0_CRYPTO_STATUS_DIN_INTR_SHFT                                              0x6
#define HWIO_CRYPTO0_CRYPTO_STATUS_OP_DONE_INTR_BMSK                                         0x20
#define HWIO_CRYPTO0_CRYPTO_STATUS_OP_DONE_INTR_SHFT                                          0x5
#define HWIO_CRYPTO0_CRYPTO_STATUS_ERR_INTR_BMSK                                             0x10
#define HWIO_CRYPTO0_CRYPTO_STATUS_ERR_INTR_SHFT                                              0x4
#define HWIO_CRYPTO0_CRYPTO_STATUS_DOUT_RDY_BMSK                                              0x8
#define HWIO_CRYPTO0_CRYPTO_STATUS_DOUT_RDY_SHFT                                              0x3
#define HWIO_CRYPTO0_CRYPTO_STATUS_DIN_RDY_BMSK                                               0x4
#define HWIO_CRYPTO0_CRYPTO_STATUS_DIN_RDY_SHFT                                               0x2
#define HWIO_CRYPTO0_CRYPTO_STATUS_OPERATION_DONE_BMSK                                        0x2
#define HWIO_CRYPTO0_CRYPTO_STATUS_OPERATION_DONE_SHFT                                        0x1
#define HWIO_CRYPTO0_CRYPTO_STATUS_SW_ERR_BMSK                                                0x1
#define HWIO_CRYPTO0_CRYPTO_STATUS_SW_ERR_SHFT                                                0x0

#define HWIO_CRYPTO0_CRYPTO_STATUS2_ADDR                                               (CRYPTO0_CRYPTO_REG_BASE      + 0x00000104)
#define HWIO_CRYPTO0_CRYPTO_STATUS2_PHYS                                               (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000104)
#define HWIO_CRYPTO0_CRYPTO_STATUS2_RMSK                                                      0x3
#define HWIO_CRYPTO0_CRYPTO_STATUS2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_STATUS2_ADDR, HWIO_CRYPTO0_CRYPTO_STATUS2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_STATUS2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_STATUS2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_STATUS2_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_STATUS2_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_STATUS2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_STATUS2_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_STATUS2_IN)
#define HWIO_CRYPTO0_CRYPTO_STATUS2_AXI_EXTRA_BMSK                                            0x2
#define HWIO_CRYPTO0_CRYPTO_STATUS2_AXI_EXTRA_SHFT                                            0x1
#define HWIO_CRYPTO0_CRYPTO_STATUS2_LOCKED_BMSK                                               0x1
#define HWIO_CRYPTO0_CRYPTO_STATUS2_LOCKED_SHFT                                               0x0

#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ADDR                                         (CRYPTO0_CRYPTO_REG_BASE      + 0x00000108)
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_PHYS                                         (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000108)
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_RMSK                                          0x1ffffff
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ADDR, HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_MAX_AXI_RD_BEATS_BMSK                         0x1f80000
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_MAX_AXI_RD_BEATS_SHFT                              0x13
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_MAX_AXI_WR_BEATS_BMSK                           0x7e000
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_MAX_AXI_WR_BEATS_SHFT                               0xd
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_NUM_BAM_PIPE_SETS_BMSK                           0x1e00
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_NUM_BAM_PIPE_SETS_SHFT                              0x9
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_KASUMI_SEL_BMSK                              0x100
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_KASUMI_SEL_SHFT                                0x8
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_SNOW3G_SEL_BMSK                               0x80
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_SNOW3G_SEL_SHFT                                0x7
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_AES_SEL_BMSK                                  0x40
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_AES_SEL_SHFT                                   0x6
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_SHA512_SEL_BMSK                               0x20
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_SHA512_SEL_SHFT                                0x5
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_SHA_SEL_BMSK                                  0x10
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_AUTH_SHA_SEL_SHFT                                   0x4
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ENCR_KASUMI_SEL_BMSK                                0x8
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ENCR_KASUMI_SEL_SHFT                                0x3
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ENCR_SNOW3G_SEL_BMSK                                0x4
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ENCR_SNOW3G_SEL_SHFT                                0x2
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ENCR_DES_SEL_BMSK                                   0x2
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ENCR_DES_SEL_SHFT                                   0x1
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ENCR_AES_SEL_BMSK                                   0x1
#define HWIO_CRYPTO0_CRYPTO_ENGINES_AVAIL_ENCR_AES_SEL_SHFT                                   0x0

#define HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_ADDR                                            (CRYPTO0_CRYPTO_REG_BASE      + 0x0000010c)
#define HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_PHYS                                            (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x0000010c)
#define HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_RMSK                                                0xffff
#define HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_ADDR, HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_RMSK)
#define HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_ENG_DOUT_FIFO_DEPTH_BMSK                            0xff00
#define HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_ENG_DOUT_FIFO_DEPTH_SHFT                               0x8
#define HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_ENG_DIN_FIFO_DEPTH_BMSK                               0xff
#define HWIO_CRYPTO0_CRYPTO_FIFO_SIZES_ENG_DIN_FIFO_DEPTH_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_SEG_SIZE_ADDR                                              (CRYPTO0_CRYPTO_REG_BASE      + 0x00000110)
#define HWIO_CRYPTO0_CRYPTO_SEG_SIZE_PHYS                                              (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000110)
#define HWIO_CRYPTO0_CRYPTO_SEG_SIZE_RMSK                                              0xffffffff
#define HWIO_CRYPTO0_CRYPTO_SEG_SIZE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_SEG_SIZE_ADDR, HWIO_CRYPTO0_CRYPTO_SEG_SIZE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_SEG_SIZE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_SEG_SIZE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_SEG_SIZE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_SEG_SIZE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_SEG_SIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_SEG_SIZE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_SEG_SIZE_IN)
#define HWIO_CRYPTO0_CRYPTO_SEG_SIZE_SEG_SIZE_BMSK                                     0xffffffff
#define HWIO_CRYPTO0_CRYPTO_SEG_SIZE_SEG_SIZE_SHFT                                            0x0

#define HWIO_CRYPTO0_CRYPTO_GOPROC_ADDR                                                (CRYPTO0_CRYPTO_REG_BASE      + 0x00000120)
#define HWIO_CRYPTO0_CRYPTO_GOPROC_PHYS                                                (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000120)
#define HWIO_CRYPTO0_CRYPTO_GOPROC_RMSK                                                       0x7
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_GOPROC_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_GOPROC_RESULTS_DUMP_BMSK                                          0x4
#define HWIO_CRYPTO0_CRYPTO_GOPROC_RESULTS_DUMP_SHFT                                          0x2
#define HWIO_CRYPTO0_CRYPTO_GOPROC_CLR_CNTXT_BMSK                                             0x2
#define HWIO_CRYPTO0_CRYPTO_GOPROC_CLR_CNTXT_SHFT                                             0x1
#define HWIO_CRYPTO0_CRYPTO_GOPROC_GO_BMSK                                                    0x1
#define HWIO_CRYPTO0_CRYPTO_GOPROC_GO_SHFT                                                    0x0

#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_ADDR                                         (CRYPTO0_CRYPTO_REG_BASE      + 0x00001000)
#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_PHYS                                         (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00001000)
#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_RMSK                                                0x7
#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_RESULTS_DUMP_BMSK                                   0x4
#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_RESULTS_DUMP_SHFT                                   0x2
#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_CLR_CNTXT_BMSK                                      0x2
#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_CLR_CNTXT_SHFT                                      0x1
#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_GO_BMSK                                             0x1
#define HWIO_CRYPTO0_CRYPTO_GOPROC_QC_KEY_GO_SHFT                                             0x0

#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_ADDR                                        (CRYPTO0_CRYPTO_REG_BASE      + 0x00002000)
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_PHYS                                        (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00002000)
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_RMSK                                               0x7
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_RESULTS_DUMP_BMSK                                  0x4
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_RESULTS_DUMP_SHFT                                  0x2
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_CLR_CNTXT_BMSK                                     0x2
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_CLR_CNTXT_SHFT                                     0x1
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_GO_BMSK                                            0x1
#define HWIO_CRYPTO0_CRYPTO_GOPROC_OEM_KEY_GO_SHFT                                            0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ADDR                                          (CRYPTO0_CRYPTO_REG_BASE      + 0x00000200)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_PHYS                                          (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000200)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_RMSK                                             0x3ffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_KEYSTREAM_ENABLE_BMSK                            0x20000
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_KEYSTREAM_ENABLE_SHFT                               0x11
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_F8_DIRECTION_BMSK                                0x10000
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_F8_DIRECTION_SHFT                                   0x10
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_USE_PIPE_KEY_ENCR_BMSK                            0x8000
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_USE_PIPE_KEY_ENCR_SHFT                               0xf
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_USE_HW_KEY_ENCR_BMSK                              0x4000
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_USE_HW_KEY_ENCR_SHFT                                 0xe
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_LAST_BMSK                                         0x2000
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_LAST_SHFT                                            0xd
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_CNTR_ALG_BMSK                                     0x1800
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_CNTR_ALG_SHFT                                        0xb
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ENCODE_BMSK                                        0x400
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ENCODE_SHFT                                          0xa
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ENCR_MODE_BMSK                                     0x3c0
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ENCR_MODE_SHFT                                       0x6
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ENCR_KEY_SZ_BMSK                                    0x38
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ENCR_KEY_SZ_SHFT                                     0x3
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ENCR_ALG_BMSK                                        0x7
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_CFG_ENCR_ALG_SHFT                                        0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_ADDR                                         (CRYPTO0_CRYPTO_REG_BASE      + 0x00000204)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_PHYS                                         (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000204)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_RMSK                                         0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_ENCR_SIZE_BMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_SIZE_ENCR_SIZE_SHFT                                      0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_ADDR                                        (CRYPTO0_CRYPTO_REG_BASE      + 0x00000208)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_PHYS                                        (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000208)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_RMSK                                        0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_ENCR_START_BMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_SEG_START_ENCR_START_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_KEYn_ADDR(n)                                          (CRYPTO0_CRYPTO_REG_BASE      + 0x00003000 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_KEYn_PHYS(n)                                          (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00003000 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_KEYn_RMSK                                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_KEYn_MAXn                                                      7
#define HWIO_CRYPTO0_CRYPTO_ENCR_KEYn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_KEYn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_ENCR_KEYn_CRYPTO_ENCR_KEY_BMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_KEYn_CRYPTO_ENCR_KEY_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_KEYn_ADDR(m,n)                                  (CRYPTO0_CRYPTO_REG_BASE      + 0x00004000 + 0x20 * (m)+0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_KEYn_PHYS(m,n)                                  (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00004000 + 0x20 * (m)+0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_KEYn_RMSK                                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_KEYn_MAXm                                                3
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_KEYn_MAXn                                                7
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_KEYn_OUTI2(m,n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_KEYn_ADDR(m,n),val)
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_KEYn_CRYPTO_ENCR_PIPE_KEY_BMSK                  0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_KEYn_CRYPTO_ENCR_PIPE_KEY_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_KEYn_ADDR(n)                                      (CRYPTO0_CRYPTO_REG_BASE      + 0x00003020 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_KEYn_PHYS(n)                                      (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00003020 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_KEYn_RMSK                                         0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_KEYn_MAXn                                                  7
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_KEYn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_XTS_KEYn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_KEYn_CRYPTO_ENCR_XTS_KEY_BMSK                     0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_KEYn_CRYPTO_ENCR_XTS_KEY_SHFT                            0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_XTS_KEYn_ADDR(m,n)                              (CRYPTO0_CRYPTO_REG_BASE      + 0x00004200 + 0x20 * (m)+0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_XTS_KEYn_PHYS(m,n)                              (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00004200 + 0x20 * (m)+0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_XTS_KEYn_RMSK                                   0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_XTS_KEYn_MAXm                                            3
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_XTS_KEYn_MAXn                                            7
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_XTS_KEYn_OUTI2(m,n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_XTS_KEYn_ADDR(m,n),val)
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_XTS_KEYn_CRYPTO_ENCR_PIPE_XTS_KEY_BMSK          0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_PIPEm_XTS_KEYn_CRYPTO_ENCR_PIPE_XTS_KEY_SHFT                 0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_ADDR                                        (CRYPTO0_CRYPTO_REG_BASE      + 0x0000020c)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_PHYS                                        (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x0000020c)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_RMSK                                        0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_CRYPTO_CNTR0_IV0_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR0_IV0_CRYPTO_CNTR0_IV0_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_ADDR                                        (CRYPTO0_CRYPTO_REG_BASE      + 0x00000210)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_PHYS                                        (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000210)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_RMSK                                        0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_CRYPTO_CNTR1_IV1_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR1_IV1_CRYPTO_CNTR1_IV1_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_ADDR                                        (CRYPTO0_CRYPTO_REG_BASE      + 0x00000214)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_PHYS                                        (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000214)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_RMSK                                        0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_CRYPTO_CNTR2_IV2_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR2_IV2_CRYPTO_CNTR2_IV2_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_ADDR                                        (CRYPTO0_CRYPTO_REG_BASE      + 0x00000218)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_PHYS                                        (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000218)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_RMSK                                        0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_CRYPTO_CNTR3_IV3_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR3_IV3_CRYPTO_CNTR3_IV3_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_ADDR                                        (CRYPTO0_CRYPTO_REG_BASE      + 0x0000021c)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_PHYS                                        (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x0000021c)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_RMSK                                        0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_CNTR_MASK_BMSK                              0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK_CNTR_MASK_SHFT                                     0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_ADDR                                       (CRYPTO0_CRYPTO_REG_BASE      + 0x00000234)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_RMSK                                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_CNTR_MASK_BMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK2_CNTR_MASK_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_ADDR                                       (CRYPTO0_CRYPTO_REG_BASE      + 0x00000238)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_RMSK                                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_CNTR_MASK_BMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK1_CNTR_MASK_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_ADDR                                       (CRYPTO0_CRYPTO_REG_BASE      + 0x0000023c)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_RMSK                                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_CNTR_MASK_BMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CNTR_MASK0_CNTR_MASK_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_ADDR(n)                                (CRYPTO0_CRYPTO_REG_BASE      + 0x00000220 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_PHYS(n)                                (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000220 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_RMSK                                   0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_MAXn                                            3
#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_CCM_INIT_CNTR_BMSK                     0xffffffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_CCM_INIT_CNTRn_CCM_INIT_CNTR_SHFT                            0x0

#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_ADDR                                      (CRYPTO0_CRYPTO_REG_BASE      + 0x00000230)
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_PHYS                                      (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000230)
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_RMSK                                         0xfffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_ADDR, HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_IN)
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_DU_SIZE_BMSK                                 0xfffff
#define HWIO_CRYPTO0_CRYPTO_ENCR_XTS_DU_SIZE_DU_SIZE_SHFT                                     0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_ADDR                                          (CRYPTO0_CRYPTO_REG_BASE      + 0x00000300)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_PHYS                                          (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000300)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_RMSK                                           0x1ffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_ADDR, HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_IN)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_COMP_EXP_MAC_BMSK                              0x1000000
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_COMP_EXP_MAC_SHFT                                   0x18
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_F9_DIRECTION_BMSK                               0x800000
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_F9_DIRECTION_SHFT                                   0x17
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_NONCE_NUM_WORDS_BMSK                       0x700000
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_NONCE_NUM_WORDS_SHFT                           0x14
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_USE_PIPE_KEY_AUTH_BMSK                           0x80000
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_USE_PIPE_KEY_AUTH_SHFT                              0x13
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_USE_HW_KEY_AUTH_BMSK                             0x40000
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_USE_HW_KEY_AUTH_SHFT                                0x12
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_FIRST_BMSK                                       0x20000
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_FIRST_SHFT                                          0x11
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_LAST_BMSK                                        0x10000
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_LAST_SHFT                                           0x10
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_POS_BMSK                                     0xc000
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_POS_SHFT                                        0xe
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_SIZE_BMSK                                    0x3e00
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_SIZE_SHFT                                       0x9
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_MODE_BMSK                                     0x1c0
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_MODE_SHFT                                       0x6
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_KEY_SZ_BMSK                                    0x38
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_KEY_SZ_SHFT                                     0x3
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_ALG_BMSK                                        0x7
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_CFG_AUTH_ALG_SHFT                                        0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_ADDR                                         (CRYPTO0_CRYPTO_REG_BASE      + 0x00000304)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_PHYS                                         (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000304)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_RMSK                                         0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_ADDR, HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_IN)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_AUTH_SIZE_BMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_SIZE_AUTH_SIZE_SHFT                                      0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_ADDR                                        (CRYPTO0_CRYPTO_REG_BASE      + 0x00000308)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_PHYS                                        (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000308)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_RMSK                                        0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_ADDR, HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_IN)
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_AUTH_START_BMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_SEG_START_AUTH_START_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_KEYn_ADDR(n)                                          (CRYPTO0_CRYPTO_REG_BASE      + 0x00003040 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_KEYn_PHYS(n)                                          (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00003040 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_KEYn_RMSK                                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_KEYn_MAXn                                                     15
#define HWIO_CRYPTO0_CRYPTO_AUTH_KEYn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_KEYn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_AUTH_KEYn_CRYPTO_AUTH_KEY_BMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_KEYn_CRYPTO_AUTH_KEY_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_PIPEm_KEYn_ADDR(m,n)                                  (CRYPTO0_CRYPTO_REG_BASE      + 0x00004800 + 0x80 * (m)+0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_PIPEm_KEYn_PHYS(m,n)                                  (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00004800 + 0x80 * (m)+0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_PIPEm_KEYn_RMSK                                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_PIPEm_KEYn_MAXm                                                3
#define HWIO_CRYPTO0_CRYPTO_AUTH_PIPEm_KEYn_MAXn                                               15
#define HWIO_CRYPTO0_CRYPTO_AUTH_PIPEm_KEYn_OUTI2(m,n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_PIPEm_KEYn_ADDR(m,n),val)
#define HWIO_CRYPTO0_CRYPTO_AUTH_PIPEm_KEYn_CRYPTO_AUTH_PIPE_KEY_BMSK                  0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_PIPEm_KEYn_CRYPTO_AUTH_PIPE_KEY_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_ADDR(n)                                           (CRYPTO0_CRYPTO_REG_BASE      + 0x00000310 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_PHYS(n)                                           (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000310 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_RMSK                                              0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_MAXn                                                       7
#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_IVn_ADDR(n), HWIO_CRYPTO0_CRYPTO_AUTH_IVn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_IVn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_IVn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AUTH_IVn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_AUTH_IVn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_AUTH_IVN_BMSK                                     0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_IVn_AUTH_IVN_SHFT                                            0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_ADDR(n)                                   (CRYPTO0_CRYPTO_REG_BASE      + 0x00000350 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_PHYS(n)                                   (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000350 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_RMSK                                      0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_MAXn                                               3
#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_ADDR(n), HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_CTRL_INFO_NONCE_BMSK                      0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_INFO_NONCEn_CTRL_INFO_NONCE_SHFT                             0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_ADDR                                         (CRYPTO0_CRYPTO_REG_BASE      + 0x00000390)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_PHYS                                         (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000390)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_RMSK                                         0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_ADDR, HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_IN)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_AUTH_BYTECNT0_BMSK                           0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT0_AUTH_BYTECNT0_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_ADDR                                         (CRYPTO0_CRYPTO_REG_BASE      + 0x00000394)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_PHYS                                         (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000394)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_RMSK                                         0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_ADDR, HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_IN)
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_AUTH_BYTECNT1_BMSK                           0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_BYTECNT1_AUTH_BYTECNT1_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_ADDR(n)                                      (CRYPTO0_CRYPTO_REG_BASE      + 0x000003a0 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_PHYS(n)                                      (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x000003a0 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_RMSK                                         0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_MAXn                                                  7
#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_ADDR(n), HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_EXP_MAC_BMSK                                 0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AUTH_EXP_MACn_EXP_MAC_SHFT                                        0x0

#define HWIO_CRYPTO0_CRYPTO_CONFIG_ADDR                                                (CRYPTO0_CRYPTO_REG_BASE      + 0x00000400)
#define HWIO_CRYPTO0_CRYPTO_CONFIG_PHYS                                                (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00000400)
#define HWIO_CRYPTO0_CRYPTO_CONFIG_RMSK                                                  0x1fffff
#define HWIO_CRYPTO0_CRYPTO_CONFIG_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_CONFIG_ADDR, HWIO_CRYPTO0_CRYPTO_CONFIG_RMSK)
#define HWIO_CRYPTO0_CRYPTO_CONFIG_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_CONFIG_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_CONFIG_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_CONFIG_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_CONFIG_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_CONFIG_IN)
#define HWIO_CRYPTO0_CRYPTO_CONFIG_REQ_SIZE_BMSK                                         0x1e0000
#define HWIO_CRYPTO0_CRYPTO_CONFIG_REQ_SIZE_SHFT                                             0x11
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MAX_QUEUED_REQS_BMSK                                   0x1c000
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MAX_QUEUED_REQS_SHFT                                       0xe
#define HWIO_CRYPTO0_CRYPTO_CONFIG_IRQ_ENABLE_BMSK                                         0x3c00
#define HWIO_CRYPTO0_CRYPTO_CONFIG_IRQ_ENABLE_SHFT                                            0xa
#define HWIO_CRYPTO0_CRYPTO_CONFIG_LITTLE_ENDIAN_MODE_BMSK                                  0x200
#define HWIO_CRYPTO0_CRYPTO_CONFIG_LITTLE_ENDIAN_MODE_SHFT                                    0x9
#define HWIO_CRYPTO0_CRYPTO_CONFIG_PIPE_SET_SELECT_BMSK                                     0x1e0
#define HWIO_CRYPTO0_CRYPTO_CONFIG_PIPE_SET_SELECT_SHFT                                       0x5
#define HWIO_CRYPTO0_CRYPTO_CONFIG_HIGH_SPD_DATA_EN_N_BMSK                                   0x10
#define HWIO_CRYPTO0_CRYPTO_CONFIG_HIGH_SPD_DATA_EN_N_SHFT                                    0x4
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MASK_DOUT_INTR_BMSK                                        0x8
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MASK_DOUT_INTR_SHFT                                        0x3
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MASK_DIN_INTR_BMSK                                         0x4
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MASK_DIN_INTR_SHFT                                         0x2
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MASK_OP_DONE_INTR_BMSK                                     0x2
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MASK_OP_DONE_INTR_SHFT                                     0x1
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MASK_ERR_INTR_BMSK                                         0x1
#define HWIO_CRYPTO0_CRYPTO_CONFIG_MASK_ERR_INTR_SHFT                                         0x0

#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_ADDR                                          (CRYPTO0_CRYPTO_REG_BASE      + 0x00005000)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_PHYS                                          (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00005000)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_RMSK                                               0x3ff
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_ADDR, HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_IN)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_DBG_EN_BMSK                                        0x200
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_DBG_EN_SHFT                                          0x9
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_MAXI2AXI_DBG_SEL_BMSK                              0x1c0
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_MAXI2AXI_DBG_SEL_SHFT                                0x6
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_DBG_SEL_BMSK                                        0x3f
#define HWIO_CRYPTO0_CRYPTO_DEBUG_ENABLE_DBG_SEL_SHFT                                         0x0

#define HWIO_CRYPTO0_CRYPTO_DEBUG_ADDR                                                 (CRYPTO0_CRYPTO_REG_BASE      + 0x00005004)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_PHYS                                                 (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00005004)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_RMSK                                                 0xffffffff
#define HWIO_CRYPTO0_CRYPTO_DEBUG_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_DEBUG_ADDR, HWIO_CRYPTO0_CRYPTO_DEBUG_RMSK)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_DEBUG_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_DEBUG_DEBUG_DISABLED_BMSK                                  0xffffffff
#define HWIO_CRYPTO0_CRYPTO_DEBUG_DEBUG_DISABLED_SHFT                                         0x0

#define HWIO_CRYPTO0_CRYPTO_PWR_CTRL_ADDR                                              (CRYPTO0_CRYPTO_REG_BASE      + 0x00005008)
#define HWIO_CRYPTO0_CRYPTO_PWR_CTRL_PHYS                                              (CRYPTO0_CRYPTO_REG_BASE_PHYS + 0x00005008)
#define HWIO_CRYPTO0_CRYPTO_PWR_CTRL_RMSK                                                     0x1
#define HWIO_CRYPTO0_CRYPTO_PWR_CTRL_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_PWR_CTRL_ADDR, HWIO_CRYPTO0_CRYPTO_PWR_CTRL_RMSK)
#define HWIO_CRYPTO0_CRYPTO_PWR_CTRL_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_PWR_CTRL_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_PWR_CTRL_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_PWR_CTRL_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_PWR_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_PWR_CTRL_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_PWR_CTRL_IN)
#define HWIO_CRYPTO0_CRYPTO_PWR_CTRL_AUTO_SHUTDOWN_EN_BMSK                                    0x1
#define HWIO_CRYPTO0_CRYPTO_PWR_CTRL_AUTO_SHUTDOWN_EN_SHFT                                    0x0

/*----------------------------------------------------------------------------
 * MODULE: CRYPTO0_CRYPTO_AXI_VMIDMT_AXI
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE                                  (CRYPTO0_CRYPTO_TOP_BASE      + 0x00019000)
#define CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS                             (CRYPTO0_CRYPTO_TOP_BASE_PHYS + 0x00019000)

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_RMSK                                0x3ff707f5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_NSCFG_BMSK                          0x30000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_NSCFG_SHFT                                0x1c
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_WACFG_BMSK                           0xc000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_WACFG_SHFT                                0x1a
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_RACFG_BMSK                           0x3000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_RACFG_SHFT                                0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_SHCFG_BMSK                            0xc00000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_SHCFG_SHFT                                0x16
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_SMCFCFG_BMSK                          0x200000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_SMCFCFG_SHFT                              0x15
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_MTCFG_BMSK                            0x100000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_MTCFG_SHFT                                0x14
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_MEMATTR_BMSK                           0x70000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_MEMATTR_SHFT                              0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_USFCFG_BMSK                              0x400
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_USFCFG_SHFT                                0xa
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_GSE_BMSK                                 0x200
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_GSE_SHFT                                   0x9
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_STALLD_BMSK                              0x100
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_STALLD_SHFT                                0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_TRANSIENTCFG_BMSK                         0xc0
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_TRANSIENTCFG_SHFT                          0x6
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_GCFGFIE_BMSK                              0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_GCFGFIE_SHFT                               0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_GCFGERE_BMSK                              0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_GCFGERE_SHFT                               0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_GFIE_BMSK                                  0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_GFIE_SHFT                                  0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_CLIENTPD_BMSK                              0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR0_CLIENTPD_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000004)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000004)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_RMSK                                 0x1001f00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_GASRAE_BMSK                          0x1000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_GASRAE_SHFT                               0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_NSNUMSMRGO_BMSK                         0x1f00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR1_NSNUMSMRGO_SHFT                            0x8

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_RMSK                                      0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_BPVMID_BMSK                               0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SCR2_BPVMID_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_RMSK                                0x70000013
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPRCNSH_BMSK                        0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPRCNSH_SHFT                              0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPRCISH_BMSK                        0x20000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPRCISH_SHFT                              0x1d
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPRCOSH_BMSK                        0x10000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPRCOSH_SHFT                              0x1c
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPREQPRIORITYCFG_BMSK                     0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPREQPRIORITYCFG_SHFT                      0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPREQPRIORITY_BMSK                         0x3
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SACR_BPREQPRIORITY_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000020)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000020)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_RMSK                               0x88001eff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_SES_BMSK                           0x80000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_SES_SHFT                                 0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_SMS_BMSK                            0x8000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_SMS_SHFT                                 0x1b
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_NUMSIDB_BMSK                           0x1e00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_NUMSIDB_SHFT                              0x9
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_NUMSMRG_BMSK                             0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR0_NUMSMRG_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_RMSK                                   0x9f00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_SMCD_BMSK                              0x8000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_SMCD_SHFT                                 0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_SSDTP_BMSK                             0x1000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_SSDTP_SHFT                                0xc
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_NUMSSDNDX_BMSK                          0xf00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR1_NUMSSDNDX_SHFT                            0x8

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_RMSK                                     0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_OAS_BMSK                                 0xf0
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_OAS_SHFT                                  0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_IAS_BMSK                                  0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR2_IAS_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_RMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_MAJOR_BMSK                         0xf0000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_MAJOR_SHFT                               0x1c
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_MINOR_BMSK                          0xfff0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_MINOR_SHFT                               0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_STEP_BMSK                              0xffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR4_STEP_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000034)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000034)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_RMSK                                 0xff03ff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_NUMMSDRB_BMSK                        0xff0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_NUMMSDRB_SHFT                            0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_MSAE_BMSK                               0x200
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_MSAE_SHFT                                 0x9
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_QRIBE_BMSK                              0x100
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_QRIBE_SHFT                                0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_NVMID_BMSK                               0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR5_NVMID_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_RMSK                                     0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_MAJOR_BMSK                               0xf0
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_MAJOR_SHFT                                0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_MINOR_BMSK                                0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SIDR7_MINOR_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_ADDR                              (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_PHYS                              (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_RMSK                              0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_SGFEA0_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFAR0_SGFEA0_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_RMSK                               0xc0000026
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_MULTI_CLIENT_BMSK                  0x80000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_MULTI_CLIENT_SHFT                        0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_MULTI_CFG_BMSK                     0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_MULTI_CFG_SHFT                           0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_CAF_BMSK                                 0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_CAF_SHFT                                  0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_SMCF_BMSK                                 0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_SMCF_SHFT                                 0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_USF_BMSK                                  0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSR_USF_SHFT                                  0x1

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_ADDR                        (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_PHYS                        (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_RMSK                        0xc0000026
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_MULTI_CLIENT_BMSK           0x80000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_MULTI_CLIENT_SHFT                 0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_MULTI_CFG_BMSK              0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_MULTI_CFG_SHFT                    0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_CAF_BMSK                          0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_CAF_SHFT                           0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_SMCF_BMSK                          0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_SMCF_SHFT                          0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_USF_BMSK                           0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSRRESTORE_USF_SHFT                           0x1

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_ADDR                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_PHYS                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_RMSK                                0x132
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_MSSSELFAUTH_BMSK                    0x100
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_MSSSELFAUTH_SHFT                      0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_NSATTR_BMSK                          0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_NSATTR_SHFT                           0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_NSSTATE_BMSK                         0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_NSSTATE_SHFT                          0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_WNR_BMSK                              0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR0_WNR_SHFT                              0x1

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_ADDR                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_PHYS                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_RMSK                            0xf0f000f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_MSDINDEX_BMSK                   0xf000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_MSDINDEX_SHFT                        0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_SSDINDEX_BMSK                     0xf0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_SSDINDEX_SHFT                        0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_STREAMINDEX_BMSK                      0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR1_STREAMINDEX_SHFT                      0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_ADDR                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_PHYS                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_RMSK                           0x3f1fffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_ATID_BMSK                      0x3f000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_ATID_SHFT                            0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_AVMID_BMSK                       0x1f0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_AVMID_SHFT                           0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_ABID_BMSK                          0xe000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_ABID_SHFT                             0xd
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_APID_BMSK                          0x1f00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_APID_SHFT                             0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_AMID_BMSK                            0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SGFSYNDR2_AMID_SHFT                             0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_ADDR                          (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000090)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_PHYS                          (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000090)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_RMSK                                 0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_CLKONOFFE_BMSK                       0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTSCR0_CLKONOFFE_SHFT                       0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_ADDR                                 (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_PHYS                                 (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_RMSK                                  0xff70ff5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_WACFG_BMSK                            0xc000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_WACFG_SHFT                                 0x1a
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_RACFG_BMSK                            0x3000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_RACFG_SHFT                                 0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_SHCFG_BMSK                             0xc00000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_SHCFG_SHFT                                 0x16
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_SMCFCFG_BMSK                           0x200000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_SMCFCFG_SHFT                               0x15
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_MTCFG_BMSK                             0x100000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_MTCFG_SHFT                                 0x14
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_MEMATTR_BMSK                            0x70000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_MEMATTR_SHFT                               0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_VMIDPNE_BMSK                              0x800
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_VMIDPNE_SHFT                                0xb
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_USFCFG_BMSK                               0x400
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_USFCFG_SHFT                                 0xa
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_GSE_BMSK                                  0x200
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_GSE_SHFT                                    0x9
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_STALLD_BMSK                               0x100
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_STALLD_SHFT                                 0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_TRANSIENTCFG_BMSK                          0xc0
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_TRANSIENTCFG_SHFT                           0x6
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_GCFGFIE_BMSK                               0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_GCFGFIE_SHFT                                0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_GCFGERE_BMSK                               0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_GCFGERE_SHFT                                0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_GFIE_BMSK                                   0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_GFIE_SHFT                                   0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_CLIENTPD_BMSK                               0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR0_CLIENTPD_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_ADDR                                 (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_PHYS                                 (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_RMSK                                       0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_BPVMID_BMSK                                0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_CR2_BPVMID_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_ADDR                                 (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_PHYS                                 (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_RMSK                                 0x70000013
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPRCNSH_BMSK                         0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPRCNSH_SHFT                               0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPRCISH_BMSK                         0x20000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPRCISH_SHFT                               0x1d
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPRCOSH_BMSK                         0x10000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPRCOSH_SHFT                               0x1c
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPREQPRIORITYCFG_BMSK                      0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPREQPRIORITYCFG_SHFT                       0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPREQPRIORITY_BMSK                          0x3
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_ACR_BPREQPRIORITY_SHFT                          0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000020)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000020)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_RMSK                                 0x8001eff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_SMS_BMSK                             0x8000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_SMS_SHFT                                  0x1b
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_NUMSIDB_BMSK                            0x1e00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_NUMSIDB_SHFT                               0x9
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_NUMSMRG_BMSK                              0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR0_NUMSMRG_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_RMSK                                    0x9f00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_SMCD_BMSK                               0x8000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_SMCD_SHFT                                  0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_SSDTP_BMSK                              0x1000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_SSDTP_SHFT                                 0xc
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_NUMSSDNDX_BMSK                           0xf00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR1_NUMSSDNDX_SHFT                             0x8

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_RMSK                                      0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_OAS_BMSK                                  0xf0
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_OAS_SHFT                                   0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_IAS_BMSK                                   0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR2_IAS_SHFT                                   0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_RMSK                                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_MAJOR_BMSK                          0xf0000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_MAJOR_SHFT                                0x1c
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_MINOR_BMSK                           0xfff0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_MINOR_SHFT                                0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_STEP_BMSK                               0xffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR4_STEP_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000034)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000034)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_RMSK                                  0xff03ff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_NUMMSDRB_BMSK                         0xff0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_NUMMSDRB_SHFT                             0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_MSAE_BMSK                                0x200
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_MSAE_SHFT                                  0x9
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_QRIBE_BMSK                               0x100
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_QRIBE_SHFT                                 0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_NVMID_BMSK                                0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR5_NVMID_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_RMSK                                      0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_MAJOR_BMSK                                0xf0
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_MAJOR_SHFT                                 0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_MINOR_BMSK                                 0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_IDR7_MINOR_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_RMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_GFEA0_BMSK                         0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFAR0_GFEA0_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_ADDR                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_PHYS                                (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_RMSK                                0xc00000a6
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_MULTI_CLIENT_BMSK                   0x80000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_MULTI_CLIENT_SHFT                         0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_MULTI_CFG_BMSK                      0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_MULTI_CFG_SHFT                            0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_PF_BMSK                                   0x80
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_PF_SHFT                                    0x7
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_CAF_BMSK                                  0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_CAF_SHFT                                   0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_SMCF_BMSK                                  0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_SMCF_SHFT                                  0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_USF_BMSK                                   0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSR_USF_SHFT                                   0x1

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_ADDR                         (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_PHYS                         (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_RMSK                         0xc00000a6
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_MULTI_CLIENT_BMSK            0x80000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_MULTI_CLIENT_SHFT                  0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_MULTI_CFG_BMSK               0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_MULTI_CFG_SHFT                     0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_PF_BMSK                            0x80
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_PF_SHFT                             0x7
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_CAF_BMSK                           0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_CAF_SHFT                            0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_SMCF_BMSK                           0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_SMCF_SHFT                           0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_USF_BMSK                            0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSRRESTORE_USF_SHFT                            0x1

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_ADDR                            (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_PHYS                            (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_RMSK                                 0x132
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_MSSSELFAUTH_BMSK                     0x100
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_MSSSELFAUTH_SHFT                       0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_NSATTR_BMSK                           0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_NSATTR_SHFT                            0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_NSSTATE_BMSK                          0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_NSSTATE_SHFT                           0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_WNR_BMSK                               0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR0_WNR_SHFT                               0x1

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_ADDR                            (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_PHYS                            (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_RMSK                             0xf0f000f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_MSDINDEX_BMSK                    0xf000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_MSDINDEX_SHFT                         0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_SSDINDEX_BMSK                      0xf0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_SSDINDEX_SHFT                         0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_STREAMINDEX_BMSK                       0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR1_STREAMINDEX_SHFT                       0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_ADDR                            (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_PHYS                            (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_RMSK                            0x3f1fffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_ATID_BMSK                       0x3f000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_ATID_SHFT                             0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_AVMID_BMSK                        0x1f0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_AVMID_SHFT                            0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_ABID_BMSK                           0xe000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_ABID_SHFT                              0xd
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_APID_BMSK                           0x1f00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_APID_SHFT                              0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_AMID_BMSK                             0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_GFSYNDR2_AMID_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_ADDR                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000090)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_PHYS                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000090)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_RMSK                                  0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_CLKONOFFE_BMSK                        0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTCR0_CLKONOFFE_SHFT                        0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_ADDR                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x0000009c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_PHYS                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x0000009c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_RMSK                           0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_RWE_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_VMIDMTACR_RWE_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000400)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000400)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_RMSK                                0xff70ff5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_WACFG_BMSK                          0xc000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_WACFG_SHFT                               0x1a
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_RACFG_BMSK                          0x3000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_RACFG_SHFT                               0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_SHCFG_BMSK                           0xc00000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_SHCFG_SHFT                               0x16
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_SMCFCFG_BMSK                         0x200000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_SMCFCFG_SHFT                             0x15
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_MTCFG_BMSK                           0x100000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_MTCFG_SHFT                               0x14
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_MEMATTR_BMSK                          0x70000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_MEMATTR_SHFT                             0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_VMIDPNE_BMSK                            0x800
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_VMIDPNE_SHFT                              0xb
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_USFCFG_BMSK                             0x400
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_USFCFG_SHFT                               0xa
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_GSE_BMSK                                0x200
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_GSE_SHFT                                  0x9
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_STALLD_BMSK                             0x100
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_STALLD_SHFT                               0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_TRANSIENTCFG_BMSK                        0xc0
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_TRANSIENTCFG_SHFT                         0x6
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_GCFGFIE_BMSK                             0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_GCFGFIE_SHFT                              0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_GCFGERE_BMSK                             0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_GCFGERE_SHFT                              0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_GFIE_BMSK                                 0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_GFIE_SHFT                                 0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_CLIENTPD_BMSK                             0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR0_CLIENTPD_SHFT                             0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000408)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000408)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_RMSK                                     0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_BPVMID_BMSK                              0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSCR2_BPVMID_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000410)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000410)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_RMSK                               0x70000013
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPRCNSH_BMSK                       0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPRCNSH_SHFT                             0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPRCISH_BMSK                       0x20000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPRCISH_SHFT                             0x1d
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPRCOSH_BMSK                       0x10000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPRCOSH_SHFT                             0x1c
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPREQPRIORITYCFG_BMSK                    0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPREQPRIORITYCFG_SHFT                     0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPREQPRIORITY_BMSK                        0x3
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSACR_BPREQPRIORITY_SHFT                        0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_ADDR                             (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000440)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_PHYS                             (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000440)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_RMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_GFEA0_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFAR0_GFEA0_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_ADDR                              (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000448)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_PHYS                              (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000448)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_RMSK                              0xc00000a6
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_MULTI_CLIENT_BMSK                 0x80000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_MULTI_CLIENT_SHFT                       0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_MULTI_CFG_BMSK                    0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_MULTI_CFG_SHFT                          0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_PF_BMSK                                 0x80
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_PF_SHFT                                  0x7
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_CAF_BMSK                                0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_CAF_SHFT                                 0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_SMCF_BMSK                                0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_SMCF_SHFT                                0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_USF_BMSK                                 0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSR_USF_SHFT                                 0x1

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_ADDR                       (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x0000044c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_PHYS                       (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x0000044c)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_RMSK                       0xc00000a6
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_MULTI_CLIENT_BMSK          0x80000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_MULTI_CLIENT_SHFT                0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_MULTI_CFG_BMSK             0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_MULTI_CFG_SHFT                   0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_PF_BMSK                          0x80
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_PF_SHFT                           0x7
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_CAF_BMSK                         0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_CAF_SHFT                          0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_SMCF_BMSK                         0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_SMCF_SHFT                         0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_USF_BMSK                          0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSRRESTORE_USF_SHFT                          0x1

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_ADDR                          (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000450)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_PHYS                          (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000450)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_RMSK                               0x132
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_MSSSELFAUTH_BMSK                   0x100
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_MSSSELFAUTH_SHFT                     0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_NSATTR_BMSK                         0x20
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_NSATTR_SHFT                          0x5
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_NSSTATE_BMSK                        0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_NSSTATE_SHFT                         0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_WNR_BMSK                             0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR0_WNR_SHFT                             0x1

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_ADDR                          (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000454)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_PHYS                          (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000454)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_RMSK                           0xf0f000f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_MSDINDEX_BMSK                  0xf000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_MSDINDEX_SHFT                       0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_SSDINDEX_BMSK                    0xf0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_SSDINDEX_SHFT                       0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_STREAMINDEX_BMSK                     0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR1_STREAMINDEX_SHFT                     0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_ADDR                          (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000458)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_PHYS                          (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000458)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_RMSK                          0x3f1fffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_ATID_BMSK                     0x3f000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_ATID_SHFT                           0x18
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_AVMID_BMSK                      0x1f0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_AVMID_SHFT                          0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_ABID_BMSK                         0xe000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_ABID_SHFT                            0xd
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_APID_BMSK                         0x1f00
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_APID_SHFT                            0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_AMID_BMSK                           0xff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSGFSYNDR2_AMID_SHFT                            0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_ADDR                         (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000490)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_PHYS                         (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000490)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_RMSK                                0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_CLKONOFFE_BMSK                      0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_NSVMIDMTCR0_CLKONOFFE_SHFT                      0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000080)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000080)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_RMSK                                   0xffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_RWE_BMSK                               0xffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SSDR0_RWE_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_ADDR                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000480)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_PHYS                               (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000480)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_RMSK                                   0xffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_RWE_BMSK                               0xffff
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MSDR0_RWE_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_ADDR                                 (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000494)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_PHYS                                 (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000494)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_RMSK                                        0x7
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_ADDR, HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_IN)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_CLKONOFFE_BMSK                              0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_CLKONOFFE_SHFT                              0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_BPMSACFG_BMSK                               0x2
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_BPMSACFG_SHFT                               0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_BPSMSACFG_BMSK                              0x1
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_MCR_BPSMSACFG_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_ADDR(n)                            (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000c00 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_PHYS(n)                            (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000c00 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_RMSK                               0x30ff7b1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_MAXn                                       15
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_TRANSIENTCFG_BMSK                  0x30000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_TRANSIENTCFG_SHFT                        0x1c
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_WACFG_BMSK                           0xc00000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_WACFG_SHFT                               0x16
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_RACFG_BMSK                           0x300000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_RACFG_SHFT                               0x14
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_NSCFG_BMSK                            0xc0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_NSCFG_SHFT                               0x12
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_TYPE_BMSK                             0x30000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_TYPE_SHFT                                0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_MEMATTR_BMSK                           0x7000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_MEMATTR_SHFT                              0xc
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_MTCFG_BMSK                              0x800
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_MTCFG_SHFT                                0xb
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_SHCFG_BMSK                              0x300
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_SHCFG_SHFT                                0x8
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_VMID_BMSK                                0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_S2VRn_VMID_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_ADDR(n)                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000e00 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_PHYS(n)                           (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000e00 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_RMSK                              0x70000013
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_MAXn                                      15
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_RCNSH_BMSK                        0x40000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_RCNSH_SHFT                              0x1e
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_RCISH_BMSK                        0x20000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_RCISH_SHFT                              0x1d
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_RCOSH_BMSK                        0x10000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_RCOSH_SHFT                              0x1c
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_REQPRIORITYCFG_BMSK                     0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_REQPRIORITYCFG_SHFT                      0x4
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_REQPRIORITY_BMSK                         0x3
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_AS2VRn_REQPRIORITY_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_ADDR(n)                             (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE      + 0x00000800 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_PHYS(n)                             (CRYPTO0_CRYPTO_AXI_VMIDMT_AXI_REG_BASE_PHYS + 0x00000800 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_RMSK                                0x800f000f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_MAXn                                        15
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_VALID_BMSK                          0x80000000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_VALID_SHFT                                0x1f
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_MASK_BMSK                              0xf0000
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_MASK_SHFT                                 0x10
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_ID_BMSK                                    0xf
#define HWIO_CRYPTO0_CRYPTO_AXI_VMIDMT_SMRn_ID_SHFT                                    0x0

/*----------------------------------------------------------------------------
 * MODULE: CRYPTO0_CRYPTO_BAM
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_BAM_REG_BASE                                                    (CRYPTO0_CRYPTO_TOP_BASE      + 0x00004000)
#define CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS                                               (CRYPTO0_CRYPTO_TOP_BASE_PHYS + 0x00004000)

#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_ADDR                                              (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_PHYS                                              (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_RMSK                                                 0xfeff3
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_CTRL_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_CTRL_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_CTRL_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_CTRL_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_CTRL_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_CTRL_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_CACHE_MISS_ERR_RESP_EN_BMSK                          0x80000
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_CACHE_MISS_ERR_RESP_EN_SHFT                             0x13
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_LOCAL_CLK_GATING_BMSK                                0x60000
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_LOCAL_CLK_GATING_SHFT                                   0x11
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_IBC_DISABLE_BMSK                                     0x10000
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_IBC_DISABLE_SHFT                                        0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_CACHED_DESC_STORE_BMSK                            0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_CACHED_DESC_STORE_SHFT                               0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_DESC_CACHE_SEL_BMSK                               0x6000
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_DESC_CACHE_SEL_SHFT                                  0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_TESTBUS_SEL_BMSK                                   0xfe0
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_TESTBUS_SEL_SHFT                                     0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_EN_ACCUM_BMSK                                       0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_EN_ACCUM_SHFT                                        0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_EN_BMSK                                              0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_EN_SHFT                                              0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_SW_RST_BMSK                                          0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_CTRL_BAM_SW_RST_SHFT                                          0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_ADDR                                          (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000004)
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_PHYS                                          (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000004)
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_RMSK                                          0xffffefff
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_REVISION_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_REVISION_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_REVISION_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_INACTIV_TMR_BASE_BMSK                         0xff000000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_INACTIV_TMR_BASE_SHFT                               0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_CMD_DESC_EN_BMSK                                0x800000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_CMD_DESC_EN_SHFT                                    0x17
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_DESC_CACHE_DEPTH_BMSK                           0x600000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_DESC_CACHE_DEPTH_SHFT                               0x15
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_NUM_INACTIV_TMRS_BMSK                           0x100000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_NUM_INACTIV_TMRS_SHFT                               0x14
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_INACTIV_TMRS_EXST_BMSK                           0x80000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_INACTIV_TMRS_EXST_SHFT                              0x13
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_HIGH_FREQUENCY_BAM_BMSK                          0x40000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_HIGH_FREQUENCY_BAM_SHFT                             0x12
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_BAM_HAS_NO_BYPASS_BMSK                           0x20000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_BAM_HAS_NO_BYPASS_SHFT                              0x11
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_SECURED_BMSK                                     0x10000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_SECURED_SHFT                                        0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_USE_VMIDMT_BMSK                                   0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_USE_VMIDMT_SHFT                                      0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_AXI_ACTIVE_BMSK                                   0x4000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_AXI_ACTIVE_SHFT                                      0xe
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_CE_BUFFER_SIZE_BMSK                               0x2000
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_CE_BUFFER_SIZE_SHFT                                  0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_NUM_EES_BMSK                                       0xf00
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_NUM_EES_SHFT                                         0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_REVISION_BMSK                                       0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_REVISION_REVISION_SHFT                                        0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_ADDR                                        (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000080)
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_PHYS                                        (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000080)
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_RMSK                                        0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_MAJOR_BMSK                                  0xf0000000
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_MAJOR_SHFT                                        0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_MINOR_BMSK                                   0xfff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_MINOR_SHFT                                        0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_STEP_BMSK                                       0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_SW_VERSION_STEP_SHFT                                          0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_ADDR                                         (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_PHYS                                         (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_RMSK                                         0xffff00ff
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_BAM_NON_PIPE_GRP_BMSK                        0xff000000
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_BAM_NON_PIPE_GRP_SHFT                              0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_PERIPH_NON_PIPE_GRP_BMSK                       0xff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_PERIPH_NON_PIPE_GRP_SHFT                           0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_BAM_NUM_PIPES_BMSK                                 0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_NUM_PIPES_BAM_NUM_PIPES_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_ADDR                                             (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_PHYS                                             (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_RMSK                                                 0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TIMER_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_TIMER_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TIMER_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_TIMER_BMSK                                           0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_TIMER_SHFT                                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_ADDR                                        (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000044)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_PHYS                                        (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000044)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_RMSK                                        0xe000ffff
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_TIMER_RST_BMSK                              0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_TIMER_RST_SHFT                                    0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_TIMER_RUN_BMSK                              0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_TIMER_RUN_SHFT                                    0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_TIMER_MODE_BMSK                             0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_TIMER_MODE_SHFT                                   0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_TIMER_TRSHLD_BMSK                               0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_TIMER_CTRL_TIMER_TRSHLD_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_ADDR                                   (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_PHYS                                   (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_RMSK                                       0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_CNT_TRSHLD_BMSK                            0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_DESC_CNT_TRSHLD_CNT_TRSHLD_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_ADDR                                          (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x0000000c)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_PHYS                                          (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x0000000c)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_RMSK                                          0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_BAM_IRQ_BMSK                                  0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_BAM_IRQ_SHFT                                        0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_P_IRQ_BMSK                                    0x7fffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_P_IRQ_SHFT                                           0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_ADDR                                      (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_PHYS                                      (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_RMSK                                      0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_BAM_IRQ_MSK_BMSK                          0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_BAM_IRQ_MSK_SHFT                                0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_P_IRQ_MSK_BMSK                            0x7fffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_P_IRQ_MSK_SHFT                                   0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_ADDR                                 (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_PHYS                                 (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_RMSK                                 0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_BAM_IRQ_UNMASKED_BMSK                0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_BAM_IRQ_UNMASKED_SHFT                      0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_P_IRQ_UNMASKED_BMSK                  0x7fffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_P_IRQ_UNMASKED_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_ADDR                                          (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000014)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_PHYS                                          (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000014)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_RMSK                                                0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_BAM_TIMER_IRQ_BMSK                                  0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_BAM_TIMER_IRQ_SHFT                                   0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_BAM_EMPTY_IRQ_BMSK                                   0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_BAM_EMPTY_IRQ_SHFT                                   0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_BAM_ERROR_IRQ_BMSK                                   0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_BAM_ERROR_IRQ_SHFT                                   0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_BAM_HRESP_ERR_IRQ_BMSK                               0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_STTS_BAM_HRESP_ERR_IRQ_SHFT                               0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_ADDR                                           (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000018)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_PHYS                                           (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000018)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_RMSK                                                 0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_BAM_TIMER_CLR_BMSK                                   0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_BAM_TIMER_CLR_SHFT                                    0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_BAM_EMPTY_CLR_BMSK                                    0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_BAM_EMPTY_CLR_SHFT                                    0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_BAM_ERROR_CLR_BMSK                                    0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_BAM_ERROR_CLR_SHFT                                    0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_BAM_HRESP_ERR_CLR_BMSK                                0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_CLR_BAM_HRESP_ERR_CLR_SHFT                                0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_ADDR                                            (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x0000001c)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_PHYS                                            (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x0000001c)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_RMSK                                                  0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_BAM_TIMER_EN_BMSK                                     0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_BAM_TIMER_EN_SHFT                                      0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_BAM_EMPTY_EN_BMSK                                      0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_BAM_EMPTY_EN_SHFT                                      0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_BAM_ERROR_EN_BMSK                                      0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_BAM_ERROR_EN_SHFT                                      0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_BAM_HRESP_ERR_EN_BMSK                                  0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_EN_BAM_HRESP_ERR_EN_SHFT                                  0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_ADDR                              (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_PHYS                              (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_RMSK                                0x7fffff
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HVMID_BMSK                  0x7c0000
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HVMID_SHFT                      0x12
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_DIRECT_MODE_BMSK             0x20000
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_DIRECT_MODE_SHFT                0x11
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HCID_BMSK                    0x1f000
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HCID_SHFT                        0xc
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HPROT_BMSK                     0xf00
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HPROT_SHFT                       0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HBURST_BMSK                     0xe0
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HBURST_SHFT                      0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HSIZE_BMSK                      0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HSIZE_SHFT                       0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HWRITE_BMSK                      0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HWRITE_SHFT                      0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HTRANS_BMSK                      0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_CTRLS_BAM_ERR_HTRANS_SHFT                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_ADDR                               (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_PHYS                               (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_RMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_BAM_ERR_ADDR_BMSK                  0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_ADDR_BAM_ERR_ADDR_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_ADDR                               (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x0000002c)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_PHYS                               (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x0000002c)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_RMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_BAM_ERR_DATA_BMSK                  0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_AHB_MASTER_ERR_DATA_BAM_ERR_DATA_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_ADDR                                         (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000070)
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_PHYS                                         (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000070)
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_RMSK                                             0x3f87
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_LOCK_EE_CTRL_BMSK                                0x2000
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_LOCK_EE_CTRL_SHFT                                   0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_BAM_VMID_BMSK                                    0x1f00
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_BAM_VMID_SHFT                                       0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_BAM_RST_BLOCK_BMSK                                 0x80
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_BAM_RST_BLOCK_SHFT                                  0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_BAM_EE_BMSK                                         0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_TRUST_REG_BAM_EE_SHFT                                         0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_ADDR                                      (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000074)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_PHYS                                      (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000074)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_RMSK                                        0x3f007f
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_SW_EVENTS_ZERO_BMSK                         0x200000
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_SW_EVENTS_ZERO_SHFT                             0x15
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_SW_EVENTS_SEL_BMSK                          0x180000
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_SW_EVENTS_SEL_SHFT                              0x13
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_BAM_DATA_ERASE_BMSK                          0x40000
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_BAM_DATA_ERASE_SHFT                             0x12
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_BAM_DATA_FLUSH_BMSK                          0x20000
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_BAM_DATA_FLUSH_SHFT                             0x11
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_BAM_CLK_ALWAYS_ON_BMSK                       0x10000
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_BAM_CLK_ALWAYS_ON_SHFT                          0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_BAM_TESTBUS_SEL_BMSK                            0x7f
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_SEL_BAM_TESTBUS_SEL_SHFT                             0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_ADDR                                      (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000078)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_PHYS                                      (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000078)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_RMSK                                      0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_BAM_TESTBUS_REG_BMSK                      0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_TEST_BUS_REG_BAM_TESTBUS_REG_SHFT                             0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_ADDR                                         (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x0000007c)
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_PHYS                                         (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x0000007c)
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_RMSK                                         0x7ffff80f
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_MULTIPLE_EVENTS_DESC_AVAIL_EN_BMSK           0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_MULTIPLE_EVENTS_DESC_AVAIL_EN_SHFT                 0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_MULTIPLE_EVENTS_SIZE_EN_BMSK                 0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_MULTIPLE_EVENTS_SIZE_EN_SHFT                       0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_ZLT_W_CD_SUPPORT_BMSK                    0x10000000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_ZLT_W_CD_SUPPORT_SHFT                          0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_CD_ENABLE_BMSK                            0x8000000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_CD_ENABLE_SHFT                                 0x1b
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_AU_ACCUMED_BMSK                           0x4000000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_AU_ACCUMED_SHFT                                0x1a
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_PSM_P_HD_DATA_BMSK                        0x2000000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_PSM_P_HD_DATA_SHFT                             0x19
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_REG_P_EN_BMSK                             0x1000000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_REG_P_EN_SHFT                                  0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_DSC_AVL_P_RST_BMSK                      0x800000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_DSC_AVL_P_RST_SHFT                          0x17
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_RETR_SVPNT_BMSK                         0x400000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_RETR_SVPNT_SHFT                             0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_CSW_ACK_IDL_BMSK                        0x200000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_CSW_ACK_IDL_SHFT                            0x15
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_BLK_CSW_BMSK                            0x100000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_BLK_CSW_SHFT                                0x14
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_P_RES_BMSK                               0x80000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_WB_P_RES_SHFT                                  0x13
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_SI_P_RES_BMSK                               0x40000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_SI_P_RES_SHFT                                  0x12
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_AU_P_RES_BMSK                               0x20000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_AU_P_RES_SHFT                                  0x11
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_PSM_P_RES_BMSK                              0x10000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_PSM_P_RES_SHFT                                 0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_PSM_CSW_REQ_BMSK                             0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_PSM_CSW_REQ_SHFT                                0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_SB_CLK_REQ_BMSK                              0x4000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_SB_CLK_REQ_SHFT                                 0xe
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_IBC_DISABLE_BMSK                             0x2000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_IBC_DISABLE_SHFT                                0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_NO_EXT_P_RST_BMSK                            0x1000
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_NO_EXT_P_RST_SHFT                               0xc
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_FULL_PIPE_BMSK                                0x800
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_FULL_PIPE_SHFT                                  0xb
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_ADML_SYNC_BRIDGE_BMSK                           0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_ADML_SYNC_BRIDGE_SHFT                           0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_PIPE_CNFG_BMSK                                  0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_PIPE_CNFG_SHFT                                  0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_ADML_DEEP_CONS_FIFO_BMSK                        0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_ADML_DEEP_CONS_FIFO_SHFT                        0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_ADML_INCR4_EN_N_BMSK                            0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_CNFG_BITS_BAM_ADML_INCR4_EN_N_SHFT                            0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_ADDR(n)                                   (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000800 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_PHYS(n)                                   (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000800 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_RMSK                                      0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_MAXn                                               7
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_BAM_IRQ_BMSK                              0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_BAM_IRQ_SHFT                                    0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_P_IRQ_BMSK                                0x7fffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_EEn_P_IRQ_SHFT                                       0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n)                               (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000804 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_PHYS(n)                               (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000804 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_RMSK                                  0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_MAXn                                           7
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_BAM_IRQ_MSK_BMSK                      0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_BAM_IRQ_MSK_SHFT                            0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_P_IRQ_MSK_BMSK                        0x7fffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_MSK_EEn_P_IRQ_MSK_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_ADDR(n)                          (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00000808 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_PHYS(n)                          (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00000808 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_RMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_MAXn                                      7
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_BAM_IRQ_UNMASKED_BMSK            0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_BAM_IRQ_UNMASKED_SHFT                  0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_P_IRQ_UNMASKED_BMSK              0x7fffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_IRQ_SRCS_UNMASKED_EEn_P_IRQ_UNMASKED_SHFT                     0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_ADDR(n)                                        (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001000 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_PHYS(n)                                        (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001000 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_RMSK                                             0x1f0ffa
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_MAXn                                                    7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_LOCK_GROUP_BMSK                                0x1f0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_LOCK_GROUP_SHFT                                    0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_WRITE_NWD_BMSK                                    0x800
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_WRITE_NWD_SHFT                                      0xb
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_PREFETCH_LIMIT_BMSK                               0x600
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_PREFETCH_LIMIT_SHFT                                 0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_AUTO_EOB_SEL_BMSK                                 0x180
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_AUTO_EOB_SEL_SHFT                                   0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_AUTO_EOB_BMSK                                      0x40
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_AUTO_EOB_SHFT                                       0x6
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_SYS_MODE_BMSK                                      0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_SYS_MODE_SHFT                                       0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_SYS_STRM_BMSK                                      0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_SYS_STRM_SHFT                                       0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_DIRECTION_BMSK                                      0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_DIRECTION_SHFT                                      0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_EN_BMSK                                             0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CTRLn_P_EN_SHFT                                             0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_P_RSTn_ADDR(n)                                         (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001004 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RSTn_PHYS(n)                                         (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001004 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RSTn_RMSK                                                   0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RSTn_MAXn                                                     7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RSTn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_RSTn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RSTn_P_SW_RST_BMSK                                          0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RSTn_P_SW_RST_SHFT                                          0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_ADDR(n)                                        (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001008 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_PHYS(n)                                        (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001008 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_RMSK                                                  0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_MAXn                                                    7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_P_PROD_HALTED_BMSK                                    0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_P_PROD_HALTED_SHFT                                    0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_P_HALT_BMSK                                           0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_HALTn_P_HALT_SHFT                                           0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_ADDR(n)                                   (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001030 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_PHYS(n)                                   (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001030 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_RMSK                                          0x1f07
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_MAXn                                               7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_BAM_P_VMID_BMSK                               0x1f00
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_BAM_P_VMID_SHFT                                  0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_BAM_P_EE_BMSK                                    0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TRUST_REGn_BAM_P_EE_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_ADDR(n)                                    (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001010 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_PHYS(n)                                    (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001010 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_RMSK                                             0x3f
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_MAXn                                                7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_TRNSFR_END_IRQ_BMSK                            0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_TRNSFR_END_IRQ_SHFT                             0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_ERR_IRQ_BMSK                                   0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_ERR_IRQ_SHFT                                    0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_OUT_OF_DESC_IRQ_BMSK                            0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_OUT_OF_DESC_IRQ_SHFT                            0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_WAKE_IRQ_BMSK                                   0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_WAKE_IRQ_SHFT                                   0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_TIMER_IRQ_BMSK                                  0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_TIMER_IRQ_SHFT                                  0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_PRCSD_DESC_IRQ_BMSK                             0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_STTSn_P_PRCSD_DESC_IRQ_SHFT                             0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_ADDR(n)                                     (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001014 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_PHYS(n)                                     (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001014 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_RMSK                                              0x3f
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_MAXn                                                 7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_TRNSFR_END_CLR_BMSK                             0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_TRNSFR_END_CLR_SHFT                              0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_ERR_CLR_BMSK                                    0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_ERR_CLR_SHFT                                     0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_OUT_OF_DESC_CLR_BMSK                             0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_OUT_OF_DESC_CLR_SHFT                             0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_WAKE_CLR_BMSK                                    0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_WAKE_CLR_SHFT                                    0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_TIMER_CLR_BMSK                                   0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_TIMER_CLR_SHFT                                   0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_PRCSD_DESC_CLR_BMSK                              0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_CLRn_P_PRCSD_DESC_CLR_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_ADDR(n)                                      (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001018 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_PHYS(n)                                      (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001018 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_RMSK                                               0x3f
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_MAXn                                                  7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_TRNSFR_END_EN_BMSK                               0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_TRNSFR_END_EN_SHFT                                0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_ERR_EN_BMSK                                      0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_ERR_EN_SHFT                                       0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_OUT_OF_DESC_EN_BMSK                               0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_OUT_OF_DESC_EN_SHFT                               0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_WAKE_EN_BMSK                                      0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_WAKE_EN_SHFT                                      0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_TIMER_EN_BMSK                                     0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_TIMER_EN_SHFT                                     0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_PRCSD_DESC_EN_BMSK                                0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_P_IRQ_ENn_P_PRCSD_DESC_EN_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_ADDR(n)                                       (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x0000101c + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_PHYS(n)                                       (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x0000101c + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_RMSK                                              0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_MAXn                                                   7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_P_TIMER_BMSK                                      0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMERn_P_TIMER_SHFT                                         0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_ADDR(n)                                  (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001020 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_PHYS(n)                                  (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001020 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_RMSK                                     0xe000ffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_MAXn                                              7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_P_TIMER_RST_BMSK                         0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_P_TIMER_RST_SHFT                               0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_P_TIMER_RUN_BMSK                         0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_P_TIMER_RUN_SHFT                               0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_P_TIMER_MODE_BMSK                        0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_P_TIMER_MODE_SHFT                              0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_P_TIMER_TRSHLD_BMSK                          0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_TIMER_CTRLn_P_TIMER_TRSHLD_SHFT                             0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_ADDR(n)                                 (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001024 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_PHYS(n)                                 (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001024 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_RMSK                                     0x11fffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_MAXn                                             7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_BAM_P_SB_UPDATED_BMSK                    0x1000000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_BAM_P_SB_UPDATED_SHFT                         0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_BAM_P_TOGGLE_BMSK                         0x100000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_BAM_P_TOGGLE_SHFT                             0x14
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_BAM_P_CTRL_BMSK                            0xf0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_BAM_P_CTRL_SHFT                               0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_BAM_P_BYTES_FREE_BMSK                       0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PRDCR_SDBNDn_BAM_P_BYTES_FREE_SHFT                          0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_ADDR(n)                                 (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001028 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_PHYS(n)                                 (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001028 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_RMSK                                     0x1ffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_MAXn                                             7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_SB_UPDATED_BMSK                    0x1000000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_SB_UPDATED_SHFT                         0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_WAIT_4_ACK_BMSK                     0x800000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_WAIT_4_ACK_SHFT                         0x17
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_ACK_TOGGLE_BMSK                     0x400000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_ACK_TOGGLE_SHFT                         0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_ACK_TOGGLE_R_BMSK                   0x200000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_ACK_TOGGLE_R_SHFT                       0x15
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_TOGGLE_BMSK                         0x100000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_TOGGLE_SHFT                             0x14
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_CTRL_BMSK                            0xf0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_CTRL_SHFT                               0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_BYTES_AVAIL_BMSK                      0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_CNSMR_SDBNDn_BAM_P_BYTES_AVAIL_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_ADDR(n)                              (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x0000182c + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_PHYS(n)                              (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x0000182c + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_RMSK                                 0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_MAXn                                          7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_P_EVNT_DEST_ADDR_BMSK                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_DEST_ADDRn_P_EVNT_DEST_ADDR_SHFT                       0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_ADDR(n)                                    (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001818 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_PHYS(n)                                    (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001818 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_RMSK                                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_MAXn                                                7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_P_BYTES_CONSUMED_BMSK                      0xffff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_P_BYTES_CONSUMED_SHFT                            0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_P_DESC_FIFO_PEER_OFST_BMSK                     0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_REGn_P_DESC_FIFO_PEER_OFST_SHFT                        0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_ADDR(n)                                    (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001800 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_PHYS(n)                                    (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001800 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_RMSK                                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_MAXn                                                7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_SW_OFST_IN_DESC_BMSK                       0xffff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_SW_OFST_IN_DESC_SHFT                             0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_SW_DESC_OFST_BMSK                              0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SW_OFSTSn_SW_DESC_OFST_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_ADDR(n)                              (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001824 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_PHYS(n)                              (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001824 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_RMSK                                 0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_MAXn                                          7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_P_DATA_FIFO_ADDR_BMSK                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DATA_FIFO_ADDRn_P_DATA_FIFO_ADDR_SHFT                       0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_ADDR(n)                              (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x0000181c + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_PHYS(n)                              (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x0000181c + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_RMSK                                 0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_MAXn                                          7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_P_DESC_FIFO_ADDR_BMSK                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DESC_FIFO_ADDRn_P_DESC_FIFO_ADDR_SHFT                       0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n)                             (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001828 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_PHYS(n)                             (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001828 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_RMSK                                    0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_MAXn                                         7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_P_TRSHLD_BMSK                           0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_EVNT_GEN_TRSHLDn_P_TRSHLD_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_ADDR(n)                                  (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001820 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_PHYS(n)                                  (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001820 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_RMSK                                     0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_MAXn                                              7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_P_DATA_FIFO_SIZE_BMSK                    0xffff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_P_DATA_FIFO_SIZE_SHFT                          0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_P_DESC_FIFO_SIZE_BMSK                        0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_FIFO_SIZESn_P_DESC_FIFO_SIZE_SHFT                           0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_ADDR(n)                                 (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001834 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_PHYS(n)                                 (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001834 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_RMSK                                    0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_MAXn                                             7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_RETR_DESC_OFST_BMSK                     0xffff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_RETR_DESC_OFST_SHFT                           0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_RETR_OFST_IN_DESC_BMSK                      0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_RETR_CNTXT_n_RETR_OFST_IN_DESC_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_ADDR(n)                                   (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001838 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_PHYS(n)                                   (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001838 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_RMSK                                          0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_MAXn                                               7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_SI_DESC_OFST_BMSK                             0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_SI_CNTXT_n_SI_DESC_OFST_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_ADDR(n)                                   (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001830 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_PHYS(n)                                   (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001830 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_RMSK                                      0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_MAXn                                               7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_WB_ACCUMULATED_BMSK                       0xffff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_WB_ACCUMULATED_SHFT                             0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_DF_DESC_OFST_BMSK                             0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_DF_CNTXT_n_DF_DESC_OFST_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n)                             (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001804 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_PHYS(n)                             (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001804 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_RMSK                                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_MAXn                                         7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_AU_PSM_ACCUMED_BMSK                 0xffff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_AU_PSM_ACCUMED_SHFT                       0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_AU_ACKED_BMSK                           0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_AU_PSM_CNTXT_1_n_AU_ACKED_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_ADDR(n)                                (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001808 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PHYS(n)                                (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001808 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_RMSK                                   0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_MAXn                                            7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_VALID_BMSK                    0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_VALID_SHFT                          0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_IRQ_BMSK                      0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_IRQ_SHFT                            0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_IRQ_DONE_BMSK                 0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_IRQ_DONE_SHFT                       0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_GENERAL_BITS_BMSK                  0x1e000000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_GENERAL_BITS_SHFT                        0x19
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_CONS_STATE_BMSK                     0x1c00000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_CONS_STATE_SHFT                          0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_PROD_SYS_STATE_BMSK                  0x380000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_PROD_SYS_STATE_SHFT                      0x13
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_PROD_B2B_STATE_BMSK                   0x70000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_PROD_B2B_STATE_SHFT                      0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_SIZE_BMSK                         0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_2_n_PSM_DESC_SIZE_SHFT                            0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_ADDR(n)                                (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x0000180c + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_PHYS(n)                                (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x0000180c + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_RMSK                                   0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_MAXn                                            7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_PSM_DESC_ADDR_BMSK                     0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_3_n_PSM_DESC_ADDR_SHFT                            0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_ADDR(n)                                (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001810 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_PHYS(n)                                (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001810 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_RMSK                                   0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_MAXn                                            7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_PSM_DESC_OFST_BMSK                     0xffff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_PSM_DESC_OFST_SHFT                           0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_PSM_SAVED_ACCUMED_SIZE_BMSK                0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_4_n_PSM_SAVED_ACCUMED_SIZE_SHFT                   0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_ADDR(n)                                (CRYPTO0_CRYPTO_BAM_REG_BASE      + 0x00001814 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_PHYS(n)                                (CRYPTO0_CRYPTO_BAM_REG_BASE_PHYS + 0x00001814 + 0x1000 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_RMSK                                   0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_MAXn                                            7
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_PSM_BLOCK_BYTE_CNT_BMSK                0xffff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_PSM_BLOCK_BYTE_CNT_SHFT                      0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_PSM_OFST_IN_DESC_BMSK                      0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_P_PSM_CNTXT_5_n_PSM_OFST_IN_DESC_SHFT                         0x0

/*----------------------------------------------------------------------------
 * MODULE: CRYPTO0_CRYPTO_BAM_VMIDMT_BAM
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE                                  (CRYPTO0_CRYPTO_TOP_BASE      + 0x00000000)
#define CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS                             (CRYPTO0_CRYPTO_TOP_BASE_PHYS + 0x00000000)

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_RMSK                                0x3ff707f5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_NSCFG_BMSK                          0x30000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_NSCFG_SHFT                                0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_WACFG_BMSK                           0xc000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_WACFG_SHFT                                0x1a
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_RACFG_BMSK                           0x3000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_RACFG_SHFT                                0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_SHCFG_BMSK                            0xc00000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_SHCFG_SHFT                                0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_SMCFCFG_BMSK                          0x200000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_SMCFCFG_SHFT                              0x15
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_MTCFG_BMSK                            0x100000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_MTCFG_SHFT                                0x14
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_MEMATTR_BMSK                           0x70000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_MEMATTR_SHFT                              0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_USFCFG_BMSK                              0x400
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_USFCFG_SHFT                                0xa
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_GSE_BMSK                                 0x200
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_GSE_SHFT                                   0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_STALLD_BMSK                              0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_STALLD_SHFT                                0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_TRANSIENTCFG_BMSK                         0xc0
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_TRANSIENTCFG_SHFT                          0x6
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_GCFGFIE_BMSK                              0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_GCFGFIE_SHFT                               0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_GCFGERE_BMSK                              0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_GCFGERE_SHFT                               0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_GFIE_BMSK                                  0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_GFIE_SHFT                                  0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_CLIENTPD_BMSK                              0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR0_CLIENTPD_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000004)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000004)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_RMSK                                 0x1000f00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_GASRAE_BMSK                          0x1000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_GASRAE_SHFT                               0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_NSNUMSMRGO_BMSK                          0xf00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR1_NSNUMSMRGO_SHFT                            0x8

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_RMSK                                      0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_BPVMID_BMSK                               0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SCR2_BPVMID_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_RMSK                                0x70000013
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPRCNSH_BMSK                        0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPRCNSH_SHFT                              0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPRCISH_BMSK                        0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPRCISH_SHFT                              0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPRCOSH_BMSK                        0x10000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPRCOSH_SHFT                              0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPREQPRIORITYCFG_BMSK                     0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPREQPRIORITYCFG_SHFT                      0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPREQPRIORITY_BMSK                         0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SACR_BPREQPRIORITY_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000020)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000020)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_RMSK                               0x88001eff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_SES_BMSK                           0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_SES_SHFT                                 0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_SMS_BMSK                            0x8000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_SMS_SHFT                                 0x1b
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_NUMSIDB_BMSK                           0x1e00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_NUMSIDB_SHFT                              0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_NUMSMRG_BMSK                             0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR0_NUMSMRG_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_RMSK                                   0x9f00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_SMCD_BMSK                              0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_SMCD_SHFT                                 0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_SSDTP_BMSK                             0x1000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_SSDTP_SHFT                                0xc
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_NUMSSDNDX_BMSK                          0xf00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR1_NUMSSDNDX_SHFT                            0x8

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_RMSK                                     0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_OAS_BMSK                                 0xf0
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_OAS_SHFT                                  0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_IAS_BMSK                                  0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR2_IAS_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_RMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_MAJOR_BMSK                         0xf0000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_MAJOR_SHFT                               0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_MINOR_BMSK                          0xfff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_MINOR_SHFT                               0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_STEP_BMSK                              0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR4_STEP_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000034)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000034)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_RMSK                                 0xff03ff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_NUMMSDRB_BMSK                        0xff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_NUMMSDRB_SHFT                            0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_MSAE_BMSK                               0x200
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_MSAE_SHFT                                 0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_QRIBE_BMSK                              0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_QRIBE_SHFT                                0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_NVMID_BMSK                               0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR5_NVMID_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_RMSK                                     0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_MAJOR_BMSK                               0xf0
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_MAJOR_SHFT                                0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_MINOR_BMSK                                0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SIDR7_MINOR_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_ADDR                              (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_PHYS                              (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_RMSK                              0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_SGFEA0_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFAR0_SGFEA0_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_RMSK                               0xc0000026
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_MULTI_CLIENT_BMSK                  0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_MULTI_CLIENT_SHFT                        0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_MULTI_CFG_BMSK                     0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_MULTI_CFG_SHFT                           0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_CAF_BMSK                                 0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_CAF_SHFT                                  0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_SMCF_BMSK                                 0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_SMCF_SHFT                                 0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_USF_BMSK                                  0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSR_USF_SHFT                                  0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_ADDR                        (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_PHYS                        (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_RMSK                        0xc0000026
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_MULTI_CLIENT_BMSK           0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_MULTI_CLIENT_SHFT                 0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_MULTI_CFG_BMSK              0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_MULTI_CFG_SHFT                    0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_CAF_BMSK                          0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_CAF_SHFT                           0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_SMCF_BMSK                          0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_SMCF_SHFT                          0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_USF_BMSK                           0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSRRESTORE_USF_SHFT                           0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_ADDR                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_PHYS                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_RMSK                                0x132
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_MSSSELFAUTH_BMSK                    0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_MSSSELFAUTH_SHFT                      0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_NSATTR_BMSK                          0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_NSATTR_SHFT                           0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_NSSTATE_BMSK                         0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_NSSTATE_SHFT                          0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_WNR_BMSK                              0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR0_WNR_SHFT                              0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_ADDR                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_PHYS                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_RMSK                            0xf0f000f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_MSDINDEX_BMSK                   0xf000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_MSDINDEX_SHFT                        0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_SSDINDEX_BMSK                     0xf0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_SSDINDEX_SHFT                        0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_STREAMINDEX_BMSK                      0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR1_STREAMINDEX_SHFT                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_ADDR                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_PHYS                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_RMSK                           0x3f1fffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_ATID_BMSK                      0x3f000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_ATID_SHFT                            0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_AVMID_BMSK                       0x1f0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_AVMID_SHFT                           0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_ABID_BMSK                          0xe000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_ABID_SHFT                             0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_APID_BMSK                          0x1f00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_APID_SHFT                             0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_AMID_BMSK                            0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SGFSYNDR2_AMID_SHFT                             0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_ADDR                          (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000090)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_PHYS                          (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000090)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_RMSK                                 0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_CLKONOFFE_BMSK                       0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTSCR0_CLKONOFFE_SHFT                       0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_ADDR                                 (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_PHYS                                 (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_RMSK                                  0xff70ff5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_WACFG_BMSK                            0xc000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_WACFG_SHFT                                 0x1a
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_RACFG_BMSK                            0x3000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_RACFG_SHFT                                 0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_SHCFG_BMSK                             0xc00000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_SHCFG_SHFT                                 0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_SMCFCFG_BMSK                           0x200000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_SMCFCFG_SHFT                               0x15
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_MTCFG_BMSK                             0x100000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_MTCFG_SHFT                                 0x14
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_MEMATTR_BMSK                            0x70000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_MEMATTR_SHFT                               0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_VMIDPNE_BMSK                              0x800
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_VMIDPNE_SHFT                                0xb
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_USFCFG_BMSK                               0x400
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_USFCFG_SHFT                                 0xa
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_GSE_BMSK                                  0x200
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_GSE_SHFT                                    0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_STALLD_BMSK                               0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_STALLD_SHFT                                 0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_TRANSIENTCFG_BMSK                          0xc0
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_TRANSIENTCFG_SHFT                           0x6
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_GCFGFIE_BMSK                               0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_GCFGFIE_SHFT                                0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_GCFGERE_BMSK                               0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_GCFGERE_SHFT                                0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_GFIE_BMSK                                   0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_GFIE_SHFT                                   0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_CLIENTPD_BMSK                               0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR0_CLIENTPD_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_ADDR                                 (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_PHYS                                 (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000008)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_RMSK                                       0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_BPVMID_BMSK                                0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_CR2_BPVMID_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_ADDR                                 (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_PHYS                                 (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000010)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_RMSK                                 0x70000013
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPRCNSH_BMSK                         0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPRCNSH_SHFT                               0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPRCISH_BMSK                         0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPRCISH_SHFT                               0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPRCOSH_BMSK                         0x10000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPRCOSH_SHFT                               0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPREQPRIORITYCFG_BMSK                      0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPREQPRIORITYCFG_SHFT                       0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPREQPRIORITY_BMSK                          0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_ACR_BPREQPRIORITY_SHFT                          0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000020)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000020)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_RMSK                                 0x8001eff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_SMS_BMSK                             0x8000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_SMS_SHFT                                  0x1b
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_NUMSIDB_BMSK                            0x1e00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_NUMSIDB_SHFT                               0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_NUMSMRG_BMSK                              0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR0_NUMSMRG_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000024)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_RMSK                                    0x9f00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_SMCD_BMSK                               0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_SMCD_SHFT                                  0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_SSDTP_BMSK                              0x1000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_SSDTP_SHFT                                 0xc
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_NUMSSDNDX_BMSK                           0xf00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR1_NUMSSDNDX_SHFT                             0x8

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000028)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_RMSK                                      0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_OAS_BMSK                                  0xf0
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_OAS_SHFT                                   0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_IAS_BMSK                                   0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR2_IAS_SHFT                                   0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000030)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_RMSK                                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_MAJOR_BMSK                          0xf0000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_MAJOR_SHFT                                0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_MINOR_BMSK                           0xfff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_MINOR_SHFT                                0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_STEP_BMSK                               0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR4_STEP_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000034)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000034)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_RMSK                                  0xff03ff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_NUMMSDRB_BMSK                         0xff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_NUMMSDRB_SHFT                             0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_MSAE_BMSK                                0x200
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_MSAE_SHFT                                  0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_QRIBE_BMSK                               0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_QRIBE_SHFT                                 0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_NVMID_BMSK                                0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR5_NVMID_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x0000003c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_RMSK                                      0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_MAJOR_BMSK                                0xf0
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_MAJOR_SHFT                                 0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_MINOR_BMSK                                 0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_IDR7_MINOR_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_RMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_GFEA0_BMSK                         0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFAR0_GFEA0_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_ADDR                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_PHYS                                (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_RMSK                                0xc00000a6
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_MULTI_CLIENT_BMSK                   0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_MULTI_CLIENT_SHFT                         0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_MULTI_CFG_BMSK                      0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_MULTI_CFG_SHFT                            0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_PF_BMSK                                   0x80
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_PF_SHFT                                    0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_CAF_BMSK                                  0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_CAF_SHFT                                   0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_SMCF_BMSK                                  0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_SMCF_SHFT                                  0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_USF_BMSK                                   0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSR_USF_SHFT                                   0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_ADDR                         (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_PHYS                         (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_RMSK                         0xc00000a6
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_MULTI_CLIENT_BMSK            0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_MULTI_CLIENT_SHFT                  0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_MULTI_CFG_BMSK               0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_MULTI_CFG_SHFT                     0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_PF_BMSK                            0x80
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_PF_SHFT                             0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_CAF_BMSK                           0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_CAF_SHFT                            0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_SMCF_BMSK                           0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_SMCF_SHFT                           0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_USF_BMSK                            0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSRRESTORE_USF_SHFT                            0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_ADDR                            (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_PHYS                            (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_RMSK                                 0x132
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_MSSSELFAUTH_BMSK                     0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_MSSSELFAUTH_SHFT                       0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_NSATTR_BMSK                           0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_NSATTR_SHFT                            0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_NSSTATE_BMSK                          0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_NSSTATE_SHFT                           0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_WNR_BMSK                               0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR0_WNR_SHFT                               0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_ADDR                            (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_PHYS                            (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_RMSK                             0xf0f000f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_MSDINDEX_BMSK                    0xf000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_MSDINDEX_SHFT                         0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_SSDINDEX_BMSK                      0xf0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_SSDINDEX_SHFT                         0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_STREAMINDEX_BMSK                       0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR1_STREAMINDEX_SHFT                       0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_ADDR                            (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_PHYS                            (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_RMSK                            0x3f1fffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_ATID_BMSK                       0x3f000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_ATID_SHFT                             0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_AVMID_BMSK                        0x1f0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_AVMID_SHFT                            0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_ABID_BMSK                           0xe000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_ABID_SHFT                              0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_APID_BMSK                           0x1f00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_APID_SHFT                              0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_AMID_BMSK                             0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_GFSYNDR2_AMID_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_ADDR                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000090)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_PHYS                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000090)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_RMSK                                  0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_CLKONOFFE_BMSK                        0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTCR0_CLKONOFFE_SHFT                        0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_ADDR                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x0000009c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_PHYS                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x0000009c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_RMSK                           0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_RWE_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_VMIDMTACR_RWE_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000400)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000400)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_RMSK                                0xff70ff5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_WACFG_BMSK                          0xc000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_WACFG_SHFT                               0x1a
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_RACFG_BMSK                          0x3000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_RACFG_SHFT                               0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_SHCFG_BMSK                           0xc00000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_SHCFG_SHFT                               0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_SMCFCFG_BMSK                         0x200000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_SMCFCFG_SHFT                             0x15
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_MTCFG_BMSK                           0x100000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_MTCFG_SHFT                               0x14
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_MEMATTR_BMSK                          0x70000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_MEMATTR_SHFT                             0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_VMIDPNE_BMSK                            0x800
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_VMIDPNE_SHFT                              0xb
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_USFCFG_BMSK                             0x400
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_USFCFG_SHFT                               0xa
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_GSE_BMSK                                0x200
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_GSE_SHFT                                  0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_STALLD_BMSK                             0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_STALLD_SHFT                               0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_TRANSIENTCFG_BMSK                        0xc0
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_TRANSIENTCFG_SHFT                         0x6
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_GCFGFIE_BMSK                             0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_GCFGFIE_SHFT                              0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_GCFGERE_BMSK                             0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_GCFGERE_SHFT                              0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_GFIE_BMSK                                 0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_GFIE_SHFT                                 0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_CLIENTPD_BMSK                             0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR0_CLIENTPD_SHFT                             0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000408)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000408)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_RMSK                                     0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_BPVMID_BMSK                              0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSCR2_BPVMID_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000410)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000410)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_RMSK                               0x70000013
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPRCNSH_BMSK                       0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPRCNSH_SHFT                             0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPRCISH_BMSK                       0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPRCISH_SHFT                             0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPRCOSH_BMSK                       0x10000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPRCOSH_SHFT                             0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPREQPRIORITYCFG_BMSK                    0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPREQPRIORITYCFG_SHFT                     0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPREQPRIORITY_BMSK                        0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSACR_BPREQPRIORITY_SHFT                        0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_ADDR                             (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000440)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_PHYS                             (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000440)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_RMSK                             0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_GFEA0_BMSK                       0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFAR0_GFEA0_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_ADDR                              (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000448)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_PHYS                              (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000448)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_RMSK                              0xc00000a6
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_MULTI_CLIENT_BMSK                 0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_MULTI_CLIENT_SHFT                       0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_MULTI_CFG_BMSK                    0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_MULTI_CFG_SHFT                          0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_PF_BMSK                                 0x80
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_PF_SHFT                                  0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_CAF_BMSK                                0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_CAF_SHFT                                 0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_SMCF_BMSK                                0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_SMCF_SHFT                                0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_USF_BMSK                                 0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSR_USF_SHFT                                 0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_ADDR                       (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x0000044c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_PHYS                       (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x0000044c)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_RMSK                       0xc00000a6
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_MULTI_CLIENT_BMSK          0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_MULTI_CLIENT_SHFT                0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_MULTI_CFG_BMSK             0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_MULTI_CFG_SHFT                   0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_PF_BMSK                          0x80
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_PF_SHFT                           0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_CAF_BMSK                         0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_CAF_SHFT                          0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_SMCF_BMSK                         0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_SMCF_SHFT                         0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_USF_BMSK                          0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSRRESTORE_USF_SHFT                          0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_ADDR                          (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000450)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_PHYS                          (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000450)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_RMSK                               0x132
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_MSSSELFAUTH_BMSK                   0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_MSSSELFAUTH_SHFT                     0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_NSATTR_BMSK                         0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_NSATTR_SHFT                          0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_NSSTATE_BMSK                        0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_NSSTATE_SHFT                         0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_WNR_BMSK                             0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR0_WNR_SHFT                             0x1

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_ADDR                          (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000454)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_PHYS                          (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000454)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_RMSK                           0xf0f000f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_MSDINDEX_BMSK                  0xf000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_MSDINDEX_SHFT                       0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_SSDINDEX_BMSK                    0xf0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_SSDINDEX_SHFT                       0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_STREAMINDEX_BMSK                     0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR1_STREAMINDEX_SHFT                     0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_ADDR                          (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000458)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_PHYS                          (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000458)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_RMSK                          0x3f1fffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_ATID_BMSK                     0x3f000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_ATID_SHFT                           0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_AVMID_BMSK                      0x1f0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_AVMID_SHFT                          0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_ABID_BMSK                         0xe000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_ABID_SHFT                            0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_APID_BMSK                         0x1f00
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_APID_SHFT                            0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_AMID_BMSK                           0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSGFSYNDR2_AMID_SHFT                            0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_ADDR                         (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000490)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_PHYS                         (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000490)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_RMSK                                0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_CLKONOFFE_BMSK                      0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_NSVMIDMTCR0_CLKONOFFE_SHFT                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000080)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000080)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_RMSK                                   0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_RWE_BMSK                               0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SSDR0_RWE_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_ADDR                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000480)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_PHYS                               (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000480)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_RMSK                                    0x1ff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_RWE_BMSK                                0x1ff
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MSDR0_RWE_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_ADDR                                 (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000494)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_PHYS                                 (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000494)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_RMSK                                        0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_CLKONOFFE_BMSK                              0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_CLKONOFFE_SHFT                              0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_BPMSACFG_BMSK                               0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_BPMSACFG_SHFT                               0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_BPSMSACFG_BMSK                              0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_MCR_BPSMSACFG_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_ADDR(n)                            (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000c00 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_PHYS(n)                            (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000c00 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_RMSK                               0x30ff7b1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_MAXn                                        8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_TRANSIENTCFG_BMSK                  0x30000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_TRANSIENTCFG_SHFT                        0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_WACFG_BMSK                           0xc00000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_WACFG_SHFT                               0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_RACFG_BMSK                           0x300000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_RACFG_SHFT                               0x14
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_NSCFG_BMSK                            0xc0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_NSCFG_SHFT                               0x12
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_TYPE_BMSK                             0x30000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_TYPE_SHFT                                0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_MEMATTR_BMSK                           0x7000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_MEMATTR_SHFT                              0xc
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_MTCFG_BMSK                              0x800
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_MTCFG_SHFT                                0xb
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_SHCFG_BMSK                              0x300
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_SHCFG_SHFT                                0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_VMID_BMSK                                0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_S2VRn_VMID_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_ADDR(n)                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000e00 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_PHYS(n)                           (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000e00 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_RMSK                              0x70000013
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_MAXn                                       8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_RCNSH_BMSK                        0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_RCNSH_SHFT                              0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_RCISH_BMSK                        0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_RCISH_SHFT                              0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_RCOSH_BMSK                        0x10000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_RCOSH_SHFT                              0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_REQPRIORITYCFG_BMSK                     0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_REQPRIORITYCFG_SHFT                      0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_REQPRIORITY_BMSK                         0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_AS2VRn_REQPRIORITY_SHFT                         0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_ADDR(n)                             (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE      + 0x00000800 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_PHYS(n)                             (CRYPTO0_CRYPTO_BAM_VMIDMT_BAM_REG_BASE_PHYS + 0x00000800 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_RMSK                                0x800f000f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_MAXn                                         8
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_VALID_BMSK                          0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_VALID_SHFT                                0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_MASK_BMSK                              0xf0000
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_MASK_SHFT                                 0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_ID_BMSK                                    0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_VMIDMT_SMRn_ID_SHFT                                    0x0

/*----------------------------------------------------------------------------
 * MODULE: CRYPTO0_CRYPTO_BAM_XPU2_BAM
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE                                    (CRYPTO0_CRYPTO_TOP_BASE      + 0x00002000)
#define CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS                               (CRYPTO0_CRYPTO_TOP_BASE_PHYS + 0x00002000)

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_ADDR                                    (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_PHYS                                    (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000000)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_RMSK                                         0x77f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SCLEIE_BMSK                                  0x400
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SCLEIE_SHFT                                    0xa
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SCFGEIE_BMSK                                 0x200
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SCFGEIE_SHFT                                   0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_DYNAMIC_CLK_EN_BMSK                          0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_DYNAMIC_CLK_EN_SHFT                            0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_NSRGCLEE_BMSK                                 0x40
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_NSRGCLEE_SHFT                                  0x6
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_NSCFGE_BMSK                                   0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_NSCFGE_SHFT                                    0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SDCDEE_BMSK                                   0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SDCDEE_SHFT                                    0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SEIE_BMSK                                      0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SEIE_SHFT                                      0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SCLERE_BMSK                                    0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SCLERE_SHFT                                    0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SCFGERE_BMSK                                   0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_SCFGERE_SHFT                                   0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_XPUNSE_BMSK                                    0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SCR_XPUNSE_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_ADDR                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000004)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_PHYS                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000004)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_RMSK                                          0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_SCFGWD_BMSK                                   0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SWDR_SCFGWD_SHFT                                   0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_ADDR                                  (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_PHYS                                  (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000040)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_RMSK                                  0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_PA_BMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SEAR0_PA_SHFT                                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_ADDR                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_PHYS                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000048)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_RMSK                                   0x8000000f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_MULTI_BMSK                             0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_MULTI_SHFT                                   0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_CLMULTI_BMSK                                  0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_CLMULTI_SHFT                                  0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_CFGMULTI_BMSK                                 0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_CFGMULTI_SHFT                                 0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_CLIENT_BMSK                                   0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_CLIENT_SHFT                                   0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_CFG_BMSK                                      0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESR_CFG_SHFT                                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_ADDR                            (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_PHYS                            (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x0000004c)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_RMSK                            0x8000000f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_MULTI_BMSK                      0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_MULTI_SHFT                            0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_CLMULTI_BMSK                           0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_CLMULTI_SHFT                           0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_CFGMULTI_BMSK                          0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_CFGMULTI_SHFT                          0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_CLIENT_BMSK                            0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_CLIENT_SHFT                            0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_CFG_BMSK                               0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESRRESTORE_CFG_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_ADDR                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_PHYS                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000050)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_RMSK                                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_ATID_BMSK                           0xff000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_ATID_SHFT                                 0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_AVMID_BMSK                            0xff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_AVMID_SHFT                                0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_ABID_BMSK                               0xe000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_ABID_SHFT                                  0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_APID_BMSK                               0x1f00
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_APID_SHFT                                  0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_AMID_BMSK                                 0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR0_AMID_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ADDR                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_PHYS                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000054)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_RMSK                                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_DCD_BMSK                            0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_DCD_SHFT                                  0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AC_BMSK                             0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AC_SHFT                                   0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_BURSTLEN_BMSK                       0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_BURSTLEN_SHFT                             0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ARDALLOCATE_BMSK                    0x10000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ARDALLOCATE_SHFT                          0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ABURST_BMSK                          0x8000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ABURST_SHFT                               0x1b
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AEXCLUSIVE_BMSK                      0x4000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AEXCLUSIVE_SHFT                           0x1a
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AWRITE_BMSK                          0x2000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AWRITE_SHFT                               0x19
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AFULL_BMSK                           0x1000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AFULL_SHFT                                0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ARDBEADNDXEN_BMSK                     0x800000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ARDBEADNDXEN_SHFT                         0x17
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AOOO_BMSK                             0x400000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AOOO_SHFT                                 0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_APREQPRIORITY_BMSK                    0x380000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_APREQPRIORITY_SHFT                        0x13
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ASIZE_BMSK                             0x70000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ASIZE_SHFT                                0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AMSSSELFAUTH_BMSK                       0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AMSSSELFAUTH_SHFT                          0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ALEN_BMSK                               0x7f00
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ALEN_SHFT                                  0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AINST_BMSK                                0x80
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AINST_SHFT                                 0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_APROTNS_BMSK                              0x40
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_APROTNS_SHFT                               0x6
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_APRIV_BMSK                                0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_APRIV_SHFT                                 0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AINNERSHARED_BMSK                         0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AINNERSHARED_SHFT                          0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ASHARED_BMSK                               0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_ASHARED_SHFT                               0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AMEMTYPE_BMSK                              0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR1_AMEMTYPE_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_ADDR                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_PHYS                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000058)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_RMSK                                       0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_MODEM_PRT_HIT_BMSK                         0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_MODEM_PRT_HIT_SHFT                         0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_SECURE_PRT_HIT_BMSK                        0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_SECURE_PRT_HIT_SHFT                        0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_NONSECURE_PRT_HIT_BMSK                     0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_SESYNR2_NONSECURE_PRT_HIT_SHFT                     0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_ADDR                                    (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000100)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_PHYS                                    (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000100)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_RMSK                                         0x71f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_CLEIE_BMSK                                   0x400
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_CLEIE_SHFT                                     0xa
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_CFGEIE_BMSK                                  0x200
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_CFGEIE_SHFT                                    0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_DYNAMIC_CLK_EN_BMSK                          0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_DYNAMIC_CLK_EN_SHFT                            0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_DCDEE_BMSK                                    0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_DCDEE_SHFT                                     0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_EIE_BMSK                                       0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_EIE_SHFT                                       0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_CLERE_BMSK                                     0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_CLERE_SHFT                                     0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_CFGERE_BMSK                                    0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_CFGERE_SHFT                                    0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_XPUMSAE_BMSK                                   0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MCR_XPUMSAE_SHFT                                   0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_ADDR                                  (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000140)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_PHYS                                  (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000140)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_RMSK                                  0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_PA_BMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MEAR0_PA_SHFT                                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_ADDR                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000148)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_PHYS                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000148)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_RMSK                                   0x8000000f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_MULTI_BMSK                             0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_MULTI_SHFT                                   0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_CLMULTI_BMSK                                  0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_CLMULTI_SHFT                                  0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_CFGMULTI_BMSK                                 0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_CFGMULTI_SHFT                                 0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_CLIENT_BMSK                                   0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_CLIENT_SHFT                                   0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_CFG_BMSK                                      0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESR_CFG_SHFT                                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_ADDR                            (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x0000014c)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_PHYS                            (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x0000014c)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_RMSK                            0x8000000f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_MULTI_BMSK                      0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_MULTI_SHFT                            0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_CLMULTI_BMSK                           0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_CLMULTI_SHFT                           0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_CFGMULTI_BMSK                          0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_CFGMULTI_SHFT                          0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_CLIENT_BMSK                            0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_CLIENT_SHFT                            0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_CFG_BMSK                               0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESRRESTORE_CFG_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_ADDR                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000150)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_PHYS                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000150)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_RMSK                                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_ATID_BMSK                           0xff000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_ATID_SHFT                                 0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_AVMID_BMSK                            0xff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_AVMID_SHFT                                0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_ABID_BMSK                               0xe000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_ABID_SHFT                                  0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_APID_BMSK                               0x1f00
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_APID_SHFT                                  0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_AMID_BMSK                                 0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR0_AMID_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ADDR                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000154)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_PHYS                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000154)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_RMSK                                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_DCD_BMSK                            0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_DCD_SHFT                                  0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AC_BMSK                             0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AC_SHFT                                   0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_BURSTLEN_BMSK                       0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_BURSTLEN_SHFT                             0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ARDALLOCATE_BMSK                    0x10000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ARDALLOCATE_SHFT                          0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ABURST_BMSK                          0x8000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ABURST_SHFT                               0x1b
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AEXCLUSIVE_BMSK                      0x4000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AEXCLUSIVE_SHFT                           0x1a
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AWRITE_BMSK                          0x2000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AWRITE_SHFT                               0x19
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AFULL_BMSK                           0x1000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AFULL_SHFT                                0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ARDBEADNDXEN_BMSK                     0x800000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ARDBEADNDXEN_SHFT                         0x17
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AOOO_BMSK                             0x400000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AOOO_SHFT                                 0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_APREQPRIORITY_BMSK                    0x380000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_APREQPRIORITY_SHFT                        0x13
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ASIZE_BMSK                             0x70000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ASIZE_SHFT                                0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AMSSSELFAUTH_BMSK                       0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AMSSSELFAUTH_SHFT                          0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ALEN_BMSK                               0x7f00
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ALEN_SHFT                                  0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AINST_BMSK                                0x80
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AINST_SHFT                                 0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_APROTNS_BMSK                              0x40
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_APROTNS_SHFT                               0x6
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_APRIV_BMSK                                0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_APRIV_SHFT                                 0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AINNERSHARED_BMSK                         0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AINNERSHARED_SHFT                          0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ASHARED_BMSK                               0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_ASHARED_SHFT                               0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AMEMTYPE_BMSK                              0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR1_AMEMTYPE_SHFT                              0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_ADDR                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000158)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_PHYS                                (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000158)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_RMSK                                       0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_MODEM_PRT_HIT_BMSK                         0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_MODEM_PRT_HIT_SHFT                         0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_SECURE_PRT_HIT_BMSK                        0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_SECURE_PRT_HIT_SHFT                        0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_NONSECURE_PRT_HIT_BMSK                     0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_MESYNR2_NONSECURE_PRT_HIT_SHFT                     0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_ADDR                                     (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000080)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_PHYS                                     (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000080)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_RMSK                                          0x71f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_CLEIE_BMSK                                    0x400
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_CLEIE_SHFT                                      0xa
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_CFGEIE_BMSK                                   0x200
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_CFGEIE_SHFT                                     0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_DYNAMIC_CLK_EN_BMSK                           0x100
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_DYNAMIC_CLK_EN_SHFT                             0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_DCDEE_BMSK                                     0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_DCDEE_SHFT                                      0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_EIE_BMSK                                        0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_EIE_SHFT                                        0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_CLERE_BMSK                                      0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_CLERE_SHFT                                      0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_CFGERE_BMSK                                     0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_CFGERE_SHFT                                     0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_XPUVMIDE_BMSK                                   0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_CR_XPUVMIDE_SHFT                                   0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_ADDR(n)                            (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x000000a0 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_PHYS(n)                            (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x000000a0 + 0x4 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_RMSK                               0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_MAXn                                        0
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_RWE_BMSK                           0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RPU_ACRn_RWE_SHFT                                  0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_ADDR                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x000000c0)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_PHYS                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x000000c0)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_RMSK                                   0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_PA_BMSK                                0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_EAR0_PA_SHFT                                       0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_ADDR                                    (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x000000c8)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_PHYS                                    (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x000000c8)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_RMSK                                    0x8000000f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_MULTI_BMSK                              0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_MULTI_SHFT                                    0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_CLMULTI_BMSK                                   0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_CLMULTI_SHFT                                   0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_CFGMULTI_BMSK                                  0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_CFGMULTI_SHFT                                  0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_CLIENT_BMSK                                    0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_CLIENT_SHFT                                    0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_CFG_BMSK                                       0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESR_CFG_SHFT                                       0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_ADDR                             (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x000000cc)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_PHYS                             (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x000000cc)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_RMSK                             0x8000000f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_OUT(v)      \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_ADDR,v)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_OUTM(m,v) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_ADDR,m,v,HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_IN)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_MULTI_BMSK                       0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_MULTI_SHFT                             0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_CLMULTI_BMSK                            0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_CLMULTI_SHFT                            0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_CFGMULTI_BMSK                           0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_CFGMULTI_SHFT                           0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_CLIENT_BMSK                             0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_CLIENT_SHFT                             0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_CFG_BMSK                                0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESRRESTORE_CFG_SHFT                                0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_ADDR                                 (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x000000d0)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_PHYS                                 (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x000000d0)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_RMSK                                 0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_ATID_BMSK                            0xff000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_ATID_SHFT                                  0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_AVMID_BMSK                             0xff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_AVMID_SHFT                                 0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_ABID_BMSK                                0xe000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_ABID_SHFT                                   0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_APID_BMSK                                0x1f00
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_APID_SHFT                                   0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_AMID_BMSK                                  0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR0_AMID_SHFT                                   0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ADDR                                 (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x000000d4)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_PHYS                                 (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x000000d4)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_RMSK                                 0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_DCD_BMSK                             0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_DCD_SHFT                                   0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AC_BMSK                              0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AC_SHFT                                    0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_BURSTLEN_BMSK                        0x20000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_BURSTLEN_SHFT                              0x1d
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ARDALLOCATE_BMSK                     0x10000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ARDALLOCATE_SHFT                           0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ABURST_BMSK                           0x8000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ABURST_SHFT                                0x1b
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AEXCLUSIVE_BMSK                       0x4000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AEXCLUSIVE_SHFT                            0x1a
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AWRITE_BMSK                           0x2000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AWRITE_SHFT                                0x19
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AFULL_BMSK                            0x1000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AFULL_SHFT                                 0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ARDBEADNDXEN_BMSK                      0x800000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ARDBEADNDXEN_SHFT                          0x17
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AOOO_BMSK                              0x400000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AOOO_SHFT                                  0x16
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_APREQPRIORITY_BMSK                     0x380000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_APREQPRIORITY_SHFT                         0x13
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ASIZE_BMSK                              0x70000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ASIZE_SHFT                                 0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AMSSSELFAUTH_BMSK                        0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AMSSSELFAUTH_SHFT                           0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ALEN_BMSK                                0x7f00
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ALEN_SHFT                                   0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AINST_BMSK                                 0x80
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AINST_SHFT                                  0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_APROTNS_BMSK                               0x40
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_APROTNS_SHFT                                0x6
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_APRIV_BMSK                                 0x20
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_APRIV_SHFT                                  0x5
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AINNERSHARED_BMSK                          0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AINNERSHARED_SHFT                           0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ASHARED_BMSK                                0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_ASHARED_SHFT                                0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AMEMTYPE_BMSK                               0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR1_AMEMTYPE_SHFT                               0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_ADDR                                 (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x000000d8)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_PHYS                                 (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x000000d8)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_RMSK                                        0x7
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_MODEM_PRT_HIT_BMSK                          0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_MODEM_PRT_HIT_SHFT                          0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_SECURE_PRT_HIT_BMSK                         0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_SECURE_PRT_HIT_SHFT                         0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_NONSECURE_PRT_HIT_BMSK                      0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_ESYNR2_NONSECURE_PRT_HIT_SHFT                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_ADDR                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000074)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_PHYS                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000074)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_RMSK                                   0xc000bfff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_CLIENTREQ_HALT_ACK_HW_EN_BMSK          0x80000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_CLIENTREQ_HALT_ACK_HW_EN_SHFT                0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_SAVERESTORE_HW_EN_BMSK                 0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_SAVERESTORE_HW_EN_SHFT                       0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_BLED_BMSK                                  0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_BLED_SHFT                                     0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_XPUT_BMSK                                  0x3000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_XPUT_SHFT                                     0xc
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_PT_BMSK                                     0x800
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_PT_SHFT                                       0xb
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_MV_BMSK                                     0x400
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_MV_SHFT                                       0xa
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_NRG_BMSK                                    0x3ff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR0_NRG_SHFT                                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_ADDR                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000078)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_PHYS                                   (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000078)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_RMSK                                   0x7f3ffeff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_AMT_HW_ENABLE_BMSK                     0x40000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_AMT_HW_ENABLE_SHFT                           0x1e
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_CLIENT_ADDR_WIDTH_BMSK                 0x3f000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_CLIENT_ADDR_WIDTH_SHFT                       0x18
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_CONFIG_ADDR_WIDTH_BMSK                   0x3f0000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_CONFIG_ADDR_WIDTH_SHFT                       0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_QRIB_EN_BMSK                               0x8000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_QRIB_EN_SHFT                                  0xf
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_ASYNC_MODE_BMSK                            0x4000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_ASYNC_MODE_SHFT                               0xe
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_CONFIG_TYPE_BMSK                           0x2000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_CONFIG_TYPE_SHFT                              0xd
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_CLIENT_PIPELINE_ENABLED_BMSK               0x1000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_CLIENT_PIPELINE_ENABLED_SHFT                  0xc
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_MSA_CHECK_HW_ENABLE_BMSK                    0x800
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_MSA_CHECK_HW_ENABLE_SHFT                      0xb
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_XPU_SYND_REG_ABSENT_BMSK                    0x400
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_XPU_SYND_REG_ABSENT_SHFT                      0xa
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_TZXPU_BMSK                                  0x200
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_TZXPU_SHFT                                    0x9
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_NVMID_BMSK                                   0xff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_IDR1_NVMID_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_ADDR                                    (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x0000007c)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_PHYS                                    (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x0000007c)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_RMSK                                    0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_IN          \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_ADDR, HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_INM(m)      \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_ADDR, m)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_MAJOR_BMSK                              0xf0000000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_MAJOR_SHFT                                    0x1c
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_MINOR_BMSK                               0xfff0000
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_MINOR_SHFT                                    0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_STEP_BMSK                                   0xffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_REV_STEP_SHFT                                      0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_ADDR(n,m)                         (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000200 + 0x80 * (n)+0x4 * (m))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_PHYS(n,m)                         (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000200 + 0x80 * (n)+0x4 * (m))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_RMSK                              0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_MAXn                                      20
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_MAXm                                       0
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_INI2(n,m)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_ADDR(n,m), HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_INMI2(n,m,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_ADDR(n,m), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_OUTI2(n,m,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_ADDR(n,m),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_OUTMI2(n,m,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_ADDR(n,m),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_INI2(n,m))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_RWE_BMSK                          0xffffffff
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_RACRm_RWE_SHFT                                 0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_ADDR(n)                             (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000250 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_PHYS(n)                             (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000250 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_RMSK                                       0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_MAXn                                        20
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_NS_BMSK                                    0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_SCR_NS_SHFT                                    0x0

#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_ADDR(n)                             (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE      + 0x00000254 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_PHYS(n)                             (CRYPTO0_CRYPTO_BAM_XPU2_BAM_REG_BASE_PHYS + 0x00000254 + 0x80 * (n))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_RMSK                                      0x1f
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_MAXn                                        20
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_INI(n)        \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_ADDR(n), HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_RMSK)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_INMI(n,mask)    \
        in_dword_masked(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_ADDR(n), mask)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_OUTI(n,val)    \
        out_dword(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_ADDR(n),val)
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_ADDR(n),mask,val,HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_INI(n))
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_VMIDCLROE_BMSK                            0x10
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_VMIDCLROE_SHFT                             0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_SCLROE_BMSK                                0x8
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_SCLROE_SHFT                                0x3
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_VMIDCLE_BMSK                               0x4
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_VMIDCLE_SHFT                               0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_SCLE_BMSK                                  0x2
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_SCLE_SHFT                                  0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_MSAE_BMSK                                  0x1
#define HWIO_CRYPTO0_CRYPTO_BAM_XPU_RGn_MCR_MSAE_SHFT                                  0x0


#endif /* __CE0_MSMHWIOREG_H__ */
