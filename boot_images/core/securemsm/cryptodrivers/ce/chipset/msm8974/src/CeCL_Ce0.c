/**
@file CeCL_Ce0.c 
@brief Crypto Engine Core Library source file 
*/

/*===========================================================================

                     Crypto Engine Core Library 

DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright (c) 2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

 $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/chipset/msm8974/src/CeCL_Ce0.c#1 $
 $DateTime: 2015/03/19 01:58:37 $
 $Author: pwbldsvc $ 

when         who     what, where, why
----------   ---     ---------------------------------------------------------- 
07/31/12      nk      Boot versi
07/05/12      nk     Initial version
============================================================================*/

#include "CeCL.h"
#include "CeEL.h"
#include "CeCL_Target.h"
#include "DALDeviceId.h"
#include "DALSys.h"
#include "DALSysTypes.h"
#include "DDIHWIO.h"
#include "ClockBoot.h"

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClClockEnable(void)
{
  // clock init  
  Clock_InitCrypto();

  // boot memory barrier
  CeElMemoryBarrier();

  return CECL_ERROR_SUCCESS;
}

/**
 * @brief 
 *        
 *
 * @return None
 *
 * @see 
 *
 */
CeCLErrorType CeClClockDisable(void)
{
  
#if 0
  // clock de-init  
  Clock_DeInitCrypto();

  // boot memory barrier 
  /CeElMemoryBarrier();
#endif

  return CECL_ERROR_SUCCESS; 
}



