/*===========================================================================

                    Crypto Engine Core Environment

GENERAL DESCRIPTION

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2000-2012 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================
                      EDIT HISTORY FOR FILE
 
  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/securemsm/cryptodrivers/ce/chipset/msm8974/inc/CeCL_Env.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
10/29/12   ejt     original file
===========================================================================*/

/*===========================================================================
                     INCLUDE FILES FOR MODULE
===========================================================================*/
#include "CeCL_Target.h"

/*===========================================================================
                 DEFINITIONS AND TYPE DECLARATIONS
===========================================================================*/

#define CECL_BAM_MODE_IS_POLLING() 0

#define CECL_BAM_IS_SUPPORTED() 0

#define CECL_NON_BLOCKING_HASH_IS_SUPPORTED() 0

//Define CE/BAM V1 HW version numbers
#define CEEL_V1_MAJ_VER 5
#define CEEL_V1_MIN_VER 0

//Define CE/BAM V1 HW version macro. Resolves to true if HW is V1
//temporary workaround to allow v2 run v1 config
#define CEEL_CE_HW_IS_V1()  ((HWIO_INF(CECL_CE_VERSION, MAJ_VER) == CEEL_V1_MAJ_VER) && \
                             (HWIO_INF(CECL_CE_VERSION, MIN_VER) == CEEL_V1_MIN_VER))
