#ifndef QFPROM_MAINPAGE_H
#define QFPROM_MAINPAGE_H

/**
  @file qfprom_mainpage.h
  @brief
  This is a Doxygen template defining the structure of the QFPROM API auto-generated
  PDF file. 
*/
/*=============================================================================
  Copyright (c) 2010 - 2011 Qualcomm Technologies Incorporated.
  All rights reserved.
  Qualcomm Confidential and Proprietary.
=============================================================================*/
/*============================================================================
                             Edit History 
 
$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/boot/docsrc/qfprom/qfprom_mainpage.h#1 $ 
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $

  when       who      what, where, why 
  --------   ----     --------------------------------------------------------
  03/08/11   ds       (Tech Pubs) Edited/added Latex comments and markup.
  11/17/10   dxiang   Initial Version
==============================================================================*/ 

/** 
@defgroup qfprom_API QFPROM APIs
@brief This section describes the QFPROM API functionality.


*/

#endif /* #ifndef QFPROM_MAINPAGE_H */
