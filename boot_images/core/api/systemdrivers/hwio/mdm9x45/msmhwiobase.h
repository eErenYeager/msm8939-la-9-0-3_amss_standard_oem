#ifndef __MSMHWIOBASE_H__
#define __MSMHWIOBASE_H__
/*
===========================================================================
*/
/**
  @file msmhwiobase.h
  @brief Auto-generated HWIO base include file.
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/hwio/mdm9x45/msmhwiobase.h#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * BASE: PRNG_PRNG
 *--------------------------------------------------------------------------*/

#define PRNG_PRNG_BASE                                              0x00022000
#define PRNG_PRNG_BASE_SIZE                                         0x00000140
#define PRNG_PRNG_BASE_PHYS                                         0x00022000

/*----------------------------------------------------------------------------
 * BASE: SPDM_WRAPPER_TOP
 *--------------------------------------------------------------------------*/

#define SPDM_WRAPPER_TOP_BASE                                       0x00040000
#define SPDM_WRAPPER_TOP_BASE_SIZE                                  0x00006000
#define SPDM_WRAPPER_TOP_BASE_PHYS                                  0x00040000

/*----------------------------------------------------------------------------
 * BASE: SECURITY_CONTROL
 *--------------------------------------------------------------------------*/

#define SECURITY_CONTROL_BASE                                       0x00058000
#define SECURITY_CONTROL_BASE_SIZE                                  0x00008000
#define SECURITY_CONTROL_BASE_PHYS                                  0x00058000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_START_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_START_ADDRESS_BASE                           0x00060000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_SIZE                      0x00004000
#define RPM_SS_MSG_RAM_START_ADDRESS_BASE_PHYS                      0x00060000

/*----------------------------------------------------------------------------
 * BASE: RPM_SS_MSG_RAM_END_ADDRESS
 *--------------------------------------------------------------------------*/

#define RPM_SS_MSG_RAM_END_ADDRESS_BASE                             0x00067fff
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_SIZE                        0x00000000
#define RPM_SS_MSG_RAM_END_ADDRESS_BASE_PHYS                        0x00067fff

/*----------------------------------------------------------------------------
 * BASE: BOOT_ROM
 *--------------------------------------------------------------------------*/

#define BOOT_ROM_BASE                                               0x00100000
#define BOOT_ROM_BASE_SIZE                                          0x00100000
#define BOOT_ROM_BASE_PHYS                                          0x00100000

/*----------------------------------------------------------------------------
 * BASE: RPM
 *--------------------------------------------------------------------------*/

#define RPM_BASE                                                    0x00200000
#define RPM_BASE_SIZE                                               0x00090000
#define RPM_BASE_PHYS                                               0x00200000

/*----------------------------------------------------------------------------
 * BASE: BIMC
 *--------------------------------------------------------------------------*/

#define BIMC_BASE                                                   0x00400000
#define BIMC_BASE_SIZE                                              0x00070000
#define BIMC_BASE_PHYS                                              0x00400000

/*----------------------------------------------------------------------------
 * BASE: EBI1_PHY_CFG
 *--------------------------------------------------------------------------*/

#define EBI1_PHY_CFG_BASE                                           0x00480000
#define EBI1_PHY_CFG_BASE_SIZE                                      0x00020000
#define EBI1_PHY_CFG_BASE_PHYS                                      0x00480000

/*----------------------------------------------------------------------------
 * BASE: MPM2_MPM
 *--------------------------------------------------------------------------*/

#define MPM2_MPM_BASE                                               0x004a0000
#define MPM2_MPM_BASE_SIZE                                          0x0000c000
#define MPM2_MPM_BASE_PHYS                                          0x004a0000

/*----------------------------------------------------------------------------
 * BASE: PC_NOC
 *--------------------------------------------------------------------------*/

#define PC_NOC_BASE                                                 0x00500000
#define PC_NOC_BASE_SIZE                                            0x00011000
#define PC_NOC_BASE_PHYS                                            0x00500000

/*----------------------------------------------------------------------------
 * BASE: SYSTEM_NOC
 *--------------------------------------------------------------------------*/

#define SYSTEM_NOC_BASE                                             0x00580000
#define SYSTEM_NOC_BASE_SIZE                                        0x00014000
#define SYSTEM_NOC_BASE_PHYS                                        0x00580000

/*----------------------------------------------------------------------------
 * BASE: CRYPTO0_CRYPTO_TOP
 *--------------------------------------------------------------------------*/

#define CRYPTO0_CRYPTO_TOP_BASE                                     0x00700000
#define CRYPTO0_CRYPTO_TOP_BASE_SIZE                                0x00040000
#define CRYPTO0_CRYPTO_TOP_BASE_PHYS                                0x00700000

/*----------------------------------------------------------------------------
 * BASE: QDSS_QDSS
 *--------------------------------------------------------------------------*/

#define QDSS_QDSS_BASE                                              0x00800000
#define QDSS_QDSS_BASE_SIZE                                         0x00100000
#define QDSS_QDSS_BASE_PHYS                                         0x00800000

/*----------------------------------------------------------------------------
 * BASE: QDSS_WRAPPER_TOP
 *--------------------------------------------------------------------------*/

#define QDSS_WRAPPER_TOP_BASE                                       0x00868000
#define QDSS_WRAPPER_TOP_BASE_SIZE                                  0x00008000
#define QDSS_WRAPPER_TOP_BASE_PHYS                                  0x00868000

/*----------------------------------------------------------------------------
 * BASE: TLMM
 *--------------------------------------------------------------------------*/

#define TLMM_BASE                                                   0x01000000
#define TLMM_BASE_SIZE                                              0x00400000
#define TLMM_BASE_PHYS                                              0x01000000

/*----------------------------------------------------------------------------
 * BASE: CLK_CTL
 *--------------------------------------------------------------------------*/

#define CLK_CTL_BASE                                                0x01800000
#define CLK_CTL_BASE_SIZE                                           0x00090000
#define CLK_CTL_BASE_PHYS                                           0x01800000

/*----------------------------------------------------------------------------
 * BASE: CORE_TOP_CSR
 *--------------------------------------------------------------------------*/

#define CORE_TOP_CSR_BASE                                           0x01900000
#define CORE_TOP_CSR_BASE_SIZE                                      0x00060000
#define CORE_TOP_CSR_BASE_PHYS                                      0x01900000

/*----------------------------------------------------------------------------
 * BASE: PMIC_ARB
 *--------------------------------------------------------------------------*/

#define PMIC_ARB_BASE                                               0x02000000
#define PMIC_ARB_BASE_SIZE                                          0x02000000
#define PMIC_ARB_BASE_PHYS                                          0x02000000

/*----------------------------------------------------------------------------
 * BASE: MODEM_TOP
 *--------------------------------------------------------------------------*/

#define MODEM_TOP_BASE                                              0x04000000
#define MODEM_TOP_BASE_SIZE                                         0x02000000
#define MODEM_TOP_BASE_PHYS                                         0x04000000

/*----------------------------------------------------------------------------
 * BASE: SDC1_SDCC5_TOP
 *--------------------------------------------------------------------------*/

#define SDC1_SDCC5_TOP_BASE                                         0x07800000
#define SDC1_SDCC5_TOP_BASE_SIZE                                    0x00030000
#define SDC1_SDCC5_TOP_BASE_PHYS                                    0x07800000

/*----------------------------------------------------------------------------
 * BASE: QPIC_QPIC
 *--------------------------------------------------------------------------*/

#define QPIC_QPIC_BASE                                              0x07980000
#define QPIC_QPIC_BASE_SIZE                                         0x00040000
#define QPIC_QPIC_BASE_PHYS                                         0x07980000

/*----------------------------------------------------------------------------
 * BASE: A7SS
 *--------------------------------------------------------------------------*/

#define A7SS_BASE                                                   0x0b000000
#define A7SS_BASE_SIZE                                              0x00050000
#define A7SS_BASE_PHYS                                              0x0b000000


#endif /* __MSMHWIOBASE_H__ */
