#ifndef PM_GPIO_H
#define PM_GPIO_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
*
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
*
*/

/**
 * @file pm_gpio.h  PMIC GPIO Module related declaration.
 *
 * @brief This file contains functions and variable declarations to support
 *        the PMIC GPIO module.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_gpio.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/20/13   aab     Adding IRQ related APIs
12/06/12   hw      Remove comdef.h and use com_dtypes.h
07/05/12   hs      Updated the interface.
03/01/12   hs       Updated the interface.
04/26/11   wra     Adding more GPIO enumeration needed from PM8921
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
                   Added struct pm_gpio_config_type
                   Added pm_gpio_config() and pm_gpio_set_interrupt_polarity()
08/11/10   wra     Removed doxygen quotations. They are causing parsing errors
07/26/10   wra     Changed documentation from @example to Examples so Deoxygen can parse the file
07/09/10   jtn     Added API to get GPIO status
07/02/10   wra     Changed pm_gpio_perph_index to int
06/23/10   vk      Added pm_gpio_set_mux_ctrl()
03/15/10   fpe     Removed RPC remoting because the application processor can do this directly
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
10/19/09   vk      Removed init API
08/01/09   vk      Modified gpio_digital_input prototype
05/20/09   vk      New file
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @enum pm_gpio_perph_index
 * @brief Type definition for different GPIOs.
*/
typedef enum
{
    PM_GPIO_1,
    PM_GPIO_2,
    PM_GPIO_3,
    PM_GPIO_4,
    PM_GPIO_5,
    PM_GPIO_6,
    PM_GPIO_7,
    PM_GPIO_8,
    PM_GPIO_9,
    PM_GPIO_10,
    PM_GPIO_11,
    PM_GPIO_12,
    PM_GPIO_13,
    PM_GPIO_14,
    PM_GPIO_15,
    PM_GPIO_16,
    PM_GPIO_17,
    PM_GPIO_18,
    PM_GPIO_19,
    PM_GPIO_20,
    PM_GPIO_21,
    PM_GPIO_22,
    PM_GPIO_23,
    PM_GPIO_24,
    PM_GPIO_25,
    PM_GPIO_26,
    PM_GPIO_27,
    PM_GPIO_28,
    PM_GPIO_29,
    PM_GPIO_30,
    PM_GPIO_31,
    PM_GPIO_32,
    PM_GPIO_33,
    PM_GPIO_34,
    PM_GPIO_35,
    PM_GPIO_36,
    PM_GPIO_37,
    PM_GPIO_38,
    PM_GPIO_39,
    PM_GPIO_40,
    PM_GPIO_41,
    PM_GPIO_42,
    PM_GPIO_43,
    PM_GPIO_44
}pm_gpio_perph_index;

/**
 * @enum pm_gpio_volt_src_type
 * @brief Select voltage source.
*/
typedef enum
{
    PM_GPIO_VIN0,
    PM_GPIO_VIN1,
    PM_GPIO_VIN2,
    PM_GPIO_VIN3,
    PM_GPIO_VIN4,
    PM_GPIO_VIN5,
    PM_GPIO_VIN6,
    PM_GPIO_VIN7,
    PM_GPIO_VOLTAGE_SOURCE_TYPE__INVALID
}pm_gpio_volt_src_type;


/**
 * @enum pm_gpio_invert_ext_pin_type
 * @brief Ext pin output inversion type.
 */
typedef enum
{
  PM_GPIO_INVERT_EXT_PIN_OUTPUT_DISABLE,
  PM_GPIO_INVERT_EXT_PIN_OUTPUT_ENABLE,
  PM_GPIO_INVERT_EXT_PIN_OUTPUT_INVALID
}pm_gpio_invert_ext_pin_type;

/**
 * @enum pm_gpio_out_buffer_config_type
 * @brief select output config type.
*/
typedef enum
{
    PM_GPIO_OUT_BUFFER_CONFIG_CMOS,
    PM_GPIO_OUT_BUFFER_CONFIG_OPEN_DRAIN,
    PM_GPIO_OUT_BUFFER_CONFIG_INVALID
}pm_gpio_out_buffer_config_type;

/**
 * @enum pm_gpio_out_buffer_drv_strength_type
 * @brief select output buffer strength.
*/
typedef enum
{
    PM_GPIO_OUT_BUFFER_OFF,
    PM_GPIO_OUT_BUFFER_LOW,
    PM_GPIO_OUT_BUFFER_MEDIUM,
    PM_GPIO_OUT_BUFFER_HIGH,
    PM_GPIO_OUT_BUFFER_DRIVE_STRENGTH__INVALID
}pm_gpio_out_buffer_drv_strength_type;

/**
 * @enum pm_gpio_current_src_pulls_type
 * @brief select current source pulls type.
*/
typedef enum
{
    PM_GPIO_I_SOURCE_PULL_UP_30uA,
    PM_GPIO_I_SOURCE_PULL_UP_1_5uA,
    PM_GPIO_I_SOURCE_PULL_UP_31_5uA,
    PM_GPIO_I_SOURCE_PULL_UP_1_5uA_PLUS_30uA_BOOST,
    PM_GPIO_I_SOURCE_PULL_DOWN_10uA,
    PM_GPIO_I_SOURCE_PULL_NO_PULL,
    PM_GPIO_CURRENT_SOURCE_PULLS_TYPE__INVALID
}pm_gpio_current_src_pulls_type;

/**
 * @enum pm_gpio_src_config_type
 * @brief Source select.
*/
typedef enum
{
    PM_GPIO_SOURCE_GND,
    PM_GPIO_SOURCE_PAIRED_GPIO,
    PM_GPIO_SOURCE_SPECIAL_FUNCTION1,
    PM_GPIO_SOURCE_SPECIAL_FUNCTION2,
    PM_GPIO_SOURCE_DTEST1,
    PM_GPIO_SOURCE_DTEST2,
    PM_GPIO_SOURCE_DTEST3,
    PM_GPIO_SOURCE_DTEST4,
    PM_GPIO_SOURCE_CONFIG_TYPE__INVALID
}pm_gpio_src_config_type;


/**
 * @enum pm_gpio_ext_pin_config_type
 * @brief Ext_Pin configuration.
*/
typedef enum
{
    PM_GPIO_EXT_PIN_ENABLE,    /*!< Enable EXT_PIN */
    PM_GPIO_EXT_PIN_DISABLE,   /*!< Puts EXT_PIN at high Z state & disables the block */
    PM_GPIO_EXT_PIN_CONFIG_TYPE__INVALID
}pm_gpio_ext_pin_config_type;

/**
 * @enum pm_gpio_uart_path_type
 * @brief UART MUX configuration.
*/
typedef enum
{
    PM_UART_MUX_NO,
    PM_UART_MUX_1,
    PM_UART_MUX_2,
    PM_UART_MUX_3,
    PM_UART_MUX_INVALID
}pm_gpio_uart_path_type;

/**
 * @enum pm_gpio_mode_select_type
 * @brief GPIO input/output mode config.
 */
typedef enum
{
    PM_GPIO_INPUT_ON,    /*!< GPIO is configured as input */
    PM_GPIO_INPUT_OUTPUT_ON,    /*!< GPIO is configured as input and output */
    PM_GPIO_OUTPUT_ON,    /*!< GPIO is configured as output */
    PM_GPIO_INPUT_OUTPUT_OFF    /*!< Both input and output are off */
} pm_gpio_mode_select_type;

/**
 * @struct pm_gpio_status_type
 * @brief Structure used to return GPIO status. This structure includes all of
 *        the enums that are used when configuring the GPIOs.
 */
typedef struct
{
    pm_gpio_mode_select_type  gpio_mode_select;   /*!< GPIO Mode select */
    pm_gpio_volt_src_type gpio_volt_source;    /*!< GPIO Voltage source select */
    pm_on_off_type gpio_mode_on_off;     /*!< GPIO Mode on-off - Deprecated for Badger */
    pm_gpio_out_buffer_config_type gpio_out_buffer_config;    /*!< GPIO output buffer config */
    pm_gpio_invert_ext_pin_type gpio_invert_ext_pin_config;   /*!< GPIO External pin invert config */
    pm_gpio_out_buffer_drv_strength_type gpio_out_buffer_drv_strength;    /*!< GPIO output buffer drive strength */
    pm_gpio_current_src_pulls_type gpio_current_src_pulls;    /*!< GPIO current source pull */
    pm_gpio_src_config_type gpio_src_config;    /*!< GPIO source config */
    pm_on_off_type gpio_dtest_buffer_on_off;    /*!< GPIO DTest buffer on-off */
    pm_gpio_ext_pin_config_type gpio_ext_pin_config;    /*!< GPIO External pin config */
} pm_gpio_status_type;

/**
 * @struct pm_gpio_config_type
 * @brief Structure to be used in conjunction with pm_gpio_config().
 *        This structure includes all fields that are available when configuring the GPIOs.
*/
typedef struct
{
    pm_gpio_perph_index gpio;                                          /*!< GPIO type */
    boolean config_gpio;                                               /*!< GPIO should be configured or not */
    pm_gpio_volt_src_type gpio_volt_source;                            /*!< GPIO voltage source */
    boolean  gpio_mode_on_off;                                         /*!< GPIO enables/disables mode selection - Deprecated for Badger */
    pm_gpio_mode_select_type  gpio_mode_select;                        /*!< GPIO mode select type - Deprecated for Badger */
    pm_gpio_out_buffer_config_type gpio_out_buffer_config;             /*!< GPIO output buffur config */
    boolean invert_ext_pin;                                            /*!< GPIO invert ext_pin */
    pm_gpio_current_src_pulls_type gpio_current_src_pulls;             /*!< GPIO current source pull config */
    pm_gpio_out_buffer_drv_strength_type gpio_out_buffer_drv_strength; /*!< GPIO output buffer drive strength */
    pm_on_off_type gpio_dtest_buffer_on_off;                           /*!< GPIO dtest buffer config */
    pm_gpio_ext_pin_config_type gpio_ext_pin_config;                   /*!< GPIO ext pin config */
    pm_gpio_src_config_type gpio_src_config;                           /*!< GPIO source config */
    boolean interrup_polarity;                                         /*!< GPIO interrupt polarity */
} pm_gpio_config_type;


/*===========================================================================

                    GPIO DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * @name pm_gpio_status_get
 *
 * @brief Return the status of one of the PMIC GPIOs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier.
 *
 * @param [out] gpio_status GPIO status
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_gpio_status_get(unsigned pmic_chip,
                                    pm_gpio_perph_index gpio,
                                    pm_gpio_status_type *gpio_status);

/**
 * @name pm_gpio_config_bias_voltage
 *
 * @brief Configure selected GPIO for bias voltage.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier.
 * @param [in] voltage_src GPIO Voltage source
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type pm_gpio_config_bias_voltage(unsigned pmic_chip,
                                                    pm_gpio_perph_index perph_index,
                                                    pm_gpio_volt_src_type voltage_src);

/**
 * @name pm_gpio_config_digital_input
 *
 * @brief Configure selected GPIO as a digital input.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] i_src_pulls Current source pulls
 * @param [in] voltage_src GPIO Voltage source
 * @param [in] out_buffer_strength GPIO output buffer strength
 * @param [in] source Select source
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
*/
extern pm_err_flag_type
pm_gpio_config_digital_input(unsigned pmic_chip,
                             pm_gpio_perph_index perph_index,
                             pm_gpio_current_src_pulls_type i_src_pulls,
                             pm_gpio_volt_src_type voltage_src,
                             pm_gpio_out_buffer_drv_strength_type out_buffer_strength,
                             pm_gpio_src_config_type source);

/**
 * @name pm_gpio_config_digital_output
 *
 * @brief Configure selected GPIO as a digital input.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] out_buffer_config GPIO output buffer configuration cmos/open_drain
 * @param [in] voltage_src GPIO Voltage source
 * @param [in] source Select source
 * @param [in] out_buffer_strength GPIO output buffer strength
 * @param [in] out_inversion Invert the output of Ext_Pin
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
*/
extern pm_err_flag_type
pm_gpio_config_digital_output(unsigned pmic_chip,
                              pm_gpio_perph_index perph_index,
                              pm_gpio_out_buffer_config_type out_buffer_config,
                              pm_gpio_volt_src_type voltage_src,
                              pm_gpio_src_config_type source,
                              pm_gpio_out_buffer_drv_strength_type out_buffer_strength,
                              boolean out_inversion);

/**
 * @name pm_gpio_config_digital_input_output
 *
 * @brief Configure selected GPIO as a digital input.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] source Select source
 * @param [in] i_source_pulls Current source pulls
 * @param [in] voltage_src GPIO Voltage source
 * @param [in] out_buffer_config GPIO output buffer configuration cmos/open_drain
 * @param [in] out_buffer_strength GPIO output buffer strength
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type
pm_gpio_config_digital_input_output(unsigned pmic_chip,
                                    pm_gpio_perph_index perph_index,
                                    pm_gpio_src_config_type source,
                                    pm_gpio_current_src_pulls_type i_source_pulls,
                                    pm_gpio_volt_src_type voltage_src,
                                    pm_gpio_out_buffer_config_type out_buffer_config,
                                    pm_gpio_out_buffer_drv_strength_type out_buffer_strength);

/**
 * @name pm_gpio_set_volt_source
 *
 * @brief Set voltage source for GPIO.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] voltage_src GPIO Voltage source
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type pm_gpio_set_volt_source(unsigned pmic_chip,
                                                pm_gpio_perph_index perph_index,
                                                pm_gpio_volt_src_type voltage_src);

/**
 * @name pm_gpio_config_mode_selection
 *
 * @brief Return the status of one of the PMIC GPIOs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] enable_disable True/False
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type pm_gpio_config_mode_selection(unsigned pmic_chip,
                                                      pm_gpio_perph_index perph_index,
                                                      pm_on_off_type enable_disable);

/**
 * @name pm_gpio_set_output_buffer_configuration
 *
 * @brief Set output buffer configuration of the PMIC GPIOs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] out_buffer_config GPIO output buffer configuration: 0:CMOS/1: open drain
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type
pm_gpio_set_output_buffer_configuration(unsigned pmic_chip,
                                        pm_gpio_perph_index perph_index,
                                        pm_gpio_out_buffer_config_type out_buffer_config);

/**
 * @name pm_gpio_set_inversion_configuration
 *
 * @brief Set inversion configuration of the PMIC GPIOs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] inversion True/False
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type
pm_gpio_set_inversion_configuration(unsigned pmic_chip,
                                    pm_gpio_perph_index perph_index,
                                    boolean inversion);

/**
 * @name pm_gpio_set_current_src_pulls
 *
 * @brief Set current source pulls of the PMIC GPIOs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] i_src_pulls Current source pulls
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type pm_gpio_set_current_src_pulls(unsigned pmic_chip,
                                                      pm_gpio_perph_index perph_index,
                                                      pm_gpio_current_src_pulls_type i_src_pulls);

/**
 * @name pm_gpio_set_ext_pin_config
 *
 * @brief Return the status of one of the PMIC GPIOs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] ext_pin_config ENABLE/DISABLE(Puts EXT_PIN at high Z state & disables the block)
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type pm_gpio_set_ext_pin_config(unsigned pmic_chip,
                                                    pm_gpio_perph_index perph_index,
                                                    pm_gpio_ext_pin_config_type ext_pin_config);

/**
 * @name pm_gpio_set_output_buffer_drv_strength
 *
 * @brief Return the status of one of the PMIC GPIOs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] out_buffer_strength GPIO output buffer drive strength
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type
pm_gpio_set_output_buffer_drv_strength(unsigned pmic_chip,
                                        pm_gpio_perph_index perph_index,
                                        pm_gpio_out_buffer_drv_strength_type out_buffer_strength);

/**
 * @name pm_gpio_set_src_configuration
 *
 * @brief Return the status of one of the PMIC GPIOs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] source Select Source
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type pm_gpio_set_src_configuration(unsigned pmic_chip,
                                                      pm_gpio_perph_index perph_index,
                                                      pm_gpio_src_config_type source);

/**
 * @name pm_gpio_set_mux_ctrl
 *
 * @brief Return the status of one of the PMIC GPIOs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] uart_path Select UART path.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
extern pm_err_flag_type pm_gpio_set_mux_ctrl(unsigned pmic_chip,
                                             pm_gpio_uart_path_type uart_path);

/**
 * @name pm_gpio_irq_enable
 *
 * @brief This function enables or disables GPIO IRQ.
 *
 * @param[in] pmic_chip. Primary: 0. Secondary: 1
 * @param[in] perph_index GPIO Identifier
 * @param[in] enable TRUE: Enable interrupt for the gpio
 *                     FALSE:Disable interrupt for the gpio
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_gpio_irq_enable(unsigned pmic_chip,
                                    pm_gpio_perph_index perph_index,
                                    boolean enable);



/*=========================================================================== */
/*                        pm_gpio_set_output_buffer_drive_strength            */
/*=========================================================================== */
/*! \details Set output buffer drive strength
 *   <table border="0">
 *      <tr><td><b>Input Parameter</b></td>
 *          <td><b>Type</b></td>
 *          <td><b>Description</b></td>
 *      </tr>
 *     <tr><td>pmic_chip </td>
 *          <td>pm_gpio_uart_path_type</td>
 *          <td>Each PMIC device in the systems is enumerated starting with zero.</td>
 *     <tr><td> gpio </td>
 *          <td>pm_gpio_perph_index</td>
 *          <td>GPIO to configure current source pulls</td>
 *      </tr>
 *     <tr><td> out_buffer_strength </td>
 *          <td>pm_gpio_out_buffer_drv_strength_type</td>
 *          <td>GPIO output buffer drive strength</td>
 *      </tr>
 *  </table>
 *  \return pm_err_flag_type
 *  \n\n
 *  <b>Example </b>
 *  \n "Set output buffer drive strength for GPIO5 to HIGH"
 *  \n errFlag = pm_gpio_set_output_buffer_drv_strength(PM_GPIO_5, 
 *  \n &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PM_GPIO_OUT_BUFFER_HIGH);
 * 
*/
extern pm_err_flag_type pm_gpio_set_output_buffer_drive_strength
(
 unsigned                              pmic_chip, 
 pm_gpio_perph_index                   perph_index,
 pm_gpio_out_buffer_drv_strength_type  out_buffer_strength
);


/*======================================================================= */
/*                        pm_gpio_set_src_configuration                */
/*======================================================================= */
/*! \details Set source configuration
 *   <table border="0">
 *      <tr><td><b>Input Parameter</b></td>
 *          <td><b>Type</b></td>
 *          <td><b>Description</b></td>
 *      </tr>
 *     <tr><td>pmic_chip </td>
 *          <td>pm_gpio_uart_path_type</td>
 *          <td>Each PMIC device in the systems is enumerated starting with zero.</td>
 *     <tr><td> gpio </td>
 *          <td>pm_gpio_perph_index</td>
 *          <td>GPIO to configure current source pulls</td>
 *      </tr>
 *     <tr><td> source </td>
 *          <td>pm_gpio_src_config_type</td>
 *          <td>Select Source</td>
 *      </tr>
 *  </table>
 *  \return pm_err_flag_type
 *  \n\n
 *  <b>Example </b>
 *  \n To serve GPIO5 as a output in level translator mode select 'Pair In' as Source
 *  \n errFlag = pm_gpio_set_src_configuration(PM_GPIO_5, 
 *  \n &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PM_GPIO_SOURCE_PAIRED_GPIO);
 * 
*/
extern pm_err_flag_type pm_gpio_set_source_configuration
(
 unsigned                   pmic_chip, 
 pm_gpio_perph_index        perph_index,
 pm_gpio_src_config_type    src
);

/*=========================================================================== */
/*                        pm_gpio_set_current_src_pulls                    */
/*=========================================================================== */

/*! \details Set current source pulls
 *   <table border="0">
 *      <tr><td><b>Input Parameter</b></td>
 *          <td><b>Type</b></td>
 *          <td><b>Description</b></td>
 *      </tr>
 *     <tr><td> pmic_chip </td>
 *          <td>pm_gpio_uart_path_type</td>
 *          <td>Each PMIC device in the systems is enumerated starting with zero.</td>
 *     <tr><td> gpio </td>
 *          <td>pm_gpio_perph_index</td>
 *          <td>GPIO to configure current source pulls</td>
 *      </tr>
 *     <tr><td> i_src_pulls </td>
 *          <td>pm_gpio_current_src_pulls_type</td>
 *          <td>current source pulls</td>
 *      </tr>
 *  </table>
 *  \return pm_err_flag_type
 *  \n\n
 *  <b>Example </b>
 *  \n "Set current source pulls to 1_5uA PLUS 30uA_BOOST for GPIO5"
 *  \n errFlag = pm_gpio_set_current_src_pulls(PM_GPIO_5, 
 *  \n &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; PM_GPIO_I_src_PULL_UP_1_5uA_PLUS_30uA_BOOST);
 * 
*/
extern pm_err_flag_type pm_gpio_set_current_source_pulls
(
 unsigned                          pmic_chip, 
 pm_gpio_perph_index               perph_index,
 pm_gpio_current_src_pulls_type    i_src_pulls
);


/**
 * @name pm_gpio_irq_clear
 *
 * @brief This function clears the GPIO IRQ.
 *
 * @param [in] pmic_chip. Primary: 0. Secondary: 1
 * @param [in] perph_index GPIO Identifier
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_gpio_irq_clear(unsigned pmic_chip, pm_gpio_perph_index perph_index);

/**
 * @name pm_gpio_irq_set_trigger
 *
 * @brief This function configures the GPIO for IRQ.
 *
 * @param [in] pmic_chip. Primary: 0. Secondary: 1
 * @param [in] perph_index GPIO Identifier
 * @param [in] trigger One of different irq triggers.
 *                       Refer enum pm_irq_trigger_type from pm_uefi_irq.h for more details.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_gpio_irq_set_trigger(unsigned pmic_chip,
                                         pm_gpio_perph_index perph_index,
                                         pm_irq_trigger_type trigger);

/**
 * @name pm_gpio_irq_status
 *
 * @brief This function configures the GPIO for IRQ.
 *
 * @param [in] pmic_chip. Primary: 0. Secondary: 1
 * @param [in] perph_index GPIO Identifier
 *GPIO Identifier
 * @param [out] status IRQ status.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_gpio_irq_status(unsigned pmic_chip,
                                    pm_gpio_perph_index perph_index,
                                    pm_irq_status_type type, boolean *status);

#endif    /* PM_GPIO_H */

