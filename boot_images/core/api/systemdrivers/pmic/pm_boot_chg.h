#ifndef PM_BOOT_CHG_H
#define PM_BOOT_CHG_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */
 
/**
 * @file pm_boot_chg.h PMIC BOOT-CHG related declaration.
 * 
 * @brief This header file contains functions and variable declarations
*   to support Qualcomm boot charging. 
*/

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_boot_chg.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
07/23/13   aab     Added API pm_boot_flag_sbl_chg_event_in_bms()
07/15/13   aab     Moved out some CHG APIs to SMBB driver
07/03/13   ps      Adding API to disable bat_therm pin CR#492767   
04/19/13   aab     Adding API to disable charging
01/20/13   dy      Added API to configure Rsense
01/09/13   aab     Added API to control Red LED (CR421406)
12/06/12   hw      Remove comdef.h and use com_dtypes.h
04/21/11   hs      Initial version. 
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"


/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/
/**
 * @enum pm_boot_chg_led_source_type
 * @brief The power source for anode of change indicator LED.
 */
typedef enum 
{
   PM_BOOT_CHG_LED_SRC__GROUND,    /*!< Ground       */
   PM_BOOT_CHG_LED_SRC__VPH_PWR,   /*!< VPH_PWR      */
   PM_BOOT_CHG_LED_SRC__BOOST,     /*!< Boost        */
   PM_BOOT_CHG_LED_SRC__INTERNAL   /*!< Internal source       */
}pm_boot_chg_led_source_type;

/**
 * @enum pm_boot_chg_led_dim_level_type
 * @brief LED brightness level type.
 */
typedef enum
{ 
   LOW,     
   MID,  
   HIGH    
}pm_boot_chg_led_dim_level_type;

/**
 * @enum pm_boot_chg_rsense_type
 * @brief Charger Rsense type.
 */
typedef enum
{
   PM_BOOT_CHG_RSENSE_INTERNAL,
   PM_BOOT_CHG_RSENSE_EXTERNAL
}pm_boot_chg_rsense_type;


/*===========================================================================

                BOOT CHARGE FUNCTION PROTOTYPES

===========================================================================*/
/**
 * @name pm_boot_chg_enable_led
 *
 * @brief This function sets the charge LED and the LED source if applicable.
 *
 * @details This function sets the system trickle charging current.
 *
 * @param [in] enable Used to enable/disable the charger LED.
 * @param [in] source Used to set the charger LED source (if applicable).
 * @note Some PMICs might not have the dedicated LED source for BOOT CHG; if not, this
 *       parameter will be ignored.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 * @note The default setting in PMIC HW is 50mA.
 *
 */

pm_err_flag_type pm_boot_chg_enable_led (boolean enable, pm_boot_chg_led_source_type source);

/**
 * @name pm_boot_chg_enable_led_with_dim_ctrl
 *
 * @brief This function sets the charge LED, LED dim level and LED source if applicable.
 *   
 * @param [in] enable This parameter is used to enable/disable the charger LED.
 * @param [in] source This parameter is used to set the charger LED source if applicable.
 * @param [in] dim_level This parameter is used to set the charger LED dim level 
 * @note Some PMICs might not have the dedicated LED source for BOOT CHG; if not, this
 *       parameter will be ignored.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_boot_chg_enable_led_with_dim_ctrl(boolean enable,
														pm_boot_chg_led_source_type source,
														pm_boot_chg_led_dim_level_type dim_level);

/**
 * @name pm_boot_chg_config_rsense
 *
 * @brief This function configures the Battery Sense Resistor to be Internal vs External.
 *   
 * @param [in] rsense_cfg This parameter is used to configure Internal vs External.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_boot_chg_config_rsense(pm_boot_chg_rsense_type rsense_cfg);

/**
 * @name pm_smbb_disable_bat_therm
 *
 * @brief This function disables the battery thermal pin.
 *   
 * @param [in] void
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_smbb_disable_bat_therm(void);

/**
 * @name pm_boot_flag_sbl_chg_event_in_bms
 *
 * @brief This function configures BMS reg to indicate SBL Charging event.
 * 
 * @details This function configures BMS User register to indicate SBL 
 *  	    Battery charging event which will be used by upper SW layers.
 *   
 * @param [in] set_flag Set event
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_boot_flag_sbl_chg_event_in_bms(boolean set_flag);

#endif    /* PM_BOOT_CHG_H */

