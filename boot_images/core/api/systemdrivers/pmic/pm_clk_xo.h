#ifndef PM_CLK_XO_H
#define PM_CLK_XO_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
*
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
*
*/

/**
 * @file pm_clk_xo.h  PMIC CLOCK XO Module related declaration.
 *
 * @brief This file contains functions and variable declarations to support
 *        the PMIC CLOCK XO module.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_clk_xo.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/13/13   hw      Rearchitecting clk module driver to peripheral driver
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "pm_clk.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @enum pm_clk_xo_mode_type
 * @brief PMIC Clock XO mode type.
*/
typedef enum
{
   PM_MODE_VLPM,
   PM_MODE_LPM,
   PM_MODE_NPM,
   PM_MODE_HLPM,
   PM_MODE_INVALID,
}pm_clk_xo_mode_type;

/*===========================================================================

                   CLOCK XO FUNCTION PROTOTYPES

===========================================================================*/
/**
 * @name pm_clk_xo_mode
 *
 * @brief This function sets XO mode.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_indx which LBC peripheral.
 * @param [in] mode XO mode.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_clk_xo_mode(uint8 pmic_chip, pm_clk_type perph_indx, pm_clk_xo_mode_type mode);

/**
 * @name pm_clk_xo_trim
 *
 * @brief This function configures XO trim.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_indx which LBC peripheral.
 * @param [in] trim_value Trim value.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_clk_xo_trim(uint8 pmic_chip, pm_clk_type perph_indx, uint32 trim_value);


#endif    /* PM_CLK_XO_H */

