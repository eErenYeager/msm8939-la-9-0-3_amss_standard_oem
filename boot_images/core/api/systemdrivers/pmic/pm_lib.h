#ifndef PM_LIB_H
#define PM_LIB_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_lib.h PMIC Library files inclusion.
 *
 * @brief This file contains Header file inclusion information for applications
 * 		  developed for the Qualcomm Power Manager Chip Set.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_lib.h#1 $

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
06/17/13   aab     Updated SMBB driver header to pm_smbb.h  
05/09/13   aab     Updated GPIO driver header to pm_gpio.h 
03/27/13   aab     Updated GPIO driver header to pm_gpio.h 
03/13/13   hw      Rearchitecting clk module driver to peripheral driver
03/07/13   aab     Added pm_rgb.h, pm_lpg.h and pm_vib.h
12/06/12   hw      Rearchitecturing module driver to peripheral driver
09/25/12   hw      Remove module header files that are not needed for sbl1 API support
04/16/12   hs      Removed irq files.
03/16/12   hs      Removed obsolete files.
03/14/12   hs      Replaced pm_mega_xo.h and pm_clocks.h with pm_xo_core.h
                   and pm_clk_buff.h
11/29/11   hs      Added pm_mega_xo.h
09/14/11   dy      Added pm_uicc.h
04/28/11   jtn     Added pm_smbc.h, pm_btm.h, pm_ovp.h
04/21/11   mpt     Added "pm_boot.h" and "pm_boot_chg.h"
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
11/05/10   hs      Added "pm_nfc.h" for NFC support.
07/05/10   wra     Modified Header includes to accomodate MSM8660
12/02/09   jtn     Add header file
11/19/09   jtn     Updated header files
06/30/09   jtn     Updated file documentation
05/01/09   jtn     New file
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_version.h"  /* include first for definition of pm_model_type */
#include "pm_smps.h"
#include "pm_vs.h"
#include "pm_ldo.h"
#include "pm_boot.h"
#include "pm_boot_chg.h"
#include "pm_pon.h"
#include "pm_gpio.h"
#include "pm_mpp.h"
#include "pm_clk.h"
#include "pm_clk_xo.h"
#include "pm_clk_sleep.h"
#include "pm_rgb.h"
#include "pm_lpg.h"
#include "pm_vib.h"
#include "pm_smbb.h"
#include "pm_lbc.h"
#include "pm_pbs.h"
#include "pm_resources_and_types.h"

#endif    /* PM_LIB_H */
