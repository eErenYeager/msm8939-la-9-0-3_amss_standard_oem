#ifndef PM_VS_H
#define PM_VS_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_vs.h PMIC VOLTAGE SWITCHER related declaration.
 *
 * @brief This header file contains enums and API definitions for the voltage
 *		  switcher for PMIC regulator driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_vs.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
12/06/12   hw      Rearchitecturing module driver to peripheral driver
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 *  @brief Voltage switcher peripheral index. This enum type contains all
 *         required VS regulators. 
 */
enum
{
  PM_VS_LVS_1,
  PM_VS_LVS_2,
  PM_VS_LVS_3,
  PM_VS_LVS_4,
  PM_VS_LVS_5,
  PM_VS_LVS_6,
  PM_VS_LVS_7,
  PM_VS_LVS_8,
  PM_VS_LVS_9,
  PM_VS_LVS_10,
  PM_VS_LVS_11,
  PM_VS_LVS_12,
  PM_VS_MVS_1,
  PM_VS_OTG_1,
  PM_VS_HDMI_1,
  PM_VS_INVALID
};

/*===========================================================================

                VOLTAGE SWITCHER FUNCTION PROTOTYPE

===========================================================================*/
/**
 * @name pm_vs_pull_down
 *
 * @brief Allows you to enable disable active pulldown.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] vs_peripheral_index VS peripheral index
 * @param [in] on_off Turn on and off active pulldown.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_vs_pull_down(uint8 pmic_chip, uint8 vs_peripheral_index, pm_on_off_type on_off);

/**
 * @name pm_vs_sw_mode
 *
 * @brief Switch between HPM, LPM, and other modes of a regulator.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] vs_peripheral_index VS peripheral index
 * @param [in] sw_mode Select the different mode of a regulator. Example, HPM, LPM.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_vs_sw_mode(uint8 pmic_chip, uint8 vs_peripheral_index, pm_sw_mode_type sw_mode);

/**
 * @name pm_vs_sw_mode_status
 *
 * @brief This function returns the mode status (LPM, NPM, AUTO, BYPASS)
 *        of the selected power rail. Note, the mode of a regulator
 *        changes dynamically.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] vs_peripheral_index VS peripheral index
 *
 * @param [out] sw_mode Variable to return to the caller with switcher mode status.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_vs_sw_mode_status(uint8 pmic_chip, uint8 vs_peripheral_index, pm_sw_mode_type* sw_mode);

/**
 * @name pm_vs_pin_ctrled
 *
 * @brief Select the pin ( connected to external signal ) that you would like to use
 *        to effect the state ( on/off ) and mode ( HPM, LPM etc ) of a regulator.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] vs_peripheral_index VS  peripheral index
 * @param [in] select_pin Select a pin.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_vs_pin_ctrled(uint8 pmic_chip, uint8 vs_peripheral_index, uint8 select_pin);

/**
 * @name pm_vs_sw_enable
 *
 * @brief Enable or disable a regulator or voltage switcher.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] vs_peripheral_index VS  peripheral index
 * @param [in] on_off Turn on and off the regulator.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_vs_sw_enable(uint8 pmic_chip, uint8 vs_peripheral_index, pm_on_off_type on_off);

#endif    /* PM_VS_H */
