#ifndef PM_VIB_H
#define PM_VIB_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
*
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
*
*/

/**
 * @file pm_vib.h PMIC VIBRATOR Module related declaration.
 *
 * @brief This file contains functions prototypes and variable/type/constant
 *        declarations for supporting Vibrator motor driver.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_vib.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
07/27/12   al      Renamed all Enums,added device index and resource index
12/19/11   sm      Port to UEFI
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @brief PMIC register mask type.
 */
typedef uint8  pm_register_mask_type;
/**
 * @brief PMIC register data type.
 */
typedef uint8  pm_register_data_type;

/**
 * @enum pm_vib_drv_volt_type
 * @brief PMIC Vibrator drive voltage.
 */
typedef enum
{
  PM_VIB_DRV_VOLT__1200MV = 0X0C,
  PM_VIB_DRV_VOLT__1300MV,
  PM_VIB_DRV_VOLT__1400MV,
  PM_VIB_DRV_VOLT__1500MV,
  PM_VIB_DRV_VOLT__1600MV,
  PM_VIB_DRV_VOLT__1700MV,
  PM_VIB_DRV_VOLT__1800MV,
  PM_VIB_DRV_VOLT__1900MV,
  PM_VIB_DRV_VOLT__2000MV,
  PM_VIB_DRV_VOLT__2100MV,
  PM_VIB_DRV_VOLT__2200MV,
  PM_VIB_DRV_VOLT__2300MV,
  PM_VIB_DRV_VOLT__2400MV,
  PM_VIB_DRV_VOLT__2500MV,
  PM_VIB_DRV_VOLT__2600MV,
  PM_VIB_DRV_VOLT__2700MV,
  PM_VIB_DRV_VOLT__2800MV,
  PM_VIB_DRV_VOLT__2900MV,
  PM_VIB_DRV_VOLT__3000MV,
  PM_VIB_DRV_VOLT__3100MV,
  PM_VIB_DRV_VOLT__INVALID
} pm_vib_drv_volt_type;

/**
 * @struct pm_vib_drv_mask_type
 * @brief PMIC Vibrator drive mask.
 */
typedef struct
{
  pm_register_mask_type  pm_vib_drv_r_w__mask;    /*!< Read-Write mask */
  pm_register_mask_type  pm_vib_drv_r_w__drv_vib_sel_mask;    /*!< Read-Write Vib select mask */
  pm_register_mask_type  pm_vib_drv_r_w__vib_logic_mask;    /*!< Read-Write Vib logic mask */
  pm_register_mask_type  pm_vib_drv_r_w__vib_ensel_mask;    /*!< Read-Write Vib enable select mask */
}pm_vib_drv_mask_type;

/**
 * @enum pm_vib_which_type
 * @brief PMIC Vibrator tupe.
 */
typedef enum
{
  PM_VIB__1,
  PM_VIB__INVALID
}pm_vib_which_type;

/**
 * @enum pm_vib_mode_type
 * @brief PMIC Vibrator modes.
 */
typedef enum
{
  PM_VIB_MODE__MANUAL,
  PM_VIB_MODE__DBUS1,
  PM_VIB_MODE__DBUS2,
  PM_VIB_MODE__DBUS3,
  PM_VIB_MODE__INVALID
}pm_vib_mode_type;

/**
 * @enum pm_vib_pol_type
 * @brief PMIC Vibrator polarity.
 */
typedef enum
{
  PM_VIB_POL__ACTIVE_HIGH,
  PM_VIB_POL__ACTIVE_LOW,
  PM_VIB_POL__INVALID
}pm_vib_pol_type;

/**
 * @struct pm_vib_status_type
 * @brief PMIC Vibrator status.
 */
typedef struct 
{
  boolean         enabled[PM_VIB_MODE__INVALID];    /*!< Vibrator mode enable */
  uint16          voltage;    /*!< Vibrator voltage */
  pm_vib_pol_type polarity;    /*!< Vibrator polarity */
} pm_vib_status_type;


/*===========================================================================

                 VIBRATOR DRIVER FUNCTION DEFINITIONS

===========================================================================*/
/**
 * @name pm_vib_enable
 *
 * @brief This function enables a specific mode of the vibrator.
 *          Enabling manual mode will force the vibrator on. Enabling the DBUS lines
 *          allows for an external source to enable/disable the vibrator, such as a LPG.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] vib Vibrator peripheral
 * @param [in] mode Mode to enable/disable
 * @param [in] enable Boolean to enable/disable the given mode
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__INVALID_VIBRATOR_INDEXED = The requested vibrator doesn't exist.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 */
pm_err_flag_type pm_vib_enable(unsigned pmic_device_index,
                               pm_vib_which_type vib,
                               pm_vib_mode_type  mode,
                               boolean enable);

/**
 * @name pm_vib_set_volt
 *
 * @brief This function enables a specific mode of the vibrator.
 *          Enabling manual mode will force the vibrator on. Enabling the DBUS lines
 *          allows for an external source to enable/disable the vibrator, such as a LPG.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] vib Vibrator peripheral
 * @param [in] voltage Voltage in mV to use for the vibrator.
 *                          Valid inputs: 1200 - 3100
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__INVALID_VIBRATOR_INDEXED = The requested vibrator doesn't exist.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 */
pm_err_flag_type pm_vib_set_volt(unsigned pmic_device_index,
                                 pm_vib_which_type vib,
                                 uint16 voltage);

/**
 * @name pm_vib_set_polarity
 *
 * @brief This function enables a specific mode of the vibrator.
 *          Enabling manual mode will force the vibrator on. Enabling the DBUS lines
 *          allows for an external source to enable/disable the vibrator, such as a LPG.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] vib Vibrator peripheral
 * @param [in] polarity Whether the vibrator is active high or low
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__INVALID_VIBRATOR_INDEXED = The requested vibrator doesn't exist.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 */
pm_err_flag_type pm_vib_set_polarity(unsigned pmic_device_index,
                                     pm_vib_which_type vib,
                                     pm_vib_pol_type polarity);

/**
 * @name pm_vib_get_status
 *
 * @brief This function returns the status of the vibrator.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] vib Vibrator peripheral
 *
 * @param [out] status Structure to store the vibrator status
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__INVALID_VIBRATOR_INDEXED = The requested vibrator doesn't exist.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 */
pm_err_flag_type pm_vib_get_status(unsigned pmic_device_index,
                                   pm_vib_which_type   vib,
                                   pm_vib_status_type  *status);

#endif    /* PM_VIB_H */

