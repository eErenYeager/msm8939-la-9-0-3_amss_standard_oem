#ifndef PM_SMPS_H
#define PM_SMPS_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_smps.h PMIC SMPS related declaration.
 *
 * @brief This header file contains enums and API definitions for the Switched
 *          Mode Power Supply (SMPS) PMIC voltage regulator driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_smps.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
04/05/13   aab     Added support for SMPS Multiphase configuration API
02/22/13   aab     Updated pm_smps_inductor_ilim() and pm_smps_inductor_ilim_status()
12/06/12   hw      Rearchitecturing module driver to peripheral driver
=============================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @brief SMPS peripheral index.
 */
enum
{
  PM_SMPS_1,   /*!< SMPS 1 */
  PM_SMPS_2,   /*!< SMPS 2 */
  PM_SMPS_3,   /*!< SMPS 3 */
  PM_SMPS_4,   /*!< SMPS 4 */
  PM_SMPS_5,   /*!< SMPS 5 */
  PM_SMPS_6,   /*!< SMPS 6 */
  PM_SMPS_7,   /*!< SMPS 7 */
  PM_SMPS_8,   /*!< SMPS 8.*/
  PM_SMPS_9,   /*!< SMPS 9 */
  PM_SMPS_10,  /*!< SMPS 10 */
  PM_SMPS_INVALID
};

/**
 * @enum pm_smps_ilim_mode_type
 * @brief SMPS mode type.
 */
typedef enum
{
    PM_ILIM_SMPS_PWM_MODE,  /*!< SMPS PWM mode. */
    PM_ILIM_SMPS_AUTO_MODE, /*!< SMPS auto mode. For SMPS mode
                                 transition between PFM and PWM. */
    PM_ILIM_SMPS_MODE_INVALID
}pm_smps_ilim_mode_type;

/**
 * @enum pm_clk_src_type
 * @brief Clock source type.
 */
typedef enum
{
   PM_CLK_TCXO,
   PM_CLK_RC,
   PM_CLK_SOURCE_INVALID
}pm_clk_src_type;

/**
 * @enum pm_smps_switching_freq_type
 * @brief SMPS switching frequency. This enumeration assumes an input clock
 *           frequency of 19.2 MHz and is 5 bits long.
 *          Clock frequency = (input clock freq) / ((CLK_PREDIV+1)(CLK_DIV + 1)).
 */
typedef enum
{
    PM_CLK_19p2_MHz     = 0,   /*!< Clock frequency = 19.2 MHz. */
    PM_CLK_9p6_MHz      = 1,   /*!< Clock frequency = 9.6 MHz. */
    PM_CLK_6p4_MHz      = 2,   /*!< Clock frequency = 6.2 MHz. */
    PM_CLK_4p8_MHz      = 3,   /*!< Clock frequency = 4.8 MHz. */
    PM_CLK_3p84_MHz     = 4,   /*!< Clock frequency = 3.84 MHz. */
    PM_CLK_3p2_MHz      = 5,   /*!< Clock frequency = 3.2 MHz. */
    PM_CLK_2p74_MHz     = 6,   /*!< Clock frequency = 2.74 MHz. */
    PM_CLK_2p4_MHz      = 7,   /*!< Clock frequency = 2.4 MHz. */
    PM_CLK_2p13_MHz     = 8,   /*!< Clock frequency = 2.13 MHz. */
    PM_CLK_1p92_MHz     = 9,   /*!< Clock frequency = 1.92 MHz. */
    PM_CLK_1p75_MHz     = 10,  /*!< Clock frequency = 1.75 MHz. */
    PM_CLK_1p6_MHz      = 11,  /*!< Clock frequency = 1.6 MHz. */
    PM_CLK_1p48_MHz     = 12,  /*!< Clock frequency = 1.48 MHz. */
    PM_CLK_1p37_MHz     = 13,  /*!< Clock frequency = 1.37 MHz. */
    PM_CLK_1p28_MHz     = 14,  /*!< Clock frequency = 1.28 MHz. */
    PM_CLK_1p2_MHz      = 15,  /*!< Clock frequency = 1.2 MHz. */
    PM_CLK_1p13_MHz     = 16,  /*!< Clock frequency = 1.13 MHz. */
    PM_CLK_1p07_MHz     = 17,  /*!< Clock frequency = 1.07 MHz. */
    PM_CLK_1p01_MHz     = 18,  /*!< Clock frequency = 1.01 MHz. */
    PM_CLK_960_KHz      = 19,  /*!< Clock frequency = 960 kHz. */
    PM_CLK_914_KHz      = 20,  /*!< Clock frequency = 914 kHz. */
    PM_CLK_873_KHz      = 21,  /*!< Clock frequency = 873 kHz. */
    PM_CLK_835_KHz      = 22,  /*!< Clock frequency = 835 kHz. */
    PM_CLK_800_KHz      = 23,  /*!< Clock frequency = 800 kHz. */
    PM_CLK_768_KHz      = 24,  /*!< Clock frequency = 768 kHz. */
    PM_CLK_738_KHz      = 25,  /*!< Clock frequency = 738 kHz. */
    PM_CLK_711_KHz      = 26,  /*!< Clock frequency = 711 kHz. */
    PM_CLK_686_KHz      = 27,  /*!< Clock frequency = 686 kHz. */
    PM_CLK_662_KHz      = 28,  /*!< Clock frequency = 662 kHz. */
    PM_CLK_640_KHz      = 29,  /*!< Clock frequency = 640 kHz. */
    PM_CLK_619_KHz      = 30,  /*!< Clock frequency = 619 kHz. */
    PM_CLK_600_KHz      = 31,  /*!< Clock frequency = 600 kHz. */
    PM_SWITCHING_FREQ_INVALID,
    PM_SWITCHING_FREQ_FREQ_NONE
}pm_smps_switching_freq_type;

/**
 * @enum pm_quiet_mode_type
 * @brief Quiet mode.
 */
typedef enum
{
    PM_QUIET_MODE__DISABLE,      /*!< Quiet mode disabled (default). */
    PM_QUIET_MODE__QUIET,        /*!< Quiet mode enabled. */
    PM_QUIET_MODE__SUPER_QUIET,  /*!< Super quiet mode enabled. */
    PM_QUIET_MODE__INVALID
}pm_quiet_mode_type;


/*===========================================================================

                    SMPS DRIVER API PROTOTYPE

===========================================================================*/
/**
 * @name pm_smps_pull_down
 *
 * @brief Allows you to enable disable active pulldown.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] on_off Turn on and off active pulldown.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_pull_down(uint8 pmic_chip, uint8 smps_peripheral_index, pm_on_off_type on_off);

/**
 * @name pm_smps_sw_mode
 *
 * @brief Switch between HPM, LPM, and other modes of a regulator.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] sw_mode Select the different mode of a regulator. Example, HPM, LPM.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_sw_mode(uint8 pmic_chip,
                                 uint8 smps_peripheral_index,
                                 pm_sw_mode_type sw_mode);

/**
 * @name pm_smps_sw_mode_status
 *
 * @brief This function returns the mode status (LPM, NPM, AUTO, BYPASS)
 *        of the selected power rail. Note, the mode of a regulator
 *        changes dynamically.
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1
 * @param [in] smps_peripheral_index Starts from 0 (for first SMPS peripheral)
 *
 * @param [out] sw_mode Variable to return to the caller with mode status.
 *                      Refer to pm_resources_and_types.h for the enum info.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_smps_sw_mode_status(uint8 pmic_chip,
                                        uint8 smps_peripheral_index,
                                        pm_sw_mode_type* sw_mode);

/**
 * @name pm_smps_pin_ctrled
 *
 * @brief Select the pin ( connected to external signal ) that you would like to use
 *        to effect the state ( on/off ) and mode ( HPM, LPM etc ) of a regulator.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] select_pin Select a pin
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_pin_ctrled(uint8 pmic_chip,
                                    uint8 smps_peripheral_index,
                                    uint8 select_pin);

/**
 * @name pm_smps_sw_enable
 *
 * @brief Enable or Disable a regulator or voltage switcher.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] on_off Turn on and off the regulator.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_sw_enable(uint8 pmic_chip,
                                   uint8 smps_peripheral_index,
                                   pm_on_off_type on_off);

/**
 * @name pm_smps_volt_level
 *
 * @brief Set the voltage of a regulator.
 *        Note, differnt type ( LDO, HFS etc ) may have different programmable voltage steps.
 *        Only support the correct programmable steps. Not rounding voltages if the voltage selected
 *        is not part of the programmable steps.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] volt_level Select the voltage in micro volts
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_volt_level(uint8 pmic_chip,
                                    uint8 smps_peripheral_index,
                                    pm_volt_level_type volt_level);

/**
 * @name pm_smps_volt_level_status
 *
 * @brief This function returns the voltage level (in micro
 *        volts) of the selected power rail.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 *
 * @param [out] volt_level Variable to return to the caller with volt
 *                           level status in micro volts (uint32).
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_smps_volt_level_status(uint8 pmic_chip,
                                           uint8 smps_peripheral_index,
                                           pm_volt_level_type* volt_level);

/**
 * @name pm_smps_clk_source
 *
 * @brief Select a desire clock source for SMPS.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] clk_source SMPS clock source, TCXO, RC etc.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_clk_source(uint8 pmic_chip, pm_clk_src_type clk_source);

/**
 * @name pm_smps_switching_freq
 *
 * @brief Select a desire frequency for the SMPS. Each SMPS can operate under different frequency.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] switching_freq Select frequency such as 19.2MHz, 3.2MHz etc.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_switching_freq(uint8 pmic_chip,
                                        uint8 smps_peripheral_index,
                                        pm_smps_switching_freq_type switching_freq);

/**
 * @name pm_smps_inductor_ilim
 *
 * @brief select the current limit for the inductor of a selected SMPS.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] ilim_level Range of ilim_level
 *                           - 3500 to 600
 *                           - 2x SMPS I limit range: 600 to 2700
 *                         - 3x SMPS I limit range: 700 to 3500
 * @param [in] smps_mode PM_ILIM_SMPS_PWM_MODE :  When Operating in PWM mode
 *                       PM_ILIM_SMPS_AUTO_MODE:  whenever there is a mode transition b/n
 *                         PFM and PWM mode under Auto-mode operation.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_inductor_ilim(uint8 pmic_chip,
                                       uint8 smps_peripheral_index,
                                       uint16 ilim_level,
                                       pm_smps_ilim_mode_type smps_mode);

/**
 * @name pm_smps_inductor_ilim_status
 *
 * @brief Obtain the current limit for the inductor of a selected SMPS by reading the SPMI register.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] smps_mode PM_ILIM_SMPS_PWM_MODE :  When Operating in PWM mode
 *                       PM_ILIM_SMPS_AUTO_MODE:  whenever there is a mode transition b/n
 *                         PFM and PWM mode under Auto-mode operation.
 *
 * @param [out] ilim_level Range of ilim_level
 *                           - 3500 to 600
 *                           - 2x SMPS I limit range: 600 to 2700
 *                         - 3x SMPS I limit range: 700 to 3500
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_inductor_ilim_status(uint8 pmic_chip,
                                              uint8 smps_peripheral_index,
                                              uint16* ilim_level,
                                              pm_smps_ilim_mode_type smps_mode );


/**
 * @name pm_smps_quiet_mode
 *
 * @brief Set quiet mode for SMPS.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] quiet_mode Enable/Disable quiet mode as well as selecting different
 *               types of quiet mode.
 * @param [in] voltage_mv Voltage to apply in millivolts.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_quiet_mode(uint8 pmic_chip,
                                    uint8 smps_peripheral_index,
                                    pm_quiet_mode_type quiet_mode,
                                    uint16 voltage_mv);


/**
 * @name pm_smps_multiphase_ctrl
 *
 * @brief Sets the phase of multiphase system.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 * @param [in] number_of_phase number of phase enabled
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_multiphase_ctrl(uint8 pmic_chip,
                                         uint8 smps_peripheral_index,
                                         uint32 number_of_phase);


/**
 * @name pm_smps_multiphase_ctrl_status
 *
 * @brief Sets the phase of multiphase system.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] smps_peripheral_index SMPS Identifier
 *
 * @param [out] number_of_phase Pointer to the variable to get number of phase enabled.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_smps_multiphase_ctrl_status(uint8 pmic_chip,
                                                uint8 smps_peripheral_index,
                                                uint32* number_of_phase);

#endif    /* PM_SMPS_H */
