#ifndef PM_PON_H
#define PM_PON_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_pon.h  PMIC PON Module related declaration.
 *
 * @brief This file contains functions and variable declarations to support
 *        the PMIC PON Driver module.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_pon.h#1 $
$DateTime: 2015/03/19 01:58:37 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/17/14   mr      Added support for Warm_Reset status check APIs. (CR-555269)
05/12/14   pxm     Add uvlo API required by customer. SR01534529
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
06/24/13   ps      Added PON Stage3 reset config API CR#495834
04/10/13   kt      Added PMIC Watchdog config support
02/27/13   kt      Added PON IRQ related APIs
12/06/12   hw      Remove comdef.h and use com_dtypes.h
22/11/12   umr     Created PON API.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "com_dtypes.h"
#include "pm_resources_and_types.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @enum pm_pon_reset_cfg_type
 * @brief Reset configure type.
 */
typedef enum
{
  PM_PON_RESET_CFG_WARM_RESET,
  PM_PON_RESET_CFG_NORMAL_SHUTDOWN,
  /* Shutdown to a state of main battery removal */
  PM_PON_RESET_CFG_D_VDD_BATT_REMOVE_SHUTDOWN,
   /* Shutdown to a state of main battery and coin cell removal*/
  PM_PON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_SHUTDOWN,
   /* Shutdown + Auto pwr up */
  PM_PON_RESET_CFG_HARD_RESET,
  /* Main Batt and coin cell remove Shutdown + Auto pwr up 8: DVDD_HARD_RESET */
  PM_PON_RESET_CFG_D_VDD_BATT_REMOVE_HARD_RESET,
  PM_PON_RESET_CFG_D_VDD_COIN_CELL_REMOVE_HARD_RESET,
  PM_PON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_HARD_RESET,
  PM_PON_RESET_CFG_IMMEDIATE_X_VDD_COIN_CELL_REMOVE_SHUTDOWN,
  /* Warm Reset and Main Batt/coin cell remove Shutdown */
  PM_PON_RESET_CFG_WARM_RESET_AND_D_VDD_BATT_REMOVE_SHUTDOWN,
  PM_PON_RESET_CFG_WARM_RESET_AND_X_VDD_COIN_CELL_REMOVE_SHUTDOWN,
  PM_PON_RESET_CFG_WARM_RESET_AND_SHUTDOWN,
  /* Warm Reset then Main Batt/coin cell remove Shutdown + Auto pwr up */
  PM_PON_RESET_CFG_WARM_RESET_THEN_HARD_RESET,
  PM_PON_RESET_CFG_WARM_RESET_THEN_D_VDD_BATT_REMOVE_HARD_RESET,
  PM_PON_RESET_CFG_WARM_RESET_THEN_X_VDD_COIN_CELL_REMOVE_HARD_RESET,
  PM_PON_RESET_CFG_INVALID
}pm_pon_reset_cfg_type;

/**
 * @enum pm_pon_reset_source_type
 * @brief Reset source type.
 *
 * @warning DO NOT CHANGE THE SEQUENCE OF TOP 4 Enums.
 */
typedef enum
{
  PM_PON_RESET_SOURCE_KPDPWR,           /*!< Power key */
  PM_PON_RESET_SOURCE_RESIN,            /*!< Resin in form MSM */
  PM_PON_RESET_SOURCE_RESIN_AND_KPDPWR, /*!< Pwr Key + Resin */
  PM_PON_RESET_SOURCE_RESIN_OR_KPDPWR,  /*!< Pwr Key or Resin */
  PM_PON_RESET_SOURCE_GP1,              /*!< General purpose-1 */
  PM_PON_RESET_SOURCE_GP2,              /*!< General purpose-1 */
  PM_PON_RESET_SOURCE_INVALID
}pm_pon_reset_source_type;

/**
 * @struct pm_pon_pon_reason_type
 * @brief Power On reason type.
 */
typedef struct
{
  unsigned hard_reset : 1;     /*!< Hard reset event trigger */
  unsigned smpl : 1;           /*!< SMPL trigger */
  unsigned rtc : 1;            /*!< RTC trigger */
  unsigned dc_chg : 1;         /*!< DC Charger trigger */
  unsigned usb_chg : 1;        /*!< USB Charger trigger */
  unsigned pon1 : 1;           /*!< PON1 trigger */
  unsigned cblpwr : 1;         /*!< CBL_PWR1_N trigger */
  unsigned kpdpwr : 1;         /*!< KPDPWR_N trigger */
}pm_pon_pon_reason_type;

/**
 * @struct pm_pon_poff_reason_type
 * @brief Power Off reason type.
 */
typedef struct
{
  unsigned soft : 1;               /*!< Software trigger */
  unsigned ps_hold : 1;            /*!< PS_HOLD trigger */
  unsigned pmic_wd : 1;            /*!< PMIC Watchdog trigger */
  unsigned gp1 : 1;                /*!< Keypad_Reset1 trigger */
  unsigned gp2 : 1;                /*!< Keypad_Reset2 trigger */
  unsigned kpdpwr_and_resin : 1;   /*!< KPDPWR_N and RESIN_N trigger */
  unsigned resin : 1;              /*!< RESIN_N trigger */
  unsigned kpdpwr : 1;             /*!< KPDPWR_N trigger */
  unsigned reserved : 3;           /*!< Reserved bits */
  unsigned charger : 1;            /*!< Charger (BOOT_DONE) trigger */
  unsigned tft : 1;                /*!< TFT trigger */
  unsigned uvlo : 1;               /*!< UVLO trigger */
  unsigned otst3 : 1;              /*!< Overtemp trigger */
  unsigned stage3 : 1;             /*!< Stage3 reset trigger */
}pm_pon_poff_reason_type;

/**
 * @struct pm_pon_warm_reset_reason_type
 * @brief Warm Reset reason type.
 */
typedef struct
{
  unsigned soft : 1;               /*!< Software trigger */
  unsigned ps_hold : 1;            /*!< PS_HOLD trigger */
  unsigned pmic_wd : 1;            /*!< PMIC Watchdog trigger */
  unsigned gp1 : 1;                /*!< Keypad_Reset1 trigger */
  unsigned gp2 : 1;                /*!< Keypad_Reset2 trigger */
  unsigned kpdpwr_and_resin : 1;   /*!< KPDPWR_N and RESIN_N trigger */
  unsigned resin : 1;              /*!< RESIN_N trigger */
  unsigned kpdpwr : 1;             /*!< KPDPWR_N trigger */
  unsigned reserved : 4;           /*!< Reserved bits */
  unsigned tft : 1;                /*!< TFT trigger */
  unsigned reserved1 : 3;          /*!< Reserved bits */
}pm_pon_warm_reset_reason_type;

/**
 * @struct pm_pon_soft_reset_reason_type
 * @brief Soft Reset reason type.
 */
typedef struct
{
  unsigned soft : 1;               /*!< Software trigger */
  unsigned ps_hold : 1;            /*!< PS_HOLD trigger */
  unsigned pmic_wd : 1;            /*!< PMIC Watchdog trigger */
  unsigned gp1 : 1;                /*!< Keypad_Reset1 trigger */
  unsigned gp2 : 1;                /*!< Keypad_Reset2 trigger */
  unsigned kpdpwr_and_resin : 1;   /*!< KPDPWR_N and RESIN_N trigger */
  unsigned resin : 1;              /*!< RESIN_N trigger */
  unsigned kpdpwr : 1;             /*!< KPDPWR_N trigger */
  unsigned reserved : 4;           /*!< Reserved bits */
  unsigned tft : 1;                /*!< TFT trigger */
  unsigned reserved1 : 3;          /*!< Reserved bits */
}pm_pon_soft_reset_reason_type;


/**
 * @enum pm_pon_irq_type
 * @brief different types of irq bit fields of pon irq module
 */
typedef enum {
  PM_PON_IRQ_KPDPWR_ON          = 0,
  PM_PON_IRQ_RESIN_ON           = 1,
  PM_PON_IRQ_CBLPWR_ON          = 2,
  PM_PON_IRQ_KPDPWR_BARK        = 3,
  PM_PON_IRQ_RESIN_BARK         = 4,
  PM_PON_IRQ_KPDPWR_RESIN_BARK  = 5,
  PM_PON_IRQ_PMIC_WD_BARK       = 6,
  PM_PON_IRQ_SOFT_RESET         = 7,
  PM_PON_IRQ_INVALID    /*INVALID*/
}pm_pon_irq_type;

/*===========================================================================

                        HEADER FILES

===========================================================================*/
/**
 * @name pm_pon_ps_hold_cfg
 *
 * @brief Configure MSM PS_HOLD behavior.
 *
 * @details Configure PMIC to act on MSM PS_HOLD state.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] ps_hold_cfg PS_HOLD config for Reset.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_ps_hold_cfg(unsigned pmic_device_index, pm_pon_reset_cfg_type ps_hold_cfg);

/**
 * @name pm_pon_reset_source_cfg
 *
 * @brief Configure PON reset sources.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] reset_source Select source for reset.
 * @param [in] s1_timer Select S1 timer in ms for bark.
 *                      Supported time in ms for all the reset source
 *                      types, Rounds UP to next largest setting if
 *                      value passed is in between the permitted values:
 *                      0, 32, 56, 80, 128, 184, 272, 408, 608, 904, 1352,
 *                      2048, 3072, 4480, 6720, 10256
 * @param [in] s2_timer Select S2 timer in ms for bite. This is the time after bark S1.
 *                      Supported time in ms for all the reset source
 *                      types, rounds up to next largest number in ms if
 *                      value passed is in between the permitted values:0,
 *                      10, 50, 100, 250, 500, 1000, 2000
 * @param [in] reset_cfg_type Configure the type of reset to be performed on the event.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_reset_source_cfg(unsigned pmic_device_index,
                                        pm_pon_reset_source_type reset_source,
                                        uint32 s1_timer,
                                        uint32 s2_timer,
                                        pm_pon_reset_cfg_type reset_cfg_type);

/**
 * @name pm_pon_reset_source_ctl
 *
 * @brief Configure PON reset Control.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] reset_source Source of the reset to be enabled / disabled.
 * @param [in] on_off Select enable / disable
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_reset_source_ctl(unsigned pmic_device_index,
                                         pm_pon_reset_source_type reset_source,
                                         pm_on_off_type on_off);

/**
 * @name pm_pon_stage3_reset_source_cfg
 *
 * @brief Configure PON stage3 reset source and timer.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] reset_source Select source for reset.
 * @param [in] s3_timer Select S3 timer in sec.
 *                      Supported time in sec for all the reset source
 *                      types, rounds up to next largest number in sec if
 *                      value passed is in between the permitted values:
 *                      0, 2, 4, 8, 16, 32, 64, 128
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_stage3_reset_source_cfg(unsigned pmic_device_index,
                                               pm_pon_reset_source_type reset_source,
                                               uint32 s3_timer);

/**
 * @name pm_pon_wdog_cfg
 *
 * @brief Configure PON PMIC Watchdog reset source
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] s1_timer Select S1 timer in seconds for bark.
 *                      Supported time in seconds: 0 to 127
 * @param [in] s2_timer Select S2 timer in seconds for bite. This is
 *                      the time after bark S1.
 *                      Supported time in seconds: 0 to 127
 * @param [in] reset_cfg_type Configure the type of reset to be performed on the event.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_wdog_cfg(unsigned pmic_device_index,
                                 uint32 s1_timer,
                                 uint32 s2_timer,
                                 pm_pon_reset_cfg_type reset_cfg_type);

/**
 * @name pm_pon_wdog_enable
 *
 * @brief PON PMIC Watchdog reset Control (enable/disable).
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] enable Select enable/disable
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_wdog_enable(unsigned pmic_device_index, pm_on_off_type enable);

/**
 * @name pm_pon_wdog_pet
 *
 * @brief Clears the PMIC Watchdog timer (Pets the PMIC Watchdog).
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_wdog_pet(unsigned pmic_device_index);

/**
 * @name pm_pon_get_pon_reason
 *
 * @brief Returns reason for Power On.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1

 * @param [out] reason PON reason type.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_get_pon_reason(unsigned pmic_device_index, pm_pon_pon_reason_type* reason);

/**
 * @name pm_pon_get_poff_reason
 *
 * @brief Returns reason for Power Off.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 *
 * @param [out] reason POFF reason type.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_get_poff_reason(unsigned pmic_device_index,
                                        pm_pon_poff_reason_type* reason);

/**
 * @name pm_pon_get_warm_reset_reason
 *
 * @brief Returns reason for Warm Reset.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 *
 * @param [out] reason Warm reset reason type.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_get_warm_reset_reason(unsigned pmic_device_index,
                                              pm_pon_warm_reset_reason_type* reason);

/**
 * @name pm_pon_get_soft_reset_reason
 *
 * @brief Returns reason for Soft Reset.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 *
 * @param [out] reason Soft reset reason type.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_get_soft_reset_reason(unsigned pmic_device_index,
                                                pm_pon_soft_reset_reason_type* reason);

/**
 * @name pm_pon_get_all_pon_reasons
 *
 * @brief Returns reasons for Power On, Power Off, Warm Reset and Soft Reset.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 *
 * @param [out] pon_reasons Returns a uint64 value with all the PON
 *                         reasons (includes PON, POFF, WARM RESET and
 *                         SOFT RESET reasons).
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_get_all_pon_reasons(unsigned pmic_device_index, uint64* pon_reasons);

/**
 * @name pm_pon_debounce_ctl
 *
 * @brief Configure PON Debounce time delay for KPD, CBL,
 *        General Purpose PON, RESIN, RESIN AND KPD, GP1 and GP2
 *        state change and interrupt triggering.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] time_delay Select time delay in milli seconds. Supported
 *                        time in ms, rounds up to the next largest
 *                        number in ms if value passed is in between the
 *                        permitted values: 15, 31, 62, 125, 250, 500, 1000, 2000
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 */
pm_err_flag_type pm_pon_debounce_ctl(unsigned pmic_device_index, uint32 time_delay);

/**
 * @name pm_pon_irq_enable
 *
 * @brief This function enables or disables pon irq
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] irq PON IRQ type
 * @param [in] enable TRUE: Enable corresponding PON interrupt
 *                    FALSE:Disable corresponding PON interrupt
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 *
 */
pm_err_flag_type pm_pon_irq_enable(unsigned pmic_device_index, pm_pon_irq_type irq, boolean enable);

/**
 * @name pm_pon_irq_clear
 *
 * @brief This function clears the PON irq
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] irq PON IRQ type
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 *
 */
pm_err_flag_type pm_pon_irq_clear(unsigned  pmic_device_index, pm_pon_irq_type irq);

/**
 * @name pm_pon_irq_set_trigger
 *
 * @brief This function configures the PON irq trigger
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] irq PON irq type. Refer enum pm_pon_irq_type.
 * @param [in] trigger Different IRQ triggers type.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 *
 */
pm_err_flag_type pm_pon_irq_set_trigger(unsigned pmic_device_index,
                                        pm_pon_irq_type irq,
                                        pm_irq_trigger_type trigger);

/**
 * @name pm_pon_irq_status
 *
 * @brief This function configures the PON for irq
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] irq PON irq type. Refer enum pm_pon_irq_type.
 * @param [in] type Type of IRQ status to read.
 *
 * @param [out] status IRQ status.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 *
 */
pm_err_flag_type pm_pon_irq_status(unsigned pmic_device_index,
                                    pm_pon_irq_type irq,
                                    pm_irq_status_type type,
                                    boolean *status);

/**
 * @brief This function configures UVLO Rising Voltage threshold and HYST Voltage threshold
 * 
 * @param[in] pmic_device_index. Primary PMIC: 0
 * @param[in] :  
 *          uvlo_rising_thresh_voltage_in_mv (Range between 2875 and 3225 )
 *          hyst_voltage_in_mv (Range between 175 and 425 )
 *            
 * @return  pm_err_flag_type 
 *         PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of
 *          range.
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 */
pm_err_flag_type 
pm_pon_config_uvlo(unsigned pmic_device_index,
                        uint32 uvlo_rising_thresh_voltage_in_mv,
                        uint32 hyst_voltage_in_mv);

/**
 * @brief This function returns WARM RESET status whether the 
 *        reset occured or not and this status needs to be
 *        explicitly cleared using the
 *        pm_pon_warm_reset_status_clear API.
 * 
 * @param[in] pmic_device_index. Primary PMIC: 0 Secondary PMIC: 1
 * @param[out] status:  
 *                WARM RESET status (returns TRUE if warm reset
 *                occured, FALSE otherwise).
 *
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__INVALID_POINTER = Null pointer passed
 *          in as an argument.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not
 *          available on this version of the PMIC.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of
 *          range.
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 */
pm_err_flag_type 
pm_pon_warm_reset_status(unsigned pmic_device_index, boolean *status);


/**
 * @brief This function clears the WARM RESET status.
 * 
 * @param[in] pmic_device_index. Primary PMIC: 0 Secondary PMIC: 1 
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not
 *          available on this version of the PMIC.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of
 *          range.
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *
 */
pm_err_flag_type 
pm_pon_warm_reset_status_clear(unsigned pmic_device_index);

#endif    /* PM_PON_H */
