#ifndef PM_PBS_H
#define PM_PBS_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_pbs.h PMIC PBS driver.
 *
 * @brief This header file contains enums, macros and public API definitions for PMIC PBS driver.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_pbs.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/11/13   kt      Created.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"


/*===========================================================================

                    OP-CODE MACROS

===========================================================================*/
/**
 * Op-code macros are the instruction command op-code values and are used in
 * constructing sequence code macros below. User need not use the op-code
 * macros and can call the below sequence code macros directly for writing
 * PBS sequences.
 */
/**
 * @brief Write register Op-code.
 */
#define PM_PBS_OPCODE_WRITE_REG           0x00
/**
 * @brief Write sub-register Op-code.
 */
#define PM_PBS_OPCODE_WRITE_REG_SUB       0x04
/**
 * @brief Write register buffer Op-code.
 */
#define PM_PBS_OPCODE_WRITE_REG_BUF       0x08
/**
 * @brief Write sub-register buffer Op-code.
 */
#define PM_PBS_OPCODE_WRITE_REG_SUB_BUF   0x0C
/**
 * @brief Read register Op-code.
 */
#define PM_PBS_OPCODE_READ_REG            0x40
/**
 * @brief Read sub-register Op-code.
 */
#define PM_PBS_OPCODE_READ_REG_SUB        0x44
/**
 * @brief GoTo Op-code.
 */
#define PM_PBS_OPCODE_GOTO                0x80
/**
 * @brief GoTo-Relative Op-code.
 */
#define PM_PBS_OPCODE_GOTO_RELATIVE       0x84
/**
 * @brief GoTo-sub Op-code.
 */
#define PM_PBS_OPCODE_GOTO_SUB            0x88
/**
 * @brief Skip-If Op-code.
 */
#define PM_PBS_OPCODE_SKIP_IF             0x90
/**
 * @brief Wait Op-code.
 */
#define PM_PBS_OPCODE_WAIT                0xC0
/**
 * @brief Info Op-code.
 */
#define PM_PBS_OPCODE_INFO                0xF8
	

/*=========================================================================

                    SEQUENCE CODE MACROS

==========================================================================*/
/**
 * @brief This macro is used to write to a single register in given
 *        slave id (sid), peripheral id (pid) and register offset
 *        (offset).
 *
 *        PMIC address is broken down into Slave ID(sid), Peripheral
 *        ID (pid) and Register offset (offset).
 *        For example, address 0x12345 is broken into following.
 *        Slave ID (sid): 0x1
 *        Peripheral ID (pid): 0x23
 *        Register offset (offset): 0x45
 *
 *        For example, to write data 0xC0 to 0x88D register, the
 *        sequence macro would be
 *        PM_PBS_WRITE_REG(0x0,0x08,0x8D,0xC0).
 */
#define PM_PBS_WRITE_REG(sid, pid, offset, data)   (((((uint32)PM_PBS_OPCODE_WRITE_REG) | ((sid)&0xFF))<<24) | (((pid)&0xFF)<<16) | (((offset)&0xFF)<<8) | ((data)&0xFF))

/**
 * @brief This macro is used to write to a single register in given
 *        slave id (sid), peripheral id (pid) and register offset
 *        (offset) using data from the local buffer R0.
 *
 *        PMIC address is broken down into Slave ID(sid), Peripheral
 *        ID (pid) and Register offset (offset).
 *        For example, address 0x12345 is broken into following.
 *        Slave ID (sid): 0x1
 *        Peripheral ID (pid): 0x23
 *        Register offset (offset): 0x45
 *
 *        For example, to write data in buffer R0 to 0x88D register,
 *        the sequence macro would be
 *        PM_PBS_WRITE_REG_BUF(0x0,0x08,0x8D).
 */
#define PM_PBS_WRITE_REG_BUF(sid, pid, offset)   (((((uint32)PM_PBS_OPCODE_WRITE_REG_BUF) | ((sid)&0xFF))<<24) | (((pid)&0xFF)<<16) | (((offset)&0xFF)<<8))

/**
 * @brief This macro is used to read a register in given slave id
 *        (sid), peripheral id (pid) and register offset (offset)
 *        and store the result in local buffer R0.
 *
 *        PMIC address is broken down into Slave ID(sid), Peripheral
 *        ID (pid) and Register offset (offset).
 *        For example, address 0x12345 is broken into following.
 *        Slave ID (sid): 0x1
 *        Peripheral ID (pid): 0x23
 *        Register offset (offset): 0x45
 *
 *        For example, to read data from 0x14B08 register and store it
 *        in R0 buffer, the sequence macro would be
 *        PM_PBS_READ_REG(0x1,0x4B,0x08).
 */
#define PM_PBS_READ_REG(sid, pid, offset)   (((((uint32)PM_PBS_OPCODE_READ_REG) | ((sid)&0xFF))<<24) | (((pid)&0xFF)<<16) | (((offset)&0xFF)<<8))

/**
 * @brief This macro is used to compare the masked contents of local
 *        data buffer R0 and the provided data using the chosen
 *        operator. if true skip over the next sequence (increment PC
 *        by 8), if false execute the next sequence (increment PC by
 *        4).
 *        Operators:
 *        0x00 = Not Equal
 *        0x01 = Equal
 *        0x02 = Less than or equal
 *        0x03 = Less than
 *        0x04 = Greater than or equal
 *        0x05 = Greater than
 *
 *        For example, to skip over the next sequence if bit 5 of the
 *        value stored in R0 buffer (mask: 0x20) is equal(Oper: 0x01)
 *        to 0x1 (data: 0x20), the sequence macro would be
 *        PM_PBS_SKIP_IF(0x20,0x01,0x20). Please notice that the data
 *        in this case is not 0x1 and would be 0x20 instead since if
 *        the 5th bit is 1 the data value would be 0x20.
 */
#define PM_PBS_SKIP_IF(mask, oper, data)   ((((uint32)PM_PBS_OPCODE_SKIP_IF)<<24) | (((mask)&0xFF)<<16) | (((oper)&0x07)<<8) | ((data)&0xFF))

/**
 * @brief This macro is used to wait for given time. Unit indicates the
 *           time increment for the counter.
 *        Clock Sources:
 *        0x00 = 19.2 MHz clock cycles
 *        0x01 = 32 KHz clock cycles
 *
 *        For example, to wait for 12 32KHz clock cycles, the sequence
 *        macro would be PM_PBS_WAIT(1,12) or PM_PBS_WAIT(0x01,0x0C).
 */
#define PM_PBS_WAIT(clk_src, time)   ((((uint32)PM_PBS_OPCODE_WAIT)<<24) | (((clk_src)&0x01)<<16) | ((time)&0xFFFF))

/**
 * @brief This macro is used to go to a new absolute address location
 *        in the PBS logical address map. (2 lsbs ignored since all
 *        instructions are 4 bytes long).
 *
 *        PBS logical address map contains 512 words(starting from 0 to
 *        511) where first 256 words are ROM and last 256 words are RAM.
 *
 *        To calculate an absolute PBS address (pbs_abs_addr) for a
 *        specific word, we need to multiply the word number of the
 *        address (in scale of 0 - 511) by 4.
 *
 *        For example, to go to word#500 in PBS address map, the
 *        sequence macro would be PM_PBS_GOTO(2000). (multiply 500 by 4)
 *
 */
#define PM_PBS_GOTO(pbs_abs_addr)   ((((uint32)PM_PBS_OPCODE_GOTO)<<24) | ((pbs_abs_addr)&0xFFFF))

/**
 * @brief This macro is used to go to a new relative location in the
 *        PBS logical address map. (2's compliment math used).
 *
 *        PBS logical address map contains 512 words(starting from 0 to
 *        511) where first 256 words are ROM and last 256 words are
 *        RAM.
 *
 *        To calculate an absolute PBS address (pbs_abs_addr) for a
 *        specific word, we need to multiply the word number of the
 *        address (in scale of 0 - 511) by 4.
 *
 *        To calculate relative PBS address (pbs_rel_addr) for a
 *        specific word, we need to calculate the difference of word
 *        numbers between the new and current PBS address locations and
 *        multiply the difference by 4.
 *
 *        For example, if you are exexuting GOTO_RELATIVE sequence at
 *        word#440 to go to word#500 in PBS address map, the sequence
 *        macro would be PM_PBS_GOTO_RELATIVE(240). (multiply
 *        difference 60 by 4)
 */
#define PM_PBS_GOTO_RELATIVE(pbs_rel_addr)   ((((uint32)PM_PBS_OPCODE_GOTO_RELATIVE)<<24) | ((pbs_rel_addr)&0xFFFF))

/**
 * @brief This macro is used as End of Sequence.
 */
#define PM_PBS_EOS   0xFCFFFFFF


/*=========================================================================

                    SUB-ROUTINE SEQUENCE CODE MACROS

==========================================================================*/
/**
 * @brief Sub-routines are entered when the PM_PBS_GOTO_SUB sequence is executed
 *        and they are finished when the PM_PBS_RETURN sequence is executed. No
 *        nested sub-routines are supported. Special write and read sequences are
 *        provided below which can be executed in a sub-routine(write and read
 *        sequence macros with the suffix _SUB). These sequences use the stored
 *        Slave ID (sid) and Peripheral ID (pid) values which are passed in as
 *        arguments in the PM_PBS_GOTO_SUB sequence.
 *
 *        This macro is used to go to a new absolute address location
 *        in the PBS logical address map and treat the code as a
 *        subroutine. Stores slave id (sid), peripheral id (pid) and PC.
 *        Returns on a PM_PBS_RETURN sequence.
 *
 *        PMIC address is broken down into Slave ID(sid), Peripheral
 *        ID (pid) and Register offset (offset).
 *        For example, address 0x12345 is broken into following.
 *        Slave ID (sid): 0x1
 *        Peripheral ID (pid): 0x23
 *        Register offset (offset): 0x45
 */
#define PM_PBS_GOTO_SUB(sid, pid, pbs_abs_addr)   (((((uint32)PM_PBS_OPCODE_GOTO_SUB) | ((sid)&0xFF))<<24) | (((pid)&0xFF)<<16) | ((pbs_abs_addr)&0xFFFF))

/**
 * @brief This macro should be called in a sub-routine and is used to write to a single register
 *          in given slave id (sid), peripheral id (pid) and register offset (offset) (uses stored
 *          slave id and peripheral id).
 *
 *        PMIC address is broken down into Slave ID(sid), Peripheral
 *        ID (pid) and Register offset (offset).
 *        For example, address 0x12345 is broken into following.
 *        Slave ID (sid): 0x1
 *        Peripheral ID (pid): 0x23
 *        Register offset (offset): 0x45
 */
#define PM_PBS_WRITE_REG_SUB(offset, data)   ((((uint32)PM_PBS_OPCODE_WRITE_REG_SUB)<<24) | (((offset)&0xFF)<<8) | ((data)&0xFF))

/**
 * @brief This macro should be called in a sub-routine and is used to read a register and store
 *          result in local buffer R0(uses stored slave id and peripheral id).
 *
 *        PMIC address is broken down into Slave ID(sid), Peripheral
 *        ID (pid) and Register offset (offset).
 *        For example, address 0x12345 is broken into following.
 *        Slave ID (sid): 0x1
 *        Peripheral ID (pid): 0x23
 *        Register offset (offset): 0x45
 */
#define PM_PBS_READ_REG_SUB(offset)   ((((uint32)PM_PBS_OPCODE_READ_REG_SUB)<<24) | (((offset)&0xFF)<<8))

/**
 * @brief This macro should be called in a sub-routine and is used to write to a single
 *          register in given slave id (sid), peripheral id (pid) and register offset (offset)
 *          using data from the local buffer R0(uses stored slave id and peripheral id).
 *
 *        PMIC address is broken down into Slave ID(sid), Peripheral
 *        ID (pid) and Register offset (offset).
 *        For example, address 0x12345 is broken into following.
 *        Slave ID (sid): 0x1
 *        Peripheral ID (pid): 0x23
 *        Register offset (offset): 0x45
 */
#define PM_PBS_WRITE_REG_SUB_BUF(offset)   ((((uint32)PM_PBS_OPCODE_WRITE_REG_SUB_BUF)<<24) | (((offset)&0xFF)<<8))

/**
 * @brief This macro should be called in a sub-routine and is used to return from Subroutine.
 *          Used to indicate End of Subroutine.
 *          PC = Stored PC.
 */
#define PM_PBS_RETURN   0x8CFFFF00


/*=========================================================================

                        TYPE DEFINITIONS

==========================================================================*/
/**
 * @enum pm_pbs_trigger_type
 * @brief PBS trigger types for which the PBS RAM sequences can be added.
 */
typedef enum
{
   PM_PBS_START_WARM_RESET,
   PM_PBS_START_POFF,
   PM_PBS_BUA_SEQUENCE,
   PM_PBS_GPIO_DTEST1,
   PM_PBS_GPIO_DTEST2,
   PM_PBS_GPIO_DTEST3,
   PM_PBS_GPIO_DTEST4,
   PM_PBS_TRIGGER_INVALID
}pm_pbs_trigger_type;


/*===========================================================================

                PBS RAM SEQUENCE FUNCTION PROTOTYPE

===========================================================================*/

/**
 * @name pm_pbs_add_ram_sequences
 *
 * @brief This function adds sequences passed by the user to PBS RAM
 *        for a specific trigger type and those sequences get executed
 *        when the corresponding trigger occurs. In case of WARM_RESET,
 *        POFF (Shutdown) and BUA triggers the added RAM sequences will
 *        get added prior to the existing PBS sequences for those trigger
 *        types and then branched to the existing sequence by the API. In
 *        case of DTEST trigger an EOS sequence will be added to the end
 *        of the added custom sequences.
 *
 * @param [in]  pmic_chip Primary PMIC chip: 0. Secondary PMIC Chip: 1
 * @param [in]  trigger Valid trigger types (WARM RESET, POFF and so on).
 *                     Check pm_pbs_trigger_type enum above for more info.
 * @param [in]  num_seqs Number of valid sequences user wants to add.
 *
 * @param [out]  seq_ptr Valid sequences to be added to the PBS RAM. Please
 *                     refer to list of valid sequence macros above to be
 *                     used to come up with sequences.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__INVALID_POINTER = Null ointer passed in.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not supported.
 *          PM_ERR_FLAG__DATA_VERIFY_FAILURE = if the sequence is not
 *          loaded correctly to PBS RAM or if a valid sequence already
 *          exists at the reserved PBS RAM address space for adding the
 *          sequences.
 *          else SPMI errors.
 *
 * @warning This function has to be called after pm_device_init
 *          and before TZ gets initialized.
 */
pm_err_flag_type pm_pbs_add_ram_sequences(uint8 pmic_chip,
                                          pm_pbs_trigger_type trigger,
                                          uint32* seq_ptr,
                                          uint8 num_seqs);

#endif    /* PM_PBS_H */
