#ifndef PM_CLK_H
#define PM_CLK_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_clk.h  PMIC CLOCK Module related declaration.
 *
 * @brief This header file contains functions and variable declarations
 *   to support Qualcomm PMIC MEGA XO module.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_clk.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/13/13   hw      Rearchitecting clock module driver to peripheral driver
12/06/12   hw      Remove comdef.h and use com_dtypes.h
03/14/12   hs      Initial version.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @enum pm_clk_type
 * @brief PMIC Clock index.
 */
typedef enum
{
	PM_CLK_SLEEP,
	PM_CLK_XO,
	PM_CLK_RF_1,
	PM_CLK_RF_2,
	PM_CLK_RF_3,
    PM_CLK_BB_1,
    PM_CLK_BB_2,
    PM_CLK_DIFF_1,
    PM_CLK_DIV_1,
    PM_CLK_DIV_2,
    PM_CLK_DIV_3,
		PM_CLK_DIST,
		PM_CLK_LN_BB,
    PM_ALL_CLKS,
	PM_CLK_INVALID
	}pm_clk_type;


/**
 * @enum pm_clk_drv_strength_type
 * @brief Clock drive strength.
 */
typedef enum
{
    PM_CLK_DRV_STRENGTH_1X,
    PM_CLK_DRV_STRENGTH_2X,
    PM_CLK_DRV_STRENGTH_3X,
    PM_CLK_DRV_STRENGTH_4X,
    PM_CLK_DRV_STRENGTH_INVALID,
}pm_clk_drv_strength_type;


/*===========================================================================

                PMIC CLOCK DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * @name pm_clk_drv_strength
 *
 * @brief Sets the output clock buffer drive strength.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index Selects which clock peripheral to use
 * @param [in] strength Select the strength desired
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available
 */
pm_err_flag_type pm_clk_drv_strength(uint8 pmic_chip,
									 pm_clk_type perph_index,
									 pm_clk_drv_strength_type strength);

/**
 * @name pm_clk_sw_enable
 *
 * @brief Enables the clock.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index Selects which clock peripheral to use
 * @param [in] on_off Turn a clock off or on ( enable or disable )
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available
 */
pm_err_flag_type pm_clk_sw_enable(uint8 pmic_chip,
								  pm_clk_type perph_index,
								  pm_on_off_type on_off);

/**
 * @name pm_clk_pull_down
 *
 * @brief Enables clock pull down.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index Selects which clock peripheral to use
 * @param [in] on_off Switch the pull down on or off ( enable or disable )
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available
 */
pm_err_flag_type pm_clk_pull_down(uint8 pmic_chip,
								  pm_clk_type perph_index,
								  pm_on_off_type on_off);

/**
 * @name pm_clk_pin_ctrled
 *
 * @brief Enables clock to be pin_controlled.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index Selects which clock peripheral to use
 * @param [in] on_off Switch the pin control capability on or off ( enable or disable )
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available
 */
pm_err_flag_type pm_clk_pin_ctrled( uint8 pmic_chip,
									pm_clk_type perph_index,
									pm_on_off_type on_off );

#endif    /* PM_CLK_H */
