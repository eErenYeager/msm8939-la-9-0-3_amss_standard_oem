#ifndef PM_RGB_H
#define PM_RGB_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
*
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
*
*/

/**
 * @file pm_rgb.h  PMIC rgb Module related declaration.
 *
 * @brief This file contains functions and variable declarations to support
 *        the PMIC RGB LED module.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_rgb.h#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
//#include "pm_lib_err.h"

#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @enum pm_rgb_which_type
 * @brief Type for selecting the available RGB peripherals.
 */
typedef enum
{
  PM_RGB_1,
  PM_RGB_INVALID
} pm_rgb_which_type;

/**
 * @brief LED bitmasks for RED.
 */
#define PM_RGB_SEGMENT_R (1 << 0)
/**
 * @brief LED bitmasks for GREEN.
 */
#define PM_RGB_SEGMENT_G (1 << 1)
/**
 * @brief LED bitmasks for BLUE.
 */
#define PM_RGB_SEGMENT_B (1 << 2)
/**
 * @brief LED bitmasks for ALL.
 */
#define PM_RGB_SEGMENT_ALL 0xFFFFFFFF

/**
 * @brief LED brightness level MIN.
 */
#define PM_RGB_DIM_LEVEL_MIN  0x00
/**
 * @brief LED bitmasks for LOW.
 */
#define PM_RGB_DIM_LEVEL_LOW  0x2A
/**
 * @brief LED bitmasks for MEDIUM.
 */
#define PM_RGB_DIM_LEVEL_MID  0x54
/**
 * @brief LED bitmasks for HIGH.
 */
#define PM_RGB_DIM_LEVEL_HIGH 0x80
/**
 * @brief LED bitmasks for MAX.
 */
#define PM_RGB_DIM_LEVEL_MAX  0xFF

/**
 * @enum pm_rgb_voltage_source_type
 * @brief Type for selecting the possible RGB LED voltage sources.
 */
typedef enum
{
  PM_RGB_VOLTAGE_SOURCE_GND,
  PM_RGB_VOLTAGE_SOURCE_VPH,
  PM_RGB_VOLTAGE_SOURCE_5V,
  PM_RGB_VOLTAGE_SOURCE_MIN_VCHG_5V,
  PM_RGB_VOLTAGE_SOURCE_INVALID
} pm_rgb_voltage_source_type;

/**
 * @struct pm_rgb_status_type
 * @brief Structure for RGB status.
 */
typedef struct
{
  uint32                      enabled_mask;    /*!< Mask enabled */
  uint32                      enabled_atc_mask;    /*!< ATC LED mask */
  pm_rgb_voltage_source_type  voltage_source;    /*!< LED voltage source */
} pm_rgb_status_type;


/*===========================================================================

                    RGB LED DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * @name pm_rgb_set_voltage_source
 *
 * @brief This function sets the voltage source for the given RGB peripheral.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] rgb_peripheral RGB peripheral identifier
 * @param [in] source Voltage source to use for the RGB
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__SBI_OPT_ERR = Communication with PM chip failed.
 */
pm_err_flag_type pm_rgb_set_voltage_source(unsigned pmic_device_index,
                                           pm_rgb_which_type rgb_peripheral,
                                           pm_rgb_voltage_source_type source);

/**
 * @name pm_rgb_enable
 *
 * @brief This function enables the invidual segments of a RGB LED.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] rgb_peripheral RGB peripheral identifier.
 * @param [in] rgb_mask Selects which RGB LED to toggle
 * @param [in] enable Boolean to enable/disable the indicated RGB segments.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__SBI_OPT_ERR = Communication with PM chip failed.
 */
pm_err_flag_type pm_rgb_enable(unsigned pmic_device_index,
                                pm_rgb_which_type rgb_peripheral,
                                uint32 rgb_mask,
                                boolean enable,
                                boolean atc);

/**
 * @name pm_rgb_enable_atc
 *
 * @brief This function enables the ATC (automatic trickle charge) mode of
 *        the invidual segments of a RGB LED.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] rgb_peripheral RGB peripheral identifier.
 * @param [in] rgb_mask Selects which RGB LED to toggle
 * @param [in] enable Boolean to enable/disable the ATC mode of the indicated RGB segments.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__SBI_OPT_ERR = Communication with PM chip failed.
 */
pm_err_flag_type pm_rgb_enable_atc(unsigned pmic_device_index,
                                    pm_rgb_which_type rgb_peripheral,
                                    uint32 rgb_mask,
                                    boolean enable);

/**
 * @name pm_rgb_get_status
 *
 * @brief This function returns the status of the RGB LED.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] rgb_peripheral RGB peripheral identifier.
 *
 * @param [out] status Structure to store the status of LED.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__SBI_OPT_ERR = Communication with PM chip failed.
 */
pm_err_flag_type pm_rgb_get_status(unsigned pmic_device_index,
                                   pm_rgb_which_type rgb_peripheral,
                                   pm_rgb_status_type *status);

/**
 * @name pm_rgb_led_config
 *
 * @brief This function enables toggling RGB LEDs.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] rgb_peripheral RGB peripheral identifier
 * @param [in] rgb_mask Selects which RGB LED to toggle
 * @param [in] source_type Selects which voltage source to use for RGB pripheral
 * @param [in] dim_level Selects the RGB LED brightness level
 * @param [in] enable_rgb Enable LED. TRUE: Enable LED, FALSE: Disable LED
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PARAM[input_param#]_OUT_OF_RANGE = Invalid input parameter.
 *          PM_ERR_FLAG__SBI_OPT_ERR = Communication with PM chip failed.
 */
pm_err_flag_type pm_rgb_led_config(unsigned pmic_device_index,
                                    pm_rgb_which_type rgb_peripheral,
                                    uint32 rgb_mask,
                                    pm_rgb_voltage_source_type source_type,
                                    uint8 dim_level,
                                    boolean enable_rgb);


#endif    /* PM_RGB_H */

