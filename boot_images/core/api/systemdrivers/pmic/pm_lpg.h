#ifndef PM_LPG_H
#define PM_LPG_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
*
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
*
*/

/**
 * @file pm_lpg.h  PMIC LPG Module related declaration.
 *
 * @brief This file contains functions and variable declarations to support
 *        the PMIC LPG module.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_lpg.h#1 $
$DateTime: 2015/03/19 01:58:37 $ $Author: pwbldsvc $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
01/14/13   al      Adding get_status
10/23/12   al      Updating copyright info and adding additional lut apis
10/16/12   al      added LUT support
08/21/12   al      Added enums required for 8974 LPG
07/27/12   al      Renamed all Enums,added device index and resource index
05/24/11   dy      Port to UEFI
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_version.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @brief LPG PWM Pre-divide max exponent.
 */
#define PM_LPG_PWM_PRE_DIVIDE_EXP_MAX   0x8
/**
 * @brief LPG max index.
 */
#define PM_LPG_INDEX_MAX                0x3F
/**
 * @brief LPG pause max.
 */
#define PM_LPG_PAUSE_MAX                0x38
/**
 * @brief LPG interval max.
 */
#define PM_LPG_INTERVAL_MAX             0xF
/**
 * @brief LPG Bank Max no.
 */
#define PM_LPG_BANK_MAX                 8

/**
 * @enum pm_lpg_pwm_output_type
 * @brief LPG PWM output type.
 */
typedef enum
{
    PM_LPG_EN_PWM_HI =0,
    PM_LPG_EN_PWM_LO ,
    PM_LPG_EN_PWM_INVALID
}pm_lpg_pwm_output_type;

/**
 * @enum pm_lpg_src_type
 * @brief LPG source type.
 */
typedef enum
{
    PM_LPG_PWM_SRC_LUT =0,
    PM_LPG_PWM_SRC_PWM_REGISTER ,
    PM_LPG_PWM_SRC_INVALID
}pm_lpg_src_type;

/**
 * @enum pm_lpg_chan_type
 * @brief LPG channel 1 to 7.
 */
typedef enum
{
    PM_LPG_CHAN_NONE,
    PM_LPG_CHAN_1 ,
    PM_LPG_CHAN_2 ,
    PM_LPG_CHAN_3 ,
    PM_LPG_CHAN_4 ,
    PM_LPG_CHAN_5 ,
    PM_LPG_CHAN_6 ,
    PM_LPG_CHAN_7 ,
    PM_LPG_CHAN_8,
    PM_LPG_CHAN_INVALID
}pm_lpg_chan_type;

/**
 * @enum pm_lpg_pwm_size_type
 * @brief LPG Bit Size (6 or 9 bit for generic LPG),
 *        7 bit PWM (8 bit piecewise mapping), 8 bit PWM (keypad BL only).
 */
typedef enum
{
    PM_LPG_PWM_6BIT,
    PM_LPG_PWM_7BIT,
    PM_LPG_PWM_8BIT,
    PM_LPG_PWM_9BIT,
    PM_LPG_PWM_SIZE_MAX
} pm_lpg_pwm_size_type;


/**
 * @enum pm_lpg_pwm_clock_type
 * @brief LPG PWM Clock Select.
 */
typedef enum
{
    PM_LPG_PWM_OFF,
    PM_LPG_PWM_1_0_KHZ,
    PM_LPG_PWM_32_0_KHZ,
    PM_LPG_PWM_19_2_MHZ,
    PM_LPG_PWM_CLOCK_MAX
} pm_lpg_pwm_clock_type;


/**
 * @enum pm_lpg_pwm_pre_divide_type
 * @brief PWM Pre-divider value.
 */
typedef enum
{
    PM_LPG_PWM_PRE_DIV_1=0,
    PM_LPG_PWM_PRE_DIV_3=1,
    PM_LPG_PWM_PRE_DIV_5=2,
    PM_LPG_PWM_PRE_DIV_6=3,
    PM_LPG_PWM_PRE_DIV_MAX
} pm_lpg_pwm_pre_divide_type;


/**
 * @enum pm_lpg_pwm_freq_expo_type
 * @brief PWM freq expo value
 */
typedef enum
{
    PM_LPG_PWM_FREQ_EXPO_0,
    PM_LPG_PWM_FREQ_EXPO_1,
    PM_LPG_PWM_FREQ_EXPO_2,
    PM_LPG_PWM_FREQ_EXPO_3,
    PM_LPG_PWM_FREQ_EXPO_4,
    PM_LPG_PWM_FREQ_EXPO_5,
    PM_LPG_PWM_FREQ_EXPO_6,
    PM_LPG_PWM_FREQ_EXPO_7,
    PM_LPG_PWM_FREQ_EXPO_MAX
} pm_lpg_pwm_freq_expo_type;

/**
 * @enum pm_lpg_phase_stag_shift_type
 * @brief LPG phase shift from previous slice.
 */
typedef enum
{
    PM_LPG_PHASE_STAG_SHIFT_0_DEG,    /*!< 0 degree shift from previous slice*/
    PM_LPG_PHASE_STAG_SHIFT_90_DEG,   /*!< 90 degree shift from previous slice*/
    PM_LPG_PHASE_STAG_SHIFT_180_DEG,  /*!< 180 degree shift from previous slice*/
    PM_LPG_PHASE_STAG_SHIFT_270_DEG,  /*!< 270 degree shift from previous slice*/
    PM_LPG_PHASE_STAG_SHIFT_INVALID   /*!< invalid shift from previous slice*/
}pm_lpg_phase_stag_shift_type;

/**
 * @enum pm_lpg_irq_type
 * @brief LPG IRQ type.
 */
typedef enum
{
    PM_LPG_INT_RT_STS =0,
    PM_LPG_INT_SET_TYPE,
    PM_LPG_INT_POLARITY_HIGH,
    PM_LPG_INT_POLARITY_LOW,
    PM_LPG_INT_LATCHED_CLR,
    PM_LPG_INT_EN_SET,
    PM_LPG_INT_EN_CLR,
    PM_LPG_INT_LATCHED_STS,
    PM_LPG_INT_PENDING_STS,
    PM_LPG_INT_MID_SEL,
    PM_LPG_INT_PRIORITY,
    PM_LPG_IRQ_INVALID
}pm_lpg_irq_type;

/**
 * @struct pm_lpg_status_type
 * @brief LPG status.
 */
typedef struct
{
   boolean pwm_output;   /*!< LPG PWM output enabled or disabled */
   uint16 pwm_value;     /*!< LPG PWM value for duty cycle */
   pm_lpg_pwm_pre_divide_type pre_div;   /*!< LPG PWM frequency pre-divider */
   pm_lpg_pwm_freq_expo_type exp;   /*!< LPG PWM frequency divider exponent */
   pm_lpg_pwm_clock_type pwm_freq_clk;   /*!< LPG PWM clock frequency */
   pm_lpg_pwm_size_type  pwm_bit_size;   /*!< LPG PWM bit size */
   pm_lpg_src_type lpg_src_type;   /*!< LPG source */
   boolean glitch_removal;   /*!< Glitch removal enable/disable*/
   boolean full_scale;   /*!< LPG full scale enabled or disabled */
   boolean en_phase_stagger;   /*!< LPG phase stagger enabled or disabled */
   pm_lpg_phase_stag_shift_type phase_stagger;   /*!< LPG phase shift */
   boolean ramp_direction;   /*!< LPG ramp direction HI or LO */
   boolean pattern_repeat;   /*!< LPG pattern repeat enabled or disabled */
   boolean ramp_toggle;   /*!< LPG ramp toggle enabled or disabled */
   boolean en_pause_hi;   /*!< LPG pause high enabled or disabled */
   boolean en_pause_lo;   /*!< LPG pause low enabled or disabled */
   uint32 lut_low_index;   /*!< LPG LUT low index value */
   uint32 lut_high_index;   /*!< LPG LUT high index value */
   uint32 interval_count;   /*!< LPG interval count value*/
}pm_lpg_status_type;


/*===========================================================================

                    LPG DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * @name pm_lpg_pwm_enable
 *
 * @brief This function Enable/Disable PWM for LPG.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] enable Enable/Disable PWM.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_pwm_enable(unsigned pmic_device_index,
                                    pm_lpg_chan_type pm_lpg_chan,
                                    boolean enable );

/**
 * @name pm_lpg_pwm_output_enable
 *
 * @brief This function enables/disables LPG PWM output.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] enable Enable/Disable PWM output.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_pwm_output_enable(unsigned pmic_device_index,
                                          pm_lpg_chan_type pm_lpg_chan,
                                          boolean enable );

/**
 * @name pm_lpg_pwm_set_pwm_value
 *
 * @brief This function sets the PWM value used to calculate duty cycle.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] pwm_value PWM value used to calculate duty cycle.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_pwm_set_pwm_value(unsigned pmic_device_index,
                                          pm_lpg_chan_type pm_lpg_chan,
                                          uint16 pwm_value );

/**
 * @name pm_lpg_pwm_set_pre_divide
 *
 * @brief This function sets the PWM pre-divider and exponent used to calculate duty cycle.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] pre_div Pre-divider value.
 * @param [in] exp Pre-divider exponent.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_pwm_set_pre_divide(unsigned pmic_device_index,
                                            pm_lpg_chan_type pm_lpg_chan,
                                            pm_lpg_pwm_pre_divide_type pre_div,
                                            pm_lpg_pwm_freq_expo_type exp);

/**
 * @name pm_lpg_pwm_clock_sel
 *
 * @brief This function sets PWM clock.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] clock PWM clock freq.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_pwm_clock_sel(unsigned pmic_device_index,
                                      pm_lpg_chan_type pm_lpg_chan,
                                      pm_lpg_pwm_clock_type clock);

/**
 * @name pm_lpg_set_pwm_bit_size
 *
 * @brief This function sets PWM size (6 or 9 bit).
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] bit_size PWM bit size.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_set_pwm_bit_size(unsigned pmic_device_index,
                                         pm_lpg_chan_type pm_lpg_chan,
                                         pm_lpg_pwm_size_type bit_size);

/**
 * @name pm_lpg_bank_enable
 *
 * @brief This function enables a LPG bank.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] bank Select LPG Bank Number (0-7).
 * @param [in] enable Enable/Disable LPG bank.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_bank_enable( unsigned pmic_device_index, uint8 bank, boolean enable );

/**
 * @name pm_lpg_bank_select
 *
 * @brief This function selects the LPG bank.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] bank Select LPG Bank Number (0-7).
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_bank_select( unsigned pmic_device_index, uint8 bank );

/**
 * @name pm_lpg_pwm_value_bypass_enable
 *
 * @brief This function enables user-inputted PWM value.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] enable Enable/Disable PWM bypass.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_pwm_value_bypass_enable(unsigned pmic_device_index,
                                                pm_lpg_chan_type pm_lpg_chan,
                                                boolean enable);

/**
 * @name pm_lpg_init
 *
 * @brief This function initializes the LPG module.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pmic_model Current PMIC HW model
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_init(unsigned pmic_device_index, pm_model_type pmic_model);

/**
 * @name pm_lpg_pwm_src_select
 *
 * @brief Selects LPG PWM source.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] lpg_src_type LPG source type.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_lpg_pwm_src_select (unsigned pmic_device_index,
                                        pm_lpg_chan_type pm_lpg_chan,
                                        pm_lpg_src_type lpg_src_type);

/**
 * @name pm_lpg_config_pwm_type
 *
 * @brief Configures the PWM type.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] glitch_removal FLASE : No glitch removal -PWM outputs are updated immediately
 *                            TRUE : glitch removal WM outputs are updated only on PWM period boundaries
 * @param [in] full_scale FALSE: PWM value is linear between code 0x00 and code 0xFF
 *                        TRUE: PWM value is linear between code 0x00, 0x01 and 0xFF
 * @param [in] en_phase_stagger FALSE: Disable phase staggering
 *                              TRUE: Enable phase staggering
 * @param [in] phase_stagger Degree shift from previous slice.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_lpg_config_pwm_type(unsigned pmic_device_index,
                                        pm_lpg_chan_type pm_lpg_chan,
                                        boolean glitch_removal,
                                        boolean full_scale,
                                        boolean en_phase_stagger,
                                        pm_lpg_phase_stag_shift_type phase_stagger);

/**
 * @name pm_lpg_pattern_config
 *
 * @brief Configures LPG pattern.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] ramp_direction FLASE : ramp from start (HI_INDEX) to end (LO_INDEX)
 *                            TRUE :  ramp from start (LO_INDEX) to end (HI_INDEX)
 * @param [in] pattern_repeat FLASE : no looping (stop after 1 pattern
 *                            TRUE :  pattern looping until this bit is set to 0
 * @param [in] ramp_toggle FALSE: return to start when end is reached
 *                         TRUE:  if start was LO_INDEX, then return to LO_INDEX when HI_INDEX
 *                           value is reached (pause is disabled) if start was HI_INDEX, then return
 *                         to HI_INDEX when LO_INDEX value is reached (pause is disabled).
 * @param [in] en_pause_hi FLASE : disable
 *                         TRUE :  enable pause time at HI_INDEX (timing defined in pause hi counter)
 * @param [in] en_pause_lo FLASE : disable
 *                         TRUE :  enable pause time at HI_INDEX (timing defined in pause hi counter)
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_lpg_pattern_config(unsigned pmic_device_index,
                                        pm_lpg_chan_type pm_lpg_chan,
                                        boolean ramp_direction,
                                        boolean pattern_repeat,
                                        boolean ramp_toggle,
                                        boolean en_pause_hi,
                                        boolean en_pause_lo);

/**
 * @name pm_lpg_lut_config_set
 *
 * @brief API to write a value to the Look up table.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] lut_index LUT byte number from 0 to 63.
 * @param [in] pwm_value 16 bit value(msb and lsb) for LUT byte number.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_lpg_lut_config_set(unsigned pmic_device_index, int lut_index, int pwm_value);

/**
 * @name pm_lpg_lut_config_get
 *
 * @brief API to read one value from the LPG look up table.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] lut_index Look up table index (0 to 63)
 * @param [in] pwm_value The look up table value.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_lut_config_get(unsigned pmic_device_index, int lut_index, int *pwm_value);

/**
 * @name pm_lpg_lut_config_set_array
 *
 * @brief This API writes a number of values to consecutive
 *        locations in the LUT starting at the index given.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] start_index  which of the look up table entries to write.
 * @param [in] count Number of entries in the array.
 *
 * @param [out] value Pointer to an array of 9 bit PWM values. The
 *              array must be large enough to contain at least "count" values.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_lut_config_set_array(unsigned pmic_device_index, int start_index, int *value, int count);

/**
 * @name pm_lpg_lut_config_get_array
 *
 * @brief API to read values from consecutive locations in the
 *        LUT starting at the index given and store the values
 *        to the array passed.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] start_index  which of the look up table entries to read.
 * @param [in] count Number of entries in the array.
 *
 * @param [out] value Pointer to an array of 9 bit PWM values. The
 *              array must be large enough to contain at least "count" values.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_lut_config_get_array(unsigned pmic_device_index, int start_index, int *value, int count);

/**
 * @name pm_lpg_pwm_ramp_generator_start
 *
 * @brief Starts the lut ramp for a selected lpg.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_lpg_pwm_ramp_generator_start(unsigned pmic_device_index, pm_lpg_chan_type pm_lpg_chan);

/**
 * @name pm_lpg_pwm_lut_index_set
 *
 * @brief This API sets the look up table index used for loop and ramp functions.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] low_index  Low look up table index to use with loop and ramp functions.
 *                   Range is 0 to 63.
 * @param [in] high_index High look up table index to use with loop and ramp functions.
 *                   Range is 0 to 63.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_pwm_lut_index_set(unsigned pmic_device_index,
                                          pm_lpg_chan_type pm_lpg_chan,
                                          int low_index,
                                          int high_index);

/**
 * @name pm_lpg_config_pause_time
 *
 * @brief API  to configure high and low pause time for LPG.
 *
 * @details This API  configures  pause time by setting  ramp step, pause_hi multiplier and pause_lo multiplier
 *          high pause time = ramp_step * hi_multiplier    milli seconds
 *          low pause time = ramp_step * low_multiplier    milli seconds
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] ramp_step Ramp step, also time between access of 2 entries in the LUT, in milliseconds.
 *                       Valid value is 0 to 511 milliseconds.
 * @param [in] hi_multiplier Multiplier for HI_INDEX. Valid value is 0 to 0xFFF.
 * @param [in] low_multiplier Multiplier for LOW_INDEX. Valid value is 0 to 0xFFF.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_config_pause_time(unsigned pmic_device_index,
                                          pm_lpg_chan_type pm_lpg_chan,
                                          uint32 ramp_step,
                                          uint32 hi_multiplier,
                                          uint32 low_multiplier);

/**
 * @name pm_lpg_pwm_ramp_generator_enable
 *
 * @brief API to enable or disable the PWM ramp generator.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 * @param [in] enable TRUE enable the LPG ramp generator
 *                    FALSE disable the LPG ramp generator
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_pwm_ramp_generator_enable(unsigned pmic_device_index,
                                                  pm_lpg_chan_type pm_lpg_chan,
                                                  boolean enable);

/**
 * @name pm_lpg_get_status
 *
 * @brief API to read LPG status.
 *
 * @param [in] pmic_device_index Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] pm_lpg_chan LPG channel number.
 *
 * @param [out] lpg_status Pointer to LPG status struct.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_lpg_get_status(unsigned pmic_device_index,
                                    pm_lpg_chan_type pm_lpg_chan,
                                    pm_lpg_status_type *lpg_status);

#endif    /* PM_LPG_H */
