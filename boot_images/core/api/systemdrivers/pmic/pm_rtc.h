#ifndef PM_RTC_H
#define PM_RTC_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_rtc.h PMIC RTC related declaration.
 *
 * @brief This file contains enums and API definitions for Real Time Clock Driver.
 */

/*===========================================================================

           R T C   S E R V I C E S   H E A D E R   F I L E

DESCRIPTION
  This file contains functions prototypes and variable/type/constant 
  declarations for the RTC services developed for the Qualcomm Power
  Management IC.
  
  Copyright (c) 2003-2009, 2013 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
===========================================================================*/

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_rtc.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
11/08/13   kt      Created.
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "com_dtypes.h"
#include "pm_resources_and_types.h"


/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

/*===========================================================================

                    RTC DRIVER FUNCTION PROTOTYPE

===========================================================================*/
/**
 * @name pm_rtc_get_time
 * 
 * @brief This function returns the current time of the RTC (in secs).
 *        This will be the actual present time if the RTC has been
 *        ticking or the time at which the RTC was last stopped.
 * 
 * @param[in] pmic_device_index. Primary PMIC: 0 Secondary PMIC: 1
 * 
 * @param[out] time_ptr: Current RTC tick count (in secs)
 *
 * @return  pm_err_flag_type 
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__RTC_HALTED = RTC is not running.
 *          PM_ERR_FLAG__INVALID_POINTER = NULL time pointer passed in.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__SPMI_OPT_ERR = SPMI error.
 */
pm_err_flag_type pm_rtc_get_time(uint8 pmic_device_index, uint32 *time_ptr);

#endif    /* PM_RTC_H */
