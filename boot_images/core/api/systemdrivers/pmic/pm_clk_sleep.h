#ifndef PM_CLK_SLEEP_H
#define PM_CLK_SLEEP_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
*/

/**
 * @file pm_clk_sleep.h CLOCK SLEEP Functionality related declaration.
 *
 * @brief This header file contains functions and variable declarations
 *        for Clock sleep functionality.
 */
/* ======================================================================= */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_clk_sleep.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/13/13   hw      Rearchitecting clk module driver to peripheral driver
========================================================================== */

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "pm_clk.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/
/**
 * @enum pm_clk_sleep_smpl_delay_type
 * @brief This enum contains definitions of Sleep SMPL Delay in Sec.
 */
typedef enum 
{
   PM_DELAY_0P5_SEC, 
   PM_DELAY_1P0_SEC, 
   PM_DELAY_1P5_SEC, 
   PM_DELAY_2P0_SEC, 
   PM_DELAY_INVALID,
}pm_clk_sleep_smpl_delay_type;


/*===========================================================================

                CLOCK SLEEP DRIVER FUNCTION PROTOTYPE

===========================================================================*/
/**
 * @name pm_clk_sleep_smpl
 *
 * @brief Turn on or off (enable or disable) SMPL (Sudden Momentum Power Loss)
 *
 * @param [in] pmic_chip  Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] perph_index Selects which clk to use
 * @param [in] strenght Select the strength desired.
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_clk_sleep_smpl(uint8 pmic_chip, pm_clk_type perph_indx, pm_on_off_type on_off, pm_clk_sleep_smpl_delay_type delay);

#endif    /* PM_CLK_SLEEP_H */

