#ifndef PM_MPPS_H
#define PM_MPPS_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
*
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
*
*/

/**
 * @file pm_mpp.h PMIC MPPs related declaration.
 *
 * @brief This header file contains enums and API definitions for MPPS driver.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_mpp.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
05/09/13   aab     Added MPP enable API
02/27/13   al      Adding IRQ related APIs
01/22/13   al      Add rt. interrupt
07/27/12   al       Renamed all Enums,added device index and resource index
05/18/11   dy      Port to UEFI
05/03/11   dy      Rename digital logic level enumerations
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
07/26/10   wra     Changed documentation from @example to Examples so Deoxygen
                    can parse the file
07/11/10   jtn     Aded APIs to get MPP status, corrected pm_mpp_dlogic_inout_pup_type
                   to reflect PMIC HW capabilities
07/02/10   wra     Changed pm_mpp_perph_index to int
03/15/10   fpe     CMI Merge - Made the interface common between 7x30 and SCMM
03/05/10   vk      Added API pm_mpp_config_dtest_output()
10/20/09   jtn     Move init function prototype to pm_device_init_i.h
10/19/09   vk      Removed init API
07/06/09   jtn     Added enums for PM8028 digital logic levels
01/26/09   Vk      Introduced following APIs
                   pm_config_secure_mpp_config_digital_input()
                   pm_secure_mpp_config_digital_input()
12/17/08   APU     Introduced the APIs:
                   1. pm_secure_mpp_config_i_sink ()
                   2. pm_make_secure__mpp_config_i_sink ()
08/01/08   jtn     Move all proc comm functions to pm_pcil.c
06/27/08   jtn     Merge changes from QSC1100 branch, consolidated
                   definition for max. number of MPPs
06/24/08   vk      Provided API that maintains the list of MPPs that have shunt
                   capacitors to ground. In the MPP configuration API for
                   anaolog output this list is used.
03/01/08   jtn     Corrected number of MPPs for Han
02/27/08   jtn     Added support for Kip PMIC
01/07/08   jtn     Merged from QSC6270 branch
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_version.h"
#include "pm_resources_and_types.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @enum pm_mpp_perph_index
 * @brief MPP index. Which MPP needs to be configured.
 */
typedef enum
{
  PM_MPP_1,
  PM_MPP_2,
  PM_MPP_3,
  PM_MPP_4,
  PM_MPP_5,
  PM_MPP_6,
  PM_MPP_7,
  PM_MPP_8,
  PM_MPP_9,
  PM_MPP_10,
  PM_MPP_11,
  PM_MPP_12,
  PM_MPP_13,
  PM_MPP_14,
  PM_MPP_15,
  PM_MPP_16,
  PM_MPP_INVALID,
} pm_mpp_perph_index;

/**
 * @enum pm_mpp_dlogic_lvl_type
 * @brief DIGITAL LOGIC LEVEL. Configures the output logic level.
 */
typedef enum
{
  PM_MPP__DLOGIC__LVL_VIO_0 = 0,
  PM_MPP__DLOGIC__LVL_VIO_1,
  PM_MPP__DLOGIC__LVL_VIO_2,
  PM_MPP__DLOGIC__LVL_VIO_3,
  PM_MPP__DLOGIC__LVL_VIO_4,
  PM_MPP__DLOGIC__LVL_VIO_5,
  PM_MPP__DLOGIC__LVL_VIO_6,
  PM_MPP__DLOGIC__LVL_VIO_7,
  PM_MPP__DLOGIC__LVL_INVALID = 8,
} pm_mpp_dlogic_lvl_type;

/**
 * @enum pm_mpp_dlogic_in_dbus_type
 * @brief MODE = DIGITAL INPUT. Configures the output logic.
 */
typedef enum
{
  PM_MPP__DLOGIC_IN__DBUS1,
  PM_MPP__DLOGIC_IN__DBUS2,
  PM_MPP__DLOGIC_IN__DBUS3,
  PM_MPP__DLOGIC_IN__DBUS4,    /*!< added for badger */
  PM_MPP__DLOGIC_IN__DBUS_INVALID
}pm_mpp_dlogic_in_dbus_type;

/**
 * @enum pm_mpp_dlogic_out_ctrl_type
 * @brief MODE = DIGITAL OUT. Configures the output logic.
 */
typedef enum
{
  PM_MPP__DLOGIC_OUT__CTRL_LOW,    /*!< MPP OUTPUT= LOGIC LOW */
  PM_MPP__DLOGIC_OUT__CTRL_HIGH,   /*!< MPP OUTPUT= LOGIC HIGH */
  PM_MPP__DLOGIC_OUT__CTRL_MPP,    /*!< MPP OUTPUT= CORRESPONDING MPP INPUT */
  PM_MPP__DLOGIC_OUT__CTRL_NOT_MPP,  /*!< MPP OUTPUT= CORRESPONDING INVERTED MPP INPUT */
  PM_MPP__DLOGIC_OUT__CTRL_INVALID
}pm_mpp_dlogic_out_ctrl_type;

/**
 * @enum pm_mpp_dlogic_inout_pup_type
 * @brief MODE = BIDIRECTIONAL. Configures the pull up resistor.
 */
typedef enum
{
  PM_MPP__DLOGIC_INOUT__PUP_1K,  /*!< PULL UP = 1  K Ohms*/
  PM_MPP__DLOGIC_INOUT__PUP_10K, /*!< PULL UP = 10 K Ohms*/
  PM_MPP__DLOGIC_INOUT__PUP_30K, /*!< PULL UP = 30 K Ohms*/
  PM_MPP__DLOGIC_INOUT__PUP_OPEN,  /*!< PULL UP = OPEN */
  PM_MPP__DLOGIC_INOUT__PUP_INVALID
}pm_mpp_dlogic_inout_pup_type;

/**
 * @enum pm_mpp_dlogic_out_dbus_type
 * @brief MODE = DIGITAL TEST OUTPUT.
 */
typedef enum
{
  PM_MPP__DLOGIC_OUT__DBUS1,
  PM_MPP__DLOGIC_OUT__DBUS2,
  PM_MPP__DLOGIC_OUT__DBUS3,
  PM_MPP__DLOGIC_OUT__DBUS4,
  PM_MPP__DLOGIC_OUT__DBUS_INVALID
}pm_mpp_dlogic_out_dbus_type;

/**
 * @enum pm_mpp_ain_ch_type
 * @brief The types defined below are used to configure the following MPPs modes:
 *        ANALOG INPUT.
 */
typedef enum
{
  PM_MPP__AIN__CH_AMUX5,
  PM_MPP__AIN__CH_AMUX6,
  PM_MPP__AIN__CH_AMUX7,
  PM_MPP__AIN__CH_AMUX8,
  PM_MPP__AIN__CH_ABUS1,
  PM_MPP__AIN__CH_ABUS2,
  PM_MPP__AIN__CH_ABUS3,
  PM_MPP__AIN__CH_ABUS4,
  PM_MPP__AIN__CH_INVALID
}pm_mpp_ain_ch_type;

/**
 * @enum pm_mpp_aout_level_type
 * @brief The types defined below are used to configure the following MPPs modes:
 *        ANALOG OUTPUT.
 */
typedef enum
{
  PM_MPP__AOUT__LEVEL_VREF_1p25_Volts,
  PM_MPP__AOUT__LEVEL_VREF_0p625_Volts,
  PM_MPP__AOUT__LEVEL_VREF_0p3125_Volts,
  PM_MPP__AOUT__LEVEL_INVALID
}pm_mpp_aout_level_type;

/**
 * @enum pm_mpp_aout_switch_type
 * @brief Analog OUTPUT Switch type.
 */
typedef enum
{
  PM_MPP__AOUT__SWITCH_OFF,
  PM_MPP__AOUT__SWITCH_ON,
  PM_MPP__AOUT__SWITCH_ON_IF_MPP_HIGH,
  PM_MPP__AOUT__SWITCH_ON_IF_MPP_LOW,
  PM_MPP__AOUT__SWITCH_INVALID
}pm_mpp_aout_switch_type;

/**
 * @enum pm_mpp_i_sink_level_type
 * @brief The types defined below are used to configure the following MPPs modes:
 *        CURRENT SINK.
 */
typedef enum
{
  PM_MPP__I_SINK__LEVEL_5mA,
  PM_MPP__I_SINK__LEVEL_10mA,
  PM_MPP__I_SINK__LEVEL_15mA,
  PM_MPP__I_SINK__LEVEL_20mA,
  PM_MPP__I_SINK__LEVEL_25mA,
  PM_MPP__I_SINK__LEVEL_30mA,
  PM_MPP__I_SINK__LEVEL_35mA,
  PM_MPP__I_SINK__LEVEL_40mA,
  PM_MPP__I_SINK__LEVEL_INVALID
}pm_mpp_i_sink_level_type;

/**
 * @enum pm_mpp_i_sink_switch_type
 * @brief MPP Current sink switch type.
 */
typedef enum
{
  PM_MPP__I_SINK__SWITCH_DIS,
  PM_MPP__I_SINK__SWITCH_ENA,
  PM_MPP__I_SINK__SWITCH_ENA_IF_MPP_HIGH,
  PM_MPP__I_SINK__SWITCH_ENA_IF_MPP_LOW,
  PM_MPP__I_SINK__SWITCH_INVALID
}pm_mpp_i_sink_switch_type;

/**
 * @enum pm_mpp_ch_atest_type
 * @brief MPP channel ATEST
 */
typedef enum
{
  PM_MPP__CH_ATEST1,
  PM_MPP__CH_ATEST2,
  PM_MPP__CH_ATEST3,
  PM_MPP__CH_ATEST4,
  PM_MPP__CH_ATEST_INVALID
}pm_mpp_ch_atest_type;

/**
 * @struct pm_mpp_digital_input_status_type
 * @brief Structure used for returning digital input configuration
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;    /*!< MPP Dlogic level */
    pm_mpp_dlogic_in_dbus_type mpp_dlogic_in_dbus;    /*!< MPP Dlogic in Dbus */
} pm_mpp_digital_input_status_type;

/**
 * @struct pm_mpp_digital_output_status_type
 * @brief Structure used for returning digital output configuration
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;    /*!< MPP Dlogic level */
    pm_mpp_dlogic_out_ctrl_type mpp_dlogic_out_ctrl;    /*!< MPP Dlogic out control */
} pm_mpp_digital_output_status_type;

/**
 * @struct pm_mpp_bidirectional_status_type
 * @brief Structure used for returning bidirectional configuration
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;    /*!< MPP Dlogic level */
    pm_mpp_dlogic_inout_pup_type mpp_dlogic_inout_pup;    /*!< MPP Dlogic in-out pull-up */
}  pm_mpp_bidirectional_status_type;

/**
 * @struct pm_mpp_analog_input_status_type
 * @brief Structure used for returning analog input configuration
 */
typedef struct
{
    pm_mpp_ain_ch_type mpp_ain_ch;    /*!< MPP analog in channel */
}  pm_mpp_analog_input_status_type;

/**
 * @struct pm_mpp_analog_output_status_type
 * @brief Structure used for returning analog output configuration
 */
typedef struct
{
    pm_mpp_aout_level_type mpp_aout_level;    /*!< MPP analog out level */
    pm_mpp_aout_switch_type mpp_aout_switch;    /*!< MPP analog out switch */
}  pm_mpp_analog_output_status_type;

/**
 * @struct pm_mpp_current_sink_status_type
 * @brief Structure used for returning current sink configuration
 */
typedef struct
{
    pm_mpp_i_sink_level_type  mpp_i_sink_level;    /*!< MPP current sink level */
    pm_mpp_i_sink_switch_type mpp_i_sink_switch;    /*!< MPP current sink switch */
}  pm_mpp_current_sink_status_type;

/**
 * @struct pm_mpp_dtest_output_status_type
 * @brief Structure used for returning DTEST output configuration
 */
typedef struct
{
    pm_mpp_dlogic_lvl_type mpp_dlogic_lvl;    /*!< MPP Dlogic level */
    pm_mpp_dlogic_out_dbus_type  mpp_dlogic_out_dbus;    /*!< MPP Dlogic out Dbus */
} pm_mpp_dtest_output_status_type;

/**
 * @enum pm_mpp_config_type
 * @brief Enumeration of available MPP modes
 */
typedef enum
{
    PM_MPP_CFG_DIGITAL_IN,
    PM_MPP_CFG_DIGITAL_OUT,
    PM_MPP_CFG_BIDIRECTIONAL,
    PM_MPP_CFG_ANALOG_IN,
    PM_MPP_CFG_ANALOG_OUT,
    PM_MPP_CFG_ISINK,
    PM_MPP_CFG_DTEST_OUT
} pm_mpp_config_type;


/**
 * @struct pm_mpp_status_type
 * @brief Structure type used for returning MPP status
 *          The structure that has valid data will depend on
 *          the MPP mode (input, output, etc.) returned in mpp_config.
 */
typedef struct
{
    pm_mpp_config_type mpp_config;    /*!< This returns the MPP's mode (input, output, etc.) */
    union
    {
        pm_mpp_digital_input_status_type mpp_digital_input_status;
        pm_mpp_digital_output_status_type mpp_digital_output_status;
        pm_mpp_bidirectional_status_type mpp_bidirectional_status;
        pm_mpp_analog_input_status_type mpp_analog_input_status;
        pm_mpp_analog_output_status_type mpp_analog_output_status;
        pm_mpp_current_sink_status_type mpp_current_sink_status;
        pm_mpp_dtest_output_status_type mpp_dtest_output_status;
    } mpp_config_settings;    /*!< Union of structures used to return MPP status */
} pm_mpp_status_type;


/*===========================================================================

                    MPP DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * @name pm_mpp_status_get
 *
 * @brief Returns the status of one of the PMIC MPPs.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 *
 * @param [out] mpp_status Pointer to the MPP status structure
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 */
pm_err_flag_type pm_mpp_status_get(unsigned pmic_chip,
                                   pm_mpp_perph_index mpp,
                                   pm_mpp_status_type *mpp_status);

/**
 * @name pm_mpp_config_digital_input
 *
 * @brief  Configure the selected Multi Purpose pin (MPP) to be a digital input pin.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] level The logic level reference that we want to use with this MPP.
 * @param [in] dbus Which data line will the MPP drive. Please note that only
 *             one MPP can be assigned per DBUS.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__DBUS_IS_BUSY_MODE = The DBUS is busy.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
extern pm_err_flag_type pm_mpp_config_digital_input(unsigned pmic_chip,
                                                    pm_mpp_perph_index mpp,
                                                    pm_mpp_dlogic_lvl_type level,
                                                    pm_mpp_dlogic_in_dbus_type dbus);

/**
 * @name pm_mpp_config_digital_output
 *
 * @brief Configure the selected Multi Purpose pin (MPP) to be a digital output pin.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] level The logic level reference that we want to use with this MPP.
 * @param [in] output_ctrl The output setting of the selected MPP.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__DBUS_IS_BUSY_MODE = The DBUS is busy.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
extern pm_err_flag_type pm_mpp_config_digital_output(unsigned pmic_chip,
                                                    pm_mpp_perph_index mpp,
                                                    pm_mpp_dlogic_lvl_type level,
                                                    pm_mpp_dlogic_out_ctrl_type output_ctrl);


/**
 * @name pm_mpp_config_digital_inout
 *
 * @brief Configure the selected Multi Purpose pin (MPP) to be a digital output pin.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] level The logic level reference that we want to use with this MPP.
 * @param [in] pull_up The pull-up resistor setting of the selected MPP.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__DBUS_IS_BUSY_MODE = The DBUS is busy.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
extern pm_err_flag_type pm_mpp_config_digital_inout(unsigned pmic_chip,
                                                    pm_mpp_perph_index mpp,
                                                    pm_mpp_dlogic_lvl_type level,
                                                    pm_mpp_dlogic_inout_pup_type pull_up);

/**
 * @name pm_mpp_config_dtest_output
 *
 * @brief Configure the selected Multi Purpose pin (MPP) to be a digital test output pin.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] level The logic level reference that we want to use with this MPP.
 * @param [in] dbus Which data line will the MPP drive.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__DBUS_IS_BUSY_MODE = The DBUS is busy.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
pm_err_flag_type pm_mpp_config_dtest_output(unsigned pmic_chip,
                                            pm_mpp_perph_index mpp,
                                            pm_mpp_dlogic_lvl_type level,
                                            pm_mpp_dlogic_out_dbus_type dbus);


/**
 * @name pm_mpp_config_analog_input
 *
 * @brief This function configures the selected MPP as an Analog Input.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] ch_select Which analog mux or analog bus will the selected MPP be routed to.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__DBUS_IS_BUSY_MODE = The DBUS is busy.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
extern pm_err_flag_type pm_mpp_config_analog_input(unsigned pmic_chip,
                                                   pm_mpp_perph_index mpp,
                                                   pm_mpp_ain_ch_type ch_select);

/**
 * @name pm_mpp_config_analog_output
 *
 * @brief This function configures the selected MPP as an analog output.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] level The logic level reference that we want to use with this MPP.
 * @param [in] OnOff This option allows the user to enable/disable the MPP output.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__DBUS_IS_BUSY_MODE = The DBUS is busy.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
extern pm_err_flag_type pm_mpp_config_analog_output(unsigned pmic_chip,
                                                    pm_mpp_perph_index mpp,
                                                    pm_mpp_aout_level_type level,
                                                    pm_mpp_aout_switch_type OnOff);

/**
 * @name pm_mpp_config_i_sink
 *
 * @brief This function configures the selected MPP as an analog output.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] level The logic level reference that we want to use with this MPP.
 * @param [in] OnOff This option allows the user to enable/disable current sink.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__DBUS_IS_BUSY_MODE = The DBUS is busy.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
extern pm_err_flag_type pm_mpp_config_i_sink(unsigned pmic_chip,
                                             pm_mpp_perph_index mpp,
                                             pm_mpp_i_sink_level_type level,
                                             pm_mpp_i_sink_switch_type OnOff);

/**
 * @name pm_mpp_config_atest
 *
 * @brief This function configures the selected MPP as ATEST.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] atest_select Which atest will the selected MPP be routed to.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
extern pm_err_flag_type pm_mpp_config_atest(unsigned pmic_chip,
                                            pm_mpp_perph_index mpp,
                                            pm_mpp_ch_atest_type atest_select);

/**
 * @name pm_mpp_enable
 *
 * @brief This function enables or disables MPP.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] enable TRUE: Enable mpp
 *                    FALSE:Disable  mpp
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
extern pm_err_flag_type pm_mpp_enable(unsigned pmic_chip, pm_mpp_perph_index mpp, boolean enable);

/**
 * @name pm_mpp_irq_enable
 *
 * @brief This function enables or disables mpp irq.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param[in] enable TRUE: Enable interrupt for the mpp
 *                   FALSE:Disable interrupt for the mpp
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
pm_err_flag_type pm_mpp_irq_enable(unsigned pmic_chip, pm_mpp_perph_index mpp, boolean enable);

/**
 * @name pm_mpp_irq_clear
 *
 * @brief This function clears the mpp irq .
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
pm_err_flag_type pm_mpp_irq_clear(unsigned pmic_chip, pm_mpp_perph_index mpp);

/**
 * @name pm_mpp_irq_set_trigger
 *
 * @brief This function configures the mpp for irq.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] trigger Different irq triggers.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__PAR2_OUT_OF_RANGE = Input Parameter 2 is out of range.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
pm_err_flag_type pm_mpp_irq_set_trigger(unsigned pmic_chip,
                                        pm_mpp_perph_index mpp,
                                        pm_irq_trigger_type trigger);

/**
 * @name pm_mpp_irq_status
 *
 * @brief This function configures the mpp for irq.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 * @param [in] type Type of IRQ status to read.
 *
 * @param [out] status IRQ status.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
pm_err_flag_type pm_mpp_irq_status(unsigned pmic_chip,
                                    pm_mpp_perph_index mpp,
                                    pm_irq_status_type type,
                                    boolean *status);

/**
 * @name pm_mpp_set_list_mpp_with_shunt_cap
 *
 * @brief This function list mpp with the shunt cap.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
pm_err_flag_type pm_mpp_set_list_mpp_with_shunt_cap(unsigned pmic_chip, pm_mpp_perph_index mpp);

/**
 * @name pm_mpp_get_mpp_with_shunt_cap_list_status_for_device
 *
 * @brief This function get mpp with the shunt cap.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] mpp Which MPP
 *
 * @param [out] shuntList Pointer to the list of mpps with the Shunt cap.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Input Parameter 1 is out of range.
 *          PM_ERR_FLAG__SBI_OPT_ERR = The SBI driver failed to communicate with the PMIC.
 *
 * @warning Interrupts are disable while communicating with the PMIC.
 */
pm_err_flag_type pm_mpp_get_mpp_with_shunt_cap_list_status_for_device(unsigned pmic_chip,
                                                                      pm_mpp_perph_index mpp,
                                                                      uint32 *shuntList);

#endif    /* PM_UEFI_MPPS_H */
