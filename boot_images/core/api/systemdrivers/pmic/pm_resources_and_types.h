#ifndef PM_RESOURCES_AND_TYPES_H
#define PM_RESOURCES_AND_TYPES_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_resources_and_types.h PMIC Resources related declaration.
 *
 * @brief This file contains enumerations with resource names for different
 *        module types that should be used by external clients when accessing
 *        the resources that are required.
 */

/*===========================================================================

                  P M    H E A D E R    F I L E

===========================================================================*/


/*===========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_resources_and_types.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
03/13/13   hw      Rearchitecting clk module driver to peripheral driver
02/27/13   kt      Added common IRQ enums
01/22/13   aab     Added support for PM8026 and PM8110
09/25/12   hw      moving some npa type to this file so we don't need to include pmapp_npa.h
09/29/11   hs      Added CLK resource IDs.
09/14/11   dy      Move UICC resource IDs to this file
04/25/11   wra     Adding Battery Temperature Management BTM, LPG, more LVS, ADC_ARB,
                    OVP & INTerrupt channel enumeration needed for PM8921
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
07/05/10   wra     Modified Header includes to accomodate MSM8660
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "DALStdDef.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @brief Invalid Data
 */
#define PM_INVALID_DATA 99

/**
 * @brief This enum contains definitions of all chargers in the target
 */
enum
{
  PM_CHG_1,
  PM_CHG_INVALID
};

/**
 * @brief PMIC power module voltage level
 */
typedef uint32 pm_volt_level_type;

/**
 * @enum pm_on_off_type
 * @brief Regulator On/OFF type.
 */
typedef enum
{
   PM_OFF,    /*!< Power OFF */
   PM_ON,    /*!< Power ON */
   PM_INVALID
}pm_on_off_type;

/**
 * @enum pm_sw_mode_type
 * @brief Regulator Power mode type
 */
typedef enum
{
    PM_SW_MODE_LPM     = 0,       /*!< Low power mode    */
    PM_SW_MODE_BYPASS  = 1,       /*!< Bypass mode       bit 5 */
    PM_SW_MODE_AUTO    = 2,       /*!< Auto mode         bit 6 */
    PM_SW_MODE_NPM     = 4,       /*!< Normal power mode bit 7 */
    PM_SW_MODE_INVALID
}pm_sw_mode_type;

/**
 * @struct PeripheralVersionInformationType
 * @brief Contains Peripheral Information such as Type, Subtype,
 *          Analog and Digital Major Versions.
 */
typedef struct
{
    uint16 base_address;
    uint8  peripheral_type;
    uint8  peripheral_subtype;
    uint8  analog_major_version;
    uint8  analog_minor_version;
    uint8  digital_major_version;
    uint8  digital_minor_version;
}peripheral_info_type;

/**
 * @enum pm_npa_generic_enable_type
 * @brief Generic enable.
 */
typedef enum
{
    PM_NPA_GENERIC_DISABLE, // default
    PM_NPA_GENERIC_ENABLE,
    PM_NPA_GENERIC_INVALID
}pm_npa_generic_enable_type;

/**
 * @enum pm_npa_bypass_allowed_type
 * @brief Bypass Allowed
 */
typedef enum
{
    PM_NPA_BYPASS_ALLOWED, // default
    PM_NPA_BYPASS_DISALLOWED,
    PM_NPA_BYPASS_INVALID
}pm_npa_bypass_allowed_type;

/**
 * @enum pm_npa_sw_mode_ldo_type
 * @brief LDO Software Mode
 */
typedef enum
{
    PM_NPA_SW_MODE_LDO__IPEAK, // default
    PM_NPA_SW_MODE_LDO__NPM,
    PM_NPA_SW_MODE_LDO__INVALID
}pm_npa_sw_mode_ldo_type;

/**
 * @enum pm_npa_sw_mode_smps_type
 * @brief SMPS Software Mode
 */
typedef enum
{
    PM_NPA_SW_MODE_SMPS__AUTO, // default
    PM_NPA_SW_MODE_SMPS__IPEAK,
    PM_NPA_SW_MODE_SMPS__NPM,
    PM_NPA_SW_MODE_SMPS__INVALID
}pm_npa_sw_mode_smps_type;

/**
 * @enum pm_npa_pin_control_enable_type
 * @brief Pin Control Enable
 */
typedef enum
{
    PM_NPA_PIN_CONTROL_ENABLE__NONE = 0, // default
    PM_NPA_PIN_CONTROL_ENABLE__EN1 = 1,
    PM_NPA_PIN_CONTROL_ENABLE__EN2 = 2,
    PM_NPA_PIN_CONTROL_ENABLE__EN3 = 4,
    PM_NPA_PIN_CONTROL_ENABLE__EN4 = 8,
    PM_NPA_PIN_CONTROL_ENABLE__INVALID
}pm_npa_pin_control_enable_type;

/**
 * @enum pm_npa_pin_control_power_mode_type
 * @brief Pin Control Power Mode
 */
typedef enum
{
    PM_NPA_PIN_CONTROL_POWER_MODE__NONE = 0, // default
    PM_NPA_PIN_CONTROL_POWER_MODE__EN1 = 1,
    PM_NPA_PIN_CONTROL_POWER_MODE__EN2 = 2,
    PM_NPA_PIN_CONTROL_POWER_MODE__EN3 = 4,
    PM_NPA_PIN_CONTROL_POWER_MODE__EN4 = 8,
    PM_NPA_PIN_CONTROL_POWER_MODE__SLEEPB = 16,
    PM_NPA_PIN_CONTROL_POWER_MODE__INVALID
}pm_npa_pin_control_power_mode_type;

/**
 * @enum pm_npa_corner_mode_type
 * @brief Pin Control Power Mode
 */
typedef enum
{
    PM_NPA_CORNER_MODE__NONE = 0, // default
    PM_NPA_CORNER_MODE__1 = 1,
    PM_NPA_CORNER_MODE__2 = 2,
    PM_NPA_CORNER_MODE__3 = 3,
    PM_NPA_CORNER_MODE__4 = 4,
    PM_NPA_CORNER_MODE__5 = 5,
    PM_NPA_CORNER_MODE__6 = 6,
    PM_NPA_CORNER_MODE__INVALID
}pm_npa_corner_mode_type;

/**
 * @enum pm_npa_clk_buff_pin_control_enable_type
 * @brief Clock buffer Pin Control Enable
 */
typedef enum
{
    PM_NPA_CLK_BUFFER_PIN_CONTROL_ENABLE__NONE = 0, // default
    PM_NPA_CLK_BUFFER_PIN_CONTROL_ENABLE__EN1 = 1,
    PM_NPA_CLK_BUFFER_PIN_CONTROL_ENABLE__INVALID
}pm_npa_clk_buff_pin_control_enable_type;

/**
 * @enum pm_clk_buff_output_drive_strength_type
 * @brief Clock buffer output drive strenght.
 */
typedef enum
{
    PM_CLK_BUFF_OUT_DRV__1X,
    PM_CLK_BUFF_OUT_DRV__2X,
    PM_CLK_BUFF_OUT_DRV__3X,
    PM_CLK_BUFF_OUT_DRV__4X,
} pm_clk_buff_output_drive_strength_type;

/**
 * @enum pm_irq_trigger_type
 * @brief Type definition for different IRQ triggers.
 */
typedef enum
{
  PM_IRQ_TRIGGER_ACTIVE_LOW,
  PM_IRQ_TRIGGER_ACTIVE_HIGH,
  PM_IRQ_TRIGGER_RISING_EDGE,
  PM_IRQ_TRIGGER_FALLING_EDGE,
  PM_IRQ_TRIGGER_DUAL_EDGE,
  PM_IRQ_TRIGGER_INVALID
} pm_irq_trigger_type;

/**
 * @enum pm_irq_status_type
 * @brief Type definition for different IRQ STATUS.
 */
typedef enum
{
  PM_IRQ_STATUS_RT,
  PM_IRQ_STATUS_LATCHED,
  PM_IRQ_STATUS_PENDING,
  PM_IRQ_STATUS_ENABLE,
  PM_IRQ_STATUS_INVALID
} pm_irq_status_type;

#endif    /* PM_RESOURCES_AND_TYPES_H */

