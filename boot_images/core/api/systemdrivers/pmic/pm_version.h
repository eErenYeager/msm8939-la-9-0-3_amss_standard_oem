#ifndef PM_VERSION_H
#define PM_VERSION_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_version.h PMIC MODEL/VERSION related declaration.
 *
 * @brief This header file contains enums and API definitions for PMIC model/
 *          version detection.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_version.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
10/08/13   rk      Added PMIC model
04/16/13   kt      Added pm_get_pbs_info API.
01/28/13   kt      Adding pm_get_pmic_info API to get the pmic device info.
01/28/13   kt      Removing pm_set_hardware_version and pm_get_hardware_version APIs.
12/06/12   hw      Remove comdef.h and use com_dtypes.h
09/19/12   vk      Add PM8019
03/10/12   wra     Removed old PMIC versions and added Badger PMIC versions.
                   Reduced hardware versions in enumeration
10/18/11   jtn/mpt Add PM8821 and PM8038
04/04/11   hw      Added pm_get_hardware_version and pm_set_hardware_version API
02/08/11   hw      Merging changes from the PMIC Distributed Driver Arch branch
07/05/10   wra     Added pm_model_type entry for the PM8921 and PM8018
07/05/10   wra     Added file header and pm_model_type entry for the PM8901
                   and ISL9519
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "com_dtypes.h"
#include "pm_err_flags.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @enum pm_model_type
 * @brief This enum gives the PMIC model type values (Peripheral Subtype values)
 *      for different PMIC Models.
 */
typedef enum
{
   PMIC_IS_UNKNOWN   = 0,
   PMIC_IS_PM8941    = 1,
   PMIC_IS_PM8841    = 2,
   PMIC_IS_PM8019    = 3,
   PMIC_IS_PM8026    = 4,
   PMIC_IS_PM8110    = 5,
   PMIC_IS_PMA8084   = 6,
   PMIC_IS_PMI8962   = 7,
   PMIC_IS_PMD9635   = 8,
   PMIC_IS_PM8994    = 9,
   PMIC_IS_PMI8994   = 10,
   PMIC_IS_PM8916    = 11,
   PMIC_IS_INVALID   = 0x7FFFFFFF,
} pm_model_type;

/**
 * @struct pm_device_info_type
 * @brief Stores the PMIC's Model type value, the All Layer
 *        Revision number and the Metal Revision number. Please refer
 *        to pm_model_type enum above to get more info on which PMIC
 *        model type value represents which PMIC.For example, for
 *        PM8019 v2.1 the PMIC model type value is 3 (since PM8019
 *        model type value is 3 as per pm_model_type enum), All layer
 *        revision number is '2' and Metal revision number is '1'.
 */
typedef struct
{
  pm_model_type  ePmicModel;             /*!< PMIC device model type. */
  uint32         nPmicAllLayerRevision;  /*!< PMIC device all layer revision number. */
  uint32         nPmicMetalRevision;     /*!< PMIC device metal revision number. */
} pm_device_info_type;

/**
 * @brief Number of LOT IDs. Each Lot id represents an Ascii value.
 */
#define PM_PBS_INFO_NUM_LOT_IDS   12

/**
 * @struct pm_pbs_info_type
 * @brief Stores the PMIC PBS related information such as PBS Lot ID, ROM Version,
 *        RAM Version, Fab Id, Wafer Id, X co-ord, Y co-ord and Test PGM Rev.
 *        PBS ROM/RAM Revision id and Variant (or Branch) id are stored in last 16
 *        bits(upper and lower 8 bits) of rom_version and ram_version.
 */
typedef struct
{
  uint8          lot_id[PM_PBS_INFO_NUM_LOT_IDS];  /*!< PBS lot id */
  uint32         rom_version;                      /*!< PBS ROM Version number */
  uint32         ram_version;                      /*!< PBS RAM Version number */
  uint32         fab_id;                           /*!< PBS Fab Id */
  uint32         wafer_id;                         /*!< PBS Wafer Id */
  uint32         x_coord;                          /*!< PBS X Coord */
  uint32         y_coord;                          /*!< PBS Y Coord */
  uint32         test_pgm_rev;                     /*!< PBS Test PGM Rev */
} pm_pbs_info_type;


/*===========================================================================

                PMIC MODEL/VERSION READ FUNCTION PROTOTYPE

===========================================================================*/
/**
 * @name pm_get_pmic_model
 *
 * @brief This function returns the PMIC's model type value.
 *        For example, the API returns '1' for PM8941, '2'
 *        for PM8841 and so on. Please refer to pm_model_type
 *        enum above to get more info on which PMIC model type
 *        value represents which PMIC.
 *
 * @param [in] pmic_device_index Primary PMIC:0 Secondary PMIC:1
 *
 * @return  pm_model_type Returns valid PMIC model/tier type if successful.
 *                          Returns PMIC_IS_INVALID under error conditions (like
 *                          invalid device index param).
 *
 */
pm_model_type pm_get_pmic_model(uint8 pmic_device_index);

/**
 * @name pm_get_pmic_revision
 *
 * @brief This function returns the PMIC's All Layer revision
 *        number. For example, the API returs '1' for PMIC v1.x,
 *        '2' for PMIC v2.x  and so on.
 *
 * @param [in] pmic_device_index Primary PMIC:0 Secondary PMIC:1
 *
 * @return uint8 Returns valid PMIC All Layer revision number if successful.
 *                 Returns 0 under error conditions (like invalid device index param).
 *
 */
uint8 pm_get_pmic_revision(uint8 pmic_device_index);

/**
 * @name pm_get_pmic_info
 *
 * @brief This function returns information about PMIC device
 *        for a specific device index in the pmic_device_info
 *        argument. This API returns PMIC's model type value,
 *        All layer revision number and Metal revision number in
 *        pmic_device_info structure. Please refer to
 *        pm_device_info_type structure above for more info. For
 *        example, for PM8019 v2.1 the PMIC model type value is
 *        3 (since PM8019 model type value is 3 as per
 *        pm_model_type enum), All layer revision number is '2'
 *        and Metal revision number is '1'.
 *
 * @param [in]  pmic_device_index Primary PMIC:0 Secondary PMIC:1
 *
 * @param [out] pmic_device_info Variable to return to the caller with PMIC device info.
 *                                 Please refer to pm_device_info_type structure above for
 *                                 more info on this structure.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 *
 */
pm_err_flag_type pm_get_pmic_info(uint8 pmic_device_index, pm_device_info_type* pmic_device_info);

/**
 * @name pm_get_pbs_info
 *
 * @brief This function returns information about PMIC device
 *        PBS Lot ID, ROM Version, RAM Version, Fab Id, Wafer
 *        Id, X coord, Y coord and test pgm rev for a specific
 *        device index in the pbs_info argument.
 *
 * @param [in]  pmic_device_index Primary PMIC:0 Secondary PMIC:1
 *
 * @param [out] pbs_info Variable to return to the caller with PMIC device PBS info.
 *                         Please refer to pm_pbs_info_type structure above for more info
 *                         on this structure.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *          PM_ERR_FLAG__PAR1_OUT_OF_RANGE = Device Index out of range.
 *          PM_ERR_FLAG__INVALID_POINTER = Null pointer passed in.
 *          PM_ERR_FLAG__INVALID = Error in collecting and placing PBS info.
 */
pm_err_flag_type pm_get_pbs_info(uint8 pmic_device_index, pm_pbs_info_type* pbs_info);

#endif    /* PM_VERSION_H */
