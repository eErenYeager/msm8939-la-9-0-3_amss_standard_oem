#ifndef PM_LDO_H
#define PM_LDO_H

/**
 * Copyright (c) 2013-2014 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * Qualcomm Confidential and Proprietary
 *
 * Export of this technology or software is regulated by the U.S. Government.
 * Diversion contrary to U.S. law prohibited.
 *
 * All ideas, data and information contained in or disclosed by
 * this document are confidential and proprietary information of
 * Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
 * By accepting this material the recipient agrees that this material
 * and the information contained therein are held in confidence and in
 * trust and will not be used, copied, reproduced in whole or in part,
 * nor its contents revealed in any manner to others without the express
 * written permission of Qualcomm Technologies Incorporated.
 *
 */

/**
 * @file pm_ldo.h PMIC LDO related declaration.
 *
 * @brief This header file contains enums and API definitions for PMIC voltage
 *         regulator driver.
 */

/* ==========================================================================

                  P M    H E A D E R    F I L E

========================================================================== */


/* ==========================================================================

                        EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/api/systemdrivers/pmic/pm_ldo.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/18/14   mr      Doxygen complaint PMIC Header (CR-602405)
12/06/12   hw      Rearchitecturing module driver to peripheral driver
===========================================================================*/

/*===========================================================================

                        PMIC INCLUDE FILES

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"


/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/
/**
 * @brief LDO Peripheral Index.
 *        This enum type contains all ldo regulators that you may need.
 */
enum
{
  PM_LDO_1,
  PM_LDO_2,
  PM_LDO_3,
  PM_LDO_4,
  PM_LDO_5,
  PM_LDO_6,
  PM_LDO_7,
  PM_LDO_8,
  PM_LDO_9,
  PM_LDO_10,
  PM_LDO_11,
  PM_LDO_12,
  PM_LDO_13,
  PM_LDO_14,
  PM_LDO_15,
  PM_LDO_16,
  PM_LDO_17,
  PM_LDO_18,
  PM_LDO_19,
  PM_LDO_20,
  PM_LDO_21,
  PM_LDO_22,
  PM_LDO_23,
  PM_LDO_24,
  PM_LDO_25,
  PM_LDO_26,
  PM_LDO_27,
  PM_LDO_28,
  PM_LDO_29,
  PM_LDO_30,
  PM_LDO_31,
  PM_LDO_32,
  PM_LDO_33,
  PM_LDO_INVALID
};


/*===========================================================================

                    LDO DRIVER FUNCTION PROTOTYPES

===========================================================================*/
/**
 * @name pm_ldo_pull_down
 *
 * @brief Allows you to enable disable active pulldown.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] ldo_peripheral_index LDO Identifier
 * @param [in] on_off Turn on and off active pulldown
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_ldo_pull_down(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type on_off);

/**
 * @name pm_ldo_sw_mode
 *
 * @brief Switch between HPM, LPM, and other modes of a regulator.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] ldo_peripheral_index LDO Identifier
 * @param [in] sw_mode Select the different mode of a regulator. Example, HPM, LPM
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_ldo_sw_mode(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_sw_mode_type sw_mode);

/**
 * @name pm_ldo_sw_mode_status
 *
 * @brief This function returns the mode status (LPM, NPM, AUTO, BYPASS)
 *        of the selected power rail. Note, the mode of a regulator
 *        changes dynamically.
 *
 * @param [in] pmic_chip Primary: 0. Secondary: 1
 * @param [in] ldo_peripheral_index  LDO Identifier
 *
 * @param [out] sw_mode Variable to return to the caller with mode status.
 *                      Refer to pm_resources_and_types.h for the enum info.
 *
 * @return  pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS.
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_ldo_sw_mode_status(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_sw_mode_type* sw_mode);

/**
 * @name pm_ldo_pin_ctrled
 *
 * @brief Select the pin ( connected to external signal ) that you would like to use
 *        to effect the state ( on/off ) and mode ( HPM, LPM etc ) of a regulator.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] ldo_peripheral_index LDO Identifier
 * @param [in] select_pin Select a pin
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_ldo_pin_ctrled(uint8 pmic_chip, uint8 ldo_peripheral_index, uint8 select_pin);

/**
 * @name pm_ldo_sw_enable
 *
 * @brief Enable or disable a regulator or voltage switcher.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] ldo_peripheral_index LDO Identifier
 * @param [in] on_off Turn on and off the regulator
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_ldo_sw_enable(uint8 pmic_chip, uint8  ldo_peripheral_index, pm_on_off_type  on_off);

/**
 * @name pm_ldo_volt_level
 *
 * @brief Set the voltage of a regulator.
 *        Note, differnt type ( LDO, HFS etc ) may have different programmable voltage steps.
 *        Only support the correct programmable steps. Not rounding voltages if the voltage
 *          selected is not part of the programmable steps.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] ldo_peripheral_index LDO Identifier
 * @param [in] volt_level Select the voltage in micro volts
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 */
pm_err_flag_type pm_ldo_volt_level(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_volt_level_type volt_level);

/**
 * @name pm_ldo_volt_level_status
 *
 * @brief Return the status of volt level of LDO.
 *
 * @param [in] pmic_chip Primary PMIC: 0 Secondary PMIC: 1
 * @param [in] ldo_peripheral_index LDO Identifier
 *
 * @param [out] volt_level Variable to return to the caller with volt
 *                           level status in micro volts (uint32).
 *
 * @return pm_err_flag_type
 *          PM_ERR_FLAG__SUCCESS = SUCCESS
 *          PM_ERR_FLAG__FEATURE_NOT_SUPPORTED = Feature not available.
 *
 */
pm_err_flag_type pm_ldo_volt_level_status(uint8 pmic_chip,
                                          uint8 ldo_peripheral_index,
                                          pm_volt_level_type* volt_level);

#endif    /* PM_LDO_H */

