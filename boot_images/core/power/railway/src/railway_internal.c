/*===========================================================================

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "railway.h"

#include "railway_internal.h"
#include "railway_config.h"

railway_internals_t railway;

void railway_init(void)
{
    unsigned num_rails = RAILWAY_CONFIG_DATA->num_rails;

    unsigned rail_state_size = num_rails * sizeof(railway_rail_state_t);
    railway.rail_state = malloc(rail_state_size);
    CORE_VERIFY(railway.rail_state);
    memset(railway.rail_state, 0, rail_state_size);

    railway_target_init();
    railway_transitioner_init();

    const int vddcx_rail_id = rail_id("vddcx");
    assert(RAIL_NOT_SUPPORTED_BY_RAILWAY!=vddcx_rail_id);

    //init time request for lowest active level on CX internal
    railway_voter_t rpm_vdd_cx_voter = railway_create_voter(vddcx_rail_id, RAILWAY_RPM_CX_VOTER_ID);
    railway_corner_vote(rpm_vdd_cx_voter, RAILWAY_SVS_SOC);
    
    const int vddmx_rail_id = rail_id("vddmx");
    assert(RAIL_NOT_SUPPORTED_BY_RAILWAY!=vddmx_rail_id);

    //init time request for lowest active level on MX internal
    railway_voter_t rpm_vdd_mx_voter = railway_create_voter(vddmx_rail_id, RAILWAY_RPM_MX_VOTER_ID);
    railway_corner_vote(rpm_vdd_mx_voter, RAILWAY_SVS_SOC);
}

