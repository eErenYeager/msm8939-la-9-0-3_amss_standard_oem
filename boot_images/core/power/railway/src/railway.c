// railway.c - railway top-level implementation
//
// Copyright 2011 - 2012 by QUALCOMM Technologies, Inc.
// All Rights Reserved
// Confidental and Proprietary
//
// $ Header: $
// $ Author: $
// $ DateTime: $
//

#include "railway.h"

#include "railway_internal.h"
#include "railway_config.h"
#include "alloca.h"
#include "pm_ldo.h"
#include "pm_smps.h"
#include <string.h>
#include <stdbool.h>

static void initialize_rail(unsigned rail_num)
{
    const railway_rail_config_t *rail_config = &RAILWAY_CONFIG_DATA->rails[rail_num];

    railway_rail_state_t* rail_state = &railway.rail_state[rail_num];

    memcpy(rail_state->corner_uvs, rail_config->default_uvs, sizeof(unsigned[RAILWAY_CORNERS_COUNT]));

    // Compute what our initial request should be.
    railway_corner corner = rail_config->initial_corner;
    unsigned uv           = rail_state->corner_uvs[corner];

    // Update our initial state.
    rail_state->current_active.mode       = corner;
    rail_state->current_active.microvolts = uv;
}

static void railway_transition_individual_rail(int rail_num)
{
    railway_rail_state_t *rail_data = &railway.rail_state[rail_num];
    const railway_settings* target = &rail_data->constrained_target;

    // Only set the actual voltage if it's changing. It could be that we're just changing modes.
    // E.g. if CPR has taken the voltage down in one mode to the point that it is the same voltage as another mode.
    if(rail_data->constrained_target.microvolts!=rail_data->current_active.microvolts)
    {
        if(RAILWAY_CONFIG_DATA->rails[rail_num].pmic_rail_type==RAILWAY_SMPS_TYPE)
        {
            pm_smps_volt_level(RAILWAY_CONFIG_DATA->rails[rail_num].pmic_chip_id, RAILWAY_CONFIG_DATA->rails[rail_num].pmic_peripheral_index, target->microvolts);
        }
        else if(RAILWAY_CONFIG_DATA->rails[rail_num].pmic_rail_type==RAILWAY_LDO_TYPE)
        {
            pm_ldo_volt_level(RAILWAY_CONFIG_DATA->rails[rail_num].pmic_chip_id, RAILWAY_CONFIG_DATA->rails[rail_num].pmic_peripheral_index, target->microvolts);
        }
    }

    memcpy(&rail_data->current_active, target, sizeof(railway_settings));
}

static void railway_quantize_constrained_target(int rail)
{
    const railway_corner* supported_corners = RAILWAY_CONFIG_DATA->rails[rail].supported_corners;
    const railway_corner highest_supported_corner = supported_corners[RAILWAY_CONFIG_DATA->rails[rail].supported_corners_count-1];

    //Assert that we don't have an explicit request greater than our highest supported corner.
    CORE_VERIFY(railway.rail_state[rail].corner_uvs[highest_supported_corner]
            >=railway.rail_state[rail].constrained_target.microvolts);

    railway_corner quantized_corner=highest_supported_corner;

    // -2 because we don't need to check the highest supported corner - we start there.
    for(int i=RAILWAY_CONFIG_DATA->rails[rail].supported_corners_count-2; i>=0; i--)
    {
        //Check that we're not going lower than the constrained target's mode.
        if(supported_corners[i]<railway.rail_state[rail].constrained_target.mode)
        {
            break;
        }

        if(railway.rail_state[rail].constrained_target.microvolts>
            railway.rail_state[rail].corner_uvs[supported_corners[i]])
        {
            break;
        }
        quantized_corner = supported_corners[i];
    }

    railway.rail_state[rail].constrained_target.mode = quantized_corner;
    railway.rail_state[rail].constrained_target.microvolts = railway.rail_state[rail].corner_uvs[quantized_corner];
}

static void railway_update_constrained_targets( void )
{
    //Go through the rails and calculate their constrained voltage target.

    //Start from the unconstrained_target (which is the minimum for each rail) and work up until we settle on voltages.
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        memcpy(&railway.rail_state[i].constrained_target,
            &railway.rail_state[i].unconstrained_target,
            sizeof(railway_settings));
    }

    //Now, quantize logical rails to a corner.
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        if(i==MX_RAIL_ID)   //Skip Mx, we do that later.
            continue;

        railway_quantize_constrained_target(i);
    }

    //Now assert that the logical rails are multiples of the PMIC step size.
    //It's up to CPR to ensure that the corner values it sets are multiples of the PMIC step size.
    railway_rail_state_t* mx_rail = &railway.rail_state[MX_RAIL_ID];
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        if(i==MX_RAIL_ID)   //Skip Mx at this point as we're only looking at logical rails.
            continue;

        CORE_VERIFY(!(railway.rail_state[i].constrained_target.microvolts%RAILWAY_CONFIG_DATA->rails[i].pmic_step_size));
    }

    //Next, bump up MX to the max of the logical rails.
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        if(i==MX_RAIL_ID)   //Skip comparison with ourselves.
            continue;

        mx_rail->constrained_target.mode =
            MAX(mx_rail->constrained_target.mode, railway.rail_state[i].constrained_target.mode);
        mx_rail->constrained_target.microvolts =
            MAX(mx_rail->corner_uvs[mx_rail->constrained_target.mode], mx_rail->constrained_target.microvolts);
    }

    //Now quantize Mx.
    railway_quantize_constrained_target(MX_RAIL_ID);
}

static void railway_do_transition_rails(void)
{
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        //To do - move this to when we update votes on rails - I'm sure that will be more efficient.
        //Need to figure out how we deal with CPR changing corner voltages under our feet though.
        railway_aggregated_voltage_target_uv(i, &railway.rail_state[i].unconstrained_target);
    }

    //Now update the constrained targets for the rails.
    railway_update_constrained_targets();

    //Now do the transitions.
    //If MX is going up, do that first.
    if(railway.rail_state[MX_RAIL_ID].constrained_target.microvolts>railway.rail_state[MX_RAIL_ID].current_active.microvolts)
    {
        railway_transition_individual_rail(MX_RAIL_ID);
    }

    //Now transition the logical rails
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        if(i==MX_RAIL_ID)
            continue;

        if((railway.rail_state[i].constrained_target.microvolts!=railway.rail_state[i].current_active.microvolts) ||
            (railway.rail_state[i].constrained_target.mode!=railway.rail_state[i].current_active.mode))
        {
            railway_transition_individual_rail(i);
        }
    }

    //Now transition Mx if it's going down.
    if(railway.rail_state[MX_RAIL_ID].constrained_target.microvolts<railway.rail_state[MX_RAIL_ID].current_active.microvolts)
    {
        railway_transition_individual_rail(MX_RAIL_ID);
    }

    //Assertion that current_active == constrained_target for all rails.
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        CORE_VERIFY(railway.rail_state[i].constrained_target.microvolts == railway.rail_state[i].current_active.microvolts);
    }
}

static bool railway_transition_in_progress = false;

//Top level fn for transitioning rails.
void railway_transition_rails()
{
    CORE_VERIFY(!railway_transition_in_progress);    //We don't support this fn becoming re-entrant for now. Guard against it.
    railway_transition_in_progress = true;

    railway_do_transition_rails();
    
    railway_transition_in_progress = false;
}

void railway_transitioner_init(void)
{
    for(unsigned i = 0; i < RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        initialize_rail(i);
    }
}
