/*===========================================================================

  railway_internal.h - bits of state for railway, in a convenient global
                       wrapper for debug

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#ifndef RAILWAY_INTERNAL_H
#define RAILWAY_INTERNAL_H

#include "railway.h"
#include "boot_error_if.h"
#include "CoreVerify.h"

#define MX_RAIL_ID 0

typedef struct
{
    railway_settings    current_active;

    railway_settings    unconstrained_target;  //Should always be updated when the votes change, but isn't currently
    railway_settings    constrained_target;    //Re-calculated each time we transition rails.

    railway_voter_t         voter_list_head;
    unsigned        corner_uvs[RAILWAY_CORNERS_COUNT];
    } railway_rail_state_t;

typedef struct
{
    railway_rail_state_t *rail_state;
} railway_internals_t;

//Returns the target voltage and corner for the named rail.
//rail is not currently used - all votes are for VddCx.
//If for_sleep is true then this will ignore any suppressible votes - this option would
//only be used for sleep to know if vdd_min is possible.
void railway_aggregated_voltage_target_uv(int rail, railway_settings* target_setting);

extern railway_internals_t railway;

void railway_init(void);
void railway_transitioner_init(void);

//Any early target-specific init should be done in this function.
void railway_target_init(void);


#endif // RAILWAY_INTERNAL_H

