// cpr.h - interface description of cpr driver
//
// Copyright 2013 by Qualcomm Technologies, Inc.
// All Rights Reserved
// Confidental and Proprietary
//
// $ Header$
// $ Author$
// $ DateTime$

#include "cpr.h"

unsigned cpr_cx_mx_settings_hash(void) { return 0xDEADBEEF; }

void cpr_init(void) {}

void cpr_externalize_state(void) {}
