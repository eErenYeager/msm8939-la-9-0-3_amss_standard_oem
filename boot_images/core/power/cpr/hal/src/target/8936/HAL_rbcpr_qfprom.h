#ifndef __HAL_RBCPR_QFPROM_H__
#define __HAL_RBCPR_QFPROM_H__
/*
===========================================================================
*/
/**
  @file HAL_rbcpr_qfprom.h
  @brief Auto-generated HWIO interface include file.

  This file contains HWIO register definitions for the following modules:
    SECURITY_CONTROL_CORE
    TLMM_CSR

  'Include' filters applied: 
  'Exclude' filters applied: RESERVED DUMMY 
*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  Qualcomm Confidential and Proprietary

  Export of this technology or software is regulated by the U.S. Government.
  Diversion contrary to U.S. law prohibited.

  All ideas, data and information contained in or disclosed by
  this document are confidential and proprietary information of
  Qualcomm Technologies Incorporated and all rights therein are expressly reserved.
  By accepting this material the recipient agrees that this material
  and the information contained therein are held in confidence and in
  trust and will not be used, copied, reproduced in whole or in part,
  nor its contents revealed in any manner to others without the express
  written permission of Qualcomm Technologies Incorporated.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/power/cpr/hal/src/target/8936/HAL_rbcpr_qfprom.h#1 $
  $DateTime: 2015/03/19 01:58:37 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/
#define SECURITY_CONTROL_CORE_REG_BASE                                                         (SECURITY_CONTROL_BASE      + 0x00000000)




#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_BMSK                                                       0x700
#define HWIO_QFPROM_RAW_PTE2_FOUNDRY_ID_SHFT                                                         0x8

#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000d8)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_NOMINAL_QUOT_VMIN_7_0_BMSK                         0xff000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_NOMINAL_QUOT_VMIN_7_0_SHFT                               0x18
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_NOMINAL_TARGET_5_0_BMSK                              0xfc0000
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_NOMINAL_TARGET_5_0_SHFT                                  0x12
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_TURBO_QUOT_VMIN_11_0_BMSK                             0x3ffc0
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_TURBO_QUOT_VMIN_11_0_SHFT                                 0x6
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_TURBO_TARGET_5_0_BMSK                                    0x3f
#define HWIO_QFPROM_RAW_CALIB_ROW1_LSB_CPR1_TURBO_TARGET_5_0_SHFT                                     0x0

#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000dc)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW1_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW1_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW1_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TURBO_TARGET_BIT_4_0_BMSK                          0xf8000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR0_TURBO_TARGET_BIT_4_0_SHFT                                0x1b
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR2_SVS_TARGET_1_0_BMSK                                 0x6000000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR2_SVS_TARGET_1_0_SHFT                                      0x19
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_ROSEL_2_0_BMSK                                      0x1c00000
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_ROSEL_2_0_SHFT                                           0x16
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_SVS_QUOT_VMIN_11_0_BMSK                              0x3ffc00
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_SVS_QUOT_VMIN_11_0_SHFT                                   0xa
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_SVS_TARGET_5_0_BMSK                                     0x3f0
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_SVS_TARGET_5_0_SHFT                                       0x4
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_NOMINAL_QUOT_VMIN_11_8_BMSK                               0xf
#define HWIO_QFPROM_RAW_CALIB_ROW1_MSB_CPR1_NOMINAL_QUOT_VMIN_11_8_SHFT                               0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000e0)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_PH_B0M0_0_BMSK                                          0x80000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_PH_B0M0_0_SHFT                                                0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_G_B0_BMSK                                               0x70000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_G_B0_SHFT                                                     0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CLK_B_BMSK                                               0xc000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CLK_B_SHFT                                                    0x1a
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CAP_B_BMSK                                               0x3000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CAP_B_SHFT                                                    0x18
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_SAR_B_BMSK                                                0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_SAR_B_SHFT                                                    0x16
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CSI_PHY_BMSK                                              0x3e0000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CSI_PHY_SHFT                                                  0x11
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_DSI_PHY_BMSK                                               0x1e000
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_DSI_PHY_SHFT                                                   0xd
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_DSIPHY_PLL_BMSK                                             0x1e00
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_DSIPHY_PLL_SHFT                                                0x9
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_NOMINAL_TARGET_BIT_5_0_BMSK                              0x1f
#define HWIO_QFPROM_RAW_CALIB_ROW2_LSB_CPR0_NOMINAL_TARGET_BIT_5_0_SHFT                               0x0

#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000e4)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW2_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW2_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW2_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_G_B2_1_0_BMSK                                           0xc0000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_G_B2_1_0_SHFT                                                 0x1e
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_VREF_B1_BMSK                                            0x30000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_VREF_B1_SHFT                                                  0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B1M3_BMSK                                             0xe000000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B1M3_SHFT                                                  0x19
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B1M2_BMSK                                             0x1c00000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B1M2_SHFT                                                  0x16
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B1M1_BMSK                                              0x380000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B1M1_SHFT                                                  0x13
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B1M0_BMSK                                               0x70000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B1M0_SHFT                                                  0x10
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_G_B1_BMSK                                                   0xe000
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_G_B1_SHFT                                                      0xd
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_VREF_B0_BMSK                                                0x1800
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_VREF_B0_SHFT                                                   0xb
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B0M3_BMSK                                                 0x700
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B0M3_SHFT                                                   0x8
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B0M2_BMSK                                                  0xe0
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B0M2_SHFT                                                   0x5
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B0M1_BMSK                                                  0x1c
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B0M1_SHFT                                                   0x2
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B0M0_2_1_BMSK                                               0x3
#define HWIO_QFPROM_RAW_CALIB_ROW2_MSB_PH_B0M0_2_1_SHFT                                               0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000e8)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_LSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_LSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_LSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_STEP1_CORR_2_0_BMSK                              0xe0000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_STEP1_CORR_2_0_SHFT                                    0x1d
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_STEP0_CORR_BMSK                                  0x1e000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_STEP0_CORR_SHFT                                        0x19
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_RANGE_CORR_BMSK                                   0x1000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_RANGE_CORR_SHFT                                        0x18
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_AVEG_CORR_BMSK                                     0x800000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_AVEG_CORR_SHFT                                         0x17
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_RPOLY_CAL_BMSK                                     0x7f8000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_TXDAC0_RPOLY_CAL_SHFT                                          0xf
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_VREF_B2_BMSK                                                0x6000
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_VREF_B2_SHFT                                                   0xd
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PH_B2M3_BMSK                                                0x1c00
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PH_B2M3_SHFT                                                   0xa
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PH_B2M2_BMSK                                                 0x380
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PH_B2M2_SHFT                                                   0x7
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PH_B2M1_BMSK                                                  0x70
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PH_B2M1_SHFT                                                   0x4
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PH_B2M0_BMSK                                                   0xe
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_PH_B2M0_SHFT                                                   0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_G_B2_2_BMSK                                                    0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_LSB_G_B2_2_SHFT                                                    0x0

#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR                                                    (SECURITY_CONTROL_CORE_REG_BASE      + 0x000000ec)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK                                                    0xffffffff
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, HWIO_QFPROM_RAW_CALIB_ROW3_MSB_RMSK)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR, m)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUT(v)      \
        out_dword(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,v)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_OUTM(m,v) \
        out_dword_masked_ns(HWIO_QFPROM_RAW_CALIB_ROW3_MSB_ADDR,m,v,HWIO_QFPROM_RAW_CALIB_ROW3_MSB_IN)
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_SVS_TARGET_BIT_4_2_BMSK                            0xe0000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_SVS_TARGET_BIT_4_2_SHFT                                  0x1d
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR0_SVS_TARGET_BIT_4_0_BMSK                            0x1f000000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR0_SVS_TARGET_BIT_4_0_SHFT                                  0x18
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR_REV_1_0_BMSK                                          0xc00000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR_REV_1_0_SHFT                                              0x16
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_NOMINAL_TARGET_BIT_4_3_BMSK                          0x300000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_NOMINAL_TARGET_BIT_4_3_SHFT                              0x14
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_MODEM_TXDAC_0_1_FUSEFLAG_BMSK                              0x80000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_MODEM_TXDAC_0_1_FUSEFLAG_SHFT                                 0x13
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_NOMINAL_TARGET_BIT_2_0_BMSK                           0x70000
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_NOMINAL_TARGET_BIT_2_0_SHFT                              0x10
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_TURBO_TARGET_BIT_4_0_BMSK                              0xf800
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_CPR2_TURBO_TARGET_BIT_4_0_SHFT                                 0xb
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TXDAC1_RANGE_CORR_BMSK                                       0x400
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TXDAC1_RANGE_CORR_SHFT                                         0xa
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TXDAC1_AVEG_CORR_BMSK                                        0x200
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TXDAC1_AVEG_CORR_SHFT                                          0x9
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TXDAC1_RPOLY_CAL_BMSK                                        0x1fe
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TXDAC1_RPOLY_CAL_SHFT                                          0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TXDAC0_STEP1_CORR_3_BMSK                                       0x1
#define HWIO_QFPROM_RAW_CALIB_ROW3_MSB_TXDAC0_STEP1_CORR_3_SHFT                                       0x0

/*----------------------------------------------------------------------------
 * MODULE: TLMM_CSR
 *--------------------------------------------------------------------------*/
#define TLMM_CSR_REG_BASE                                                                   (TLMM_BASE      + 0x00000000)


#define HWIO_TLMM_HW_REVISION_NUMBER_ADDR                                                   (TLMM_CSR_REG_BASE      + 0x00110010)
#define HWIO_TLMM_HW_REVISION_NUMBER_RMSK                                                   0xffffffff
#define HWIO_TLMM_HW_REVISION_NUMBER_IN          \
        in_dword_masked(HWIO_TLMM_HW_REVISION_NUMBER_ADDR, HWIO_TLMM_HW_REVISION_NUMBER_RMSK)
#define HWIO_TLMM_HW_REVISION_NUMBER_INM(m)      \
        in_dword_masked(HWIO_TLMM_HW_REVISION_NUMBER_ADDR, m)
#define HWIO_TLMM_HW_REVISION_NUMBER_VERSION_ID_BMSK                                        0xf0000000
#define HWIO_TLMM_HW_REVISION_NUMBER_VERSION_ID_SHFT                                              0x1c
#define HWIO_TLMM_HW_REVISION_NUMBER_PARTNUM_BMSK                                            0xffff000
#define HWIO_TLMM_HW_REVISION_NUMBER_PARTNUM_SHFT                                                  0xc
#define HWIO_TLMM_HW_REVISION_NUMBER_QUALCOMM_MFG_ID_BMSK                                        0xffe
#define HWIO_TLMM_HW_REVISION_NUMBER_QUALCOMM_MFG_ID_SHFT                                          0x1
#define HWIO_TLMM_HW_REVISION_NUMBER_START_BIT_BMSK                                                0x1
#define HWIO_TLMM_HW_REVISION_NUMBER_START_BIT_SHFT                                                0x0

#endif /* __HAL_RBCPR_QFPROM_H__ */
