/*============================================================================
  FILE:         TsensBootBsp.c

  OVERVIEW:     9x45 BSP for Tsens boot.

  DEPENDENCIES: None

                Copyright (c) 2014 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/hwengines/tsens/config/9x45/TsensBootBsp.c#1 $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2014-07-09  jjo  Initial version.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "TsensBootBsp.h"
#include "TsensiConversion.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof(a[0]))

#define TSENS_PERIOD_ACTIVE               0x60   // Sleep Interval = 1 x 250 + 32 x 1.95 = 312.4 ms
#define TSENS_SENSOR_CONV_TIME_US          150
#define TSENS_NUM_GET_TEMP_RETRIES           5
#define TSENS_GLOBAL_CONFIG         0x0002EA6B

#define TSENS_Y1                            30
#define TSENS_Y2                           120

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static const TsensBootSensorType aSensors[] =
{
   /* Sensor 0 */
   {
      /* .uTsensConfig   */ 0x1C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 531,
      /* .nM_default     */ (int32)(TSENS_FACTOR * (1 / 3.3965)),
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 1 */
   {
      /* .uTsensConfig   */ 0x1C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 531,
      /* .nM_default     */ (int32)(TSENS_FACTOR * (1 / 3.3965)),
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 2 */
   {
      /* .uTsensConfig   */ 0x1C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 531,
      /* .nM_default     */ (int32)(TSENS_FACTOR * (1 / 3.3965)),
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 3 */
   {
      /* .uTsensConfig   */ 0x1C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 531,
      /* .nM_default     */ (int32)(TSENS_FACTOR * (1 / 3.3965)),
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 4 */
   {
      /* .uTsensConfig   */ 0x1C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 531,
      /* .nM_default     */ (int32)(TSENS_FACTOR * (1 / 3.3965)),
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },
};

const TsensBootBspType TsensBootBsp[] =
{
   {
      /* .paSensors           */ aSensors,
      /* .uNumSensors         */ ARRAY_LENGTH(aSensors),
      /* .uPeriod             */ TSENS_PERIOD_ACTIVE,
      /* .uSensorConvTime_us  */ TSENS_SENSOR_CONV_TIME_US,
      /* .uNumGetTempRetries  */ TSENS_NUM_GET_TEMP_RETRIES,
      /* .uGlobalConfig       */ TSENS_GLOBAL_CONFIG,
      /* .nY1                 */ TSENS_Y1,
      /* .nY2                 */ TSENS_Y2
   }
};

