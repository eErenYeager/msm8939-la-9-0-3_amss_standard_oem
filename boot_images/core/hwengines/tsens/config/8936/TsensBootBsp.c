/*============================================================================
  FILE:         TsensBootBsp.c

  OVERVIEW:     8936 BSP for Tsens boot.

  DEPENDENCIES: None

                Copyright (c) 2015 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.


  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2015-03-25  PR   MHM sensor addition on V3.
  2014-07-09  jjo  Remove turbo mode setting.
  2014-06-30  SA   Set sampling rate to 62.5ms (CR 687328).
  2014-06-06  SA   Updated slopes for 1-point calib devices.
  2014-04-23  SA   Initial version.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "TsensBootBsp.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof(a[0]))

#define TSENS_PERIOD                         1   // 62.5 ms
#define TSENS_SENSOR_CONV_TIME_US          150
#define TSENS_NUM_GET_TEMP_RETRIES           5
#define TSENS_GLOBAL_CONFIG         0x0302F16C

#define TSENS_Y1                            30
#define TSENS_Y2                           120

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/
static const TsensBootSensorType aSensors[] =
{
   /* Sensor 0 */
   {
      /* .uTsensConfig   */ 0x1C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11257,  /* TSENS_FACTOR * (1 / 2.911) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 1 */
   {
      /* .uTsensConfig   */ 0x11C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11749,  /* TSENS_FACTOR * (1 / 2.789) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 2 */
   {
      /* .uTsensConfig   */ 0x11C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11276,  /* TSENS_FACTOR * (1 / 2.906) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 3 */
   {
      /* .uTsensConfig   */ 0x11C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11860,  /* TSENS_FACTOR * (1 / 2.763) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 4 */
   {
      /* .uTsensConfig   */ 0x11C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11214,  /* TSENS_FACTOR * (1 / 2.922) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 5 */
   {
      /* .uTsensConfig   */ 0x11C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11429,  /* TSENS_FACTOR * (1 / 2.867) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 6 */
   {
      /* .uTsensConfig   */ 0x11C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11566,  /* TSENS_FACTOR * (1 / 2.833) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 7 */
   {
      /* .uTsensConfig   */ 0x11C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11546,  /* TSENS_FACTOR * (1 / 2.838) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },

   /* Sensor 8 */
   {
      /* .uTsensConfig   */ 0x11C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11538,  /* TSENS_FACTOR * (1 / 2.840) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },
   /* Sensor 9 */
   {
      /* .uTsensConfig   */ 0x11C3,
      /* .eCal           */ TSENS_BSP_SENSOR_CAL_NORMAL,
      /* .nX1_default    */ 500,
      /* .nM_default     */ 11538,  /* TSENS_FACTOR * (1 / 2.840) */
      /* .nCriticalMin   */ -35,
      /* .nCriticalMax   */ 120
   },
};

TsensBootBspType TsensBootBsp[] =
{
   {
      /* .paSensors           */ aSensors,
      /* .uNumSensors         */ ARRAY_LENGTH(aSensors),
      /* .uPeriod             */ TSENS_PERIOD,
      /* .uSensorConvTime_us  */ TSENS_SENSOR_CONV_TIME_US,
      /* .uNumGetTempRetries  */ TSENS_NUM_GET_TEMP_RETRIES,
      /* .uGlobalConfig       */ TSENS_GLOBAL_CONFIG,
      /* .nY1                 */ TSENS_Y1,
      /* .nY2                 */ TSENS_Y2
   }
};

