#ifndef __HAL_HWIO_TSENS_H__
#define __HAL_HWIO_TSENS_H__
/*============================================================================
  @file HALhwioTsens.h

  Implementation of the TSENS HAL for 9x45 - HWIO info was auto-generated

                Copyright (c) 2014 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Confidential and Proprietary.
============================================================================*/
/* $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/hwengines/tsens/hal/9x45/HALhwioTsens.h#1 $ */

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/
#include "msmhwiobase.h"

/*-------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * ----------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * MODULE: MPM2_TSENS
 *--------------------------------------------------------------------------*/
#define MPM2_TSENS_REG_BASE                                                                  (MPM2_MPM_BASE      + 0x00008000)

#define HWIO_MPM2_TSENS_HW_VER_ADDR                                                          (MPM2_TSENS_REG_BASE      + 0x00000000)
#define HWIO_MPM2_TSENS_HW_VER_RMSK                                                          0xffffffff
#define HWIO_MPM2_TSENS_HW_VER_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_HW_VER_ADDR, HWIO_MPM2_TSENS_HW_VER_RMSK)
#define HWIO_MPM2_TSENS_HW_VER_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_HW_VER_ADDR, m)
#define HWIO_MPM2_TSENS_HW_VER_MAJOR_BMSK                                                    0xf0000000
#define HWIO_MPM2_TSENS_HW_VER_MAJOR_SHFT                                                          0x1c
#define HWIO_MPM2_TSENS_HW_VER_MINOR_BMSK                                                     0xfff0000
#define HWIO_MPM2_TSENS_HW_VER_MINOR_SHFT                                                          0x10
#define HWIO_MPM2_TSENS_HW_VER_STEP_BMSK                                                         0xffff
#define HWIO_MPM2_TSENS_HW_VER_STEP_SHFT                                                            0x0

#define HWIO_MPM2_TSENS_CTRL_ADDR                                                            (MPM2_TSENS_REG_BASE      + 0x00000004)
#define HWIO_MPM2_TSENS_CTRL_RMSK                                                            0x1fffffff
#define HWIO_MPM2_TSENS_CTRL_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_CTRL_ADDR, HWIO_MPM2_TSENS_CTRL_RMSK)
#define HWIO_MPM2_TSENS_CTRL_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_CTRL_ADDR, m)
#define HWIO_MPM2_TSENS_CTRL_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_CTRL_ADDR,v)
#define HWIO_MPM2_TSENS_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_CTRL_ADDR,m,v,HWIO_MPM2_TSENS_CTRL_IN)
#define HWIO_MPM2_TSENS_CTRL_TSENS_CLAMP_BMSK                                                0x10000000
#define HWIO_MPM2_TSENS_CTRL_TSENS_CLAMP_SHFT                                                      0x1c
#define HWIO_MPM2_TSENS_CTRL_TSENS_CLAMP_UNCLAMPED_FVAL                                             0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_CLAMP_CLAMPED_FVAL                                               0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_BYPASS_EN_BMSK                                             0x8000000
#define HWIO_MPM2_TSENS_CTRL_TSENS_BYPASS_EN_SHFT                                                  0x1b
#define HWIO_MPM2_TSENS_CTRL_TSENS_BYPASS_EN_DISABLED_FVAL                                          0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_BYPASS_EN_ENABLED_FVAL                                           0x1
#define HWIO_MPM2_TSENS_CTRL_MEASURE_PERIOD_BMSK                                              0x7f80000
#define HWIO_MPM2_TSENS_CTRL_MEASURE_PERIOD_SHFT                                                   0x13
#define HWIO_MPM2_TSENS_CTRL_SENSOR15_EN_BMSK                                                   0x40000
#define HWIO_MPM2_TSENS_CTRL_SENSOR15_EN_SHFT                                                      0x12
#define HWIO_MPM2_TSENS_CTRL_SENSOR15_EN_DISABLED_FVAL                                              0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR15_EN_ENABLED_FVAL                                               0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR14_EN_BMSK                                                   0x20000
#define HWIO_MPM2_TSENS_CTRL_SENSOR14_EN_SHFT                                                      0x11
#define HWIO_MPM2_TSENS_CTRL_SENSOR14_EN_DISABLED_FVAL                                              0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR14_EN_ENABLED_FVAL                                               0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR13_EN_BMSK                                                   0x10000
#define HWIO_MPM2_TSENS_CTRL_SENSOR13_EN_SHFT                                                      0x10
#define HWIO_MPM2_TSENS_CTRL_SENSOR13_EN_DISABLED_FVAL                                              0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR13_EN_ENABLED_FVAL                                               0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR12_EN_BMSK                                                    0x8000
#define HWIO_MPM2_TSENS_CTRL_SENSOR12_EN_SHFT                                                       0xf
#define HWIO_MPM2_TSENS_CTRL_SENSOR12_EN_DISABLED_FVAL                                              0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR12_EN_ENABLED_FVAL                                               0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR11_EN_BMSK                                                    0x4000
#define HWIO_MPM2_TSENS_CTRL_SENSOR11_EN_SHFT                                                       0xe
#define HWIO_MPM2_TSENS_CTRL_SENSOR11_EN_DISABLED_FVAL                                              0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR11_EN_ENABLED_FVAL                                               0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR10_EN_BMSK                                                    0x2000
#define HWIO_MPM2_TSENS_CTRL_SENSOR10_EN_SHFT                                                       0xd
#define HWIO_MPM2_TSENS_CTRL_SENSOR10_EN_DISABLED_FVAL                                              0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR10_EN_ENABLED_FVAL                                               0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR9_EN_BMSK                                                     0x1000
#define HWIO_MPM2_TSENS_CTRL_SENSOR9_EN_SHFT                                                        0xc
#define HWIO_MPM2_TSENS_CTRL_SENSOR9_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR9_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR8_EN_BMSK                                                      0x800
#define HWIO_MPM2_TSENS_CTRL_SENSOR8_EN_SHFT                                                        0xb
#define HWIO_MPM2_TSENS_CTRL_SENSOR8_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR8_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR7_EN_BMSK                                                      0x400
#define HWIO_MPM2_TSENS_CTRL_SENSOR7_EN_SHFT                                                        0xa
#define HWIO_MPM2_TSENS_CTRL_SENSOR7_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR7_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR6_EN_BMSK                                                      0x200
#define HWIO_MPM2_TSENS_CTRL_SENSOR6_EN_SHFT                                                        0x9
#define HWIO_MPM2_TSENS_CTRL_SENSOR6_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR6_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR5_EN_BMSK                                                      0x100
#define HWIO_MPM2_TSENS_CTRL_SENSOR5_EN_SHFT                                                        0x8
#define HWIO_MPM2_TSENS_CTRL_SENSOR5_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR5_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR4_EN_BMSK                                                       0x80
#define HWIO_MPM2_TSENS_CTRL_SENSOR4_EN_SHFT                                                        0x7
#define HWIO_MPM2_TSENS_CTRL_SENSOR4_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR4_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR3_EN_BMSK                                                       0x40
#define HWIO_MPM2_TSENS_CTRL_SENSOR3_EN_SHFT                                                        0x6
#define HWIO_MPM2_TSENS_CTRL_SENSOR3_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR3_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR2_EN_BMSK                                                       0x20
#define HWIO_MPM2_TSENS_CTRL_SENSOR2_EN_SHFT                                                        0x5
#define HWIO_MPM2_TSENS_CTRL_SENSOR2_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR2_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR1_EN_BMSK                                                       0x10
#define HWIO_MPM2_TSENS_CTRL_SENSOR1_EN_SHFT                                                        0x4
#define HWIO_MPM2_TSENS_CTRL_SENSOR1_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR1_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_SENSOR0_EN_BMSK                                                        0x8
#define HWIO_MPM2_TSENS_CTRL_SENSOR0_EN_SHFT                                                        0x3
#define HWIO_MPM2_TSENS_CTRL_SENSOR0_EN_DISABLED_FVAL                                               0x0
#define HWIO_MPM2_TSENS_CTRL_SENSOR0_EN_ENABLED_FVAL                                                0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_ADC_CLK_SEL_BMSK                                                 0x4
#define HWIO_MPM2_TSENS_CTRL_TSENS_ADC_CLK_SEL_SHFT                                                 0x2
#define HWIO_MPM2_TSENS_CTRL_TSENS_ADC_CLK_SEL_INTERNAL_OSCILLATOR_FVAL                             0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_ADC_CLK_SEL_EXTERNAL_CLOCK_SOURCE_FVAL                           0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_SW_RST_BMSK                                                      0x2
#define HWIO_MPM2_TSENS_CTRL_TSENS_SW_RST_SHFT                                                      0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_SW_RST_RESET_DEASSERTED_FVAL                                     0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_SW_RST_RESET_ASSERTED_FVAL                                       0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_EN_BMSK                                                          0x1
#define HWIO_MPM2_TSENS_CTRL_TSENS_EN_SHFT                                                          0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_EN_DISABLED_FVAL                                                 0x0
#define HWIO_MPM2_TSENS_CTRL_TSENS_EN_ENABLED_FVAL                                                  0x1

#define HWIO_MPM2_TSENS_TEST_CTRL_ADDR                                                       (MPM2_TSENS_REG_BASE      + 0x00000008)
#define HWIO_MPM2_TSENS_TEST_CTRL_RMSK                                                             0x1f
#define HWIO_MPM2_TSENS_TEST_CTRL_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_TEST_CTRL_ADDR, HWIO_MPM2_TSENS_TEST_CTRL_RMSK)
#define HWIO_MPM2_TSENS_TEST_CTRL_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_TEST_CTRL_ADDR, m)
#define HWIO_MPM2_TSENS_TEST_CTRL_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_TEST_CTRL_ADDR,v)
#define HWIO_MPM2_TSENS_TEST_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_TEST_CTRL_ADDR,m,v,HWIO_MPM2_TSENS_TEST_CTRL_IN)
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_BMSK                                              0x1e
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SHFT                                               0x1
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_0_FVAL                                      0x0
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_1_FVAL                                      0x1
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_2_FVAL                                      0x2
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_3_FVAL                                      0x3
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_4_FVAL                                      0x4
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_5_FVAL                                      0x5
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_6_FVAL                                      0x6
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_7_FVAL                                      0x7
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_8_FVAL                                      0x8
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_9_FVAL                                      0x9
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_10_FVAL                                     0xa
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_11_FVAL                                     0xb
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_12_FVAL                                     0xc
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_13_FVAL                                     0xd
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_14_FVAL                                     0xe
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_SEL_SENSOR_15_FVAL                                     0xf
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_EN_BMSK                                                0x1
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_EN_SHFT                                                0x0
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_EN_TEST_DISABLED_FVAL                                  0x0
#define HWIO_MPM2_TSENS_TEST_CTRL_TSENS_TEST_EN_TEST_ENABLED_FVAL                                   0x1

#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_ADDR(n)                                       (MPM2_TSENS_REG_BASE      + 0x0000000c + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_RMSK                                            0x3fffff
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MAXn                                                  15
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_ADDR(n), HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_RMSK)
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_ADDR(n), mask)
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_ADDR(n),val)
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_ADDR(n),mask,val,HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_INI(n))
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MAX_STATUS_MASK_BMSK                            0x200000
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MAX_STATUS_MASK_SHFT                                0x15
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MAX_STATUS_MASK_NORMAL_OPERATION_FVAL                0x0
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MAX_STATUS_MASK_MASK_OFF_MAX_STATUS_FVAL             0x1
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MIN_STATUS_MASK_BMSK                            0x100000
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MIN_STATUS_MASK_SHFT                                0x14
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MIN_STATUS_MASK_NORMAL_OPERATION_FVAL                0x0
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MIN_STATUS_MASK_MASK_OFF_MIN_STATUS_FVAL             0x1
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MAX_THRESHOLD_BMSK                               0xffc00
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MAX_THRESHOLD_SHFT                                   0xa
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MIN_THRESHOLD_BMSK                                 0x3ff
#define HWIO_MPM2_TSENS_Sn_MIN_MAX_STATUS_CTRL_MIN_THRESHOLD_SHFT                                   0x0

#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_ADDR                                                   (MPM2_TSENS_REG_BASE      + 0x0000004c)
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_RMSK                                                   0x1fffffff
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_GLOBAL_CONFIG_ADDR, HWIO_MPM2_TSENS_GLOBAL_CONFIG_RMSK)
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_GLOBAL_CONFIG_ADDR, m)
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_GLOBAL_CONFIG_ADDR,v)
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_GLOBAL_CONFIG_ADDR,m,v,HWIO_MPM2_TSENS_GLOBAL_CONFIG_IN)
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_DISABLE_PTAT_OPAMP_CHOPPING_BMSK          0x10000000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_DISABLE_PTAT_OPAMP_CHOPPING_SHFT                0x1c
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_VBE_BMSK                                   0x8000000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_VBE_SHFT                                        0x1b
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_VBE_VBE_MISSION_HIGH_FVAL                        0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_VBE_VBE_TEST_LOW_FVAL                            0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_CHOPPING_FREQ_BMSK                         0x6000000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_CHOPPING_FREQ_SHFT                              0x19
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_DEM_FREQ_BMSK                              0x1800000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_REMOTE_DEM_FREQ_SHFT                                   0x17
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_TEST_MODE_VBE_SEL_BMSK                        0x400000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_TEST_MODE_VBE_SEL_SHFT                            0x16
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_EXTRA_TEST_CTRL_BMSK                          0x380000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_EXTRA_TEST_CTRL_SHFT                              0x13
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_VDD_R_EN_BMSK                                  0x40000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_VDD_R_EN_SHFT                                     0x12
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_VDD_R_EN_DISABLED_FVAL                             0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_VDD_R_EN_ENABLED_FVAL                              0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_TURBO_MODE_EN_BMSK                             0x20000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_TURBO_MODE_EN_SHFT                                0x11
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_TURBO_MODE_EN_DISABLED_FVAL                        0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_TURBO_MODE_EN_ENABLED_FVAL                         0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_QUANTIZER_OUT_EN_BMSK                          0x10000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_QUANTIZER_OUT_EN_SHFT                             0x10
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_QUANTIZER_OUT_EN_DISABLED_FVAL                     0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_QUANTIZER_OUT_EN_ENABLED_FVAL                      0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM3_EN_BMSK                                0x8000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM3_EN_SHFT                                   0xf
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM3_EN_DISABLED_FVAL                          0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM3_EN_ENABLED_FVAL                           0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM2_EN_BMSK                                0x4000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM2_EN_SHFT                                   0xe
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM2_EN_DISABLED_FVAL                          0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM2_EN_ENABLED_FVAL                           0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM1_EN_BMSK                                0x2000
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM1_EN_SHFT                                   0xd
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM1_EN_DISABLED_FVAL                          0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CLK_DEM1_EN_ENABLED_FVAL                           0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_DEM_SEL_BMSK                                    0x1800
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_DEM_SEL_SHFT                                       0xb
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_GAIN_CAL_EN_BMSK                                 0x400
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_GAIN_CAL_EN_SHFT                                   0xa
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CHOP_SEL_BMSK                                    0x300
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CHOP_SEL_SHFT                                      0x8
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_OSC_CLK_DEBUG_EN_BMSK                             0x80
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_OSC_CLK_DEBUG_EN_SHFT                              0x7
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_OSC_CLK_DEBUG_EN_DISABLED_FVAL                     0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_OSC_CLK_DEBUG_EN_ENABLED_FVAL                      0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CTAT_CHOPPING_EN_BMSK                             0x40
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CTAT_CHOPPING_EN_SHFT                              0x6
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CTAT_CHOPPING_EN_DISABLED_FVAL                     0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_CTAT_CHOPPING_EN_ENABLED_FVAL                      0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_PTAT_CHOPPING_EN_BMSK                             0x20
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_PTAT_CHOPPING_EN_SHFT                              0x5
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_PTAT_CHOPPING_EN_DISABLED_FVAL                     0x0
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CTRL_PTAT_CHOPPING_EN_ENABLED_FVAL                      0x1
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CFG_OSC_FREQ_BMSK                                      0x18
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CFG_OSC_FREQ_SHFT                                       0x3
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CFG_IIN_SLOPE_BMSK                                      0x7
#define HWIO_MPM2_TSENS_GLOBAL_CONFIG_TSENS_CFG_IIN_SLOPE_SHFT                                      0x0

#define HWIO_MPM2_TSENS_SENSOR_CONFIG_ADDR                                                   (MPM2_TSENS_REG_BASE      + 0x00000050)
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_RMSK                                                        0x3ff
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_SENSOR_CONFIG_ADDR, HWIO_MPM2_TSENS_SENSOR_CONFIG_RMSK)
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_SENSOR_CONFIG_ADDR, m)
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_SENSOR_CONFIG_ADDR,v)
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_SENSOR_CONFIG_ADDR,m,v,HWIO_MPM2_TSENS_SENSOR_CONFIG_IN)
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_TSENS_CFG_IREF_GAIN_BMSK                                    0x380
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_TSENS_CFG_IREF_GAIN_SHFT                                      0x7
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_TSENS_CTRL_CTAT_WEIGHT_BMSK                                  0x78
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_TSENS_CTRL_CTAT_WEIGHT_SHFT                                   0x3
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_TSENS_CTRL_IREF_SUBTRACTION_WEIGHT_BMSK                       0x7
#define HWIO_MPM2_TSENS_SENSOR_CONFIG_TSENS_CTRL_IREF_SUBTRACTION_WEIGHT_SHFT                       0x0

#define HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_ADDR                                                (MPM2_TSENS_REG_BASE      + 0x00000054)
#define HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_RMSK                                                     0x1ff
#define HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_ADDR, HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_RMSK)
#define HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_ADDR, m)
#define HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_ADDR,v)
#define HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_ADDR,m,v,HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_IN)
#define HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_TSENSE_CONFIG_REMOTE_IN_BMSK                             0x1ff
#define HWIO_MPM2_TSENS_CONFIG_REMOTE_IN_TSENSE_CONFIG_REMOTE_IN_SHFT                               0x0

/*----------------------------------------------------------------------------
 * MODULE: MPM2_TSENS_TM
 *--------------------------------------------------------------------------*/
#define MPM2_TSENS_TM_REG_BASE                                                    (MPM2_MPM_BASE      + 0x00009000)

#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_ADDR                           (MPM2_TSENS_TM_REG_BASE      + 0x00000000)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_RMSK                                  0x1
#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_ADDR, HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_RMSK)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_ADDR, m)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_OUT(v)      \
        out_dword(HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_ADDR,v)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_OUTM(m,v) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_ADDR,m,v,HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_IN)
#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_INTERRUPT_EN_BMSK                     0x1
#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_INTERRUPT_EN_SHFT                     0x0
#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_INTERRUPT_EN_DISABLED_FVAL            0x0
#define HWIO_MPM2_TSENS_UPPER_LOWER_INTERRUPT_CTRL_INTERRUPT_EN_ENABLED_FVAL             0x1

#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_ADDR(n)                        (MPM2_TSENS_TM_REG_BASE      + 0x00000004 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_RMSK                             0x3fffff
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_MAXn                                   15
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_ADDR(n), HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_RMSK)
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_ADDR(n), mask)
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_OUTI(n,val)    \
        out_dword(HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_ADDR(n),val)
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_OUTMI(n,mask,val) \
        out_dword_masked_ns(HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_ADDR(n),mask,val,HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_INI(n))
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_UPPER_STATUS_CLR_BMSK            0x200000
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_UPPER_STATUS_CLR_SHFT                0x15
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_UPPER_STATUS_CLR_NORMAL_OPERATION_FVAL        0x0
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_UPPER_STATUS_CLR_CLEAR_UPPER_STATUS_FVAL        0x1
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_LOWER_STATUS_CLR_BMSK            0x100000
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_LOWER_STATUS_CLR_SHFT                0x14
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_LOWER_STATUS_CLR_NORMAL_OPERATION_FVAL        0x0
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_LOWER_STATUS_CLR_CLEAR_LOWER_STATUS_FVAL        0x1
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_UPPER_THRESHOLD_BMSK              0xffc00
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_UPPER_THRESHOLD_SHFT                  0xa
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_LOWER_THRESHOLD_BMSK                0x3ff
#define HWIO_MPM2_TSENS_Sn_UPPER_LOWER_STATUS_CTRL_LOWER_THRESHOLD_SHFT                  0x0

#define HWIO_MPM2_TSENS_Sn_STATUS_ADDR(n)                                         (MPM2_TSENS_TM_REG_BASE      + 0x00000044 + 0x4 * (n))
#define HWIO_MPM2_TSENS_Sn_STATUS_RMSK                                                0x7fff
#define HWIO_MPM2_TSENS_Sn_STATUS_MAXn                                                    15
#define HWIO_MPM2_TSENS_Sn_STATUS_INI(n)        \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_STATUS_ADDR(n), HWIO_MPM2_TSENS_Sn_STATUS_RMSK)
#define HWIO_MPM2_TSENS_Sn_STATUS_INMI(n,mask)    \
        in_dword_masked(HWIO_MPM2_TSENS_Sn_STATUS_ADDR(n), mask)
#define HWIO_MPM2_TSENS_Sn_STATUS_VALID_BMSK                                          0x4000
#define HWIO_MPM2_TSENS_Sn_STATUS_VALID_SHFT                                             0xe
#define HWIO_MPM2_TSENS_Sn_STATUS_MAX_STATUS_BMSK                                     0x2000
#define HWIO_MPM2_TSENS_Sn_STATUS_MAX_STATUS_SHFT                                        0xd
#define HWIO_MPM2_TSENS_Sn_STATUS_MAX_STATUS_MAX_THRESHOLD_NOT_VIOLATED_FVAL             0x0
#define HWIO_MPM2_TSENS_Sn_STATUS_MAX_STATUS_MAX_THRESHOLD_VIOLATED_FVAL                 0x1
#define HWIO_MPM2_TSENS_Sn_STATUS_UPPER_STATUS_BMSK                                   0x1000
#define HWIO_MPM2_TSENS_Sn_STATUS_UPPER_STATUS_SHFT                                      0xc
#define HWIO_MPM2_TSENS_Sn_STATUS_UPPER_STATUS_UPPER_THRESHOLD_NOT_VIOLATED_FVAL         0x0
#define HWIO_MPM2_TSENS_Sn_STATUS_UPPER_STATUS_UPPER_THRESHOLD_VIOLATED_FVAL             0x1
#define HWIO_MPM2_TSENS_Sn_STATUS_LOWER_STATUS_BMSK                                    0x800
#define HWIO_MPM2_TSENS_Sn_STATUS_LOWER_STATUS_SHFT                                      0xb
#define HWIO_MPM2_TSENS_Sn_STATUS_LOWER_STATUS_LOWER_THRESHOLD_NOT_VIOLATED_FVAL         0x0
#define HWIO_MPM2_TSENS_Sn_STATUS_LOWER_STATUS_LOWER_THRESHOLD_VIOLATED_FVAL             0x1
#define HWIO_MPM2_TSENS_Sn_STATUS_MIN_STATUS_BMSK                                      0x400
#define HWIO_MPM2_TSENS_Sn_STATUS_MIN_STATUS_SHFT                                        0xa
#define HWIO_MPM2_TSENS_Sn_STATUS_MIN_STATUS_MIN_THRESHOLD_NOT_VIOLATED_FVAL             0x0
#define HWIO_MPM2_TSENS_Sn_STATUS_MIN_STATUS_MIN_THRESHOLD_VIOLATED_FVAL                 0x1
#define HWIO_MPM2_TSENS_Sn_STATUS_LAST_TEMP_BMSK                                       0x3ff
#define HWIO_MPM2_TSENS_Sn_STATUS_LAST_TEMP_SHFT                                         0x0

#define HWIO_MPM2_TSENS_TRDY_ADDR                                                 (MPM2_TSENS_TM_REG_BASE      + 0x00000084)
#define HWIO_MPM2_TSENS_TRDY_RMSK                                                        0x7
#define HWIO_MPM2_TSENS_TRDY_IN          \
        in_dword_masked(HWIO_MPM2_TSENS_TRDY_ADDR, HWIO_MPM2_TSENS_TRDY_RMSK)
#define HWIO_MPM2_TSENS_TRDY_INM(m)      \
        in_dword_masked(HWIO_MPM2_TSENS_TRDY_ADDR, m)
#define HWIO_MPM2_TSENS_TRDY_OSC_CLK_OFF_BMSK                                            0x4
#define HWIO_MPM2_TSENS_TRDY_OSC_CLK_OFF_SHFT                                            0x2
#define HWIO_MPM2_TSENS_TRDY_OSC_CLK_OFF_CLK_IS_ON_FVAL                                  0x0
#define HWIO_MPM2_TSENS_TRDY_OSC_CLK_OFF_CLK_IS_OFF_FVAL                                 0x1
#define HWIO_MPM2_TSENS_TRDY_SLP_CLK_OFF_BMSK                                            0x2
#define HWIO_MPM2_TSENS_TRDY_SLP_CLK_OFF_SHFT                                            0x1
#define HWIO_MPM2_TSENS_TRDY_SLP_CLK_OFF_CLK_IS_ON_FVAL                                  0x0
#define HWIO_MPM2_TSENS_TRDY_SLP_CLK_OFF_CLK_IS_OFF_FVAL                                 0x1
#define HWIO_MPM2_TSENS_TRDY_TRDY_BMSK                                                   0x1
#define HWIO_MPM2_TSENS_TRDY_TRDY_SHFT                                                   0x0
#define HWIO_MPM2_TSENS_TRDY_TRDY_TEMPERATURE_MEASUREMENT_IN_PROGRESS_FVAL               0x0
#define HWIO_MPM2_TSENS_TRDY_TRDY_TEMPERATURE_READING_IS_READY_FVAL                      0x1

/*----------------------------------------------------------------------------
 * MODULE: SECURITY_CONTROL_CORE
 *--------------------------------------------------------------------------*/
#define SECURITY_CONTROL_CORE_REG_BASE                                                                             (SECURITY_CONTROL_BASE      + 0x00000000)

#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040d0)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK                                                  0xffffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_LSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_LSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS2_POINT1_BMSK                                    0xf0000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS2_POINT1_SHFT                                          0x1c
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS1_POINT1_BMSK                                     0xf000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS1_POINT1_SHFT                                          0x18
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS0_POINT1_BMSK                                      0xf00000
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS0_POINT1_SHFT                                          0x14
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE1_BMSK                                         0xffc00
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE1_SHFT                                             0xa
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE0_BMSK                                           0x3ff
#define HWIO_QFPROM_CORR_CALIB_ROW0_LSB_TSENS_BASE0_SHFT                                             0x0

#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR                                                  (SECURITY_CONTROL_CORE_REG_BASE      + 0x000040d4)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK                                                  0x7fffffff
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_IN          \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, HWIO_QFPROM_CORR_CALIB_ROW0_MSB_RMSK)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_INM(m)      \
        in_dword_masked(HWIO_QFPROM_CORR_CALIB_ROW0_MSB_ADDR, m)
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_FEC_BMSK                                              0x7f000000
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_FEC_SHFT                                                    0x18
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE_BMSK                                              0xfff800
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_SPARE_SHFT                                                   0xb
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_CAL_SEL_BMSK                                         0x700
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS_CAL_SEL_SHFT                                           0x8
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS4_POINT1_BMSK                                          0xf0
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS4_POINT1_SHFT                                           0x4
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS3_POINT1_BMSK                                           0xf
#define HWIO_QFPROM_CORR_CALIB_ROW0_MSB_TSENS3_POINT1_SHFT                                           0x0

/*-------------------------------------------------------------------------
 * Include Files
 * ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Type Declarations
 * ----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------
 * Function Declarations and Documentation
 * ----------------------------------------------------------------------*/

#endif

