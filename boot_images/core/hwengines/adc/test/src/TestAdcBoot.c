/*============================================================================
  FILE:         TestAdcBoot.c

  OVERVIEW:     Test file for boot ADC.

  Usage: call AdcTestInit in sbl1. Infinite loops are inserted at the
  start and end of the test: change gAdcTestResumeBoot = 1 to resume boot
  execution or change PC.

  DEPENDENCIES: None

                Copyright (c) 2013 Qualcomm Technologies, Inc.
                All Rights Reserved.
                Qualcomm Technologies Proprietary and Confidential.
============================================================================*/
/*============================================================================
  EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.  Please
  use ISO format for dates.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/hwengines/adc/test/src/TestAdcBoot.c#1 $

  when        who  what, where, why
  ----------  ---  -----------------------------------------------------------
  2013-04-18  jjo  Added XO_THERM_MV.
  2012-12-03  jjo  Initial version.

============================================================================*/
/*----------------------------------------------------------------------------
 * Include Files
 * -------------------------------------------------------------------------*/
#include "AdcBoot.h"
#include "AdcInputs.h"
#include "DalDevice.h"
#include "DALStdDef.h"
#include "DALSys.h"

/*----------------------------------------------------------------------------
 * Preprocessor Definitions and Constants
 * -------------------------------------------------------------------------*/
#define ARRAY_LENGTH(a) (sizeof(a) / sizeof(a[0]))

/*----------------------------------------------------------------------------
 * Type Declarations
 * -------------------------------------------------------------------------*/
typedef enum
{
   ADC_ERROR_SUCCESS = 0,
   ADC_ERROR_INIT_FAILED,
   ADC_ERROR_GET_CHANNEL_FAILED,
   ADC_ERROR_READ_FAILED,
   ADC_ERROR_RECALIBRATE_FAILED
} AdcTestErrorType;

typedef struct
{
   const char *pszChannelName;
   AdcBootDeviceChannelType channel;
   AdcBootResultType reading;
} AdcTestReadType;

typedef struct
{
   const char *pszChannelName;
   AdcBootDeviceChannelType channel;
   AdcBootRecalibrateChannelResultType recal;
} AdcTestRecalType;

 /*----------------------------------------------------------------------------
 * Static Function Declarations
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Global Data Definitions
 * -------------------------------------------------------------------------*/
//volatile DALBOOL gAdcTestResumeBoot = FALSE;

AdcTestErrorType gAdcTestBootError = ADC_ERROR_SUCCESS;

const char *apszReadChannels[] =
{
   /* VADC channels */
   ADC_INPUT_USB_IN,
   ADC_INPUT_DC_IN,
   ADC_INPUT_VCHG,
   ADC_INPUT_VCOIN,
   ADC_INPUT_VBATT,
   ADC_INPUT_VPH_PWR,
   ADC_INPUT_PMIC_THERM,
   ADC_INPUT_CHG_TEMP,
   ADC_INPUT_BATT_THERM,
   ADC_INPUT_BATT_ID,
   ADC_INPUT_PMIC_HARDWARE_ID,
   ADC_INPUT_XO_THERM_MV,
   ADC_INPUT_XO_THERM,
   ADC_INPUT_XO_THERM_GPS,
   ADC_INPUT_SYS_THERM1,
   ADC_INPUT_SYS_THERM2,
   ADC_INPUT_PA_THERM,
   ADC_INPUT_PA_THERM1,
   ADC_INPUT_SYS_THERM3,
   ADC_INPUT_SYS_THERM4,
   ADC_INPUT_SYS_THERM5,
   ADC_INPUT_USB_DATA,
   ADC_INPUT_USB_ID,
   ADC_INPUT_PLATFORM_ID,
   /* IADC channels */
   ADC_INPUT_IBATT_INTERNAL,
   ADC_INPUT_IBATT_EXTERNAL,
   ADC_INPUT_RSENSE_INTERNAL,
   ADC_INPUT_RSENSE_EXTERNAL
};

AdcTestReadType adcTestRead[ARRAY_LENGTH(apszReadChannels)];

const char *apszRecalChannels[] =
{
   /* IADC channels */
   ADC_INPUT_IBATT_INTERNAL,
   ADC_INPUT_IBATT_EXTERNAL
};

AdcTestRecalType adcTestRecal[ARRAY_LENGTH(apszRecalChannels)];

/*----------------------------------------------------------------------------
 * Static Variable Definitions
 * -------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 * Static Function Declarations and Definitions
 * -------------------------------------------------------------------------*/
void AdcTestInit()
{
   AdcTestRecalType *pAdcTestRecal;
   AdcTestReadType *pAdcTestRead;
   AdcBootRecalibrateChannelResultType recal;
   AdcBootDeviceChannelType channel;
   AdcBootResultType reading;
   DALResult result;
   uint32 uChannel;

//   gAdcTestResumeBoot = FALSE;
//   while (gAdcTestResumeBoot == FALSE);

   result = AdcBoot_Init();
   if (result != DAL_SUCCESS)
   {
     gAdcTestBootError = ADC_ERROR_INIT_FAILED;
   }

   /* Read all channels */
   for (uChannel = 0; uChannel < ARRAY_LENGTH(apszReadChannels); uChannel++)
   {
      pAdcTestRead = &adcTestRead[uChannel];

      pAdcTestRead->pszChannelName = apszReadChannels[uChannel];

      result = AdcBoot_GetChannel(apszReadChannels[uChannel], &channel);
      if (result != DAL_SUCCESS)
      {
         gAdcTestBootError = ADC_ERROR_GET_CHANNEL_FAILED;
         continue;
      }

      pAdcTestRead->channel = channel;

      result = AdcBoot_Read(&pAdcTestRead->channel, &reading);
      if (result != DAL_SUCCESS)
      {
         gAdcTestBootError = ADC_ERROR_READ_FAILED;
         continue;
      }

      pAdcTestRead->reading = reading;
   }

   /* Recalibrate channels */
   for (uChannel = 0; uChannel < ARRAY_LENGTH(apszRecalChannels); uChannel++)
   {
      pAdcTestRecal = &adcTestRecal[uChannel];

      pAdcTestRecal->pszChannelName = apszRecalChannels[uChannel];

      result = AdcBoot_GetChannel(apszRecalChannels[uChannel], &channel);
      if (result != DAL_SUCCESS)
      {
         gAdcTestBootError = ADC_ERROR_GET_CHANNEL_FAILED;
         continue;
      }

      pAdcTestRecal->channel = channel;

      result = AdcBoot_RecalibrateChannel(&pAdcTestRecal->channel, &recal);
      if (result != DAL_SUCCESS)
      {
         gAdcTestBootError = ADC_ERROR_RECALIBRATE_FAILED;
         continue;
      }

      pAdcTestRecal->recal = recal;
   }

//   gAdcTestResumeBoot = FALSE;
//   while (gAdcTestResumeBoot == FALSE);

   return;
}

