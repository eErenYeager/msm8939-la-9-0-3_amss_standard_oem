#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/buses/i2c/config/i2c_boot_8916.xml"





















#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/ClockBoot.h"










 













 



 

#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/services/com_dtypes.h"

















 
















 






 








 
#line 58 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/services/com_dtypes.h"



 





















 



 


typedef  unsigned char      boolean;      




#line 107 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/services/com_dtypes.h"


typedef  unsigned int      uint32;       




typedef  unsigned short     uint16;       




typedef  unsigned char      uint8;        




typedef  signed int         int32;        




typedef  signed short       int16;        




typedef  signed char        int8;         




typedef  unsigned long      uintnt;      

 





 

typedef  unsigned char      byte;          



typedef  unsigned short     word;          
typedef  unsigned int       dword;         

typedef  unsigned char      uint1;         
typedef  unsigned short     uint2;         
typedef  unsigned int       uint4;         

typedef  signed char        int1;          
typedef  signed short       int2;          
typedef  signed int         int4;          

typedef  signed int         sint31;        
typedef  signed short       sint15;        
typedef  signed char        sint7;         

typedef uint16 UWord16 ;
typedef uint32 UWord32 ;
typedef int32  Word32 ;
typedef int16  Word16 ;
typedef uint8  UWord8 ;
typedef int8   Word8 ;
typedef int32  Vect32 ;


   

    typedef long long     int64;        



    typedef  unsigned long long  uint64;       
#line 205 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/services/com_dtypes.h"







#line 32 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/ClockBoot.h"



 



 










 
typedef enum
{
  CLOCK_BOOT_PERF_NONE,
  CLOCK_BOOT_PERF_MIN,
  CLOCK_BOOT_PERF_NOMINAL,
  CLOCK_BOOT_PERF_MAX,
  CLOCK_BOOT_PERF_DEFAULT,
  CLOCK_BOOT_PERF_NUM,
  CLOCK_BOOT_PERF_32BITS = 0x7FFFFFFF
} ClockBootPerfLevelType;



 
typedef enum
{
   CLK_SDC_NONE,
   CLK_SDC1,
   CLK_SDC2,
   CLK_SDC3,
   CLK_SDC4,
   CLK_SDC5,
   CLK_SDC_NUM_CLKS
} ClockSDCType;



 
typedef enum
{
   CLK_BLSP_UART_NONE,
   CLK_BLSP1_UART1_APPS,
   CLK_BLSP1_UART2_APPS,
   CLK_BLSP1_UART3_APPS,
   CLK_BLSP1_UART4_APPS,
   CLK_BLSP1_UART5_APPS,
   CLK_BLSP1_UART6_APPS,
   CLK_BLSP2_UART1_APPS,
   CLK_BLSP2_UART2_APPS,
   CLK_BLSP2_UART3_APPS,
   CLK_BLSP2_UART4_APPS,
   CLK_BLSP2_UART5_APPS,
   CLK_BLSP2_UART6_APPS,
   CLK_BLSP_UART_NUM_CLKS
} ClockUARTType;




 
typedef enum
{
   CLK_BLSP_QUP_I2C_NONE,
   CLK_BLSP1_QUP1_I2C_APPS,
   CLK_BLSP1_QUP2_I2C_APPS,
   CLK_BLSP1_QUP3_I2C_APPS,
   CLK_BLSP1_QUP4_I2C_APPS,
   CLK_BLSP1_QUP5_I2C_APPS,
   CLK_BLSP1_QUP6_I2C_APPS,
   CLK_BLSP_QUP_I2C_NUM_CLKS
} ClockQUPI2CType;

typedef enum
{
   
  CLOCK_RESOURCE_QUERY_INVALID         = 0,

  



 
  CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,

  



 
  CLOCK_RESOURCE_QUERY_MIN_FREQ_KHZ, 

  



 
  CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ,
} ClockQueryType;












 

typedef enum
{
  CLOCK_VREG_LEVEL_OFF          = 0,  
  CLOCK_VREG_LEVEL_LOW_MINUS    = 2,  
  CLOCK_VREG_LEVEL_LOW          = 3,  
  CLOCK_VREG_LEVEL_NOMINAL      = 4,  
  CLOCK_VREG_LEVEL_NOMINAL_PLUS = 5,  
  CLOCK_VREG_LEVEL_HIGH_MINUS   = 6,  
  CLOCK_VREG_LEVEL_HIGH         = 7,  
  CLOCK_VREG_LEVEL_HIGH_NO_CPR  = 8,  
  CLOCK_VREG_NUM_LEVELS,
} ClockVRegLevelType;





 
typedef struct 
{
  uint32             nFreqKHz;
  ClockVRegLevelType eVRegLevel;
}ClockPlanType;



 


 
boolean Clock_PreDDRInit( uint32 ddr_type );




 


 
boolean Clock_PreDDRInitEx( uint32 ddr_type );




 
















 

boolean Clock_Init(ClockBootPerfLevelType eSysPerfLevel,
                    ClockBootPerfLevelType eCPUPerfLevel);




 














 

boolean Clock_SetSysPerfLevel(ClockBootPerfLevelType eSysPerfLevel);




 














 

boolean Clock_SetCPUPerfLevel(ClockBootPerfLevelType eCPUPerfLevel);




 













 
boolean Clock_SetL2PerfLevel(ClockBootPerfLevelType eL2PerfLevel);




 













 
boolean Clock_SetSDCClockFrequency(ClockBootPerfLevelType ePerfLevel,
                                    ClockSDCType eClock);





 













 
uint32 Clock_SetSDCClockFrequencyExt( uint32 nFreqKHz, ClockSDCType eClock);




 













 
boolean Clock_SetUARTClockFrequency(ClockBootPerfLevelType ePerfLevel,
                                    ClockUARTType eClock);





 












 
boolean Clock_DisableUARTClock(ClockUARTType eClock);





 













 
boolean Clock_SetI2CClockFrequency
(
  ClockBootPerfLevelType ePerfLevel,
  ClockQUPI2CType eClock
);





 












 
boolean Clock_DisableI2CClock(ClockQUPI2CType eClock);




 













 
void Clock_InitForDownloadMode(void);




 












 
boolean Clock_InitUSB(void);




 












 

boolean Clock_InitCrypto(void);




 












 
boolean Clock_ExitBoot(void);




 












 
boolean Clock_DebugInit(void);




 












 
boolean Clock_DisableUSB(void);




 












 
uint32 Clock_DDRSpeed(void);



 












 
boolean Clock_I2CInit(void);



 












 
boolean Clock_EnableQPICForDownloadMode( void );



 

void Clock_BIMCQuery(ClockQueryType nQuery,void* pResource);

void Clock_SwitchBusVoltage(ClockVRegLevelType eVoltageLevel);

boolean Clock_SetBIMCSpeed(uint32 nFreqKHz );



 













 
boolean Clock_ExtBuckGPIOMisc( void );




 












 
boolean Clock_Usb30SwitchPipeClk(void);





 











 
void Clock_Usb30DisableSWCollapse(boolean enable);

boolean Is_DDR_Type_LPDDR3( void );


#line 23 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/buses/i2c/config/i2c_boot_8916.xml"

<driver name="I2C">
    <global_def> 
      <var_seq name="i2cqup1_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c023, 0x2001c033,end 
      </var_seq>

      <var_seq name="i2cqup2_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c063, 0x2001c073,end 
      </var_seq>
      
      <var_seq name="i2cqup3_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c0a2, 0x2001c0b2,end 
      </var_seq>    

      <var_seq name="i2cqup4_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c0e2, 0x2001c0f2,end 
      </var_seq>

      <var_seq name="i2cqup5_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c122, 0x2001c132,end 
      </var_seq>
      
      <var_seq name="i2cqup6_gpio_config_arr" type=DALPROP_DATA_TYPE_UINT32_SEQ>
         0x2001c162, 0x2001c172,end 
      </var_seq>  
    </global_def>

    <device id=DALDEVICEID_I2C_DEVICE_1>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP1_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78B5000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup1_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>

    <device id=DALDEVICEID_I2C_DEVICE_2>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP2_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78B6000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup2_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>

    <device id=DALDEVICEID_I2C_DEVICE_3>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP3_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78B7000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup3_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>

    <device id=DALDEVICEID_I2C_DEVICE_4>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP4_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78B8000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup4_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>
    
    <device id=DALDEVICEID_I2C_DEVICE_5>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP5_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78B9000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup5_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>
    
    <device id=DALDEVICEID_I2C_DEVICE_6>
        <props name="CHIP_CLK_INDEX" type=DALPROP_ATTR_TYPE_UINT32>            CLK_BLSP1_QUP6_I2C_APPS    </props> 
        <props name="CHIP_QUP_BASE_ADDR" type=DALPROP_ATTR_TYPE_UINT32>        0x78BA000</props>
        <props name="CHIP_APPS_CLK_FREQ_KHZ" type=DALPROP_ATTR_TYPE_UINT32>    19200</props>
        <props name="CHIP_GPIO_CONFIG_ARR" type=DALPROP_ATTR_TYPE_UINT32_SEQ_PTR> i2cqup6_gpio_config_arr   </props>
        <props name="SW_ENABLE_INTERRUPTS" type=DALPROP_ATTR_TYPE_UINT32>      0    </props>
    </device>
</driver>
 
