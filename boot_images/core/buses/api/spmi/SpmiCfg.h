/**
 * @file:  SpmiCfg.h
 * @brief: This module provides configuration options for the SPMI controller
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/03/19 01:58:37 $
 * $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/buses/api/spmi/SpmiCfg.h#1 $
 * $Change: 7697609 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */
#ifndef SPMICFG_H
#define	SPMICFG_H

#include "SpmiTypes.h"

//******************************************************************************
// Data Structures
//******************************************************************************

typedef struct
{
    uint8 slaveId;  /**< Slave id of the SPMI device */
    uint8 periphId; /**< Peripheral id on the slave device */
    uint16 channel; /**< The SW channel number to associate with the peripheral (0 indexed) */
    uint8 intOwner; /**< The EE that owns this peripheral's interrupt.  This should match the channel's owner */
} SpmiCfg_ChannelCfg;

typedef struct
{
    uint8 slaveId;  /**< Slave id of the SPMI device */
    uint16 address; /**< Address (Periph id + offset) in the device */
} SpmiCfg_Ppid;

typedef struct 
{
    uint8 pvcPortId;              /**< PVC Port id */
    Spmi_AccessPriority priority; /**< SPMI bus command priority */
    SpmiCfg_Ppid* ppids;          /**< Peripheral access table */
    uint32 numPpids;              /**< Number of addresses in the ppids array */
} SpmiCfg_PvcPortCfg;

//******************************************************************************
// Public API Functions
//******************************************************************************

/**
 * @brief Initialize the driver 
 * 
 * This function initializes the Spmi Configuration driver. It is mandatory 
 * to call this function before using any other APIs of this driver
 * 
 * @param[in] initHw  True to initialize static hw configuration
 * 
 * @return  SPMI_SUCCESS on success, error code on error
 * 
 * @see SpmiCfg_DeInit
 */
Spmi_Result SpmiCfg_Init(boolean initHw);

/**
 * @brief De-initialize the driver 
 * 
 * This function de-initializes the Spmi Configuration driver.
 * 
 * @return  SPMI_SUCCESS on success, error code on error
 * 
 * @see SpmiCfg_Init
 */
Spmi_Result SpmiCfg_DeInit(void);

/**
 * @brief Gets the ppid and owner information for a SW Channel
 * 
 * This function gets the ppid and owner information of a channel
 * 
 * @param[in] channel  The SW Channel to query
 * @param[out] slaveId   Slave id of the device mapped to the channel
 * @param[out] periphId  Peripheral id of the device mapped to the channel
 * @param[out] owner     The channel's interrupt owner
 * 
 * @return  SPMI_SUCCESS on success, error code on error
 * 
 * @see SpmiCfg_GetPeripherialInfo
 */
Spmi_Result SpmiCfg_GetChannelInfo(uint16 channel, uint8* slaveId, uint8* periphId, uint8* owner);

/**
 * @brief Gets the channel and owner information of a peripheral
 * 
 * This function gets the channel and owner information of a peripheral
 * 
 * @param[in] slaveId   Slave id of the device
 * @param[in] periphId  Peripheral id
 * @param[out] channel  The peripheral's SW Channel
 * @param[out] owner    The peripheral's interrupt owner
 * 
 * @return  SPMI_SUCCESS on success, error code on error
 * 
 * @see SpmiCfg_GetChannelInfo
 */
Spmi_Result SpmiCfg_GetPeripherialInfo(uint8 slaveId, uint8 periphId, uint16* channel, uint8* owner);

/**
 * @brief Maps SW channels to a peripherals and sets ownership
 * 
 * This function will map logical SW Channels to the peripherals and set ownership.  
 * The mapping will exist for both the owner and the observer SW channels.
 * 
 * @Note Each owner's channels should be consecutive so they can be grouped together
 *       during SMMU/XPU configuration.
 * 
 * @param[in] entries  An array containing the mapping information.  The array
 *                     needs to be sorted high-to-low by slaveId then periphId.
 *                     e.g.  entries[0] -> {slaveId = 1, periphId = 0x12, ...}
 *                           entries[1] -> {slaveId = 0, periphId = 0x14, ...}
 *                           entries[2] -> {slaveId = 0, periphId = 0x03, ...}
 * @param[in] numEntries  Number of entries in the array
 * 
 * @return  SPMI_SUCCESS on success, error code on error
 */
Spmi_Result SpmiCfg_ConfigureChannels(SpmiCfg_ChannelCfg* entries, uint32 numEntries);


/**
 * @brief Configures the PVC ports of the PMIC Arbiter 
 * 
 * This function configures the PMIC Arbiter for various PVC 
 * ports. It also enables the listed PVC ports. 
 * 
 * @param[in] portCfg  The port configuration information
 * 
 * @return  SPMI_SUCCESS on success, error code on failure
 */
Spmi_Result SpmiCfg_ConfigurePvcPort(const SpmiCfg_PvcPortCfg* portCfg);

/**
 * @brief Configures the ports of the PMIC Arbiter
 * 
 * The PMIC Arbiter has 1 SW port, n number of PVC ports, and possibly other
 * ports for things such as debug.  Each of these ports contend with each other
 * for access to the SPMI bus.  This function will set the priority of these 
 * ports relative to each other.
 * 
 * @param[in] ports  Array containing the priority info.  The priority 
 *                   is used as the array index and the highest priority is 0.
 *                   e.g. ports[0] = 2 (port 2 has the highest priority)
 *                        ports[1] = 5 (port 5 has the next highest priority)
 * @param[in] numPorts  Number of ports in the array
 * 
 * @return  SPMI_SUCCESS on success, error code on failure
 */
Spmi_Result SpmiCfg_SetPortPriorities(uint8* ports, uint32 numPorts);

/**
 * @brief Enables or disables all PVC ports
 * 
 * This function will enable or disable the HW PVC ports.  Called after
 * the ports have been configured.
 * 
 * @param[in] enable  TRUE to enable, FALSE to disable
 * 
 * @return  SPMI_SUCCESS on success, error code on failure
 * 
 * @see SpmiCfg_ConfigurePvcPort
 */
Spmi_Result SpmiCfg_SetPVCPortsEnabled(boolean enable);

#endif
