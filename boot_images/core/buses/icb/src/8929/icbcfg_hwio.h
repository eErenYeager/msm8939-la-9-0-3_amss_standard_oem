#ifndef __ICBCFG_HWIO_H__
#define __ICBCFG_HWIO_H__
/*
===========================================================================
*/
/**
  @file icbcfg_hwio.h
  @brief Auto-generated HWIO interface include file.

*/
/*
  ===========================================================================

  Copyright (c) 2014 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/buses/icb/src/8929/icbcfg_hwio.h#1 $
  $DateTime: 2015/07/23 04:47:57 $
  $Author: pwbldsvc $

  ===========================================================================
*/

/* ICBCFG HWIO File for 8936 target */

/*----------------------------------------------------------------------------
 * BASE: BIMC
 *--------------------------------------------------------------------------*/
#define BIMC_BASE                                               0x00400000

#endif /* __ICBCFG_HWIO_H__ */
