/**
 * @file:  SpmiBare.h
 * @brief: 
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/03/19 01:58:37 $
 * $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/buses/spmi/src/platform/config/bear/default/SpmiBare.h#1 $
 * $Change: 7697609 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */
#ifndef SPMIBARE_H
#define	SPMIBARE_H

#define SPMI_BARE_OWNER_NUMBER     0
#define SPMI_BARE_PMIC_ARB_ADDRESS 0x02000000

#endif

