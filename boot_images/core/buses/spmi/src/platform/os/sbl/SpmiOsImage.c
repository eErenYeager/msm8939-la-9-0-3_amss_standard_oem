/**
 * @file:  SpmiOsImage.c
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/03/19 01:58:37 $
 * $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/buses/spmi/src/platform/os/sbl/SpmiOsImage.c#1 $
 * $Change: 7697609 $ 
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#include "SpmiOs.h"
#include "SpmiOsNhlos.h"
#include "SpmiLogs.h"
#include "DALSys.h"
#include "DALDeviceId.h"
#include "DDIClock.h"

//******************************************************************************
// Macros / Definitions / Constants
//******************************************************************************

// Ids expected by the Clk driver
#define SERIAL_CLK_ID "gcc_spmi_ser_clk"
#define AHB_CLK_ID    "gcc_spmi_ahb_clk"

//******************************************************************************
// Global Data
//******************************************************************************

static boolean clkHdlInited = FALSE;
static DalDeviceHandle* clkDev = NULL;
static ClockIdType ahbClkId;
static ClockIdType serClkId;

//******************************************************************************
// Local Helper Functions
//******************************************************************************

static void initClkDev(void)
{
    if(DAL_ClockDeviceAttach( DALDEVICEID_CLOCK, &clkDev ) != DAL_SUCCESS) {
        clkDev = NULL;
    }
    else 
    {
        if(DalClock_GetClockId( clkDev, SERIAL_CLK_ID, &serClkId ) != DAL_SUCCESS) {
            SPMI_LOG_WARNING( "Could not get handle to serial clock" );
        }
        
        if(DalClock_GetClockId( clkDev, AHB_CLK_ID, &ahbClkId ) != DAL_SUCCESS) {
            SPMI_LOG_WARNING( "Could not get handle to ahb clock" );
        }
    }
    
    clkHdlInited = TRUE;
}

//******************************************************************************
// Public API Functions
//******************************************************************************

Spmi_Result SpmiOs_Malloc(uint32 size, void** buf)
{
    return SPMI_FAILURE_FUNCTIONALITY_NOT_SUPPORTED;
}

Spmi_Result SpmiOs_Free(void* buf)
{
    return SPMI_FAILURE_FUNCTIONALITY_NOT_SUPPORTED;
}

Spmi_Result SpmiOs_RegisterISR(SpmiOs_IsrPtr isr, void* ctx)
{
    return SPMI_FAILURE_FUNCTIONALITY_NOT_SUPPORTED;
}

SpmiOs_ClkStatus SpmiOs_GetSerialClkState()
{
    if(!clkHdlInited) {
        initClkDev();
    }
    
    if(clkDev != NULL) {
        return DalClock_IsClockOn( clkDev, serClkId ) ? SPMI_CLK_ON : SPMI_CLK_OFF;
    }
    
    return SPMI_CLK_STATUS_NOT_SUPPORTED;
}

SpmiOs_ClkStatus SpmiOs_GetAhbClkState()
{
    if(!clkHdlInited) {
        initClkDev();
    }
    
    if(clkDev != NULL) {
        return DalClock_IsClockOn( clkDev, ahbClkId ) ? SPMI_CLK_ON : SPMI_CLK_OFF;
    }
    
    return SPMI_CLK_STATUS_NOT_SUPPORTED;
}

uint64 SpmiOs_GetTimeTick() 
{
    return 0;
}
