/**
 * @file:  PmicArbConstants.h
 * @brief: 
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/03/19 01:58:37 $
 * $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/buses/spmi/src/core/hal/bear/PmicArbConstants.h#1 $
 * $Change: 7697609 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 11/25/13 Initial Version
 */
#ifndef PMICARBCONSTANTS_H
#define	PMICARBCONSTANTS_H

#include "PmicArbCoreHwio.h"

#define PMIC_ARB_MAX_PERIPHERAL_SUPPORT (HWIO_PMIC_ARBq_CHNLn_CMD_MAXn + 1)

#endif

