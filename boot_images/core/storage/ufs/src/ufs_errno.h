#ifndef __UFS_ERRNO_H
#define __UFS_ERRNO_H
/**********************************************************************
 *
 *   UFS private definition header file
 *
 * Copyright (c) 2012-2013 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 *
 **********************************************************************/

/*======================================================================

                        EDIT HISTORY FOR MODULE

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/storage/ufs/src/ufs_errno.h#1 $
  $DateTime: 2015/03/19 01:58:37 $ $Author: pwbldsvc $

when         who     what, where, why
----------   ---     ---------------------------------------------------
2012-12-18   rh      Initial creation
=======================================================================*/

#define EOK                         0
#define ENOENT                      2
#define EIO                         5
#define EBUSY                       16
#define ENODEV                      19
#define EINVAL                      22
#define EPROTO                      100
#define ETIME                       101




#endif /* __UFS_ERRNO_H */
