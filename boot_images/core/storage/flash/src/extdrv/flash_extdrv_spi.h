#ifndef ___FLASH_EXTDRV_SPI_H___
#define ___FLASH_EXTDRV_SPI_H___

/*=======================================================================
 * FILE:        flash_extdrv_spi.h
 *
 * DESCRIPTION
 *     SPI driver interface API's for flash drivers to drive SPI flash.
 *
 * Copyright (c) 2010 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * QUALCOMM confidential and Proprietary
 *=======================================================================*/

/*=======================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *   $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/storage/flash/src/extdrv/flash_extdrv_spi.h#1 $
 *   $DateTime: 2015/03/19 01:58:37 $
 *   $Author: pwbldsvc $
 *
 * when         who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 08/27/10     yg      Initial version
 *=======================================================================*/

#include "comdef.h"

/* Return value definitions */
#define SPI_DEVICE_DONE     0
#define SPI_DEVICE_FAIL    -1

struct spi_handle;

typedef struct spi_handle* spi_handle_t;

/*
 *   Mode :   Clock @ Idle       Latch Edge     Drive Edge (= ~ Latch_Edge)
 *  ------------------------------------------------------------------------
 *    0   :      Low              Rising        Falling
 *    1   :      Low              Falling       Rising
 *    2   :      High             Falling       Rising
 *    3   :      High             Rising        Falling
 *
 */
enum spi_mode {
  SPI_MODE_0 = 0,
  SPI_MODE_1,
  SPI_MODE_2,
  SPI_MODE_3,
  SPI_MODE_UNKNOWN = 0x7FFFFFFF,
};

/* This constant should be assigned to the interface_version field whereever
 * referenced while sending config data to the APIs
 * 101 holds for 1.01 */
#define SPI_INTERFACE_VERSION              0x101

/* By default 8 bit data width is used */
enum spi_data_width {
  SPI_DATA_WIDTH_8_BIT = 0,  /* 8 Bit DATA Width */
  SPI_DATA_WIDTH_16_BIT,     /* 16 Bit DATA Width */
  SPI_DATA_WIDTH_24_BIT,     /* 24 Bit DATA Width */
  SPI_DATA_WIDTH_32_BIT,     /* 32 Bit DATA Width */
  SPI_DATA_WIDTH_UNKNOWN = 0x7FFFFFFF /* Unknown Width */
};

/* Choose between whether to return all the data received or discard the junk
 * data received while sending the command bytes */
enum return_data_option
{
  DATA_DISCARD_WRITE_SIZE, /* Discard write size data in the beginning */
  DATA_RETURN_ALL,         /* Return all the data */
  DATA_RETURN_OPTION_UNKNOWN = 0x7FFFFFFF /* Unknown Width */     
};

struct spi_config
{
  uint16 interface_version;
  enum spi_data_width data_width;
  enum return_data_option data_ret_option;
};


/***************************************************************************
 *
 *                    Public API's
 *
 ***************************************************************************/

/* Open specified port for read/write operation.
 * By default operates in 8 bit mode and configured for discarding write size
 *
 * Returns the handle on success or 0 on failure */
spi_handle_t spi_open (int port, enum spi_mode mode);

/* Override if default configuration of 8 bit mode and discarding write size
 * need to be changed
 *
 * Returns 0 on success and -1 on failure */
int spi_configure (spi_handle_t h, struct spi_config* config_options);

/* Write command and data buffer out into SPI bus
 * If cmd_bytes, data_bytes and both buffers are 4 byte aligned then the API
 * would operate in the most efficient mode
 *
 * Returns 0 on success and -1 on failure */
int spi_write (spi_handle_t h, uint8* cmd_buffer, int cmd_bytes,
               uint8* data_buffer, int data_bytes);

/* Write command buffer and read into data buffer to/from SPI bus
 * If cmd_bytes, data_bytes and both buffers are 4 byte aligned then the API
 * would operate in the most efficient mode
 *
 * Returns 0 on success and -1 on failure */
int spi_read  (spi_handle_t h, uint8* cmd_buffer, int cmd_bytes,
               uint8* data_buffer, int data_bytes);

/* Close the handle
 * Returns 0 on success and -1 on failure */
int spi_close (spi_handle_t h);

/* Unit test routine, delete later */
int spi_test (int port);

#endif   /*  ___FLASH_EXTDRV_SPI_H___  */

