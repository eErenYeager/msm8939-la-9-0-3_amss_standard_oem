#ifndef FLASH_EXTDRV_I2C_8974_H
#define FLASH_EXTDRV_I2C_8974_H

/*=======================================================================
 * FILE:        flash_extdrv_i2c_8974.h
 *
 * SERVICES:
 *
 * DESCRIPTION: Header file for I2C related registers
 *
 * PUBLIC CLASSES:  Not Applicable
 *
 * INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 *
 * Copyright (c) 2012 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * QUALCOMM Confidential and Proprietary
 *=======================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/storage/flash/src/extdrv/config_v2/flash_extdrv_i2c_8974.h#1 $ $DateTime: 2015/03/19 01:58:37 $ $Author: pwbldsvc $
 *
 *
 * when         who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 09/18/12     eo      Configure I2C Clock based on platform
 * 08/03/12     eo      Update to QUP4 for HW Platform support.
 * 07/27/12     eo      Use BOOT_PRE_SILICON feature for lower loop count
 * 07/20/12     eo      Reduce read/write check loop count to 1000
 * 05/29/12     eo      Initial Revision
 *==================================================================*/

/*===================================================================
 *
 *                     Include Files
 *
 ====================================================================*/
#include "msmhwiobase.h"
#include "flash_extdrv_i2c.h"

/* Enable I2C multi-byte support */
#define FLASH_I2C_ENABLE_MULTIBYTE_READ
#define FLASH_I2C_ENABLE_MULTIBYTE_WRITE

/* EEPROM device address */
#ifndef FEATURE_RUMI_BOOT
  #define FLASH_I2C_DEVICE_ADDR            0x52
#else
  #define FLASH_I2C_DEVICE_ADDR            0x50
#endif

/* The following configs need to be checked and updated for 8974.
 * I2C10: BLSP2, QUP4:. */
#define BLSP2_QUP0                          (PERIPH_SS_BASE + 0x163000)
#define BSLP2_QUP1                          (PERIPH_SS_BASE + 0x164000)
#define BSLP2_QUP2                          (PERIPH_SS_BASE + 0x165000)
#define BSLP2_QUP3                          (PERIPH_SS_BASE + 0x166000)
#define BSLP2_QUP4                          (PERIPH_SS_BASE + 0x167000)
#define BSLP2_QUP5                          (PERIPH_SS_BASE + 0x168000)

#define TLMM_CSR_REG_BASE                   (TLMM_BASE + 0x00010000)
#define HWIO_GPIO_CFGn_ADDR(c) \
  (TLMM_CSR_REG_BASE + 0x00001000 + 0x10 * (c))

#ifndef FEATURE_RUMI_BOOT
  #define I2C_GSBI_QUP_REG_BASE              BSLP2_QUP4
  #define I2C_GSBI_REG_BASE                  0x0 //NOP: Not Applicable
  #define I2C_GSBI_QUP_APPS_MD_ADDR          0x0 //NOP: Not Applicable
  #define I2C_GSBI_QUP_APPS_NS_ADDR          0x0 //NOP: Not Applicable
  #define I2C_GSBI_HCLK_CTL_ADDR             0x0 //NOP: Not Applicable
  /* GPIO pins (83 and 84) for EEPROM */
  #define I2C_GPIO_CFG_SDA_ADDR              HWIO_GPIO_CFGn_ADDR(83)
  #define I2C_GPIO_CFG_CLK_ADDR              HWIO_GPIO_CFGn_ADDR(84)
#else
  /* Temp set to use non RUMI config values */
  #define I2C_GSBI_QUP_REG_BASE              BSLP2_QUP3
  #define I2C_GSBI_REG_BASE                  0x0 // NOP: Not Applicable
  #define I2C_GSBI_QUP_APPS_MD_ADDR          0x0 // NOP: Not Applicable
  #define I2C_GSBI_QUP_APPS_NS_ADDR          0x0 // NOP: Not Applicable
  #define I2C_GSBI_HCLK_CTL_ADDR             0x0 // NOP: Not Applicable
  /* GPIO pins (55 and 56) for EEPROM */
  #define I2C_GPIO_CFG_SDA_ADDR              HWIO_GPIO_CFGn_ADDR(55)
  #define I2C_GPIO_CFG_CLK_ADDR              HWIO_GPIO_CFGn_ADDR(56)
#endif

/* GPIO pull up, 16mA drive mode, Func Select 3. */
/* (NOTE this might need adjusted for 8974 surf) */
#define I2C_SDA_GPIO_CFG_VAL               0x1CF
#define I2C_CLK_GPIO_CFG_VAL               0x1CF

/* The following magic numbers are from clock regime team */
#ifndef FEATURE_RUMI_BOOT
  #define I2C_QUP_CLK_MD_VAL               0x100F7
  #define I2C_QUP_CLK_NS_VAL               0xF80B43
#else
  #define I2C_QUP_CLK_MD_VAL               0x0
  #define I2C_QUP_CLK_NS_VAL               0xA00
#endif

#ifndef BOOT_PRE_SILICON
  #define I2C_DELAY_COUNT                  40000
  #define I2C_RW_CHECK_LOOP_COUNT          500000
#else
  #define I2C_DELAY_COUNT                  2000
  #define I2C_RW_CHECK_LOOP_COUNT          5000
#endif

#define I2C_GSBI_QUP_CONFIG_ADDR           (I2C_GSBI_QUP_REG_BASE + 00000000)
#define I2C_GSBI_QUP_STATE_ADDR            (I2C_GSBI_QUP_REG_BASE + 0x00000004)
#define I2C_GSBI_QUP_IO_MODES_ADDR         (I2C_GSBI_QUP_REG_BASE + 0x00000008)
#define I2C_GSBI_QUP_SW_RESET_ADDR         (I2C_GSBI_QUP_REG_BASE + 0x0000000c)
#define I2C_GSBI_QUP_OPERATIONAL_ADDR      (I2C_GSBI_QUP_REG_BASE + 0x00000018)
#define I2C_GSBI_QUP_ERROR_FLAGS_ADDR      (I2C_GSBI_QUP_REG_BASE + 0x0000001c)
#define I2C_GSBI_QUP_ERROR_FLAGS_EN_ADDR   (I2C_GSBI_QUP_REG_BASE + 0x00000020)
#define I2C_GSBI_QUP_MX_OUTPUT_COUNT_ADDR  (I2C_GSBI_QUP_REG_BASE + 0x00000100)
#define I2C_GSBI_QUP_MX_WRITE_COUNT_ADDR   (I2C_GSBI_QUP_REG_BASE + 0x00000150)
#define I2C_GSBI_QUP_MX_INPUT_COUNT_ADDR   (I2C_GSBI_QUP_REG_BASE + 0x00000200)
#define I2C_GSBI_QUP_MX_READ_COUNT_ADDR    (I2C_GSBI_QUP_REG_BASE + 0x00000208)
#define I2C_GSBI_I2C_MASTER_CLK_CTL_ADDR   (I2C_GSBI_QUP_REG_BASE + 0x00000400)

#define I2C_GSBI_QUP_OUTPUT_FIFOc_ADDR(c)  \
  (I2C_GSBI_QUP_REG_BASE + 0x00000110 + 4 * (c))
#define I2C_GSBI_QUP_INPUT_FIFOc_ADDR(c)   \
  (I2C_GSBI_QUP_REG_BASE + 0x00000218 + 4 * (c))

#define I2C_GSBI_QUP_OUTPUT_FIFO_ADDR  I2C_GSBI_QUP_OUTPUT_FIFOc_ADDR(0)
#define I2C_GSBI_QUP_INPUT_FIFO_ADDR   I2C_GSBI_QUP_INPUT_FIFOc_ADDR(0)

#define I2C_GSBI_CTRL_REG_ADDR             (I2C_GSBI_REG_BASE + 00000000)
                                   
/* I2C, QUP, GSBI Bit */
#define I2C_STATE_VALID \
  I2C_GSBI_QUP_STATE_STATE_VALID_BMSK
#define I2C_INPUT_FIFO_N_EMPTY \
  I2C_GSBI_QUP_OPERATIONAL_INPUT_FIFO_N_EMPTY_BMSK
#define I2C_OUTPUT_DONE_FLAG \
  I2C_GSBI_QUP_OPERATIONAL_MAX_OUTPUT_DONE_FLAG_BMSK
#define I2C_INPUT_DONE_FLAG \
  I2C_GSBI_QUP_OPERATIONAL_MAX_INPUT_DONE_FLAG_BMSK
#define I2C_IO_MODES_UNPACK_EN \
  I2C_GSBI_QUP_IO_MODES_UNPACK_EN_BMSK
#define I2C_IO_MODES_PACK_EN \
  I2C_GSBI_QUP_IO_MODES_PACK_EN_BMSK
#define I2C_CFG_N_BMSK \
  I2C_GSBI_QUP_CONFIG_N_BMSK
#define I2C_PROTOCOL_CODE_BMSK \
  I2C_GSBI_GSBI_CTRL_REG_PROTOCOL_CODE_BMSK
#define I2C_CFG_MINI_CORE_BMSK \
  I2C_GSBI_QUP_CONFIG_MINI_CORE_BMSK
#define I2C_INPUT_MODE_BMSK \
  I2C_GSBI_QUP_IO_MODES_INPUT_MODE_BMSK
#define I2C_OUTPUT_MODE_BMSK \
  I2C_GSBI_QUP_IO_MODES_OUTPUT_MODE_BMSK
#define I2C_GSBI_HCLK_ENABLE \
  I2C_GSBI_HCLK_CTL_CLK_BRANCH_ENA_BMSK

/* The following is extracted from msmhwioreg.h without GSBI port dependency */
#define I2C_GSBI_QUP_STATE_STATE_VALID_BMSK                    0x4
#define I2C_GSBI_QUP_OPERATIONAL_INPUT_FIFO_N_EMPTY_BMSK       0x20
#define I2C_GSBI_QUP_OPERATIONAL_MAX_OUTPUT_DONE_FLAG_BMSK     0x400
#define I2C_GSBI_QUP_OPERATIONAL_MAX_INPUT_DONE_FLAG_BMSK      0x800
#define I2C_GSBI_QUP_IO_MODES_UNPACK_EN_BMSK                   0x4000
#define I2C_GSBI_QUP_IO_MODES_PACK_EN_BMSK                     0x8000
#define I2C_GSBI_QUP_CONFIG_N_BMSK                             0x1f
#define I2C_GSBI_GSBI_CTRL_REG_PROTOCOL_CODE_BMSK              0x70
#define I2C_GSBI_QUP_CONFIG_MINI_CORE_BMSK                     0xf00
#define I2C_GSBI_QUP_IO_MODES_INPUT_MODE_BMSK                  0x3000
#define I2C_GSBI_QUP_IO_MODES_OUTPUT_MODE_BMSK                 0xc00
#define I2C_GSBI_HCLK_CTL_CLK_BRANCH_ENA_BMSK                  0x10

/* Watchdog defines */
/* for 8974: 0xF9017004 KPSS_APCS_WDOG_RESET */
#define WDOG_RESET_ADDR                                        0xF9017004
#define WDOG_RESET_VAL                                         1

/* Define I2C clock setting. */
#define I2C_CLOCK_KHZ                                          I2C_CLK_100KHZ

void i2c_reconfigure(struct i2c_reg_addrs *i2c_reg_addr, 
  struct i2c_config_data* i2c_cfg_data_val) \
{}

#endif  /* FLASH_EXTDRV_I2C_8974_H */
