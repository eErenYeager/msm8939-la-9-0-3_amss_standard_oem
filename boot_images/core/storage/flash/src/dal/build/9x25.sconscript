#===============================================================================
#
# 9x25 Target Specific Scons file for Flash DAL
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright (c) 2012 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Confidential and Proprietary
#
#-------------------------------------------------------------------------------
#
#  $Header:  $
#  $DateTime: 2011/08/04 22:04:12 $
#  $Author: coresvc $
#  $Change: 1868724 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 10/19/12   sv      Use direct cpu mode for Ehostdl on 9x25
# 09/17/12   eo      Ehostdl/hostdl support for 9x25
# 09/05/12   sv      BAM based NAND driver support
# 06/20/12   sv      Initial Revision
#===============================================================================

Import('env')

nand_dcpu_reduced_images = [
   ]
   
nand_dcpu_images = [
   'EHOSTDL_IMAGE',
   ]

nand_bam_reduced_images = [
   'NAND_BOOT_DRIVER',
   ]

nand_bam_images = [
   'MODEM_IMAGE', 'CBSP_MODEM_IMAGE',
   'APPS_IMAGE', 'CBSP_APPS_IMAGE',
   'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
   'HOSTDL_IMAGE', 'NAND_TOOLS_IMAGE',
   ]

nor_spi_int_images = [
   'QDSP6_SW_IMAGE', 'CBSP_QDSP6_SW_IMAGE',
   ]

nor_spi_poll_images = [
   'NOR_TOOLS_IMAGE',
   ]

nor_i2c_images = [
   'I2C_BOOT_DRIVER',
   'NOR_TOOLS_IMAGE',
   ]

dictionary_keys = set(env.Dictionary())


# Find the intersection
nand_dcpu_images_result_keys = set(nand_dcpu_images) & dictionary_keys
nand_dcpu_reduced_images_result_keys = set(nand_dcpu_reduced_images) & dictionary_keys
nand_bam_images_result_keys = set(nand_bam_images) & dictionary_keys
nand_bam_reduced_images_result_keys = set(nand_bam_reduced_images) & dictionary_keys
nor_spi_int_images_result_keys = set(nor_spi_int_images) & dictionary_keys
nor_spi_poll_images_result_keys = set(nor_spi_poll_images) & dictionary_keys
nor_i2c_images_result_keys = set(nor_i2c_images) & dictionary_keys


#Enable the required flags for this image
if len(nor_spi_int_images_result_keys) != 0 and env.has_key('USES_NOR_SPI') :
   env.Replace(ENABLE_NOR_SPI_INT = 'yes')
else:
   if len(nand_dcpu_reduced_images_result_keys) != 0 :
      env.Replace(ENABLE_NAND_DCPU_REDUCED = 'yes')
   elif len(nand_dcpu_images_result_keys) != 0 :
      env.Replace(ENABLE_NAND_DCPU = 'yes')
   elif len(nand_bam_reduced_images_result_keys) != 0 :
      env.Replace(ENABLE_NAND_BAM_REDUCED = 'yes')
   elif len(nand_bam_images_result_keys) != 0 :
      env.Replace(ENABLE_NAND_BAM = 'yes')

if len(nor_spi_poll_images_result_keys) != 0 :
   env.Replace(ENABLE_NOR_SPI_POLL = 'yes')

if len(nor_i2c_images_result_keys) != 0 :
   env.Replace(ENABLE_NOR_I2C = 'yes')
