/*====================================================================
 *
 * FILE:        flash_spi_wrapper_poll.c
 *
 * SERVICES:    Polling mode SPI support for flash drivers
 *
 * DESCRIPTION: SPI support for flash drivers used in tools
 *
 * PUBLIC CLASSES:  Not Applicable
 *
 * INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A
 *
 * Copyright (c) 2010, 2012 Qualcomm Technologies Incorporated.
 * All Rights Reserved.
 * QUALCOMM Confidential and Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/storage/flash/src/dal/flash_spi_wrapper_poll.c#1 $ 
 *  $DateTime: 
 *  $ $Author: pwbldsvc $
 *
 *
 * when         who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 08/21/12     sb      Remove DAL SPI dependency
 * 09/06/10     sv      Initial Revision
 *==================================================================*/

/*===================================================================
 *
 *                     Include Files
 *
 ====================================================================*/

#include "flash_nor_spi.h"
#include "flash_nor_config.h"
#include "flash_spi_wrapper.h"
#include "flash_dal_util.h"
#include "flash_extdrv_spi.h"

/* SPI device handle */
static spi_handle_t spi_handle = NULL;

/* Function to configure the SPI device */
static int flash_spi_dev_config(void)
{
  int result = FLASH_DEVICE_DONE;

  return result;
}

/* This function initializes SPI specific data structures
              The following actions are performed:
                1.  Attach the SPI device
                2.  Initialize flash SPI transfer request.
*/
int flash_spi_init(flash_client_ctxt *client_ctxt)
{
  int retval = FLASH_DEVICE_DONE;
  int port_num;
  
  port_num = (int)flash_get_property(FLASH_SPI_GSBI_PORT_ID);
  if (FLASH_PROPERTY_NOT_DEFINED == (uint32)port_num)
  {
    FLASHLOG(3,(" Flash: Failed to get port number.\n"));
    return FLASH_DEVICE_FAIL;
  }

  spi_handle = spi_open (port_num, SPI_MODE_3);
  if (spi_handle == NULL)
  {
    retval = FLASH_DEVICE_FAIL;
  }    

  return retval;
}

/*  This function detaches the SPI handle.
 */
int flash_spi_close(void)
{
  /* Close SPI Device handle */
  spi_close(spi_handle);

  return FLASH_DEVICE_DONE;
}

/*  This is SPI read wrapper for SPI driver. This is used to read data from a   
 *  device connected through SPI Interface
 */
void flash_spi_read_op(struct nor_spi_xfer_buf_info *flash_buf_info, int *status)
{
  int spi_status = SPI_DEVICE_FAIL; 

  spi_status = spi_read (spi_handle, flash_buf_info->spi_cmd_buf, 
    flash_buf_info->cmd_buf_len, flash_buf_info->spi_data_buf, 
    flash_buf_info->data_buf_len);

  if(spi_status == SPI_DEVICE_DONE)
  {
    *status = FLASH_DEVICE_DONE;
  }
  else
  {
    *status = FLASH_DEVICE_FAIL;
  }      
}

/*  This is SPI write wrapper for SPI driver. This is used to write data to a   
 *  device connected through SPI Interface
 */
void flash_spi_write_op(struct nor_spi_xfer_buf_info *flash_buf_info, int *status)
{
  int spi_status = SPI_DEVICE_FAIL; 
  
  spi_status = spi_write (spi_handle,  flash_buf_info->spi_cmd_buf, 
    flash_buf_info->cmd_buf_len, flash_buf_info->spi_data_buf, 
    flash_buf_info->data_buf_len);

  if(spi_status == SPI_DEVICE_DONE)
  {
    *status = FLASH_DEVICE_DONE;
  }
  else
  {
    *status = FLASH_DEVICE_FAIL;
  } 
}

/*
 * Init/Configure the SPI wrapper APIs
 */
void flash_spi_configure(void *spi_configs)
{
  struct nor_spi_wrapper *wrapper = (struct nor_spi_wrapper *)spi_configs;

  if (NULL != wrapper)
  {
    wrapper->init = flash_spi_init;
    wrapper->close = flash_spi_close;
    wrapper->dev_cfg = flash_spi_dev_config;
    wrapper->read_op = flash_spi_read_op;
    wrapper->write_op = flash_spi_write_op;
  }
}

