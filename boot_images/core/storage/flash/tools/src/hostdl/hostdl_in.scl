// *====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
//
//                     SCATTER LOADING DESCRIPTION FILE TEMPLATE
//  
//
//  GENERAL DESCRIPTION
//
//  Memory Map for HOSTDL.
//
//  This file is a template which gets run through the C pre-processor to
//  generate the actual scatter load file which will be used.  This allows
//  this file to be used by all targets and be relocated on the fly.
//
//  Copyright (c) 2008-2012, 2014 Qualcomm Incorporated. 
//  All Rights Reserved.
//  Qualcomm Confidential and Proprietary
//
//*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

/* ===========================================================================

                           EDIT HISTORY FOR FILE
  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/storage/flash/tools/src/hostdl/hostdl_in.scl#1 $ $DateTime: 2015/03/19 01:58:37 $ $Author: pwbldsvc $
   
 when       who     what, where, why
 --------   ---     -------------------------------------------------------
 06/25/14   sb      Replace qhsusb with qusb 
 11/01/12   sv      Update HEAP Section name 
 10/19/12   sv      Add new ZI region to use ZI from RPM code region 
 09/22/11   sv      Modified for ehostdl/hostdl support for 9x15
 08/18/10   bb      .scl file cleanup for e-hostdl and hostdl
 07/23/10   sv      Adapted to boot changes in 9K
 03/10/10   sv      Changes for ehostdl support in nighthawk
 02/09/10   op      Backout the change, it breaks other targets
 01/15/10   op      Support Halcyon boot for Ehostdl
 01/08/10   op      Support Halcyon boot for Ehostdl
 08/23/09   op      Add Ehostdl support to 8K WM7
 04/23/09   sv      Modified for Ehostdl support
 10/14/08   mm      Created.
==========================================================================*/

RAM CODE_START_ADDR
{
   CODE CODE_START_ADDR 
   {
      EHOSTDL_STARTUP_SOURCE (BootRom, +FIRST)
   }

   CODE_ALL +0
   {
      * (+RO)
   }

   APP_RAM DATA_ADDR
   {

      * (.constdata)   ; Relocate all constdata to RAM since accessing
                       ; string literals and other unaligned
                       ; data in ADSP IRAM could cause issues
      * (+RW, +ZI)
   }

   ZI_RAM  ZI_ADDR
   {
      qusb_*.o (+ZI)
      * (FLASH_TOOLS_HEAP_ZONE)
   }
}
