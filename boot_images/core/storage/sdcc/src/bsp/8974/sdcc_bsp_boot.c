/**********************************************************************
* sdcc_bsp.c
*
* SDCC(Secure Digital Card Controller) driver BSP.
*
* This file implements the SDCC driver BSP for the board in use for BOOT.
*
* Copyright (c) 2009-13 by Qualcomm Technologies, Inc.
* All Rights Reserved.
* Qualcomm Technologies Proprietary and Confidential.
*
**********************************************************************/

/*=======================================================================
                        Edit History

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/storage/sdcc/src/bsp/8974/sdcc_bsp_boot.c#1 $
$DateTime: 2015/03/19 01:58:37 $

YYYY-MM-DD   who     what, where, why
---------------------------------------------------------------------- 
2013-08-21   bn      Code clean up. Avoid hard coded drive number
2013-08-12   bn      Code clean up
2013-07-19   bn      Changed the drive strength for 8974Pro
2013-07-01   bn      Changed the drive strength for 8974Pro
2013-05-14   bb      Enable 8-bit mode for eMMC
2012-07-31   vk      Use ClockBoot APIs for frequency setting
2012-06-22   vk      Use Dalsys_busywait instead of software polling
2012-05-25   vk      Enable H clock for SDC3
2012-02-10   rh      Update the HWIO definition file
2011-12-20   rh      Adding the cache flush routine
2011-09-13   rh      Mark SDC3 as an embedded slot
2011-03-22   vin     Mark SDC3 as an open slot
2010-12-10   rh      Temporarily add RUMI support - to be removed later
2010-12-08   rh      SDC1 port should use special TLMM/GPIO registers
2010-12-08   rh      Use APCS_DGT instead of SCSS DGT register
2010-08-12   rh      Switching to DGT timer by the request of the RF/PMIC team
2010-07-02   vin     Added sdcc_bsp_clk_type  
2010-06-16   vj      Added sdcc_bsp_get_max_supported_clk 
2010-06-11   rh      Mark the third slot as embedded slot type
2010-05-25   rh      Mark the first slot as embedded slot type
2010-04-28   rh      device change to device_id in sdcc_bsp_dmov_properties
2010-04-14   rh      Use the Scorpion delay counter for SSBL and SPBL
2010-03-18   rh      Changes require for SURF bringup
2009-12-04   rh      Initial version adapted from 7x30
===========================================================================*/

/*==============================================================================
  Includes for this module.
  ----------------------------------------------------------------------------*/
#include "sdcc_bsp.h"
#include "sdcc_priv.h"
#include SDCC_OSAL_H
#include SDCC_HWIO_H
#include "ClockBoot.h"
#include "busywait.h"

/*==============================================================================
                           S D C C    B S P
==============================================================================*/

/* Max number of slots supported by the Board */
#define SDCC_BSP_NUM_SLOTS         4
/* Used for slot number validation*/
#define SDCC_BSP_INVALID_SLOT SDCC_BSP_NUM_SLOTS

#define SDCC_BSP_WLAN_SLOT_DEFAULT 1
/* There are currently no designated slots for UBM & CMMB*/
#define SDCC_BSP_UBM_SLOT_DEFAULT  SDCC_BSP_INVALID_SLOT
#define SDCC_BSP_CMMB_SLOT_DEFAULT SDCC_BSP_INVALID_SLOT

/* Cache line size */
#define SDCC_BSP_CACHE_LINE_SIZE   32

/* Slot configuration information for the board */
static sdcc_bsp_slot_type sdcc_bsp_slot_config [SDCC_BSP_NUM_SLOTS] = {
   SDCC_BSP_EMBEDDED_MEM_SLOT|SDCC_BSP_8BIT_SLOT,
   SDCC_BSP_OPEN_SLOT,
   SDCC_BSP_NO_SLOT,
   SDCC_BSP_NO_SLOT
};

#define GPIO_DRIVE_2MA                      0x0
#define GPIO_DRIVE_4MA                      0x1
#define GPIO_DRIVE_6MA                      0x2
#define GPIO_DRIVE_8MA                      0x3
#define GPIO_DRIVE_10MA                     0x4
#define GPIO_DRIVE_12MA                     0x5
#define GPIO_DRIVE_14MA                     0x6
#define GPIO_DRIVE_16MA                     0x7

#define GPIO_NO_PULL                        0x0
#define GPIO_PULL_DOWN                      0x1
#define GPIO_KEEPER                         0x2
#define GPIO_PULL_UP                        0x3

#define HWIO_GCC_SDCCn_BCR_OUT(n, v)    \
        out_dword(HWIO_GCC_SDCC1_BCR_ADDR + (0x40 * (n)), v)

/* use for reading the MSM ID 8974Pro */
#define HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_SHFT  0x10
#define HWIO_TCSR_SOC_HW_VERSION_DEVICE_NUMBER_BMSK  0xfff0000
#define HWIO_TCSR_SOC_HW_VERSION_ADDR 0xFD4A8000
#define HWIO_TCSR_SOC_HW_VERSION_INM(m)      \
        in_dword_masked(HWIO_TCSR_SOC_HW_VERSION_ADDR, m)

/******************************************************************************
* Description:
*    Sets the SDC controller clock speed based on the input parameter.
*
* Arguments:
*    driveno        [IN] : drive number
*    speed          [IN/OUT] : clock speed
*    mode           [IN] : which clock branch to adjust
******************************************************************************/
void sdcc_bsp_config_clock (uint32 driveno, uint32 *speed, uint32 mode)
{
  ClockSDCType clk;
  ClockBootPerfLevelType level;
#ifdef T_RUMI 
  /* RUMI3 specific configruation - to be stripped out during bringup */
  if (*speed < 400)
  {
    out_dword( 0x902828, 0x00100CF );
  }
  else 
  {
    out_dword( 0x902828, 0x0000000 );
  }
#else
  if (mode == SDCC_BSP_CLK_CONFIG_HCLK)
  {
    /* Clocks for eMMC are turned on in PBL */
    /* Turn on the clocks for external SD */
    /* External SD is the SDCC_BSP_OPEN_SLOT as set in sdcc_bsp_slot_config */
    if(SDCC_BSP_OPEN_SLOT == sdcc_bsp_slot_config [driveno])
    {
      switch(driveno)
      {
        case 0:
          HWIO_OUTF(GCC_SDCC1_AHB_CBCR, CLK_ENABLE, driveno);
          break;

        case 1:
          HWIO_OUTF(GCC_SDCC2_AHB_CBCR, CLK_ENABLE, driveno);
          break;

        case 2:
          HWIO_OUTF(GCC_SDCC3_AHB_CBCR, CLK_ENABLE, driveno);
          break;

        case 3:
          HWIO_OUTF(GCC_SDCC4_AHB_CBCR, CLK_ENABLE, driveno);
          break;
        
        default:
          break;
      }
    }

    return;
  }

  if (mode == SDCC_BSP_CLK_CONFIG_RESET)
  {
    HWIO_GCC_SDCCn_BCR_OUT (driveno, HWIO_GCC_SDCC1_BCR_BLK_ARES_BMSK);
    sdcc_bsp_hw_clk_busy_wait (500);
    HWIO_GCC_SDCCn_BCR_OUT (driveno, 0);
    return;
  }

  switch(driveno)
  {
    case 0:
      clk = CLK_SDC1;
      break;
    case 1:
      clk = CLK_SDC2;
      break;
    case 2:
      clk = CLK_SDC3;
      break;
    case 3:
      clk = CLK_SDC4;
      break;
    default:
      clk = CLK_SDC_NONE;
  }

  /* CLOCK_BOOT_PERF_MIN,   400 kHz
     CLOCK_BOOT_PERF_NOMINAL,  25 MHz 
     CLOCK_BOOT_PERF_MAX,   50 MHz 	*/

  level = CLOCK_BOOT_PERF_MIN;

  if(speed != NULL)
  {
    if (*speed <= 400)
    {
      level = CLOCK_BOOT_PERF_MIN;
    }
    else if (*speed <= 26000)
    {
      level = CLOCK_BOOT_PERF_NOMINAL;
    }
    else
    {
      level = CLOCK_BOOT_PERF_MAX;
    }
  }
  
  Clock_SetSDCClockFrequency (level, clk);

#endif /* T_RUMI*/

} /* sdcc_bsp_config_clock */

/******************************************************************************
* Name: sdcc_hw_clk_busy_wait
*
* Description:
*    This function provides a busy wait for the caller.
*
* Arguments:
*    us              [IN] : time in micro seconds
*
* Returns:
*    None
*      
******************************************************************************/
#if defined(SDCC_USES_SCORPION_WAIT)
void sdcc_bsp_hw_clk_busy_wait(uint32 us)
{
   uint32 count;
   HWIO_OUT(APCS_DGT_CNT, 0);
   HWIO_OUT(APCS_DGT_EN, 1);
   
   count = (us * 0.032768 * 160) + 1;
   
   while (HWIO_IN(APCS_DGT_CNT) < count);
   HWIO_OUT(APCS_DGT_EN, 0);
}
#else /* SCORPION_WAIT */
void sdcc_bsp_hw_clk_busy_wait(uint32 us)
{
   busywait(us);
}
#endif /* SDCC_USES_SCORPION_WAIT */

sdcc_bsp_slot_type
sdcc_bsp_get_slot_type (uint32 slot_num)
{
   sdcc_bsp_slot_type slot_type = SDCC_BSP_NO_SLOT;

   if (slot_num >= SDCC_BSP_NUM_SLOTS)
   {
      /* Return No slot avaliable*/
      slot_type = SDCC_BSP_NO_SLOT;
   }
   else
   {
      slot_type = sdcc_bsp_slot_config [slot_num];
   }

   return slot_type;
}

sdcc_bsp_err_type
sdcc_bsp_vdd_control (uint32 slot_num, uint32 state)
{
   return SDCC_BSP_NO_ERROR;
}

sdcc_bsp_err_type
sdcc_bsp_vdd_configure (uint32 slot_num)
{
   (void) slot_num;

   /* Place holder for sdcc vdd config */
   return SDCC_BSP_NO_ERROR;
}

/*=============================================================================
 * FUNCTION      sdcc_bsp_gpio_control
 *
 * DESCRIPTION   This function enables/disables the GPIO interface to the 
 *               card slot.
 *
 * PARAMETERS    [IN]: slot number
 *               [IN]: GPIO ON/OFF
 *
 * RETURN VALUE  sdcc_bsp_err_type.
 *
 *===========================================================================*/
sdcc_bsp_err_type
sdcc_bsp_gpio_control (uint32 slot_num, uint32 state)
{
   uint32 i = 0;
   uint8 emmc_clk_drv_strength = 0;
   uint8 emmc_cmd_drv_strength = 0;
   uint8 emmc_data_drv_strength = 0;

   (void)i;

#ifndef T_RUMI 
   if(slot_num >= SDCC_BSP_NUM_SLOTS)
   {
      return SDCC_BSP_ERR_INVALID_PARAM;
   }

   if(SDCC_BSP_NO_SLOT == sdcc_bsp_slot_config [slot_num])
   {
      return SDCC_BSP_ERR_INVALID_PARAM;
   }

   if (slot_num == SDCC_BSP_SLOT_0)
   {
      /* 8974 Pro uses 19mm emmc that requires a reduced drive strength */
      if ( HWIO_INF( TCSR_SOC_HW_VERSION, DEVICE_NUMBER ) == 0x8 )
      {
         emmc_clk_drv_strength = GPIO_DRIVE_16MA;
         emmc_cmd_drv_strength = GPIO_DRIVE_10MA;
         emmc_data_drv_strength = GPIO_DRIVE_10MA;         
      }
      else
      {
         emmc_clk_drv_strength = GPIO_DRIVE_8MA;
         emmc_cmd_drv_strength = GPIO_DRIVE_8MA;
         emmc_data_drv_strength = GPIO_DRIVE_8MA; 
      }    
      HWIO_OUTM( TLMM_SDC1_HDRV_PULL_CTL, 
              ( HWIO_FMSK(TLMM_SDC1_HDRV_PULL_CTL, SDC1_CMD_PULL)  |
                HWIO_FMSK(TLMM_SDC1_HDRV_PULL_CTL, SDC1_DATA_PULL) |
                HWIO_FMSK(TLMM_SDC1_HDRV_PULL_CTL, SDC1_CLK_HDRV)  |
                HWIO_FMSK(TLMM_SDC1_HDRV_PULL_CTL, SDC1_CMD_HDRV)  |
                HWIO_FMSK(TLMM_SDC1_HDRV_PULL_CTL, SDC1_DATA_HDRV) ),
              ( ( GPIO_PULL_UP << HWIO_SHFT(TLMM_SDC1_HDRV_PULL_CTL, SDC1_CMD_PULL) ) |
                ( GPIO_PULL_UP << HWIO_SHFT(TLMM_SDC1_HDRV_PULL_CTL, SDC1_DATA_PULL) ) |     
                ( emmc_clk_drv_strength << HWIO_SHFT(TLMM_SDC1_HDRV_PULL_CTL, SDC1_CLK_HDRV) ) |     
                ( emmc_cmd_drv_strength << HWIO_SHFT(TLMM_SDC1_HDRV_PULL_CTL, SDC1_CMD_HDRV) ) |     
                ( emmc_data_drv_strength << HWIO_SHFT(TLMM_SDC1_HDRV_PULL_CTL, SDC1_DATA_HDRV) ) ) );             
   }
   else if (slot_num == SDCC_BSP_SLOT_1)
   {
      HWIO_OUTM( TLMM_SDC2_HDRV_PULL_CTL, 
              ( HWIO_FMSK(TLMM_SDC2_HDRV_PULL_CTL, SDC2_CMD_PULL)  |
                HWIO_FMSK(TLMM_SDC2_HDRV_PULL_CTL, SDC2_DATA_PULL) |
                HWIO_FMSK(TLMM_SDC2_HDRV_PULL_CTL, SDC2_CLK_HDRV)  |
                HWIO_FMSK(TLMM_SDC2_HDRV_PULL_CTL, SDC2_CMD_HDRV)  |
                HWIO_FMSK(TLMM_SDC2_HDRV_PULL_CTL, SDC2_DATA_HDRV) ),
              ( ( GPIO_PULL_UP << HWIO_SHFT(TLMM_SDC2_HDRV_PULL_CTL, SDC2_CMD_PULL) ) |
                ( GPIO_PULL_UP << HWIO_SHFT(TLMM_SDC2_HDRV_PULL_CTL, SDC2_DATA_PULL) ) |     
                ( GPIO_DRIVE_8MA << HWIO_SHFT(TLMM_SDC2_HDRV_PULL_CTL, SDC2_CLK_HDRV) ) |     
                ( GPIO_DRIVE_8MA << HWIO_SHFT(TLMM_SDC2_HDRV_PULL_CTL, SDC2_CMD_HDRV) ) |     
                ( GPIO_DRIVE_8MA << HWIO_SHFT(TLMM_SDC2_HDRV_PULL_CTL, SDC2_DATA_HDRV) ) ) );             
   }
#endif

   return SDCC_BSP_NO_ERROR;
}

/******************************************************************************
* FUNCTION      sdcc_bsp_get_default_slot
*
* DESCRIPTION   SDCC would call this function to determine the 
*               default slot for a given application.
*    
* PARAMETERS    [IN]: slot type indicating the intended application
*
* RETURN VALUE  slot number
*
******************************************************************************/
uint32 sdcc_bsp_get_default_slot(sdcc_bsp_slot_type app)
{
   switch(app)
   {
   case SDCC_BSP_WLAN_SLOT:
      return SDCC_BSP_WLAN_SLOT_DEFAULT;
   case SDCC_BSP_UBM_SLOT:
      return SDCC_BSP_UBM_SLOT_DEFAULT;
   case SDCC_BSP_CMMB_SLOT:
      return SDCC_BSP_CMMB_SLOT_DEFAULT;
   default:
      return SDCC_BSP_INVALID_SLOT;
   }
}

/******************************************************************************
* FUNCTION      sdcc_bsp_get_cache_align_size
*
* DESCRIPTION   Function to get the cache alignment size.
*    
* PARAMETERS    None
*
* RETURN VALUE  cache alignment size
*
******************************************************************************/
uint32 sdcc_bsp_get_cache_align_size(void)
{
   return SDCC_BSP_CACHE_LINE_SIZE;
}

/*=============================================================================
* FUNCTION      sdcc_bsp_set_vreg_level
*
* DESCRIPTION   Set the VREG voltage level
*
* PARAMETERS    [IN]: Voltage to set the VREG to
*
* RETURN VALUE  None
*
*===========================================================================*/
void sdcc_bsp_set_vreg_level(uint32 voltageSelection)
{
   (void) voltageSelection;
}

/******************************************************************************
* FUNCTION      sdcc_bsp_cache_operation
*
* DESCRIPTION   Performs cache operations on the provided memory region.
*               Used for DMOV related operations only.
*               Clean operation is done for writes to basically push
*               data in the cache to the physical memory
*               Invalidate operation is done for reads to invalidate
*               a memory region in the cache
*    
* PARAMETERS    [IN]: cache operation type
*               [IN]: start address of the memory region
*               [IN]: length of the memory region
*
* RETURN VALUE  None
*
******************************************************************************/
void sdcc_bsp_cache_operation (uint32 op, void *addr, uint32 length)
{
   (void) op;
   (void) addr;
   (void) length;
#if 0
   if (op & SDCC_BSP_CACHE_CLEAN)
      dcache_flush_region(addr, length);
   if (op & SDCC_BSP_CACHE_INVAL)
      dcache_flush_region(addr, length);
   if (op & SDCC_BSP_MEM_BARRIER)
      memory_barrier();
#endif 
}

/*=============================================================================
* FUNCTION      sdcc_bsp_get_max_supported_clk
*
* DESCRIPTION   Get the maximum supported clock
*
* PARAMETERS    [IN]: drive number
*
* RETURN VALUE  Clock frequency supported in kHz
*===========================================================================*/
uint32 sdcc_bsp_get_max_supported_clk (uint32 driveno)
{
   return 100000;  /* 100 MHz max */
}
