/*==================================================================
 *
 * FILE:        deviceprogrammer_scatter_load.h
 *
 * DESCRIPTION:
 *   
 *
 *        Copyright � 2008-2013 Qualcomm Technologies Incorporated.
 *               All Rights Reserved.
 *               QUALCOMM Proprietary
 *==================================================================*/

/*===================================================================
 *
 *                       EDIT HISTORY FOR FILE
 *
 *   This section contains comments describing changes made to the
 *   module. Notice that changes are listed in reverse chronological
 *   order.
 *
 *   $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/storage/tools/deviceprogrammer/src/firehose/deviceprogrammer_scatter_load.h#1 $ 
 *   $DateTime: 2015/03/19 01:58:37 $ $Author: pwbldsvc $
 *
 * YYYY-MM-DD   who     what, where, why
 * ----------   ---     ----------------------------------------------
 * 2013-06-03   ah      Added legal header
 * 2013-05-31   ab      Initial checkin
 */

#ifndef DEVICEPROGRAMMER_SCATTER_LOAD_H
#define DEVICEPROGRAMMER_SCATTER_LOAD_H

#include "deviceprogrammer_utils.h"

void deviceprogrammer_scatter_load(void);

#endif

