;
; CPSR mode and bit definitions
Mode_SVC                EQU    0x13
Mode_ABT                EQU    0x17
Mode_UND                EQU    0x1b
Mode_SYS                EQU    0x1f
Mode_FIQ                EQU    0x11
Mode_IRQ                EQU    0x12
I_Bit                   EQU    0x80
F_Bit                   EQU    0x40
; CPSR Control Masks 
PSR_Fiq_Mask    EQU     0x40
PSR_Irq_Mask    EQU     0x80
; Processor mode definitions 
PSR_Supervisor  EQU     0x13
;============================================================================
;
;                             MODULE IMPORTS
;
;============================================================================
    ; Import the external symbols that are referenced in this module.
    IMPORT   deviceprogrammer_main_ctl
    IMPORT   main_firehose
;============================================================================
;
;                             MODULE EXPORTS
;
;============================================================================
    ; Export the symbols __main and _main to prevent the linker from
    ; including the standard runtime library and startup routine.
    EXPORT  _main
    EXPORT  __main
;============================================================================
; BOOT LOADER ENTRY POINT
;
; Execution of boot rom begins here.
;============================================================================
    AREA    DEVICEPROGRAMMER_ENTRY_POINT, CODE, READONLY
    CODE32
    PRESERVE8
    ENTRY
_main
__main
;============================================================================
; The mARM exception vector table located in boot ROM contains four bytes
; for each vector.  The boot ROM vector table will contain long branches that
; allow branching anywhere within the 32 bit address space.  Long branches
; will be used for all vectors except the reset vector.  Each long branch
; consists of a 32 bit LDR instruction using the PC relative 12 bit immediate
; offset address mode (ARM address mode 2).
;============================================================================
    b       deviceprogrammer_reset_handler              ; reset vector
    b       deviceprogrammer_reset_handler              ; undef_instr_vect vector
    b       deviceprogrammer_reset_handler              ; swi_vector
    b       deviceprogrammer_reset_handler              ; prefetch_abort_vector
    b       deviceprogrammer_reset_handler              ; data_abort_vector
    b       deviceprogrammer_reset_handler              ; reserved_vector
    b       deviceprogrammer_reset_handler              ; irq_vector
    b       deviceprogrammer_reset_handler              ; fiq_vector
;============================================================================
; The following initializes the relevant MSM circuits for IMEM operation
; then transfers control to the main control code in "C" which continues the
; boot strap process.
;============================================================================
deviceprogrammer_reset_handler
    ;Change to Supervisor Mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit
    ; Save the passing parameter from PBL
    mov     r7, r0
    ; Setup the supervisor mode stack
    ldr     r0, =0x08035000
    mov     r13, r0
    ; Switch to undefined mode and setup the undefined mode stack
    msr     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
    mov     r13, r0
    ; Switch to abort mode and setup the abort mode stack
    msr     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
    mov     r13, r0
    ; Return to supervisor mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit
    ; Restore the passing parameter
    mov     r0, r7
    ; ------------------------------------------------------------------
    ; This function never returns.
    ; deviceprogrammer_main_ctl();
    ; ------------------------------------------------------------------
    b       deviceprogrammer_main_ctl
deviceprogrammer_self_loop
    b deviceprogrammer_self_loop
    END
