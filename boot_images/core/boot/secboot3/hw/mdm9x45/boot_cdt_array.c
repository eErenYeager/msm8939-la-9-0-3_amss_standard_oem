/*===========================================================================

                    BOOT CONFIG DATA TABLE ARRAY

DESCRIPTION
  Contains config data table's default value and will be linked in sbl

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/boot/secboot3/hw/mdm9x45/boot_cdt_array.c#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $
    
when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/12/13   kedara  Modify DDR parameters for RUMI.
06/19/13   kedara  Initial Creation for 9x35,using the same file as 9x25

===========================================================================*/
/*==========================================================================

			       INCLUDE FILES

===========================================================================*/
#include "boot_comdef.h"
#include "boot_config_data.h"

/*=============================================================================

	    LOCAL DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, types,
variables and other items needed by this module.

=============================================================================*/
/**
* fixed size array that holds the cdt table in memory, it's intialized to 
* CDT v1 with two blocks which are platform id and DDR parameter. The platform id type 
* is UNKNOWN as default.
* This file should be automatically generated with cdt_generator.py tool if customized
* platform id and DDR paramter are desired.
*/
__align(4) uint8 config_data_table[CONFIG_DATA_TABLE_MAX_SIZE] = 
{
	/* Header */
	0x43, 0x44, 0x54, 0x00, 
	0x01, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 

	/* Meta data */
	0x16, 0x00, 
	0x05, 0x00, 
	0x1B, 0x00, 
	0x88, 0x01, 

	/* Block data */
#ifdef BOOT_PRE_SILICON
  /* 0x10 is virtio platform id*/
  /* 0x0F is Rumi platform id*/  
	0x02, 0x0F, 0x01, 0x00, 0x00,
#else
	0x02, 0x00, 0x01, 0x00, 0x00,
#endif 
	0x01, 0x00, 0x00, 0x00, 
	0x00, 0x52, 0x44, 0x44, 
	0xFF, 0xFF, 0x00, 0x00, 
	0x02, 0x00, 0x00, 0x00, 
	0xB8, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x02, 0x00, 0x00, 0x00, 
	0x14, 0x05, 0x00, 0x00, 
	0xA4, 0x01, 0x00, 0x00, 
	0x60, 0xAE, 0x0A, 0x00, 
	0x76, 0x02, 0x00, 0x00, 
	0x40, 0x01, 0x00, 0x00, 
	0x58, 0x98, 0x00, 0x00, 
	0x78, 0x05, 0x00, 0x00, 
	0x4B, 0x00, 0x00, 0x00, 
	0x4B, 0x00, 0x00, 0x00, 
	0x0E, 0x01, 0x00, 0x00, 
	0x64, 0x00, 0x00, 0x00, 
	0x96, 0x00, 0x00, 0x00, 
	0x03, 0x00, 0x00, 0x00, 
	0xF0, 0x00, 0x00, 0x00, 
	0x02, 0x00, 0x00, 0x00, 
	0x0E, 0x00, 0x00, 0x00, 
#ifdef BOOT_PRE_SILICON
	0x09, 0x00, 0x00, 0x00, 
	0x08, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
#else
	0x0A, 0x00, 0x00, 0x00, 
	0x08, 0x00, 0x00, 0x00, 
	0x0E, 0x00, 0x00, 0x00, 
	0x0A, 0x00, 0x00, 0x00, 
	0x08, 0x00, 0x00, 0x00, 
#endif /*BOOT_PRE_SILICON*/
	0x20, 0x00, 0x00, 0x00, 
	0x04, 0x00, 0x00, 0x00, 
	0x03, 0x00, 0x00, 0x00, 
	0xF0, 0x00, 0x00, 0x00, 
	0xF4, 0x01, 0x00, 0x00, 
	0x4B, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x05, 0x00, 0x00, 0x00, 
	0x80, 0x84, 0x1E, 0x00, 
	0x10, 0x27, 0x00, 0x00, 
	0x02, 0x00, 0x00, 0x00, 
	0x10, 0x0E, 0x00, 0x00, 
	0x84, 0x03, 0x00, 0x00, 
	0x10, 0x27, 0x00, 0x00, 
	0x05, 0x00, 0x00, 0x00, 
	0x96, 0x00, 0x00, 0x00, 
	0xF4, 0x01, 0x00, 0x00, 
	0x80, 0xA2, 0x00, 0x00, 
	0x00, 0x20, 0x00, 0x00, 
	0x19, 0x00, 0x00, 0x00, 
	0x37, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 

};

/**
	cdt table size
*/
uint32 config_data_table_size = 419;
