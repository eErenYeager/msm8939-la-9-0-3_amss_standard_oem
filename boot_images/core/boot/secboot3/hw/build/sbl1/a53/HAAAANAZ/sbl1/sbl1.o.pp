#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/sbl1/sbl1.s"
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                              SBL1
;
; GENERAL DESCRIPTION
;   This file bootstraps the processor. The Start-up Primary Bootloader
;   (SBL1) performs the following functions:
;
;      - Set up the hardware to continue boot process.
;      - Initialize DDR memory
;      - Load Trust-Zone OS
;      - Load RPM firmware
;      - Load APPSBL and continue boot process
;
;   The SBL1 is written to perform the above functions with optimal speed.
;   It also attempts to minimize the execution time and hence reduce boot time.
;
; Copyright 2014 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                           EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; $Header: 
;
; when       who     what, where, why
; --------   ---     --------------------------------------------------------
; 07/14/14   lm      Added sbl1_external_abort_enable funtion
; 07/01/14   sk      Added sbl_save_regs function
; 05/01/14   ck      Added logic to assign stacks based on processor mode
; 03/19/14   ck      Fixed stack base issue.  Now using proper address which is "Limit" of SBL1_STACK 
; 03/07/14   ck      Removed -4 logic from check_for_nesting as bear SBL has its own vector table
; 03/03/14   ck      Updated vector table with branches as VBAR is being used in Bear family 
; 11/15/12   dh      Add boot_read_l2esr for 8974
; 08/31/12   dh      Correct the stack base in check_for_nesting, remove unused code
; 07/16/12   dh      Remove watchdog reset code
; 02/06/12   dh      Update start up code
; 01/31/12   dh      Initial revision for 8974
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


;============================================================================
;
;                            MODULE INCLUDES
;
;============================================================================
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_msm.h"














 

















 






 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"




 
















 




















 

#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/hwio/msm8916/msmhwiobase.h"




 



 


























 



 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 






#line 46 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"



 




#line 67 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 87 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 107 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 121 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 139 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"



 




#line 160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 168 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 178 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 192 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 206 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 220 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 230 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 238 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 252 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"



 




#line 287 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 315 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 329 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 413 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 447 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 461 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 489 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 503 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 517 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 531 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 573 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 615 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 691 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 705 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 719 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 733 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 775 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 851 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 865 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 907 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 921 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 935 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 977 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1019 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1053 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1067 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1081 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1117 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1133 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1153 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1169 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1201 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1211 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1243 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1257 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1273 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1289 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1305 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1321 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1341 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1383 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1419 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1437 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1501 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1535 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1551 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1571 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1591 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1669 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1685 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1705 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1725 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1757 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1777 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1793 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1825 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1841 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1857 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1873 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1889 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1919 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1975 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 1991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2011 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2027 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2057 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2097 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2143 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2183 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2213 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2241 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2267 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2285 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2299 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2327 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2353 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2383 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2397 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2407 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2435 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2449 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2481 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2501 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2517 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2537 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2573 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2589 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2609 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2625 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2645 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2661 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2697 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2755 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2769 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2797 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2813 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2855 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2873 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2887 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2901 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2915 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2929 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2961 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 2987 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3005 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3019 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3033 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3061 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3077 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3093 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3119 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3137 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3151 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3179 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3209 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3225 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3251 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3297 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3311 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3325 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3341 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3357 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3383 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3415 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3429 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3457 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3473 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3489 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3515 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3547 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3561 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3575 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3589 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3621 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3647 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3665 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3679 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3693 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3707 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3727 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3775 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3795 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3813 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3849 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3865 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3879 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3897 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3911 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3931 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 3987 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4015 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4057 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4089 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4107 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4117 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4145 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4161 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4179 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4215 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4231 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4245 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4281 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4295 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4321 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4341 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4367 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4383 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4399 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4419 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4435 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4449 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4485 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4511 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4565 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4591 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4625 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4641 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4657 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4673 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4689 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4705 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4721 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4771 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4811 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4821 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4839 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4855 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4871 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4887 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4903 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4919 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4933 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4979 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 4995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5011 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5031 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5067 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5087 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5103 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5125 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5141 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5151 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5171 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5187 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5207 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5223 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5243 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5259 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5275 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5285 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5337 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5357 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5373 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5389 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5407 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5417 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5431 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5447 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5461 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5477 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5491 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5537 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5551 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5567 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5581 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5597 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5611 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5627 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5641 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5657 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5671 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5687 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5701 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5717 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5731 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5747 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5791 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5811 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5835 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5859 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5883 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5907 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5931 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5955 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 5979 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6003 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6027 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6051 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6061 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6085 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6103 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6127 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6147 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6167 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6183 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6197 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6211 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6225 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6239 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6253 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6267 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6281 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6295 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6309 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6337 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6351 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6365 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6379 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6407 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6421 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6435 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6471 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6491 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6527 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6575 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6615 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6703 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6771 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6811 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6859 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6899 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6919 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 6967 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7007 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7055 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7095 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7163 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7251 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7291 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7311 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7407 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7427 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7475 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7523 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7543 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7591 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7619 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7665 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7679 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7695 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7717 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7731 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7745 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7763 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7779 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7791 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7821 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7837 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7847 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7863 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7889 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7907 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7921 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7935 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7949 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7965 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 7991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8023 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8037 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8051 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8067 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8093 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8111 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8125 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8139 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8153 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8171 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8213 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8241 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8255 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8299 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8315 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8331 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8347 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8363 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8409 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8425 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8441 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8457 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8473 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8489 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8531 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8563 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8577 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8591 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8675 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8695 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8721 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8747 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8765 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8779 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8793 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8807 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8827 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8843 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8863 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8879 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8899 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8915 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8935 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8951 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 8965 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9055 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9097 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9113 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9129 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9145 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9181 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9195 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9211 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9231 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9261 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9277 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9297 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9327 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9343 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9373 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9389 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9403 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9419 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9449 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9485 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9499 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9515 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9531 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9545 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9561 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9575 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9591 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9621 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9635 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9651 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9695 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9709 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9767 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9827 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9841 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9855 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9869 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9883 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9899 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9925 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9943 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9957 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9971 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9985 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 9999 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10015 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10059 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10073 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10087 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10131 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10189 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10217 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10231 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10261 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10277 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10291 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10311 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10325 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10351 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10387 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10445 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10471 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10487 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10513 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10569 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10585 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10599 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10643 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10669 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10695 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10711 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10727 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10753 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10767 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10793 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10813 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10829 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10843 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10887 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10913 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10959 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10973 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 10999 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11017 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11031 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11045 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11059 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11079 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11099 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11125 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11143 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11171 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11261 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11275 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11289 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11307 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11333 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11351 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11395 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11409 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11423 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11437 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11501 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11519 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11537 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11563 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11581 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11595 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11609 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11623 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11641 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11667 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11685 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11699 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11713 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11727 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11745 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"



 




#line 11766 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11780 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"



 




#line 11797 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"



 




#line 11818 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11832 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11848 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11862 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11882 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11896 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11910 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11940 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11954 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11968 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11982 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 11996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12010 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12024 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12038 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12052 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12094 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12108 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12136 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12164 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12186 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12196 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12206 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12216 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12250 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12262 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12300 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12310 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12320 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12332 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12346 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12362 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12376 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12388 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12402 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12414 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12426 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"



 




#line 12451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12467 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12481 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12561 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12615 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12635 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12651 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12665 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12685 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12701 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12717 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12767 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12821 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12837 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12853 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12923 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 12957 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13013 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13037 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13051 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13091 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13106 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13121 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13136 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13151 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13171 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13213 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13235 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13250 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13299 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13347 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13371 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13429 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13499 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13544 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13559 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13573 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13589 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13606 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13623 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13639 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13653 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13668 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13683 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13699 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13713 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13727 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13755 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13769 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13797 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13811 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13825 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13839 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13853 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13864 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13872 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13886 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13898 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13909 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13923 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13937 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13957 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13975 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13987 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 13997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14015 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14025 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14036 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14054 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14066 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14098 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14110 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14124 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14136 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14146 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14168 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14218 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14268 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14284 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14296 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14306 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14322 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14334 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14346 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14392 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14466 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14532 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14562 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14614 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14634 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14644 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14680 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14691 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14702 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14713 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14724 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14740 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14760 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14776 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14792 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14803 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14816 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14826 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14838 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14858 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14878 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14898 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14916 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14948 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 14980 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15010 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15036 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15058 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15068 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15093 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15106 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15118 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15128 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15139 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15162 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15172 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15192 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15202 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15212 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15222 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15232 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15242 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15252 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15262 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15272 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15297 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15363 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15445 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15475 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15511 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15557 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15619 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15631 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15651 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15670 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15688 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15702 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15738 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15760 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15776 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15794 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15830 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15844 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15858 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15868 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15880 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15896 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15910 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"



 




#line 15933 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15949 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15965 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15981 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 15995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16011 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16025 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16055 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16085 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16159 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16221 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16237 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16257 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16273 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16293 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16307 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16365 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16427 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16479 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16499 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16555 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16615 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16629 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16643 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16653 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16673 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16688 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16702 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16716 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16770 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16824 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16874 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16884 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16898 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16932 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16946 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16962 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16980 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 16996 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17010 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17024 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17038 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17064 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17078 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17136 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17152 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17168 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17196 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17210 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17238 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17252 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17266 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17280 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17294 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17308 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17324 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17338 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17354 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17368 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17382 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17396 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17410 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17424 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17438 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17452 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17466 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17480 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17494 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17508 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17522 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17536 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17550 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17567 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17584 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17609 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17634 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17648 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17664 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17700 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17740 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17780 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17820 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17834 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17848 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17858 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17868 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17882 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17892 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17902 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17912 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17936 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17946 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17960 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17970 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17980 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 17994 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18004 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18014 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18028 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18038 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18048 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18062 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18072 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18082 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18096 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18106 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18120 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18134 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18148 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18162 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"

#line 18182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/msmhwioreg.h"


#line 42 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_msm.h"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"




 









 









  



 







 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"




 







 









  



 




 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/dal/HALcomdef.h"





























 




 
#line 117 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/dal/HALcomdef.h"



#line 34 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"





 




  







 









 








 





  













 
























 















 





























 


















 







































 


































 
#line 265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"
 











 


 



   


 





 
#line 330 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"

#line 355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"








 






 
#line 377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"


#line 406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 438 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"

 



#line 39 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"





 










 


 






 









#line 91 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"






 
#line 130 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"









 


























 
#line 174 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"


 



#line 43 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_msm.h"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"













 





















 





 
#line 202 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"






 
















 
#line 232 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"


#line 44 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_msm.h"





 

 




 



 




 



 





 





























#line 52 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/sbl1/sbl1.s"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/boot_target.h"















 

































 





 




#line 1 "./custhaaaanaza.h"






 

#line 1 "./targhaaaanaza.h"






 

#line 125 "./targhaaaanaza.h"




#line 11 "./custhaaaanaza.h"











#line 62 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/boot_target.h"



 





 




#line 101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/boot_target.h"






 






  


 






 






 





 









 




 







 









 






 


  




 





 





 




 




 




 





 




 




 




 




 




 




                                 


                                 
                                 


                                 
                               


                                 
                               


                                 
 


                                 
 


                             
                                 



 






 





                             


                             
#line 299 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/boot_target.h"




  
 






 



                              


 



   


 


 




 


#line 53 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/sbl1/sbl1.s"

;============================================================================
;
;                             MODULE DEFINES
;
;============================================================================
;
Mode_SVC                EQU    0x13
Mode_ABT                EQU    0x17
Mode_UND                EQU    0x1b
Mode_USR                EQU    0x10
Mode_FIQ                EQU    0x11
Mode_IRQ                EQU    0x12
Mode_SYS                EQU    0x1F

I_Bit                   EQU    0x80
F_Bit                   EQU    0x40
A_Bit					EQU    0x100

;============================================================================
;
;                             MODULE IMPORTS
;
;============================================================================

    ; Import the external symbols that are referenced in this module.
    IMPORT |Image$$SBL1_SVC_STACK$$ZI$$Limit|
    IMPORT |Image$$SBL1_UND_STACK$$ZI$$Limit|
    IMPORT |Image$$SBL1_ABT_STACK$$ZI$$Limit|
    IMPORT boot_undefined_instruction_c_handler
    IMPORT boot_swi_c_handler
    IMPORT boot_prefetch_abort_c_handler
    IMPORT boot_data_abort_c_handler
    IMPORT boot_reserved_c_handler
    IMPORT boot_irq_c_handler
    IMPORT boot_fiq_c_handler
    IMPORT boot_nested_exception_c_handler
    IMPORT sbl1_main_ctl
    IMPORT boot_crash_dump_regs_ptr

;============================================================================
;
;                             MODULE EXPORTS
;
;============================================================================

    ; Export the external symbols that are referenced in this module.
    EXPORT sbl_loop_here
    EXPORT boot_read_l2esr
   
    ; Export the symbols __main and _main to prevent the linker from
    ; including the standard runtime library and startup routine.
    EXPORT   __main
    EXPORT   _main
    EXPORT  sbl_save_regs
    EXPORT  sbl1_external_abort_enable
;============================================================================
;
;                             MODULE DATA AREA
;
;============================================================================

;
; Area that defines the register data structure
;
    AREA    SAVE_REGS, DATA

arm_core_t RN     r7 
           MAP    0,arm_core_t

svc_r0     FIELD  4
svc_r1     FIELD  4
svc_r2     FIELD  4
svc_r3     FIELD  4
svc_r4     FIELD  4
svc_r5     FIELD  4
svc_r6     FIELD  4
svc_r7     FIELD  4
svc_r8     FIELD  4
svc_r9     FIELD  4
svc_r10    FIELD  4
svc_r11    FIELD  4
svc_r12    FIELD  4
svc_sp     FIELD  4
svc_lr     FIELD  4
svc_spsr   FIELD  4
svc_pc     FIELD  4
cpsr       FIELD  4
sys_sp     FIELD  4
sys_lr     FIELD  4
irq_sp     FIELD  4
irq_lr     FIELD  4
irq_spsr   FIELD  4
abt_sp     FIELD  4
abt_lr     FIELD  4
abt_spsr   FIELD  4
udf_sp     FIELD  4
udf_lr     FIELD  4
udf_spsr   FIELD  4
fiq_r8     FIELD  4
fiq_r9     FIELD  4
fiq_r10    FIELD  4
fiq_r11    FIELD  4
fiq_r12    FIELD  4
fiq_sp     FIELD  4
fiq_lr     FIELD  4
fiq_spsr   FIELD  4

    PRESERVE8
    AREA    SBL1_VECTORS, CODE, READONLY, ALIGN=4
    CODE32
unused_reset_vector
    B     0x00000000
undefined_instruction_vector
    B     sbl1_undefined_instruction_nested_handler
swi_vector
    B     boot_swi_c_handler
prefetch_abort_vector
    B     sbl1_prefetch_abort_nested_handler
data_abort_vector
    B     sbl1_data_abort_nested_handler
reserved_vector
    B     boot_reserved_c_handler
irq_vector
    B     boot_irq_c_handler
fiq_vector
    B     boot_fiq_c_handler


;============================================================================
; Qualcomm SECONDARY BOOT LOADER 1 ENTRY POINT
;============================================================================

    AREA  SBL1_ENTRY, CODE, READONLY, ALIGN=4
    CODE32
    ENTRY
    
__main
_main

;============================================================================
;   We contiue to disable interrupt and watch dog until we jump to SBL3
;============================================================================
sbl1_entry

    ;Change to Supervisor Mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Save the passing parameter from PBL
    mov     r7, r0

    ; Set VBAR (Vector Base Address Register) to SBL vector table
    ldr     r0, =0x08005000
    MCR     p15, 0, r0, c12, c0, 0
	
    ; Setup the supervisor mode stack
    ldr     r0, =|Image$$SBL1_SVC_STACK$$ZI$$Limit|
    mov     r13, r0

    ; Switch to IRQ
    msr     CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F_Bit
    mov     r13, r0

    ; Switch to FIQ
    msr     CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F_Bit
    mov     r13, r0	
	
    ; Switch to undefined mode and setup the undefined mode stack
    msr     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
    ldr     r0, =|Image$$SBL1_UND_STACK$$ZI$$Limit|
    mov     r13, r0

    ; Switch to abort mode and setup the abort mode stack
    msr     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
    ldr     r0, =|Image$$SBL1_ABT_STACK$$ZI$$Limit|
    mov     r13, r0

    ; Return to supervisor mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Restore the passing parameter
    mov     r0, r7
    
    ; ------------------------------------------------------------------
    ; Call functions external to perform SBL1 function.
    ; It should never return.
    ; ------------------------------------------------------------------
    ldr    r5, =sbl1_main_ctl
    blx    r5

    ; For safety
    bl loophere  ; never returns, keep lr in r14 for debug


;============================================================================
; sbl_save_regs
;
; PROTOTYPE
;   void sbl_save_regs();
;
; ARGS
;   None
;
; DESCRIPTION
;   Configure VBAR, vector table base register.
;   
;============================================================================    
sbl_save_regs	
    ; Save CPSR
    stmfd   sp!, {r6}
    mrs     r6, CPSR

    stmfd   sp!, {r7}

    ; Switch to SVC mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Capture User Mode r0-r14 (no SPSR)
    ; Registers are stored in svc structure for backwards compatibility
    ldr     arm_core_t,=boot_crash_dump_regs_ptr
    ldr     arm_core_t, [r7]
    str     r0,  svc_r0
    str     r1,  svc_r1
    str     r2,  svc_r2
    str     r3,  svc_r3
    str     r4,  svc_r4
    str     r5,  svc_r5
    ; Store r6 later after restoring it from the stack
    ; Store r7 later after restoring it from the stack
    str     r8,  svc_r8
    str     r9,  svc_r9
    str     r10, svc_r10
    str     r11, svc_r11    
    str     r12, svc_r12
    str     r14, svc_lr

    ; Store SP value
    str     sp, svc_sp

    ; Store SPSR
    mrs     r0, SPSR
    str     r0, svc_spsr

    ; Store the PC for restoration later
    ldr     r0, =sbl_save_regs
    str     r0, svc_pc

    ; Save SYS mode registers
    msr     CPSR_c, #Mode_SYS:OR:I_Bit:OR:F_Bit
    str     r13, sys_sp
    str     r14, sys_lr

    ; Save IRQ mode registers
    msr     CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F_Bit
    str     r13, irq_sp
    str     r14, irq_lr
    mrs     r0, SPSR
    str     r0, irq_spsr    

    ; Save ABT mode registers
    msr     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
    str     r13, abt_sp
    str     r14, abt_lr
    mrs     r0, SPSR
    str     r0, abt_spsr  

    ; Save UND mode registers
    msr     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
    str     r13, udf_sp
    str     r14, udf_lr
    mrs     r0, SPSR
    str     r0, udf_spsr  

    ; Save FIQ mode registers
    msr     CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F_Bit
    str     r8,  fiq_r8
    str     r9,  fiq_r9
    str     r10, fiq_r10
    str     r11, fiq_r11    
    str     r12, fiq_r12    
    str     r13, fiq_sp
    str     r14, fiq_lr
    mrs     r0, SPSR
    str     r0, fiq_spsr  

    ; Switch back to original mode using r6 which holds the cpsr
    msr     CPSR_c, r6

    ; Store CPSR
    str     r6, cpsr
    
    ; Restore r7 value 
    ldmfd   sp!, {r2}
    str     r2, svc_r7

    ; Restore r6 value
    ldmfd   sp!, {r1}
    str     r1, svc_r6
    mov     r6, r1
    mov     r7, r2	

    ; Finished so return    
    bx lr

;======================================================================
; Called by sbl1_error_handler only. We clean up the registers and loop
; here until JTAG is connected.
;======================================================================
sbl_loop_here
    mov r0,#0
    mov r1,#0
    mov r2,#0
    mov r3,#0
    mov r4,#0
    mov r5,#0
    mov r6,#0
    mov r7,#0
    mov r8,#0
    mov r9,#0
    mov r10,#0
    mov r11,#0
    mov r12,#0
loophere
    b loophere


;======================================================================
; SBL1 exception handlers that can have nested calls to them.  These
; handlers check for nesting and if it is the first exception they
; call a "C" exception handler that calls the SBL1 error handler.
; If it is a nested exception, the "C" exception handler is not
; re-entered and the JTAG interface is enabled immediately. Nesting
; is only a concern for undefined instruction and abort exceptions.
; Note, a separate exception handler is used for each exception to
; provide additional debug information (see sbl1_error_handler.c for
; more information).
;======================================================================

	
sbl1_undefined_instruction_nested_handler
    ldr r5,=boot_undefined_instruction_c_handler
    b   check_for_nesting

sbl1_prefetch_abort_nested_handler
    ldr r5,=boot_prefetch_abort_c_handler
    b   check_for_nesting

sbl1_data_abort_nested_handler
    ldr r5,=boot_data_abort_c_handler
    b   check_for_nesting

;======================================================================
; Checks for nested exceptions and then calls the "C" exception
; handler pointed to by R5 if this is the first time this exception
; has occurred, otherwise it calls the "C" nested exception handler
; that just enables JTAG debug access.  The mode stack pointer is used
; to determine if a nested exception or a second abort exception has
; occurred.  This is accomplished by comparing the mode stack pointer
; to the top of the stack that was initially assigned to the stack.
; If they are equal, it is a first time exception.
;======================================================================
check_for_nesting

    ; Initial stack base depends on the current processor mode
    ; Mode will either be ABT or UND.  Load proper stack base.
    mrs r7, cpsr
    and r7, r7, #Mode_SYS ; Use Mode_SYS for mode bitmask as all bits are high
	
    ldreq r6,=|Image$$SBL1_ABT_STACK$$ZI$$Limit|
    cmp r7, #Mode_UND
    ldreq r6,=|Image$$SBL1_UND_STACK$$ZI$$Limit|

    mov r7, r13                            ; Save current stack ptr
    cmp r6, r7                             ; Compare initial and actual
    blxeq r5                               ; First time exception
    ldr r5,=boot_nested_exception_c_handler; This is a nested exception
    blx r5

boot_read_l2esr
    mov r1, #0x204    ;Indirect address of L2ESR
    isb
    mcr p15,3,r1,c15,c0,6 ;Write the L2CPUCPSELR with the indirect address of the L2ESR
    isb
    mrc p15,3,r0,c15,c0,7 ;store L2ESR to r0
    isb
    bx lr
    
; void sbl1_external_abort_enable(uint32 flags)
sbl1_external_abort_enable FUNCTION
    and     r0, r0, #F_Bit:OR:I_Bit:OR:A_Bit 	; Only care about A/I/F bits.
    mrs     r1, cpsr                            ; Read the status register.
    bic     r1, r1, r0                          ; Clear requested A/I/F bits
    msr     cpsr_cx, r1                         ; Write control & extension field
    bx      lr
    ENDFUNC	
    END
