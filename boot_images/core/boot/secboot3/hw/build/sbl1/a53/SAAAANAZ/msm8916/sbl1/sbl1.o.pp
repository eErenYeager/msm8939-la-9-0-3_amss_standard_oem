#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/sbl1/sbl1.s"
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                              SBL1
;
; GENERAL DESCRIPTION
;   This file bootstraps the processor. The Start-up Primary Bootloader
;   (SBL1) performs the following functions:
;
;      - Set up the hardware to continue boot process.
;      - Initialize DDR memory
;      - Load Trust-Zone OS
;      - Load RPM firmware
;      - Load APPSBL and continue boot process
;
;   The SBL1 is written to perform the above functions with optimal speed.
;   It also attempts to minimize the execution time and hence reduce boot time.
;
; Copyright 2014 by Qualcomm Technologies, Incorporated.All Rights Reserved.
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
;
;                           EDIT HISTORY FOR FILE
;
; This section contains comments describing changes made to the module.
; Notice that changes are listed in reverse chronological order.
;
; $Header: 
;
; when       who     what, where, why
; --------   ---     --------------------------------------------------------
; 07/14/14   lm      Added sbl1_external_abort_enable funtion
; 07/01/14   sk      Added sbl_save_regs function
; 05/01/14   ck      Added logic to assign stacks based on processor mode
; 03/19/14   ck      Fixed stack base issue.  Now using proper address which is "Limit" of SBL1_STACK 
; 03/07/14   ck      Removed -4 logic from check_for_nesting as bear SBL has its own vector table
; 03/03/14   ck      Updated vector table with branches as VBAR is being used in Bear family 
; 11/15/12   dh      Add boot_read_l2esr for 8974
; 08/31/12   dh      Correct the stack base in check_for_nesting, remove unused code
; 07/16/12   dh      Remove watchdog reset code
; 02/06/12   dh      Update start up code
; 01/31/12   dh      Initial revision for 8974
;*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*


;============================================================================
;
;                            MODULE INCLUDES
;
;============================================================================
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_msm.h"














 

















 






 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"




 

















 


























 

#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/hwio/msm8936/msmhwiobase.h"




 



 


























 



 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 







 






#line 53 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 




#line 80 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 112 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 164 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 180 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 194 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 210 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 240 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 258 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 302 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 318 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 332 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 346 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 360 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 374 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 384 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 394 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 




#line 415 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 435 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 455 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 487 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 




#line 508 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 516 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 526 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 540 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 554 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 568 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 578 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 586 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 600 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 




#line 635 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 719 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 795 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 837 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 851 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 865 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 879 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 921 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 963 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1011 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1039 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1053 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1067 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1081 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1123 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1213 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1241 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1255 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1325 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1367 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1415 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1457 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1471 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1485 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1527 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1569 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1603 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1617 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1645 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1659 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1673 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1687 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1729 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1781 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1827 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1841 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1869 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1883 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1897 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1911 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 1995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2043 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2085 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2099 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2113 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2155 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2207 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2253 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2267 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2281 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2333 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2353 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2369 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2389 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2405 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2421 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2437 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2453 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2479 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2495 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2509 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2525 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2541 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2557 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2573 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2593 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2635 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2655 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2671 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2689 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2715 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2735 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2753 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2773 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2803 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2823 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2843 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2859 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2885 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2901 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2921 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2937 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2957 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2977 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 2993 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3045 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3061 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3077 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3093 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3109 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3125 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3141 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3171 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3197 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3243 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3263 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3279 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3293 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3309 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3349 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3379 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3395 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3421 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3439 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3453 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3467 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3481 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3525 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3539 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3567 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3581 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3625 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3639 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3653 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3667 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3693 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3777 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3795 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3823 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3837 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3863 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3907 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3917 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3959 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3975 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 3991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4011 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4027 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4063 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4083 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4099 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4119 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4155 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4171 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4191 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4207 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4251 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4279 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4293 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4307 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4339 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4365 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4383 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4397 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4411 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4425 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4439 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4455 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4471 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4497 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4515 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4543 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4557 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4571 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4587 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4603 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4629 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4647 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4661 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4675 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4689 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4703 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4719 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4735 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4779 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4793 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4807 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4821 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4835 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4851 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4867 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4911 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4925 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4953 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4967 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4983 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 4999 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5025 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5043 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5057 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5085 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5099 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5131 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5189 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5217 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5237 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5251 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5271 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5285 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5305 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5323 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5339 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5375 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5389 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5407 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5421 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5441 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5455 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5497 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5525 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5539 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5557 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5567 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5581 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5599 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5617 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5627 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5655 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5671 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5689 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5709 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5725 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5755 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5775 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5791 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5805 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5831 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5851 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5877 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5893 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5909 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5929 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5945 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5959 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5979 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 5995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6021 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6039 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6059 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6075 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6151 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6167 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6183 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6215 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6231 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6263 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6279 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6293 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6329 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6353 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6363 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6381 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6397 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6413 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6429 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6445 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6461 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6475 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6537 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6573 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6589 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6609 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6629 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6665 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6685 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6701 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6775 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6803 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6823 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6851 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6875 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6897 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6911 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6963 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6985 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 6999 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7013 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7027 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7043 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7059 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7069 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7089 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7105 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7121 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7137 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7157 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7173 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7209 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7245 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7281 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7297 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7307 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7325 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7345 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7379 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7395 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7411 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7429 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7439 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7453 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7499 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7513 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7543 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7559 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7573 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7589 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7603 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7619 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7679 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7693 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7709 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7739 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7753 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7769 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7813 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7857 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7881 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7929 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7963 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 7987 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8011 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8035 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8059 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8069 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8093 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8111 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8159 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8179 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8189 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8205 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8219 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8233 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8247 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8261 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8275 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8289 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8331 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8345 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8359 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8373 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8387 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8415 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8429 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8457 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8471 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8485 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8501 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8541 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8557 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8585 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8725 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8769 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8797 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8845 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8889 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8937 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 8981 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9057 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9149 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9221 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9361 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9405 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9481 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9557 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9605 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9653 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9729 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9743 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9757 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9771 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9803 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9855 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9869 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9883 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9901 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9917 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9929 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9963 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9979 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 9989 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10005 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10031 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10049 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10063 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10077 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10091 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10107 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10133 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10151 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10179 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10209 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10235 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10253 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10267 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10281 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10295 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10327 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10341 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10369 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10383 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10399 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10413 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10427 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10443 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10459 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10475 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10491 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10507 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10537 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10553 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10569 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10585 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10601 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10617 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10665 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10691 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10709 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10765 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10835 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10879 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10949 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10975 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 10995 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11021 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11047 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11065 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11079 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11093 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11107 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11133 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11151 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11179 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11213 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11249 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11285 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11321 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11337 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11357 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11373 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11409 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11423 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11467 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11487 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11513 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11545 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11571 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11587 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11603 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11619 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11635 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11651 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11671 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11687 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11701 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11717 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11753 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11767 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11803 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11819 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11833 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11849 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11865 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11879 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11895 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11909 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11925 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11939 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11955 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11975 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 11991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12005 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12021 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12037 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12051 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12067 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12081 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12097 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12111 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12127 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12147 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12163 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12177 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12209 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12223 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12239 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12253 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12283 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12299 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12313 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12329 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12373 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12387 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12401 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12415 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12429 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12445 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12461 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12487 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12519 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12533 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12547 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12561 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12577 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12603 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12621 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12635 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12693 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12719 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12765 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12779 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12793 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12835 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12853 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12867 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12881 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12895 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12909 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12925 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12951 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12969 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12983 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 12997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13011 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13025 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13041 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13055 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13085 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13105 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13119 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13145 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13181 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13195 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13239 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13281 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13307 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13327 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13343 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13363 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13379 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13437 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13463 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13489 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13521 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13547 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13561 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13587 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13607 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13623 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13637 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13681 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13707 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13733 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13753 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13769 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13809 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13827 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13841 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13855 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13869 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13889 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13931 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13949 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13963 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13977 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 13991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14009 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14035 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14053 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14067 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14081 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14095 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14113 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14127 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14153 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14171 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14185 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14199 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14213 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14231 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14257 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14275 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14289 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14335 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14361 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14379 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14393 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14407 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14421 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14439 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14483 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14509 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14527 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14541 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14555 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14569 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14597 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14625 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14643 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14661 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14679 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14697 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14723 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14741 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14755 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14769 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14783 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14827 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14845 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14859 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14873 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14887 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 




#line 14926 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14940 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 




#line 14957 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 




#line 14978 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 14992 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15008 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15022 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15042 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15056 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15070 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15086 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15136 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15150 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15164 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15178 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15192 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15206 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15220 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15234 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15248 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15262 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15304 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15318 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15332 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15346 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15357 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15368 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15378 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15388 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15398 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15432 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15444 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15458 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15472 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15482 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15492 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15502 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15514 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15528 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15544 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15558 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15570 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15584 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 




#line 15633 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15649 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15677 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15731 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15785 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15805 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15821 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15835 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15855 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15871 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15887 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15937 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 15991 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16007 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16023 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16095 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16191 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16215 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16229 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16271 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16286 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16301 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16316 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16331 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16351 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16373 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16395 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16417 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16432 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16465 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16481 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16529 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16549 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16573 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16603 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16639 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16673 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16707 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16722 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16737 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16751 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16767 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16784 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16801 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16831 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16846 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16861 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16877 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16891 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16905 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16919 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16933 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16947 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16961 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16975 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 16989 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17003 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17017 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17031 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17042 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17050 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17064 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17076 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17087 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17115 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17135 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17153 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17175 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17193 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17203 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17214 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17232 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17244 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17276 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17288 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17302 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17312 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17322 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17332 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17382 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17432 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17448 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17460 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17470 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17486 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17498 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17510 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17606 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17618 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17630 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17702 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17742 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17794 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17814 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17824 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17862 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17873 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17884 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17895 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17906 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17922 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17942 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17964 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17986 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 17997 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18010 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18020 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18032 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18056 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18080 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18100 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18124 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18154 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18190 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18224 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18258 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18269 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18280 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18290 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18302 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18315 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18328 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18340 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18350 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18361 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18372 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18384 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18394 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18404 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18414 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18424 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18434 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18444 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18464 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18474 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18484 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18494 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18519 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18587 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18623 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18675 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18695 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18705 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18743 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18789 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18839 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18851 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18863 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18883 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18902 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18920 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18934 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18970 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 18992 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19008 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19026 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19062 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19076 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19090 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19100 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19112 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19128 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19142 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 




#line 19165 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19181 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19197 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19213 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19227 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19243 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19257 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19273 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19287 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19303 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19317 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19333 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19391 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19453 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19469 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19489 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19505 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19525 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19539 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19597 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19659 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19675 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19695 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19711 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19731 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19787 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19847 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19861 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19875 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19889 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19899 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19909 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19919 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19934 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19952 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19966 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19980 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 19994 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20048 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20102 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20152 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20162 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20176 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20232 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20246 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20262 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20280 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20296 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20310 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20324 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20344 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20358 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20372 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20386 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20400 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20414 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20428 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20470 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20496 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20512 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20534 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20572 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20594 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20616 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20632 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20642 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20656 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20672 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20686 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20700 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20714 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20728 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20742 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20756 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20772 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20786 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20802 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20816 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20830 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20844 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20858 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20872 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20886 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20900 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20914 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20928 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20942 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20956 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20970 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20984 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 20998 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21012 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21029 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21046 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21071 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21096 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21122 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21138 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21182 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21196 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21212 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21248 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21288 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21328 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21368 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21382 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21396 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21416 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21430 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21440 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21450 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21460 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21474 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21484 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21494 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21508 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21518 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21528 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21542 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21552 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21562 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21576 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21586 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21596 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21610 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21620 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21630 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21644 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21654 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21668 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21682 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21696 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21710 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21730 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"





 





#line 21761 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21782 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21803 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21824 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21845 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21866 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21887 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21908 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21929 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21950 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21971 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 21992 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22013 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22034 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22055 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22076 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22097 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22118 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22139 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22160 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22181 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22202 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22223 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22244 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22261 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22278 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22295 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22318 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22367 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22392 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22409 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22424 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22433 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22442 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22451 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22466 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22481 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22496 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22511 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22526 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22541 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22556 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22571 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22582 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22593 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22608 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22623 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22646 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22663 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22680 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22696 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22715 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22730 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22745 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22760 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"



 





#line 22777 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22806 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22817 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22834 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"

#line 22851 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/msmhwioreg.h"


#line 42 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_msm.h"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"




 









 









  



 







 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"




 







 









  



 




 
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/dal/HALcomdef.h"





























 




 
#line 117 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/dal/HALcomdef.h"



#line 34 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"





 




  







 









 








 





  













 
























 















 





























 


















 







































 


































 
#line 265 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"
 











 


 



   


 





 
#line 330 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"

#line 355 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"








 






 
#line 377 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"


#line 406 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 422 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 438 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"




 
#line 454 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/HALhwio.h"

 



#line 39 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"





 










 


 






 









#line 91 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"






 
#line 130 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"









 


























 
#line 174 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/api/systemdrivers/msmhwio.h"


 



#line 43 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_msm.h"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"













 





















 





 
#line 202 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"






 
















 
#line 232 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_error_handler.h"


#line 44 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/src/boot_msm.h"





 

 




 



 




 



 





 





























#line 52 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/sbl1/sbl1.s"
#line 1 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/boot_target.h"















 

































 





 




#line 1 "./custsaaaanaza.h"






 

#line 1 "./targsaaaanaza.h"






 

#line 126 "./targsaaaanaza.h"




#line 11 "./custsaaaanaza.h"











#line 62 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/boot_target.h"



 





 




#line 101 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/boot_target.h"






 






  


 






 






 





 









 




 







 









 






 


  




 





 





 




 




 




 





 




 




 




 




 




 




                                 


                                 
                                 


                                 
                               


                                 
                               


                                 
 


                                 
 


                             
                                 



 






 





                             


                             
#line 299 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8936/boot_target.h"




  
 






 



                              


 



   


 


 




 


#line 53 "/local/mnt/workspace/CRMBuilds/BOOT.BF.3.0.c8-00023-M8936AAAAANAZB-1_20170728_035427/b/boot_images/core/boot/secboot3/hw/msm8916/sbl1/sbl1.s"

;============================================================================
;
;                             MODULE DEFINES
;
;============================================================================
;
Mode_SVC                EQU    0x13
Mode_ABT                EQU    0x17
Mode_UND                EQU    0x1b
Mode_USR                EQU    0x10
Mode_FIQ                EQU    0x11
Mode_IRQ                EQU    0x12
Mode_SYS                EQU    0x1F

I_Bit                   EQU    0x80
F_Bit                   EQU    0x40
A_Bit					EQU    0x100

;============================================================================
;
;                             MODULE IMPORTS
;
;============================================================================

    ; Import the external symbols that are referenced in this module.
    IMPORT |Image$$SBL1_SVC_STACK$$ZI$$Limit|
    IMPORT |Image$$SBL1_UND_STACK$$ZI$$Limit|
    IMPORT |Image$$SBL1_ABT_STACK$$ZI$$Limit|
    IMPORT boot_undefined_instruction_c_handler
    IMPORT boot_swi_c_handler
    IMPORT boot_prefetch_abort_c_handler
    IMPORT boot_data_abort_c_handler
    IMPORT boot_reserved_c_handler
    IMPORT boot_irq_c_handler
    IMPORT boot_fiq_c_handler
    IMPORT boot_nested_exception_c_handler
    IMPORT sbl1_main_ctl
    IMPORT boot_crash_dump_regs_ptr

;============================================================================
;
;                             MODULE EXPORTS
;
;============================================================================

    ; Export the external symbols that are referenced in this module.
    EXPORT sbl_loop_here
    EXPORT boot_read_l2esr
   
    ; Export the symbols __main and _main to prevent the linker from
    ; including the standard runtime library and startup routine.
    EXPORT   __main
    EXPORT   _main
    EXPORT  sbl_save_regs
    EXPORT  sbl1_external_abort_enable
;============================================================================
;
;                             MODULE DATA AREA
;
;============================================================================

;
; Area that defines the register data structure
;
    AREA    SAVE_REGS, DATA

arm_core_t RN     r7 
           MAP    0,arm_core_t

svc_r0     FIELD  4
svc_r1     FIELD  4
svc_r2     FIELD  4
svc_r3     FIELD  4
svc_r4     FIELD  4
svc_r5     FIELD  4
svc_r6     FIELD  4
svc_r7     FIELD  4
svc_r8     FIELD  4
svc_r9     FIELD  4
svc_r10    FIELD  4
svc_r11    FIELD  4
svc_r12    FIELD  4
svc_sp     FIELD  4
svc_lr     FIELD  4
svc_spsr   FIELD  4
svc_pc     FIELD  4
cpsr       FIELD  4
sys_sp     FIELD  4
sys_lr     FIELD  4
irq_sp     FIELD  4
irq_lr     FIELD  4
irq_spsr   FIELD  4
abt_sp     FIELD  4
abt_lr     FIELD  4
abt_spsr   FIELD  4
udf_sp     FIELD  4
udf_lr     FIELD  4
udf_spsr   FIELD  4
fiq_r8     FIELD  4
fiq_r9     FIELD  4
fiq_r10    FIELD  4
fiq_r11    FIELD  4
fiq_r12    FIELD  4
fiq_sp     FIELD  4
fiq_lr     FIELD  4
fiq_spsr   FIELD  4

    PRESERVE8
    AREA    SBL1_VECTORS, CODE, READONLY, ALIGN=4
    CODE32
unused_reset_vector
    B     0x00000000
undefined_instruction_vector
    B     sbl1_undefined_instruction_nested_handler
swi_vector
    B     boot_swi_c_handler
prefetch_abort_vector
    B     sbl1_prefetch_abort_nested_handler
data_abort_vector
    B     sbl1_data_abort_nested_handler
reserved_vector
    B     boot_reserved_c_handler
irq_vector
    B     boot_irq_c_handler
fiq_vector
    B     boot_fiq_c_handler


;============================================================================
; Qualcomm SECONDARY BOOT LOADER 1 ENTRY POINT
;============================================================================

    AREA  SBL1_ENTRY, CODE, READONLY, ALIGN=4
    CODE32
    ENTRY
    
__main
_main

;============================================================================
;   We contiue to disable interrupt and watch dog until we jump to SBL3
;============================================================================
sbl1_entry

    ;Change to Supervisor Mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Save the passing parameter from PBL
    mov     r7, r0

    ; Set VBAR (Vector Base Address Register) to SBL vector table
    ldr     r0, =0x08005000
    MCR     p15, 0, r0, c12, c0, 0
	
    ; Setup the supervisor mode stack
    ldr     r0, =|Image$$SBL1_SVC_STACK$$ZI$$Limit|
    mov     r13, r0

    ; Switch to IRQ
    msr     CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F_Bit
    mov     r13, r0

    ; Switch to FIQ
    msr     CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F_Bit
    mov     r13, r0	
	
    ; Switch to undefined mode and setup the undefined mode stack
    msr     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
    ldr     r0, =|Image$$SBL1_UND_STACK$$ZI$$Limit|
    mov     r13, r0

    ; Switch to abort mode and setup the abort mode stack
    msr     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
    ldr     r0, =|Image$$SBL1_ABT_STACK$$ZI$$Limit|
    mov     r13, r0

    ; Return to supervisor mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Restore the passing parameter
    mov     r0, r7
    
    ; ------------------------------------------------------------------
    ; Call functions external to perform SBL1 function.
    ; It should never return.
    ; ------------------------------------------------------------------
    ldr    r5, =sbl1_main_ctl
    blx    r5

    ; For safety
    bl loophere  ; never returns, keep lr in r14 for debug


;============================================================================
; sbl_save_regs
;
; PROTOTYPE
;   void sbl_save_regs();
;
; ARGS
;   None
;
; DESCRIPTION
;   Configure VBAR, vector table base register.
;   
;============================================================================    
sbl_save_regs	
    ; Save CPSR
    stmfd   sp!, {r6}
    mrs     r6, CPSR

    stmfd   sp!, {r7}

    ; Switch to SVC mode
    msr     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit

    ; Capture User Mode r0-r14 (no SPSR)
    ; Registers are stored in svc structure for backwards compatibility
    ldr     arm_core_t,=boot_crash_dump_regs_ptr
    ldr     arm_core_t, [r7]
    str     r0,  svc_r0
    str     r1,  svc_r1
    str     r2,  svc_r2
    str     r3,  svc_r3
    str     r4,  svc_r4
    str     r5,  svc_r5
    ; Store r6 later after restoring it from the stack
    ; Store r7 later after restoring it from the stack
    str     r8,  svc_r8
    str     r9,  svc_r9
    str     r10, svc_r10
    str     r11, svc_r11    
    str     r12, svc_r12
    str     r14, svc_lr

    ; Store SP value
    str     sp, svc_sp

    ; Store SPSR
    mrs     r0, SPSR
    str     r0, svc_spsr

    ; Store the PC for restoration later
    ldr     r0, =sbl_save_regs
    str     r0, svc_pc

    ; Save SYS mode registers
    msr     CPSR_c, #Mode_SYS:OR:I_Bit:OR:F_Bit
    str     r13, sys_sp
    str     r14, sys_lr

    ; Save IRQ mode registers
    msr     CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F_Bit
    str     r13, irq_sp
    str     r14, irq_lr
    mrs     r0, SPSR
    str     r0, irq_spsr    

    ; Save ABT mode registers
    msr     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
    str     r13, abt_sp
    str     r14, abt_lr
    mrs     r0, SPSR
    str     r0, abt_spsr  

    ; Save UND mode registers
    msr     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
    str     r13, udf_sp
    str     r14, udf_lr
    mrs     r0, SPSR
    str     r0, udf_spsr  

    ; Save FIQ mode registers
    msr     CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F_Bit
    str     r8,  fiq_r8
    str     r9,  fiq_r9
    str     r10, fiq_r10
    str     r11, fiq_r11    
    str     r12, fiq_r12    
    str     r13, fiq_sp
    str     r14, fiq_lr
    mrs     r0, SPSR
    str     r0, fiq_spsr  

    ; Switch back to original mode using r6 which holds the cpsr
    msr     CPSR_c, r6

    ; Store CPSR
    str     r6, cpsr
    
    ; Restore r7 value 
    ldmfd   sp!, {r2}
    str     r2, svc_r7

    ; Restore r6 value
    ldmfd   sp!, {r1}
    str     r1, svc_r6
    mov     r6, r1
    mov     r7, r2	

    ; Finished so return    
    bx lr

;======================================================================
; Called by sbl1_error_handler only. We clean up the registers and loop
; here until JTAG is connected.
;======================================================================
sbl_loop_here
    mov r0,#0
    mov r1,#0
    mov r2,#0
    mov r3,#0
    mov r4,#0
    mov r5,#0
    mov r6,#0
    mov r7,#0
    mov r8,#0
    mov r9,#0
    mov r10,#0
    mov r11,#0
    mov r12,#0
loophere
    b loophere


;======================================================================
; SBL1 exception handlers that can have nested calls to them.  These
; handlers check for nesting and if it is the first exception they
; call a "C" exception handler that calls the SBL1 error handler.
; If it is a nested exception, the "C" exception handler is not
; re-entered and the JTAG interface is enabled immediately. Nesting
; is only a concern for undefined instruction and abort exceptions.
; Note, a separate exception handler is used for each exception to
; provide additional debug information (see sbl1_error_handler.c for
; more information).
;======================================================================

	
sbl1_undefined_instruction_nested_handler
    ldr r5,=boot_undefined_instruction_c_handler
    b   check_for_nesting

sbl1_prefetch_abort_nested_handler
    ldr r5,=boot_prefetch_abort_c_handler
    b   check_for_nesting

sbl1_data_abort_nested_handler
    ldr r5,=boot_data_abort_c_handler
    b   check_for_nesting

;======================================================================
; Checks for nested exceptions and then calls the "C" exception
; handler pointed to by R5 if this is the first time this exception
; has occurred, otherwise it calls the "C" nested exception handler
; that just enables JTAG debug access.  The mode stack pointer is used
; to determine if a nested exception or a second abort exception has
; occurred.  This is accomplished by comparing the mode stack pointer
; to the top of the stack that was initially assigned to the stack.
; If they are equal, it is a first time exception.
;======================================================================
check_for_nesting

    ; Initial stack base depends on the current processor mode
    ; Mode will either be ABT or UND.  Load proper stack base.
    mrs r7, cpsr
    and r7, r7, #Mode_SYS ; Use Mode_SYS for mode bitmask as all bits are high
	
    ldreq r6,=|Image$$SBL1_ABT_STACK$$ZI$$Limit|
    cmp r7, #Mode_UND
    ldreq r6,=|Image$$SBL1_UND_STACK$$ZI$$Limit|

    mov r7, r13                            ; Save current stack ptr
    cmp r6, r7                             ; Compare initial and actual
    blxeq r5                               ; First time exception
    ldr r5,=boot_nested_exception_c_handler; This is a nested exception
    blx r5

boot_read_l2esr
    mov r1, #0x204    ;Indirect address of L2ESR
    isb
    mcr p15,3,r1,c15,c0,6 ;Write the L2CPUCPSELR with the indirect address of the L2ESR
    isb
    mrc p15,3,r0,c15,c0,7 ;store L2ESR to r0
    isb
    bx lr
    
; void sbl1_external_abort_enable(uint32 flags)
sbl1_external_abort_enable FUNCTION
    and     r0, r0, #F_Bit:OR:I_Bit:OR:A_Bit 	; Only care about A/I/F bits.
    mrs     r1, cpsr                            ; Read the status register.
    bic     r1, r1, r0                          ; Clear requested A/I/F bits
    msr     cpsr_cx, r1                         ; Write control & extension field
    bx      lr
    ENDFUNC	
    END
