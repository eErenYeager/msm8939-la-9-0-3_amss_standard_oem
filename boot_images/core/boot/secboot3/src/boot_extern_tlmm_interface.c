/*===========================================================================

                    BOOT EXTERN TLMM DRIVER DEFINITIONS

DESCRIPTION
  Contains wrapper definition for external tlmm drivers

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None
  
Copyright 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
============================================================================*/
/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.
    
$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/boot/secboot3/src/boot_extern_tlmm_interface.c#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $
    
when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/01/14   yps     Delete LED_GPIO_NUMBER macro and add gpio parameter into boot_gpio_enable_led function
06/19/14   yps     Add boot_gpio_enable_led to control led blink during sd dump
10/19/11   dh      Initial Creation.

===========================================================================*/

/*==========================================================================

                               INCLUDE FILES

===========================================================================*/
#include "gpio_boot.h"
/*===========================================================================
                      FUNCTION DECLARATIONS
===========================================================================*/ 


/*===========================================================================

  FUNCTION GPIO_INIT

  DESCRIPTION
        This function initializes the GPIOs for the TLMM configuration
        specified at boot time.

  PARAMETERS
    None.

  DEPENDENCIES
    None.

  RETURN VALUE
    None.

  SIDE EFFECTS
    It uses the stack

===========================================================================*/
boolean boot_gpio_init()
{
  return gpio_init();
}
/*===========================================================================

**  Function :  boot_gpio_enable_led

** ==========================================================================
*/
/*! 
 * @brief This function executes the GPIO control red led function
 * @return Error flag.
 *
 */ 
boolean boot_gpio_enable_led( boolean current_led_status,uint32 iLedGPIONumber)
{
    DalDeviceHandle* hTlmmDriver = NULL;
    DALResult eResult = DAL_ERROR;
    DALGpioSignalType gpio_config;
    /* Initialize the TLMM handle. */
    eResult = DAL_DeviceAttach(DALDEVICEID_TLMM, &hTlmmDriver);
    if(DAL_SUCCESS == eResult)
    {
     /*
  	   Config gpio func: generic I/O, dir:OUT , pull: No pull, driver strength:0  
         */
  	  gpio_config = DAL_GPIO_CFG_OUT(iLedGPIONumber,0x0,0x1,0x3,0x0,0x0);
  	/* 
  	 * Try to allocate or attach to shared memory.
  	 */
  	  eResult = DalTlmm_ConfigGpio(hTlmmDriver, gpio_config, DAL_TLMM_GPIO_ENABLE);
  	  eResult = DalTlmm_GpioOut(hTlmmDriver,gpio_config, (DALGpioValueType)current_led_status);
  
  	  if(DAL_SUCCESS == eResult)
  	  {
	  	  /* 
	  	   * Only need to initialize TLMM internal structures.	No need to
	  	   * maintain a handle.
	  	   */
	  	  eResult = DAL_DeviceDetach(hTlmmDriver);
  	   }
     }
  
     if(DAL_SUCCESS == eResult)
     {
  	   return(TRUE);
     }
     return(FALSE);
}

