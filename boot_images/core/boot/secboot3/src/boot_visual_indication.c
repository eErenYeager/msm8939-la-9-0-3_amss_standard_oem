
/*=============================================================================

                        Boot Visual Indication

GENERAL DESCRIPTION
  This file defines SBL visual indication functions

Copyright 2013 by Qualcomm Technologies, Inc.  All Rights Reserved.
=============================================================================*/


/*=============================================================================

                            EDIT HISTORY FOR MODULE

  This section contains comments describing changes made to the module.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/boot.bf/3.0.c8/boot_images/core/boot/secboot3/src/boot_visual_indication.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------------------
09/01/14   yps     Use platform ID and subtype to distinguish LED control GPIO of every HW board.
06/19/14   yps     Add gpio control LED function when sd dump
04/09/13   dh      Initial creation
=============================================================================*/

/*=============================================================================

                            INCLUDE FILES FOR MODULE

=============================================================================*/
#include "boot_visual_indication.h"
#include "boot_extern_pmic_interface.h"
#include "boot_extern_tlmm_interface.h"
#include "DDIPlatformInfo.h"
#include "DALDeviceId.h"

/*=========================================================================

                                 Macro Definitions
                                 
==========================================================================*/
#define DALPLATFORMINFO_SUBTYPE_QRD_T    0x05


static uint32 current_time = 0;
static uint32 previous_time = 0;
static boot_boolean current_led_status = FALSE;

/* The LED flash interval in microseconds*/
static uint32 LED_FLASH_INTERVAL_US = 500000;

/*The GPIO number control LED bink*/
static uint32 Led_GPIO_Number=8;

/*===========================================================================

**  Function :  boot_led_gpio_set

** ==========================================================================
*/
/*! 
 * @brief This function determin which GPIO control LED blinking
 * @return None.
 */
 void boot_led_gpio_set(void)
{
     /*To determin which GPIO control LED blink*/
    DalDeviceHandle* hPlatformInfo = NULL;    
    DalPlatformInfoPlatformInfoType pPlatformInfo;
    DalChipInfoIdType pChipInfo;
    DALResult eResult = DAL_ERROR;
    

    pChipInfo=DalChipInfo_ChipId();

    do
   	{
      eResult = DAL_PlatformInfoDeviceAttach(DALDEVICEID_PLATFORMINFO, &hPlatformInfo);
      if(eResult!=DAL_SUCCESS)
      {
  	    break;
      }
  
      eResult = DalPlatformInfo_GetPlatformInfo(hPlatformInfo, &pPlatformInfo);
      if(eResult!=DAL_SUCCESS)
      {
  	  	break;
      }
  
      if ((pPlatformInfo.subtype == DALPLATFORMINFO_SUBTYPE_QRD_T)&&(pPlatformInfo.platform == DALPLATFORMINFO_TYPE_QRD)&&(pChipInfo == DALCHIPINFO_ID_MSM8916))
      {
     	Led_GPIO_Number=119;
      }
    }while(0);
	
    return;
}


/*===========================================================================

**  Function :  boot_enable_led

** ==========================================================================
*/
/*! 
 * @brief This function turn on/off the blue led according to input
 * @return None.
 *
 */ 
boot_boolean boot_enable_led(boot_boolean led_on)
{
  return ((boot_pm_enable_led(led_on) ==  PM_ERR_FLAG__SUCCESS)&&(boot_gpio_enable_led(led_on,Led_GPIO_Number) == TRUE)) ? TRUE : FALSE;
}


/*===========================================================================

**  Function :  boot_toggle_led_init

** ==========================================================================
*/
/*! 
 * @brief This function initializes the timer count for led toggling
 * @return None.
 *
 */ 
void boot_toggle_led_init(void)
{

    boot_led_gpio_set();

     previous_time = boot_get_time();
}


/*===========================================================================

**  Function :  boot_toggle_led

** ==========================================================================
*/
/*! 
 * @brief This function executes the pmic rgb led functions to toggle LED
 * @return TRUE if operation is successful, FALSE if not.
 *
 */ 
boot_boolean boot_toggle_led(void)
{
  boot_boolean ret_val = TRUE;
  current_time = boot_get_time();
  
  /* There may be problem when current_time is out of range, but only 
   * one time, so do not take it into account
   */
  if ((current_time - previous_time) >= LED_FLASH_INTERVAL_US)
  {
    current_led_status = !current_led_status;
    ret_val = boot_enable_led(current_led_status);
    previous_time = current_time;
  }
  
  return ret_val;
}
