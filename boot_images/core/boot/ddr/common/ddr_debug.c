/**
 * @file boot_ddr_debug.c
 * @brief
 * Implementation for DDR Debug Mode.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/boot/ddr/common/ddr_debug.c#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
05/11/14   yps     Porting code to 8916 platform
09/17/13   sl      Added more DDR tuning options.
09/13/13   sl      Fixed pslew/nslew order.
08/29/13   sl      Use USB APIs instead of Boot APIs.
08/20/13   sl      Added DDR tuning.
08/07/13   sr      Added watchdog reset support.
06/18/13   sl      Initial version.
================================================================================
                     Copyright 2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "stdio.h"
#include "string.h"
#include "qhsusb_al_bulk.h"
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_hwio.h"
#include "ddr_test.h"
#include "DDR_PHY.h"
#include "HAL_DDR.h"
#include "mpm_hwio.h"
#include "Boot_extern_clk_interface.h"
#include "HALhwio.h"
#include "boot_extern_pmic_interface.h"
/*==============================================================================
                                  MACROS
==============================================================================*/
/* Length of transportation buffer in characters */
#define BUFFER_LENGTH   512

#define DEFAULT_SAMPLE_SIZE   (32)

#define MAX_STEP_POWER (16) 

/* Maximum length of command in characters */
#define CMD_MAX_LENGTH  64

/* Watchdog bark and bite time */
#define WATCHDOG_BARK_TIME_SCLK  0x7FFF
#define WATCHDOG_BITE_TIME_SCLK  0xFFFF

/* DDR tuning parameters */
#define DDR_TUNING_LOOP_COUNT  128
#define DQ_ROUT_MAX            0x7
#define DQ_PSLEW_MAX           0x3
#define DQ_NSLEW_MAX           0x3

#define RESET_DEBUG_SW_ENTRY_ENABLE  0x1
//#define SWEEP_WRITE_CDC_ONLY
#define BITFLIP_TEST_SIZE 64*1024

/*==============================================================================
                                  DATA
==============================================================================*/
/* Echo and message buffers: must be addressable by USB and 4-byte aligned */
__attribute__((aligned(4)))
static char rx_buf[BUFFER_LENGTH], tx_buf[BUFFER_LENGTH+1], msg_buf[BUFFER_LENGTH+1] = {0};

static volatile uint32 min_base = 0;

/* Number of bytes to be echoed */
static uint32 bytes_to_echo;

/* Indicator whether receiving callback function is called */
static boolean is_rx_called = FALSE;


static char str[BUFFER_LENGTH];
static char str1[BUFFER_LENGTH]={0};

/* Debug command of DDR Debug Mode */
static struct
{
  char     cmd[CMD_MAX_LENGTH+1];
  uint32   idx;
  boolean  ready;
} debug_cmd = {{0}, 0, FALSE};

/* DDR tuning pattern */
static const uint32 ddr_tuning_pattern[] =
{
  0xDEADBEEF, 0x5A5A5A5A, 0xA5A5A5A5, 0xFEEDFACE,
  0xCAFEBABE, 0xA5A5A5A5, 0x5A5A5A5A, 0x0BADF00D,

  0xA5A5A5A5, 0x5A5A5A5A, 0xA5A5A5A5, 0x5A5A5A5A,
  0x0F0F0F0F, 0xF0F0F0F0, 0x00000000, 0xFFFFFFFF,

  0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0xFFFFFFFF,
  0x00000000, 0xFFFFFFFF, 0x00000000, 0x00000000,
};

/**
 * @brief function defect test
 *        name function test name
 *        fp   test function pointer
 */
typedef struct  {
    char *name;
    boolean (*fp)(const uint32, const uint32);
}funcDefectTest_s;

/* DDR tuning cookies */
__attribute__((section("DDR_DEBUG_TUNING_COOKIE")))
static void (*volatile reset_cb_func)(boolean) = NULL;

__attribute__((section("DDR_DEBUG_TUNING_COOKIE")))
static volatile uint32 dq_rout, dq_pslew, dq_nslew, read_cdc_delay, write_cdc_delay;


/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
pm_err_flag_type switch_cx_mx_to_turbo_mode(void)
{
	pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;


	err_flag = pm_smps_volt_level(0,PM_SMPS_1,1287500);
	if(err_flag != PM_ERR_FLAG__SUCCESS)
		return err_flag;
	

    err_flag = pm_ldo_volt_level(0,PM_LDO_3,1287500);
	return err_flag;
}
static void ddr_debug_dog_set_bark_time(unsigned bark_time)
{
  while(!HWIO_INF(MPM2_WDOG_BARK_VAL_REG, SYNC_STATUS));
  HWIO_OUTF(MPM2_WDOG_BARK_VAL_REG, WDOG_BARK_VAL, bark_time);
}

static void ddr_debug_dog_set_bite_time(unsigned bite_time)
{
  while(!HWIO_INF(MPM2_WDOG_BITE_VAL_REG, SYNC_STATUS));
  HWIO_OUTF(MPM2_WDOG_BITE_VAL_REG, WDOG_BITE_VAL, bite_time);
}



void sysdbg_enable(void)
{

	// We need to enable PROC_HALT_EN so that MSM pulls up PS_HOLD after reset
	HWIO_OUTF(GCC_RESET_DEBUG, PROC_HALT_EN, 0x1);

	// Set the debug timer after which controller can reset the system
	// 256 sclk cycles = ~ 7.8ms
	HWIO_OUTF(GCC_RESET_DEBUG, PRE_ARES_DEBUG_TIMER_VAL, 0x100);	
	// Enable / Disable the debug through watchdog feature
	HWIO_OUTF(GCC_RESET_DEBUG, SECURE_WDOG_DEBUG_EN, 0x1);
	HWIO_OUTF(GCC_RESET_DEBUG, MSM_TSENSE_RESET_DEBUG_EN, 0x1);
	HWIO_OUTF(GCC_RESET_DEBUG, PMIC_ABNORMAL_RESET_DEBUG_EN, 0x1);

	//HWIO_OUTF(GCC_QDSS_STM_CMD_RCGR, ROOT_EN, 0x1);
	//HWIO_OUTF(GCC_QDSS_STM_CMD_RCGR, UPDATE, 0x1);
	//HWIO_OUTF(GCC_QDSS_STM_CBCR, CLK_ENABLE, 0x1);

//	HWIO_OUTF(GCC_QDSS_TRACECLKIN_CMD_RCGR, ROOT_EN, 0x1);
//	HWIO_OUTF(GCC_QDSS_TRACECLKIN_CMD_RCGR, UPDATE, 0x1);
//	HWIO_OUTF(GCC_QDSS_TRACECLKIN_CBCR, CLK_ENABLE, 0x1);
//	HWIO_OUTF(GCC_SYS_NOC_QDSS_STM_AXI_CBCR, CLK_ENABLE, 0x1);
	
//	HWIO_OUTF(GCC_QDSS_AT_CMD_RCGR, ROOT_EN, 0x1);
//	HWIO_OUTF(GCC_QDSS_AT_CMD_RCGR, UPDATE, 0x1);
//	HWIO_OUTF(GCC_QDSS_AT_CBCR, CLK_ENABLE, 0x1);

//	HWIO_OUTF(GCC_QDSS_TSCTR_CMD_RCGR, ROOT_EN, 0x1);
//	HWIO_OUTF(GCC_QDSS_TSCTR_CMD_RCGR, UPDATE, 0x1);

//	HWIO_OUTF(GCC_QDSS_DAP_AHB_CBCR, CLK_ENABLE, 0x1);
//	HWIO_OUTF(GCC_QDSS_CFG_AHB_CBCR,CLK_ENABLE, 0x1);

//	HWIO_OUTF(GCC_QDSS_DAP_CBCR, CLK_ENABLE, 0x1);
//	HWIO_OUT(QDSS_WRAPPER_CS_DEBUG_UI_LOCKACCESS, 0xC5ACCE55);
//	HWIO_OUTF(QDSS_WRAPPER_DEBUG_UI_SECURE, SEC_CTL, 0x1);
//	HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL, 0x1);      // Clear all status
//	HWIO_OUT(QDSS_WRAPPER_DEBUG_UI_CTL_MASK, 0x0);     // Clear SW_TRIG_MASK
//	HWIO_OUTF(GCC_RESET_DEBUG, BLOCK_RESIN, 0x1);

	//Set STOP_CAPTURE_DEBUG_TIMER to 2ms
//	HWIO_OUT(GCC_STOP_CAPTURE_DEBUG_TIMER, 0x40);
    // Set the ETR timer to flush data out
//	HWIO_OUT(GCC_FLUSH_ETR_DEBUG_TIMER, 0x180);
	// Flag to enable Debug
	HWIO_OUTM(TCSR_RESET_DEBUG_SW_ENTRY, 0x1, 0X1);
}
extern pm_err_flag_type  pm_spmi_lite_write_byte(uint32 uSlaveId , 
                                          uint16 addr, 
                                          uint8 data, 
                                          uint8 priority);
static void ddr_debug_dog_init(boolean normal)
{
  /*config PMIC reset domain to keep VDD MX when wdog reset*/
   pm_spmi_lite_write_byte(1 , 
											0x47D0, 
											0xA5, 
											1);
   pm_spmi_lite_write_byte(1 , 
											0x47DA, 
											0x05, 
											1);
  boot_pm_pon_ps_hold_cfg(0, PM_PON_RESET_CFG_WARM_RESET);

  HWIO_OUT(MPM2_WDOG_CTL_REG, 0x0);
  /* Clear RESET_ACCESS_FIRST_PASS and RESET_DEBUG_FIRST_PASS */
  HWIO_GCC_RESET_DEBUG_OUTM(HWIO_GCC_RESET_DEBUG_RESET_ACCESS_FIRST_PASS_BMSK,
                            0);
  HWIO_GCC_RESET_DEBUG_OUTM(HWIO_GCC_RESET_DEBUG_RESET_DEBUG_FIRST_PASS_BMSK,
                            0);

  /* Set RESET_DEBUG_SW_ENTRY register with System Debug entry point */
  HWIO_TCSR_RESET_DEBUG_SW_ENTRY_OUT(SCL_SYSTEM_DEBUG_BASE |
                                     RESET_DEBUG_SW_ENTRY_ENABLE);
  sysdbg_enable();

  if (normal)
  {
    ddr_debug_dog_set_bark_time(WATCHDOG_BARK_TIME_SCLK);
    ddr_debug_dog_set_bite_time(WATCHDOG_BITE_TIME_SCLK);
  }
  else
  {
    ddr_debug_dog_set_bark_time(WATCHDOG_BARK_TIME_SCLK>>8);
    ddr_debug_dog_set_bite_time(WATCHDOG_BITE_TIME_SCLK>>8);
  }
}

static void ddr_debug_dog_enable(boolean enable)
{
   HWIO_OUT( MPM2_WDOG_CTL_REG, (enable ? 0x80000001 : 0) );
}

static void ddr_debug_dog_kick( void )
{
   HWIO_OUT(MPM2_WDOG_RESET_REG, 1);
}

static void ddr_debug_force_reset( void )
{
  ddr_debug_dog_init(FALSE);

  ddr_debug_dog_enable(TRUE);

  /* Wait for watchdog reset */
  for (; ;) {}
}

/* =============================================================================
**  Function : ddr_debug_rx_cb
** =============================================================================
*/
/**
*   @brief
*   Callback for transportation layer receiving.
*
*   @param[in]  bytes_read  Number of bytes read
*   @param[in]  err_code    Error code returned
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
static void ddr_debug_rx_cb(uint32 bytes_read, uint32 err_code)
{
  uint32 read, write;

  if (!bytes_read)  { return; }

  for (read = 0, write = 0; read < bytes_read/sizeof(char); read++)
  {
    if (rx_buf[read] == '\r')
    {
      debug_cmd.cmd[debug_cmd.idx] = '\0';
      debug_cmd.idx = 0;
      debug_cmd.ready = TRUE;

      tx_buf[write++] = '\r';
      tx_buf[write++] = '\n';

      break;
    }
    else if (rx_buf[read] == '\b' || rx_buf[read] == '\x7F')
    {
      if (debug_cmd.idx > 0)
      {
        debug_cmd.idx--;

        tx_buf[write++] = rx_buf[read];
      }
    }
    else if ( rx_buf[read] >= 'a' && rx_buf[read] <= 'z' ||
              rx_buf[read] >= 'A' && rx_buf[read] <= 'Z' ||
              rx_buf[read] >= '0' && rx_buf[read] <= '9' )
    {
      if (debug_cmd.idx < CMD_MAX_LENGTH)
      {
        debug_cmd.cmd[debug_cmd.idx++] = rx_buf[read];

        tx_buf[write++] = rx_buf[read];
      }
    }
  }

  bytes_to_echo = write * sizeof(char);

  is_rx_called = TRUE;

} /* ddr_debug_rx_cb */

/* =============================================================================
**  Function : ddr_debug_tx_cb
** =============================================================================
*/
/**
*   @brief
*   Callback for transportation layer transmitting.
*
*   @param[in]  bytes_written  Number of bytes written
*   @param[in]  err_code       Error code returned
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
static void ddr_debug_tx_cb(uint32 bytes_written, uint32 err_code)
{
} /* ddr_debug_tx_cb */

/* =============================================================================
**  Function : ddr_debug_input
** =============================================================================
*/
/**
*   @brief
*   Get input command for DDR Debug Mode.
*
*   @param  None
*
*   @retval  Input command
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
static const char *ddr_debug_input( void )
{
  uint32 err_code;

  debug_cmd.ready = FALSE;

  do
  {
    /* Receive user's inputs */
    qhsusb_al_bulk_receive( (byte *)rx_buf, sizeof(rx_buf),
                            ddr_debug_rx_cb, &err_code );

    while (!is_rx_called) {
      qhsusb_al_bulk_poll();
    }
    is_rx_called = FALSE;

    /* Echo user's inputs */
    qhsusb_al_bulk_transmit( (byte *)tx_buf, bytes_to_echo,
                              ddr_debug_tx_cb, &err_code );
  } while (!debug_cmd.ready);

  return debug_cmd.cmd;

} /* ddr_debug_input */

/* =============================================================================
**  Function : ddr_debug_output
** =============================================================================
*/
/**
*   @brief
*   Output message for DDR Debug Mode.
*
*   @param[in]  msg  Message to be output
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
static void ddr_debug_output(const char *msg)
{
  uint32 err_code;

  strncpy(msg_buf, msg, BUFFER_LENGTH);

  qhsusb_al_bulk_transmit( (byte *)msg_buf, strlen(msg_buf) * sizeof(char),
                           ddr_debug_tx_cb, &err_code );

} /* ddr_debug_output */

static void ddr_debug_test(uint32 *ddr_base, uint32 failures[4])
{
  uint32 idx;
  uint32 result;

  result = 0;

  for (idx = 0; idx < sizeof(ddr_tuning_pattern)/sizeof(uint32); idx++)
  {
    ddr_base[idx] = 0;
  }
  
  for (idx = 0; idx < sizeof(ddr_tuning_pattern)/sizeof(uint32); idx++)
  {
    ddr_base[idx] = ddr_tuning_pattern[idx];
  }

  for (idx = 0; idx < sizeof(ddr_tuning_pattern)/sizeof(uint32); idx++)
  {
    result |= ddr_base[idx] ^ ddr_tuning_pattern[idx];
  }

  for (idx = 0; idx < 4; idx++)
  {
    if ( result & (0xFF << (idx*8)) )  { failures[idx]++; }
  }

} /* ddr_debug_test */


static boolean ddr_defect_interval_test_mem_map
(
 uint32 base,
 uint32 limit
);

static boolean ddr_function_defect_test_bit_flip_tuning(const uint32 base, const uint32 size, uint32 failures[4])
{
    volatile uint32 * base_addr = (uint32 *)base;
    const uint8 UINT32_LEN = 32;
    const uint8 LOOP_CYCLE = 2;
    const uint32 ONE = 0x1;
    uint8 k = 0, j = 0;
    uint32 i = 0;
    uint32 temp_value = 0;
    uint32 expect_value = 0;
    uint32 result = 0;
    uint32 idx = 0;

   for (j = 0; j < UINT32_LEN * 2; ++j)
    {
        uint32 val = ONE << j;	
	 
        for (k = 0; k < LOOP_CYCLE; ++k)
        {
            val = ~val;
			
            for (i = 0; i <= size; i++)
            {
                base_addr[i] = (i % 2) == 0 ? val : ~val;
            }
	  		ddr_debug_dog_kick();
            /* verify */
            for (i = 0; i <= size; i++)
            {
                temp_value = base_addr[i];
		   		expect_value = (i % 2) == 0 ? val : ~val;
                if (temp_value != expect_value)
                {
                	memset(str1,0,BUFFER_LENGTH);
                	if((base_addr[i]!=expect_value) && (temp_value == base_addr[i]))
                	{
               			snprintf(str1,BUFFER_LENGTH, "[Bitflip]write failed at address:0x%x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               (base_addr+i), base_addr[i], expect_value);
                	}
					else
					{
					 	snprintf(str1,BUFFER_LENGTH, "[Bitflip]read failed at address:0x%x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               (base_addr+i), temp_value, expect_value);
					}
					
     	   		 	result |= temp_value ^ expect_value;
		   
                    for (idx = 0; idx < 4; idx++)
                    {
                    	if ( result & (0xFF << (idx*8)) )  { failures[idx]++; }
                    }
  
                    return FALSE;
                }
            }
	        ddr_debug_dog_kick();			
        }
    }
	
  return TRUE;
}

static void ddr_debug_do_tuning(boolean in_progress, boolean is_all)
{

  ddr_size_info ddr_info;
  uint32 count;
  uint32 failures[4];
  static uint32 tempresult[4]={0xFF};
  uint16 CDC_DELAY_START = 210;
  uint16 READ_CDC_DELAY_MAX = 1400;
  uint16 WRITE_CDC_DELAY_MAX = 1400 ;
  uint8 CDC_DELAY_INC = 5;
  /* CDC_DELAY_START is begin CDC delay configuration, we will scan CDC delay from CDC_DELAY_START to CDC_DELAY_MAX. 
	   CDC_DELAY_INC is CDC delay scan step, the stepping rate depend on DDR PHY. 
	  CDC delay units is ps.
   */
 switch(boot_clock_get_ddr_speed())
 	{
 	case 600000:
		   CDC_DELAY_START = 150;
   		   READ_CDC_DELAY_MAX = 800;
           WRITE_CDC_DELAY_MAX = 800 ;
           CDC_DELAY_INC = 2;
		ddr_debug_output("\r\n DDR test frequency on 600MHz\r\n");  
		ddr_debug_output("\r\n Scan range[150,2,800]\r\n");
		ddr_debug_output("\r\n Pass range[208,408]\r\n");
		break;
 	case 533000:
	case 534000:
		   CDC_DELAY_START = 100;
   		   READ_CDC_DELAY_MAX = 930;
           WRITE_CDC_DELAY_MAX = 930 ;
           CDC_DELAY_INC = 2;
		   dq_pslew = dq_nslew = 0;
		ddr_debug_output("\r\n DDR test frequency on 533MHz\r\n");
		ddr_debug_output("\r\n Scan range[100,2,930]\r\n");
		ddr_debug_output("\r\n Pass range[259,679]\r\n");
		break;
 	case 333333:
		   CDC_DELAY_START = 210;
   		   READ_CDC_DELAY_MAX = 1400;
           WRITE_CDC_DELAY_MAX = 1400 ;
           CDC_DELAY_INC = 5;
		ddr_debug_output("\r\n DDR test frequency on 333MHz\r\n");
		ddr_debug_output("\r\n Scan range[210,5,1400]\r\n");
		ddr_debug_output("\r\n Pass range[400,1100]\r\n");
	    break;

	default:
		break;
 	}
		

  if (!in_progress)
  {
	ddr_debug_output("\r\nScan DDR: [rout, rdcdc, wrcdc]...\r\n");

    if (is_all) { dq_rout = dq_pslew = dq_nslew = 0; }
	
    read_cdc_delay = write_cdc_delay = CDC_DELAY_START;
  }
  else
  {
     memset(str,0,BUFFER_LENGTH);
    snprintf(str, BUFFER_LENGTH, "[%X, %X, %X]: "
                                 "X|X|X|X\r\n",
                                 dq_rout, read_cdc_delay, write_cdc_delay);
    ddr_debug_output(str);

    if ( (write_cdc_delay = (write_cdc_delay + CDC_DELAY_INC) & WRITE_CDC_DELAY_MAX) == 0 &&
         (read_cdc_delay = (read_cdc_delay + CDC_DELAY_INC) & READ_CDC_DELAY_MAX) == 0 &&
         ( !is_all || (dq_rout = (dq_rout + 1) & DQ_ROUT_MAX) == 0) )
    {
      return;
    }
  }

  ddr_info = ddr_get_size();

  /* Enable watchdog */
//  ddr_debug_dog_enable(TRUE);

  do /* dq_rout */
  {

  		if( read_cdc_delay > READ_CDC_DELAY_MAX)
			read_cdc_delay = CDC_DELAY_START ;
		/* Update drive strength: rout/pslew/nslew */
        HAL_SDRAM_PHY_Update_Drive_Strength(0,SDRAM_INTERFACE_0, dq_rout, dq_pslew, dq_nslew);

        do /* read_cdc_delay */
        {
          /* Update read CDC delay */
		  //read_cdc_delay = 0x1d5;
          HAL_SDRAM_PHY_Update_CDC_Delay(0,SDRAM_INTERFACE_0, TRUE, read_cdc_delay);
		  if( write_cdc_delay > WRITE_CDC_DELAY_MAX)
		      write_cdc_delay = CDC_DELAY_START;

          do /* write_cdc_delay */
          {
            /* Update write CDC delay */
            HAL_SDRAM_PHY_Update_CDC_Delay(0,SDRAM_INTERFACE_0, FALSE, write_cdc_delay);

            /* Issue manual IOCAL and CDCCAL to make new settings take effect */
            //HAL_SDRAM_PHY_Manual_IO_Cal(SDRAM_INTERFACE_0);
			/* Issue manual IOCAL */
            HAL_SDRAM_PHY_Manual_IO_Cal(0, SDRAM_INTERFACE_0, HAL_SDRAM_DDR_Wait);
			
            //HAL_SDRAM_PHY_Manual_CDC_Cal(SDRAM_INTERFACE_0);
			/* Issue manual CDCCAL */
			HAL_SDRAM_PHY_Manual_CDC_Cal(0, SDRAM_INTERFACE_0);

            failures[0] = failures[1] = failures[2] = failures[3] = 0;

            for (count = 0; count < DDR_TUNING_LOOP_COUNT; count++)
            {
              if (ddr_info.sdram0_cs0 != 0)
              {
                ddr_debug_test((uint32 *)(uint32)ddr_info.sdram0_cs0_addr, failures);
              }
              if (ddr_info.sdram0_cs1 != 0)
              {
                ddr_debug_test((uint32 *)(uint32)ddr_info.sdram0_cs1_addr, failures);
              }
            }
			ddr_function_defect_test_bit_flip_tuning((uint32)ddr_info.sdram0_cs0_addr, BITFLIP_TEST_SIZE , failures);

            if(((failures[0]!=tempresult[0]) 
			   || (failures[1]!=tempresult[1]) 
			   || (failures[2]!=tempresult[2]) 
			   || (failures[3]!=tempresult[3]))
			   || failures[0]
			   || failures[1]
			   || failures[2]
			   || failures[3]
			   || (write_cdc_delay == CDC_DELAY_START)
			   || (write_cdc_delay == WRITE_CDC_DELAY_MAX))

            {

			   memset(str,0,BUFFER_LENGTH);
			   if( failures[0]==0
			   	&& failures[1]==0
			   	&& failures[2]==0
			   	&& failures[3]==0
			   	  )
			   	{
                  snprintf(str, BUFFER_LENGTH, "%4u, %4u, %4u,"
                                         "%3u,%3u,%3u,%3u.[log]%s\r\n",
                                         dq_rout, read_cdc_delay, write_cdc_delay,
                                         failures[0], failures[1], failures[2], failures[3],"[Bitflip]PASS");
			   	}
			   else
			   	{
			   	  snprintf(str, BUFFER_LENGTH, "%4u, %4u, %4u,"
                                         "%3u,%3u,%3u,%3u.[log]%s\r\n",
                                         dq_rout, read_cdc_delay, write_cdc_delay,
                                         failures[0], failures[1], failures[2], failures[3],str1);
			   	}
     		    memset(str1,0,BUFFER_LENGTH);
				tempresult[0]= failures[0];
				tempresult[1]= failures[1];
				tempresult[2]= failures[2];
				tempresult[3]= failures[3];


		        ddr_debug_output(str);
				
			}
            
            /* Kick watchdog */
            ddr_debug_dog_kick();

          } while ((write_cdc_delay = (write_cdc_delay + CDC_DELAY_INC))<=WRITE_CDC_DELAY_MAX);

        }while ((read_cdc_delay = (read_cdc_delay + CDC_DELAY_INC))<= READ_CDC_DELAY_MAX);
  } while (is_all && (dq_rout = (dq_rout + 1) & DQ_ROUT_MAX) != 0);

  ddr_debug_output("DDR tuning done.\r\n");
  ddr_debug_output("Please press ENTER to continue: ");

} /* ddr_debug_do_tuning */

static void ddr_debug_tune_cdc(boolean in_progress)
{
  /* Install reset callback function */
  reset_cb_func = &ddr_debug_tune_cdc;

  /* Do tuning */
  ddr_debug_do_tuning(in_progress, FALSE);

  /* Uninstall reset callback function */
  reset_cb_func = NULL;

  /* Force watchdog reset */
  //ddr_debug_force_reset();

} /* ddr_debug_tune_cdc */

static void ddr_debug_tune_all(boolean in_progress)
{
  /* Install reset callback function */
  reset_cb_func = &ddr_debug_tune_all;

  /* Do tuning */
  ddr_debug_do_tuning(in_progress, TRUE);

  /* Uninstall reset callback function */
  reset_cb_func = NULL;

  /* Force watchdog reset */
  //ddr_debug_force_reset();

} /* ddr_debug_tune_all */

/* =============================================================================
**  Function : ddr_function_defect_test_solid_bits
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        Write the data 0x00000000 and 0x11111111 alternately to the tested 
 *  memory area, and read back to check if the same value is fetched.
 *
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
static boolean ddr_function_defect_test_solid_bits(uint32 base, uint32 size)
{
    volatile uint32 * base_addr = (uint32 *)base;
    const uint32 UINT32_ZERO = 0x0;
    const uint8 LOOP_CYCLES = 64;
    uint8  j = 0;
    uint32 i = 0;

    for (j = 0; j < LOOP_CYCLES; ++j)
    {
        uint32 val = (j % 2) == 0 ? (~UINT32_ZERO) : UINT32_ZERO;
		 
        for (i = 0; i <= size; i++)
        {  
            base_addr[i] = (i % 2) == 0 ? val : ~val;
        }
		 if(j == LOOP_CYCLES / 2)	
		       ddr_debug_output("\r\n"); 
		 else
		 	ddr_debug_output("."); 
	 
        /* verify */
        for (i = 0; i <= size; i++)
        {			
            if (base_addr[i] != ((i % 2) == 0 ? val : ~val))
            {
       	        snprintf(str,BUFFER_LENGTH, "\r\naddress:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               base_addr + i, base_addr[i], ((i % 2) == 0 ? val : ~val));
                ddr_debug_output(str); 
                return FALSE;
            }
        }
    }
    return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_check_board
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        CheckBoard test on the memory test size given
 *        with partten (0x55555555, 0xAAAAAAAA)
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
static boolean ddr_function_defect_test_check_board(uint32 base, uint32 size)
{
    volatile uint32 * base_addr = (uint32 *)base;
    const uint32 CHECK_BOARD = 0x55555555;
    const uint8 LOOP_CYCLES = 64;
    uint8  j = 0;
    uint32 i = 0;

    for (j = 0; j < LOOP_CYCLES; ++j)
    {
        uint32 val = (j % 2) == 0 ? (~CHECK_BOARD) : CHECK_BOARD;
        for (i = 0; i <= size; i++)
        {						
            base_addr[i] = (i % 2) == 0 ? val : ~val;
        }
		
	 if(j == LOOP_CYCLES / 2)	
	       ddr_debug_output("\r\n"); 
	 else
	 	ddr_debug_output("."); 	
	  
        /* verify */
        for (i = 0; i <= size; i++)
        {			
	     if (base_addr[i] != ((i % 2) == 0 ? val : ~val))
            {
       	   snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               base_addr + i, base_addr[i], ((i % 2) == 0 ? val : ~val));
                ddr_debug_output(str); 
                return FALSE;
            }
        }
    }

  return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_bit_spread
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        BitSpread test on the memory test size given
 *        with address:   00000101b -> 00001010b -> 00010100b  -> ...
 *             address++: 11111010b -> 11110101b -> 11101011b -> ...
 *             ...      :
 *        shmoo
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 * @sideeffects
 * Memory is corrupted after this function is called. Error is logged if any.
 */
static boolean ddr_function_defect_test_bit_spread(const uint32 base, const uint32 size)
{
    volatile uint32 * base_addr = (uint32 *)base;
    const uint8 UINT32_LEN = 32;
    const uint32 ONE = 0x1;
    uint8  j = 0;
    uint32 i = 0;


    for (j = 0; j < UINT32_LEN * 2; ++j)
    {
        uint32 val = 0;
        val = (ONE << j) | (ONE << (j + 2));

        for (i = 0; i <= size; i++)
        {
            base_addr[i] = (i % 2) == 0 ? val : ~val;
        }

	 if(j == UINT32_LEN)	
	       ddr_debug_output("\r\n"); 
	 else
	 	ddr_debug_output("."); 	
			 
        /* verify */
        for (i = 0; i <= size; i++)
        {
            if (base_addr[i] != ((i % 2) == 0 ? val : ~val))
            {
       	   snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               base_addr + i, base_addr[i], ((i % 2) == 0 ? val : ~val));
                ddr_debug_output(str); 
                 return FALSE;
            }
        }
    }

   return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_bit_flip
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        BitFlip test on the memory test size given
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
static boolean ddr_function_defect_test_bit_flip(const uint32 base, const uint32 size)
{
    volatile uint32 * base_addr = (uint32 *)base;
    const uint8 UINT32_LEN = 32;
    const uint8 LOOP_CYCLE = 2;
    const uint32 ONE = 0x1;
    uint8 k = 0, j = 0;
    uint32 i = 0;

    for (j = 0; j < UINT32_LEN * 2; ++j)
    {
        uint32 val = ONE << j;
		
	 if(j == UINT32_LEN)	
	       ddr_debug_output("\r\n"); 
	 else
	 	ddr_debug_output("."); 
	 
        for (k = 0; k < LOOP_CYCLE; ++k)
        {
            val = ~val;
            for (i = 0; i <= size; i++)
            {
                base_addr[i] = (i % 2) == 0 ? val : ~val;
            }
	  
            /* verify */
            for (i = 0; i <= size; i++)
            {
                if (base_addr[i] != ((i % 2) == 0 ? val : ~val))
                {
       	       snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               base_addr + i, base_addr[i], ((i % 2) == 0 ? val : ~val));
                    ddr_debug_output(str); 
                    return FALSE;
                }
            }
        }
    }
	
  return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_walking_one
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        WalkingOne test on the memory test size given
 *        with address:   0000001b -> 0000010b
 *                     -> 0000100b -> 0001000b
 *             ...      :
 *        shmoo
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
static boolean ddr_function_defect_test_walking_one(const uint32 base, const uint32 size)
{
    volatile uint32 * base_addr = (uint32 *)base;
    const uint8 UINT32_LEN = 32;
    const uint32 ONE = 0x1;
    uint8  j = 0;
    uint32 i = 0;

    for (j = 0; j < UINT32_LEN * 2; ++j)
    {
        uint32 val = 0;
        if (j < UINT32_LEN)
            val = (ONE << j) ;
        else
            val = (ONE << (UINT32_LEN * 2 - 1 - j));

        for (i = 0; i <= size; i++)
        {
            base_addr[i] = val;
        }

	 if(j == UINT32_LEN)	
	       ddr_debug_output("\r\n"); 
	 else
	 	ddr_debug_output("."); 
	  
        /* verify */
        for (i = 0; i <= size; i++)
        {
            if (base_addr[i] != val)
            {
       	   snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               base_addr + i, base_addr[i], ((i % 2) == 0 ? val : ~val));
                ddr_debug_output(str); 
                return FALSE;
            }
        }
    }

  return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_walking_zero
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        WalkingZero test on the memory test size given
 *        with address:   1111110b -> 1111101b
 *                     -> 1111011b -> 1110111b
 *             ...      :
 *        shmoo
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
static boolean ddr_function_defect_test_walking_zero(const uint32 base, const uint32 size)
{
    volatile uint32 * base_addr = (uint32 *)base;
    const uint8 UINT32_LEN = 32;
    const uint32 ONE = 0x1;
    uint8  j = 0;
    uint32 i = 0;

    for (j = 0; j < UINT32_LEN * 2; ++j)
    {
        uint32 val = 0;
        if (j < UINT32_LEN)
            val = ~(ONE << j) ;
        else
            val = ~(ONE << (UINT32_LEN * 2 - 1 - j));
        for (i = 0; i <= size; i++)
        {
            base_addr[i] = val;
        }

	 if(j == UINT32_LEN)	
	       ddr_debug_output("\r\n"); 
	 else
	 	ddr_debug_output("."); 
	  
        /* verify */
        for (i = 0; i <= size; i++)
        {
            if (base_addr[i] != val)
            {
       	   snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               base_addr + i, base_addr[i], ((i % 2) == 0 ? val : ~val));
                ddr_debug_output(str); 
                return FALSE;
            }
        }
    }

  return TRUE;
}
/* =============================================================================
**  Function : pseudo_rand_value_generate
** =============================================================================
*/
/**
 * @brief pseudo_rand_value_generate
 *        use a pseudo method to generate the random data from the seed
 *
 * @param[in] seed
 *
 * @return the random value
 */
uint32 pseudo_rand_value_generate(const uint32 seed)
{
    uint32 randVal = (seed * 123 + 59) % 0xFFFFFFFF;
    return randVal;
}

/* =============================================================================
**  Function : ddr_function_defect_test_rand_val
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *        ddr_function_defect_test_rand_val will fill the data test area with random 
 *        test data,the 1st data will come from data 0, the remaining data will be 
 *        generated from a pesudo random generation function
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 * @sideeffects
 * Memory is corrupted after this function is called. Error is logged if any.
 */
static boolean ddr_function_defect_test_rand_val(const uint32 base, const uint32 size)
{
   volatile uint32 * base_addr = (uint32 *)base;
    uint32 j = 0;
    uint32 randVal = 0;

    for (j = 0; j <= size; ++j)
    {
        base_addr[j] = randVal;
        randVal = pseudo_rand_value_generate(randVal);
    }
	
    ddr_debug_output("."); 
	
    /* verify */
    randVal = 0;
    for (j = 0; j <= size; ++j)
    {
        if (base_addr[j] != randVal)
        {
            snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               base_addr + j, base_addr[j], randVal);
            ddr_debug_output(str); 
            return FALSE;
        }
        randVal = pseudo_rand_value_generate(randVal);
    }

  return TRUE;
}

/* =============================================================================
**  Function : ddr_function_defect_test_seq_incr
** =============================================================================
*/
/**
 * @brief DDR function defect test API
 *  The value wrote to the address is increased 
 *  continuously with address increased
 *
 * @param[in] base  test memory base start pointer
 * @param[in] size  the type of space used to be test
 *
 * @return TRUE test pass, FALSE test fail
 *
 *   @sideeffects
 *   Memory is corrupted after this function is called. Error is logged if any.
 */
static boolean ddr_function_defect_test_seq_incr(const uint32 base, const uint32 size)
{
    volatile uint32 * base_addr = (uint32 *)base;
    uint32 j = 0;
    uint32 val = 0x0;

    for (j = 0; j <= size; ++j)
    {
        base_addr[j] = val;
        ++val;
    }
	
    ddr_debug_output("."); 	
	
    /* verify */	
    val = 0;
    for (j = 0; j <= size; ++j)
    {
        if (base_addr[j] != val)
        {
            snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                               base_addr + j, base_addr[j], val);
            ddr_debug_output(str); 
            return FALSE;
        }
        ++val;
    }

  return TRUE;
}

/* =============================================================================
**  Function : ddr_defect_interval_test_mem_map
** =============================================================================
*/
/**
*   @brief
*   Test if memory size in words is power of 2, and base address is aligned with
*   size.
*
*   Memory size in words is power of 2 if and only if size in bytes is power of
*   2 (i.e., (size & (size-1)) == 0x0) and is no smaller than one word. Base
*   address is aligned with size if and only if (base & (size-1)) == 0x0.
*
*   Notice this function takes memory limit as input, where limit = (size-1).
*
*   @param[in]  base     Base address of memory
*   @param[in]  limit    Maximum offset (in bytes) of memory
*
*   @return TRUE test pass, FALSE test fail
*/

static boolean ddr_defect_interval_test_mem_map
(
 uint32 base,
 uint32 limit
)
{
  return ( ((limit + 1) & limit) == 0x0 &&
           limit >= sizeof(uint32) - 1 &&
           (base & limit) == 0x0 );
}

boolean ddr_function_defect_test(const uint32  ddr_base, const uint32 size)
{
    static funcDefectTest_s function_defect_test[] =
    {
        {"Solid bits", ddr_function_defect_test_solid_bits},
        {"Checkerboard", ddr_function_defect_test_check_board},
        {"Bit Spread", ddr_function_defect_test_bit_spread},
        {"Bit Flip", ddr_function_defect_test_bit_flip},
        {"Walking ones", ddr_function_defect_test_walking_one},
        {"Walking zeros", ddr_function_defect_test_walking_zero},
        {"Random value", ddr_function_defect_test_rand_val},
        {"Seq incr", ddr_function_defect_test_seq_incr},
    };
	
    uint8 ddr_function_defect_test_num = sizeof(function_defect_test) / sizeof(*function_defect_test);
    uint8 i= 0;
	
    for(i=0; i < ddr_function_defect_test_num; i++)
    {    
         if(function_defect_test[i].name && function_defect_test[i].fp)
	    {
	        ddr_debug_output(function_defect_test[i].name);
		    ddr_debug_output(":\r\n"); 
      		 if(!(function_defect_test[i].fp(ddr_base, size >> 2)))
  		  {
      		  	  return 	FALSE;
  		  }
	         else
			ddr_debug_output("ok\r\n"); 	
    	    }
     }
	
  return TRUE;
}

/**
 * @brief DDR memory test by step
 *        test method:
 *        Step 0: write/read every double word until whole space looped
 *        Step 1: write/read every second double word until whole space looped
 *                totally 2 cycles.
 *        Step 2: write/read every four double word until whole space looped
 *                totally 4 cycles.
 *        ......
 *        Step n: write/read every 2^n double word until whole space looped
 *                totally 2^n cycles.
 *        NOTE: request at least 40MiB for test, and the memory space size
 *              should be n * 64KiByte
 *
 * @param base test memory space start pointer
 * @param length test memory space length in byte
 * @param stepPower the power of the step number
 * @param sampleSize the repeat times of this step test*
 * @return True test pass, FALSE test failure at read verification
 */
boolean ddr_interval_stress_test_per_step(uint32  const base, const uint32 length,
        const uint32 stepPower, const uint32 sampleSize)
{
    uint32 sampleCycle   = 0;
    uint32 stepStart     = 0;
    uint32 stepCycle     = 0;
    uint32 stepLength    = 0;
    uint32 dwordLength   = 0;
    volatile uint32 *base_addr;
	
    /* init function var*/
    stepLength = 1 << stepPower;
    dwordLength = length >> 2;
    base_addr = (uint32 *) base;
    	 
    /* write speed test by step defined*/
    for (sampleCycle = 0; sampleCycle != sampleSize; ++sampleCycle)
    {
	    for (stepStart = 0; stepStart != stepLength; ++stepStart)
	    {
	        uint32 stepEnd = dwordLength - stepStart;
	        for (stepCycle = 0; stepCycle < stepEnd;
	            stepCycle += stepLength)
	        {
	            base_addr[stepStart + stepCycle] = stepStart + stepCycle;
	        }
	    }
    }	
	
    /* read verification for ram data retention */
    for (stepStart = 0; stepStart != stepLength; ++stepStart)
    {
        uint32 stepEnd = dwordLength - stepStart;
        for (stepCycle = 0; stepCycle < stepEnd;
            stepCycle += stepLength)
        {
            if (base_addr[stepStart + stepCycle] != stepStart + stepCycle)
            {
                snprintf(str,BUFFER_LENGTH, "address:0x%08x, error data:0x%08x, expect data:0x%08x\r\n", 
		   	                                      base_addr + stepStart + stepCycle, base_addr[stepStart + stepCycle],stepStart + stepCycle);
                ddr_debug_output(str); 
                return FALSE;
            }
        }
    }
    return TRUE;
}

boolean ddr_interval_stress_test(uint32 const base, const uint32 size)
{
    uint32 i = 0;
    uint32 stepShmooBeg = 0;
    uint32 stepShmooEnd = MAX_STEP_POWER;
	
    /* print out related test information */
    snprintf(str,BUFFER_LENGTH, "\r\nMemory Test Space: 0x%x++(0x%x) %%BYTE\r\n"
          "\rInterval step ran ge: 0x%x~0x%x \r\n",
          base, size, stepShmooBeg, stepShmooEnd);
    ddr_debug_output(str);	
    ddr_debug_output("\rstep\t\tResult\r\n"); 
	
    for (i = stepShmooBeg; i != stepShmooEnd; i++)
    {
        if (!(ddr_interval_stress_test_per_step(base, size, i, DEFAULT_SAMPLE_SIZE)))
        {
             return FALSE;
        }
	 else
	 {
        snprintf(str,BUFFER_LENGTH, "\r\n%d\t\tok\r\n", 1 << i);
        ddr_debug_output(str); 	   	
	 }
    }
   return TRUE;
}


boolean ddr_defect_interval_test
(  
  uint32            test_select,
  SDRAM_INTERFACE   interface,
  SDRAM_CHIPSELECT  chip_select
)
{   
    uint32 ddr_base = 0, ddr_size = 0;
    ddr_size_info ddr_info = ddr_get_size();
	
    /* get base address and size */
    if (interface == SDRAM_INTERFACE_0)
    {
      if (chip_select == SDRAM_CS0)
      {
        ddr_base = ddr_info.sdram0_cs0_addr;
        ddr_size = ddr_info.sdram0_cs0;
      }
      else if (chip_select == SDRAM_CS1)
      {
        ddr_base = ddr_info.sdram0_cs1_addr;
        ddr_size = ddr_info.sdram0_cs1;
      }
      else if (chip_select == SDRAM_BOTH)
      {
        ddr_base = (ddr_info.sdram0_cs0_addr < ddr_info.sdram0_cs1_addr) ?
                    ddr_info.sdram0_cs0_addr : ddr_info.sdram0_cs1_addr;
        ddr_size = ddr_info.sdram0_cs0 + ddr_info.sdram0_cs1;
      }
    }
    else if (interface == SDRAM_INTERFACE_1)
    {
      if (chip_select == SDRAM_CS0)
      {
        ddr_base = ddr_info.sdram1_cs0_addr;
        ddr_size = ddr_info.sdram1_cs0;
      }
      else if (chip_select == SDRAM_CS1)
      {
        ddr_base = ddr_info.sdram1_cs1_addr;
        ddr_size = ddr_info.sdram1_cs1;
      }
      else if (chip_select == SDRAM_BOTH)
      {
        ddr_base = (ddr_info.sdram1_cs0_addr < ddr_info.sdram1_cs1_addr) ?
                    ddr_info.sdram1_cs0_addr : ddr_info.sdram1_cs1_addr;
        ddr_size = ddr_info.sdram1_cs0 + ddr_info.sdram1_cs1;
      }
    }

     /* convert size from megabyte to byte */
     ddr_size <<= 20;
   
     if (ddr_size == 0)
     {
         return FALSE;
     }
     else if (!ddr_defect_interval_test_mem_map(ddr_base, ddr_size-1))
     {
         ddr_debug_output("\r\nThe memory size in words is not power of 2\r\n");
         return FALSE;
     }
     else if (ddr_base + ddr_size > min_base)
     {
       /* adjust base address and size based on minimum base address for test */
       if (ddr_base < min_base)
       {
              ddr_size -= (min_base - ddr_base);
             ddr_base = min_base;
       }	 

	  ddr_size >>= 2;
			
		if(test_select == 0)
		{
			if(!(ddr_function_defect_test(ddr_base, ddr_size - 1)))	
				return FALSE;
		}
		if (test_select == 1)
		{
			if(!(ddr_interval_stress_test(ddr_base, ddr_size - 1)))
				return FALSE;
		}		
      }
	 
  return TRUE;
}

/* =============================================================================
**  Function : ddr_debug
** =============================================================================
*/
/**
*   @brief
*   Command interpreter of DDR Debug Mode.
*
*   @param  None
*
*   @retval  None
*
*   @dependencies
*   Transportation layer (e.g., USB) must be set up, and all data must be moved
*   out of DDR (otherwise would be corrupted).
*
*   @sideeffects
*   DDR could be corrupted.
*
*   @sa
*   None
*/
void ddr_debug( void )
{
  static const char ddr_debug_msg_menu[] =
  "--------------------------------------------\r\n"
  "|     DDR MUST - Memory Unit Shmoo Test     |\r\n"
  "|      Copyright 2013 by QUALCOMM Inc.      |\r\n"
  "--------------------------------------------\r\n"
//  "0: Restart System\r\n"
  "1: DDR DATA Lines and ADD Lines Test (< 1sec)\r\n"
  "2: DDR Read/Write Test: Advanced (< 5min)\r\n"
  "3: Manual CDC Scan: CDC Only (< 30min)[Rout(0,1,7)]\r\n"
  "4: Automatic CDC Scan: All (overnight)\r\n"
  "5: Switch DDR VDDQ: 1.2V~1.3V[VDDQ(1,1,9)]\r\n"
  "6: The DDR Function Defect Test\r\n"
  "7: The DDR Interval Stress Test\r\n";

  extern void qhsusb_dci_write_scratch(uint32 core_id, uint8 offset, uint32 data);
  const char *cmd;
  uint8 VDDQ = 0;
  
  /* Start USB enumeration */
  qhsusb_al_bulk_init(0x0, QHSUSB_MODE__SER_ONLY);

  qhsusb_dci_write_scratch(0, 0, 0x0);

  ddr_debug_dog_init(TRUE);
  memset(str,0,BUFFER_LENGTH);
  memset(str1,0,BUFFER_LENGTH);

  if (reset_cb_func != NULL)
  {
    reset_cb_func(TRUE);
  }

  while (1)
  {
    ddr_debug_output("\r\nPlease press ENTER to continue: ");
    ddr_debug_input();

    ddr_debug_output(ddr_debug_msg_menu);

    while (1)
    {
      ddr_debug_output("\r\nPlease select an option: ");
      cmd = ddr_debug_input();

      if (strlen(cmd) != 1 || *cmd < '0' || *cmd > '7') { continue; }

      switch (*cmd)
      {
        case '0':
          ddr_debug_output("\r\nRestarting system...\r\n");
          ddr_debug_force_reset();
          break;

        case '1':
          ddr_debug_output("\r\nTesting data/address lines of DDR...\r\n");
          if (ddr_test(DDR_TEST_SUITE_ACCESS, 0, SDRAM_INTERFACE_0, SDRAM_BOTH, 0))
          {
            ddr_debug_output("DDR test passed.\r\n");
          }
          else
          {
            ddr_debug_output("DDR test failed.\r\n");
          }
          break;

        case '2':
          ddr_debug_output("\r\nTesting all range of DDR by own-address algorithm...\r\n");
          if (ddr_test(DDR_TEST_SUITE_ACCESS, 1, SDRAM_INTERFACE_0, SDRAM_BOTH, 0))
          {
            ddr_debug_output("DDR test passed.\r\n");
          }
          else
          {
            ddr_debug_output("DDR test failed.\r\n");
          }
          break;

        case '3':
          ddr_debug_output("\r\n");
          while (1)
          {
            ddr_debug_output("Please input rout(e.g, 2): ");
            cmd = ddr_debug_input();

            if ( strlen(cmd) == 3 &&
                 cmd[0] >= '0' && cmd[0] <= '0' + DQ_ROUT_MAX &&
                 cmd[1] >= '0' && cmd[1] <= '0' + DQ_PSLEW_MAX &&
                 cmd[2] >= '0' && cmd[2] <= '0' + DQ_NSLEW_MAX )
            {
              dq_rout = cmd[0] - '0';
              dq_pslew = cmd[1] - '0';
              dq_nslew = cmd[2] - '0';

              ddr_debug_tune_cdc(FALSE);
              break;
            }
	      if ( strlen(cmd) == 1 &&
                 cmd[0] >= '0' && cmd[0] <= '0' + DQ_ROUT_MAX)
            {
              dq_rout = cmd[0] - '0';
              ddr_debug_tune_cdc(FALSE);
              break;
            }
          }
          break;

        case '4':
          ddr_debug_tune_all(FALSE);
          break;
		  
		case '5':
			{
            ddr_debug_output("Please input select (e.g, 1 for 1=1.2V  2=1.2125V 3=1.225V 4=1.2375V ... 9 =1.3V,step=0x0125V, maxset:1.3V ): ");
            cmd = ddr_debug_input();
			 VDDQ = cmd[0] - '0';
            if (( strlen(cmd) == 1)&& (VDDQ>0)&& (VDDQ<10))
            {
             
              
			  	{
				pm_err_flag_type ret_status;	
				   ret_status = pm_ldo_volt_level(0,PM_LDO_2,1200000+(VDDQ-1)*12500);
			  		if(PM_ERR_FLAG__SUCCESS!=ret_status)
				  	{
				  	ddr_debug_output("Set VDDQ failed\n");
				  	break;
				  	}
					else
					{
					ddr_debug_output("Set VDDQ ok\n");
					}
              	}
           
            }
			else
				{
				 ddr_debug_output("input failed \n");
				}
		     break;
          }
		
		case '6':
					ddr_debug_output("\r\nDDR Function Stress Test\r\n");
					if(ddr_defect_interval_test(0,SDRAM_INTERFACE_0, SDRAM_BOTH))
					  {
						   ddr_debug_output("DDR test passed\r\n");
					  }
					else
					  {
						   ddr_debug_output("DDR test failed\r\n");
					  }
			break;
		case '7':
					ddr_debug_output("\r\nDDRInterval Stress Test\r\n");
					if(ddr_defect_interval_test(1,SDRAM_INTERFACE_0, SDRAM_BOTH))
					  {
						   ddr_debug_output("DDR test passed\r\n");
					  }
					else
					  {
						   ddr_debug_output("DDR test failed\r\n");
					  }
			break;		
        default:
          /* Should not reach here */
          break;
      }

      break;
    }
  }

} /* ddr_debug */
