/**
 * @file ddr_target.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/boot/ddr/hw/msm8x26/ddr_target.c#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
09/05/13   sr      changes to not support training in boot.
06/20/13   rj      Added support for lpddr2 on GIMLI
06/06/13   rj      Updated 8926 delta settings and added function
                   related to ddr draining required.
05/13/13   sl      Removed ddr_apply_delta_settings().
04/10/13   sl      Use voltage mode instead of voltage number.
04/03/13   sl      Added macros for vddmx voltage levels.
03/01/13   tw      Added stubs for applying delta settings
02/21/13   sl      Initial version.
================================================================================
                      Copyright 2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_params.h"
#include "ddr_internal.h"
#include "ddr_target.h"
#include "ddr_config.h"
#include "ddr_log.h"
#include "DDIChipInfo.h"


/*==============================================================================
                                  DATA
==============================================================================*/
/* DDR logging level */
enum ddr_log_level ddr_log_level = DDR_ERROR;

/* Initial DDR PHY CDC delay */
uint32 DDR_PHY_INIT_CDC_DELAY;


/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
/* =============================================================================
**  Function : ddr_target_init
** =============================================================================
*/
/**
*   @brief
*   Function called at the end of ddr_init() to initialize target specific
*   configurations.
*
*   @param  None
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_target_init()
{
  extern uint32 ddr_bimc_config_MSM8926[][2];
  extern uint32 ddr_phy_config_MSM8926[][2];
  extern uint32 ddr_caphy_config_MSM8926[][2];
  extern uint32 ddr_dqphy_config_MSM8926[][2];
  extern uint32 ddr_bimc_config_MSM8x26v2[][2];
  extern uint32 ddr_phy_config_MSM8x26v2[][2];
  extern uint32 ddr_caphy_config_MSM8x26v2[][2];
  extern uint32 ddr_dqphy_config_MSM8x26v2[][2];

  if (DalChipInfo_ChipFamily() == DALCHIPINFO_FAMILY_MSM8x26)
  {
    if (!(DalChipInfo_ChipVersion() < DALCHIPINFO_VERSION(2,0)))
    {
      ddr_bimc_config_delta = ddr_bimc_config_MSM8x26v2;
      ddr_phy_config_delta = ddr_phy_config_MSM8x26v2;
      ddr_caphy_config_delta = ddr_caphy_config_MSM8x26v2;
      ddr_dqphy_config_delta = ddr_dqphy_config_MSM8x26v2;
    }
    DDR_PHY_INIT_CDC_DELAY = 0x271;
  }
  else if (DalChipInfo_ChipFamily() == DALCHIPINFO_FAMILY_MSM8926)
  {
    if (ddr_get_params(SDRAM_INTERFACE_0)->common.device_type == DDR_TYPE_LPDDR2)
    {
      ddr_bimc_config_delta = ddr_bimc_config_MSM8x26v2;
      ddr_phy_config_delta = ddr_phy_config_MSM8x26v2;
      ddr_caphy_config_delta = ddr_caphy_config_MSM8x26v2;
      ddr_dqphy_config_delta = ddr_dqphy_config_MSM8x26v2;

      DDR_PHY_INIT_CDC_DELAY = 0x271;
    }
    else if (ddr_get_params(SDRAM_INTERFACE_0)->common.device_type == DDR_TYPE_LPDDR3)
    {
      ddr_bimc_config_delta = ddr_bimc_config_MSM8926;
      ddr_phy_config_delta = ddr_phy_config_MSM8926;
      ddr_caphy_config_delta = ddr_caphy_config_MSM8926;
      ddr_dqphy_config_delta = ddr_dqphy_config_MSM8926;

      DDR_PHY_INIT_CDC_DELAY = 0x22C;
    }
  }
} /* ddr_target_init */

/* =============================================================================
**  Function : ddr_post_init
** =============================================================================
*/
/**
*   @brief
*   Function called after DDR is initialized for DDR training if required.
*
*   @param  None
*
*   @retval  TURE   DDR training required
*   @retval  FALSE  DDR training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_post_init()
{
  return FALSE;

} /* ddr_post_init */

/* =============================================================================
**  Function : ddr_params_is_training_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. LPDDR3 will always require training syncronization
*   to be done between rpm and sbl
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_required( void )
{
  return FALSE;
} /* ddr_params_is_training_required */

/* =============================================================================
**  Function : ddr_params_is_training_on_rpm_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not in RPM. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. 
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_on_rpm_required( void )
{
  return FALSE;
} /* ddr_params_is_training_on_rpm_required */

/* =============================================================================
**  Function : ddr_do_phy_training
** =============================================================================
*/
/**
*   @brief
*   function called to do the DDR PHY training 
*
*   @param  None
*
*   @retval  NONE
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_do_phy_training( void )
{  
} /* ddr_do_phy_training */


