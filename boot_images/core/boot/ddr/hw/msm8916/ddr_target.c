/**
 * @file ddr_target.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/boot/ddr/hw/msm8916/ddr_target.c#4 $
$DateTime: 2015/08/21 04:29:39 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
08/18/15   aus     Sysnoc reconfiguration for 1+2 DDR 
06/25/15   aus     Added support for 3GB DDR 
02/24/14   tw      Updated update clock period function pointer
02/09/14   tw      added support for common ddr library
01/24/14   tw      initial port for 8916
                   added support for sbl\rpm shared ddr driver
09/05/13   sr      changes to not support training in boot.
06/20/13   rj      Added support for lpddr2 on GIMLI
06/06/13   rj      Updated 8926 delta settings and added function
                   related to ddr draining required.
05/13/13   sl      Removed ddr_apply_delta_settings().
04/10/13   sl      Use voltage mode instead of voltage number.
04/03/13   sl      Added macros for vddmx voltage levels.
03/01/13   tw      Added stubs for applying delta settings
02/21/13   sl      Initial version.
================================================================================
                      Copyright 2013-2015 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_hwio.h"
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_params.h"
#include "ddr_internal.h"
#include "ddr_target.h"
#include "ddr_config.h"
#include "ddr_log.h"
#include "DDIChipInfo.h"
#include "BIMC.h"
#include "DDR_PHY.h"
#include "ddr_func_tbl.h"
#include "DDIChipInfo.h"
#include "clock_hwio.h"
#include "mpm_hwio.h"

/*==============================================================================
                                  DATA
==============================================================================*/
/* DDR logging level */
enum ddr_log_level ddr_log_level = DDR_ERROR;

/* Initial DDR PHY CDC delay */
uint32 DDR_PHY_INIT_CDC_DELAY;

__attribute__((section("ddr_func_ptr"))) ddr_func ddr_func_tbl;

boolean wdog_reset = FALSE;

/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
#define BUSYWAIT_XO_FREQUENCY_IN_KHZ 20

/* ddr driver implementation for WDOG reset case */
boolean ddr_busywait_init(void)
{
  HWIO_OUTF(MPM2_MPM_CONTROL_CNTCR, EN, 0x1); 
  HWIO_OUTI(MPM2_QTMR_AC_CNTACR_n,0,0x3F);
  
  return TRUE;
}

void ddr_busywait (uint32 pause_time_us)
{
  uint32 start_count = 0;
  uint64 delay_count = 0;

  start_count = HWIO_IN(MPM2_QTMR_V1_CNTPCT_LO);
  /*
   * Perform the delay and handle potential overflows of the timer.
   */

  delay_count = (pause_time_us * (uint64)BUSYWAIT_XO_FREQUENCY_IN_KHZ);
  while ( (HWIO_IN(MPM2_QTMR_V1_CNTPCT_LO) - start_count) < delay_count);
  
}

/* =============================================================================
**  Function : ddr_target_init
** =============================================================================
*/
/**
*   @brief
*   Function called at the end of ddr_init() to initialize target specific
*   configurations.
*
*   @param  None
*
*   @retval  None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_target_init()
{
  struct ddr_shared_params *header;
  
  extern uint32 ddr_bimc_config_MSM8x16_LPDDR3[][2];
  extern uint32 ddr_phy_config_MSM8x16_LPDDR3[][2];
  extern uint32 ddr_caphy_config_MSM8x16_LPDDR3[][2];
  extern uint32 ddr_dqphy_config_MSM8x16_LPDDR3[][2];
  
  //if (DalChipInfo_ChipFamily() == DALCHIPINFO_ID_MSM8916)
  {
    if (ddr_get_params(SDRAM_INTERFACE_0)->common.device_type == DDR_TYPE_LPDDR3)
    {
      ddr_bimc_config_delta = ddr_bimc_config_MSM8x16_LPDDR3;
      ddr_phy_config_delta = ddr_phy_config_MSM8x16_LPDDR3;
      ddr_caphy_config_delta = ddr_caphy_config_MSM8x16_LPDDR3;
      ddr_dqphy_config_delta = ddr_dqphy_config_MSM8x16_LPDDR3;
    }
    DDR_PHY_INIT_CDC_DELAY = 0x271;
  }
  
  /* initialization of function tables for shared ddr library 
   * taking into account RPM view of shared library is different 
   */
  ddr_func_tbl.enter_self_refresh = (void (*)(uint32, SDRAM_INTERFACE, SDRAM_CHIPSELECT, ddr_wait))
                                    ((uint8 *)&HAL_SDRAM_Enter_Self_Refresh - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.exit_self_refresh = (void (*)(uint32, SDRAM_INTERFACE, SDRAM_CHIPSELECT, ddr_wait))
                                   ((uint8 *)&HAL_SDRAM_Exit_Self_Refresh - SCL_RPM_CODE_RAM_BASE);
  
  ddr_func_tbl.get_read_latency = (uint32 (*) (DDR_TYPE , uint32 )) 
                                  ((uint8 *)&HAL_SDRAM_RL - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.get_write_latency = (uint32 (*) (DDR_TYPE , uint32 )) 
                                   ((uint8 *)&HAL_SDRAM_WL - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.bimc_update_clock_period = (void (*)(uint32, uint32, ddr_divide )) 
                                          ((uint8 *)&HAL_SDRAM_BIMC_Update_Clock_Period - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.bimc_dpe_pre_clock_switch = (void (*)(uint32 , SDRAM_INTERFACE , uint32 , uint32 )) 
                                           ((uint8 *)&HAL_SDRAM_DPE_Pre_Clock_Switch - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.bimc_dpe_post_clock_switch = (void (*)(uint32 , SDRAM_INTERFACE , uint32 , uint32 )) 
                                            ((uint8 *)&HAL_SDRAM_DPE_Post_Clock_Switch - SCL_RPM_CODE_RAM_BASE);
  
  ddr_func_tbl.phy_manual_io_cal = (void (*)(uint32 , SDRAM_INTERFACE , ddr_wait )) 
                                   ((uint8 *)&HAL_SDRAM_PHY_Manual_IO_Cal - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_update_cdc_config = (void (*)(uint32 , SDRAM_INTERFACE , uint32 )) 
                                    ((uint8 *)&HAL_SDRAM_PHY_Update_CDC_Config - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_manual_cdc_cal = (void (*)(uint32 , SDRAM_INTERFACE )) 
                                    ((uint8 *)&HAL_SDRAM_PHY_Manual_CDC_Cal - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_update_drive_strength = (void (*) (uint32 ,SDRAM_INTERFACE, uint32 , uint32 , uint32 )) 
                                           ((uint8 *)&HAL_SDRAM_PHY_Update_Drive_Strength - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_update_cdc_delay = (void (*)(uint32 , SDRAM_INTERFACE , boolean read, uint32 cdc_delay)) 
                                      ((uint8 *)&HAL_SDRAM_PHY_Update_CDC_Delay - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_enable_rapid_cdc_cal = (void (*)(uint32 , SDRAM_INTERFACE )) 
                                          ((uint8 *)&HAL_SDRAM_PHY_Enable_Rapid_CDC_Cal - SCL_RPM_CODE_RAM_BASE);
  ddr_func_tbl.phy_disable_rapid_cdc_cal = (void (*)(uint32 , SDRAM_INTERFACE )) 
                                           ((uint8 *)&HAL_SDRAM_PHY_Disable_Rapid_CDC_Cal - SCL_RPM_CODE_RAM_BASE);
  
   
  /* clear out shared header and fill in with updated information */
  header = ddr_get_shared_header();
  memset(header, 0, sizeof(struct ddr_shared_params));
  
  header->ddr_func_tbl_ptr = (void *) ((uint8 *)&ddr_func_tbl - SCL_RPM_CODE_RAM_BASE);
  header->size = sizeof(ddr_func);
} /* ddr_target_init */

/* =============================================================================
**  Function : ddr_post_init
** =============================================================================
*/
/**
*   @brief
*   Function called after DDR is initialized for DDR training if required.
*
*   @param  None
*
*   @retval  TURE   DDR training required
*   @retval  FALSE  DDR training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_post_init()
{
  return FALSE;

} /* ddr_post_init */

/* =============================================================================
**  Function : sys_debug_bric_settings
** =============================================================================
*/
/**
*   @brief
*   function called to update the BRIC register settings.
*
*   @param [in] None
*
*   @retval  NONE
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void sys_debug_bric_settings(void)
{
  uint32 mr8, device_density, device_io_width;
  
  mr8 = (uint32)HAL_SDRAM_Read_MR(0, SDRAM_INTERFACE_0, SDRAM_CS0, 0x8);
  device_density = (mr8 & 0x3C) >> 2;
  device_io_width = (mr8 & 0xC0) >> 6;
  
  // 1.5GB + 1.5GB configuration
  if ((HAL_SDRAM_Get_Base_Addr(SDRAM_INTERFACE_0, SDRAM_CS0) == 0x0) && (device_density == 14) && (device_io_width == 1)) {
    //Disable segment address registers
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER,0,0x00000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER,0,0x00000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER,0,0x00000000);

    //DDR memory configuration: (3GB = 4GB -512MB -512MB) 4GB(0x0000_0000 � 0xFFFF_FFFF)
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER,0,0x00000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER,0,0x00000000);
    
    //Remove first 512MB(0x0000_0000 � 0x1FFF_FFFF)
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER,0,0x00000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER,0,0xE0000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER,0,0x00000002);

    //Remove last 512MB(0xE000_0000 � 0xFFFF_FFFF)
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER,0,0xE0000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_MASK_C_LOWER,0,0xE0000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER,0,0xE0000002);

    //Remap 0x6000_0000 to 0x0000_00000 (1.5GB shift)
    HWIO_OUTI(BIMC_BRIC_REMAPn_ADDR_BASE_LOWER,0,0x60000000);
    HWIO_OUTI(BIMC_BRIC_REMAPn_ADDR_MASK_LOWER,0,0xE0000000);
    HWIO_OUTI(BIMC_BRIC_REMAPn_ADDR_BASE_LOWER,0,0x60000001);
    HWIO_OUTI(BIMC_BRIC_REMAPn_OP1,0,0x00FA0000);
    HWIO_OUTI(BIMC_BRIC_REMAPn_OP1,0,0x00FA0001);

    //Enable segment address registers
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_C_LOWER,0,0xE0000003);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER,0,0x00000003);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER,0,0x00000001);
  }
  // 1GB + 2GB configuration
  else {
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER,0,0x80000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER,0,0x80000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER,3,0xC0000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_A_LOWER,0,0x80000001);

    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER,0,0x40000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_MASK_B_LOWER,0,0xC0000000);
    HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_BASE_B_LOWER,0,0x40000001);
  } 
  
  //SYSNOC slave address range configuration to 512MB 
  HWIO_OUTI(BIMC_BRIC_SEGMENTn_ADDR_MASK_A_LOWER,2,0xE0000000);  
}


/* =============================================================================
**  Function : ddr_params_is_training_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. LPDDR3 will always require training syncronization
*   to be done between rpm and sbl
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_required( void )
{
  return FALSE;
} /* ddr_params_is_training_required */

/* =============================================================================
**  Function : ddr_params_is_training_on_rpm_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not in RPM. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. 
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_on_rpm_required( void )
{
  return FALSE;
} /* ddr_params_is_training_on_rpm_required */

/* =============================================================================
**  Function : ddr_do_phy_training
** =============================================================================
*/
/**
*   @brief
*   function called to do the DDR PHY training 
*
*   @param  None
*
*   @retval  NONE
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_do_phy_training( void )
{  
} /* ddr_do_phy_training */

/* =============================================================================
**  Function : ddr_restore_from_wdog_reset
** =============================================================================
*/
/**
*   @brief
*   function called to take ddr out of self refresh following a wdog reset
*
*   @param [in] clk_speed_khz  clock speed of ddr in khz
*
*   @retval  NONE
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_restore_from_wdog_reset( uint32 clk_speed_khz )
{
  struct ddr_device_params_common *ddr_params_ch0;
  SDRAM_CHIPSELECT ch0_chip_sel = SDRAM_CS_NONE;
  /* Get DDR device parameters */
  ddr_params_ch0 = &(ddr_get_params(SDRAM_INTERFACE_0)->common);
  
  if(ddr_params_ch0->num_banks_cs0 != 0)
  {
    ch0_chip_sel |= SDRAM_CS0;
  }
  if(ddr_params_ch0->num_banks_cs1 != 0)
  {
    ch0_chip_sel |= SDRAM_CS1;
  }
  
  /* set status variable to indicate wdog reset */
  wdog_reset = TRUE;
  
  /* initialize ddr version of busywait */
  ddr_busywait_init();
  
  /* hack mpm to unfreeze io */
  HWIO_OUT(MPM2_MPM_DDR_PHY_FREEZEIO_EBI1, 0x0);
  
  /* restore cdt parameters */
  ddr_restore_param_wdog_reset();
  
  /* restore ddr from self refresh */
  if(ch0_chip_sel != SDRAM_CS_NONE)
  {
    HAL_SDRAM_Init(SDRAM_INTERFACE_0, ch0_chip_sel, clk_speed_khz);
    
    /* exit self refresh */
    HAL_SDRAM_Exit_Self_Refresh(0, SDRAM_INTERFACE_0, ch0_chip_sel, HAL_SDRAM_DDR_Wait);
    
    /* update rl\wl latency to match configuration */
    HAL_SDRAM_DPE_Force_Update_Latency(0, SDRAM_INTERFACE_0, ch0_chip_sel, clk_speed_khz);
  }

  sys_debug_bric_settings();
}
