#ifndef __DDR_TARGET_H__
#define __DDR_TARGET_H__


/**
 * @file ddr_target.h
 * @brief
 * Header file for DDR target-specific info.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/boot/ddr/hw/msm8x62/ddr_target.h#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
08/12/13   sr      Initial version.
================================================================================
                      Copyright 2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_hwio.h"
#include "HALhwio.h"


/*==============================================================================
                                  MACROS
==============================================================================*/
/* DDR controller register names */
#define DDR_ADDR(ddr_reg) BIMC_S_DDR0_##ddr_reg
#define PHY_DQ_ADDR(dim_reg) CH0_DQ0_DIM_DQ_##dim_reg
#define PHY_CA_ADDR(dim_reg) CH0_CA_DIM_CA_##dim_reg


/* DDR controller offsets */
#define SDRAM_0_OFFSET     0x0
#define SDRAM_1_OFFSET (BIMC_S_DDR1_REG_BASE - BIMC_S_DDR0_REG_BASE)

#define PHY_0_OFFSET       0x0
#define PHY_1_OFFSET   (CH1_DQ0_DDRPHY_DQ_REG_BASE - CH0_DQ0_DDRPHY_DQ_REG_BASE)

#define PHY_CA_MST_OFFSET  0x0
#define PHY_DQ0_OFFSET     0x0
#define PHY_DQ1_OFFSET     (CH0_DQ1_DDRPHY_DQ_REG_BASE - CH0_DQ0_DDRPHY_DQ_REG_BASE)
#define PHY_DQ2_OFFSET     (CH0_DQ2_DDRPHY_DQ_REG_BASE - CH0_DQ0_DDRPHY_DQ_REG_BASE)
#define PHY_DQ3_OFFSET     (CH0_DQ3_DDRPHY_DQ_REG_BASE - CH0_DQ0_DDRPHY_DQ_REG_BASE)

/* DDR base address */
#define DDR_BASE  0x0

/* XO clock frequency in KHz */
#define XO_SPEED_IN_KHZ  19200

/* Initial DDR PHY CDC delay */
extern uint32 DDR_PHY_INIT_CDC_DELAY;

/* Broadcast offsets */
#define CA_BCAST_OFFSET (HWIO_EBI1_AHB2PHY_BROADCAST_ADDRESS_SPACE_n_ADDR(0) \
        - CH0_CA_DDRPHY_CA_REG_BASE)
#define DQ_BCAST_OFFSET (HWIO_EBI1_AHB2PHY_BROADCAST_ADDRESS_SPACE_n_ADDR(0) \
        - CH0_DQ0_DDRPHY_DQ_REG_BASE)



#endif /* __DDR_TARGET_H__ */
