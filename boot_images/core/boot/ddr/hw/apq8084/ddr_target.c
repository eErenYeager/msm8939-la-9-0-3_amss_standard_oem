/**
 * @file ddr_target.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/boot/ddr/hw/apq8084/ddr_target.c#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
09/09/13   sr      Removed unused extern variables .
09/06/13   sr      Skeleton Code for PHY training in SBL1
09/05/13   sr      initial changes to support training in boot.
08/27/13   sr      is_training_required moved from params.c to target.c
06/21/13   tw      Initial Port for 8084
05/13/13   sl      Updated delta table calls.
03/01/13   tw      Added calling delta table
11/02/12   tw      re-enabled 800mhz, added site initialization
11/02/12   tw      enabled flag for training required
11/02/12   sy      Back out 800Mhz
09/21/12   sl      Combined LPDDR device parameters.
09/17/12   sl      Updated DPE clock switch function calls.
09/12/12   sl      Use macro-style DDR logging.
09/07/12   sl      Moved ddr_get_partition() to ddr_drivers.c.
09/06/12   sl      Added parameter tweaking functions.
08/17/12   sl      Removed ddr_register_fill().
08/08/12   dh      change CORE_CLK_PERIOD to 50 for bring up
07/24/12   tw      Removed bus configuration from one time settings
11/30/11   tw      Initial version.
================================================================================
                   Copyright 2011-2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_internal.h"
#include "ddr_sync.h"
#include "ddr_log.h"
#include "ddr_params.h"
#include "ddr_hwio.h"
#include "ddr_target.h"
#include "HAL_DDR.h"
#include "BIMC.h"
#include "SITE.h"
#include "ddr_config.h"
#include "ClockBoot.h"
#include "ddr_phy_ca_training_ddrss.h"
#include "ddr_phy_dq_rd_training_ddrss.h"
#include "ddr_phy_dq_wr_training_ddrss.h"
#include "bimc_scale.h"

/*==============================================================================
                                  MACROS
==============================================================================*/
/* Macro for round-up division */
#define div_ceil(n, d)  (((n) + (d) - 1) / (d))

#define EIGHT_SEGMENT_MASK 0xFF
#define FOUR_SEGMENT_MASK 0xF


/*==============================================================================
                                  DATA
==============================================================================*/
/* DDR interface status, keeps track of active interfaces and their status */
extern ddr_interface_state ddr_status;

/* DDR Partition information */
ddr_size_partition ddr_size_partition_info;

uint32 msm_revision = DDR_TLMM_HW_REV_ID_V10;
/*
uint32 phy_threshold[DDR_PHY_MAX_THRESHOLD_NUM] = 
{
  400000, 
  533000, 
  667000, 
  0       
};
*/

/*==============================================================================
                                  FUNCTIONS
==============================================================================*/
/* ============================================================================
**  Function : ddr_target_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called at the end of ddr init to initialize any 
*   target specific configurations 
*   
*   @details
*   Target specific configurations such as override of timing registers and 
*   calling target specific init functions will be captured in this api
*   
*   @param 
*   None
*   
*   @par Dependencies
*   
*   @par Side Effects
*   None
*   
*   @retval  None
*   
*   @sa None
*/

void ddr_target_init()
{
#ifdef BUILD_BOOT_CHAIN
  uint32 * ddr_training_cookie = (uint32 *)SHARED_IMEM_DDR_TRAINING_COOKIE;
  
  /* Read TLMM msm revision id */
  msm_revision = HWIO_INF(TLMM_HW_REVISION_NUMBER, VERSION_ID);
  
  /* Write to IMEM shared cookie to enable/disable training */
  *ddr_training_cookie = DDR_TRAINING_REQUIRED;
  
  /* BIMC MISC Static settings */
  ddr_set_config(0x0, ddr_bimc_misc_config_base, ddr_bimc_misc_config_delta);
  
#else
  /* Update partition information */
  ddr_size_partition_info = ddr_get_partition();

  /* Initialize for DDR DMM Resource */
  ddr_rpmfw_init();
#endif
}

/* ============================================================================
**  Function : ddr_post_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called after ddr is initialized. It will take care of any
*   post initialization activities such as ddr training.
*
*   @details
*   This function is called after ddr is initialized. It will take care of any
*   post initialization activities such as ddr training.
*
*   @param
*   boolean -
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

boolean ddr_post_init()
{
#ifdef FEATURE_DDR_PARAM_SAVE_TO_EMMC
  return TRUE;
#else
  return FALSE;
#endif
}

/* ============================================================================
**  Function : ddr_pre_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before clock switching occures.
*   The function will configure the ddr such that no data loss will occur
*   
*   @details
*   DDR will be stalled and new timing parameter is loaded into shadow.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*   
*   @param curr_clk   -   [IN] the current clock speed
*   @param new_clk    -  [IN] the clock speed we are switching to
*   @param new_clk    -  [IN] interface to switch clock for
*   
*   @par Dependencies
*   
*   
*   @par Side Effects
*   None
*   
*   @retval  None
*   
*   @sa None
*/

void ddr_pre_clock_switch(uint32 curr_clk, uint32 new_clk, SDRAM_INTERFACE interface_name)
{
} /* ddr_pre_clock_switch */

/* ============================================================================
**  Function : ddr_post_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after clock switching occurs.
*   The function will configure the ddr such that no data loss will occur
*   
*   @details
*   DDR will be unstalled.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*   
*   @param curr_clk          -  [IN] the current clock speed
*   @param new_clk           -  [IN] the clock speed we are switching to
*   @param interface_name    -  [IN] interface to switch clock for
*
*   @par Dependencies
*   This code has to be on IRAM because ddr is unavailable during clock switching
*   
*   @par Side Effects
*   None
*   
*   @retval  None
*   
*   @sa None
*/

void ddr_post_clock_switch(uint32 curr_clk, uint32 new_clk, SDRAM_INTERFACE interface_name)
{
} /* ddr_post_clock_switch */

/* ============================================================================
**  Function : ddr_pre_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_pre_vddmx_switch(uint32 vddmx_microvolts)
{
  /* Stepping Down in VDDCX voltage */
  ddr_status.vddmx_voltage = vddmx_microvolts;

} /* ddr_pre_vddmx_switch */

/* ============================================================================
**  Function : ddr_post_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddmx_switch(uint32 vddmx_microvolts)
{

} /* ddr_post_vddmx_switch */

/* ============================================================================
**  Function : ddr_pre_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before vddcx switch.
*
*   @param settings - contains the VDDCX voltage level we just switched to
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/
void ddr_pre_vddcx_switch(uint32 vddcx_microvolts)
{
} /* ddr_pre_vddcx_switch */

/* ============================================================================
**  Function : ddr_post_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after VDDCX is switched
*
*   @param none
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddcx_switch(uint32 vddcx_microvolts)
{
} /* ddr_post_vddcx_switch */

/* ============================================================================
**  Function : ddr_pre_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right before XO shutdown. Puts DDR into self refresh mode and
*   disables CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_post_xo_shutdown
*/

void ddr_pre_xo_shutdown(uint32 clk_speed)
{
  ddr_enter_self_refresh_all(clk_speed);

} /* ddr_pre_xo_shutdown */

/* ============================================================================
**  Function : ddr_post_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right after XO wakeup. Takes DDR out of self refresh mode and enables
*   CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_pre_xo_shutdown
*/

void ddr_post_xo_shutdown(uint32 clk_speed)
{
  ddr_exit_self_refresh_all(clk_speed);

} /* ddr_post_xo_shutdown */

/* ============================================================================
**  Function : ddr_check_partition
** ============================================================================
*/
/*!
*   @brief
*   A helper function for ddr_dmm_partition to check the given interface and partition
*   whether we can put into dpd or full\partial self refresh
*   
*   @details
*   Given the retention and active state, put the corresponding ddr interface 
*   into self refresh, or deep power down when possible     
*
*   Truth table for active and retention state:
*                        Active State:0         |      Active_state:1
*   Retention_state:0    self refresh/dpd       |      Invalid config, assume to be 
*                                               |      Active retention
*   Retention_state:1    self refresh retention |      Active Retention
*
*   @param interface_name   -  [IN] the interface to check for 
*   @param chip_sel         -  [IN] the chip select on the interface to check for 
*   @param retention_state  -  [IN] the retention state for the partitions given
*   @param active_state     -  [IN] the active state for the partitions given
*   @param num_partitions   -  [IN] the number of partitions on this interface
*   
*   @par Dependencies
*   None
*   
*   @par Side Effects
*   None
*   
*   @retval
*   None
*   
*   @sa None
*/
void ddr_check_partition(SDRAM_INTERFACE interface_name, SDRAM_CHIPSELECT chip_sel, uint32 retention_state, uint32 active_state, uint8 num_partitions)
{

} /* ddr_check_partition */

/* ============================================================================
**  Function : ddr_dmm_partition
** ============================================================================
*/
/*!
*   @brief
*   Given the retention and active state, put the corresponding ddr interface 
*   into self refresh, or deep power down when possible
*   
*   @details
*   Given the retention and active state, put the corresponding ddr interface 
*   into self refresh, or deep power down when possible     
*
*   Truth table for active and retention state:
*                        Active State:0         |      Active_state:1
*   Retention_state:0    self refresh/dpd       |      Invalid config, assume to be 
*                                               |      Active retention
*   Retention_state:1    self refresh retention |      Active Retention
*
*   @param retention_state  -  [IN] the retention state for the partitions given
*   @param active_state     -  [IN] the active state for the partitions given
*   
*   @par Dependencies
*   Caller of this API has to take care not to put ddr interface that is in use 
*   into self refresh or deep power down.
*   
*   @par Side Effects
*   None
*   
*   @retval
*  None
*   
*   @sa None
*/

void ddr_dmm_partition(uint32 retention_state, uint32 active_state)
{
}
/* =============================================================================
**  Function : ddr_params_is_training_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. LPDDR3 will always require training syncronization
*   to be done between rpm and sbl
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_required( void )
{
  //return TRUE;
  return FALSE;
} /* ddr_params_is_training_required */

/* =============================================================================
**  Function : ddr_params_is_training_on_rpm_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not in RPM. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. 
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_on_rpm_required( void )
{
  return FALSE;
} /* ddr_params_is_training_on_rpm_required */

/* =============================================================================
**  Function : ddr_pre_setup_for_training
** =============================================================================
*/
/**
*   @brief
*   Disable SCMO ranks, SHKE house-keeping , DPE CSPD & enable SCMO interrupts .
*
*   @param  None
*
*   @retval  None 
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_pre_setup_for_training(struct ddr_device_params_common *ddr_params_ch0,
		                   struct ddr_device_params_common *ddr_params_ch1)
{
  SDRAM_CHIPSELECT chip_select_ch0, chip_select_ch1;
  
  chip_select_ch0 = SDRAM_CS_NONE;
  if (ddr_params_ch0->num_banks_cs0 != 0)
  {
    chip_select_ch0 |= SDRAM_CS0;
  }
  if (ddr_params_ch0->num_banks_cs1 != 0)
  {
    chip_select_ch0 |= SDRAM_CS1;
  }

  chip_select_ch1 = SDRAM_CS_NONE;
  if (ddr_params_ch1->num_banks_cs0 != 0)
  {
    chip_select_ch1 |= SDRAM_CS0;
  }
  if (ddr_params_ch1->num_banks_cs1 != 0)
  {
    chip_select_ch1 |= SDRAM_CS1;
  }

  if (chip_select_ch0 != SDRAM_CS_NONE)
  {
    /* Disable SCMO ranks */
   HAL_SDRAM_SCMO_Rank_Disable(SDRAM_INTERFACE_0, chip_select_ch0);

    /* Disable SHKE house-keeping */
   HAL_SDRAM_SHKE_Disable_Auto_Refresh(SDRAM_INTERFACE_0, chip_select_ch0);
   HAL_SDRAM_SHKE_Disable_Periodic_SRR(SDRAM_INTERFACE_0, chip_select_ch0);
   HAL_SDRAM_SHKE_Disable_ZQCAL(SDRAM_INTERFACE_0, chip_select_ch0);
   HAL_SDRAM_SHKE_Disable_HW_Self_Refresh(SDRAM_INTERFACE_0, chip_select_ch0);


    /* Disable DPE CSPD */
    BOOT_HWIO_OUTM(DDR_ADDR(DPE_PWR_CTRL_0), SDRAM_0_OFFSET,
                   HWIO_FMSK(DDR_ADDR(DPE_PWR_CTRL_0), CLK_STOP_PWR_DN_EN),
                   0x0U << HWIO_SHFT(DDR_ADDR(DPE_PWR_CTRL_0), CLK_STOP_PWR_DN_EN));
    BOOT_HWIO_OUTM(DDR_ADDR(DPE_CONFIG_4), SDRAM_0_OFFSET,
                   HWIO_FMSK(DDR_ADDR(DPE_CONFIG_4), LOAD_ALL_CONFIG),
                   0x1U << HWIO_SHFT(DDR_ADDR(DPE_CONFIG_4), LOAD_ALL_CONFIG));

    /* Enable SCMO interrupt */
    BOOT_HWIO_OUTM(DDR_ADDR(SCMO_INTERRUPT_ENABLE), SDRAM_0_OFFSET,
                   HWIO_FMSK(DDR_ADDR(SCMO_INTERRUPT_ENABLE), IRQ_EN),
                   0x1U << HWIO_SHFT(DDR_ADDR(SCMO_INTERRUPT_ENABLE), IRQ_EN));
  }
  if (chip_select_ch1 != SDRAM_CS_NONE)
  {
    /* Disable SCMO ranks */
    HAL_SDRAM_SCMO_Rank_Disable(SDRAM_INTERFACE_1, chip_select_ch1);

        /* Disable SHKE house-keeping */
   HAL_SDRAM_SHKE_Disable_Auto_Refresh(SDRAM_INTERFACE_1, chip_select_ch1);
   HAL_SDRAM_SHKE_Disable_Periodic_SRR(SDRAM_INTERFACE_1, chip_select_ch1);
   HAL_SDRAM_SHKE_Disable_ZQCAL(SDRAM_INTERFACE_1, chip_select_ch1);
   HAL_SDRAM_SHKE_Disable_HW_Self_Refresh(SDRAM_INTERFACE_1, chip_select_ch1);



    /* Disable DPE CSPD */
    BOOT_HWIO_OUTM(DDR_ADDR(DPE_PWR_CTRL_0), SDRAM_1_OFFSET,
                   HWIO_FMSK(DDR_ADDR(DPE_PWR_CTRL_0), CLK_STOP_PWR_DN_EN),
                   0x0U << HWIO_SHFT(DDR_ADDR(DPE_PWR_CTRL_0), CLK_STOP_PWR_DN_EN));
    BOOT_HWIO_OUTM(DDR_ADDR(DPE_CONFIG_4), SDRAM_1_OFFSET,
                   HWIO_FMSK(DDR_ADDR(DPE_CONFIG_4), LOAD_ALL_CONFIG),
                   0x1U << HWIO_SHFT(DDR_ADDR(DPE_CONFIG_4), LOAD_ALL_CONFIG));

    /* Enable SCMO interrupt */
    BOOT_HWIO_OUTM(DDR_ADDR(SCMO_INTERRUPT_ENABLE), SDRAM_1_OFFSET,
                   HWIO_FMSK(DDR_ADDR(SCMO_INTERRUPT_ENABLE), IRQ_EN),
                   0x1U << HWIO_SHFT(DDR_ADDR(SCMO_INTERRUPT_ENABLE), IRQ_EN));
  }
}

/* =============================================================================
**  Function : ddr_post_setup_for_training
** =============================================================================
*/
/**
*   @brief
*   Enable SCMO ranks, SHKE house-keeping , DPE CSPD & Disable SCMO interrupts .
*
*   @param  None
*
*   @retval  None 
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/

void ddr_post_setup_for_training(struct ddr_device_params_common *ddr_params_ch0,
		                    struct ddr_device_params_common *ddr_params_ch1)
{
  SDRAM_CHIPSELECT chip_select_ch0, chip_select_ch1;
  
  chip_select_ch0 = SDRAM_CS_NONE;
  if (ddr_params_ch0->num_banks_cs0 != 0)
  {
    chip_select_ch0 |= SDRAM_CS0;
  }
  if (ddr_params_ch0->num_banks_cs1 != 0)
  {
    chip_select_ch0 |= SDRAM_CS1;
  }

  chip_select_ch1 = SDRAM_CS_NONE;
  if (ddr_params_ch1->num_banks_cs0 != 0)
  {
    chip_select_ch1 |= SDRAM_CS0;
  }
  if (ddr_params_ch1->num_banks_cs1 != 0)
  {
    chip_select_ch1 |= SDRAM_CS1;
  }

  if (chip_select_ch0 != SDRAM_CS_NONE)
  {
    /* Check if any SCMO error occurs */
    while ( BOOT_HWIO_INM(DDR_ADDR(SCMO_ESYN_APACKET_1), SDRAM_0_OFFSET,
                          HWIO_FMSK(DDR_ADDR(SCMO_ESYN_APACKET_1), ERR_CODE)) )
    {}

    /* Disable SCMO interrupt */
    BOOT_HWIO_OUTM(DDR_ADDR(SCMO_INTERRUPT_ENABLE), SDRAM_0_OFFSET,
                   HWIO_FMSK(DDR_ADDR(SCMO_INTERRUPT_ENABLE), IRQ_EN),
                   0x0U << HWIO_SHFT(DDR_ADDR(SCMO_INTERRUPT_ENABLE), IRQ_EN));

    /* Enable SHKE house-keeping */
    HAL_SDRAM_SHKE_Enable_Auto_Refresh(SDRAM_INTERFACE_0, chip_select_ch0);
    HAL_SDRAM_SHKE_Enable_Periodic_SRR(SDRAM_INTERFACE_0, chip_select_ch0);
    HAL_SDRAM_SHKE_Enable_HW_Self_Refresh(SDRAM_INTERFACE_0, chip_select_ch0);

    /* Enable DPE CSPD */
    BOOT_HWIO_OUTM(DDR_ADDR(DPE_PWR_CTRL_0), SDRAM_0_OFFSET,
                   HWIO_FMSK(DDR_ADDR(DPE_PWR_CTRL_0), CLK_STOP_PWR_DN_EN),
                   0x1U << HWIO_SHFT(DDR_ADDR(DPE_PWR_CTRL_0), CLK_STOP_PWR_DN_EN));
    BOOT_HWIO_OUTM(DDR_ADDR(DPE_CONFIG_4), SDRAM_0_OFFSET,
                   HWIO_FMSK(DDR_ADDR(DPE_CONFIG_4), LOAD_ALL_CONFIG),
                   0x1U << HWIO_SHFT(DDR_ADDR(DPE_CONFIG_4), LOAD_ALL_CONFIG));

    /* Enable SCMO ranks */
    HAL_SDRAM_SCMO_Rank_Enable(SDRAM_INTERFACE_0, chip_select_ch0);
  }
  if (chip_select_ch1 != SDRAM_CS_NONE)
  {
    /* Check if any SCMO error occurs */
    while ( BOOT_HWIO_INM(DDR_ADDR(SCMO_ESYN_APACKET_1), SDRAM_1_OFFSET,
                          HWIO_FMSK(DDR_ADDR(SCMO_ESYN_APACKET_1), ERR_CODE)) )
    {}

    /* Disable SCMO interrupt */
    BOOT_HWIO_OUTM(DDR_ADDR(SCMO_INTERRUPT_ENABLE), SDRAM_1_OFFSET,
                   HWIO_FMSK(DDR_ADDR(SCMO_INTERRUPT_ENABLE), IRQ_EN),
                   0x0U << HWIO_SHFT(DDR_ADDR(SCMO_INTERRUPT_ENABLE), IRQ_EN));

    /* Enable SHKE house-keeping */
    HAL_SDRAM_SHKE_Enable_Auto_Refresh(SDRAM_INTERFACE_1, chip_select_ch1);
    HAL_SDRAM_SHKE_Enable_Periodic_SRR(SDRAM_INTERFACE_1, chip_select_ch1);
    HAL_SDRAM_SHKE_Enable_HW_Self_Refresh(SDRAM_INTERFACE_1, chip_select_ch1);

    /* Enable DPE CSPD */
    BOOT_HWIO_OUTM(DDR_ADDR(DPE_PWR_CTRL_0), SDRAM_1_OFFSET,
                   HWIO_FMSK(DDR_ADDR(DPE_PWR_CTRL_0), CLK_STOP_PWR_DN_EN),
                   0x1U << HWIO_SHFT(DDR_ADDR(DPE_PWR_CTRL_0), CLK_STOP_PWR_DN_EN));
    BOOT_HWIO_OUTM(DDR_ADDR(DPE_CONFIG_4), SDRAM_1_OFFSET,
                   HWIO_FMSK(DDR_ADDR(DPE_CONFIG_4), LOAD_ALL_CONFIG),
                   0x1U << HWIO_SHFT(DDR_ADDR(DPE_CONFIG_4), LOAD_ALL_CONFIG));

    /* Enable SCMO ranks */
    HAL_SDRAM_SCMO_Rank_Enable(SDRAM_INTERFACE_1, chip_select_ch1);
  }
}

/* ============================================================================
**  Function : ddr_phy_training
** ============================================================================
*/
/*!
*   @brief
*   This function will train the ddr phy at the highest frequency supported.
*
*   @details
*   This function will train the ddr phy at the highest frequency supported.
*   the rest of frequencies will be interpolated.
*
*   @param interface_name     -  [IN] the interface to train for
*   @param chip_select        -  [IN] the chip select to train for
*   @param training_params    -  [IN/OUT] pointer to training parameter to
*                                         populate trained results.
*
*   @par Dependencies
*
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/
void ddr_phy_training(SDRAM_INTERFACE interface_name,
    SDRAM_CHIPSELECT chip_select,
    struct ddr_trained_params_lpddr3 *training_params)
{
  uint32 phy_base = 0xFC4E0000;
  uint32 bimc_base = 0xFC380000;
  /*uint32 dq_rd_training_values[32];
  uint16 dq_read_training[4][2];
  uint32 dq_read_perbit_training[2][4][2]; */
  uint16 ca_training_full_cdc[2];
  uint16 ca_training_perbit[2][2]; 
/*  uint16 dq_wr_training_values[32];
  uint16 dq_write_training[4][2];
  uint32 dq_write_perbit_training[4][2];
  uint32 dq_write_perbit_dm_training[4]; */

  //phy_threshold[DDR_PHY_MAX_THRESHOLD_NUM - 1] = training_params->freq[training_params->nlevels - 1];

  /* perform DCC training */

#if 0

  HAL_SDRAM_PHY_DCC_Calibration(interface_name, DDR_DCC1_OFFSET, dcc_result[1]);
  HAL_SDRAM_PHY_DCC_Calibration(interface_name, DDR_DCC0_OFFSET, dcc_result[0]);
#endif
  DDRSS_ddr_phy_ca_training( phy_base, 
		             interface_name, 
			     chip_select, 
			     0,                                   /*fine_delay_training*/
			     0,                                   /*per_bit_deskew_training*/
			     ca_training_full_cdc, 
			     ca_training_perbit, 
			     128,                                 /*max_loop_count*/
			     0,                                   /*coarse_init_val*/
			     0,                                   /*fine_init_val*/
			     0);                                  /*per_bit_deskew_init_val*/

#if 0  
  
  //while(!HAL_SDRAM_Write_Leveling(interface_name, chip_select));
  //dog_kick();
#endif

  BIMC_Enable_Auto_Refresh(bimc_base, interface_name, chip_select);
  BIMC_Enable_Periodic_SRR(bimc_base, interface_name, chip_select);
  BIMC_Enable_Periodic_ZQCAL(bimc_base, interface_name, chip_select);
  BIMC_Enable_HW_Self_Refresh(bimc_base, interface_name, chip_select, 0 /*uint32 concurrent_sr*/);

 
#if 0 
  Clock_SetBIMCSpeed(200000);
  /* Prepare ddr training pattern for PHY training 
   * before we bump up clock speed 
   */
  HAL_SDRAM_Apply_Phy_Training_Pattern(interface_name, chip_select, TRUE);
  Clock_SetBIMCSpeed(pClockPlan[nNumLevel-1].nFreqKHz);
#endif

#if 0
  DDRSS_ddr_phy_dq_rd_training_ddrss( phy_base, 
  		                      interface_name, 
  		                      1, /*rank = 0*/ 
		                      0, 
		                      0, 
		                      128, 
		                      0, 
		                      0, 
		                      0, 
		                      dq_rd_training_values, 
		                      dq_read_training, 
		                      dq_read_perbit_training );
  
 
  DDRSS_ddr_phy_dq_wr_training_ddrss( phy_base, 
		                      interface_name, 
				      1, /* rank=0*/ 
				      0, 
				      0, 
				      128, 
				      0, 
				      0, 
				      0, 
				      dq_wr_training_values, 
				      dq_write_training, 
				      dq_write_perbit_training, 
				      dq_write_perbit_dm_training);
#endif
#if 0 
  //while(!HAL_SDRAM_Write_Leveling(interface_name, chip_select));
  //dog_kick();
  
  /* Save DCC result */
  for(i = 0; i < 5; i++)
  {
    training_params->phy_dcc[i].dcc0 = dcc_result[0][i];
    training_params->phy_dcc[i].dcc1 = dcc_result[1][i];
  }  
  
  /* Save PHY Training Result*/
  training_params->phy_params.ca_coarse_dly = ca_trained_delay;
  training_params->phy_params.ca_fine_dly = ca_trained_fine_delay;

  /* for each DQ byte, DQ0,1,2,3 */
  for (j = 0; j < 4; j++)
  {
    training_params->phy_params.read_coarse_delay[j] = read_training_coarse[j];
    training_params->phy_params.read_fine_delay[j] = read_training_fine[j];

    training_params->phy_params.write_coarse_delay[j] = write_training_coarse[j];
    training_params->phy_params.write_fine_delay[j] = write_training_fine[j];
  }
#endif
}


/* =============================================================================
**  Function : ddr_do_phy_training
** =============================================================================
*/
/**
*   @brief
*   Indicates that PHY training needs to be done in SBL1.
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_do_phy_training( void )
{ 
  SDRAM_CHIPSELECT chip_select_ch0, chip_select_ch1;
  struct ddr_device_params_common *ddr_params_ch0, *ddr_params_ch1;
  struct ddr_trained_params_lpddr3 *training_params_ch0, *training_params_ch1;
  uint32          nNumLevel;
  ClockPlanType   *pClockPlan;

  /* Get DDR Training parameters */
  training_params_ch0 = &(ddr_get_trained_params(SDRAM_INTERFACE_0)->lpddr3_training);
  training_params_ch1 = &(ddr_get_trained_params(SDRAM_INTERFACE_1)->lpddr3_training);

  if(training_params_ch0 == NULL || training_params_ch1 == NULL)
  {
    return FALSE;
  }

  memset((void *)training_params_ch0, 0, sizeof(*training_params_ch0));
  memset((void *)training_params_ch1, 0, sizeof(*training_params_ch1));

  /* Get DDR device parameters */
  ddr_params_ch0 = &(ddr_get_params(SDRAM_INTERFACE_0)->common);
  ddr_params_ch1 = &(ddr_get_params(SDRAM_INTERFACE_1)->common);

  if(ddr_params_ch0 == NULL || ddr_params_ch1 == NULL)
  {
    return FALSE;
  }

  /* Pre setup for training */
  ddr_pre_setup_for_training(ddr_params_ch0,ddr_params_ch1);
  
  /* Call BIMCQuery to get the Num Perf Levels */
  Clock_BIMCQuery(CLOCK_RESOURCE_QUERY_NUM_PERF_LEVELS,&nNumLevel);

  /* Call BIMCQuery to get the Clock Plan */
  Clock_BIMCQuery(CLOCK_RESOURCE_QUERY_ALL_FREQ_KHZ,pClockPlan);

  DALSYS_Malloc(nNumLevel * sizeof(ClockPlanType), (void **)&pClockPlan);

  Clock_SetBIMCSpeed(pClockPlan[nNumLevel-1].nFreqKHz);
  
#if 0
  if ( HWIO_INF(TLMM_HW_REVISION_NUMBER, VERSION_ID) >= DDR_TLMM_HW_REV_ID_V12 ||
       HWIO_INF(TLMM_HW_REVISION_NUMBER, PRODUCT_DEVICE_ID) >= DDR_TLMM_HW_DEVICE_ID_PRO )
  {
#endif
    /* Kick the watchdog */
   // dog_kick();


  chip_select_ch0 = SDRAM_CS_NONE;
  if (ddr_params_ch0->num_banks_cs0 != 0)
  {
    chip_select_ch0 |= SDRAM_CS0;
  }
  if (ddr_params_ch0->num_banks_cs1 != 0)
  {
    chip_select_ch0 |= SDRAM_CS1;
  }

  chip_select_ch1 = SDRAM_CS_NONE;
  if (ddr_params_ch1->num_banks_cs0 != 0)
  {
    chip_select_ch1 |= SDRAM_CS0;
  }
  if (ddr_params_ch1->num_banks_cs1 != 0)
  {
    chip_select_ch1 |= SDRAM_CS1;
  }

  /* disable the wdog timer in boot while training */

   HWIO_OUT(MPM2_WDOG_CTL_REG, 0x0);

    /* at this point we should be at the highest frequency and voltage level,
     * start training for DCC and PHY
     */
    if (chip_select_ch0 != SDRAM_CS_NONE)
    {
      /* Enable SCMO ranks */
      HAL_SDRAM_SCMO_Rank_Enable(SDRAM_INTERFACE_0, chip_select_ch0);

      /* Train the PHY */
      ddr_phy_training(SDRAM_INTERFACE_0, chip_select_ch0, training_params_ch0);

     // dog_kick();

      /* Disable SCMO ranks */
      HAL_SDRAM_SCMO_Rank_Disable(SDRAM_INTERFACE_0, chip_select_ch0);
    }

    if (chip_select_ch1 != SDRAM_CS_NONE)
    {
      /* Enable SCMO ranks */
      HAL_SDRAM_SCMO_Rank_Enable(SDRAM_INTERFACE_1, chip_select_ch1);

      /* Train PHY */
      ddr_phy_training(SDRAM_INTERFACE_1, chip_select_ch1, training_params_ch1);

      /* Enable SCMO ranks */
      HAL_SDRAM_SCMO_Rank_Disable(SDRAM_INTERFACE_1, chip_select_ch1);
    }

    /* Update configuration registers with newly trained values */
#if 0
    HAL_SDRAM_PHY_Apply_Training_Parameters(SDRAM_INTERFACE_0);
    HAL_SDRAM_PHY_Apply_Training_Parameters(SDRAM_INTERFACE_1);
#endif 

//    dog_kick();
#if 0
  }
#endif

  /* enable the wdog timer after training */
   HWIO_OUT(MPM2_WDOG_CTL_REG, 0x0);


  /* Post setup for training */
  ddr_post_setup_for_training(ddr_params_ch0,ddr_params_ch1);


  /* update training cookie */
  ddr_set_training_cookie(DDR_PARAMS_MAGIC_NUM);

  DALSYS_Free(pClockPlan);

  return TRUE;

} /* ddr_do_phy_training */

