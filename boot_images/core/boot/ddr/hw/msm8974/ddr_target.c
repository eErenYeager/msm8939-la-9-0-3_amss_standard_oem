/**
 * @file ddr_target.c
 * @brief
 * Target specific DDR drivers.
 */
/*==============================================================================
                                EDIT HISTORY

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/boot/ddr/hw/msm8974/ddr_target.c#1 $
$DateTime: 2015/03/19 01:58:37 $
$Author: pwbldsvc $
================================================================================
when       who     what, where, why
--------   ---     -------------------------------------------------------------
09/05/13   sr      changes to not support training in boot.
06/21/13   sr      Code cleanup after using latest updated script to generate reg address.
06/07/13   rj      Added ddr training check function.
05/13/13   sl      Updated delta table calls.
03/01/13   tw      Added calling delta table
11/02/12   tw      re-enabled 800mhz, added site initialization
11/02/12   tw      enabled flag for training required
11/02/12   sy      Back out 800Mhz
09/21/12   sl      Combined LPDDR device parameters.
09/17/12   sl      Updated DPE clock switch function calls.
09/12/12   sl      Use macro-style DDR logging.
09/07/12   sl      Moved ddr_get_partition() to ddr_drivers.c.
09/06/12   sl      Added parameter tweaking functions.
08/17/12   sl      Removed ddr_register_fill().
08/08/12   dh      change CORE_CLK_PERIOD to 50 for bring up
07/24/12   tw      Removed bus configuration from one time settings
11/30/11   tw      Initial version.
================================================================================
                   Copyright 2011-2013 Qualcomm Technologies Incorporated
                              All Rights Reserved
                     Qualcomm Confidential and Proprietary
==============================================================================*/
/*==============================================================================
                                  INCLUDES
==============================================================================*/
#include "ddr_common.h"
#include "ddr_drivers.h"
#include "ddr_internal.h"
#include "ddr_sync.h"
#include "ddr_log.h"
#include "ddr_params.h"
#include "ddr_hwio.h"
#include "ddr_target.h"
#include "HAL_DDR.h"
#include "BIMC.h"
#include "SITE.h"
#include "ddr_config.h"



/*==============================================================================
                                  MACROS
==============================================================================*/
/* Macro for round-up division */
#define div_ceil(n, d)  (((n) + (d) - 1) / (d))

#define EIGHT_SEGMENT_MASK 0xFF
#define FOUR_SEGMENT_MASK 0xF


/*==============================================================================
                                  DATA
==============================================================================*/
/* DDR interface status, keeps track of active interfaces and their status */
extern ddr_interface_state ddr_status;

/* DDR Partition information */
ddr_size_partition ddr_size_partition_info;


/*==============================================================================
                                  FUNCTIONS
==============================================================================*/

/* ============================================================================
**  Function : ddr_target_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called at the end of ddr init to initialize any 
*   target specific configurations 
*   
*   @details
*   Target specific configurations such as override of timing registers and 
*   calling target specific init functions will be captured in this api
*   
*   @param 
*   None
*   
*   @par Dependencies
*   
*   @par Side Effects
*   None
*   
*   @retval  None
*   
*   @sa None
*/

void ddr_target_init()
{
#ifdef BUILD_BOOT_CHAIN
  uint32 * ddr_training_cookie = (uint32 *)SHARED_IMEM_DDR_TRAINING_COOKIE;
  
    
  /* Write to IMEM shared cookie to enable/disable training */
  *ddr_training_cookie = DDR_TRAINING_REQUIRED;
  
    /* BIMC MISC Static settings */
  ddr_set_config(0x0, ddr_bimc_misc_config_base, ddr_bimc_misc_config_delta);

  /* SITE initialization */
  SITE_Initialization(SDRAM_INTERFACE_0);
  SITE_Initialization(SDRAM_INTERFACE_1);
  
#else
  /* Update partition information */
  ddr_size_partition_info = ddr_get_partition();

  /* Initialize for DDR DMM Resource */
  ddr_rpmfw_init();
#endif
}

/* ============================================================================
**  Function : ddr_post_init
** ============================================================================
*/
/*!
*   @brief
*   This function is called after ddr is initialized. It will take care of any
*   post initialization activities such as ddr training.
*
*   @details
*   This function is called after ddr is initialized. It will take care of any
*   post initialization activities such as ddr training.
*
*   @param
*   boolean -
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval  None
*
*   @sa None
*/

boolean ddr_post_init()
{
  volatile uint32 * ddr_training_cookie = (uint32 *)SHARED_IMEM_DDR_TRAINING_COOKIE;

  if(*ddr_training_cookie == DDR_TRAINING_NOT_REQUIRED)
  {
    return FALSE;
  }
  else
  {
    while( ((*ddr_training_cookie) != DDR_TRAINING_DATA_UPDATED) &&
           ((*ddr_training_cookie) != DDR_TRAINING_DATA_NOT_UPDATED));

    return ((*ddr_training_cookie) == DDR_TRAINING_DATA_UPDATED);
  }
}

/* ============================================================================
**  Function : ddr_pre_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before clock switching occures.
*   The function will configure the ddr such that no data loss will occur
*   
*   @details
*   DDR will be stalled and new timing parameter is loaded into shadow.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*   
*   @param curr_clk   -   [IN] the current clock speed
*   @param new_clk    -  [IN] the clock speed we are switching to
*   @param new_clk    -  [IN] interface to switch clock for
*   
*   @par Dependencies
*   
*   
*   @par Side Effects
*   None
*   
*   @retval  None
*   
*   @sa None
*/

void ddr_pre_clock_switch(uint32 curr_clk, uint32 new_clk, SDRAM_INTERFACE interface_name)
{
} /* ddr_pre_clock_switch */

/* ============================================================================
**  Function : ddr_post_clock_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after clock switching occurs.
*   The function will configure the ddr such that no data loss will occur
*   
*   @details
*   DDR will be unstalled.
*   Depending on bumping up or stepping down clock speed, we will load the
*   shadow register value to actual registers before or after clock switch
*   has occurred.
*   
*   @param curr_clk          -  [IN] the current clock speed
*   @param new_clk           -  [IN] the clock speed we are switching to
*   @param interface_name    -  [IN] interface to switch clock for
*
*   @par Dependencies
*   This code has to be on IRAM because ddr is unavailable during clock switching
*   
*   @par Side Effects
*   None
*   
*   @retval  None
*   
*   @sa None
*/

void ddr_post_clock_switch(uint32 curr_clk, uint32 new_clk, SDRAM_INTERFACE interface_name)
{
} /* ddr_post_clock_switch */

/* ============================================================================
**  Function : ddr_pre_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_pre_vddmx_switch(uint32 vddmx_microvolts)
{
  /* Stepping Down in VDDCX voltage */
  ddr_status.vddmx_voltage = vddmx_microvolts;

} /* ddr_pre_vddmx_switch */

/* ============================================================================
**  Function : ddr_post_vddmx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after voltage switch occurs.
*
*   @param vddmx_microvolts - vddmx voltage in microvolts
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddmx_switch(uint32 vddmx_microvolts)
{

} /* ddr_post_vddmx_switch */

/* ============================================================================
**  Function : ddr_pre_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right before vddcx switch.
*
*   @param settings - contains the VDDCX voltage level we just switched to
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/
void ddr_pre_vddcx_switch(uint32 vddcx_microvolts)
{
} /* ddr_pre_vddcx_switch */

/* ============================================================================
**  Function : ddr_post_vddcx_switch
** ============================================================================
*/
/*!
*   @brief
*   This function is called right after VDDCX is switched
*
*   @param none
*
*   @par Dependencies
*
*   @par Side Effects
*   None
*
*   @retval None
*
*   @sa None
*/

void ddr_post_vddcx_switch(uint32 vddcx_microvolts)
{
} /* ddr_post_vddcx_switch */

/* ============================================================================
**  Function : ddr_pre_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right before XO shutdown. Puts DDR into self refresh mode and
*   disables CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_post_xo_shutdown
*/

void ddr_pre_xo_shutdown(uint32 clk_speed)
{
  ddr_enter_self_refresh_all(clk_speed);

} /* ddr_pre_xo_shutdown */

/* ============================================================================
**  Function : ddr_post_xo_shutdown
** ============================================================================
*/
/**
*   @brief
*   Called right after XO wakeup. Takes DDR out of self refresh mode and enables
*   CDC and I/O calibration.
*
*   @param[in]  clk_speed    Current clock speed
*
*   @return
*   None
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   ddr_pre_xo_shutdown
*/

void ddr_post_xo_shutdown(uint32 clk_speed)
{
  ddr_exit_self_refresh_all(clk_speed);

} /* ddr_post_xo_shutdown */

/* ============================================================================
**  Function : ddr_check_partition
** ============================================================================
*/
/*!
*   @brief
*   A helper function for ddr_dmm_partition to check the given interface and partition
*   whether we can put into dpd or full\partial self refresh
*   
*   @details
*   Given the retention and active state, put the corresponding ddr interface 
*   into self refresh, or deep power down when possible     
*
*   Truth table for active and retention state:
*                        Active State:0         |      Active_state:1
*   Retention_state:0    self refresh/dpd       |      Invalid config, assume to be 
*                                               |      Active retention
*   Retention_state:1    self refresh retention |      Active Retention
*
*   @param interface_name   -  [IN] the interface to check for 
*   @param chip_sel         -  [IN] the chip select on the interface to check for 
*   @param retention_state  -  [IN] the retention state for the partitions given
*   @param active_state     -  [IN] the active state for the partitions given
*   @param num_partitions   -  [IN] the number of partitions on this interface
*   
*   @par Dependencies
*   None
*   
*   @par Side Effects
*   None
*   
*   @retval
*   None
*   
*   @sa None
*/
void ddr_check_partition(SDRAM_INTERFACE interface_name, SDRAM_CHIPSELECT chip_sel, uint32 retention_state, uint32 active_state, uint8 num_partitions)
{

 uint32 paritition_mask =  (num_partitions == 8) ? EIGHT_SEGMENT_MASK : FOUR_SEGMENT_MASK;
 uint32 pasr_status = (retention_state | active_state) & paritition_mask;;

 if(pasr_status == 0)
 {
   /* If not already in deep power down */
   if((interface_name == SDRAM_INTERFACE_0 && chip_sel == SDRAM_CS0 && ddr_status.sdram0_cs0 == DDR_ACTIVE) ||
      (interface_name == SDRAM_INTERFACE_0 && chip_sel == SDRAM_CS1 && ddr_status.sdram0_cs1 == DDR_ACTIVE) ||
      (interface_name == SDRAM_INTERFACE_1 && chip_sel == SDRAM_CS0 && ddr_status.sdram1_cs0 == DDR_ACTIVE) ||
      (interface_name == SDRAM_INTERFACE_1 && chip_sel == SDRAM_CS1 && ddr_status.sdram1_cs1 == DDR_ACTIVE))
      {
         /* Deep power down is possible on this interface */
         ddr_enter_deep_power_down(interface_name, chip_sel);        
      }   
 }
 else
 {
   if((interface_name == SDRAM_INTERFACE_0 && chip_sel == SDRAM_CS0 && ddr_status.sdram0_cs0 == DDR_DPD) ||
      (interface_name == SDRAM_INTERFACE_0 && chip_sel == SDRAM_CS1 && ddr_status.sdram0_cs1 == DDR_DPD) ||
      (interface_name == SDRAM_INTERFACE_1 && chip_sel == SDRAM_CS0 && ddr_status.sdram1_cs0 == DDR_DPD) ||
      (interface_name == SDRAM_INTERFACE_1 && chip_sel == SDRAM_CS1 && ddr_status.sdram1_cs1 == DDR_DPD))
   {
     ddr_exit_deep_power_down(interface_name, chip_sel, ddr_status.clk_speed);
   }   
    
   /* 
    Definition for segment mask in JEDEC MR17 is 
      0:refresh enable to the segment (=unmasked, default)
      1:refresh blocked (=masked)   
    so we must inverse and mask for the segments.  
   */
   pasr_status = (~pasr_status) & paritition_mask;
   /* Update the partial array self refresh status in ddr to reflect this */
   HAL_SDRAM_Write_MR(interface_name, chip_sel, 17, pasr_status);
   
   if( (active_state & paritition_mask) == 0 )
   {
     /* interface is inactive, put it into self refresh */
     ddr_enter_self_refresh(interface_name, chip_sel, 0);
   }
   else
   {
     if((interface_name == SDRAM_INTERFACE_0 && chip_sel == SDRAM_CS0 && ddr_status.sdram0_cs0 == DDR_SELF_REFRESH) ||
        (interface_name == SDRAM_INTERFACE_0 && chip_sel == SDRAM_CS1 && ddr_status.sdram0_cs1 == DDR_SELF_REFRESH) ||
        (interface_name == SDRAM_INTERFACE_1 && chip_sel == SDRAM_CS0 && ddr_status.sdram1_cs0 == DDR_SELF_REFRESH) ||
        (interface_name == SDRAM_INTERFACE_1 && chip_sel == SDRAM_CS1 && ddr_status.sdram1_cs1 == DDR_SELF_REFRESH))
      {
        ddr_exit_self_refresh(interface_name, chip_sel, 0);
      }
   }
 }
}

/* ============================================================================
**  Function : ddr_dmm_partition
** ============================================================================
*/
/*!
*   @brief
*   Given the retention and active state, put the corresponding ddr interface 
*   into self refresh, or deep power down when possible
*   
*   @details
*   Given the retention and active state, put the corresponding ddr interface 
*   into self refresh, or deep power down when possible     
*
*   Truth table for active and retention state:
*                        Active State:0         |      Active_state:1
*   Retention_state:0    self refresh/dpd       |      Invalid config, assume to be 
*                                               |      Active retention
*   Retention_state:1    self refresh retention |      Active Retention
*
*   @param retention_state  -  [IN] the retention state for the partitions given
*   @param active_state     -  [IN] the active state for the partitions given
*   
*   @par Dependencies
*   Caller of this API has to take care not to put ddr interface that is in use 
*   into self refresh or deep power down.
*   
*   @par Side Effects
*   None
*   
*   @retval
*  None
*   
*   @sa None
*/

void ddr_dmm_partition(uint32 retention_state, uint32 active_state)
{
  uint8 rank_shift = 0;
  uint8 ddr_interleave_status = ddr_param_interleaved();

  if (ddr_interleave_status & SDRAM_CS0)
  {
    if (ddr_size_partition_info.sdram0_cs0.num_partitions != 0)
    {
      ddr_check_partition(SDRAM_INTERFACE_0, SDRAM_CS0, retention_state,
          active_state, ddr_size_partition_info.sdram0_cs0.num_partitions);

      ddr_check_partition(SDRAM_INTERFACE_1, SDRAM_CS0, retention_state,
          active_state, ddr_size_partition_info.sdram0_cs0.num_partitions);

      rank_shift += ddr_size_partition_info.sdram0_cs0.num_partitions;
    }
  }
  else
  {
    /* SDRAM INTERFACE 0 - SDRAM_CS0 */
    if (ddr_size_partition_info.sdram0_cs0.num_partitions != 0)
    {
      ddr_check_partition(SDRAM_INTERFACE_0, SDRAM_CS0, retention_state,
          active_state, ddr_size_partition_info.sdram0_cs0.num_partitions);

      rank_shift += ddr_size_partition_info.sdram0_cs0.num_partitions;
    }

    /* SDRAM INTERFACE 1 - SDRAM_CS0 */
    if (ddr_size_partition_info.sdram1_cs0.num_partitions != 0)
    {
      /*
       * If SDRAM INTERfACE 0 SDRAM CS1 is populated, we must shift
       * the rank shift by that number of partitions because the definition
       * is done by memory addressing, which is sequentially down from
       * CS0 to CS1
       */
      ddr_check_partition(
          SDRAM_INTERFACE_1,
          SDRAM_CS0,
          retention_state
              >> (rank_shift + ddr_size_partition_info.sdram0_cs1.num_partitions),
          active_state
              >> (rank_shift + ddr_size_partition_info.sdram0_cs1.num_partitions),
          ddr_size_partition_info.sdram1_cs0.num_partitions);
    }
  }

  if (ddr_interleave_status & SDRAM_CS1)
  {
    if (ddr_size_partition_info.sdram0_cs1.num_partitions != 0)
    {
      ddr_check_partition(SDRAM_INTERFACE_0, SDRAM_CS1,
          retention_state >> rank_shift, active_state >> rank_shift,
          ddr_size_partition_info.sdram0_cs1.num_partitions);

      ddr_check_partition(SDRAM_INTERFACE_1, SDRAM_CS1,
          retention_state >> rank_shift, active_state >> rank_shift,
          ddr_size_partition_info.sdram0_cs1.num_partitions);
    }
  }
  else
  {
    /* SDRAM INTERFACE 0 - SDRAM_CS1 */
    if (ddr_size_partition_info.sdram0_cs1.num_partitions != 0)
    {
      ddr_check_partition(SDRAM_INTERFACE_0, SDRAM_CS1,
          retention_state >> rank_shift, active_state >> rank_shift,
          ddr_size_partition_info.sdram0_cs1.num_partitions);
      rank_shift += ddr_size_partition_info.sdram0_cs1.num_partitions;
    }

    /* SDRAM INTERFACE 1 - SDRAM_CS1 */
    if (ddr_size_partition_info.sdram1_cs1.num_partitions != 0)
    {
      ddr_check_partition(
          SDRAM_INTERFACE_1,
          SDRAM_CS1,
          retention_state
              >> (rank_shift + ddr_size_partition_info.sdram1_cs0.num_partitions),
          active_state
              >> (rank_shift + ddr_size_partition_info.sdram1_cs0.num_partitions),
          ddr_size_partition_info.sdram1_cs1.num_partitions);
    }
  }

  /* Update ddr log with the new status */
  DDR_LOG(DDR_STATUS, DDR_DMM, retention_state, active_state);

}/* ddr_get_frequency */

/* =============================================================================
**  Function : ddr_params_is_training_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. LPDDR3 will always require training syncronization
*   to be done between rpm and sbl
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_required( void )
{
  return TRUE;
} /* ddr_params_is_training_required */

/* =============================================================================
**  Function : ddr_params_is_training_on_rpm_required
** =============================================================================
*/
/**
*   @brief
*   Indicate whether DDR parameter training is required or not in RPM. Training is
*   required if and only if DDR itself (e.g. PCDDR3) requires parameter training
*   and DDR parameter partition is invalid. 
*
*   @param  None
*
*   @retval  TRUE   Training required
*   @retval  FALSE  Training not required
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
boolean ddr_params_is_training_on_rpm_required( void )
{
  return TRUE;
} /* ddr_params_is_training_on_rpm_required */

/* =============================================================================
**  Function : ddr_do_phy_training
** =============================================================================
*/
/**
*   @brief
*   function called to do the DDR PHY training 
*
*   @param  None
*
*   @retval  NONE
*
*   @dependencies
*   None
*
*   @sideeffects
*   None
*
*   @sa
*   None
*/
void ddr_do_phy_training( void )
{  
} /* ddr_do_phy_training */


