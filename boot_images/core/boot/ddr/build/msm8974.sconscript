#===============================================================================
#
# DDR Libs
#
# GENERAL DESCRIPTION
#    build script
#
# Copyright 2009-2012 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header: //source/qcom/qct/core/boot/scons/main/latest/target/msm8660/src/boot/ddr/build/msm8660.sconscript#7 $
#  $DateTime: 2010/10/20 19:46:16 $
#  $Author: shal $
#  $Change: 1487005 $
#                      EDIT HISTORY FOR FILE
#
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
# 08/27/13   sr      Added ddr_params.c under v1 folder.
# 10/01/12   sl      Reorganized DDR PHY folder.
# 09/12/12   sl      Removed ddr_log.c.
# 08/17/12   sl      Removed LPDDR2/LPDDR3 specific files
# 08/10/12   sl      Removed unnecessary files
# 08/03/12   tw      Added LPDDR3 specific file
# 08/01/12   tw      Added ddr calculator parser script output (ddr_config)
# 07/19/12   tw      Added support for QDSS SWEVENT logging
# 03/07/12   tw      Added BIMC support
# 12/20/11   sl      Reorganized DDR folder.
# 08/01/11   kpa     Merge HAL scons code for new PW1.2 dir structure.
# 07/13/11   kpa     Update DDR_test.lib path
# 07/01/11   dh      Add SHARED_IMEM_DDR_PARAM_BASE define
# 06/29/11   tw      Added support for DDR DMM
# 12/09/10   kpa     Changes to make ddr compile for 8960
# 12/06/10   sl      Added ddr_forced_cdc.c.
# 10/15/10   sl      Added ddr_test.c.
# 06/22/10   sl      Moved ddr_sync.c to ddr/shared/src; added ddr_devices.c.
# 06/11/10   kedara  Updated to add DDR lib to ehostdl.
# 12/11/09   tw      Updated to support LPDDR2
# 11/12/09   tw      Changes for DDR HAL
#===============================================================================
Import('env')
env = env.Clone()

#-------------------------------------------------------------------------------
# Source PATH
#-------------------------------------------------------------------------------
SRCPATH = "${BUILD_ROOT}"

env.VariantDir('${BUILDPATH}', SRCPATH, duplicate=0)

env.PublishPrivateApi('DDR', [
  "${INC_ROOT}/core/boot/ddr/hw/msm8974/",
  "${INC_ROOT}/core/boot/ddr/hw/controller/",
  "${INC_ROOT}/core/boot/ddr/hw/phy/",
  "${INC_ROOT}/core/boot/ddr/hw/phy/v2/",
  "${INC_ROOT}/core/boot/ddr/common/params/v1/",
])

#-------------------------------------------------------------------------------
# Internal depends within CoreBSP
#-------------------------------------------------------------------------------
CBSP_API = [
  'BOOT',
  'SERVICES',
  'SYSTEMDRIVERS',
  'DAL',
  'KERNEL',
]

env.RequirePublicApi(CBSP_API)
env.RequireRestrictedApi(CBSP_API)

env.Append(CPPDEFINES = [
  "SHARED_IMEM_DDR_PARAM_BASE=0xFE805190",
  "SHARED_IMEM_DDR_TRAINING_COOKIE=0xFE805018"
])

env.Append(CPPDEFINES = [
  "FEATURE_LPDDR3"
])

#-------------------------------------------------------------------------------
# Sources, libraries
#-------------------------------------------------------------------------------
DDR_DRIVERS_SOURCES =[
  '${BUILDPATH}/core/boot/ddr/common/ddr_sync.c',
  '${BUILDPATH}/core/boot/ddr/common/params/v1/ddr_params.c',
  '${BUILDPATH}/core/boot/ddr/common/ddr_drivers.c',
  '${BUILDPATH}/core/boot/ddr/hw/msm8974/ddr_target.c',
  '${BUILDPATH}/core/boot/ddr/hw/msm8974/ddr_config.c',
]

DDR_RPM_MESSAGE_SOURCES = [
]

DDR_TEST_SOURCES = [
  '${BUILDPATH}/core/boot/ddr/common/ddr_test.c',
]

DDR_HAL_SOURCES = [
  '${BUILDPATH}/core/boot/ddr/hw/controller/BIMC_v1.c',
  '${BUILDPATH}/core/boot/ddr/hw/controller/BIMC_SHKE_v1.c',
  '${BUILDPATH}/core/boot/ddr/hw/controller/BIMC_DPE_v1.c',
  '${BUILDPATH}/core/boot/ddr/hw/controller/BIMC_SCMO_v1.c',
  '${BUILDPATH}/core/boot/ddr/hw/controller/SITE.c',
  '${BUILDPATH}/core/boot/ddr/hw/phy/v2/DDR_PHY.c',
  '${BUILDPATH}/core/boot/ddr/hw/phy/AHB2PHY.c',
]

if 'BUILD_BOOT_CHAIN' in env:
  DDR_HAL_SOURCES += []

ddr_drivers_lib = env.Library('${BUILDPATH}/DDR_DRIVERS', DDR_DRIVERS_SOURCES)
ddr_rpm_message_lib = env.Library('${BUILDPATH}/DDR_RPM_MESSAGE', DDR_RPM_MESSAGE_SOURCES)
ddr_test_lib = env.Library('${BUILDPATH}/DDR_TEST', DDR_TEST_SOURCES)
ddr_hal_lib = env.Library('${BUILDPATH}/DDR_HAL', DDR_HAL_SOURCES)

#-------------------------------------------------------------------------------
# Add Libraries to image
#-------------------------------------------------------------------------------
env.AddLibsToImage('DDR_BOOT_DRIVER', [ddr_drivers_lib, ddr_test_lib, ddr_hal_lib])
env.AddLibsToImage('RPM_IMAGE', [ddr_drivers_lib, ddr_hal_lib])
