/*===========================================================================

                         S H A R E D   M E M O R Y

DESCRIPTION
    Defines the public Sharem Memory api that are required at boot time

Copyright (c) 2011 - 2013 by Qualcomm Technologies, Incorporated.  All Rights Reserved.

===========================================================================*/


/*===========================================================================

                           EDIT HISTORY FOR FILE

$Header: //components/rel/boot.bf/3.0.c8/boot_images/core/mproc/smem/src/smem_boot.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/19/13   bt      Move SMEM info initialization to family-specific boot file.
11/05/13   bt      Support 64-bit architectures.
09/19/13   bt      Changed naming from 'up/downwards' to 'un/cached'.
06/06/13   bt      Add 1-to-1 versions of SMEM mapping functions.
05/14/13   bt      Add smem_save_smem_info_to_imem, for dynamic SMEM mappings.
04/12/13   bt      Add TOC and partition setup for secure SMEM.
04/12/13   bt      Update spinlock and OS prototypes.
10/12/12   rs      Changed SMEM version mask name
08/23/12   bt      Initialize Nway structure with SMD_NWAY_EDGE_UNUSED.
08/09/12   bt      Add smem_map_* functions, from smem_dal.c.
06/19/12   hwu     Stub out spinlock in boot. 
01/20/12   pa      Removing spinlocks from bootloader and stubbing out spinlock
                   APIs for common SMEM usage (smem.c).  
10/28/11   bm      KlockWork warning fixes.
06/27/11   bt      Add defines needed for splitting SMSM from SMEM.
06/15/11   bt      At boot, allocate the smd_loopback_command_type array.
06/13/11   rs      SMEM_XXX_PROC must be defined before including 
                   smem_version.h 
05/24/11   bm      Creation - SMEM API required at boot time.
===========================================================================*/


/*===========================================================================

                        INCLUDE FILES

===========================================================================*/
#include "smem_v.h"
#include "smem_internal.h"
#include "smem_target.h"
#include "smem_version.h"
#include "smsm_type.h"
#include "smem_ext.h"
#include "spinlock.h"
#include "smem_boot.h"
#include "smem_toc.h"
#include "smem_partition.h"
#include "smem_targ_info.h"

/*===========================================================================

            GLOBAL DEFINITIONS AND DECLARATIONS FOR MODULE

============================================================================*/
extern smem_init_info_type smem_init_info;

/** Address containing the SMEM memory info, e.g physical address and size, 
 *  and possibly other information. */
static smem_targ_info_type *smem_targ_info_ptr = 
    (smem_targ_info_type *)SMEM_INVALID_ADDR;

/*===========================================================================

                      PRIVATE FUNCTION DECLARATIONS
                      
===========================================================================*/
/*===========================================================================
  FUNCTION  smem_heap_info_init
===========================================================================*/
/**
  Initializes the heap info structure.

  @return
  None.
*/
/*=========================================================================*/
static void smem_heap_info_init( void )
{
  uint32 offset;

  smem_init_info.smem_heap_info = (smem_heap_info_type *)
    ( smem_init_info.smem_data_base_addr + smem_get_offset( SMEM_HEAP_INFO ) );

  /*------------------------------------------------------------
   * The heap starts after the last fixed buffer, 
   * so check to see if the last fixed buffer has been allocated,
   *------------------------------------------------------------*/
  if( smem_init_info.smem_allocation_table[SMEM_LAST_FIXED_BUFFER].allocated !=
      SMEM_ALLOCATION_ENTRY_ALLOCATED )
  {
    ERR_FATAL( "Can't initialize heap info", 0, 0, 0 );
  }

  if( smem_init_info.smem_heap_info->initialized != SMEM_HEAP_INFO_INIT )
  {
    offset = 
       ( smem_init_info.smem_allocation_table[SMEM_LAST_FIXED_BUFFER].offset + 
         smem_init_info.smem_allocation_table[SMEM_LAST_FIXED_BUFFER].size);

    /* TODO - do we need any special alignment here? */

    smem_init_info.smem_heap_info->free_offset    = offset;
    smem_init_info.smem_heap_info->heap_remaining = 
                                        smem_ext_get_smem_size() - offset;
    smem_init_info.smem_heap_info->initialized    = SMEM_HEAP_INFO_INIT;
  }

} /* smem_heap_info_init */

/*===========================================================================
  FUNCTION  smem_setup_toc
===========================================================================*/
/**
  Sets up the SMEM Table of Contents describing each protected partition, and
  fills out the headers at the beginning of each partition.
 
  @return
  None.
*/
/*=========================================================================*/
static void smem_setup_toc( void )
{
  const smem_toc_config_type *smem_toc_config_tbl, *partn_config_entry;
  smem_toc_type              *toc_header;
  smem_toc_entry_type        *toc_entry;
  uint32 toc_version, num_partitions, i, j, total_excl_size;
  uint32 total_partn_size = SMEM_TOC_SIZE;
  DALSYSPropertyVar prop;
  /* The handle is a uint32[2], so it won't be null after declaration. */
  DALSYS_PROPERTY_HANDLE_DECLARE(smem_prop_handle);
  
  if (DALSYS_GetDALPropertyHandleStr("/core/mproc/smem", smem_prop_handle) !=
        DAL_SUCCESS)
  {
    ERR_FATAL("smem_setup_toc: Cannot get DALProp handle.", 
              0, 0, 0);
  }
  else if (DALSYS_GetPropertyValue(smem_prop_handle, "smem_toc_vers", 0, 
                                   &prop) != DAL_SUCCESS)
  {
    ERR_FATAL("smem_setup_toc: Cannot get smem_toc_vers DAL prop.", 0, 0, 0);
  }
  /* The SMEM TOC version will be put in prop.Val.dwVal. */
  toc_version = prop.Val.dwVal;
  
  if (DALSYS_GetPropertyValue(smem_prop_handle, "smem_partitions", 0, 
                              &prop) != DAL_SUCCESS)
  {
    ERR_FATAL("smem_setup_toc: Cannot get smem_partitions DAL prop.", 0, 0, 0);
  }
  /* The SMEM partition info array will be put in prop.Val.pStruct 
   * (array of smem_toc_config_type's) */
  smem_toc_config_tbl = (smem_toc_config_type *)prop.Val.pStruct;
  
  /* Include the entry for the TOC itself in the num_partitions count. */
  num_partitions = 0;
  i = 0;
  do
  {
    /* Calculate number of partitions and total size for sanity. */
    partn_config_entry = &(smem_toc_config_tbl[i]);
    
    num_partitions++;
    total_partn_size += partn_config_entry->size;
    
    i++;
    /* The last entry has invalid hosts and size 0 to signify the end. */
  } while (partn_config_entry->host0 != SMEM_INVALID_HOST && 
           partn_config_entry->host1 != SMEM_INVALID_HOST);
  
  if (smem_init_info.smem_heap_info->heap_remaining < total_partn_size)
  {
    ERR_FATAL("smem_setup_toc: Heap remaining (%d) < total partitions sz (%d)!", 
              smem_init_info.smem_heap_info->heap_remaining, 
              total_partn_size, 0);
  }
  
  /* The TOC table in SMEM must fit in the last 4kB page. */
  if (sizeof(smem_toc_type) + 
      (sizeof(smem_toc_entry_type)*num_partitions) > SMEM_TOC_SIZE)
  {
    ERR_FATAL("smem_setup_toc: TOC too big! Size: %d Bytes, "
              "must be %d or less.",
              sizeof(smem_toc_type) + 
              (sizeof(smem_toc_entry_type)*num_partitions), SMEM_TOC_SIZE, 0);
  }
  
  /* First reduce the generic SMEM heap, then fill in the TOC in the area at
   * the end that we've just reserved. */
  smem_init_info.smem_heap_info->heap_remaining -= SMEM_TOC_SIZE;
  
  toc_header = (smem_toc_type *)
      (smem_init_info.smem_data_base_addr + 
       smem_init_info.smem_heap_info->free_offset + 
       smem_init_info.smem_heap_info->heap_remaining);
  toc_header->identifier = SMEM_TOC_IDENTIFIER;
  toc_header->version = toc_version;
  toc_header->num_entries = num_partitions;
  
  /* Add the TOC itself as the first partition entry in the SMEM TOC. */
  toc_entry = &(toc_header->entry[0]);
  toc_entry->offset = (uint32)(smem_init_info.smem_heap_info->free_offset + 
       smem_init_info.smem_heap_info->heap_remaining);
  toc_entry->size  = SMEM_TOC_SIZE;
  toc_entry->flags = SMEM_TOC_ENTRY_FLAGS_ENABLE_PROTECTION;
  toc_entry->host0 = SMEM_INVALID_HOST;
  toc_entry->host1 = SMEM_INVALID_HOST;
  toc_entry->size_cacheline = 0;
  for (j = 0; j < SMEM_TOC_MAX_EXCLUSIONS; j++)
  {
    toc_entry->exclusion_sizes[j] = 0;
  }
  
  /* Fill in each partition entry in the SMEM TOC, from the smem_toc_config_tbl 
   * obtained from DevConfig. */
  for (i = 1; i < num_partitions; i++)
  {
    partn_config_entry = &(smem_toc_config_tbl[i-1]);
    toc_entry = &(toc_header->entry[i]);
    
    /* First reduce the generic SMEM heap by the size of this partition, 
     * then fill out the TOC entry and header for this partition. */
    smem_init_info.smem_heap_info->heap_remaining -= partn_config_entry->size;
    
    toc_entry->offset = (smem_init_info.smem_heap_info->free_offset + 
        smem_init_info.smem_heap_info->heap_remaining);
    toc_entry->size  = partn_config_entry->size;
    toc_entry->flags = partn_config_entry->flags;
    toc_entry->host0 = partn_config_entry->host0;
    toc_entry->host1 = partn_config_entry->host1;
    toc_entry->size_cacheline = partn_config_entry->size_cacheline;
    total_excl_size = 0;
    for (j = 0; j < SMEM_TOC_MAX_EXCLUSIONS; j++)
    {
      toc_entry->exclusion_sizes[j] = partn_config_entry->exclusion_sizes[j];
      total_excl_size += partn_config_entry->exclusion_sizes[j];
    }
    
    if (toc_entry->size != 0)
    {
      smem_partition_header_type *partn_header;
      
      /* Set up the header struct at the beginning of this partition. */
      partn_header = (smem_partition_header_type *)
          (smem_init_info.smem_data_base_addr + toc_entry->offset);
      partn_header->identifier = SMEM_PARTITION_HEADER_ID;
      partn_header->host0 = partn_config_entry->host0;
      partn_header->host1 = partn_config_entry->host1;
      /* Size does not count the exclusion ranges. */
      partn_header->size = partn_config_entry->size - total_excl_size;
      /* Offsets in this partition are from 0 to the allocatable size. */
      /* Upward free offset starts after the end of this partition header. */
      partn_header->offset_free_uncached = sizeof(smem_partition_header_type);
      /* Downward free offset is the last free byte, also excluding the exclusion
       * regions. */
      partn_header->offset_free_cached = partn_header->size;
    }
  }
  
} /* smem_setup_toc */

/*===========================================================================

                      PUBLIC FUNCTION DECLARATIONS
                      
===========================================================================*/

/*===========================================================================
FUNCTION      smem_boot_init
===========================================================================*/
/**
@brief        Called by BOOT to initialize smem
                  
@dependencies None
      
@return       None
*/
/*=========================================================================*/
void smem_boot_init( void )
{
  uint32 offset;
  uint32 idx;
  uint32 tbl_idx;
  uint32 lc;
  smem_smsm_size_info_type *smsm_size_info;
  smem_smd_loopback_command_type *smd_lb_nway_cmd_reg;

  smem_set_base_addr();

  /* zero out the entire smem region */
  SMEM_MEMSET_SMEM( smem_init_info.smem_data_base_addr, 0,
                    smem_ext_get_smem_size() );
  
  /*------------------------------------------------------------
   * Setup Allocation table for "fixed" structures. Since everyone
   * is compatible. We can just write the values in shared memory
   * without spin locking. The values will be written once for each
   * call to smem_init, but everyone is writing the same values.
   *------------------------------------------------------------*/
  smem_init_info.smem_allocation_table = (smem_allocation_table_type *)
    ( smem_init_info.smem_data_base_addr +
      smem_get_offset(SMEM_ALLOCATION_TABLE) );

  offset = 0;

  for(idx = 0; smem_init_info.smem_buf_data_tbl[idx].tag != SMEM_INVALID; idx++)
  {
    tbl_idx = (uint32) smem_init_info.smem_buf_data_tbl[idx].tag;
    smem_init_info.smem_allocation_table[tbl_idx].offset = offset;
    smem_init_info.smem_allocation_table[tbl_idx].size = 
                                    smem_init_info.smem_buf_data_tbl[idx].size;
    smem_init_info.smem_allocation_table[tbl_idx].allocated = 
                                    SMEM_ALLOCATION_ENTRY_ALLOCATED;

    offset += smem_init_info.smem_buf_data_tbl[idx].size;
  }

  smem_init_info.smem_memory_barrier = (uint32 *)
    ( smem_init_info.smem_data_base_addr + 
      smem_get_offset( SMEM_MEMORY_BARRIER_LOCATION ) );

  SMEM_MEMORY_BARRIER();

  /*------------------------------------------------------------
   * Setup heap info.
   *------------------------------------------------------------*/
  smem_heap_info_init();

  /* Set up SMEM Partition Table of Contents and Partition headers. */
  smem_setup_toc();
  
  /* Save the SMEM target information after the TOC has been filled out.*/
  smem_save_targ_smem_info();

  /* Moved before the version check/set in order to fulfill that function's
   * dependency that smem has been initialized (and indicate so). */
  smem_init_info.smem_state = SMEM_BOOT_INITIALIZED;

  /*------------------------------------------------------------
   * Set SMEM version info and verify that everyone registered so
   * far is compatible.
   * Each call to smem_init verifies that all previous calls are
   * compatible. So, the last call to smem_init will be certain to
   * detect if anyone is incompatible.
   *------------------------------------------------------------*/

  /* This may be thread-safe now that only one protocol version exists here. */
  if (smem_version_set(SMEM_VERSION_INFO, SMEM_VERSION_ID, 
                       SMEM_MAJOR_VERSION_MASK) == FALSE)
  {
    ERR_FATAL( "smem_boot_init: major version (%d) does not match all procs!",
        SMEM_VERSION_ID, 0, 0);
  }
  
  /* Initialize SMSM info to be read by all processors in smsm_init(), to set
   * their local smsm_info_type fields. */
  smsm_size_info = smem_alloc(SMEM_SMSM_SIZE_INFO, 
      sizeof(smem_smsm_size_info_type));
  if(smsm_size_info == NULL)
  {
    ERR_FATAL( "smem_boot_init: Could not allocate smsm_size_info",
        0, 0, 0);
  }    
  smsm_size_info->num_hosts   = SMSM_NUM_HOSTS;
  smsm_size_info->num_entries = SMSM_NUM_ENTRIES;
  
  /* Allocate the SMD Loopback Registers with a size (not necessarily
   * dependent) to the size of the smd_channel_type enumeration.  
   * This is the only place this structure will be allocated, and in order to
   * access it clients must use smem_get_addr() instead. */
  smd_lb_nway_cmd_reg = smem_alloc(SMEM_SMD_LOOPBACK_REGISTER, 
             sizeof(smem_smd_loopback_command_type) * SMEM_SMD_LOOPBACK_NUM_CHANNELS);
  if(smd_lb_nway_cmd_reg == NULL)
  {
    ERR_FATAL( "smem_boot_init: Could not allocate smd_lb_nway_cmd_reg",
        0, 0, 0);
  } 
  for (lc = 0; lc < SMEM_SMD_LOOPBACK_NUM_CHANNELS; lc++)
  {
    smd_lb_nway_cmd_reg[lc].edge = SMEM_SMD_NWAY_EDGE_UNUSED;
  }
  
  SMEM_MEMORY_BARRIER();
} /* smem_boot_init */

/*===========================================================================
FUNCTION      smem_boot_debug_init
===========================================================================*/
/**
@brief        Initialize shared memory for crash debugging.
 
              This function may be called at boot to recover enough state from
              shared memory to allow debug to recover state information from
              shared memory that may be used for offline debugging. This
              function will:

              (1) Zero the spinlocks
              (2) Configure the local data structures to allow access to
                  shared memory.

              Debug may call smem_get_addr() to determine the address and size
              of shared memory items. Debug should not write to the shared
              memory items.

              After calling this function, and recovering the relevant
              debugging information, boot must call smem_boot_init() to
              reinitialize shared memory before continuing.

              This function is supported only in the first system bootloader.

@dependencies None

@return       None

@sideeffect   None
*/
/*===========================================================================*/
void smem_boot_debug_init( void )
{
  smem_set_base_addr();

  smem_init_info.smem_allocation_table = (smem_allocation_table_type *)
    ( smem_init_info.smem_data_base_addr + 
      smem_get_offset(SMEM_ALLOCATION_TABLE) );

  /* Indicating smem is not zero-initialized */
  smem_init_info.smem_state = SMEM_PRE_BOOT_INIT;
} /* smem_boot_debug_init */

/*===========================================================================
FUNCTION      smem_spin_lock_init
===========================================================================*/
/**
  Initializes spinlocks.
 
  Spinlocks are used to acquire exclusive access to resources shared by
  all processors in the system.
 
  @param[in] spin_lock_array    Pointer to spinlocks array in SMEM.
  @param[in] int_lock_array     Pointer to intlocks array in SMEM (unused).
  @param[in] num_int_locks      Number of spinlocks.

  @return
  NA.
 
  @dependencies
  None.

  @sideeffects
  Initializes OS specific mutual exclusion mechanisms.
*/
/*=========================================================================*/
void smem_spin_lock_init( 
       volatile uint32 *spin_lock_array,
       uint32 *int_lock_array,
       uint32 num_int_locks )
{
  /* Spinlocks are not implemented in boot, under the assumption that no other
   * processors are running concurrently. */
}

/*===========================================================================
FUNCTION      smem_spin_lock
===========================================================================*/
/**
  Acquires a spinlock as indicated by input integer, protected by non-
  preemtable critical section (effectively locking interrupts).
 
  @param[in] lock    Spinlock identifier.

  @return
  NA.
 
  @dependencies
  None.

  @sideeffects
  Prevents context switch (via critical section) and causes remote
  processors to spin if they try to use the same resource (via spinlock).
*/
/*=========================================================================*/
void smem_spin_lock( uint32 lock )
{
  /* Spinlocks are not implemented in boot, under the assumption that no other
   * processors are running concurrently. */
}

/*===========================================================================
FUNCTION      smem_spin_unlock
===========================================================================*/
/**
  Releases a spin lock as indicated by input integer, protected by non-
  preemtable critical section (effectively unlocking interrupts).
 
  @param[in] lock    Spinlock identifier.

  @return
  NA.
 
  @dependencies
  None.

  @sideeffects
  Allows other threads (via critical section) and remote processors (via
  spinlock) to acquire the shared resource.
*/
/*=========================================================================*/
void smem_spin_unlock( uint32 lock )
{
  /* Spinlocks are not implemented in boot, under the assumption that no other
   * processors are running concurrently. */
}

/*===========================================================================
  FUNCTION  smem_clear_spin_locks
===========================================================================*/
/**
  This function unlocks all spinlocks held by the specified host.
 
  Mutex characteristics:
   - Each Mutex has a separate read/write Mutex-register
   - Each Mutex-register can hold an integer value
   - '0' value = Mutex is free/unlocked
   - Non-zero value = Mutex is locked
 
  A more detailed description can be found in System FPB (80-VR139-1A Rev. A)
  documentation.

  @param[in] host      Host identifier.

  @return
  None

  @dependencies 
  None.

  @sideeffects
  None.
*/
/*==========================================================================*/
void smem_spin_locks_clear( smem_host_type host )
{
  /* Spinlocks are not implemented in boot, under the assumption that no other
   * processors are running concurrently. */
}

/*===========================================================================
  FUNCTION  smem_map_spinlock_region_va
===========================================================================*/
/**
  Creates a virtual address of the HW Mutexes region, used for SMEM spinlocks,
  to enable use of HWIO macros.

  @return
  The spinlocks region base virtual address if successfully mapped, or NULL
  otherwise.

  @dependencies
  None.

  @sideeffects
  None.
*/
/*=========================================================================*/
void *smem_map_spinlock_region_va( void )
{
  /* Spinlocks are not implemented in boot, under the assumption that no other
   * processors are running concurrently.  So this spinlock region does not 
   * have to be mapped. */
  return NULL;
}

/*===========================================================================
  FUNCTION  smem_set_spin_lock_api
===========================================================================*/
/**
  Lookup chip family and version to determine whether to use one of two 
  spinlock mechanisms:
  For 9x25/8974 below version 2: 
    use exclusive memory access spinlock API
  Otherwise:
    use HW mutex spinlock API.
 
  @return
  None.
*/
/*=========================================================================*/
void smem_set_spin_lock_api( void )
{
  /* Spinlocks are not implemented in boot, under the assumption that no other
   * processors are running concurrently. */
}

/*===========================================================================
  FUNCTION  smem_map_memory_va
===========================================================================*/
/**
  Map the memory region and return the virtual address of the mapped (physical)
  memory region.

  @return
  Virtual address of the mapped memory region.

  @param[in]    mem_region   Memory region base (physical) address.
  @param[in]    size         Size of the memory region.

  @dependencies
  None.

  @sideeffects
  None
*/
/*=========================================================================*/
void *smem_map_memory_va( void* mem_region, uint32 size )
{
  /** On Rex OS, there is really no way to translate a physical address into
   * a virtual address, so for now we assume either the mapping is 1-to-1, or 
   * we are actually being passed a virtual address already.  The exception is 
   * for the RPM Message RAM, where definitions of both should be statically 
   * known. */
  if(mem_region == (void*)SMEM_RPM_MSG_RAM_BASE_PHYS )
  {
    return (void*) SMEM_RPM_MSG_RAM_BASE ;
  }
  else
  {
    return mem_region;
  }
}

/*===========================================================================
  FUNCTION  smem_map_smem_targ_info
===========================================================================*/
/**
  Map the SMEM target info region (if it has not been mapped by this function 
  already) and return the virtual base address of the structure.

  If this is not the first time this function has been called, it will simply 
  return the saved pointer, rather than attempting to do the mapping again.

  @return
  Pointer to the structure that holds the SMEM target info like physical 
  address and size.
*/
/*=========================================================================*/
smem_targ_info_type *smem_map_smem_targ_info( void )
{
  if (smem_targ_info_ptr == SMEM_INVALID_ADDR)
  {
    /* Assume mapping is 1-to-1, and that no mapping is required for HWIO, 
     * so it just uses an address define. */
    smem_targ_info_ptr = (smem_targ_info_type *)SMEM_TARG_INFO_ADDR;
  }

  return smem_targ_info_ptr;
}

