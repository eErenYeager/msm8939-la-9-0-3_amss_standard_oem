/*===========================================================================

                      S H A R E D   M E M O R Y   T A R G E T

DESCRIPTION
    Defines the Family-specific helper functions for the SMEM API in 
    Bootloaders.

Copyright (c) 2013 by Qualcomm Technologies, Incorporated.  All 
Rights Reserved.

===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/27/13   bt      Fix 64-bit structure alignment calculation.
11/14/13   bt      Initial revision for family-specific SMEM info 
                   initialization in SBL on Bear.
===========================================================================*/
/*===========================================================================
                        INCLUDE FILES
===========================================================================*/
#include <stdint.h>
#include "smem_target.h"
#include "smem_targ_info.h"
#include "smem_internal.h"
#include "smem_toc.h"

/*===========================================================================
                  PUBLIC FUNCTION DECLARATIONS
===========================================================================*/
/*===========================================================================
  FUNCTION  smem_save_targ_smem_info
===========================================================================*/
/**
  Saves the SMEM base physical address and size in a structure at a set 
  address.  This must be called from the beginning of smem_boot_init().

  The size and address can then be read by each image at the beginning of 
  smem_init.  This allows each other image to have a common binary for all 
  targets, irrespective of the memory map.

  @return
  None.
*/
/*=========================================================================*/
void smem_save_targ_smem_info( void )
{
  /* Bootloaders memory map is 1-to-1.  SMEM target info should be populated
   * in the last 4kB page, directly following the TOC entries. */
  /* Need to look into the TOC to find its num_entries and determine the
   * total size. */
  smem_toc_type *toc = (smem_toc_type *)
      (smem_get_base_addr() + smem_get_size() - SMEM_TOC_SIZE);
  uint32 toc_size = sizeof(smem_toc_type) + 
      (sizeof(smem_toc_entry_type) * toc->num_entries);
  /* This should always be true, but ensure that the address for the smem
   * target info is 64-bit aligned, so any architecture will read it the same
   * without needing any assumptions about alignment/packing. */
  uint32 toc_padded_size = (toc_size + 0x7) & (~(0x7));
  uintptr_t smem_targ_info_addr = ((uintptr_t)toc + toc_padded_size);
  smem_targ_info_type *smem_targ_info_ptr = 
      (smem_targ_info_type *)smem_targ_info_addr;

  /* The TOC table + SMEM target info must fit in the last 4kB page. */
  if (toc_padded_size + sizeof(smem_targ_info_type) > SMEM_TOC_SIZE)
  {
    ERR_FATAL("SMEM TOC + targ_info too big! TOC: %d B; targ_info: %d B, "
              "max: %d B.",
              toc_size, sizeof(smem_targ_info_type), SMEM_TOC_SIZE);
  }

  /* Fill out the SMEM target info structure. */
  smem_targ_info_ptr->identifier          = SMEM_TARG_INFO_IDENTIFIER;
  smem_targ_info_ptr->smem_size           = SCL_SHARED_RAM_SIZE;
  smem_targ_info_ptr->smem_base_phys_addr = SCL_SHARED_RAM_BASE;

  /* Save the address of the SMEM target info into HWIO registers
   * HWIO_TCSR_TZ_WONCE_n_ADDR(SMEM_TARG_INFO_REG_BASE_IDX++1). This will 
   * affect the value of the define SMEM_TARG_INFO_ADDR for all images. 
   * Bootloaders does not map the HWIO, but merely uses a define. */
  HWIO_OUTI( TCSR_TZ_WONCE_n, SMEM_TARG_INFO_REG_BASE_IDX, 
      smem_targ_info_addr & 0xFFFFFFFF );
  HWIO_OUTI( TCSR_TZ_WONCE_n, SMEM_TARG_INFO_REG_BASE_IDX+1, 
      ( (uint64)smem_targ_info_addr >> 32 ) );
}
