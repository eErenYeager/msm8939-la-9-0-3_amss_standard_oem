/*===========================================================================

                 S H A R E D   M E M O R Y   M A P P I N G

DESCRIPTION
    Defines the OS specific helper functions for SMEM internal functions 
    regarding the OS mapping of the SMEM region(s) and its pages.

Copyright (c) 2013 by Qualcomm Technologies, Incorporated.
All Rights Reserved.

===========================================================================*/

/*===========================================================================

                           EDIT HISTORY FOR FILE

when       who     what, where, why
--------   ---     ----------------------------------------------------------
11/13/13   bt      Initial revision to (not) map SMEM pages with Rex.
===========================================================================*/
/*===========================================================================
                  INCLUDE FILES
===========================================================================*/
#include "smem_os.h"
#include "smem_os_common.h"
#include "smem_target.h"
#include "smem_v.h"

/*===========================================================================
                  LOCAL DATA DECLARATIONS
===========================================================================*/

/*===========================================================================
                  EXTERNAL DATA DECLARATIONS
===========================================================================*/

/*===========================================================================
                  PRIVATE FUNCTION DECLARATIONS
===========================================================================*/

/*===========================================================================
                  PUBLIC FUNCTION DECLARATIONS
===========================================================================*/
/*===========================================================================
  FUNCTION  smem_map_base_va
===========================================================================*/
/**
  Map the memory region and return the virtual address of the mapped (physical)
  base address of SMEM.

  @param[in]    phys_addr     Memory region base (physical) address.
  @param[in]    size          Size of the SMEM region.

  @return
  Virtual address of the mapped SMEM region.
*/
/*=========================================================================*/
char *smem_map_base_va( char *phys_addr, uint32 size )
{
  return (char *)smem_map_memory_va(phys_addr, size);
}

/*===========================================================================
  FUNCTION  smem_map_page_attr
===========================================================================*/
/**
  Map the page of SMEM (or a whole address range) at offset page_offset and 
  size of at least page_size.  The cached argument allows the SMEM module to 
  dynamically decide upon each address range's cacheability.
  
  The mapping will be contiguous, from SMEM's base physical address + 
  page_offset to SMEM's base virtual address + page_offset, with "RW"
  permisions.  Use the returned actual page-aligned size to determine where
  the mapping stops and when a new page must be mapped.

  @param[in]    page_offset   The offset from the base address to map (will be 
                              rounded down to a 4kB alignment).
  @param[in]    page_size     Size of this SMEM page or address range (will be
                              rounded up to a 4kB multiple).
  @param[in]    cached        Should this address range be cached memory?

  @return
  The actual page size mapped, rounded up to a 4kB multiple.
*/
/*=========================================================================*/
uint32 smem_map_page_attr
(
  uint32                page_offset, 
  uint32                page_size, 
  boolean               cached
)
{
  uint32 aligned_offset;

  /* Must still align the page_size in case the remote side is using dynamic 
   * mapping of SMEM pages, which would limit the allocations in both 
   * directions to page boundaries. */

  /* Round offset down to be 4kB aligned.  This really only is for downward
   * allocations; caller must make sure that the page has not been mapped 
   * already. */
  aligned_offset = page_offset & (~SMEM_PAGE_ALIGN_MASK);
  /* Adjust the size accounting for the offset adjustment. */
  page_size += (page_offset - aligned_offset);
  /* Round page size up to a 4kB multiple.  Caller should confirm this 
   * returned value in order to map the next page starting at the correct 
   * aligned offset (whether it is upwards or downwards). */
  page_size = (page_size + SMEM_PAGE_ALIGN_MASK) & (~SMEM_PAGE_ALIGN_MASK);

  /* Don't actually perform any mapping, since all of SMEM is mapped already. */

  return page_size;
}
