#===============================================================================
#
# BUILDSPEC file builder
#
# GENERAL DESCRIPTION
#    build rules script
#
# Copyright (c) 2009-2009 by Qualcomm Technologies, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
#  $Header:
#  $DateTime: 2015/03/19 01:58:37 $
#  $Author: pwbldsvc $
#  $Change: 7697609 $
#                      EDIT HISTORY FOR FILE
#                      
#  This section contains comments describing changes made to the module.
#  Notice that changes are listed in reverse chronological order.
#  
# when       who     what, where, why
# --------   ---     ---------------------------------------------------------
#
#===============================================================================
import os
import subprocess
import string
import re
import sys
import string
import atexit
import SCons.Action
import utils
from SCons.Script import *

sadir = ""
cmddict = {}

#------------------------------------------------------------------------------
# Hooks for Scons
#------------------------------------------------------------------------------
def exists(env):
   return env.Detect('buildspec_builder')

def generate(env):
   #-------------------------------------------------------------------------------
   # def builder for trace file capture
   #

   # builder output overrides from defaults
   buildspec = str(env.GetOption('buildspec'))
   BUILDSPEC = os.environ.get('BUILDSPEC')
   target_root = str(env['TARGET_ROOT'])
   topofbuild  = target_root
   
   # Precedence:
   #    1) buildspec command-line argument
   #    2) BUILDSPEC environment variable
   if buildspec != 'none':
      topofbuild = buildspec
   elif BUILDSPEC:
      pass
   else:
      # buildspec has not been enabled, so get out of here
      return

   # If the topofbuild dir doesn't exist, print a warning and get out
   if not os.path.exists(topofbuild):
      env.PrintWarning("buildspec enabled, but directory doesn't exist: " + str(topofbuild))
      return
   
   # Make sure we're not cleaning
   clean   = env.GetOption('clean')
   no_exec = env.GetOption('no_exec')   
   if not clean and not no_exec:
      # Make sure we're building something
      if len(SCons.Script.COMMAND_LINE_TARGETS) > 0:
         # Make sure that we're an internal only build
         if env.IsBuildInternal():
            sadir = os.path.normpath(topofbuild+"/"+"static_analysis")

            if os.name is 'nt':
               satool = r'\\stone\aswcrm\smart\nt\bin\SAGenConfig.exe'
               sacmd  = satool
            elif os.name is 'posix':
               satool = r'/prj/qct/asw/qctss/linux/bin/SAGenConfig.pl'
               sacmd  = 'perl ' + satool

            if os.path.exists(satool):
               env.PrintInfo("buildspec enabled--tracefile will be generated in: " + sadir)
               env['PRINT_CMD_LINE_FUNC'] = buildspec_print_cmd_line
               atexit.register(buildspec_complete, env, sadir, sacmd)
            else:
               env.PrintWarning("sagenconfig tool not found: " + satool)

#===============================================================================
# buildspec_print_cmd_line
#        - Overwrite default scons command line printer to also log all output
#          to log file in verbose mode
#        - Log commands executed to build spec for static analysis tools
#        - enabled from command line with --buildspec option
#===============================================================================
def buildspec_print_cmd_line(s, target, src, env):
   global cmddict
   
   verbose = int(env.GetOption('verbose'))
   buildspec = str(env.GetOption('buildspec'))

   targetstr = string.join([str(x) for x in target]);
   srcstr = string.join([str(x) for x in src]);

   #----------------------------------------------------------------------
   # split the command str on our string identifier (&&&)
   # element one is the non-verbose output for stdout
   # element two is the entire command line with no formating
   #----------------------------------------------------------------------
   strlist = s.split("&&&");
   sys.stdout.write(" %s...\n"%strlist[0]);

   #----------------------------------------------------------------------
   # if verbose flag is on (not zero)
   # print out the entire command line to stdout 
   # currently there is no variation between verbose 1, 2, or 3
   #----------------------------------------------------------------------
   if verbose and len(strlist) > 1:
      sys.stdout.write(" %s...\n"%strlist[1]);

   #----------------------------------------------------------------------
   # not all commands have a STR list identified
   # only log those we have setup in the generate function above (i.e.: CXXCOMSTR)
   # Performance Note: We open and close this file alot it would be better
   # to save off the file handle and write to it instead of open/close
   #----------------------------------------------------------------------
   #import pdb; pdb.set_trace()
   if env.has_key('IMAGE_NAME'):
      imagename = str(env['IMAGE_NAME'])
   else:
      imagename = 'none'
   if not cmddict.has_key(imagename):
      cmddict[imagename] = []
   if len(strlist) > 1:
      # Save information to our global data structure
      cmddict[imagename].append(strlist[1])


#===============================================================================
# buildspec_complete:
#        - exit function handler to finish buildspec log, and create kw log
#===============================================================================
def buildspec_complete(env, sadir, sacmd):
   # Complicated mess to determine the path in which to create the trace files
   if env.has_key('IMAGE_NAME'):
      imagename = str(env['IMAGE_NAME'])
   else:
      imagename = 'none'

   # Create SA dir 
   buildpath=str(env['BUILDPATH'])
   sa_buildpath_dir = sadir+"/"+buildpath
   if not os.path.exists(sa_buildpath_dir):
      os.makedirs(sa_buildpath_dir)

   # Filenames and paths
   tracefilename = str(imagename+".bspec.trace")
   tracepath = sa_buildpath_dir+"/"+tracefilename

   # Write the tracefile
   counter = 0
   f = open(os.path.normpath(tracepath), "w")
   # Iterate through the list of commands
   if cmddict.has_key(imagename):
      for cmd in cmddict[imagename]:
         pieces = cmd.strip(' ').split()
         cmdname = os.path.normpath(pieces[0])
         line_to_write = ";".join(["exec", str(counter), os.getcwd(), cmdname, cmdname] + pieces[1:])
         f.write(line_to_write + "\n")
         counter += 1
      f.close()

   # Setup the SA command to run
   root = os.path.normpath(str(env['TARGET_ROOT']))
   reltracepath = tracepath[tracepath.find('static_analysis'):]
   sacmd += ' -r ' + root + " -t trace -f " + reltracepath
   # Run the SA command
   try:
      utils.exec_cmds(env, sacmd)
   except NameError:
      print "call to sagenconfig failed"

