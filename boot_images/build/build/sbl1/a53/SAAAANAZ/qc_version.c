/*
#============================================================================
#  Name:                                                                     
#    qc_version.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2017 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Fri Jul 28 03:58:35 2017 
#============================================================================
*/
const char QC_IMAGE_VERSION_STRING_AUTO_UPDATED[]="QC_IMAGE_VERSION_STRING=BOOT.BF.3.0.C8-00023";
