Firehose is a new serial protocol that can be used with "Emergency DownLoad" (EDL). This protocol is fast and there is *no need* for a "single image" file to get into mass storage mode.

There are no pre-checked-in deviceprogrammer binaries, what is compiled and present in the build is what is ready for use.
The deviceprogrammer image is compiled into the build\ms\bin\<platform> directory (e.g. build\ms\bin\8974\), which is the directory where other build compiled images such as sbl1.mbn also reside.
 

To create the "programmer", go to the build\ms directory and type
 
build deviceprogrammer BUILD_ID=<build_flavor>
 
NOTE: For best results, run a CLEAN before each build attempt
 
Examples
---------------------------------------------------------------------------------------------------
build deviceprogrammer BUILD_ID=AAAAANAA
  -- creates prog_emmc_firehose_8974.mbn, used for programming device
 
b8974 deviceprogrammer BUILD_ID=AAAAANAA DEVICEPROGRAMMER_IMAGE_TYPE=read
  -- creates READ_emmc_firehose_8974.mbn, used for reading data back from device
 
b8974 deviceprogrammer BUILD_ID=AAAAANAA DEVICEPROGRAMMER_IMAGE_TYPE=gpp
-- creates GPP_emmc_firehose_8974.mbn, used for creating General Purpose Partitions
