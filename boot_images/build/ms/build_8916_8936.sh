#===============================================================================
#
# CBSP Buils system
#
# General Description
#    build shell script file.
#
# Copyright (c) 2009,2013 - 2014 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
#
#-------------------------------------------------------------------------------
#
# $Header: //components/rel/boot.bf/3.0.c8/boot_images/build/ms/build_8916_8936.sh#1 $
# $DateTime: 2015/03/19 01:58:37 $
# $Author: pwbldsvc $
# $Change: 7697609 $
#                      EDIT HISTORY FOR FILE
#
# This section contains comments describing changes made to the module.
# Notice that changes are listed in reverse chronological order.
#
# when       who     what, where, why
# --------   ---     -----------------------------------------------------------
# 03/07/14   jz      Added for 9x45 build command
# 02/13/14   sve     Added 8936 build command
# 10/08/13   ck      Fixed 8916 default build id to match .builds file
# 10/07/13   ck      Removed targets that are not in Bear family
# 09/18/13   svelic  Added support for jsdcc.elf compilation for 8962 target
# 09/09/13   jz      Added emmcbld jtagprogrammer deviceprogrammer for 8084
# 09/03/13   svelic  Added support for jsdcc.elf compilation for 8084 target
# 09/12/13   ck      Add support for MSM8916
# 07/23/13   plc     Add support for MSM8994
# 07/11/13    wg     Updated Build_ID for 8x62 and 8092
# 07/03/13   ajs     Added deviceprogrammer to 8974, 8x10 and 8x26 targets
# 06/26/13   plc     Adding support for 8092
# 06/13/13   kedara  Support 9x35 source compilation.
# 05/10/13   sy      Fixing 9x25 builds  and 8084 mistakes
# 05/06/13   plc     Adding support for 8x62
# 04/23/13   sy      Adding 8084 production build  
# 03/27/13   dh      Add 8084 build id GAAAANAZ, only compile boot for 8084
# 04/08/13   sg      adding emmcbld for 8x10 and 8x26
# 03/04/13   sy      Fixing 9x25 and 8974 Pack issue 
# 12/12/12   sy      Adding emmcbld to 8974
# 10/31/12   sy      Fix 8974 Pack 
# 09/18/12   sy      Enable 8x10 ,8x26 compilation 
# 09/09/12   sy      Adding Linux pack 
# 08/08/12   sy      Enable warning=error at the top level build script
# 08/01/12   sy      Adding emmcbld 
# 07/20/2012 sy      Adding 9x25 compilation
#===============================================================================
#===============================================================================
# Set up default paths
#===============================================================================
#!/bin/bash
export BUILD_ROOT=../..
export CORE_BSP_ROOT=$BUILD_ROOT/core/bsp
export TOOLS_SCONS_ROOT=$BUILD_ROOT/tools/build/scons

cd `dirname $0`
#-------------Usage --------------
function usage()
{
    echo "==============================================================="
    echo "# This script compiles boot binaries for ONE of the Bear family"
    echo "# i.e.  8916. "
    echo "# Target must be specific by TARGET_FAMILY"
    echo "# -------------------------------------------------------------"
    echo ""
    echo " ./build_all.sh                        # compile all available targets"
    echo " ./build.sh  --usage                   # display this menu"
    echo " ./build.sh  TARGET_FAMILY=8916 --prod # create customer build for 8916"
    echo " ./build.sh  TARGET_FAMILY=8916        # compiling 8916"
    echo " ./build.sh  -c TARGET_FAMILY=8916     # clean 8916 binaries"
    echo " ./build.sh  TARGET_FAMILY=8916        # compile 8916 binaries"
    echo " ./build.sh                            # Error!! target name is missing"
    echo ""
    echo " # To build individual module , use the target specific build comand\n"
    echo " ./build_8916.sh sbl1                       # compiling 8916 sbl1\n"
    echo " ./build_8916.sh boot                       # compiling 8916 boot\n"
    echo "==============================================================="
    sleep 2   
}
#-------- set the build dir and  reset the command line parameter ----- 
build_dir=`dirname $0`
cd $build_dir
#===============================================================================
#Set up  Environment 
#===============================================================================
usage


# if setenv.sh exist, run it.
[ -f $BUILD_ROOT/build/ms/setenv.sh ] && source setenv.sh || echo "Warning: SetEnv file not existed" 
#----- Parse target enviroment----
unset TARGET_FAMILY 
unset  BUILD_ID
unset cmds
isProd=0 

while [ $# -gt 0 ]
do
    case "$1" in
	BUILD_ID=*) 
		BUILD_ID=${1#BUILD_ID=}
		;;
	TARGET_FAMILY=*) 
		TARGET_FAMILY=${1#TARGET_FAMILY=}
		;;
	--usage) 
		usage
		exit 0
		;;	
	--prod) 
		isProd=1	
		cmds=${cmds//--prod/}
		if [ -e setenv.sh ] ; then #this is integrator build
			cmds="USES_FLAGS=USES_NO_STRIP_NO_ODM,USES_NO_DEBUG $cmds" 
		fi 
		;;
	*)
		cmds="$cmds $1"
    esac
    shift
done

source $TOOLS_SCONS_ROOT/build/rename-log.sh buildlog

#Set Default to 8916 
export TARGET_FAMILY=${TARGET_FAMILY:-8916}

#Enable warning=error 
cmds="CFLAGS=--diag_error=warning $cmds"

#8916 compilation
if [ "$TARGET_FAMILY" = "8916" ]; then
   if [ -z $BUILD_ID ]
   then
	if [ $isProd -gt 0 ]; then
		sh build_8916.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=HAAAANAZ $cmds
		sh build_8936.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=SAAAANAZ $cmds
	else
    		sh build_8916.sh boot jsdcc emmcbld deviceprogrammer $cmds
			sh build_8936.sh boot jsdcc emmcbld deviceprogrammer $cmds
	fi
   else
    	sh build_8916.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8936.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=$BUILD_ID $cmds
   fi
fi

#8936 compilation
if [ "$TARGET_FAMILY" = "8936" ]; then
   if [ -z $BUILD_ID ]
   then
	if [ $isProd -gt 0 ]; then
		sh build_8936.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=SAAAANAZ $cmds
		sh build_8916.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=HAAAANAZ $cmds
	else
    		sh build_8936.sh boot jsdcc emmcbld deviceprogrammer $cmds
			sh build_8916.sh boot jsdcc emmcbld deviceprogrammer $cmds
	fi
   else
    	sh build_8936.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=$BUILD_ID $cmds
		sh build_8916.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=$BUILD_ID $cmds
   fi
fi

#8909 compilation
if [ "$TARGET_FAMILY" = "8909" ]; then
   if [ -z $BUILD_ID ]
   then
	if [ $isProd -gt 0 ]; then
		sh build_8916.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=DAAAANAZ $cmds
	else
    		sh build_8909.sh boot jsdcc emmcbld deviceprogrammer $cmds
	fi
   else
    	sh build_8909.sh boot jsdcc emmcbld deviceprogrammer BUILD_ID=$BUILD_ID $cmds
   fi
fi

#9x45 compilation on nand and flashless
if [ "$TARGET_FAMILY" = "9x45" ]
then
    if [ -z $BUILD_ID ] 
    then
	if [ $isProd -gt 0 ]
	then 
	  sh build_9x45.sh boot BUILD_ID=AAATANAZ $cmds  
	  sh build_9x45.sh boot BUILD_ID=AAAHANAZ $cmds
	else 
	  sh build_9x45.sh boot BUILD_ID=AAATANAA $cmds  
	  sh build_9x45.sh boot BUILD_ID=AAAHANAA $cmds
	fi
   elif [[ $BUILD_ID =~ AAATANAZ ]] 
   then 
	sh build_9x45.sh boot BUILD_ID=$BUILD_ID $cmds  
   elif [[ $BUILD_ID =~ AAAHANAZ ]] 
   then
 	sh build_9x45.sh boot BUILD_ID=$BUILD_ID $cmds
   elif [[ $BUILD_ID =~ AAAT.. ]] 
   then 
	sh build_9x45.sh boot BUILD_ID=$BUILD_ID $cmds  
   elif [[ $BUILD_ID =~ AAAH.. ]] 
   then 
	sh build_9x45.sh boot BUILD_ID=$BUILD_ID $cmds
   else
	echo "Warning: build id require"
   fi
fi

