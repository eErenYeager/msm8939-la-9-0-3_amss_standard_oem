#ifndef CUSTSAAAANAZA_H
#define CUSTSAAAANAZA_H
/* ========================================================================
FILE: CUSTSAAAANAZA

Copyright (c) 2001-2017 by QUALCOMM Incorporated.  All Rights Reserved.        
=========================================================================== */

#ifndef TARGSAAAANAZA_H
   #include "targsaaaanaza.h"
#endif

#define FEATURE_QFUSE_PROGRAMMING
#define FEATURE_DLOAD_MEM_DEBUG
#define IMAGE_KEY_SBL1_IMG_DEST_ADDR SCL_SBL1_CODE_BASE
#define BOOT_TEMP_CHECK_THRESHOLD_DEGC
#define FEATURE_TPM_HASH_POPULATE




#endif /* CUSTSAAAANAZA_H */
