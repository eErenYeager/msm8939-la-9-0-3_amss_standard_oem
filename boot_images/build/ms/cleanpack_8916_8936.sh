#!/bin/bash

cd `dirname $0`
#First build to ensure no error
if [ $1 = "8936" ]
then
       echo  " Building for Target 8936 "
       sh build_8916_8936.sh TARGET_FAMILY=8936  --prod 
       sh build_8916_8936.sh TARGET_FAMILY=8936 BUILD_ID=SAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged 

       # Remove unship files
       sh build_8916_8936.sh TARGET_FAMILY=8936 BUILD_ID=SAAAANAZ  -c --implicit-deps-unchanged --cleanpack

       #recommpile
       sh build_8916_8936.sh TARGET_FAMILY=8936 BUILD_ID=SAAAANAZ 
fi

if [ $1 = "8974" ]
then
       echo  " Building for Target 8974 "
       sh build_8916_8936.sh TARGET_FAMILY=8974  --prod 
       sh build_8916_8936.sh TARGET_FAMILY=8974 BUILD_ID=AAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged 

       # Remove unship files
       sh build_8916_8936.sh TARGET_FAMILY=8974 BUILD_ID=AAAAANAZ  -c --implicit-deps-unchanged --cleanpack

       #recommpile
       sh build_8916_8936.sh TARGET_FAMILY=8974 BUILD_ID=AAAAANAZ 
fi

if [ $1 = "9x35" ]
then
       echo  " Building for Target 9x35 "
       sh build_8916_8936.sh TARGET_FAMILY=9x35  --prod
 
       sh build_8916_8936.sh TARGET_FAMILY=9x35 BUILD_ID=AAATANAZ USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged 
       sh build_8916_8936.sh TARGET_FAMILY=9x35 BUILD_ID=AAAHANAZ USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged 

       # Remove unship files
       sh build_8916_8936.sh TARGET_FAMILY=9x35 BUILD_ID=AAATANAZ -c --implicit-deps-unchanged --cleanpack
       sh build_8916_8936.sh TARGET_FAMILY=9x35 BUILD_ID=AAAHANAZ -c --implicit-deps-unchanged --cleanpack

       #recommpile
       sh build_8916_8936.sh TARGET_FAMILY=9x35 BUILD_ID=AAATANAZ
       sh build_8916_8936.sh TARGET_FAMILY=9x35 BUILD_ID=AAAHANAZ
       
fi

if [ $1 = "8x26" ]
then
       echo  " Building for Target 8x26 "
       sh build_8916_8936.sh TARGET_FAMILY=8x26  --prod

       sh build_8916_8936.sh TARGET_FAMILY=8x26 BUILD_ID=FAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged 

       # Remove unship files
       sh build_8916_8936.sh TARGET_FAMILY=8x26 BUILD_ID=FAAAANAZ  -c --implicit-deps-unchanged --cleanpack

       #recommpile
       sh build_8916_8936.sh TARGET_FAMILY=8x26 BUILD_ID=FAAAANAZ 
              
fi

if [ $1 = "8x10" ]
then
       echo  " Building for Target 8x10 "
       sh build_8916_8936.sh TARGET_FAMILY=8x10  --prod

       sh build_8916_8936.sh TARGET_FAMILY=8x10 BUILD_ID=DAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged 

       # Remove unship files
       sh build_8916_8936.sh TARGET_FAMILY=8x10 BUILD_ID=DAAAANAZ  -c --implicit-deps-unchanged --cleanpack

       #recommpile
       sh build_8916_8936.sh TARGET_FAMILY=8x10 BUILD_ID=DAAAANAZ 
              
fi


if [ $1 = "8084" ]
then
       echo  " Building for Target 8084 "
       sh build_8916_8936.sh TARGET_FAMILY=8084  --prod

       sh build_8916_8936.sh TARGET_FAMILY=8084 BUILD_ID=GAAAANAZ  USES_FLAGS=USES_CLEAN_PACK -c --implicit-deps-unchanged 

       # Remove unship files
       sh build_8916_8936.sh TARGET_FAMILY=8084 BUILD_ID=GAAAANAZ  -c --implicit-deps-unchanged --cleanpack

       #recommpile
       sh build_8916_8936.sh TARGET_FAMILY=8084 BUILD_ID=GAAAANAZ 
              
fi





