@echo off
rem ==========================================================================
rem
rem  RPM build system launcher
rem
rem Copyright (c) 2012 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary/GTDR
rem ==========================================================================
rem $Header: //components/rel/rpm.bf/2.0.c9/build/build_8929.bat#1 $

SET BUILD_ASIC=8929
SET MSM_ID=8929
SET HAL_PLATFORM=8929
SET TARGET_FAMILY=8929
SET CHIPSET=msm8929
SET SECPOLICY=USES_SEC_POLICY_DEFAULT_SIGN
python build_common.py %*
