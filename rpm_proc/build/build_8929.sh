# ==========================================================================
#
#  RPM build system launcher
#
# Copyright (c) 2012 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary/GTDR
# ==========================================================================
# $Header: //components/rel/rpm.bf/2.0.c9/build/build_8929.sh#1 $

export BUILD_ASIC=8929
export MSM_ID=8929
export HAL_PLATFORM=8929
export TARGET_FAMILY=8929
export CHIPSET=msm8929
export SECPOLICY=USES_SEC_POLICY_DEFAULT_SIGN

cd './rpm_proc/build/'

python ./build_common.py $@
