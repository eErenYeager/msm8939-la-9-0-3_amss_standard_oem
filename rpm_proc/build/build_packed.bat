@echo off

rem ==========================================================================
rem
rem RPM packed build launcher
rem
rem Copyright (c) 2012 by QUALCOMM, Incorporated.
rem All Rights Reserved.
rem QUALCOMM Proprietary and Confidential
rem ==========================================================================

SET SECPOLICY=USES_SEC_POLICY_DEFAULT_SIGN
cd ..
python build\packrat_bfam.py %*

