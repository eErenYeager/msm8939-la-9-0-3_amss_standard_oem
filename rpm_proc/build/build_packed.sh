# ==========================================================================
#
# RPM packed build launcher

# Copyright (c) 2012 by QUALCOMM, Incorporated.
# All Rights Reserved.
# QUALCOMM Proprietary and Confidential
# ==========================================================================

export SECPOLICY=USES_SEC_POLICY_DEFAULT_SIGN
cd ./rpm_proc/
python ./build/packrat_bfam.py $@

