#ifndef __ICB_RPM_HWIO_H__
#define __ICB_RPM_HWIO_H__
/*
===========================================================================
*/
/**
  @file icb_rpm_hwio.h

  @brief Additional HWIO definitions for ICB RPM driver.
*/
/*
===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

===========================================================================

  $Header: //components/rel/rpm.bf/2.0.c9/core/buses/icb/src/8916/icb_rpm_hwio.h#1 $
  $DateTime: 2015/03/13 04:27:04 $
  $Author: pwbldsvc $

===========================================================================
*/

#endif /* __ICB_RPM_HWIO_H__ */
