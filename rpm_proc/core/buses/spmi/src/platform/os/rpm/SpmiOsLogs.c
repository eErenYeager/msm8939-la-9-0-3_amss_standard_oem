/**
 * @file:  SpmiOsLogs.c
 * 
 * Copyright (c) 2013 by Qualcomm Technologies Incorporated. All Rights Reserved.
 * 
 * $DateTime: 2015/03/13 04:27:04 $
 * $Header: //components/rel/rpm.bf/2.0.c9/core/buses/spmi/src/platform/os/rpm/SpmiOsLogs.c#1 $
 * $Change: 7660976 $
 * 
 *                              Edit History
 * Date     Description
 * -------  -------------------------------------------------------------------
 * 10/1/13  Initial Version
 */

#include "SpmiOsLogs.h"

//******************************************************************************
// Global Data
//******************************************************************************

ULogHandle spmiLogHandle;
