/*===========================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#include <stdbool.h>
#include <string.h>

#include "comdef.h"
#include "HALhwio.h"
#include "HAL_rbcpr.h"
#include "HAL_rbcpr_hwio.h"
#include "err.h"
#include "timetick.h"
#include "HAL_rbcpr_qfprom.h"
#include "CoreVerify.h"

//Do 2 shifts to mask off the upper and lower bits we don't care about.
//Result will be in the lsb's of the returned value.


//unsigned HAL_rbcpr_read_fuse(HAL_rbcpr_fuse_setting* fuse_setting)
uint32 HAL_rbcpr_read_fuse_setting(uint32* fuse_address, int8 offset, int8 number_of_bits)
{
    return HAL_RBCPR_MASK_AND_SHIFT_TO_LSB(*fuse_address, offset, number_of_bits);
}

extern const HAL_rbcpr_bsp_type HAL_rbcpr_bsp_data;

void HAL_rbcpr_enable_block(HAL_rbcpr_rail_t rail, boolean enable)
{
    if (enable)
    {
        HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, HW_TO_PMIC_EN, 0x0);
        HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, LOOP_EN , 0x1);
    }
    else
    {
        HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, LOOP_EN , 0x0);
    }
}

void HAL_rbcpr_next_meas_en(HAL_rbcpr_rail_t rail, boolean recom_taken)
{
    if (recom_taken)
    {
        /* Any value will send a pulse to the block */
        HWIO_OUTX(rail->hw_base_address, RBIF_CONT_ACK_CMD, 0x1);
    }
    else
    {
        /* Any value will send a pulse to the block */
        HWIO_OUTX(rail->hw_base_address, RBIF_CONT_NACK_CMD, 0x1);
    }
}

void HAL_rbcpr_set_auto_loop_disable(HAL_rbcpr_rail_t rail, bool setting)
{
    HWIO_OUTXF(rail->hw_base_address, RBIF_TIMER_ADJUST, AUTO_LOOP_DISABLE, (setting ? 0x1 : 0x0));
}

void HAL_rbcpr_set_sw_auto_cont_ack(HAL_rbcpr_rail_t rail, bool auto_ack_enable)
{
    HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, SW_AUTO_CONT_ACK_EN, (auto_ack_enable ? 0x1 : 0x0));
}

void HAL_rbcpr_prgm_gcnt_target(HAL_rbcpr_rail_t rail, HAL_rbcpr_rosc_type osc, uint32 gcnt, uint32 target)
{
    uint32 val;
    uint32 *reg;

    /* For the mask we use TARGET0, GCNT0 register */
    val = (gcnt << HWIO_SHFT(RBCPR_GCNT_TARGET0, GCNT0))|target;

    /* Find the offset of the register from the first register */
    reg = (uint32 *)(HWIO_ADDRX(rail->hw_base_address, RBCPR_GCNT_TARGET0) + (osc*4));

    /* Now write the value */
    *reg = val;
	/* TN3 Masking For both TSMC and GF */	
	#ifdef RPM8936_TN3_MASKING
	if (osc == 2 || osc == 3)
	{	
	*reg = *reg & (~(HWIO_FMSK(RBCPR_GCNT_TARGET0,GCNT0)));
	}
	#endif
}

void HAL_rbcpr_write_stepquot(HAL_rbcpr_rail_t rail, uint32 step_quot, uint32 idle_clocks)
{
	HWIO_OUTXF(rail->hw_base_address, RBCPR_STEP_QUOT, STEP_QUOT, step_quot);
    HWIO_OUTXF(rail->hw_base_address, RBCPR_STEP_QUOT, IDLE_CLOCKS, idle_clocks);
}

void HAL_rbcpr_write_up_threshold(HAL_rbcpr_rail_t rail, uint32 error_steps)
{
    HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, UP_THRESHOLD, error_steps);
}

void HAL_rbcpr_write_dn_threshold(HAL_rbcpr_rail_t rail, uint32 error_steps)
{
    HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, DN_THRESHOLD, error_steps);
}

void HAL_rbcpr_write_consecutive_up(HAL_rbcpr_rail_t rail, uint32 consecutive_up)
{
    HWIO_OUTXF(rail->hw_base_address, RBIF_TIMER_ADJUST, CONSECUTIVE_UP, consecutive_up);
}

void HAL_rbcpr_write_consecutive_dn(HAL_rbcpr_rail_t rail, uint32 consecutive_nd)
{
    HWIO_OUTXF(rail->hw_base_address, RBIF_TIMER_ADJUST, CONSECUTIVE_DN, consecutive_nd);
}
void HAL_rbcpr_poll_status_and_dont_return_until_not_busy(HAL_rbcpr_rail_t rail)
{
    int fail_count=0;
    do{
        do{
            do{
                fail_count++;
                assert(fail_count<100);
                while (HAL_RBCPR_IDLE!=HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, BUSY));
            } while (HAL_RBCPR_IDLE!=HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, BUSY));
        } while (HAL_RBCPR_IDLE!=HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, BUSY));
    } while (HAL_RBCPR_IDLE!=HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, BUSY));
}
boolean HAL_rbcpr_poll_status(HAL_rbcpr_rail_t rail)
{
    int fail_count=0;
    do{
        do{
            do{
                do{
                    fail_count++;
                    if(fail_count>100)
                        return FALSE;
                } while (HAL_RBCPR_IDLE!=HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, BUSY));
            } while (HAL_RBCPR_IDLE!=HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, BUSY));
        } while (HAL_RBCPR_IDLE!=HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, BUSY));
    } while (HAL_RBCPR_IDLE!=HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, BUSY));
    
    return TRUE;
}

//To do - rework this, get it out of the HAL I think.
void HAL_rbcpr_save_log(HAL_rbcpr_rail_t rail, HAL_rbcpr_log_type* log_buffer, HAL_rbcpr_result_type *rbcpr_result, HAL_rbcpr_meas_source_type source, railway_corner corner)
{
    uint8 index = log_buffer->rbcpr_log_index;
    uint32 reg = HWIO_INX(rail->hw_base_address, RBCPR_LOG_0);

    log_buffer->rbcpr_logs[index].source = source;
    log_buffer->rbcpr_logs[index].corner = corner;
    log_buffer->rbcpr_logs[index].timestamp = (uint32)timetick_get_safe();
    log_buffer->rbcpr_logs[index].mid_count = (reg & HWIO_FMSK(RBCPR_LOG_0, MID_COUNT))>>HWIO_SHFT(RBCPR_LOG_0, MID_COUNT);
    log_buffer->rbcpr_logs[index].mx_vlevel = 0;
    log_buffer->rbcpr_logs[index].rbcpr_flag = (HAL_rbcpr_flag_type)((reg & 0x7C0)>>HWIO_SHFT(RBCPR_LOG_0, MIN_FLAG));
    log_buffer->rbcpr_logs[index].dig_level = (reg & HWIO_FMSK(RBCPR_LOG_0, VLEVEL))>>HWIO_SHFT(RBCPR_LOG_0, VLEVEL);
    memcpy(&log_buffer->rbcpr_logs[index].rbcpr_result, rbcpr_result, sizeof(HAL_rbcpr_result_type));
    log_buffer->rbcpr_log_index = (log_buffer->rbcpr_log_index + 1)%HAL_RBCPR_LOG_SIZE;
}

boolean HAL_rbcpr_poll_result(HAL_rbcpr_rail_t rail,  boolean allow_give_up, HAL_rbcpr_result_type *rbcpr_result)
{
    boolean success = TRUE;
    
    if(allow_give_up)
    {
        success = HAL_rbcpr_poll_status(rail);
    }
    else
    {
        HAL_rbcpr_poll_status_and_dont_return_until_not_busy(rail);
    }
    

    if(success)
    {
        rbcpr_result->error = HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, ERROR);
        rbcpr_result->error_steps = HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, ERROR_STEPS);
        rbcpr_result->step_up = HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, STEP_UP);
        rbcpr_result->step_down = HWIO_INXF(rail->hw_base_address, RBCPR_RESULT_0, STEP_DN);
    }
    else
    {
        rbcpr_result->error = 0;
        rbcpr_result->error_steps = 0;
        rbcpr_result->step_up = FALSE;
        rbcpr_result->step_down = FALSE;
    }

    return success;
}

void HAL_rbcpr_mask_sensor(HAL_rbcpr_rail_t rail, HAL_rbcpr_sensor_type sensor, boolean mask)
{
    uint32 reg, bit, bmask;
    volatile uint32 *reg_addr;

    /* Enable that particular sensor */
    if (sensor >= rail->sensors)
        ERR_FATAL("",0, 0, 0);

    /* Compute the register to write to and bit position  */
    reg = (sensor / 32);
    bit = (sensor % 32);
    bmask = (1 << bit);

    /* Find the offset of the register from the first register */
    reg_addr = &((volatile uint32 *)HWIO_ADDRX(rail->hw_base_address, RBCPR_SENSOR_MASK0))[reg];

    /* Mask Sensor */
    if (mask)
    {
        *reg_addr |= bmask;
    }
    else /* Unmask sensor */
    {
        *reg_addr = *reg_addr&(~bmask);
    }
}

void HAL_rbcpr_mask_sensor_all(HAL_rbcpr_rail_t rail, boolean mask)
{
    /* Mask all sensorss */
    if (mask)
    {
        HWIO_OUTX(rail->hw_base_address, RBCPR_SENSOR_MASK0, 0xFFFFFFFF);
        HWIO_OUTX(rail->hw_base_address, RBCPR_SENSOR_MASK1, 0xFFFFFFFF);
    }
    else /* Unmask all the sensors */
    {
        HWIO_OUTX(rail->hw_base_address, RBCPR_SENSOR_MASK0, 0x0);
        HWIO_OUTX(rail->hw_base_address, RBCPR_SENSOR_MASK1, 0x0);
    }
}

void HAL_rbcpr_bypass_sensor(HAL_rbcpr_rail_t rail, HAL_rbcpr_sensor_type sensor, boolean bypass)
{
    uint8 reg, bit, bmask;
    uint32 *reg_addr;

    /* Enable that particular sensor */
    if (sensor >= rail->sensors)
        ERR_FATAL("",0, 0, 0);

    /* Compute the register to write to and bit position  */
    reg = (sensor / 32);
    bit = (sensor % 32);
    bmask = (1 << bit);

    /* Find the offset of the register from the first register */
    reg_addr = (uint32 *)(HWIO_ADDRX(rail->hw_base_address, RBCPR_SENSOR_BYPASS0) + reg*4);

    /* Mask Sensor */
    if (bypass)
    {
        *reg_addr |= bmask;
    }
    else /* Unmask sensor */
    {
        *reg_addr = *reg_addr &(~bmask);
    }
}

void HAL_rbcpr_bypass_sensor_all(HAL_rbcpr_rail_t rail, boolean bypass)
{
    /* Bypass all sensorss */
    if (bypass)
    {
        HWIO_OUTX(rail->hw_base_address, RBCPR_SENSOR_BYPASS0, 0xFFFFFFFF);
        HWIO_OUTX(rail->hw_base_address, RBCPR_SENSOR_BYPASS1, 0xFFFFFFFF);
    }
    else /* Bypass none of the sensors */
    {
        HWIO_OUTX(rail->hw_base_address, RBCPR_SENSOR_BYPASS0, 0x0);
        HWIO_OUTX(rail->hw_base_address, RBCPR_SENSOR_BYPASS1, 0x0);
    }
}

void HAL_config_timers(HAL_rbcpr_rail_t rail)
{
    // Convert the time in msecs to clock cycles
    // Assume clock is 19.2 MHz XO clock */
    uint32 interval = rail->timer_config.interval * 19200;
    HAL_rbcpr_pgm_timer(rail, interval, rail->timer_config.enable);
}

void HAL_rbcpr_pgm_timer(HAL_rbcpr_rail_t rail, uint32 interval, boolean enable)
{
    /* Program the timer */
    HWIO_OUTX(rail->hw_base_address, RBCPR_TIMER_INTERVAL, interval);

    /* Enable the timer */
    if (enable)
    {
        HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, TIMER_EN, 0x1);
    }
    else
    {
        HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, TIMER_EN, 0x0);
    }
}

void HAL_rbcpr_enable_int(HAL_rbcpr_rail_t rail, uint32 interrupt_mask, uint8 enable)
{
    /* Find the offset of the register from the first register */
    // for now we only use interrupt 0.
    volatile uint32 *reg_addr = (volatile uint32 *)(HWIO_ADDRX(rail->hw_base_address, RBIF_IRQ_EN_0));
    uint32 reg = *reg_addr;

    if(enable)
    {
        reg |= interrupt_mask;
    }
    else
    {
        reg &= ~interrupt_mask;
    }

    /* Now write to the register */
    *reg_addr = reg;
}

void HAL_rbcpr_clear_int(HAL_rbcpr_rail_t rail, boolean enable)
{
    uint8 bitmask = 0, status;

    /* Read the interrupt status register */
    status = HWIO_INX(rail->hw_base_address, RBIF_IRQ_STATUS);
    /* Zero out the mask to clear the set bits. Might need to revisit as this is pointless */
    bitmask = status ^ status ;
    /* Now write to the register */
    HWIO_OUTX(rail->hw_base_address, RBIF_IRQ_CLEAR, bitmask);
}

void HAL_rbcpr_clear_int_all(HAL_rbcpr_rail_t rail)
{
    /* We want to clear all the interrupts. So write 0x7F */
    HWIO_OUTX(rail->hw_base_address, RBIF_IRQ_CLEAR, ((1 << 7) - 1));
}

void HAL_config_vlimits(HAL_rbcpr_rail_t rail)
{
    /* Write to the SW_VLEVEL register as default values don't work */
    HWIO_OUTX(rail->hw_base_address, RBIF_SW_VLEVEL, rail->settings.vlevel);

    /* Write the ceiling and floor limits to the register */
    HWIO_OUTX(rail->hw_base_address, RBIF_LIMIT, rail->settings.ceiling<<HWIO_SHFT(RBIF_LIMIT, CEILING)|rail->settings.floor);
}

void HAL_rbcpr_set_sw_auto_cont_nack_dn(HAL_rbcpr_rail_t rail, bool enable)
{
    HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, SW_AUTO_CONT_NACK_DN_EN, (enable ? 0x1 : 0));
}

bool HAL_rbcpr_use_redundant_fuses(void)
{
#if 0
    //Redundancy select is stored in QFPROM_CORR_CALIB_ROW1_MSB[31:29]
    unsigned fuse = HWIO_IN(QFPROM_CORR_CALIB_ROW1_MSB);
    unsigned redundancy_select = MASK_AND_SHIFT_TO_LSB(fuse, 29, 1);

    //redundancy_select == 0x1 implies that the redundant fuses are to be
    //used for CPR settings.
    return (redundancy_select == 0x1);
#endif 
	return 0;
}

void HAL_rbcpr_read_efuse
(
    HAL_rbcpr_rail_t            rail,
    bool                        use_redundant_fuses,
    int8                       *fuse_settings,
    HAL_rbcpr_fuse_type       corner
)
{
    int32                       target_voltage;
    CORE_VERIFY_PTR(fuse_settings);

    const HAL_rbcpr_efuse_info_type  *fuse = use_redundant_fuses ? \
                                      &(rail->efuse_info_rd) : &(rail->efuse_info);
    const HAL_rbcpr_fuse_setting     *fuse_target = NULL;

    CORE_VERIFY_PTR(fuse);

    CORE_VERIFY(corner < HAL_RBCPR_FUSE_COUNT);

    fuse_target = &fuse->target_voltages[corner];
	
    
    // TARGET_VOLTAGE is 5 bits
     target_voltage = HAL_rbcpr_read_fuse_setting( (uint32 *)fuse_target->fuse_addresses[fuse_target->fuse_index],
	                                               fuse_target->offset, 5);
	 
    *fuse_settings = (target_voltage & 0x10) ? \
                    -(target_voltage & 0xF) : (target_voltage & 0xF);
}

void HAL_rbcpr_collect_coredump(HAL_rbcpr_rail_t rail, HAL_rbcpr_log_type* log_buffer)
{
    uint8 i, index;
    HAL_rbcpr_coredump_type *coredump;

    index = log_buffer->coredump_index;
    coredump = &log_buffer->rbcpr_coredump[index];

    coredump->limit = HWIO_INX(rail->hw_base_address, RBIF_LIMIT);
    coredump->step_quot = HWIO_INX(rail->hw_base_address, RBCPR_STEP_QUOT);

    for(i=0; i<8; i++)
        coredump->gcnt_target[i] = *((uint32 *)(HWIO_ADDRX(rail->hw_base_address, RBCPR_GCNT_TARGET0) + i*4));
    coredump->irq0 = HWIO_INX(rail->hw_base_address, RBIF_IRQ_EN_0);
    coredump->irq_status = HWIO_INX(rail->hw_base_address, RBIF_IRQ_STATUS);

    index ^= 0x1;
    log_buffer->coredump_index = index;
}

HAL_rbcpr_rail_t HAL_rbcpr_get_rail_handle(const char* rail_name)
{
    const HAL_rbcpr_rail_s *rail_data = NULL;
    for(int i=0; i<HAL_rbcpr_bsp_data.num_rails; i++)
    {
        rail_data = &HAL_rbcpr_bsp_data.rails[i];
        if(!strcmp(rail_name, rail_data->name))
            return rail_data;
    }
    assert(0);
    return NULL;    //Can't actually get here
}

void HAL_rbcpr_set_count_mode(HAL_rbcpr_rail_t rail, HAL_rbcpr_count_mode mode)
{
    switch(mode)
    {
        case HAL_RBCPR_ALL_AT_ONCE:
            HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, COUNT_MODE , 0x0);
            break;
        case HAL_RBCPR_STAGGERED:
            HWIO_OUTXF(rail->hw_base_address, RBCPR_CTL, COUNT_MODE , 0x1);
            break;
        default:
            abort();
    };
}

void HAL_rbcpr_save_hw_state(HAL_rbcpr_rail_t rail)
{
    HAL_rbcpr_dump* dump = (HAL_rbcpr_dump*)&rail->core_dump;
    dump->rbcpr_timer_interval = HWIO_INX(rail->hw_base_address, RBCPR_TIMER_INTERVAL);
    dump->rbif_timer_adjust = HWIO_INX(rail->hw_base_address, RBIF_TIMER_ADJUST);
    dump->rbcpr_ctl = HWIO_INX(rail->hw_base_address, RBCPR_CTL);
    dump->rbcpr_result_0 = HWIO_INX(rail->hw_base_address, RBCPR_RESULT_0);
    dump->rbcpr_result_1 = HWIO_INX(rail->hw_base_address, RBCPR_RESULT_1);
    dump->rbif_irq_en_0 = HWIO_INX(rail->hw_base_address, RBIF_IRQ_EN_0);
    dump->rbif_irq_status = HWIO_INX(rail->hw_base_address, RBIF_IRQ_STATUS);
    dump->rbcpr_debug0 = HWIO_INX(rail->hw_base_address, RBCPR_DEBUG0);
    dump->rbcpr_debug1 = HWIO_INX(rail->hw_base_address, RBCPR_DEBUG1);
    dump->rbcpr_debug2 = HWIO_INX(rail->hw_base_address, RBCPR_DEBUG2);
    dump->rbcpr_log_0 = HWIO_INX(rail->hw_base_address, RBCPR_LOG_0);
    dump->rbcpr_log_1 = HWIO_INX(rail->hw_base_address, RBCPR_LOG_1);
    dump->rbcpr_log_2 = HWIO_INX(rail->hw_base_address, RBCPR_LOG_2);
    dump->rbcpr_log_3 = HWIO_INX(rail->hw_base_address, RBCPR_LOG_3);
    dump->rbcpr_log_4 = HWIO_INX(rail->hw_base_address, RBCPR_LOG_4);
    dump->rbcpr_log_5 = HWIO_INX(rail->hw_base_address, RBCPR_LOG_5);
    dump->rbcpr_log_6 = HWIO_INX(rail->hw_base_address, RBCPR_LOG_6);
    dump->rbcpr_log_7 = HWIO_INX(rail->hw_base_address, RBCPR_LOG_7);
}
