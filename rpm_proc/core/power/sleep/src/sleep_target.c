/*============================================================================
  FILE:         sleep_target.c

  OVERVIEW:     This file provides target-specific functionality for the RPM.

  DEPENDENCIES: None

                Copyright (c) 2011-2014 QUALCOMM Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary

  $Header: //components/rel/rpm.bf/2.0.c9/core/power/sleep/src/sleep_target.c#1 $
  $DateTime: 2015/03/13 04:27:04 $
  $Author: pwbldsvc $
============================================================================*/

#include "comdef.h"
#include "npa.h"
#include "npa_resource.h"
#include "rpm_definitions.h"
#include "railway.h"
#include "assert.h"

#include "CoreVerify.h"

#define ARRAY_SIZE(x) (sizeof(x)/sizeof((x)[0]))

static uint32 cxo_needed = 1;

extern npa_resource_definition *sleep_lpr_resource;
extern npa_node_definition sleep_uber_node;
static npa_client_handle   uber_node_handle;
int mx_id, cx_id;


void sleep_target_swfi(void)
{
  __wfi();
}

static void sleep_monitor_npa
(
   void *context,
   unsigned int event_type,
   void *data,
   unsigned int data_size
)
{
  uint32 request = 0;
  uint32 state = 0;
  npa_event_data *ed = (npa_event_data *)data;
  state = ed->state;

  if(state & RPM_CXO_NEEDED)
    request |= RPM_CXO_NEEDED;

  npa_issue_required_request(uber_node_handle, request);
}

typedef struct
{
  const char *name;
  uint32     *state;
  uint32      min_allowed;
  uint32      max_allowed;
} sleep_monitor_type;

static sleep_monitor_type sleep_monitor_table[1] =
{
  { "/xo/cxo", &cxo_needed, 0, 1 },
};

void sleep_monitor_init
(
   void *context,
   unsigned int event_type,
   void *data,
   unsigned int data_len
)
{
  int i;

  for(i = 0; i < ARRAY_SIZE(sleep_monitor_table); i++)
  {
    CORE_VERIFY_PTR(npa_create_change_event_cb(sleep_monitor_table[i].name,
                                              "sleep",
                                               sleep_monitor_npa,
                                               &sleep_monitor_table[i]));
  }
}

void sleep_target_init(void)
{
  npa_resource_state initial_state[] = { 0xffffffff };
  int                i;
  const char        *sleep_list[ARRAY_SIZE(sleep_monitor_table)];

  /* define uber node */
  npa_define_node(&sleep_uber_node, initial_state, NULL);

  uber_node_handle = npa_create_sync_client("/sleep/uber",
                                            "sleep",
                                            NPA_CLIENT_REQUIRED);
  CORE_VERIFY_PTR(uber_node_handle);

  /* vote against all sleep modes until sleep_monitor_update gets a chance to run */
  npa_issue_required_request(uber_node_handle, NPA_MAX_STATE);

  /* set up the monitor to enable sleep on XO's once they're available */
  for(i = 0; i < ARRAY_SIZE(sleep_monitor_table); i++)
  {
    sleep_list[i] = sleep_monitor_table[i].name;
  }
  npa_resources_available_cb(ARRAY_SIZE(sleep_list),
                             sleep_list, sleep_monitor_init, NULL);
}

