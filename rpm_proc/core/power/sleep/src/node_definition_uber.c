/*============================================================================
  FILE:         node_definition_pxo.c
 
  OVERVIEW:     This file provides the NPA node definition for the 
                /xo/pxo node.
 
  DEPENDENCIES: None
 
                Copyright (c) 2010 Qualcomm Technologies Incorporated.
                All Rights Reserved.
                Qualcomm Confidential and Proprietary
============================================================================*/
#include "comdef.h"
#include "npa.h"
#include "npa_resource.h"
#include "CoreVerify.h"
#include "rpm_definitions.h"


#include "mpm.h"

void sleep_modes_init(void);

static npa_resource_state sleep_uber_driver ( npa_resource *resource,
                                             npa_client *client,
                                             npa_resource_state state )
{

  //halt is enabled by default
  npa_resource_state enabled_modes = 1;

  /* At init time, register the LPRs with the sleep LPR node */
  if(client->type == NPA_CLIENT_INITIALIZE)
  {



    //initialize sleep modes if necessary
    sleep_modes_init();

  }
  /* At run time, enable the LPRM flags for modes that have met their
   * activation criteria. */

  if(0 == (state & (RPM_CXO_NEEDED)))
    enabled_modes |= 2;
  if(0 == (state & (RPM_VDD_DIG_NEEDED | RPM_VDD_MEM_NEEDED | RPM_CXO_NEEDED)))
    enabled_modes |= 4;
    

  return state;
}


static npa_resource_definition sleep_uber_resource[] = 
{ 
  {
    "/sleep/uber",        /* Name */
    "on/off",             /* Units */
    0x7,                  /* Max State */
    &npa_or_plugin,       /* Plugin */
    NPA_RESOURCE_DEFAULT, /* Attributes */
    NULL,                 /* User Data */
  }
};

npa_node_definition sleep_uber_node = 
{ 
  "/node/sleep/uber", /* name */
  sleep_uber_driver,  /* driver_fcn */
  NPA_NODE_DEFAULT,   /* attributes */
  NULL,               /* data */
  0, NULL,            /* dependency count, dependency list */
  NPA_ARRAY(sleep_uber_resource)
};

