/*===========================================================================
                              railway_aggregator.c

SERVICES:

DESCRIPTION:

INITIALIZATION AND SEQUENCING REQUIREMENTS:
  Description...

Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


//===========================================================================
//                     Includes and Variable Definitions
//===========================================================================

//---------------------------------------------------------------------------
// Include Files
//---------------------------------------------------------------------------
#include "railway.h"
#include "CoreVerify.h"
#include "railway_config.h"
#include "pmapp_npa.h"
#include "pm_resources_and_types.h"

//---------------------------------------------------------------------------
// Type Declarations
//---------------------------------------------------------------------------
struct railway_sw_mode_state;

typedef struct railway_sw_mode_voter_s {
    pm_npa_sw_mode_smps_type sw_mode;
    int id;
    struct railway_sw_mode_state* root_ptr;
    railway_sw_mode_voter_t voter_link;       //Need a better plan for the linked list.
} railway_sw_mode_voter_s;

typedef void (*railway_sw_mode_transition_fn)(pm_npa_sw_mode_smps_type aggregated_vote);

typedef struct railway_sw_mode_state
{
    railway_sw_mode_voter_t sw_mode_voter_list_head;
    pm_npa_sw_mode_smps_type current_sw_mode;
    pm_railway_type_info_type pm_rail_id;
} railway_sw_mode_state;

//These MUST be in the same order as the rail definitions in the railway config.
static railway_sw_mode_state railway_sw_mode[] =
{
    {   ///Mx voters
        .current_sw_mode = PM_NPA_SW_MODE_SMPS__INVALID,
        .pm_rail_id=PM_RAILWAY_MX,
    },
    {   //Cx voters
        .current_sw_mode = PM_NPA_SW_MODE_SMPS__INVALID,
        .pm_rail_id=PM_RAILWAY_CX,
    },
    {   //Gfx voters
        .current_sw_mode = PM_NPA_SW_MODE_SMPS__INVALID,
        .pm_rail_id=PM_RAILWAY_GFX,
    },
};

//We don't actually care about the rail ID since we treat the automode as coupled for
//vddcx and vddmx
railway_sw_mode_voter_t railway_create_sw_mode_voter(int rail, int id)
{
  //Rail ID goes from 0 to (num_rails-1)
  CORE_VERIFY(rail<RAILWAY_CONFIG_DATA->num_rails);
  //Store all the voters in a linked list. To do, make this nicer.
  railway_sw_mode_voter_t voter = (railway_sw_mode_voter_t)malloc(sizeof(railway_sw_mode_voter_s));
  CORE_VERIFY_PTR(voter);
  voter->sw_mode = PM_NPA_SW_MODE_SMPS__INVALID;
  voter->id = id;
  voter->root_ptr = &railway_sw_mode[rail];
  voter->voter_link = voter->root_ptr->sw_mode_voter_list_head;   //Will set the first voter's link to NULL, after that it just pushes out the list.
  voter->root_ptr->sw_mode_voter_list_head = voter;
  return voter;
}

static pm_npa_sw_mode_smps_type railway_aggregated_sw_mode_vote(const railway_sw_mode_state* railway_sw_mode_ptr)
{   
    railway_sw_mode_voter_t voter = railway_sw_mode_ptr->sw_mode_voter_list_head;

    //We aggregate to find the higest value of all the voters.
    pm_npa_sw_mode_smps_type ret = PM_NPA_SW_MODE_SMPS__AUTO;
    while (voter)
    {
        //Ignore invalid votes. That implies the voter isn't yet used.
        if((voter->sw_mode != PM_NPA_SW_MODE_SMPS__INVALID) &&  (voter->sw_mode>ret))
        {
            ret = voter->sw_mode;
        }
    voter = (railway_sw_mode_voter_t)voter->voter_link;
    }
    return ret;
}

// Update this voter's sw mode vote. The default value is PM_NPA_SW_MODE_SMPS__AUTO
// Changing a vote will result in all voters being agregated, and if the agregation changes
// then the sw mode request will be forwarded to the PMIC driver straight away.
void railway_sw_mode_vote(railway_sw_mode_voter_t voter, pm_npa_sw_mode_smps_type sw_mode)
{
    CORE_VERIFY(voter);
    if(voter->sw_mode == sw_mode)
    {
        return; //Nothing to do.
    }

    voter->sw_mode = sw_mode;
    railway_sw_mode_state* const railway_sw_mode_ptr = voter->root_ptr;
    pm_npa_sw_mode_smps_type aggregated_vote = railway_aggregated_sw_mode_vote(railway_sw_mode_ptr);
    if(railway_sw_mode_ptr->current_sw_mode!=aggregated_vote)
    {
        const pm_sw_mode_type translated_vote = (aggregated_vote == PM_NPA_SW_MODE_SMPS__AUTO ? PM_SW_MODE_AUTO : PM_SW_MODE_NPM);
        pm_railway_set_mode(railway_sw_mode_ptr->pm_rail_id, translated_vote);
        railway_sw_mode_ptr->current_sw_mode=aggregated_vote;
    }
}
