/*===========================================================================
                              railway.c

SERVICES:

DESCRIPTION: railway.c - railway top-level implementation

INITIALIZATION AND SEQUENCING REQUIREMENTS:
  Description...

Copyright (c) 2014 Qualcomm Technologies, Incorporated.  All Rights Reserved.
QUALCOMM Proprietary.  Export of this technology or software is regulated
by the U.S. Government. Diversion contrary to U.S. law prohibited.
===========================================================================*/


//===========================================================================
//                     Includes and Variable Definitions
//===========================================================================

//---------------------------------------------------------------------------
// Include Files
//---------------------------------------------------------------------------
#include "CoreVerify.h"
#include "DALSys.h"

#include "railway.h"

#include "railway_internal.h"
#include "railway_config.h"
#include "rpmserver.h"
#include "swevent.h"
#include "alloca.h"

//---------------------------------------------------------------------------
// Constant / Define Declarations
//--------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Type Declarations
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Global Constant Definitions
//---------------------------------------------------------------------------
int railway_mx_bypass_count = 0;

//---------------------------------------------------------------------------
// Local Object Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Static Variable Definitions
//---------------------------------------------------------------------------
static const railway_internal_cb_type railway_callback_remap[RAILWAY_NUM_CB_TYPES] =
{
    RAILWAY_INTERNAL_PRECHANGE_CB,      //RAILWAY_PRECHANGE_CB
    RAILWAY_INTERNAL_POSTCHANGE_CB,     //RAILWAY_POSTCHANGE_CB
    RAILWAY_INTERNAL_PRECHANGE_CB,      //RAILWAY_LAST_PRECHANGE_CB
    RAILWAY_INTERNAL_POSTCHANGE_CB,     //RAILWAY_FIRST_POSTCHANGE_CB
};


//---------------------------------------------------------------------------
// Forward Declarations
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// External References
//---------------------------------------------------------------------------


//===========================================================================
//                             Macro Definitions
//===========================================================================

//===========================================================================
//                           Function Definitions
//===========================================================================
// 


/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
void railway_set_voltage_settling_cb(int rail, voltage_settling_cb_type callback, void* cookie)
{
    //Don't want to set this thing twice.
    CORE_VERIFY(railway.rail_state[rail].voltage_settling_cb==NULL);

    railway.rail_state[rail].voltage_settling_cb = callback;
    railway.rail_state[rail].voltage_settling_cb_cookie = cookie;
}

/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static void railway_update_sleep(int rail)
{
  uint32 sleep_vote = RAILWAY_CONFIG_DATA->rails[rail].sleep_mask;
  railway_rail_state_t *rail_data = &railway.rail_state[rail];

  if(rail_data->sleep_handle)
  {
    // This rail is required to stay on if it has a *required* request to stay on.
    // We make the distinction of required vs. supressible here, and *do not*
    // look at supressible for the purposes of preventing the sleep of a rail.
    railway_settings sleep_target;
    railway_aggregated_voltage_target_uv(rail, &sleep_target, true);

    //if the votes aggregate to retention mode and retention voltage, tell sleep we can do vdd_min
    if ((sleep_target.mode <= RAILWAY_RETENTION) &&
        (sleep_target.microvolts <= rail_data->corner_uvs[RAILWAY_RETENTION]))
      sleep_vote = 0;

    npa_issue_required_request(rail_data->sleep_handle, sleep_vote);
  }
}

/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static void railway_setup_sleep_handle(void* rail_void, uint32 unused, const char **resources, uint32 num_resources)
{
  const int rail = (int)rail_void;

  const char* vreg = RAILWAY_CONFIG_DATA->rails[rail].vreg_name;

  // The sleep voting resource has come up now, so we can get a handle to it.
  railway.rail_state[rail].sleep_handle = npa_create_sync_client("/sleep/uber", vreg, NPA_CLIENT_REQUIRED);
  CORE_VERIFY_PTR(railway.rail_state[rail].sleep_handle);

  // Check if we need to vote against sleep on this resource immediately.
  railway_update_sleep(rail);
}

/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static void initialize_rail(uint32 rail_num)
{
    const railway_rail_config_t *rail_config = &RAILWAY_CONFIG_DATA->rails[rail_num];

    railway_rail_state_t* rail_state = &railway.rail_state[rail_num];

    memcpy(rail_state->corner_uvs, rail_config->default_uvs, sizeof(uint32[RAILWAY_CORNERS_COUNT]));

    // Compute what our initial request should be.
    railway_corner corner = rail_config->initial_corner;
    uint32 uv             = rail_state->corner_uvs[corner];

    // Update our initial state.
    rail_state->current_active.mode       = corner;
    rail_state->current_active.microvolts = uv;

    // Set up our sleep requests as well.
    if(rail_config->sleep_mask)
        npa_resource_available_cb("/sleep/uber", (npa_callback)railway_setup_sleep_handle, (void *)rail_num);

    if(rail_config->supports_sw_mode)
    {
        rail_state->sm_mode_voter = railway_create_sw_mode_voter(rail_num, 0x10+rail_num);   //To do - get some better ids.
    }
}

/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static void railway_walk_cbs(const railway_rail_state_t *rail_data, railway_cb_type type, const railway_settings *proposal)
{
    //Must be done from 0 upwards as we have some callbacks which must be called first which are in the 0th slot.
    for(int i=0; i<RAILWAY_MAX_CBS; i++)
    {
        if(rail_data->cbs[type][i].callback_fn)
        {
            rail_data->cbs[type][i].callback_fn(proposal, rail_data->cbs[type][i].callback_cookie);
        }
    }
}

/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static bool railway_transition_individual_rail(int rail_num)
{
    railway_rail_state_t *rail_data = &railway.rail_state[rail_num];
    const railway_settings* target = &rail_data->constrained_target;

    if(RAILWAY_CONFIG_DATA->rails[rail_num].supports_sw_mode)
    {
        if(target->mode > RAILWAY_CONFIG_DATA->rails[rail_num].maximum_for_automode)
        {
            railway_sw_mode_vote(rail_data->sm_mode_voter, PM_NPA_SW_MODE_SMPS__NPM);
        }
    }

    railway_walk_cbs(rail_data, RAILWAY_PRECHANGE_CB, target);

    SWEVENT(RAILWAY_CHANGE_VOLTAGE, rail_num, target->microvolts);

    // Only set the actual voltage if it's changing. It could be that we're just changing modes.
    // E.g. if CPR has taken the voltage down in one mode to the point that it is the same voltage as another mode.
    if(railway.rail_state[rail_num].constrained_target.microvolts!=railway.rail_state[rail_num].current_active.microvolts)
    {
        pm_railway_set_voltage(RAILWAY_CONFIG_DATA->rails[rail_num].pm_rail_id, target->microvolts);
    }

    if(railway.rail_state[rail_num].voltage_settling_cb)
    {
        if(railway.rail_state[rail_num].voltage_settling_cb(target, railway.rail_state[rail_num].voltage_settling_cb_cookie))
        {
            return true;
        }
    }
    
    memcpy(&rail_data->current_active, target, sizeof(railway_settings));
    railway_walk_cbs(rail_data, RAILWAY_POSTCHANGE_CB, target);

    if(RAILWAY_CONFIG_DATA->rails[rail_num].supports_sw_mode)
    {
        if(target->mode <= RAILWAY_CONFIG_DATA->rails[rail_num].maximum_for_automode)
        {
            railway_sw_mode_vote(rail_data->sm_mode_voter, PM_NPA_SW_MODE_SMPS__AUTO);
        }
    }
    return false;
}

static void railway_quantize_constrained_target(int rail)
{
    const railway_corner* supported_corners = RAILWAY_CONFIG_DATA->rails[rail].supported_corners;
    const railway_corner highest_supported_corner = supported_corners[RAILWAY_CONFIG_DATA->rails[rail].supported_corners_count-1];

    //Assert that we don't have an explicit request greater than our highest supported corner.
    assert(railway.rail_state[rail].corner_uvs[highest_supported_corner]
            >=railway.rail_state[rail].constrained_target.microvolts);

    railway_corner quantized_corner=highest_supported_corner;

    // -2 because we don't need to check the highest supported corner - we start there.
    for(int i=RAILWAY_CONFIG_DATA->rails[rail].supported_corners_count-2; i>=0; i--)
    {
        //Check that we're not going lower than the constrained target's mode.
        if(supported_corners[i]<railway.rail_state[rail].constrained_target.mode)
        {
            break;
        }

        if(railway.rail_state[rail].constrained_target.microvolts>
            railway.rail_state[rail].corner_uvs[supported_corners[i]])
        {
            break;
        }
        quantized_corner = supported_corners[i];
    }

    railway.rail_state[rail].constrained_target.mode = quantized_corner;
    railway.rail_state[rail].constrained_target.microvolts = railway.rail_state[rail].corner_uvs[quantized_corner];
}

/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static void railway_update_constrained_targets( void )
{
    //Go through the rails and calculate their constrained voltage target.

    //Start from the unconstrained_target (which is the minimum for each rail) and work up until we settle on voltages.
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        memcpy(&railway.rail_state[i].constrained_target,
            &railway.rail_state[i].unconstrained_target,
            sizeof(railway_settings));
    }

    //Now, quantize logical rails to a corner.
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        if(i==MX_RAIL_ID)   //Skip Mx, we do that later.
            continue;

        railway_quantize_constrained_target(i);
    }

    //Now assert that the logical rails are multiples of the PMIC step size.
    //It's up to CPR to ensure that the corner values it sets are multiples of the PMIC step size.
    railway_rail_state_t* mx_rail = &railway.rail_state[MX_RAIL_ID];
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        if(i==MX_RAIL_ID)   //Skip Mx at this point as we're only looking at logical rails.
            continue;

        assert(!(railway.rail_state[i].constrained_target.microvolts%RAILWAY_CONFIG_DATA->rails[i].pmic_step_size));
    }

    //Next, bump up MX to the max of the logical rails.
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        if(i==MX_RAIL_ID)   //Skip comparison with ourselves.
            continue;

        mx_rail->constrained_target.mode =
            MAX(mx_rail->constrained_target.mode, railway.rail_state[i].constrained_target.mode);
        mx_rail->constrained_target.microvolts =
            MAX(mx_rail->corner_uvs[mx_rail->constrained_target.mode], mx_rail->constrained_target.microvolts);
    }

    //Now quantize Mx.
    railway_quantize_constrained_target(MX_RAIL_ID);
}

//Returns true if the algorithm needs to be run again.
/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static bool railway_do_transition_rail (bool chip_sleep_imminent)
{
    for(int i=0; i<RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        //To do - move this to when we update votes on rails - I'm sure that will be more efficient.
        //Need to figure out how we deal with CPR changing corner voltages under our feet though.
        railway_aggregated_voltage_target_uv(i, &railway.rail_state[i].unconstrained_target, false);
    }

    //Now update the constrained targets for the rails.
    railway_update_constrained_targets();

    //Now do the transitions.
    //If MX is going up, do that first.
    if(railway.rail_state[MX_RAIL_ID].constrained_target.microvolts>railway.rail_state[MX_RAIL_ID].current_active.microvolts)
    {
        CORE_VERIFY(!railway_transition_individual_rail(MX_RAIL_ID));
    }

    // Now transition the logical rails
    for (int i = 0; i < RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        if (i == MX_RAIL_ID)
            continue;

        if((railway.rail_state[i].constrained_target.microvolts!=railway.rail_state[i].current_active.microvolts) ||
            (railway.rail_state[i].constrained_target.mode!=railway.rail_state[i].current_active.mode))
        {
            if(railway_transition_individual_rail(i))
            {
                return true;
            }
        }
    }

    // Now transition Mx if it's going down.
    if (railway.rail_state[MX_RAIL_ID].constrained_target.microvolts <
        railway.rail_state[MX_RAIL_ID].current_active.microvolts)
    {
      if (! chip_sleep_imminent)
      {
        CORE_VERIFY (!railway_transition_individual_rail (MX_RAIL_ID));
      }
      else
      {
        railway_mx_bypass_count ++;
      }
    }

    // Assertion that current_active == constrained_target for all rails.
    for (int i = 0; i < RAILWAY_CONFIG_DATA->num_rails; i++)
    {
		if(!((chip_sleep_imminent) && (i == MX_RAIL_ID)))
		{
			assert (railway.rail_state[i].constrained_target.microvolts == railway.rail_state[i].current_active.microvolts);
		}
    }

    for (int i = 0; i < RAILWAY_CONFIG_DATA->num_rails; i++)
    {
      railway_update_sleep (i);
    }
    
    return false;
}

static bool railway_transition_in_progress = false;
extern bool sleep_deep_without_mx (void);

/*===========================================================================
FUNCTION: Top level fn for transitioning a rail. Actually transitions all rails, not just rail_num.

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
void railway_transition_rail(int rail_num)
{
    assert(!railway_transition_in_progress);    //We don't support this fn becoming re-entrant for now. Guard against it.
    railway_transition_in_progress = true;

    bool chip_sleep_imminent = sleep_deep_without_mx ();
    
    while(railway_do_transition_rail (chip_sleep_imminent));

    railway_transition_in_progress = false;
}


/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
void railway_transitioner_init(void)
{
    for(uint32 i = 0; i < RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        initialize_rail(i);
    }
#ifdef MSM8936_IMAGE_MX_OVERRIDE
    railway_override_rail_voltage(MX_RAIL_ID, RAILWAY_NOMINAL);
#endif
}


/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static void railway_set_pre_callback(railway_rail_state_t *rail_state, railway_cb_type type, railway_cb cb, void* cookie)
{
    const railway_internal_cb_type cb_type = railway_callback_remap[type];

    //Deal with special callbacks first.
    if(type!=RAILWAY_PRECHANGE_CB)
    {
        //If we ever extend this, implement another lookup to map the enum railway_cb_type to the array index.
        //For now, support the last entry only.
        CORE_VERIFY(type==RAILWAY_LAST_PRECHANGE_CB);
        
        CORE_VERIFY(rail_state->cbs[cb_type][RAILWAY_MAX_CBS-1].callback_fn==NULL);
        rail_state->cbs[cb_type][RAILWAY_MAX_CBS-1].callback_fn = cb;
        rail_state->cbs[cb_type][RAILWAY_MAX_CBS-1].callback_cookie = cookie;
        return;
    }
    
    // If this check fails then we need to increase RAILWAY_MAX_CBS by 1.
    // The last CB is reserved for RAILWAY_LAST_PRECHANGE_CB, so check the "RAILWAY_MAX_CBS-2"-th entry
    CORE_VERIFY(!rail_state->cbs[cb_type][RAILWAY_MAX_CBS-2].callback_fn);
    
    // The last CB is reserved for RAILWAY_LAST_PRECHANGE_CB, so search up to the "RAILWAY_MAX_CBS-1"-th entry
    for(int i = 0; i < RAILWAY_MAX_CBS-2; ++i)
    {
        if(!rail_state->cbs[cb_type][i].callback_fn)
        {
            rail_state->cbs[cb_type][i].callback_fn = cb;
            rail_state->cbs[cb_type][i].callback_cookie = cookie;
            break;
        }
    }
}

/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
static void railway_set_post_callback(railway_rail_state_t *rail_state, railway_cb_type type, railway_cb cb, void* cookie)
{
    const railway_internal_cb_type cb_type = railway_callback_remap[type];

    //Deal with special callbacks first.
    if(type!=RAILWAY_POSTCHANGE_CB)
    {
        //If we ever extend this, implement another lookup to map the enum railway_cb_type to the array index.
        //For now, support the 0th entry only.
        CORE_VERIFY(type==RAILWAY_FIRST_POSTCHANGE_CB);
        
        CORE_VERIFY(rail_state->cbs[cb_type][0].callback_fn==NULL);
        rail_state->cbs[cb_type][0].callback_fn = cb;
        rail_state->cbs[cb_type][0].callback_cookie = cookie;
        return;
    }

    // If this check fails then we need to increase RAILWAY_MAX_CBS by 1.
    CORE_VERIFY(!rail_state->cbs[cb_type][RAILWAY_MAX_CBS-1].callback_fn);
    
    //Skip the first entry as that's for RAILWAY_FIRST_POSTCHANGE_CB
    for(int i = 1; i < RAILWAY_MAX_CBS; ++i)
    {
        if(!rail_state->cbs[cb_type][i].callback_fn)
        {
            rail_state->cbs[cb_type][i].callback_fn = cb;
            rail_state->cbs[cb_type][i].callback_cookie = cookie;
            break;
        }
    }
}

/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
void railway_set_callback(int rail, railway_cb_type type, railway_cb cb, void* cookie)
{
    CORE_VERIFY(type<RAILWAY_NUM_CB_TYPES);
    CORE_VERIFY(cb);
    CORE_VERIFY(rail<RAILWAY_CONFIG_DATA->num_rails);

    railway_rail_state_t *rail_state = &railway.rail_state[rail];
    const railway_internal_cb_type cb_type = railway_callback_remap[type];
    
    if(cb_type==RAILWAY_INTERNAL_PRECHANGE_CB)
    {
        railway_set_pre_callback(rail_state, type, cb, cookie);
    }
    else if(cb_type==RAILWAY_INTERNAL_POSTCHANGE_CB)
    {
        railway_set_post_callback(rail_state, type, cb, cookie);
    }
    else
    {
        CORE_VERIFY(0);
    }
}

/*===========================================================================
FUNCTION: 

DESCRIPTION:

RETURN VALUE:
===========================================================================*/
uint32 railway_get_sleep_voltage(int rail)
{
  railway_settings settings;

  CORE_VERIFY(rail < RAILWAY_CONFIG_DATA->num_rails);

  railway_aggregated_voltage_target_uv(rail, &settings, true);

  return settings.microvolts;
}



