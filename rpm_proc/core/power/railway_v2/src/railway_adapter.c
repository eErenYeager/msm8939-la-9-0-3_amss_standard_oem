/*===========================================================================
  railway_adapter.c

  SERVICES:

  DESCRIPTION:

  INITIALIZATION AND SEQUENCING REQUIREMENTS:
  Description...

  Copyright (c) 2013 Qualcomm Technologies, Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary.  Export of this technology or software is regulated
  by the U.S. Government. Diversion contrary to U.S. law prohibited.
  ===========================================================================*/


//===========================================================================
//                     Includes and Variable Definitions
//===========================================================================

//---------------------------------------------------------------------------
// Include Files
//---------------------------------------------------------------------------
#include <stdlib.h>

#include "rpmserver.h"
#include "../common/vect.h"

#include "railway.h"

#include "railway_internal.h"
#include "railway_adapter.h"
#include "railway_config.h"
#include "CoreVerify.h"
#include "pmapp_npa.h"

//---------------------------------------------------------------------------
// Constant / Define Declarations
//--------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Type Declarations
//---------------------------------------------------------------------------
typedef struct
{
  uint32 sw_enable;
  uint32 microvolts;
  railway_corner corner;
  pm_npa_sw_mode_smps_type sw_mode;
  railway_corner active_floor;
} railway_voter_irep;

typedef struct
{
  int rail_num;
  vect_t  *voter_handles;
  vect_t  *sw_mode_voter_handles;
} cb_data_t;


//---------------------------------------------------------------------------
// Global Constant Definitions
//---------------------------------------------------------------------------
bool ghost_vote;

//---------------------------------------------------------------------------
// Local Object Definitions
//---------------------------------------------------------------------------
ResourceData *railway_mx = NULL;

//---------------------------------------------------------------------------
// Static Variable Definitions
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Forward Declarations
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// External References
//---------------------------------------------------------------------------


//===========================================================================
//                             Macro Definitions
//===========================================================================

//===========================================================================
//                           Function Definitions
//===========================================================================

/*===========================================================================
  FUNCTION: set_ghost_vote

  DESCRIPTION:

  RETURN VALUE:
  ===========================================================================*/
void set_ghost_vote(bool do_ghost_vote)
{
  ghost_vote=do_ghost_vote;
}

//This translation is required because masters vote for levels 0-6, but we support more levels internally in the RPM.
//Masters don't vote for level RAILWAY_TURBO_HIGH nor RAILWAY_SUPER_TURBO_NO_CPR
const static railway_corner railway_master_corner_remap[] =
{
  RAILWAY_NO_REQUEST,
  RAILWAY_RETENTION,
  RAILWAY_SVS_KRAIT,
  RAILWAY_SVS_SOC,
  RAILWAY_NOMINAL,
  RAILWAY_TURBO,
  RAILWAY_SUPER_TURBO,
};

#define RAILWAY_CORNER_REMAP_TABLE_SIZE (sizeof(railway_master_corner_remap)/sizeof(railway_corner))

/*===========================================================================
  FUNCTION: railway_xlate

  DESCRIPTION:

  RETURN VALUE:
  ===========================================================================*/
void railway_xlate(rpm_translation_info *info)
{
  uint32               type, length;
  uint32               *value;
  railway_voter_irep *req = info->dest_buffer;

  /* Read until we run out of KVPs. */
  while( !kvp_eof(info->new_kvps) )
  {
    kvp_get( info->new_kvps, &type, &length, (const char **)&value);

    if(length==0)
    {   /* Implies we need to invalidate any previous request the client 
           might have on this kvp type. */
      switch(type)
      {
        case PM_NPA_KEY_SOFTWARE_ENABLE:
          req->sw_enable = 0;
          break;
        case PM_NPA_KEY_CORNER_LEVEL_KEY:
        case RAILWAY_INTERNAL_KEY_CORNER_LEVEL_KEY:
          req->corner = RAILWAY_NO_REQUEST;
          break;
        case PM_NPA_KEY_MICRO_VOLT:
          req->microvolts = 0;
          break;
        case PM_NPA_KEY_SMPS_SOFTWARE_MODE:
           req->sw_mode = PM_NPA_SW_MODE_SMPS__AUTO;
           break;
        case PM_NPA_KEY_ACTIVE_FLOOR:
          req->active_floor = RAILWAY_NO_REQUEST;
          break;
        default:
          break;
      }
    }
    else
    {
      switch(type)
      {
        case PM_NPA_KEY_SOFTWARE_ENABLE:
          CORE_VERIFY(sizeof(npa_resource_state) == length);
          req->sw_enable = *value;
          break;
        case PM_NPA_KEY_CORNER_LEVEL_KEY:
          CORE_VERIFY(sizeof(npa_resource_state) == length);
          CORE_VERIFY(*value<RAILWAY_CORNER_REMAP_TABLE_SIZE);
		  CORE_VERIFY((railway_corner)*value < RAILWAY_SUPER_TURBO_NO_CPR);
          req->corner = railway_master_corner_remap[*value];
          break;
        case PM_NPA_KEY_MICRO_VOLT:
          CORE_VERIFY(sizeof(npa_resource_state) == length);
          req->microvolts = *value;
          break;
        case PM_NPA_KEY_SMPS_SOFTWARE_MODE:
          CORE_VERIFY(sizeof(npa_resource_state) == length);
          req->sw_mode = (pm_npa_sw_mode_smps_type)*value;
          break;
        case PM_NPA_KEY_ACTIVE_FLOOR:
          CORE_VERIFY(sizeof(npa_resource_state) == length);
          CORE_VERIFY(*value<RAILWAY_CORNER_REMAP_TABLE_SIZE);
		  CORE_VERIFY((railway_corner)*value < RAILWAY_SUPER_TURBO_NO_CPR);
          req->active_floor = railway_master_corner_remap[*value];
          break;
        case RAILWAY_INTERNAL_KEY_CORNER_LEVEL_KEY:
          CORE_VERIFY(sizeof(npa_resource_state) == length);
          req->corner = (railway_corner)*value;
          break;
        default:
          break;
      }
    }
  }
}

/*===========================================================================
  FUNCTION: railway_apply

  DESCRIPTION:

  RETURN VALUE:
  ===========================================================================*/
void railway_apply(rpm_application_info *info)
{
  cb_data_t   *rail_info = (cb_data_t *)info->cb_data;
  railway_voter_irep* new_irep;
  railway_voter_irep  invalidated_irep;

  //info->new_state can be NULL in the case that a request has been invalidated.
  //In that case, use a zeroed-out irep.
  if(info->new_state)
  {
    new_irep = (railway_voter_irep*)info->new_state;
  }
  else
  {
    memset(&invalidated_irep, 0, sizeof(railway_voter_irep));
    new_irep = &invalidated_irep;
  }

  uint32 voter = info->client;
  int rail_id = rail_info->rail_num;
  CORE_VERIFY(rail_id!=-1);

  // Ensure we have enough room to store a voter handle for this voter.
  if(voter >= vect_size(rail_info->voter_handles))
    vect_resize(rail_info->voter_handles, voter+1);

  // Ensure we have enough room to store a sw mode voter handle for this voter.
  if(voter >= vect_size(rail_info->sw_mode_voter_handles))
    vect_resize(rail_info->sw_mode_voter_handles, voter+1);

  // Ensure we have an internal voter handle.
  railway_voter_t h = (railway_voter_t)vect_at(rail_info->voter_handles, voter);
  if(!h)
  {
    bool is_external = info->client < rpm_get_num_ees();
    h = railway_create_voter(rail_id, is_external ? false : true, voter);
    vect_set(rail_info->voter_handles, voter, h);
  }

  railway_corner_vote(h, new_irep->corner);
  railway_uv_vote(h, new_irep->microvolts);
  railway_sw_enable_vote(h, new_irep->sw_enable);
  railway_active_floor_vote(h, new_irep->active_floor);
  
  if(RAILWAY_CONFIG_DATA->rails[rail_id].supports_sw_mode)
  {
    railway_sw_mode_voter_t h2 = (railway_sw_mode_voter_t)vect_at(rail_info->sw_mode_voter_handles, voter);
    //Only need to make a sw_mode voter if there's a non-zero auto mode vote once.
    if(new_irep->sw_mode!=0 && !h2)
    {
      h2 = railway_create_sw_mode_voter(rail_id, voter);
      vect_set(rail_info->sw_mode_voter_handles, voter, h2);
    }
    if(h2)
    {
      railway_sw_mode_vote(h2, new_irep->sw_mode);
    }
  }

  if(!ghost_vote)
  {
    railway_transition_rail(rail_id);
  }

}

/*===========================================================================
  FUNCTION: railway_adapter_init

  DESCRIPTION:

  RETURN VALUE:
  ===========================================================================*/
void railway_adapter_init(void)
{
  for(int i = 0; i < RAILWAY_CONFIG_DATA->num_rails; i++)
  {
    const railway_rail_config_t *rail = &RAILWAY_CONFIG_DATA->rails[i];

    cb_data_t *cb_data = malloc(sizeof(cb_data_t));
    CORE_VERIFY_PTR(cb_data);
    cb_data->rail_num = i;
    cb_data->voter_handles = vect_create();
    CORE_VERIFY(cb_data->voter_handles);

    cb_data->sw_mode_voter_handles = vect_create();
    CORE_VERIFY(cb_data->sw_mode_voter_handles);

    ResourceData *r = rpm_override_resource(rail->vreg_type, rail->vreg_num, sizeof(railway_voter_irep),
                                            railway_xlate, railway_apply, cb_data);
    if (i == 0)
    {
      railway_mx = r;
    }
  }
}
