/*
===========================================================================

FILE:         railway_config.c

DESCRIPTION:
  Per target railway configurations

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.0.c9/core/power/railway_v2/src/8936/railway_config.c#4 $
$Date: 2015/12/22 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2012 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include "CoreVerify.h"
#include "kvp.h"
#include "msmhwio.h"
#include "rpmserver.h"
#include "railway_config.h"
#include "pm_version.h"

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */
#define SECURITY_CONTROL_BASE 0x60058000
#define SECURITY_CONTROL_CORE_REG_BASE (SECURITY_CONTROL_BASE + 0x00000000)
#define HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR (SECURITY_CONTROL_CORE_REG_BASE + 0x000040ec)

#define CPR_EFUSE_IN(addr, mask, shift) \
         ( (addr) ? \
         ( ( (*((uint32*)addr)) & mask ) >> shift ) : 0 )

uint32 mss_open_nom_ceil = 1200000;
uint32 mx_init_nom_voltage;

uint32 mss_rbcpr_rail_efuse_multipler = 10000;
uint32 mss_rbcpr_rail_pmic_step_size = 12500;




//
// BEGIN config data; should migrate to the system enumeration data method
//
static const railway_config_data_t temp_config_data =
{
    .rails     = (railway_rail_config_t[])
    {
        // Must init VDDMX first, as voting on the other rails will cause Mx changes to occur.
        {
            .vreg_name      = "vddmx",

            .vreg_type      = RPM_LDO_A_REQ,
            .vreg_num       = 3,

            .pm_rail_id     = PM_RAILWAY_MX,
            .pmic_step_size = 12500,    // not used

            .initial_corner = RAILWAY_SUPER_TURBO,

            .supports_explicit_voltage_requests = true,
            .default_uvs = (const unsigned[])
            {
                0,                      // RAILWAY_NO_REQUEST
                750000,                 // RAILWAY_RETENTION
                1050000,                // RAILWAY_SVS_KRAIT
                1050000,                // RAILWAY_SVS_SOC
                1200000,                // RAILWAY_NOMINAL
                1225000,                // RAILWAY_TURBO
                1287500,                // RAILWAY_TURBO_HIGH
                1287500,                // RAILWAY_SUPER_TURBO
                1287500,                // RAILWAY_SUPER_TURBO_NO_CPR
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_RETENTION,
                RAILWAY_SVS_SOC,
                RAILWAY_NOMINAL,
				RAILWAY_TURBO,
                RAILWAY_SUPER_TURBO,
            },
            .supported_corners_count = 5,

            .sleep_mask = 0x4,          // RPM_VDD_MEM_NEEDED
        },

        // VDDCX
        {
            .vreg_name      = "vddcx",

            .vreg_type      = RPM_SMPS_A_REQ,
            .vreg_num       = 2,

            .pm_rail_id     = PM_RAILWAY_CX,
            .pmic_step_size = 12500,     // not used

            .initial_corner = RAILWAY_SUPER_TURBO,

            .supports_explicit_voltage_requests = true,

            .default_uvs = (const unsigned[])
            {
                0,                      // RAILWAY_NO_REQUEST
                700000,                 // RAILWAY_RETENTION
                1050000,                // RAILWAY_SVS_KRAIT
                1050000,                // RAILWAY_SVS_SOC
                1150000,                // RAILWAY_NOMINAL
                1225000,                // RAILWAY_TURBO
                1287500,                // RAILWAY_TURBO_HIGH
                1287500,                // RAILWAY_SUPER_TURBO
                1287500,                // RAILWAY_SUPER_TURBO_NO_CPR
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_RETENTION,
                RAILWAY_SVS_SOC,
                RAILWAY_NOMINAL,
				RAILWAY_TURBO,
                RAILWAY_SUPER_TURBO,
            },
            .supported_corners_count = 5,

            .sleep_mask = 0x2,          // RPM_VDD_DIG_NEEDED
        },
    },

    .num_rails = 2,
};
//
// END config data
//

const railway_config_data_t * const RAILWAY_CONFIG_DATA = &temp_config_data;

/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */

// TODO: This can be avoided if railway_init() is after pm_init()
// TODO: Make rail_state.corner_uvs a copy of rail_config.default_uvs,
//       instead of a pointer to rail_config.default_uvs
void railway_init_early_proxy_votes(void)
{
    int i;

    for (i = 0; i < RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        railway_rail_config_t *rail_cfg   = &(RAILWAY_CONFIG_DATA->rails[i]);
        railway_rail_state_t  *rail_state = &(railway.rail_state[i]);

        (void)pm_railway_set_voltage(rail_cfg->pm_rail_id,
                                     rail_cfg->default_uvs[rail_cfg->initial_corner]);

        rail_state->current_active.mode       = rail_cfg->initial_corner;
        rail_state->current_active.microvolts = rail_cfg->default_uvs[rail_cfg->initial_corner];
    }
}

void railway_init_proxies_and_pins(void)
{
}

void railway_target_init(void)
{
}
void railway_voltage_fuse_based_update(void)
{
}
void railway_override_rail_voltage(int rail,railway_corner corner)
{

  // Read MSS open loop fuse value for NOM corner and compute final corner voltage
  if (rail == MX_RAIL_ID && corner == RAILWAY_NOMINAL)
  {
    int targetVsteps0 = CPR_EFUSE_IN(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0x70000, 16);
	int targetVsteps1 = CPR_EFUSE_IN(HWIO_QFPROM_CORR_CALIB_ROW3_MSB_ADDR, 0x300000, 20);
	int  initVoltOffsetSteps =  ((targetVsteps1 << 3) | targetVsteps0);
	 
	//init MX-NOM corner 
    mx_init_nom_voltage = 1150000;
  
	if(initVoltOffsetSteps & 0x10)
    {
        initVoltOffsetSteps =  -(initVoltOffsetSteps & 0xF);
    }
	 const int voltage_offset = initVoltOffsetSteps * mss_rbcpr_rail_efuse_multipler;
	 uint32 target_voltage = voltage_offset + mss_open_nom_ceil;
	 
	//Now round up to the PMIC step size.
    if(target_voltage% mss_rbcpr_rail_pmic_step_size)
    {
       target_voltage += (mss_rbcpr_rail_pmic_step_size - (target_voltage % mss_rbcpr_rail_pmic_step_size));
    }
     
	// Check NOM ceiling
	if (target_voltage < mx_init_nom_voltage)
	{
	   target_voltage = mx_init_nom_voltage;
	}
	mss_open_nom_ceil = target_voltage;
	 
  }
}
