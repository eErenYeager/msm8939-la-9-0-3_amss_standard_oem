/*===========================================================================

  railway_internal.h - bits of state for railway, in a convenient global
                       wrapper for debug

  Copyright (c) 2012 Qualcomm Technologies Incorporated.
  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ===========================================================================*/

#ifndef RAILWAY_INTERNAL_H
#define RAILWAY_INTERNAL_H

#include "railway.h"
#include "CoreVerify.h"
#include "npa_resource.h"

#define RAILWAY_MAX_CBS 6

#define MX_RAIL_ID 0
#define CX_RAIL_ID 1

#define RAILWAY_INTERNAL_KEY_CORNER_LEVEL_KEY 0x6c637772  //rwcl

typedef enum
{
    RAILWAY_INTERNAL_PRECHANGE_CB = 0,
    RAILWAY_INTERNAL_POSTCHANGE_CB,
    RAILWAY_INTERNAL_NUM_CB_TYPES,
} railway_internal_cb_type;

typedef struct
{
    railway_cb      callback_fn;
    void*           callback_cookie;
} railway_cb_data;

typedef void (*railway_aggregation_workaround_fn)(int rail_id);

typedef struct
{
    railway_settings    current_active;

    railway_settings    unconstrained_target;  //Should always be updated when the votes change, but isn't currently
    railway_settings    constrained_target;    //Re-calculated each time we transition rails.

    railway_sw_mode_voter_t sm_mode_voter;
    // Storage for callbacks.
    railway_cb_data         cbs[RAILWAY_INTERNAL_NUM_CB_TYPES][RAILWAY_MAX_CBS];

    npa_client_handle       sleep_handle;
    railway_voter_t         voter_list_head;
    uint32        corner_uvs[RAILWAY_CORNERS_COUNT];
    
    voltage_settling_cb_type voltage_settling_cb;
    void* voltage_settling_cb_cookie;
} railway_rail_state_t;

typedef struct
{
    railway_rail_state_t *rail_state;
} railway_internals_t;

//Returns the target voltage and corner for the named rail.
//rail is not currently used - all votes are for VddCx.
//If for_sleep is true then this will ignore any suppressible votes - this option would
//only be used for sleep to know if vdd_min is possible.
void railway_aggregated_voltage_target_uv(int rail, railway_settings* target_setting, bool for_sleep);

extern railway_internals_t railway;

void railway_transitioner_init(void);

//Any early target-specific init should be done in this function.
void railway_target_init(void);

void railway_voltage_fuse_based_update(void);


void railway_mx_voltage_update(void);

void railway_voltage_fuse_based_update(void);


#endif // RAILWAY_INTERNAL_H

