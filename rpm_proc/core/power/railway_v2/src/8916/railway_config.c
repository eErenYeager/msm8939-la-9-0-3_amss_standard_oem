/*
===========================================================================

FILE:         railway_config.c

DESCRIPTION:
  Per target railway configurations

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.0.c9/core/power/railway_v2/src/8916/railway_config.c#5 $
$Date: 2016/06/15 $

when       who     what, where, why
--------   ---     --------------------------------------------------------

===========================================================================
             Copyright (c) 2012 QUALCOMM Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include "CoreVerify.h"
#include "kvp.h"
#include "msmhwio.h"
#include "rpmserver.h"
#include "railway_config.h"
#include "pm_version.h"
#include "msmhwiobase.h"
#include "HAL_rbcpr_qfprom.h"
#include "rbcpr.h"

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

//
// BEGIN config data; should migrate to the system enumeration data method
//
static const railway_config_data_t temp_config_data =
{
    .rails     = (railway_rail_config_t[])
    {
        // Must init VDDMX first, as voting on the other rails will cause Mx changes to occur.
        {
            .vreg_name      = "vddmx",

            .vreg_type      = RPM_LDO_A_REQ,
            .vreg_num       = 3,

            .pm_rail_id     = PM_RAILWAY_MX,
            .pmic_step_size = 12500,    // not used

            .initial_corner = RAILWAY_NOMINAL,

            .supports_explicit_voltage_requests = true,
            .default_uvs = (const unsigned[])
            {
                0,                      // RAILWAY_NO_REQUEST
                750000,                 // RAILWAY_RETENTION
                1050000,                // RAILWAY_SVS_KRAIT
                1050000,                // RAILWAY_SVS_SOC
                1150000,                // RAILWAY_NOMINAL
                1287500,                // RAILWAY_TURBO
                1287500,                // RAILWAY_TURBO_HIGH
                1287500,                // RAILWAY_SUPER_TURBO
                1287500,                // RAILWAY_SUPER_TURBO_NO_CPR
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_RETENTION,
                RAILWAY_SVS_SOC,
                RAILWAY_NOMINAL,
                RAILWAY_SUPER_TURBO,
            },
            .supported_corners_count = 4,

            .sleep_mask = 0x4,          // RPM_VDD_MEM_NEEDED
        },

        // VDDCX
        {
            .vreg_name      = "vddcx",

            .vreg_type      = RPM_SMPS_A_REQ,
            .vreg_num       = 1,

            .pm_rail_id     = PM_RAILWAY_CX,
            .pmic_step_size = 12500,     // not used

            .initial_corner = RAILWAY_NOMINAL,

            .supports_explicit_voltage_requests = true,

            .default_uvs = (const unsigned[])
            {
                0,                      // RAILWAY_NO_REQUEST
                700000,                 // RAILWAY_RETENTION
                1050000,                // RAILWAY_SVS_KRAIT
                1050000,                // RAILWAY_SVS_SOC
                1150000,                // RAILWAY_NOMINAL
                1287500,                // RAILWAY_TURBO
                1287500,                // RAILWAY_TURBO_HIGH
                1287500,                // RAILWAY_SUPER_TURBO
                1287500,                // RAILWAY_SUPER_TURBO_NO_CPR
            },

            .supported_corners = (railway_corner[])
            {
                RAILWAY_RETENTION,
                RAILWAY_SVS_SOC,
                RAILWAY_NOMINAL,
                RAILWAY_SUPER_TURBO,
            },
            .supported_corners_count = 4,

            .sleep_mask = 0x2,          // RPM_VDD_DIG_NEEDED
        },
    },

    .num_rails = 2,
};
//
// END config data
//

const railway_config_data_t * const RAILWAY_CONFIG_DATA = &temp_config_data;

/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */

// TODO: This can be avoided if railway_init() is after pm_init()
// TODO: Make rail_state.corner_uvs a copy of rail_config.default_uvs,
//       instead of a pointer to rail_config.default_uvs
void railway_init_early_proxy_votes(void)
{
    int i;

    for (i = 0; i < RAILWAY_CONFIG_DATA->num_rails; i++)
    {
        railway_rail_config_t *rail_cfg   = &(RAILWAY_CONFIG_DATA->rails[i]);
        railway_rail_state_t  *rail_state = &(railway.rail_state[i]);

        (void)pm_railway_set_voltage(rail_cfg->pm_rail_id,
                                     rail_cfg->default_uvs[rail_cfg->initial_corner]);

        rail_state->current_active.mode       = rail_cfg->initial_corner;
        rail_state->current_active.microvolts = rail_cfg->default_uvs[rail_cfg->initial_corner];
    }
}

void railway_init_proxies_and_pins(void)
{
}

void railway_target_init(void)
{

}

void railway_voltage_fuse_based_update(void)
{
    uint32 speed_bin = HWIO_INF(QFPROM_RAW_PTE2, SPEED_BIN); 
    uint32 wafer_bin = HWIO_INF(QFPROM_RAW_PTE1, WAFER_BIN); 
	
	/* MX NOM&SVS voltage bump by 50mv for WAFER VLV parts */ 	
    if ((speed_bin == 0x03) && (wafer_bin ==4))
    {
	railway_mx_voltage_update();
    }
}
void railway_mx_voltage_update(void)
{
    railway_rail_config_t *mx_rail_cfg   = &(RAILWAY_CONFIG_DATA->rails[0]);
    
    //update MX NOM voltage for speed bin=3 parts
    railway_set_corner_voltage(MX_RAIL_ID, RAILWAY_NOMINAL, (mx_rail_cfg->default_uvs[RAILWAY_NOMINAL] +50000));

    //update MX SVS voltage for speed bin=3 parts
    railway_set_corner_voltage(MX_RAIL_ID, RAILWAY_SVS_KRAIT, (mx_rail_cfg->default_uvs[RAILWAY_SVS_KRAIT] +50000));
    railway_set_corner_voltage(MX_RAIL_ID, RAILWAY_SVS_SOC, (mx_rail_cfg->default_uvs[RAILWAY_SVS_SOC] +50000));

}
