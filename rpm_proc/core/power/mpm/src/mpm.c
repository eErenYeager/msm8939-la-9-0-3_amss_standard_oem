/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=

           M U L T I P R O C E S S O R   P O W E R   M A N A G E R

GENERAL DESCRIPTION

  This module contains routines for the multiprocessor power manager (MPM).

EXTERNALIZED FUNCTIONS


INITIALIZATION AND SEQUENCING REQUIREMENTS

  mpm_init( ) must be called once, before any other mpm_ function.



             Copyright (c) 2005-2009 by Qualcomm Technologies, Inc.  
                               All Rights Reserved.

=*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*=


==============================================================================

                           EDIT HISTORY FOR MODULE

$Header: //components/rel/rpm.bf/2.0.c9/core/power/mpm/src/mpm.c#1 $

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

when         who   what, where, why
--------   ---   -----------------------------------------------------------
4/5/12     mda    Initial MPMv2 

============================================================================*/



/*============================================================================

                           INCLUDE FILES FOR MODULE

============================================================================*/

#include "mpm.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "HALmpm.h"

/*============================================================================

                   DEFINITIONS AND DECLARATIONS FOR MODULE

This section contains local definitions for constants, macros, typedefs,
and other items needed by this module.

============================================================================*/

/* Active and sleep voltages to be programmed into the MPM HW. */
static uint32_t mpm_active_voltages_uv[MPM_VOLTAGE_RAIL_COUNT];
static uint32_t mpm_sleep_voltages_uv[MPM_VOLTAGE_RAIL_COUNT];

/* Status of the XO's */
static bool mpm_xo_enabled[MPM_XO_COUNT];

/* Resources we've set up for sleep. */
static uint32_t mpm_xo_sleep_enabled;
static uint32_t mpm_rail_sleep_enabled;


/*=============================================================================

FUNCTION MPM_CONFIG_RAIL_FOR_SLEEP

DESCRIPTION
  This function is used to program the MPM with the settings required to put
  a voltage rail to sleep.
  
  In order to save overhead in situations where the sleep wouldn't save power,
  this function refrains from programming the MPM hardware if the sleep voltage
  of the rail is not less than the active voltage.

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
static void mpm_config_rail_for_sleep
(
  uint32_t voltage_rail
)
{
  uint32_t active_uv, sleep_uv;

  if(voltage_rail >= MPM_VOLTAGE_RAIL_COUNT)
    return;

  active_uv = mpm_active_voltages_uv[voltage_rail];
  sleep_uv  = mpm_sleep_voltages_uv[voltage_rail];

  if((mpm_rail_sleep_enabled & (1 << voltage_rail)) && (sleep_uv <= active_uv))
  {
    /* Program the HW with the rail's information. */
    HAL_mpm_CfgVoltageRailSleep((HAL_mpm_VoltageRailType)voltage_rail,
                                sleep_uv, active_uv);
  }
} /* mpm_config_rail_for_sleep */


/*============================================================================

FUNCTION MPM_CONFIG_XO_FOR_SLEEP

DESCRIPTION
  Configures the MPM hardware to shut down an XO at sleep time (if that XO
  isn't already disabled).

DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
static void mpm_config_cxo_for_sleep(void)
{
  if (mpm_xo_enabled[MPM_XO_CXO] &&
      (mpm_xo_sleep_enabled & (1 << MPM_XO_CXO)))
  {
    HAL_mpm_CfgCXOSleep((HAL_mpm_XOType)MPM_XO_CXO);
  }
} /* mpm_config_cxo_for_sleep */


/*============================================================================

FUNCTION MPM_INIT

DESCRIPTION
  This function initializes the MPM registers to known values.

DEPENDENCIES
  Must be called with the I bit set to prevent timing issues.

RETURN VALUE
  None

SIDE EFFECTS
  None

============================================================================*/
void mpm_init (void)
{
  char **ppszVersion    = NULL;

  HAL_mpm_Init( ppszVersion );

  mpm_xo_enable(MPM_XO_CXO);

} /* mpm_init */
void mpm_config_freeze_ios(bool enable)
{
  HAL_mpm_CfgFreezeIOs(enable);
}

void mpm_init_xtal(void)
{
    HAL_mpm_Init_Xtal_Latency();
}

void mpm_config_ebi1_freeze_io(bool enable)
{
  HAL_mpm_CfgEbi1SwCtl(enable);
}

void mpm_setup_cxo_sleep(void)
{
  uint32_t needy_rails, i;

  /* First configure the XO for sleep. */
  mpm_xo_sleep_enabled |= (1 << MPM_XO_CXO);
  mpm_config_cxo_for_sleep();

  /* Next, we have to reconfigure any rails that are set up for sleep since
   * configuring an XO can accidentally overwrite some required HW settings
   * that the rails may need.  Hopefully future hardware can fix that. */
  needy_rails = mpm_rail_sleep_enabled;
  for (i = 0; i < MPM_VOLTAGE_RAIL_COUNT && needy_rails; i++)
  {
    if(needy_rails & (1 << i))
    {
      needy_rails ^= (1 << i);
      mpm_config_rail_for_sleep(i);
    }
  }
}

void mpm_setup_rail_sleep(mpm_voltage_rail_type rail)
{
  if(rail >= MPM_VOLTAGE_RAIL_COUNT)
    return;

  /* First configure the XO for sleep. */
  mpm_rail_sleep_enabled |= (1 << rail);
  mpm_config_rail_for_sleep(rail);

  /* Vdd Mem always needs to be configured after Vdd Dig, so if this was an out
   * of order request, fix that by reprogramming Vdd Mem. */
  if((rail == MPM_VOLTAGE_RAIL_VDD_DIG) &&
     ((1 << MPM_VOLTAGE_RAIL_VDD_MEM) & mpm_rail_sleep_enabled))
  {
    mpm_config_rail_for_sleep(MPM_VOLTAGE_RAIL_VDD_MEM);
  }
}

void mpm_setup_sleep_timer(uint64_t duration_sclk)
{
  HAL_mpm_SetWakeUpTime(duration_sclk);
}

void mpm_teardown_sleep (void)
{
  if(!mpm_xo_sleep_enabled && !mpm_rail_sleep_enabled)
    return; /* we're already torn down, nothing to do */

  /* Clear out the hardware. */
  HAL_mpm_ClearSleepModes();

  /* Record that the hardware is currently clear. */
  mpm_xo_sleep_enabled = 0;
  mpm_rail_sleep_enabled = 0;
}

/*=============================================================================

FUNCTION MPM_SET_ACTIVE_VOLTAGE

DESCRIPTION
  This function is used to tell the MPM driver what the active voltage of a
  given rail should be set to when coming out of sleep.

PARAMETERS
  voltage_rail    The voltage rail whose active value is being configured.
  active_val_uv   The value that the active value should be set to when coming
                  out of XO shutdown, in uV.  For example, if the active
                  voltage is 1.25 V, then 1250000 should be passed into this
                  function.
                      
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
void mpm_set_active_voltage
(
  mpm_voltage_rail_type voltage_rail,
  uint32_t active_val_uv
)
{
  if(voltage_rail >= MPM_VOLTAGE_RAIL_COUNT)
    return;

  /* Record this voltage, it will be used when setting the active voltage
   * before TCXO shutdown with VDD minimization */
  mpm_active_voltages_uv[voltage_rail] = active_val_uv;
}


/*=============================================================================

FUNCTION MPM_SET_SLEEP_VOLTAGE

DESCRIPTION
  This function is used to tell the MPM driver what the voltage of a given rail
  should be set to when entering sleep.

PARAMETERS
  voltage_rail    The voltage rail whose sleep value is being configured.
  active_val_uv   The value that the rail should be set to when sleep for that
                  rail is enabled at the time sleep occurs, in mV.  For
                  example, if the voltage is 0.5 V, then 500 should be passed
                  into this function.
                      
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
void mpm_set_sleep_voltage
(
  mpm_voltage_rail_type voltage_rail,
  uint32_t sleep_val_uv
)
{
  if(voltage_rail >= MPM_VOLTAGE_RAIL_COUNT)
    return;

  /* Record this voltage, it will be used when setting the active voltage
   * before TCXO shutdown with VDD minimization */
  mpm_sleep_voltages_uv[voltage_rail] = sleep_val_uv;
}


/*=============================================================================

FUNCTION MPM_XO_ENABLE

DESCRIPTION
  This function is used to tell the MPM driver to turn on an XO immediately.

PARAMETERS
  xo    The XO that should be immediately powered on.
                      
DEPENDENCIES
  None

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
void mpm_xo_enable
(
  mpm_xo_type xo
)
{
  if(xo >= MPM_XO_COUNT)
    return;

  if(!mpm_xo_enabled[xo])
  {
    HAL_mpm_EnableCXO();
    mpm_xo_enabled[xo] = TRUE;
  }
}


/*=============================================================================

FUNCTION MPM_XO_DISABLE

DESCRIPTION
  This function is used to tell the MPM driver to turn off an XO immediately.

PARAMETERS
  xo    The XO that should be immediately powered off.
                      
DEPENDENCIES
  The XO being powered off should not be in use by any hardware.  Particularly,
  to turn off the XO used by this processor, mpm_setup_sleep must be used.

RETURN VALUE
  None

SIDE EFFECTS
  None

=============================================================================*/
void mpm_xo_disable
(
  mpm_xo_type xo
)
{
  if(xo >= MPM_XO_COUNT)
    return;

  if(mpm_xo_enabled[xo])
  {
    HAL_mpm_DisableCXO();
    mpm_xo_enabled[xo] = FALSE;
  }
}

/*=============================================================================

FUNCTION MPM_GET_TIMETICK

DESCRIPTION
  Read the MPM TIMETICK counter, and return the current count. NOTE:
  the counter tracks time in slow clock counts, from 0 to 0xFFFFFFFF 
  (~1.5 days)

DEPENDENCIES
  Must be called from an INTLOCK'd context.

RETURN VALUE
  Slow clock timetick counter value

SIDE EFFECTS
  None

=============================================================================*/
uint64_t mpm_get_timetick( void )
{
  /* Sleep Xtal Time Tick count */
  uint64_t                   tick;

  HAL_mpm_GetCurrentSclkCount( &tick );

  return tick;

} /* mpm_get_timetick */

uint64_t mpm_vdd_min_enter_latency( void )
{
  return (uint64_t)HAL_mpm_GetVddMinEnterLatency();
}

uint64_t mpm_vdd_min_exit_latency( void )
{
  return (uint64_t)HAL_mpm_GetVddMinExitLatency();
}


void mpm_sw_done( mpm_sleep_mode_type sleep_mode, uint64_t sleep_duration )
{
  HAL_mpm_SWDone((HAL_mpm_SleepModeType)sleep_mode, sleep_duration);
}

