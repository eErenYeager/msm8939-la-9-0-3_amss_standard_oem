/*
===========================================================================

FILE:         HALmpmintPlatform.c

DESCRIPTION:  
  This is the platform hardware abstraction layer implementation for the
  MPM interrupt controller block.

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.0.c9/core/power/mpm/hal/source/MPM_V4/HALmpmPlatform.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
08/06/09   ajf     Created new platform based on MPM_V2 platform.

===========================================================================
           Copyright (c) 2008-2012 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
               QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include "BSPmpm.h"
#include "cortex-m3.h"
#include "../common/HALmpmInternal.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "HALmpm.h"
#include "HALhwio.h"
#include "mpm2_hwio.h"
#include "qtimer_hwio.h"
#include "rpm_hwio.h"
#include "swevent.h"
#include "tcsr_hwio.h"
#include "rob.h"
#include "QDSSLite.h"
#include "gpio_debug.h"
#include "sleep_lpr.h"
#define INT_STATUS_REGISTER_COUNT 2

#define ARR_SIZE(x) (sizeof(x) / sizeof (x[0]))

/* -----------------------------------------------------------------------
**                           DATA
** ----------------------------------------------------------------------- */

/*
 * Consolidated data
 */

static uint32_t wakeup_ints[INT_STATUS_REGISTER_COUNT];
static bool HAL_mpm_freeze_ios_enabled = TRUE;
	
#ifdef TSENSE_SAMPLING_1SEC_8936
static uint32 tsens_measure_period = 0;
#endif
HAL_mpm_PlatformType mpmPlatform = 
{
  // Placeholder for BSP config data.
  NULL,

  // Placeholders for some custom functions (unused).
  NULL,
  NULL,
  NULL
};

static uint32_t rpm_mpm_registers[50] __attribute__((used));
static uint32 sleep_bail_before_halt_counter = 0;

/* ===========================================================================  
**  HAL_mpm_SetCXOWarmup
**
** ======================================================================== */
void HAL_mpm_SetCXOWarmup (uint16_t warmupSclks)
{
  HWIO_OUTF(MPM2_MPM_CXO_POWER_RAMPUP_TIME, CXO_WARMUP_TIME, warmupSclks);
}
/* ===========================================================================
   	**  HAL_mpm_CfgFreezeIOs
	 	**
** ======================================================================== */
void HAL_mpm_CfgFreezeIOs(bool enable)
{
  HAL_mpm_freeze_ios_enabled = enable;
}	

/* ===========================================================================
**  HAL_mpm_CfgVoltageRailSleep
**
** ======================================================================== */

void HAL_mpm_CfgVoltageRailSleep(HAL_mpm_VoltageRailType voltageRail, uint32_t offMicrovolts, uint32_t onMicrovolts)
{
  uint32_t mask, val = 0;
  uint32_t numCommands = 1;
 
  HWIO_OUT(MPM2_MPM_SPMI_CDIV_CNTRL, 0x28); //program SMPI clock div value


  // First, the part of the register we need to set.
  //mask  = HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CORE_RESET);
  mask  = 0;
  switch(voltageRail)
  {
  case HAL_MPM_VOLTAGE_RAIL_VDD_DIG:
    
    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, VDDCX_MIN_EN); //enable CX minimization
    if(!(HAL_mpm_freeze_ios_enabled && mpmPlatform.mpmCfg->mpmCfg.ioCfg.freezeIOs))
    {
      mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_FREEZE);
    }
    if(!mpmPlatform.mpmCfg->mpmCfg.ioCfg.clampIOs)
    {
      mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CLAMPS);
    }

    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_HW_RESTORED);
    val = mask;

    // Always add these to the mask--that way they are cleared if the BSP
    // checks above indicate that they shouldn't be set.
    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_FREEZE);
    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CLAMPS);

    break;
  case HAL_MPM_VOLTAGE_RAIL_VDD_MEM:
    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, VDDMX_MIN_EN); //enable MX minimization
    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, SMEM_EN); //enable split rail mem
     if(!(HAL_mpm_freeze_ios_enabled && mpmPlatform.mpmCfg->mpmCfg.ioCfg.freezeIOs))
    {
      mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_FREEZE_IO_M);
    }
    if(!mpmPlatform.mpmCfg->mpmCfg.ioCfg.clampIOs)
    {
      mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CLAMP_MEM);
    }
    val = mask;

    // Always add these to the mask--that way they are cleared if the BSP
    // checks above indicate that they shouldn't be set.
    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_FREEZE_IO_M);
    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CLAMP_MEM);
  }

  HWIO_OUTM(MPM2_MPM_LOW_POWER_CFG, mask, val);

  /* program SPMI commands */
  numCommands = HAL_mpm_CfgVoltageCommands(voltageRail, offMicrovolts, onMicrovolts);
  HWIO_OUTF(MPM2_MPM_SPMI_CMD_CFG, NUM_CMD_ON, numCommands);
  HWIO_OUTF(MPM2_MPM_SPMI_CMD_CFG, NUM_CMD_OFF, numCommands);
  HAL_mpm_CfgNumCommands();
}

/* ===========================================================================  
**  HAL_mpm_CfgXOSleep
**
** ======================================================================== */
void HAL_mpm_CfgCXOSleep (HAL_mpm_XOType xo)
{

  uint32_t mask = 0;
  uint32_t val = 0;

  if (mpmPlatform.mpmCfg->mpmCfg.refClk == HAL_MPM_XO_CXO)
  {
    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, ENABLE_CXOPAD_GATING);
    mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, CXO_SD_EN);
  }
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_FREEZE_IO_M);
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_FREEZE);
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CLAMP_MEM);
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CLAMPS);
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CORE_RESET);

  val = mask;

  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_TRST_N);

  HWIO_OUTM(MPM2_MPM_LOW_POWER_CFG, mask, val);

  // MPM_V4 automatically powers CXO back on after a sleep.  This register 
  //is to warm up another ref clock, so only do so if the ref clk is not CXO.
  if (mpmPlatform.mpmCfg->mpmCfg.refClk != HAL_MPM_XO_CXO)
  {
    HWIO_OUT(MPM2_MPM_INT_WKUP_CLK_SEL_1, 0xffffffff);
    HWIO_OUT(MPM2_MPM_INT_WKUP_CLK_SEL_2, 0xffffffff);
  }
  else
  {
    HWIO_OUT(MPM2_MPM_INT_WKUP_CLK_SEL_1, 0x0);
    HWIO_OUT(MPM2_MPM_INT_WKUP_CLK_SEL_2, 0x0);
  }
}

/* ===========================================================================  
**  HAL_mpm_ClearSleepModes
**
** ======================================================================== */
void HAL_mpm_ClearSleepModes (void)
{
  uint32_t mask = 0;

  // Issue HARDWARE RESTORED command to allow unclamp / unfreeze.
  HWIO_OUT(MPM2_MPM_HARDWARE_RESTORED, 1);

  // Build up a mask of everything we want to clear (omitting EBI1 registers  
  //   as thats only triggered by DDR collapse/restore)
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, SMEM_EN); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_FREEZE_IO_M); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CLAMP_MEM); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_HW_RESTORED); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, ENABLE_CXOPAD_GATING); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_TRST_N); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CORE_RESET); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_CLAMPS); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DISABLE_FREEZE); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, DEBUG_ON); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, VDDMX_MIN_EN); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, VDDMX_PC_EN); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, VDDCX_MIN_EN); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, VDDCX_PC_EN); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, PXO_SD_EN); 
  mask |= HWIO_FMSK(MPM2_MPM_LOW_POWER_CFG, CXO_SD_EN); 

  // Disable all power saving bits.
  HWIO_OUTM( MPM2_MPM_LOW_POWER_CFG, mask, 0 );

  // Additionally, make sure that next time the MPM is used we don't
  // auto-reactivate CXO unless we meant to.
  HWIO_OUT(MPM2_MPM_INT_WKUP_CLK_SEL_1, 0);
  HWIO_OUT(MPM2_MPM_INT_WKUP_CLK_SEL_2, 0);
}


/* ===========================================================================  
**  HAL_mpm_DisableCXO
**
** ======================================================================== */
void HAL_mpm_DisableCXO (void)
{
  uint32_t mask, value;
  mask  = HWIO_FMSK(MPM2_MPM_CXO_CTRL, CXO_DIS);
  mask |= HWIO_FMSK(MPM2_MPM_CXO_CTRL, CXOPAD_DIS);
  value = mask;
  HWIO_OUTM(MPM2_MPM_CXO_CTRL, mask, value);
}


/* ===========================================================================  
**  HAL_mpm_EnableCXO
**
** ======================================================================== */
void HAL_mpm_EnableCXO (void)
{
  uint32_t mask;
  mask  = HWIO_FMSK(MPM2_MPM_CXO_CTRL, CXO_DIS);
  mask |= HWIO_FMSK(MPM2_MPM_CXO_CTRL, CXOPAD_DIS);
  HWIO_OUTM(MPM2_MPM_CXO_CTRL, mask, 0);
}


/* ===========================================================================  
**  HAL_mpm_ReleaseXO
**
** ======================================================================== */
//[MDA] not possible with MPMv2 as there is no ALT_FUNC_SEL. Wait for Mazen's response to verify

/* ===========================================================================  
**  HAL_mpm_DebugThroughPC
**
** ======================================================================== */
void HAL_mpm_DebugThroughPC( bool32 enable )
{
  uint32_t bmsk = HWIO_FMSK( MPM2_MPM_LOW_POWER_CFG, DEBUG_ON ) | HWIO_FMSK( MPM2_MPM_LOW_POWER_CFG, DISABLE_TRST_N );
  uint32_t val = 0;

  if ( enable ) 
  {
    val = bmsk;
  }

  HWIO_OUTM( MPM2_MPM_LOW_POWER_CFG, bmsk, val );
}


/* ===========================================================================  
**  HAL_mpm_ConfigDebugBus
**
** ======================================================================== */
void HAL_mpm_ConfigDebugBus( bool32 enable )
{
  uint32_t bmsk = HWIO_FMSK( MPM2_MPM_DEBUG_BUS_EN, DEBUG_EN );
  uint32_t val = 0;

  (mpmPlatform.mpmCfg)->mpmCfg.dbgBusEnable = enable;

  if ( enable ) 
  {
    /* Both bits cannot be enabled at the same time, so set it 
     * up to write a 0x0 to the other bit. */
    bmsk |= HWIO_FMSK( MPM2_MPM_DEBUG_BUS_EN, DEBUG_EN_1 );
    val = HWIO_FMSK( MPM2_MPM_DEBUG_BUS_EN, DEBUG_EN );
  }

  HWIO_OUTM( MPM2_MPM_DEBUG_BUS_EN, bmsk, val );
}

/* ===========================================================================  
**  HAL_mpm_ConfigDebugBus_1
**
** ======================================================================== */
void HAL_mpm_ConfigDebugBus_1( bool32 enable )
{
  uint32_t bmsk = HWIO_FMSK( MPM2_MPM_DEBUG_BUS_EN, DEBUG_EN_1 );
  uint32_t val = 0;

  (mpmPlatform.mpmCfg)->mpmCfg.dbgBusEnable_1 = enable;

  if ( enable ) 
  {
    /* Both bits cannot be enabled at the same time, so set it 
     * up to write a 0x0 to the other bit. */
    bmsk |= HWIO_FMSK( MPM2_MPM_DEBUG_BUS_EN, DEBUG_EN );
    val = HWIO_FMSK( MPM2_MPM_DEBUG_BUS_EN, DEBUG_EN_1 );
  }

  HWIO_OUTM( MPM2_MPM_DEBUG_BUS_EN, bmsk, val );
}

/* ===========================================================================  
**  HAL_mpm_PlatformInitEarly
**
** ======================================================================== */
void HAL_mpm_PlatformInitEarly (HAL_mpm_PlatformType *pmPlatform)
{
  uint32 tcsr_ctrl_state;
  //The following 32bit register is split into 16 bit fields but the fields were swapped
  //workaround to intialize the register correctly.
  HWIO_OUT(MPM2_MPM_CNTR_INCR_DATA, 0x24a0000);

  //enable the MPM counter
  HWIO_OUTF(MPM2_MPM_CONTROL_CNTCR, EN, 1);

  //enable access to the CNTP_* and CNTPCT registers
  HWIO_OUTI(MPM2_QTMR_AC_CNTACR_n, 0, 33);
  HWIO_OUTF(MPM2_QTMR_AC_CNTSR, NSN, 1);

  //enable bus and proc clock halt on wfi
  // RJ: temporarily disable BUS_CLK_HALT so that Apps can read sleep stats
  // HWIO_OUTF(RPM_WFI_CONFIG, BUS_CLK_HALT, 1);
  HWIO_OUTF(RPM_WFI_CONFIG, PROC_CLK_HALT, 1);

  //enable internal LDO001
  tcsr_ctrl_state = HWIO_INF(TCSR_LDO_UPDATE_STATE_CTRL, LDO_UPDATE_STATE);
  HWIO_OUTF(TCSR_LDO_UPDATE_STATE_CTRL, LDO_UPDATE_STATE, 1);
  HWIO_OUTF(TCSR_LDO_BGC_CONFIG, LDO_BGC, 1);
  
#ifndef MPM_INTERNAL_VREF
  // use external vref
  HWIO_OUTF(TCSR_LDO_VREF_CTRL, LDO_VREF_SEL_OVR, 1);
  HWIO_OUTF(TCSR_LDO_VREF_CTRL, LDO_VREF_SEL_SW, 0);
#endif //MPM_INTERNAL_VREF

  HWIO_OUTF(TCSR_LDO_UPDATE_STATE_CTRL, LDO_UPDATE_STATE, tcsr_ctrl_state);

  wakeup_ints[0] = 0;
  wakeup_ints[1] = 0;

} /* END HAL_mpm_PlatformInitEarly */

uint64_t mpm_wakeup_time = 0;

/* ===========================================================================
**  HAL_mpm_SetWakeUpTime
**
** ======================================================================== */
void HAL_mpm_SetWakeUpTime( uint64_t sclkDuration )
{
  uint64_t wakeup_time, temp_time;
  uint32_t lo, hi;

  /* Figure out what time we need to wake up at. */
  HAL_mpm_GetCurrentSclkCount(&wakeup_time);
  temp_time = wakeup_time;
  wakeup_time += sclkDuration;

  //check for wraparound
  if(temp_time > wakeup_time)
    wakeup_time = 0xFFFFFFFFFFFFFF;

  mpm_wakeup_time = wakeup_time;
  lo = (uint32)wakeup_time;
  hi = wakeup_time >> 32;

  SWEVENT(SLEEP_MPM_WAKEUP_TIME, lo, hi);
  
  HWIO_OUT(MPM2_QTMR_V1_CNTP_CVAL_LO, lo);
  HWIO_OUT(MPM2_QTMR_V1_CNTP_CVAL_HI, hi);

  // Make sure the timer is ticking.
  HWIO_OUTF(MPM2_QTMR_V1_CNTP_CTL, EN, 1);
}

/* ===========================================================================
**  HAL_mpm_CfgEbi1SwCtl
**
** ======================================================================== */
void HAL_mpm_CfgEbi1SwCtl(bool enable)
{

  if (enable && mpmPlatform.mpmCfg->mpmCfg.ioCfg.swEbi1CtlEnable)
  {
    HWIO_OUTF(MPM2_MPM_LOW_POWER_CFG, SW_EBI1_CTL_ENABLE, 1);
    HWIO_OUTF(MPM2_MPM_LOW_POWER_CFG, SW_EBI1_CTL_VALUE, 1);
  }
  else if ((!enable) && mpmPlatform.mpmCfg->mpmCfg.ioCfg.swEbi1CtlEnable)
  {
    HWIO_OUTF (MPM2_MPM_LOW_POWER_CFG, SW_EBI1_CTL_ENABLE, 1);
    HWIO_OUTF (MPM2_MPM_LOW_POWER_CFG, SW_EBI1_CTL_VALUE, 0);
  }
}

/* ===========================================================================
**  HAL_mpm_CfgWbootEbiIOCntl
**
** ======================================================================== */
void HAL_mpm_CfgWbootEbiIOCntl( bool32 wbootFreezeEBI1, bool32 wbootFreezeEBI2 )
{
  uint32_t bmsk = HWIO_FMSK(MPM2_MPM_WARM_BOOT_CFG, ENABLE_EBI1) |
                HWIO_FMSK(MPM2_MPM_WARM_BOOT_CFG, ENABLE_EBI2);

  uint32_t val  = 0;

  if (!wbootFreezeEBI1) 
  {
    val |= HWIO_FMSK(MPM2_MPM_WARM_BOOT_CFG, ENABLE_EBI1);
  }

  if (!wbootFreezeEBI2) 
  {
    val |= HWIO_FMSK(MPM2_MPM_WARM_BOOT_CFG, ENABLE_EBI2);
  }

  HWIO_OUTM(MPM2_MPM_WARM_BOOT_CFG, bmsk, val);
}


/* ===========================================================================
**  HAL_mpm_PenDebounceCtl
**
** ======================================================================== */
void HAL_mpm_PenDebounceCtl( bool32 enable )
{
  uint32_t val = enable ? 1 : 0;

  HWIO_OUTF(MPM2_MPM_PEN_DEBOUNCE_CTL, ENABLE, val);
}

/* ===========================================================================
**  HAL_mpm_ConfigPenDebounceDelay
**
** ======================================================================== */
void HAL_mpm_ConfigPenDebounceDelay( HAL_mpm_PenDebounceDelayType delay )
{
  uint32_t val = delay;

  HWIO_OUTF(MPM2_MPM_PEN_DEBOUNCE_CTL, TIME, val);
}

/* ===========================================================================
**  HAL_mpm_GetCurrentSclkCount
**
** ======================================================================== */
void HAL_mpm_GetCurrentSclkCount( uint64_t *pTickCount)
{
  /* Time Tick count */
  //uint32_t pTickCountHigh;
  //uint32_t pTickCountLow;

  uint32_t                   ticklo, tickhi, tickhi_count;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /* Reading the counter value once may not return an accurate value if the
     counter is in the processing of counting to the next value, and several
     bits are changing.  Instead, the counter is repeatedly read until a
     consistant value is read. */


  

  do
  {
    tickhi = HWIO_IN(MPM2_QTMR_V1_CNTPCT_HI);
    __schedule_barrier();
 
    ticklo = HWIO_IN(MPM2_QTMR_V1_CNTPCT_LO);
    __schedule_barrier();

    tickhi_count = HWIO_IN(MPM2_QTMR_V1_CNTPCT_HI);
    __schedule_barrier();

  } while (tickhi_count != tickhi);

  /* The counter has returned the same value twice in a row, and hence must
     be stable. */

  //pTickCountHigh = tickhi_count;
  //pTickCountLow = ticklow;
 
  //convert to a uint64
  //*pTickCount = pTickCountHigh;
  //*pTickCount = (*pTickCount << 32);
  //*pTickCount |= pTickCountLow;

  *pTickCount = (((uint64_t)tickhi) << 32) | ticklo;
}

uint32_t HAL_mpm_ReadFSMState(void)
{
  uint32_t old_state, state = HWIO_INF(MPM2_MPM_LOW_POWER_STATUS, FSM_STATE);

  do
  {
    old_state = state;
    state     = HWIO_INF(MPM2_MPM_LOW_POWER_STATUS, FSM_STATE);
  } while(state != old_state);

  return state;
}

__asm static void halt(void)
{
    ; Lock interrupts via primask instead of basepri during sleep, or else we never wake up.
    mrs r0, basepri
    cpsid i
    mov r1, #0
    msr basepri, r1

    ; Do the halt, using ISB to clear the M3 store buffer
    dsb
    isb
    wfi

    ; Revert interrupt lock to basepri-based.
    msr basepri, r0
    cpsie i
    bx r14
}

void HAL_mpm_GetWakeupInts(uint32 mask_index, uint32 *status)
{
  if (mask_index >= INT_STATUS_REGISTER_COUNT)
    return;

  *status = wakeup_ints[mask_index];
}

extern boolean Clock_IsQDSSOn(void);
extern bool sleep_is_any_interrupt_pending(void);

void HAL_mpm_SWDone(HAL_mpm_SleepModeType sleep_type, uint64_t sleep_duration)
{
  static const    uint32_t MPM_STATE_IDLE = 0;
  static const    uint32_t MPM_STATE_UNFREEZE = 5;
  static const    uint32_t MPM_STATE_BOOT = 9;
  uint32_t                 sleep_val;
  uint32_t                 qdss_was_locked = 0;
  uint32_t                 en1, en2;

  uint32_t                 disable_hw_restored = 0;
  boolean                  is_qdss_on = Clock_IsQDSSOn();
  uint32_t                 mpm_state = MPM_STATE_IDLE;
  volatile unsigned int *addr = (volatile unsigned int *)MPM2_MPM_BASE;
  unsigned int i = 0;
  
  en1 = HWIO_IN(MPM2_MPM_INT_EN_1);
  en2 = HWIO_IN(MPM2_MPM_INT_EN_2);

  SWEVENT(SLEEP_MPM_INTS_ENABLED, en1, en2);

  if (is_qdss_on)
  {
     QDSSPreXOShutdown();
  }

  //switch counter to sleep clock. 
  HWIO_OUTF(MPM2_MPM_CONTROL_CNTCR, FCREQ, 1);

  /* configure the MPM to halt the chip on wfi */
  sleep_val  = 1 << HWIO_SHFT(RPM_WFI_CONFIG, PROC_CLK_HALT);
  
  /* don't set chip sleep if we are in debug mode */
  if (HAL_MPM_SLEEP_MODE_DEBUG != sleep_type)
  {
    sleep_val |= 1 << HWIO_SHFT(RPM_WFI_CONFIG, CHIP_SLEEP_UPON_WFI);
    sleep_val |= 1 << HWIO_SHFT(RPM_WFI_CONFIG, BUS_CLK_HALT);
    // RJ: temporarily disable BUS_CLK_HALT so that Apps can read sleep stats from Apps
  }

  HWIO_OUT(RPM_WFI_CONFIG, sleep_val);
  
  if (is_qdss_on)
  {
    /* share the qdss hardware nicely: check if the csr is locked before we unlock it */
    qdss_was_locked = HWIO_INF(QDSS_CS_QDSSCSR_LOCKSTATUS, ACCESS);
  }

  if(qdss_was_locked)
  {
    HWIO_OUT(QDSS_CS_QDSSCSR_LOCKACCESS, 0xc5acce55);

    /* Bit[0] of this register masks an internal debugger signal that prevents the
     * M3 from "really" sleeping when there's a debugger attached, regardless of
     * the run/break state of the processor. */
    HWIO_OUTM(QDSS_CS_QDSSCSR_QDSSPWRREQIGNORE, 1, 1); // bit[0] controls the RPM signal
  }

  // capture all MPM registers
  for (i = 0; i < ARR_SIZE(rpm_mpm_registers); i++, addr++)
  {
    rpm_mpm_registers[i] = in_dword(addr);
  }
  
  // mark the event
  rob_mark_event (ROB_EVENT_MPM_PRE_SW_DONE);

  HAL_mpm_SetWakeUpTime(sleep_duration);
  
  // check if we need to resynchronize our state with the MPM
  disable_hw_restored = HWIO_INF(MPM2_MPM_LOW_POWER_CFG, DISABLE_HW_RESTORED); 

  
  gpio_test_high(GPIO_DEEP_SLEEP, GPIO_SLEEP_TRANSITION);
  /* invoke sleep */
  if (!sleep_is_any_interrupt_pending())
  {
    halt();
  }
  else
  {
    //log that we bailed early
    SWEVENT(SLEEP_NO_DEEP_SLEEP, HAL_MPM_SLEEP_MODE_COUNT, SLEEP_INTR_PENDING_BEFORE_HALT);
    sleep_bail_before_halt_counter++;
  }
  gpio_test_low(GPIO_DEEP_SLEEP, GPIO_ACTIVE_TRANSITION);

  /* if the qdss hardware was not already locked by someone else, clean up after ourselves */
  if(qdss_was_locked)
  {
    /* clear the mask of the debug signal so that our halt behavior is good */
    HWIO_OUTM(QDSS_CS_QDSSCSR_QDSSPWRREQIGNORE, 1, 0); // bit[0] controls the RPM signal

    HWIO_OUT(QDSS_CS_QDSSCSR_LOCKACCESS, 0);
  }

  //get sleep exit time. Switch back to XO clock
  HWIO_OUTF(MPM2_MPM_CONTROL_CNTCR, FCREQ, 0);

  /* configure the MPM to halt the chip on wfi */
  sleep_val  = 1 << HWIO_SHFT(RPM_WFI_CONFIG, PROC_CLK_HALT);
  HWIO_OUT(RPM_WFI_CONFIG, sleep_val);

  mpm_state = HAL_mpm_ReadFSMState();
  
  if ((HAL_MPM_SLEEP_MODE_VDD_MIN == sleep_type) && (!disable_hw_restored))
  {
    //resynch with the mpm state machine
    if (MPM_STATE_BOOT == mpm_state)
    {
      HWIO_OUT(MPM2_MPM_HARDWARE_RESTORED, 1);
    }
  }

  // Secondary short circuit path in the RPM core
  if (MPM_STATE_IDLE != mpm_state)
  {
    // Must be in unfreeze 
    while(MPM_STATE_UNFREEZE != HAL_mpm_ReadFSMState());
  }
  
  // Disable the qtimer -- we're awake and don't need it running.
  HWIO_OUTF(MPM2_QTMR_V1_CNTP_CTL, EN, 0);

  // mark the event
  rob_mark_event (ROB_EVENT_MPM_POST_SW_DONE);
  
  en1 = HWIO_IN(MPM2_MPM_INT_EN_1);
  en2 = HWIO_IN(MPM2_MPM_INT_EN_2);

  // log wakeup interrupts
  wakeup_ints[0] = HWIO_IN(MPM2_MPM_INT_STATUS_1);
  wakeup_ints[1] = HWIO_IN(MPM2_MPM_INT_STATUS_2);

  if (is_qdss_on) {
    QDSSPostXOShutdown(); //this restores qdss timestamps
  }

  SWEVENT(SLEEP_MPM_INTS, (wakeup_ints[0] & en1), (wakeup_ints[1] & en2));

  // Clear all interrupts.
  HWIO_OUT(MPM2_MPM_INT_CLEAR_1, 0xFFFFFFFF);
  HWIO_OUT(MPM2_MPM_INT_CLEAR_2, 0xFFFFFFFF);

  // Disable all interrupts.
  HWIO_OUT(MPM2_MPM_INT_EN_1, 0);
  HWIO_OUT(MPM2_MPM_INT_EN_2, 0);

  // Disabling the interrupts should move us from UNFREEZE to IDLE
  while(MPM_STATE_IDLE != HAL_mpm_ReadFSMState());

  HWIO_OUT(MPM2_MPM_INT_EN_1, en1);
  HWIO_OUT(MPM2_MPM_INT_EN_2, en2);
}

/* ===========================================================================
**  HAL_mpm_true_sleep_xtal_set
**
** ======================================================================== */
void HAL_mpm_true_sleep_xtal_set ( void )
{
    /* To support a true 32kHz sleep clock:                             **
    **  Clock = 32.768 kHz -> 19.2MHz/Clock = 585.9375                  **
    **  Lower 16 bits of MPM_CNTR_INCR_DATA are for fractional count    **
    **  Set register to 0x0249F000 as this is a fixed point 16.16 num    */
    HWIO_OUT(MPM2_MPM_CNTR_INCR_DATA, 0x0249F000);
}

#ifdef TSENSE_SAMPLING_1SEC_8936
/* ===========================================================================
**  HAL_deep_sleep_enter_tsense
**
** ======================================================================== */
void HAL_deep_sleep_enter_tsense(void)
{
    // to improve sleep current in 8936, the sampling rate of TSENSE is 
    // reduced to 1 second. This will be reverted back to the the original  
    // value of 62.5 ms during wake
     tsens_measure_period = HWIO_INF(MPM2_TSENS_CTRL, MEASURE_PERIOD);
     HWIO_OUTF(MPM2_TSENS_CTRL, MEASURE_PERIOD, 0x5);
     HWIO_OUTF(MPM2_TSENS_CTRL, TSENS_SW_RST , 0x1);
     HWIO_OUTF(MPM2_TSENS_CTRL, TSENS_SW_RST , 0x0);
     HWIO_OUTF(MPM2_TSENS_CTRL, TSENS_EN  , 0x1);
}

/* ===========================================================================
**  HAL_deep_sleep_exit_tsense
**
** ======================================================================== */
void HAL_deep_sleep_exit_tsense(void)
{
    // restore the sampling rate of TSENSE
     HWIO_OUTF(MPM2_TSENS_CTRL, MEASURE_PERIOD, tsens_measure_period);
     HWIO_OUTF(MPM2_TSENS_CTRL, TSENS_SW_RST , 0x1);
     HWIO_OUTF(MPM2_TSENS_CTRL, TSENS_SW_RST , 0x0);
     HWIO_OUTF(MPM2_TSENS_CTRL, TSENS_EN  , 0x1);
}
#endif
