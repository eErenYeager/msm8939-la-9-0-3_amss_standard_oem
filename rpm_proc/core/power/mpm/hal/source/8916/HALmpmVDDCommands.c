/*
===========================================================================

FILE:         HALmpmVDDCommands.c

DESCRIPTION:
  This is the platform hardware abstraction layer implementation for the
  MPM VDD commands.
  This platform is for the RPM on 8916.

===========================================================================

                             Edit History

$Header: //components/rel/rpm.bf/2.0.c9/core/power/mpm/hal/source/8916/HALmpmVDDCommands.c#1 $

when       who     what, where, why
--------   ---     --------------------------------------------------------
02/21/14   sv      Used generic api to obtain vset value for regulators. (CR-549436)
===========================================================================
             Copyright (c) 2013 Qualcomm Technologies Incorporated.
                    All Rights Reserved.
              QUALCOMM Proprietary and Confidential
===========================================================================
*/

/* -----------------------------------------------------------------------
**                           INCLUDES
** ----------------------------------------------------------------------- */

#include <stdlib.h>
#include "CoreVerify.h"
#include "HALhwio.h"
#include "mpm2_hwio.h"
#include "HALmpm.h"
#include "pmapp_npa.h"
#include "pm_smps.h"
#include "pm_version.h"

#include "../common/HALmpmInternal.h"

extern HAL_mpm_PlatformType mpmPlatform;

static volatile uint16 cx_delay, mx_delay;

//The values for OffMicrovolts for S3/S4 has been taken from sleep sequence values in 8916_HW_SW_Document
static uint32 s3_offMicrovolts=1225000;
static uint32 s4_offMicrovolts=1850000;

/* -----------------------------------------------------------------------
**                           STATIC FUNCTIONS
** ----------------------------------------------------------------------- */

static void HAL_mpm_GetVSet
(
  HAL_mpm_VoltageRailType   rail,
  uint32                    uv,
  uint32                   *vset
)
{
  pm_railway_type_info_type pm_rail = PM_RAILWAY_INVALID;

  switch(rail)
  {
     case HAL_MPM_VOLTAGE_RAIL_VDD_DIG:
       pm_rail = PM_RAILWAY_CX;
       break;

    case HAL_MPM_VOLTAGE_RAIL_VDD_MEM:
      pm_rail = PM_RAILWAY_MX;
      break;

    default:
      abort();
  }

   CORE_VERIFY(PM_ERR_FLAG__SUCCESS == pm_railway_calculate_vset(pm_rail, uv, vset));
}

// SPMI Delay table from MPM2 HPG
// 4bit   # XO cycles     Delay in usec
// 0000   0               0
// 0001   192             10
// 0010   480             25
// 0011   960             50
// 0100   1920            100
// 0101   2880            150
// 0110   3840            200
// 0111   4800            250
// 1000   5760            300
// 1001   6720            350
// 1010   7680            400
// 1011   9600            500
// 1100   11520           600
// 1101   13440           700
// 1110   16320           850
// 1111   19200           1000
static uint16 delayTable[16]      = {0, 10, 25, 50, 100, 150, 200, 250, 300, 350, 400, 500, 600, 700, 850, 1000};
static uint16 delayTableSclks[16] = {1,  1,  1,  2,   4,   5,   7,   9,  10,  12,  14,  17,  20,  24,  28,   33};
static uint16 HAL_mpm_GetDelayFromMicrovolts(uint32 offMicrovolts, uint32 onMicrovolts)
{
  uint32 desiredDelay = 0;
  unsigned int max = 16;
  unsigned int min = 0;
  unsigned int i = 0;
  unsigned int mid = max / 2;

  // PM8026 step FTS2 and LDO @1.5mV/usec
  desiredDelay = (uint32)CEILING_DIV((onMicrovolts - offMicrovolts), 1500);
  // Add 50uS of additional delay, per pmic systems
  desiredDelay += 50;

  //max of 1000us delay
  if (desiredDelay > 1000)
    return 15;

  for (i = 0; i < 4; i++)
  {
    if (delayTable[mid] == desiredDelay)
      break;
    if (delayTable[mid] < desiredDelay)
    {
      if (i == 3)
      {
        mid++;
        break;
      }
      min = mid;
    }
    else
    {
      if (i == 3)
      {
        break;
      }
      max = mid;
    }
    mid = ((max + min) / 2);
  }
  return mid;
}

static void config_sleep_commands(void)
{
  uint32 regVal;

  // VDD_OFF_CFG_5 - PBS Trigger (200us)
  regVal  = 0x6             << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_5, DELAY);
  regVal |= 0               << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_5, SLAVE_ID);
  regVal |= 0x7542          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_5, SLAVE_ADDR);
  regVal |= 0x01            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_5, SLAVE_DATA);
  HWIO_OUT(MPM2_MPM_PMIC_VDD_OFF_CFG_5, regVal);
}

static void config_wakeup_commands(void)
{
  uint32 regVal;

  // VDD_ON_CFG_1 - PBS Trigger (200us)
  regVal  = 0x6             << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_1, DELAY);
  regVal |= 0               << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_1, SLAVE_ID);
  regVal |= 0x7542          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_1, SLAVE_ADDR);
  regVal |= 0x01            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_1, SLAVE_DATA);
  HWIO_OUT(MPM2_MPM_PMIC_VDD_ON_CFG_1, regVal);
}

/* -----------------------------------------------------------------------
**                           FUNCTIONS
** ----------------------------------------------------------------------- */

unsigned int HAL_mpm_CfgVoltageCommands
(
  HAL_mpm_VoltageRailType voltageRail,
  uint32_t                offMicrovolts,
  uint32_t                onMicrovolts
)
{
  uint16 delay;
  uint32 vset;

  uint32 valONVSet=0, valOFFVSet=0;
  uint32 s3_active_volt_level;
  uint32 s4_active_volt_level;
  uint16 s3_delay=0;
  uint16 s4_delay=0;
 
  pm_device_info_type pmic_device_info;
  
  pm_get_pmic_info(0, &pmic_device_info);

  delay  = HAL_mpm_GetDelayFromMicrovolts(offMicrovolts, onMicrovolts);

  switch(voltageRail)
  {
    case HAL_MPM_VOLTAGE_RAIL_VDD_DIG:
    {
      cx_delay = delay;
      // VDD_OFF_CFG_1 - Set S1 (Cx) to RETENTION
      HAL_mpm_GetVSet(HAL_MPM_VOLTAGE_RAIL_VDD_DIG, offMicrovolts,  &vset);
      // VDD_OFF_CFG_1 - Set S1 (Cx) VOLTAGE_CTL2 VSET
      valOFFVSet   = cx_delay      	 << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_1, DELAY);
      valOFFVSet  |= 0x1             << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_1, SLAVE_ID);
      valOFFVSet  |= 0x1441          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_1, SLAVE_ADDR);
      valOFFVSet  |= vset            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_1, SLAVE_DATA);
      HWIO_OUT(MPM2_MPM_PMIC_VDD_OFF_CFG_1, valOFFVSet);
      
      // VDD_ON_CFG_4 - Set S1 (Cx) ON
      HAL_mpm_GetVSet(HAL_MPM_VOLTAGE_RAIL_VDD_DIG, onMicrovolts, &vset);
      // VDD_ON_CFG_4 - Set S1 (Cx) VOLTAGE_CTL2 VSET
      valONVSet    = delay           << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_4, DELAY);
      valONVSet   |= 0x1             << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_4, SLAVE_ID);
      valONVSet   |= 0x1441          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_4, SLAVE_ADDR);
      valONVSet   |= vset            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_4, SLAVE_DATA);
      HWIO_OUT(MPM2_MPM_PMIC_VDD_ON_CFG_4, valONVSet);

      HWIO_OUTF(MPM2_MPM_CXO_POWER_RAMPUP_TIME, POWER_RAMPUP_TIME, delayTableSclks[delay]);
      return 1;
    }

    case HAL_MPM_VOLTAGE_RAIL_VDD_MEM:
    {
      mx_delay = delay;
      // VDD_OFF_CFG_2 - Set L3 (Mx) to RETENTION
      HAL_mpm_GetVSet(HAL_MPM_VOLTAGE_RAIL_VDD_MEM, offMicrovolts, &vset);
      // VDD_OFF_CFG_2 - Set L3 (Mx) VOLTAGE_CTL2 VSET
      valOFFVSet   = delay           << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_2, DELAY);
      valOFFVSet  |= 0x1             << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_2, SLAVE_ID);
      valOFFVSet  |= 0x4241          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_2, SLAVE_ADDR);
      valOFFVSet  |= vset            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_2, SLAVE_DATA);
      HWIO_OUT(MPM2_MPM_PMIC_VDD_OFF_CFG_2, valOFFVSet);

	  // VDD_OFF_CFG_3 - Set S3 (parent of L3)
	  //Calculate Vset for S3 OFF Voltage 
	  pm_smps_calculate_vset(0, 2, s3_offMicrovolts, &vset);
	  //get S3 ACTIVE Voltage
	  pm_smps_volt_level_status (0, 2, &s3_active_volt_level);
	  //Calcualte delay for S3 ramp up/down
	  s3_delay = HAL_mpm_GetDelayFromMicrovolts(s3_offMicrovolts, s3_active_volt_level);
 
      valOFFVSet    = s3_delay        << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_3, DELAY);
      valOFFVSet   |= 1               << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_3, SLAVE_ID);
      valOFFVSet   |= 0x1A41          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_3, SLAVE_ADDR);
      valOFFVSet   |= vset            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_3, SLAVE_DATA);
      HWIO_OUT(MPM2_MPM_PMIC_VDD_OFF_CFG_3, valOFFVSet);
     
      if (!((1 == pmic_device_info.nPmicAllLayerRevision) && (pmic_device_info.nPmicMetalRevision == 0)))
      { 
        // VDD_OFF_CFG_4 - Set S4
		//Calculate Vset for S4 OFF Voltage 
		pm_smps_calculate_vset(0, 3, s4_offMicrovolts, &vset);
		//get S4 ACTIVE Voltage
		pm_smps_volt_level_status (0, 3, &s4_active_volt_level);
		//Calcualte delay for S4 ramp up/down
		s4_delay = HAL_mpm_GetDelayFromMicrovolts(s4_offMicrovolts, s4_active_volt_level);
				
		valOFFVSet    = s4_delay           << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_4, DELAY);
        valOFFVSet   |= 1               << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_4, SLAVE_ID);
        valOFFVSet   |= 0x1D41          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_4, SLAVE_ADDR);
        valOFFVSet   |= vset            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_4, SLAVE_DATA);
        HWIO_OUT(MPM2_MPM_PMIC_VDD_OFF_CFG_4, valOFFVSet);
      }

      // VDD_ON_CFG_2 - Set S3 (parent of L3) 
	  //Calculate Vset for S3 ON Voltage
	  pm_smps_calculate_vset(0, 2, s3_active_volt_level, &vset);
      valONVSet    = s3_delay           << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_2, DELAY);
      valONVSet   |= 1               << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_2, SLAVE_ID);
      valONVSet   |= 0x1A41          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_2, SLAVE_ADDR);
      valONVSet   |= vset            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_2, SLAVE_DATA);
      HWIO_OUT(MPM2_MPM_PMIC_VDD_ON_CFG_2, valONVSet);
	  
      // VDD_ON_CFG_3 - Set L3 (Mx) ON
      HAL_mpm_GetVSet(HAL_MPM_VOLTAGE_RAIL_VDD_MEM, onMicrovolts, &vset);
      // VDD_ON_CFG_3 - Set L3 (Mx) VOLTAGE_CTL2 VSET
      valONVSet    = 0               << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_3, DELAY);
      valONVSet   |= 0x1             << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_3, SLAVE_ID);
      valONVSet   |= 0x4241          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_3, SLAVE_ADDR);
      valONVSet   |= vset            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_3, SLAVE_DATA);
      HWIO_OUT(MPM2_MPM_PMIC_VDD_ON_CFG_3, valONVSet);

      if (!((1 == pmic_device_info.nPmicAllLayerRevision) && (pmic_device_info.nPmicMetalRevision == 0)))
      {
        // VDD_ON_CFG_5- Set S4
		//Calculate Vset for S4 ON Voltage
		pm_smps_calculate_vset(0, 3, s4_active_volt_level, &vset);
        valONVSet    = s4_delay        << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_5, DELAY);
        valONVSet   |= 1               << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_5, SLAVE_ID);
        valONVSet   |= 0x1D41          << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_5, SLAVE_ADDR);
        valONVSet   |= vset            << HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_5, SLAVE_DATA);
        HWIO_OUT(MPM2_MPM_PMIC_VDD_ON_CFG_5, valONVSet);
      }

      if (mx_delay > cx_delay)
      {
          // VDD_OFF_CFG_2 - Set L3 (Mx) OFF delay as MAX of CX, MX 
          //HWIO_OUTF(MPM2_MPM_PMIC_VDD_OFF_CFG_2, DELAY, mx_delay);
          // VDD_ON_CFG_4  - Set S1 (Cx) ON delay as MAX of CX, MX
          HWIO_OUTF(MPM2_MPM_PMIC_VDD_ON_CFG_4, DELAY, mx_delay);
      }

      config_sleep_commands();
      config_wakeup_commands();

      return 12;
    }

    default:
      abort();
  }
}

void HAL_mpm_CfgNumCommands(void)
{
  HWIO_OUTF(MPM2_MPM_SPMI_CMD_CFG, NUM_CMD_ON,  4); // 7 commands
  HWIO_OUTF(MPM2_MPM_SPMI_CMD_CFG, NUM_CMD_OFF, 4); // 5 commands
}

uint32 HAL_mpm_GetVddMinEnterLatency(void)
{
  uint32 off_cfg = HWIO_ADDR(MPM2_MPM_PMIC_VDD_OFF_CFG_1);
  uint32 shift   = HWIO_SHFT(MPM2_MPM_PMIC_VDD_OFF_CFG_1, DELAY);
  uint32 delay   = 0;
  uint32 num_cmds, val;
  uint32 i;

  // Get # of VDD_OFF_CFG commands
  num_cmds = HWIO_INF(MPM2_MPM_SPMI_CMD_CFG, NUM_CMD_OFF) + 1;
  for (i = 0; i < num_cmds; i++)
  {
    val = *((uint32 *)(off_cfg + (i << 2)));
    delay += delayTable[(val >> shift)];
  }

  // Convert us into timetick
  delay = (delay * 19200) / 1000;

  return delay;
}

uint32 HAL_mpm_GetVddMinExitLatency(void)
{
  uint32 on_cfg  = HWIO_ADDR(MPM2_MPM_PMIC_VDD_ON_CFG_1);
  uint32 shift   = HWIO_SHFT(MPM2_MPM_PMIC_VDD_ON_CFG_1, DELAY);
  uint32 delay   = 0;
  uint32 num_cmds, val;
  uint32 i;

  // Get # of VDD_ON_CFG commands
  num_cmds = HWIO_INF(MPM2_MPM_SPMI_CMD_CFG, NUM_CMD_ON) + 1;
  for (i = 0; i < num_cmds; i++)
  {
    val = *((uint32 *)(on_cfg + (i << 2)));
    delay += delayTable[(val >> shift)];
  }

  // Convert us into timetick
  delay = (delay * 19200) / 1000;

  // Add CXO delay (in sclks so convert this to xo ticks as well)
  delay += (mpmPlatform.mpmCfg->mpmCfg.wakeupDelays.xoDelays[0] * 586);

  delay += (HWIO_INF(MPM2_MPM_CXO_POWER_RAMPUP_TIME, POWER_RAMPUP_TIME) * 586);

  // Add unknown 600us
  delay += 11520;

  return delay;
}

