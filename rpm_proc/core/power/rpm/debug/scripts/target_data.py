

#
# Target-specific pretty-printing
#

# This variable starts as a dictionary of targets.  When command line arguments
# are read, it is overwritten with only the specific dictionary for the target
# selected.
all_targets_data = {
    '8974' : {
        'masters' : {
            0   : '"APSS"',
            1   : '"MSS SW"',
            2   : '"PRONTO"',
        },
        'sleep_modes' : {
            0   :  '"XO Shutdown"',
            1   :  '"VDD Minimization"',
        },
        'mpm_ints' : {
        },
        'plls' : {
            1  : 'XO',
            2  : 'GPLL0',
            3  : 'GPLL1',
            4  : 'GPLL2',
            5  : 'GPLL3',
            6  : 'GPLL4',
            7  : 'GPLL5',
            8  : 'BIMC_PLL',
            9  : 'GPLL6',
        },
        'clock_power' : {
            0  : 'Enter',
            1  : 'Done'
        },
        'clock_bimc' : {
            0  : 'Enter',
            1  : 'Enter Self Refresh',
            2  : 'Enter Power Collapsed',
            3  : 'Done',
            4  : 'Set DDR',
            5  : 'DDR done',
            6  : 'Wait for power domain enable',
            7  : 'Wait for power domain disable',
            8  : 'Saving DEHR',
            9  : 'Restoring DEHR',
            10 : 'DEHR done',
        },
        'Voltages' : {
            0  : 'OFF         ',
            1  : 'RETENTION   ',
            3  : 'LOW         ',
            4  : 'NONIMAL     ',
            5  : 'NONIMAL_PLUS',
            6  : 'HIGH        ',
        },
        'icb_masters' : {
            3   : 'ICBID_MASTER_SNOC_BIMC_0',
            21  : 'ICBID_MASTER_BIMC_SNOC',
            29  : 'ICBID_MASTER_PNOC_SNOC',
            33  : 'ICBID_MASTER_SDCC_1',
            35  : 'ICBID_MASTER_SDCC_2',
            41  : 'ICBID_MASTER_BLSP_1',
            42  : 'ICBID_MASTER_USB_HS',
            76  : 'ICBID_MASTER_SNOC_BIMC_1',
            77  : 'ICBID_MASTER_SNOC_PCNOC',
            78  : 'ICBID_MASTER_AUDIO',
            85  : 'ICBID_MASTER_PCNOC_INT_0',
            86  : 'ICBID_MASTER_PCNOC_INT_1',
            87  : 'ICBID_MASTER_PCNOC_M_0',
            88  : 'ICBID_MASTER_PCNOC_M_1',
            89  : 'ICBID_MASTER_PCNOC_S_0',
            90  : 'ICBID_MASTER_PCNOC_S_1',
            91  : 'ICBID_MASTER_PCNOC_S_2',
            92  : 'ICBID_MASTER_PCNOC_S_3',
            96  : 'ICBID_MASTER_PCNOC_S_8',
            97  : 'ICBID_MASTER_PCNOC_S_9',
            99  : 'ICBID_MASTER_SNOC_INT_0',
            101 : 'ICBID_MASTER_SNOC_INT_BIMC',
        },
        'icb_slaves' : {
            0   : 'ICBID_SLAVE_EBI1',
            1   : 'ICBID_SLAVE_APPSS_L2',
            2   : 'ICBID_SLAVE_BIMC_SNOC',
            24  : 'ICBID_SLAVE_SNOC_BIMC_0',
            26  : 'ICBID_SLAVE_IMEM',
            28  : 'ICBID_SLAVE_SNOC_PCNOC',
            30  : 'ICBID_SLAVE_QDSS_STM',
            31  : 'ICBID_SLAVE_SDCC_1',
            33  : 'ICBID_SLAVE_SDCC_2',
            39  : 'ICBID_SLAVE_BLSP_1',
            40  : 'ICBID_SLAVE_USB_HS',
            41  : 'ICBID_SLAVE_PDM',
            45  : 'ICBID_SLAVE_PCNOC_SNOC',
            50  : 'ICBID_SLAVE_TCSR',
            51  : 'ICBID_SLAVE_TLMM',
            55  : 'ICBID_SLAVE_MESSAGE_RAM',
            59  : 'ICBID_SLAVE_PMIC_ARB',
            70  : 'ICBID_SLAVE_SNOC_CFG',
            104 : 'ICBID_SLAVE_SNOC_BIMC_1',
            105 : 'ICBID_SLAVE_AUDIO',
            114 : 'ICBID_SLAVE_PCNOC_INT_0',
            115 : 'ICBID_SLAVE_PCNOC_INT_1',
            116 : 'ICBID_SLAVE_PCNOC_M_0',
            117 : 'ICBID_SLAVE_PCNOC_M_1',
            118 : 'ICBID_SLAVE_PCNOC_S_0',
            119 : 'ICBID_SLAVE_PCNOC_S_1',
            120 : 'ICBID_SLAVE_PCNOC_S_2',
            121 : 'ICBID_SLAVE_PCNOC_S_3',
            125 : 'ICBID_SLAVE_PCNOC_S_8',
            126 : 'ICBID_SLAVE_PCNOC_S_9',
            130 : 'ICBID_SLAVE_SNOC_INT_0',
            132 : 'ICBID_SLAVE_SNOC_INT_BIMC',
        },
    },
}

names = {
    'clk0' : {
        0 : 'CX0',
        1 : 'QDSS',
        2 : 'dcvs.ena',
    },
    'clk1' : {
        0 : 'pcnoc',
        1 : 'snoc',
        2 : 'sysmmnoc',
    },
    'clk2' : {
        0 : 'bimc',
    },
    'clka' : {
        1  : 'BB clk1',
        2  : 'BB clk2',
        3  : 'not supported',
        4  : 'RF clk1',
        5  : 'RF clk2',
        6  : 'RF clk3',
        7  : 'DIFF clk',
        8  : 'not supported',
        9  : 'not supported',
        10  : 'SLEEP clk',
        11 : 'DIV clk1',
        12 : 'DIV clk2',
        13 : 'DIV clk3',
    },
    'bmas' : {}, # will be filled by select_target from all_targets_data
    'bslv'  : {}, # will be filled by select_target from all_targets_data
}

try:
    target_data
except NameError:
    target_data = None

def select_target(target_name):
    global target_data
    target_data = all_targets_data[target_name]
    try:
        names['bmas'] = target_data['icb_masters']
    except:
        names['bmas'] = {};
    
    try:
        names['bslv'] = target_data['icb_slaves']
    except:
        names['bslv'] = {};

def get_name_from_resource_id(name, id):
    out = ''
    try:
        out = names[name][id]
    except:
        out = 'Unknown'
    return out
    
def get_resource_name(resource):
    out = ''
    try:
        for c in reversed([hex(resource)[i:i+2] for i in xrange(2,10,2)]):
            out = out + chr(int(c, 16))
        return out;
    except:
        resource = resource << 8
        return get_resource_name(resource)

def get_master_name(master):
    return target_data['masters'].get(master, '"Unknown master %i"' % master)

def get_sleep_mode(mode):
    return target_data['sleep_modes'].get(mode, '"Unknown mode %i"' % mode)

def get_pll(pll):
    return target_data['plls'].get(pll, '"Unknown PLL %i"' % pll)

def get_clock_power(power):
    return target_data['clock_power'].get(power, '"Unknown power state %i"' % power)

def get_clock_bimc(bimc):
    return target_data['clock_bimc'].get(bimc, '"Unknown BIMC state %i"' % bimc)

def get_Voltage(Voltage):
    return target_data['Voltages'].get(Voltage, '"Unknown voltage level %i"' % Voltage)


def get_interrupt_name(interrupt):
    rpm_interrupt_ids = {
        0   : '"SPM Shutdown Handshake"',
        1   : '"SPM Bringup Handshake"',
    }
    return rpm_interrupt_ids.get(interrupt, '"Unknown interrupt %i"' % interrupt)

def get_set_name(set):
    rpm_set_ids = {
        0   : '"Active Set"',
        1   : '"Sleep Set"',
    }
    return rpm_set_ids.get(set, '"Unknown set %i"' % set)

def decode_bitfield(name, bit_definitions, data):
    known_bits = 0
    for id in bit_definitions:
        known_bits |= (1 << id)
    unknown_data = data - (data & known_bits)
    string = ' | '.join(['[' + bit_definitions[x] + ']' for x in bit_definitions if (1 << x) & data])
    if unknown_data:
        if string:
            string += ' ...and '
        multi_or_single = ''
        if log(unknown_data, 2) != int(log(unknown_data, 2)):
            multi_or_single = 's'
        string += 'unknown %s%s 0x%0.8x' % (name, multi_or_single, unknown_data)
    return string

def get_action_names(actions):
    rpm_action_ids = {
        0   : 'Request',
        1   : 'Notification',
    }
    return decode_bitfield('action', rpm_action_ids, actions)

def get_interrupt_names(interrupts):
    return decode_bitfield('interrupt', target_data['mpm_ints'], interrupts)

def get_icb_master_name(iden):
    return target_data['icb_masters'].get(iden, '"Unknown master %i"' % iden)

def get_icb_slave_name(iden):
    return target_data['icb_slaves'].get(iden, '"Unknown slave %i"' % iden)
