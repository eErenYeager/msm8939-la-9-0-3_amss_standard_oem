import sys, re, os
from base_parser import Parser

class PMICLdoStartApply:
	__metaclass__ = Parser
	id = 0x201
	def parse(self, data):
		if data[1] == 0:
			return 'START Apply() LDO%dA' % data[0]
		else:
			return 'START Apply() LDO%dB' % data[0]

class PMICLdoStartPreDep:
	__metaclass__ = Parser
	id = 0x202
	def parse(self, data):
		if data[1] == 0:
			return 'START Pre-Dep() LDO%dA' % data[0]
		else:
			return 'START Pre-Dep() LDO%dB' % data[0]

class PMICLdoEndPreDep:
	__metaclass__ = Parser
	id = 0x203
	def parse(self, data):
		if data[1] == 0:
			return 'END Pre-Dep() LDO%dA' % data[0]
		else:
			return 'END Pre-Dep() LDO%dB' % data[0]

class PMICLdoStartPostDep:
	__metaclass__ = Parser
	id = 0x204
	def parse(self, data):
		if data[1] == 0:
			return 'START Post-Dep() LDO%dA' % data[0]
		else:
			return 'START Post-Dep() LDO%dB' % data[0]

class PMICLdoEndPostDep:
	__metaclass__ = Parser
	id = 0x205
	def parse(self, data):
		if data[1] == 0:
			return 'END Post-Dep() LDO%dA' % data[0]
		else:
			return 'END Post-Dep() LDO%dB' % data[0]

class PMICLdoAggregation1:
	__metaclass__ = Parser
	id = 0x206
	def parse(self, data):
		if data[1] == 0:
			return ("START Execute_Driver()\n"
				"\t\tLDO%dA setting:\n"
				"\t\tsw_en = %d\n"
				"\t\tldo_sw_mode = %d") %\
				(data[0], data[2], data[3])
 		else:
			return ("START Execute_Driver()\n"
				"\t\tLDO%dB setting:\n"
				"\t\tsw_en = %d\n"
				"\t\tldo_sw_mode = %d") %\
				(data[0], data[2], data[3])

class PMICLdoAggregation2:
	__metaclass__ = Parser
	id = 0x207
	def parse(self, data):
		return ("\tpc_en = %d\n"
			"\t\tpc_mode = %d\n"
			"\t\tis_en_transition = %d\n"
			"\t\tip = %d") %\
			(data[0], data[1], data[2], data[3])

class PMICLdoAggregation3:
	__metaclass__ = Parser
	id = 0x208
	def parse(self, data):
		return ("\tregulated_uvol = %d\n"
			"\t\tbypass_uv = %d\n"
			"\t\tnoise_hr = %d\n"
			"\t\tbyp_allowed = %d") %\
			(data[0], data[1], data[2], data[3])

class PMICLdoAggregation4:
	__metaclass__ = Parser
	id = 0x209
	def parse(self, data):
		return ("\tis_en_transition = %d\n"
			"\t\tnoise_hr = %d\n"
			"\t\ten_byp = %d\n"
			"\t\tbypass_uv = %d") %\
			(data[0], data[1], data[2], data[3])

class PMICLdoAggregation5:
	__metaclass__ = Parser
	id = 0x20a
	def parse(self, data):
		return ("\tbyp_allowed = %d") % data[0]

class PMICLdoPowerMode:
	__metaclass__ = Parser
	id = 0x20b
	def parse(self, data):
		return {
			0: "Setting Mode: IPEAK - NPM",
			1: "Setting Mode: IPEAK - PFM",
			2: "Setting Mode: NPM"
				} [data[0]]

class PMICLdoNoChange:
	__metaclass__ = Parser
	id = 0x20c
	def parse(self, data):
		if data[1] == 0:
			return 'NO CHANGES DETECTED LDO%dA' % data[0]
		else:
			return 'NO CHANGES DETECTED LDO%dB' % data[0]

class PMICSmpsStartApply:
	__metaclass__ = Parser
	id = 0x20d
	def parse(self, data):
		if data[1] == 0:
			return 'START Apply() SMPS%dA' % data[0]	
		else:
			return 'START Apply() SMPS%dB' % data[0]

class PMICSmpsStartPreDep:
	__metaclass__ = Parser
	id = 0x20e
	def parse(self, data):
		if data[1] == 0:
			return 'START Pre-Dep() SMPS%dA' % data[0]
		else:
			return 'START Pre-Dep() SMPS%dB' % data[0]

class PMICSmpsEndPreDep:
	__metaclass__ = Parser
	id = 0x20f
	def parse(self, data):
		if data[1] == 0:
			return 'END Pre-Dep() SMPS%dA' % data[0]
		else:
			return 'END Pre-Dep() SMPS%dB' % data[0]

class PMICSmpsStartPostDep:
	__metaclass__ = Parser
	id = 0x210
	def parse(self, data):
		if data[1] == 0:
			return 'START Post-Dep() SMPS%dA' % data[0]
		else:
			return 'START Post-Dep() SMPS%dB' % data[0]

class PMICSmpsEndPostDep:
	__metaclass__ = Parser
	id = 0x211
	def parse(self, data):
		if data[1] == 0:
			return 'END Post-Dep() SMPS%dA' % data[0]
		else:
			return 'END Post-Dep() SMPS%dB' % data[0]

class PMICSmpsAggregation1:
	__metaclass__ = Parser
	id = 0x212
	def parse(self, data):
		if data[1] == 0:
			return ("START Execute_Driver()\n"
				"\t\tSMPS%dA setting:\n"
				"\t\tsw_en = %d\n"
				"\t\tsmps_sw_mode = %d") %\
				(data[0], data[2], data[3])
 		else:
			return ("START Execute_Driver()\n"
				"\t\tSMPS%dB setting:\n"
				"\t\tsw_en = %d\n"
				"\t\tsmps_sw_mode = %d") %\
				(data[0], data[2], data[3])

class PMICSmpsAggregation2:
	__metaclass__ = Parser
	id = 0x213
	def parse(self, data):
		return ("\tpc_en = %d\n"
			"\t\tpc_mode = %d\n"
			"\t\tglobal_byp_en = %d\n"
			"\t\tuvol = %d") %\
			(data[0], data[1], data[2], data[3])

class PMICSmpsAggregation3:
	__metaclass__ = Parser
	id = 0x214
	def parse(self, data):
		return ("\tip = %d\n"
			"\t\tfreq = %d\n"
			"\t\tfreq_reason = %d\n"
			"\t\tquiet_mode = %d") %\
			(data[0], data[1], data[2], data[3])

class PMICSmpsAggregation4:
	__metaclass__ = Parser
	id = 0x215
	def parse(self, data):
		return ("\tbyp_allowed = %d\n"
			"\t\thr = %d") %\
			(data[0], data[1])

class PMICSmpsPowerMode:
	__metaclass__ = Parser
	id = 0x216
	def parse(self, data):
		return {
			0: "Setting Mode: AUTO",
			1: "Setting Mode: IPEAK - NPM",
			2: "Setting Mode: IPEAK - LPM",
			3: "Setting Mode: NPM"
				} [data[0]]

class PMICSmpsNoChange:
	__metaclass__ = Parser
	id = 0x217
	def parse(self, data):
		if data[1] == 0:
			return 'NO CHANGES DETECTED SMPS%dA' % data[0]
		else:
			return 'NO CHANGES DETECTED SMPS%dB' % data[0]

class PMICVsStartApply:
	__metaclass__ = Parser
	id = 0x218
	def parse(self, data):
		if data[1] == 0:
			return 'START Apply() VS%dA' % data[0]
		else:
			return 'START Apply() VS%dB' % data[0]

class PMICVsStartPreDep:
	__metaclass__ = Parser
	id = 0x219
	def parse(self, data):
		if data[1] == 0:
			return 'START Pre-Dep() VS%dA' % data[0]
		else:
			return 'START Pre-Dep() VS%dB' % data[0]

class PMICVsEndPreDep:
	__metaclass__ = Parser
	id = 0x21a
	def parse(self, data):
		if data[1] == 0:
			return 'END Pre-Dep() VS%dA' % data[0]
		else:
			return 'END Pre-Dep() VS%dB' % data[0]


class PMICVsStartPostDep:
	__metaclass__ = Parser
	id = 0x21b
	def parse(self, data):
		if data[1] == 0:
			return 'START Post-Dep() VS%dA' % data[0]
		else:
			return 'START Post-Dep() VS%dB' % data[0]

class PMICVsEndPostDep:
	__metaclass__ = Parser
	id = 0x21c
	def parse(self, data):
		if data[1] == 0:
			return 'END Post-Dep() VS%dA' % data[0]
		else:
			return 'END Post-Dep() VS%dB' % data[0]

class PMICVsAggregation1:
	__metaclass__ = Parser
	id = 0x21d
	def parse(self, data):
		if data[1] == 0:
			return ("START Execute_Driver()\n"
				"\t\tVS%dA setting:\n"
				"\t\tsw_en = %d\n"
				"\t\tpc_en = %d") %\
				(data[0], data[2], data[3])
 		else:
			return ("START Execute_Driver()\n"
				"\t\tVS%dB setting:\n"
				"\t\tsw_en = %d\n"
				"\t\tpc_en = %d") %\
				(data[0], data[2], data[3])

class PMICVsAggregation2:
	__metaclass__ = Parser
	id = 0x21e
	def parse(self, data):
		return ("\tuvol = %d\n"
			"\t\tip = %d\n"
			"\t\tisInitialized = %d") %\
			(data[0], data[1], data[2])

class PMICVsNoChange:
	__metaclass__ = Parser
	id = 0x21f
	def parse(self, data):
		if data[1] == 0:
			return 'NO CHANGES DETECTED VS%dA' % data[0]
		else:
			return 'NO CHANGES DETECTED VS%dB' % data[0]

class PMICClkBufStartApply:
	__metaclass__ = Parser
	id = 0x220
	def parse(self, data):
		if data[1] == 0:
			return 'START Apply() CLK BUFFER%dA' % data[0]
		else:
			return 'START Apply() CLK BUFFER%dB' % data[0]

class PMICClkBufStartPreDep:
	__metaclass__ = Parser
	id = 0x221
	def parse(self, data):
		if data[1] == 0:
			return 'START Pre-Dep() CLK BUFFER%dA' % data[0]
		else:
			return 'START Pre-Dep() CLK BUFFER%dB' % data[0]

class PMICClkBufEndPreDep:
	__metaclass__ = Parser
	id = 0x222
	def parse(self, data):
		if data[1] == 0:
			return 'END Pre-Dep() CLK BUFFER%dA' % data[0]
		else:
			return 'END Pre-Dep() CLK BUFFER%dB' % data[0]

class PMICClkBufStartPostDep:
	__metaclass__ = Parser
	id = 0x223
	def parse(self, data):
		if data[1] == 0:
			return 'START Post-Dep() CLK BUFFER%dA' % data[0]
		else:
			return 'START Post-Dep() CLK BUFFER%dB' % data[0]

class PMICClkBufEndPostDep:
	__metaclass__ = Parser
	id = 0x224
	def parse(self, data):
		if data[1] == 0:
			return 'END Post-Dep() CLK BUFFER%dA' % data[0]
		else:
			return 'END Post-Dep() CLK BUFFER%dB' % data[0]

class PMICClkBufAggregation1:
	__metaclass__ = Parser
	id = 0x225
	def parse(self, data):
		if data[1] == 0:
			return ("START Execute_Driver()\n"
				"\t\tCLK BUFFER%dA setting:\n"
				"\t\tsw_en = %d\n"
				"\t\tpc_en = %d") %\
				(data[0], data[2], data[3])
 		else:
			return ("START Execute_Driver()\n"
				"\t\tCLK BUFFER%dB setting:\n"
				"\t\tsw_en = %d\n"
				"\t\tpc_en = %d") %\
				(data[0], data[2], data[3])

class PMICClkBufAggregation2:
	__metaclass__ = Parser
	id = 0x226
	def parse(self, data):
		return ("\tisInitialized = %d") % data[0]

class PMICClkBufNoChange:
	__metaclass__ = Parser
	id = 0x227
	def parse(self, data):
		if data[1] == 0:
			return 'NO CHANGES DETECTED CLK BUFFER%dA' % data[0]
		else:
			return 'NO CHANGES DETECTED CLK BUFFER%dB' % data[0]

class PMICSwEnableSetting:
	__metaclass__ = Parser
	id = 0x228
	def parse(self, data):
		return "\tWrote value to sw_en register: 0x%02x" % data[0]

class PMICSwEnableerror:
	__metaclass__ = Parser
	id = 0x229
	def parse(self, data):
		return {
			0: ('\tinternalResourceIndex out of range - '
			    'skipping writing mode setting to register'),
			1: ('\tmode is an invalid command - skipping '
			    'writing mode setting to register'),
			2: ('\tWriting value 0x00 to sw_en register failed '),
			3: ('\tWriting value 0x80 to sw_en register failed')
			}[data[0]]

class PMICVoltLevelSetting:
	__metaclass__ = Parser
	id = 0x22a
	def parse(self, data):
		return '\tWrote value to voltage level register: 0x%02x' %\
			data[0]

class PMICVoltRangeSetting:
	__metaclass__ = Parser
	id = 0x22b
	def parse(self, data):
		return '\tWrote value to voltage range register: 0x%02x' %\
			data[0]

class PMICVoltLevelError:
	__metaclass__ = Parser
	id = 0x22c
	def parse(self, data):
		return {	
			0: ("\tFeature not supported - skipping writing "
			    "range and level setting to register"),
			1: "\tWriting range setting failed - skipping "
			    "writing level setting to register",
			2: ("\tWriting level setting to register was "
			    "unsuccessful"),
			3: ("\tVoltage level out of range - skipping "
			    "writing range and level setting to register")
				} [data[0]]

class PMICPwrModeSetting:
	__metaclass__ = Parser
	id = 0x22d
	def parse(self, data):
		return '\tWrote value to power mode register: 0x%02x' %\
			data[0]

class PMICPwrModeError:
	__metaclass__ = Parser
	id = 0x22e
	def parse(self, data):
		return {
			0: ("\tinternalResourceIndex out of range - "
			    "skipping writing power mode to register"),
			1: ("\tSoftware mode is invalid - skipping "
			    "writing power mode to register"),
			2: ("\tWriting power mode to register was "
			    "unsuccessful")
				} [data[0]]

class PMICBoostStartApply:
	__metaclass__ = Parser
	id = 0x22f
	def parse(self, data):
		if data[1] == 0:
			return 'START Apply() BOOST%dA' % data[0]	
		else:
			return 'START Apply() BOOST%dB' % data[0]

class PMICBoostStartPreDep:
	__metaclass__ = Parser
	id = 0x230
	def parse(self, data):
		if data[1] == 0:
			return 'START Pre-Dep() BOOST%dA' % data[0]	
		else:
			return 'START Pre-Dep() BOOST%dB' % data[0]

class PMICBoostEndPreDep:
	__metaclass__ = Parser
	id = 0x231
	def parse(self, data):
		if data[1] == 0:
			return 'END Pre-Dep() BOOST%dA' % data[0]	
		else:
			return 'END Pre-Dep() BOOST%dB' % data[0]

class PMICBoostStartPostDep:
	__metaclass__ = Parser
	id = 0x232
	def parse(self, data):
		if data[1] == 0:
			return 'START Post-Dep() BOOST%dA' % data[0]	
		else:
			return 'START Post-Dep() BOOST%dB' % data[0]

class PMICBoostEndPostDep:
	__metaclass__ = Parser
	id = 0x233
	def parse(self, data):
		if data[1] == 0:
			return 'END Post-Dep() BOOST%dA' % data[0]	
		else:
			return 'END Post-Dep() BOOST%dB' % data[0]

class PMICBoostAggregation1:
	__metaclass__ = Parser
	id = 0x234
	def parse(self, data):
		return "\tsw_en = %d\n" %	data[0]

class PMICBoostNoChange:
	__metaclass__ = Parser
	id = 0x235
	def parse(self, data):
		if data[1] == 0:
			return 'NO CHANGES DETECTED BOOST%dA' % data[0]
		else:
			return 'NO CHANGES DETECTED BOOST%dB' % data[0]
