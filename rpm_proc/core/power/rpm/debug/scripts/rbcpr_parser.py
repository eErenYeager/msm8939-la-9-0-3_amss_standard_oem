import sys, re, os
from base_parser import Parser

class RBCPRPreSwitchEntry:
    __metaclass__ = Parser
    id = 0x29E
    def parse(self, data):
        return 'rbcpr_pre_swith_entry: (rail %d) (corner %d) (microvolts %d)' % (data[0], data[1], data[2])

class RBCPRPostSwitchEntry:
    __metaclass__ = Parser
    id = 0x29F
    def parse(self, data):
        return 'rbcpr_post_swith_entry: (rail %d) (corner %d) (microvolts %d)' % (data[0], data[1], data[2]])
