/*==============================================================================

FILE:      DalRPMFWSpmHandshake.c

DESCRIPTION: This file implements SPM Handshakes and state changes

PUBLIC CLASSES:  Not Applicable

INITIALIZATION AND SEQUENCING REQUIREMENTS:  N/A

      Copyright (c) 2010 Qualcomm Technologies Incorporated.
               All Rights Reserved.
         QUALCOMM Proprietary and Confidential


$Header: //components/rel/rpm.bf/2.0.c9/core/power/rpm/server/rpm_spm_handshake.cpp#1 $
$Date: 2015/03/13 $

==============================================================================*/

#define __STDC_LIMIT_MACROS

#include <stdlib.h>
#include <assert.h>
#include <limits.h>

extern "C"
{
    #include "DALSys.h"
    #include "cortex-m3.h"
}
#include "swevent.h"
#include "rpm.h"
#include "rpmserver.h"
#include "rpmserver_internal.h"
#include "CoreVerify.h"

#include "msmhwiobase.h"
#include "HALhwio.h"
#include "rpm_hwio.h"

#include "time_service.h"
#include "rpm_config.h"
#include "rpm_sched.h"
#include "rpm_spm_handshake.h"
#include "rpm_messageram.h"
#include "cortex-m3.h"
#include "saw2_hwio.h"

#ifdef __cplusplus
extern "C" {
#endif
void busywait (uint32 pause_time_us);
#ifdef __cplusplus
} // extern "C"
#endif

#ifdef MSM8936_APPS_SECURE_WDOG_WA_NEW
bool one_time = true;
typedef struct 
{
  uint32 apps_alias0_dbg_clk_on_req;
  uint32 apps_alias1_dbg_clk_on_req;
  uint32 apps_alias2_dbg_clk_on_req;
  uint32 apps_alias3_dbg_clk_on_req;
  uint32 apps_alias4_dbg_clk_on_req;
  uint32 apps_alias5_dbg_clk_on_req;
  uint32 apps_alias6_dbg_clk_on_req;
  uint32 apps_alias7_dbg_clk_on_req;
  
}apps_bimc_if_regs;

apps_bimc_if_regs apps_bimc_if_regs_ctx;


void rpm_apps_sec_wdog_wa_phase1(void)
{
    // 1. save state of bimc_apps AXI i/f regs
    apps_bimc_if_regs_ctx.apps_alias0_dbg_clk_on_req = HWIO_INF(APCS_ALIAS0_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ);
    apps_bimc_if_regs_ctx.apps_alias1_dbg_clk_on_req = HWIO_INF(APCS_ALIAS1_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ);
    apps_bimc_if_regs_ctx.apps_alias2_dbg_clk_on_req = HWIO_INF(APCS_ALIAS2_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ);
    apps_bimc_if_regs_ctx.apps_alias3_dbg_clk_on_req = HWIO_INF(APCS_ALIAS3_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ);
    apps_bimc_if_regs_ctx.apps_alias4_dbg_clk_on_req = HWIO_INF(APCS_ALIAS4_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ);
    apps_bimc_if_regs_ctx.apps_alias5_dbg_clk_on_req = HWIO_INF(APCS_ALIAS5_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ);
    apps_bimc_if_regs_ctx.apps_alias6_dbg_clk_on_req = HWIO_INF(APCS_ALIAS6_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ);
    apps_bimc_if_regs_ctx.apps_alias7_dbg_clk_on_req = HWIO_INF(APCS_ALIAS7_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ);
	
    // 2. clear bimc_apps AXI i/f regs 
    HWIO_OUTF(APCS_ALIAS0_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, 0);
    HWIO_OUTF(APCS_ALIAS1_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, 0);
    HWIO_OUTF(APCS_ALIAS2_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, 0);
    HWIO_OUTF(APCS_ALIAS3_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, 0);
    HWIO_OUTF(APCS_ALIAS4_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, 0);
    HWIO_OUTF(APCS_ALIAS5_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, 0);
    HWIO_OUTF(APCS_ALIAS6_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, 0);
    HWIO_OUTF(APCS_ALIAS7_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, 0);
	
    //3. Ensure that the BIMC-APSS AXI i/f clock branch to the BIMC is gated by polling for the following register state to be 1
    while(HWIO_INF(GCC_BIMC_APSS_AXI_CBCR, CLK_OFF ) != 1);
}

void rpm_apps_sec_wdog_wa_phase2(void)
{
    // 4. Wait for CCI SPM to assert the CCI reset
    while( HWIO_INF(CCI_SAW2_SPM_STS,CURR_PWR_CTL) & ( 0x80 ) != 0x80 ) {} 
   
    // 5. Restore the state of all the CSRs, cleared in step 1) above, thereby enabling back the BIMC-APSS AXI i/f clock branch to BIMC
    HWIO_OUTF(APCS_ALIAS0_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, apps_bimc_if_regs_ctx.apps_alias0_dbg_clk_on_req);
    HWIO_OUTF(APCS_ALIAS1_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, apps_bimc_if_regs_ctx.apps_alias1_dbg_clk_on_req);
    HWIO_OUTF(APCS_ALIAS2_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, apps_bimc_if_regs_ctx.apps_alias2_dbg_clk_on_req);
    HWIO_OUTF(APCS_ALIAS3_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, apps_bimc_if_regs_ctx.apps_alias3_dbg_clk_on_req);
    HWIO_OUTF(APCS_ALIAS4_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, apps_bimc_if_regs_ctx.apps_alias4_dbg_clk_on_req);
    HWIO_OUTF(APCS_ALIAS5_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, apps_bimc_if_regs_ctx.apps_alias5_dbg_clk_on_req);
    HWIO_OUTF(APCS_ALIAS6_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, apps_bimc_if_regs_ctx.apps_alias6_dbg_clk_on_req);
    HWIO_OUTF(APCS_ALIAS7_GCC_DBG_CLK_ON_REQ,APCS_GCCDBGPWRUPREQ, apps_bimc_if_regs_ctx.apps_alias7_dbg_clk_on_req);
	
	// 6. Generate the EVENT from RPM side to CCI SPM so that the SPM state machine can progress.
    if (one_time)
    {
        HWIO_OUTF(CCI_SAW_SPM_FORCE_EVENT_EN,EN,0x80);        // One time
	one_time = false;
    }

	// Assert the event
    HWIO_OUTF(CCI_SAW_SPM_FORCE_EVENT,EVENT_VAL,0x80);
#if 0 
    moving to shutdown senario
    // Wait for the event to get asserted. 
    // for(delay=0;delay<0x100;delay++); // dummy delay in VI. In SW Wait for delay equivalent to atleast 5 XO cycles
    busywait (2);

    // Deassert the event
    HWIO_OUTF(CCI_SAW_SPM_FORCE_EVENT,EVENT_VAL,0x00);
#endif
}

#endif
typedef struct
{
  uint8_t ee;
  uint8_t core;
} rpm_spm_isr_context_t;

void rpm_spm_shutdown_low_isr(void) __irq;
void rpm_spm_shutdown_high_isr(void) __irq;


static rpm_spm_isr_context_t   *spm_isr_context;

// Table of callbacks for SPM state changes.
// FIXME: in future revision, get the size of this table from target specific
//        code, since it's really target-dependent

#define SPM_CHANGE_STATE(new_state) \
{ \
    changed_state = TRUE; \
    rpm->ees[ee].subsystem_status = new_state; \
}

inline void rpm_acknowledge_spm_handshakes(unsigned ee)
{
    unsigned pending_bringups = __rbit(rpm->ees[ee].pending_bringups);

    unsigned core;
    while((core = __clz(pending_bringups)) != 32)
    {
        SWEVENT(RPM_BRINGUP_ACK, ee, core, rpm->ees[ee].is_soft_transition);
		#ifdef MSM8936_APPS_SECURE_WDOG_WA_NEW
        if(ee == 0)
	    {
            rpm_apps_sec_wdog_wa_phase1();
	    }
        #endif
        if (rpm->ees[ee].is_soft_transition)
        {
            //send bringup ACK message
            rpm->ees[ee].is_soft_transition = false;
            rpm->ees[ee].handler->writeResponse(SPM_BRINGUP_ACK, 0, RPM_COMMAND_SERVICE);
        }
        else
        {
            HWIO_OUT(RPM_GPO_WDSET, (1 << (rpm->ees[ee].spm.bringupAcks[core]))); // set bringup_ack
        }
        pending_bringups &= ~(0x80000000 >> core);
		#ifdef MSM8936_APPS_SECURE_WDOG_WA_NEW
        if(ee == 0)
	    {
	    rpm_apps_sec_wdog_wa_phase2();
	    }
        #endif
    }

    // Handled the known pending bringups.
    if (rpm->ees[ee].pending_bringups)
    {
      rpm->ees[ee].stats->bringup_ack = time_service_now ();
    }
    
    rpm->ees[ee].pending_bringups = 0;
}

bool rpm_get_wakeup_deadline(unsigned ee, uint64_t &deadline)
{
  vmpm_data_t *vmpm = message_ram_vmpm((vmpm_masters)ee);
  deadline = vmpm->wakeup_time;

  return (deadline < UINT64_MAX);
}

void rpm_spm_state_machine(unsigned ee, rpm_spm_entry_reason reason)
{
    INTLOCK();

    bool        changed_state = false;
    EEData     *ee_state      = &(rpm->ees[ee]);
    SetChanger *changer       = ee_state->changer;

    do
    {
        switch(ee_state->subsystem_status)
        {
            case SPM_AWAKE:
                changed_state = FALSE;

                if(0 == ee_state->num_active_cores)
                {
                    SPM_CHANGE_STATE(SPM_GOING_TO_SLEEP);
                }
                else
                {
                    // We're awake, so make sure we keep up with any incoming bringup reqs.
                    rpm_acknowledge_spm_handshakes(ee);
                }
                break;

            case SPM_GOING_TO_SLEEP:
                if(changed_state)
                {
                    // check for scheduled wakeup
                    uint64_t deadline = 0;

                    if(! rpm_get_wakeup_deadline(ee, deadline))
                    {
                      deadline = 0;
                    }
                    changer->setWakeTime (deadline);
                    // enqueue immediate set transition to sleep
                    changer->enqueue(RPM_SLEEP_SET, 0);
                }
                changed_state = FALSE;

                // When we've finished selecting the sleep set, we're officially asleep.
                if((SPM_TRANSITION_COMPLETE == reason) && (RPM_SLEEP_SET == changer->currentSet()))
                {
                  SPM_CHANGE_STATE(SPM_SLEEPING);
                }
                // However, we might get a wakeup request before we've made it all the way to sleep.
                if(SPM_BRINGUP_REQ == reason)
                {
                    // Set the preempt flag; this will force the set change to recycle if
                    // it's currently running.  It will notice the processor has woken up
                    // and stop performing its work.
                    theSchedule().preempt();
                }
                break;

            case SPM_SLEEPING:
                if(changed_state)
                {
                    // check for scheduled wakeup
                    uint64_t deadline = changer->getWakeTime ();
                    
                    // enqueue scheduled wakeup request
                    changer->enqueue(RPM_ACTIVE_SET, deadline);
                }
                changed_state = FALSE;

                if(ee_state->num_active_cores > 0)
                {
                    SPM_CHANGE_STATE(SPM_WAKING_UP);
                }
                break;

            case SPM_WAKING_UP:
                if(changed_state)
                {
                    // work our way back to the active set
                    if(RPM_SLEEP_SET == changer->currentSet() || changer->inTransition())
                    {
                        changer->enqueue(RPM_ACTIVE_SET, 0);
                    }
                }
                changed_state = FALSE;

                // check for completion
                if(RPM_ACTIVE_SET == changer->currentSet() && !changer->inTransition())
                {
                  SPM_CHANGE_STATE(SPM_AWAKE);
                }
                break;
        }
    } while(changed_state);

    INTFREE();
}

static void rpm_spm_bringup_isr(void) __irq
{
    INTLOCK();

    unsigned isrNum = interrupt_current_isr();
    unsigned ee     = spm_isr_context[isrNum].ee;
    unsigned core   = spm_isr_context[isrNum].core;

    EEData               *ee_state = &(rpm->ees[ee]);
    const SPMDescription *spm      = &(ee_state->spm);

    CORE_VERIFY(ee < 0xFF);

    SWEVENT(RPM_BRINGUP_REQ, ee, core, ee_state->is_soft_transition);

    // Record this core as being active now.
    ee_state->num_active_cores++;
    ee_state->stats->active_cores |= (1 << core);
    ee_state->stats->bringup_req = time_service_now ();
    
    // We can't actually acknowledge the bringup here, because we haven't brought
    // the master out of its sleep set.
    ee_state->pending_bringups |= (1 << core);

    // Disable bringup_req as an interrupt, as the signal will stay high.
    interrupt_disable(spm->bringupInts[core]);
    
    // Kick off whatever other action needs to happen due to this event.
    rpm_spm_state_machine(ee, SPM_BRINGUP_REQ);

    INTFREE();
}

static void rpm_spm_shutdown_high_isr(void) __irq
{
    INTLOCK();

    unsigned isrNum = interrupt_current_isr();
    unsigned ee     = spm_isr_context[isrNum].ee;
    unsigned core   = spm_isr_context[isrNum].core;
    unsigned active_cores;
    
    EEData               *ee_state = &(rpm->ees[ee]);
    const SPMDescription *spm      = &(ee_state->spm);

    CORE_VERIFY(ee < 0xFF);
#ifdef MSM8936_APPS_SECURE_WDOG_WA_NEW
    if(ee == 0) 
    {
        // Deassert the event
        HWIO_OUTF(CCI_SAW_SPM_FORCE_EVENT,EVENT_VAL,0x00);
    }
#endif
    // A core turned off, so record that.
    ee_state->num_active_cores--;
    active_cores = ee_state->stats->active_cores;
    active_cores &= ~(1 << core);
    ee_state->stats->active_cores = active_cores;
    if (ee_state->num_active_cores == 0)
    {
      ee_state->stats->num_shutdowns ++;
      ee_state->stats->shutdown_req = time_service_now ();
    }
    SWEVENT(RPM_SHUTDOWN_REQ, ee, core, ee_state->is_soft_transition);

    // Acknowledge the shutdown by clearing bringup ack and setting shutdown ack.
    if(!ee_state->is_soft_transition)
    {
        HWIO_OUT(RPM_GPO_WDCLR, (1 << (spm->bringupAcks[core])));  // clear bringup_ack
        HWIO_OUT(RPM_GPO_WDSET, (1 << (spm->shutdownAcks[core]))); // set shutdown_ack
    }
    else
    {
        //send soft ACK
        rpm->ees[ee].handler->writeResponse(SPM_SHUTDOWN_ACK, 0, RPM_COMMAND_SERVICE);
    }

    // Wait for the shutdown to go through.
    interrupt_set_isr(spm->shutdownInts[core], rpm_spm_shutdown_low_isr);
    interrupt_configure(spm->shutdownInts[core], LEVEL_LOW);
    
    INTFREE();
}

static void rpm_spm_shutdown_low_isr(void) __irq
{
    INTLOCK();

    unsigned isrNum = interrupt_current_isr();
    unsigned ee     = spm_isr_context[isrNum].ee;
    unsigned core   = spm_isr_context[isrNum].core;

    EEData               *ee_state = &(rpm->ees[ee]);
    const SPMDescription *spm      = &(ee_state->spm);

    CORE_VERIFY(ee < 0xFF);

    SWEVENT(RPM_SHUTDOWN_ACK, ee, core, ee_state->is_soft_transition);

    // We're shutdown, switch over to sleep mode.
    if(!ee_state->is_soft_transition)
    {
        HWIO_OUT(RPM_GPO_WDCLR, (1 << (spm->shutdownAcks[core]))); // clear shutdown_ack
        interrupt_enable(spm->bringupInts[core]); // enable bringup_req ISR
    }

    // Reset shutdown_req monitoring for after the wakeup.
    interrupt_configure(spm->shutdownInts[core], LEVEL_HIGH);
    interrupt_set_isr(spm->shutdownInts[core], rpm_spm_shutdown_high_isr);

    // Kick off whatever other action needs to happen due to this event.
    rpm_spm_state_machine(ee, SPM_SHUTDOWN_REQ);

    INTFREE();
}

void rpm_spm_soft_transition( unsigned ee, rpm_spm_entry_reason reason)
{
  EEData *ee_state              = &(rpm->ees[ee]);
  const SPMDescription *spm     = &(ee_state->spm);
  
  //set flag, soft trigger interrupt
  ee_state->is_soft_transition = true;
  switch(reason)
  {
      case SPM_SHUTDOWN_REQ:
          interrupt_soft_trigger(spm->shutdownInts[0]);
          break;
      case SPM_BRINGUP_REQ:
          interrupt_soft_trigger(spm->bringupInts[0]);
          interrupt_enable(spm->bringupInts[0]); // enable bringup_req ISR
          break;

      default:
          abort();
  }    
}

void rpm_spm_init(void)
{
    unsigned num_ees = rpm->num_ees;

    unsigned highestISR = 0;
    for(unsigned i = 0; i < num_ees; ++i)
    {
        EEData               *ee       = &(rpm->ees[i]);
        const SPMDescription *spm      = &(ee->spm);
        unsigned              numCores = spm->numCores;
        unsigned active_cores = 0;
        
        // All cores start "awake."
        ee->num_active_cores = numCores;
        ee->pending_bringups = 0;
        ee->subsystem_status = SPM_AWAKE;
        ee->is_soft_transition = false;
        
        for(unsigned core = 0; core < numCores; ++core)
        {
            unsigned bringupISR = spm->bringupInts[core];
            if(bringupISR > highestISR) highestISR = bringupISR;
            interrupt_configure(bringupISR, LEVEL_HIGH);
            interrupt_set_isr(bringupISR, rpm_spm_bringup_isr);

            unsigned shutdownISR = spm->shutdownInts[core];
            if(shutdownISR > highestISR) highestISR = shutdownISR;
            interrupt_configure(shutdownISR, LEVEL_HIGH);
            interrupt_set_isr(shutdownISR, rpm_spm_shutdown_high_isr);
            interrupt_enable(shutdownISR);

            active_cores |= (1 << core);
            ee->stats->active_cores = active_cores;
        }
    }

    // Allocate context blocks for each ISR.
    ++highestISR;
    spm_isr_context = new rpm_spm_isr_context_t[highestISR];
    memset(spm_isr_context, 0xFF, highestISR * sizeof(rpm_spm_isr_context_t));
    for(unsigned i = 0; i < num_ees; ++i)
    {
        EEData               *ee       = &(rpm->ees[i]);
        const SPMDescription *spm      = &(ee->spm);
        unsigned              numCores = spm->numCores;
        
        for(unsigned core = 0; core < numCores; ++core)
        {
            spm_isr_context[spm->shutdownInts[core]].ee = i;
            spm_isr_context[spm->shutdownInts[core]].core = core;

            spm_isr_context[spm->bringupInts[core]].ee = i;
            spm_isr_context[spm->bringupInts[core]].core = core;
        }
    }
}

void rpm_spm_trigger_wakeup_int(unsigned ee)
{
    if(!rpm->ees[ee].is_soft_transition)
    {
        unsigned wakeupInt = rpm->ees[ee].wakeupInt;
        HWIO_OUT(RPM_IPC, wakeupInt);
    }
    else
    {
        rpm_spm_soft_transition(ee, SPM_BRINGUP_REQ);
    }
}

