/*=============================================================================

                                MAIN

GENERAL DESCRIPTION
  This file contains the initial operations for the RPM.

EXTERNALIZED FUNCTIONS

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None

Copyright (c) 2009-2011 by Qualcomm Technologies, Inc.  All Rights Reserved.

  $Header: //components/rel/rpm.bf/2.0.c9/core/bsp/rpm/src/main.c#1 $
  $Author: pwbldsvc $
  $DateTime: 2015/03/13 04:27:04 $
=============================================================================*/

#include <assert.h>
#include "comdef.h"

#include "cortex-m3.h"
#include "npa_init.h"
#include "comdef.h"
#include "smem.h"
#include "smd_lite.h"
#include "rpmserver.h"
#include "mpm.h"
#include "vmpm.h"
#include "dog.h"
#include "time_service.h"
#include "QDSSLite.h"
#include "icb_rpm.h"
#include "swevent.h"
#include "railway.h"
#include "svs.h"
#include "kvp.h"
#include "ddr_drivers.h"
#include "system_db_rpm.h"
#include "rbcpr_comdef.h"
#include "rpm_settling_timer.h"
#include "sleep_v.h"
#include "HALhwio.h"
#include "hw_version.h"
#include "coredump.h"
#include "Clock.h"
#include "PlatformInfo.h"
#include "version.h"
#include "timetick.h"
#include "version.h"
#include "image_layout.h"
#include "pm_err_flags.h"
#include "pm_version.h"
#include "pmapp_npa.h"

#if defined(MSM8909_STUBS) 
void pm_init(void){}
void ddr_init(void){}
boolean Clock_Init(void){return TRUE;}
void Clock_LogState ( void ){}
DALResult Clock_IsClockOn (uint32   nClock,  boolean *pbIsOn ){ return 1; }
unsigned pm_railway_set_voltage(pm_railway_type_info_type rail, unsigned voltage_uv){return 1;}
uint32 pm_clk_sleep_src_status(uint8 pmic_chip, uint32 clk_type, uint32* on_off){ return 1;}
uint32 pm_clk_xo_set_warmup_time(uint8 pmic_chip, uint32 clk_type, uint32 timer_value) {return 1;}
DALResult Clock_EnableClock (  uint32  nClock) {return 1;}
DALResult Clock_DisableClock (  uint32  nClock) {return 1;}
DALResult Clock_GetClockId ( const char   *szClock,  uint32 *pnId ) {return 1;}
boolean Clock_IsQDSSOn(void) {return TRUE; }
DALResult Clock_GetPowerDomainId (  const char    *szPowerDomain, uint32 *pnPowerDomainId ) { return 1;}
DALResult Clock_ProcessorRestore (  ClockSleepModeType  eMode,  uint32 nFlags) {return 1;}
DALResult Clock_ProcessorSleep ( ClockSleepModeType  eMode,  uint32              nFlags ) {return 1;}
void pm_npa_rpm_exit_sleep (void) {}
void pm_npa_rpm_enter_sleep (void) {}
unsigned pm_railway_set_mode(pm_railway_type_info_type rail, pm_sw_mode_type  mode){return 1;}
pm_err_flag_type pm_get_pmic_info(uint8 pmic_device_index, pm_device_info_type* pmic_device_info){return PM_ERR_FLAG__SUCCESS;}
pm_err_flag_type pm_railway_calculate_vset(pm_railway_type_info_type rail , unsigned voltage_uv ,uint32*  vset){ return PM_ERR_FLAG__SUCCESS;}
void acc_init(void){}
void icb_init(void) {}

#endif

#if defined(MDM9x45_STUBS)
void ddr_init(void){}
boolean Clock_Init(void){return TRUE;}
DALResult Clock_EnableClock (  uint32  nClock) {return 1;}
DALResult Clock_DisableClock (  uint32  nClock) {return 1;}
DALResult Clock_GetClockId ( const char   *szClock,  uint32 *pnId ) {return 1;}
boolean Clock_IsQDSSOn(void) {return TRUE; }
DALResult Clock_GetPowerDomainId (  const char    *szPowerDomain, uint32 *pnPowerDomainId ) { return 1;}
DALResult Clock_ProcessorRestore (  ClockSleepModeType  eMode,  uint32 nFlags) {return 1;}
DALResult Clock_ProcessorSleep ( ClockSleepModeType  eMode,  uint32              nFlags ) {return 1;}
void icb_init(void) {}
void Clock_LogState ( void ){}
DALResult Clock_IsClockOn (uint32   nClock,  boolean *pbIsOn ){ return 1; }
#endif

#ifdef MSM8929_STUBS
DALResult Clock_EnableClock (  uint32  nClock) {return 1;}
DALResult Clock_GetClockId ( const char   *szClock,  uint32 *pnId ) {return 1;}
void icb_init(void) {}
boolean Clock_Init (void){return 0;}
boolean Clock_IsQDSSOn(void){return 0;}
DALResult Clock_GetPowerDomainId( const char * szPowerDomain,  ClockPowerDomainIdType * pnPowerDomainId){return 0;}
DALResult Clock_ProcessorRestore( ClockSleepModeType  eMode,  uint32  nFlags){return 0;}
DALResult Clock_ProcessorSleep( ClockSleepModeType  eMode,  uint32  nFlags){return 0;}
void Clock_LogState ( void ){}
DALResult Clock_IsClockOn(  ClockIdType  nClock,  boolean * pbIsOn){return 0;}
#endif

//prng stack protection init
void __init_stack_chk_guard(void);

void xpu_init(void);
void time_service_init(void);
void sched_run(void);
uint64_t sched_get_next_start(void);
unsigned int npa_num_pending_events(void);
void npa_process_event(void);
void exceptions_init(void);
void exceptions_enable(void);
void pm_init(void);
void debug_init(void);
void swevent_init(void);
void swevent_qdss_init(void);
void prevent_further_heap_frees(void);
void busywait_init(void);
void rpm_settling_timer_init(void);

void gpio_toggle_init(void);

//Get rid of this once we're back to one railway.
void railway_init_v2(void);
void railway_init_early_proxy_votes(void);
void populate_image_header(void);
void zqcal_task_init(void);
void acc_init(void);
void debug_cookie_init(void);
void rpm_init_done(void);
void rpm_server_init_done(void);

#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))

typedef void (*init_fcn)(void);


static void init_smdl(void) { smdl_init(0); }


const init_fcn init_fcns[] =
{
  populate_image_header,
  npa_init,

#if (!defined(MSM8909_STUBS) )
  railway_init_v2,
#endif
  PlatformInfo_Init, /* pm_init is using PlatformInfo APIs */
  pm_init,
  acc_init,

#if (!defined(MSM8909_STUBS) )
  railway_init_early_proxy_votes,
 #endif
  // xpu_init, /* cookie set here also indicates to SBL that railway is ready */
  (init_fcn)Clock_Init,
  __init_stack_chk_guard,
  ddr_init,
  smem_init,
  init_smdl,
  version_init,     /* Needs to be after smem_init */
  rpmserver_init,
  rpm_server_init_done,
  railway_init_proxies_and_pins,
 
#if (!defined(MSM8909_STUBS) )
  rbcpr_init,
#endif  
  svs_init,
  vmpm_init,
  sleep_init,

#if (!defined(MSM8909_STUBS) )
  QDSSInit,
 #endif
  exceptions_enable,
  swevent_qdss_init,
  icb_init,

#if (!defined(MSM8909_STUBS) )
  debug_init,
  system_db_init,
  zqcal_task_init,
#endif
  rpm_settling_timer_init,
  gpio_toggle_init,
  rpm_set_changer_common_init,
  
};

#define DOG_BARK_DURING_BOOT  0x1000// ~128ms
#define DOG_BITE_DURING_BOOT  0x1500// ~168 ms

#define DOG_BARK_AFTER_BOOT 0x400 // ~31.2 ms
#define DOG_BITE_AFTER_BOOT 0x800 // ~62.5 ms

#define WP_ONLINE_DUMP_TZ_IRQ 12

static void init_time_proxy_votes(void)
{
#if ((!defined(MDM9x45_STUBS)) && (!defined(MSM8909_STUBS)))

  kvp_t *kvp;
  uint32 req = 1;
  static unsigned ENABLED_KEY = 0x62616e45;

  //init time request for CXO on behalf of apps
  kvp = kvp_create(0);
  kvp_clear(kvp);
  kvp_put(kvp, ENABLED_KEY, sizeof(req), &req);
  rpm_send_init_proxy_vote(RPM_CLOCK_0_REQ, /*CXO is CLK0*/ 0, /*Apps is master 0*/ 0, kvp);

#if 0
  kvp_clear(kvp);
  // uvol = 1800000
  req = 1200000;
  kvp_put(kvp, 0x7675, sizeof(uint32), &req);
 rpm_send_init_proxy_vote(RPM_SMPS_A_REQ, 1, 0, kvp);
#endif

  // proxy vote for L6 on behalf of Apps
  kvp_clear(kvp);
  // swen = 1
  req = 1;
  kvp_put(kvp, 0x6E657773, sizeof(uint32), &req);
  // ma = 50
  req = 50;
  kvp_put(kvp, 0x616D, sizeof(uint32), &req);
  // uvol = 1800000
  req = 1800000;
  kvp_put(kvp, 0x7675, sizeof(uint32), &req);

  rpm_send_init_proxy_vote(RPM_LDO_A_REQ, 6, 0, kvp);

  kvp_destroy(kvp);
#endif
}

int main(void)
{
  unsigned i;

  debug_cookie_init();
  
  exceptions_init();
  busywait_init();
  timetick_init();
  dog_init();

  dog_set_bite_time(DOG_BITE_DURING_BOOT);
  dog_set_bark_time(DOG_BARK_DURING_BOOT);

  rpm_core_dump.hw_version.v = HWIO_IN(TCSR_SOC_HW_VERSION);
#if 0
  //MsgRAM clock are already setup by SBL
  //GCC_RPM_CLOCK_BRANCH_ENA_VOTE : 0x01800000 + 0x00036004

  // We need to have the clock to message RAM turned on *before* we intiailize
  // the rpmserver driver.  This is *before* we initialize the clock driver,
  // so we have to just do it ourselves.
  volatile uint32_t *RPM_CLOCK_BRANCH_ENA_VOTE = (volatile uint32_t *)0xFC401444;
  *RPM_CLOCK_BRANCH_ENA_VOTE |= 0x200; // sets the vote bit for msg_ram_hclk
#endif
  mpm_init();
  time_service_init();
  swevent_init();

  SWEVENT(RPM_BOOT_STARTED);

  for(i = 0; i < ARRAY_SIZE(init_fcns); ++i)
  {
    dog_kick();
    init_fcns[i]();
  }

  dog_set_bark_time(DOG_BARK_AFTER_BOOT);
  dog_set_bite_time(DOG_BITE_AFTER_BOOT);

  init_time_proxy_votes();

  prevent_further_heap_frees();
  SWEVENT(RPM_BOOT_FINISHED);

  rpm_init_done();
  
  // enable interrupt from TZ to stop RPM to collect online dumps
  interrupt_enable(WP_ONLINE_DUMP_TZ_IRQ);
  
  #define ever ;;
  for(ever)
  {
    assert(!intlock_nest_level);
    do
    {
      sched_run();
      assert(!intlock_nest_level);

      if(npa_num_pending_events() > 0)
      {
        npa_process_event();
        assert(!intlock_nest_level);
      }
      else
        break; // nothing to do--try to sleep
    } while(1);

    assert(!intlock_nest_level);
    INTLOCK();
    if(!npa_num_pending_events() && (sched_get_next_start() >= time_service_now()))
      sleep_perform_lpm();
    INTFREE();
    assert(!intlock_nest_level);
  }
}

