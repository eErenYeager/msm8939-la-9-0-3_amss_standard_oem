#  ===========================================================================
#
#  Copyright (c) 2011 Qualcomm Technologies Incorporated.  
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ===========================================================================


import argparse
import glob
import itertools
import sys
import os
import logging
from hansei_utils import print_p

dump_file_types = [
    #
    # Core stuff, most important.
    #
    {   # The place all of the code lives
        'name'     : {'rpm_code_ram.bin', 'CODERAM.BIN', 'RPM0.bin'},
        'ext'      : {'.BIN', '.LST'},
        'basename' : {'RPM_CODE_RAM', 'CODERAM', 'RPM0'},
        'size'     : 131072,
        'required' : True,
        'base'     : 0x00000,
    },
    {   # The place all of the data lives
        'name'     : {'rpm_data_ram.bin', 'DATARAM.BIN', 'RPM1.bin'},
        'ext'      : {'.BIN', '.LST'},
        'basename' : {'RPM_DATA_RAM', 'DATARAM', 'RPM1'},
        'size'     : 65536,
        'required' : True,
        'base'     : 0x90000,
    },
    {   # The place all of IPC messages live
        'name'     : {'rpm_msg_ram.bin', 'MSGRAM.BIN', 'RPM2.bin'},
        'ext'      : {'.BIN', '.LST'},
        'basename' : {'RPM_MSG_RAM', 'MSGRAM', 'RPM2'},
        'size'     : 20480,
        'required' : True, # really?  maybe not strictly required
        'base'     : 0x60060000,
    },
    #{   # The values of RPM registers. Only consulted if there was no core dump.
    #    'name'     : 'rpm_registers.cmm',
    #    # FIXME: need some parser here to make this file useful...
    #},

    #
    # Generic RPM peripherals, less important
    #
    {   # RPM's generic Cortex-M3 hardware.
        'name'     : 'rpm_scs.bin',
        'ext'      : {''},
        'basename' : {''},
        'size'     : 0,
        'base'     : 0xE000E000,
    },
    {   # RPM's Qualcomm-written hardware.
        'name'     : 'rpm__dec.bin',
        'ext'      : {''},
        'basename' : {''},
        'size'     : 0,
        'base'     : 0x80000,
    },
    {   # RPM's timer-related hardware (next 3)
        'name'     : 'rpm__qtmr_ac.bin',
        'ext'      : {''},
        'basename' : {''},
        'size'     : 0,
        'base'     : 0x82000,
    },
    {
        'name'     : 'rpm__qtmr_f0.bin',
        'ext'      : {''},
        'basename' : {''},
        'size'     : 0,
        'base'     : 0x83000,
    },
    {
        'name'     : 'rpm__qtmr_f1.bin',
        'ext'      : {''},
        'basename' : {''},
        'size'     : 0,
        'base'     : 0x84000,
    },
    {   # RPM code RAM access control unit.
        'name'     : 'rpm__apu.bin',
        'ext'      : {''},
        'basename' : {''},
        'size'     : 0,
        'base'     : 0x87000,
    },
    #
    # System peripherals, vaguely RPM related.
    #
    {   # The bits of the MPM that control low power states.
        'name'     : 'mpm.bin',
        'ext'      : {''},
        'basename' : {''},
        'size'     : 0,
        'base'     : 0x4A0000,
    },
    {   # Some parts of the TCSR.
        'name'     : 'tcsr_regs.bin',
        'ext'      : {''},
        'basename' : {''},
        'size'     : 0,
        'base'     : 0x1937300,
    },
]

'''
Parse arguments
'''

parser = argparse.ArgumentParser(description='Generate introspective RPM logs.')
parser.add_argument('files', metavar='dumpfile', nargs='+', type=str, help='dumped ram files (*.bin, *.lst)')
parser.add_argument('--elf', metavar='rpm.elf', default='', help='.elf with (at least closely) matching symbols. Defaults to elf in current build')
parser.add_argument('--output', '-o', metavar='logs_path', default='./output', help='location to place output logs')
parser.add_argument('--verbose', '-v', action='count', default=0, help='verbosity (more v\'s == more messages)')
parser.add_argument('--parser', '-p', metavar='parse_path', default='', help='absolute path to folder containing rpm_log_bfam.py')
parser.add_argument('--parser_rel', '-r', action='count', default=0, help='notes that the path given for the --parser argument is a relative path to folder containing rpm_log_bfam.py from folder containing hansei.py')

args = parser.parse_args(sys.argv[1:])

print('#------------------------------------------------------------------------------#')
print('#                            Welcome to RPM Hansei                             #')
print('#------------------------------------------------------------------------------#')
print_p('...Checking for required tools:')
try:
    error_msg = '\tERROR: Failed to find pyelftools. Please ensure you have this package.    '
    from elftools.elf.elffile import ELFFile
    print_p('\tpyelftools... found')
except:
    print_p(error_msg)
    print('#------------------------------------------------------------------------------#')
print('#------------------------------------------------------------------------------#')

from memory import Memory
from dwarf import DebugInfo, decode_object


temp_files = [f if not os.path.isdir(f) else os.path.join(f, '*.bin') for f in args.files] # if a directory is specified, look for dumps *in* it
#hack for when .lst files are given
args.files = temp_files + [f if not os.path.isdir(f) else os.path.join(f, '*.lst') for f in args.files]
args.files = set(itertools.chain(*[glob.glob(f) for f in args.files]))

if not os.path.exists(args.output):
    os.makedirs(args.output)

verbosity = [logging.ERROR, logging.WARNING, logging.INFO, logging.DEBUG]
logging.basicConfig(format='%(levelname)-8s | %(name)-20s | %(message)s', level=verbosity[min(len(verbosity)-1, args.verbose)])
logger = logging.getLogger(__name__)

print_p("\tReading dump files...")
required = {dft['base'] for dft in dump_file_types if dft.get('required')}
#print_p("\tREQUIRED ..%s"  % (required))
system = Memory()
read_bases = {0}
for dumpfile in args.files:
    dfn = os.path.basename(dumpfile)
    dfs = os.path.getsize(dumpfile)
    dfe = os.path.splitext(dumpfile)[1]
    dfne = os.path.basename(os.path.splitext(dumpfile)[0])
    #import pdb; pdb.set_trace()
    
    for dft in dump_file_types:
        #try for known names
        if dfn in dft['name']:
            logger.debug("Reading %s into 0x%0.8x" % (dumpfile, dft['base']))
            with open(dumpfile, 'rb') as data:
                if dft['base'] or dft['base']== 0x0 :
                    read_bases.add(dft['base'])
                    system.load_hunk(dft['base'], data.read())
                    required -= {dft['base']}
                    
        # try for combinations of known base names and extensions
        elif (dfe.upper() in dft['ext']) and (dfne.upper() in dft['basename']):
            logger.debug("Reading %s into 0x%0.8x" % (dumpfile, dft['base']))
            with open(dumpfile, 'rb') as data:
                if dft['base'] not in read_bases:
                    read_bases.add(dft['base'])
                    system.load_hunk(dft['base'], data.read())
                    required -= {dft['base']}
        '''   
        # try for known size -- some other files can have the same size as core rpm dumps, this is an issue
        elif dfs == dft['size']:
            logger.debug("Reading %s into 0x%0.8x" % (dumpfile, dft['base']))
            with open(dumpfile, 'rb') as data:
                print("\t%s -- checking if already removed[size]" % (dfn))
                if dft['base'] not in read_bases:
                    read_bases.add(dft['base'])
                    print("\t\tAdded to read_bases")
                    system.load_hunk(dft['base'], data.read())
                    print("\t\tloaded")
                    required -= {dft['base']}
                    print("\t\tremoved from required")
                    print("\t%s -- READ[size]" % (dfn))
                    print(read_bases)
        '''

if required:
    print_p("ERROR FATAL: Did not find all required RAM dumps!")
    print "\t(Missing dumps for regions starting at [%s].)" % (' '.join(hex(base) for base in required))
    print_p("ABORTING")
    print('#------------------------------------------------------------------------------#')
    sys.exit(-1)
    
print('#------------------------------------------------------------------------------#')
'''
Find and read ELF file
'''
if args.elf == '':
    elf_dir = os.path.realpath(__file__).split('rpm_proc')[0]+'rpm_proc\\core\\bsp\\rpm\\build'
    elf_file = '';
    for file in os.listdir(elf_dir):
        if file.endswith(".elf"):
            elf_file = file
    if elf_file == '':
        args.elf = elf_dir+"\\"+elf_file

if args.elf == '':
    print_p("ERROR FATAL: No .elf file given and no .elf file found in current build... Aborting")
    print('#------------------------------------------------------------------------------#')
    sys.exit(-1)

print_p('\tReading ELF...')
di = DebugInfo(args.elf)

print('#------------------------------------------------------------------------------#')
'''
Parse dumps
'''
print_p('\tDumping summary...')
import dumpers.summary as summary
summary.dump(args.output, system, di)

'''
Find ulog parsers
'''
if (args.parser != '' and args.parser_rel != 0):
    args.parser = os.path.realpath(__file__).split(os.path.basename(__file__))[0]+args.parser
if (args.parser == ''):
    args.parser = os.path.realpath(__file__).split('rpm_proc')[0]+'rpm_proc/core/power/rpm/debug/scripts/'
    #try server location if build path doesn't work
    if (os.path.isfile(args.parser+'rpm_log_bfam.py')==False):
        args.parser=os.path.realpath(__file__).split(os.path.basename(__file__))[0]+'../bfam_log_parser/'
    if (os.path.isfile(args.parser+'rpm_log_bfam.py')==False):
        print_p('\t\t-Failed to find ulog parser, please specifiy location with --parser(-p).')
        print_p('\t\t\tFor path relative to location of hansei.py, also include -r.')
if (args.parser[len(args.parser)-1] != '/'):
    args.parser = args.parser+'/'

print_p('\tDumping ulogs...')
import dumpers.rpm_ulog as rpm_ulog
rpm_ulog.dump(args.output, args.parser, system, di)

print_p('\tDumping NPA state...')
import dumpers.npa as npa
npa.dump(args.output, system, di)

print_p('\tDumping railway state...')
import dumpers.railway as railway
railway.dump(args.output, system, di)

print_p('\tDumping mpm registers...')
import dumpers.mpm as mpm
mpm.dump(args.output, system, di)

print_p('\tDumping sleep stats...')
import dumpers.sleep_stats as sleep_stats
sleep_stats.dump(args.output, system, di)

print_p('\tDumping RPM server state...')
import dumpers.rpmserver as rpmserver
rpmserver.dump(args.output, system, di)

print_p('\tDumping watchdog events...')

#this is really just a hack to get SDI data loaded for PMIC wdog stuff
for dumpfile in args.files:
    dfn = os.path.basename(dumpfile)
    #import pdb; pdb.set_trace()
    
    if dfn in ['OCIMEM.BIN']:
        logger.debug("Reading %s into 0x%0.8x" % (dumpfile, 0xb0000000))
        with open(dumpfile, 'rb') as data:
            system.load_hunk(0xb0000000, data.read())

import dumpers.wdogs as wdogs
wdogs.dump(args.output, system, di)

print('#------------------------------------------------------------------------------#')
print_p('\tHansei finished')
print('#------------------------------------------------------------------------------#')
print("Files/directories used during processing:")
print "\tElf: "+args.elf
print "\tUlog parser Directory: "+args.parser
print "\tOutput Directory: %s"%args.output

