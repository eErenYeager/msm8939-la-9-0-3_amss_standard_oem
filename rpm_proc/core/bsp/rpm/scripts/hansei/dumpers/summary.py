from __future__ import print_function

import os
from locators.core_dump import locate as locate_core_dump
from dwarf import decode_object
from hansei_utils import *

from sys import stdout
chips = {
    # family '0' -> Badger family
    0 : {
        0 : 'MSM8916', # chip '0' 
        1 : 'MDM8936', # chip '1' 
    }
}
def find_version_info(str_name, mem_start, mem_end, memory):
    output = ""
    for addr in range(mem_start, mem_end, 1):
        if (memory.read(addr, len(str_name)) == str_name):
            offset = len(str_name)
            while (memory.read(addr+offset, 1) != "\0" and (addr+offset) < mem_end):
                output+=memory.read(addr+offset, 1)
                offset+=1
    if (output):
        return output
    else:
        return "NOT FOUND"

def dump(dump_path, memory, debug_info):
    dump = open(os.path.join(dump_path, 'rpm-summary.txt'), 'w')

    address = locate_core_dump(memory, debug_info)
    dump_type = debug_info.variables['rpm_core_dump'].vartype
    rpm_core_dump = decode_object('rpm_core_dump', address, dump_type, memory, debug_info)

    hw_info = rpm_core_dump.hw_version.parts
    chip_name = '<unknown device -> family %d device %d>' % (hw_info.family_number, hw_info.device_number)
    chip_name = chips.get(hw_info.family_number, {}).get(hw_info.device_number, chip_name)
    print('\nCollected from %s %d.%d\n' % (chip_name, hw_info.major_version, hw_info.minor_version), file=dump)

    rpm_ver_type = debug_info.variables['rpm_core_dump'].vartype
    rpm_build_date_type = debug_info.variables['gBuildDate'].vartype
    rpm_build_time_type = debug_info.variables['gBuildTime'].vartype
    print("QC_IMAGE_VERSION_STRING:  %s" % find_version_info("QC_IMAGE_VERSION_STRING=",0x190000, 0x91000, memory), file=dump)
    print("IMAGE_VARIANT_STRING:     %s" % find_version_info("IMAGE_VARIANT_STRING=",0x190000, 0x91000, memory), file=dump)
    print("OEM_IMAGE_VERSION_STRING: %s" % find_version_info("OEM_IMAGE_VERSION_STRING=",0x90000, 0x91000, memory), file=dump)
    print("RPM Version:              %s" % find_version_info("IMAGE VERSION:",0x90000, 0x91000, memory), file=dump)
    if rpm_core_dump.dumped_at == 0:
        print('\nThe RPM is ok.', file=dump)
        return
    
    print('\nThe RPM crashed (@ time = 0x%x)' % rpm_core_dump.dumped_at, file=dump)
    xpsr = rpm_core_dump.registers.xPSR
    
    ipsr = xpsr & 0xff
    if ipsr == 0:
        print('\tNot in an exception context; vanilla fatal error scenario.', file=dump)
    elif ipsr < 16:
        fault_types = {
            2  : 'NMI',
            3  : 'Hard Fault',
            4  : 'Memory Management Fault',
            5  : 'Bus Fault',
            6  : 'Usage Fault',
            11 : 'SVCall',
            12 : 'Debug Monitor',
            14 : 'PendSV',
            15 : 'SysTick',
        }
        print('\tIn a fault context -> %s' % fault_types[ipsr], file=dump)
    else:
        print('\tFatal error inside ISR #%d' % (ipsr - 16), file=dump)
    
    
    def decode_bitfield(name, bit_definitions, data, joiner = ' | '):
    	known_bits = 0
    	for id in bit_definitions:
    		known_bits |= (1 << id)
    	unknown_data = data - (data & known_bits)
    	string = joiner.join(['[' + bit_definitions[x] + ']' for x in bit_definitions if (1 << x) & data])
    	if unknown_data:
    		if string:
    			string += ' ...and '
    		multi_or_single = ''
    		if log(unknown_data, 2) != int(log(unknown_data, 2)):
    			multi_or_single = 's'
    		string += 'unknown %s%s 0x%0.8x' % (name, multi_or_single, unknown_data)
    	return string
    
    
    fault_regs = rpm_core_dump.fault_detail
    
    shcas_bits = {
        0  : 'Memory fault in progress',
        1  : 'Bus fault in progress',
        3  : 'Usage fault in progress',
        7  : 'SVCall in progress',
        8  : 'Debug Monitor in progress',
        10 : 'PendSV in progress',
        11 : 'SysTick in progress',
        12 : 'Usage fault pended',
        13 : 'Memory management fault pended',
        14 : 'Bus fault pended',
        15 : 'SVCall pended',
        16 : 'Memory management fault enabled (this is not a problem)',
        17 : 'Bus fault enabled (this is not a problem)',
        18 : 'Usage fault enabled (this is not a problem)',
    }
    
    print('\nSystem handler status {\n\t%s\n}' % decode_bitfield('bits', shcas_bits, fault_regs.SystemHandlerCtrlAndState, '\n\t'), file=dump)
    
    cfs_bits = {
        0  : 'Attempt to fetch an instruction from a non-executable region.',
        1  : 'Attempt to load or store a location that does not permit it.',
        3  : 'Unstacking from an exception return has caused one or more access violations.',
        4  : 'Stacking for an exception has caused one or more access violations.',
        7  : 'Memory manage address register (MMAR) is valid.',
        8  : 'Instruction bus error.',
        9  : 'Precise data bus error.',
        10 : 'Imprecise data bus error.',
        11 : 'Unstack from exception has caused one or more bus errors.',
        12 : 'Stacking for exception has caused one or more bus errors.',
        15 : 'Bus fault address register (BFAR) is valid.',
        16 : 'Undefined instruction exception.',
        17 : 'Invalid state exception.',
        18 : 'Illegal attempt to load EXC_RETURN into PC.',
        19 : 'Attempt to use a coprocessor instruction.',
        24 : 'Unaligned memory access.',
        25 : 'Divide by zero.',
    }
    
    print('\nConfigurable fault status {\n\t%s\n}' % decode_bitfield('bits', cfs_bits, fault_regs.ConfigurableFaultStatus, '\n\t'), file=dump)
    
    hfs_bits = {
        0  : 'Fault on vector table read during exception processing.',
        30 : 'Hard Fault activated because a configurable fault was received and cannot activate because of priority or it is disabled.',
        31 : 'Fault related to debug.',
    }
    
    print('\nHard fault status {\n\t%s\n}' % decode_bitfield('bits', hfs_bits, fault_regs.HardFaultStatus, '\n\t'), file=dump)
        
    dfs_bits = {
        0  : 'Halt requested by NVIC.',
        1  : 'BKPT instruction execution.',
        2  : 'DWT match.',
        3  : 'Vector fetch occurred.',
        4  : 'EDBGRQ signal asserted.',
    }
    
    print('\nDebug fault status {\n\t%s\n}' % decode_bitfield('bits', hfs_bits, fault_regs.DebugFaultStatus, '\n\t'), file=dump)
    
    if 0 != (fault_regs.ConfigurableFaultStatus & (1 << 7)):
        print('\nMemory manage fault address: 0x%0.8x' % fault_regs.MemManageAddress, file=dump)
    
    if 0 != (fault_regs.ConfigurableFaultStatus & (1 << 15)):
        print('\nMemory manage fault address: 0x%0.8x' % fault_regs.BusFaultAddress, file=dump)
    
    print('\nAuxilary fault address register: 0x%0.8x' % fault_regs.AuxFaultStatus, file=dump)
    dump.close()
    
