#  ===========================================================================
#
#  Copyright (c) 2013 Qualcomm Technologies Incorporated.  
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ===========================================================================


from __future__ import print_function

import logging
logger = logging.getLogger(__name__)

import os
import itertools
from binascii import hexlify
from locators.core_dump import locate as locate_core_dump
from dwarf import decode_object, Structure
from dwarf import Array as darray
from hansei_utils import *

def dump(dump_path, memory, debug_info):
    address = locate_core_dump(memory, debug_info)
    dump_type = debug_info.variables['rpm_core_dump'].vartype
    rpm_core_dump = decode_object('rpm_core_dump', address, dump_type, memory, debug_info)

    #import pdb; pdb.set_trace()
    rpm = cast(rpm_core_dump.rpmserver_state, 'SystemData', memory, debug_info)

    #save_logger_level = update_logger(logging.DEBUG, logging)
    dump_mpm(dump_path, rpm, memory, debug_info)
    #update_logger(save_logger_level, logging)

def dump_mpm(dump_path, rpm, memory, debug_info):
    mpm_file_name = os.path.join(dump_path, 'mpm.txt')
    with open(mpm_file_name, 'w') as mpm_file:
        
        print("\n ~~MPM Register Dump~~", file=mpm_file)
        
        try:
            mpm_type = debug_info.variables['rpm_mpm_registers'].die
            mpm_address = debug_info.variables['rpm_mpm_registers'].address
        except:
            print("Failed to find mpm register dump variable", file=mpm_file)
            return
        
        # This will break if mpm_register type ever changes...
        mpm_type2 = debug_info.types['uint32_t']
        mpm_array = darray('mpm_registers', mpm_address, mpm_type2, 50, memory, debug_info)
        
        '''
        mpm_reg_0 = decode_object('rpm_mpm_registers', mpm_address, None, memory, debug_info, die=mpm_type2)
        test = memory.read(mpm_address+(0*4), 4)
        test[3].encode('hex')+test[2].encode('hex')+test[1].encode('hex')+test[0].encode('hex')
        memory.read(mpm_address+(x*4), 4)[3].encode('hex')+memory.read(mpm_address+(x*4), 4)[2].encode('hex')+memory.read(mpm_address+(x*4), 4)[1].encode('hex')+memory.read(mpm_address+(x*4), 4)[0].encode('hex')
        mpm_regs = decode_object('rpm_mpm_registers', mpm_address, None, memory, debug_info, die=mpm_type)
        '''
        
        
        for reg_num in range(50):
            reg = mpm_array[reg_num]
            print("\tREGISTER[%02.0u] = 0x%0.8x\n" % (reg_num, reg), file=mpm_file)
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            