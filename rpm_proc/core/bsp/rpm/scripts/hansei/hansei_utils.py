#  ===========================================================================
#
#  Copyright (c) 2013 Qualcomm Technologies Incorporated.  
#  All Rights Reserved.
#  QUALCOMM Proprietary and Confidential.
#
#  ===========================================================================

'''
print_p - pretty print function
'''
def print_p(str):
    x = 0
    while(x<len(str)):
        if str[x] == '\t':
            str = str[0:x]+"     "+str[x+1:]
        x+=1
    print('#%-78.78s#'%str)


from dwarf import decode_object, Structure

def check_member(member, object):
    for x in object.members():
        if member in x:
            return True
    return False
    
def cast(obj, typename, memory, debug_info):
    type_die = debug_info.types[typename]
    return decode_object(obj._name, obj.address, None, memory, debug_info, die=type_die)

def update_logger(new_level, logging):
    save_logger_level = logging.getLogger('dwarf').getEffectiveLevel()
    logging.getLogger('dwarf').setLevel(new_level)
    print("NEW LOGGING LEVEL: %d" % logging.getLogger('dwarf').getEffectiveLevel())
    return save_logger_level
    
def type_to_str(type_int):
    type_str = ''
    while type_int > 0:
        type_str += chr(type_int & 0xFF)
        type_int >>= 8

    if not type_str:
        return '<empty>'
    return type_str
    
def find_value(value, length, mem_start, mem_end, memory):
    output = ""
    #import pdb; pdb.set_trace()
    for addr in range(mem_start, mem_end, 4):
        if (memory.read(addr, length) == str_name):
            return addr
    return 0x0