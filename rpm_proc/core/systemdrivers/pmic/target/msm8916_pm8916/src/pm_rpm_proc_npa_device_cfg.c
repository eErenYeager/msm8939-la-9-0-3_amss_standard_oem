/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

             P M    NPA    D E V I C E    C F G  S E R V I C E S

GENERAL DESCRIPTION
  This file contains PMIC configuration functions on RPM for NPA Device layer.

EXTERNALIZED FUNCTIONS
  None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
  None.

Copyright (c) 2011           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/target/msm8916_pm8916/src/pm_rpm_proc_npa_device_cfg.c#2 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/15/15   mr      Relax the upper voltage limit for LDO17 to 3.3 Voltage in RPM for SBC Platform (CR-805258)
06/09/14   mr      XO changes, Keep L7 always ON and change L11 Min mode to IPEAK (CR-677315)
03/24/14   rk      APSS fails to wakeup on VDD_MIN (CR - 494038)
09/11/13   rk      Changed sleep settings for PMIC 2.0
09/11/13   rk      Adding Sleep Settings
08/13/12   umr     XO shutdown PinCtl and Manual config
04/16/12   hs      Removed pm_vreg_internal.h
10/13/11   jtn     Port pm_npa_rpm_validate_req() API from 8660, update for 8960
09/21/11   hw      Follow Sleep_b for S1, S3, S4, L24. S7 turned off
02/22/11   umr     Created.
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES FOR MODULE

===========================================================================*/
#include "assert.h"
#include "err.h"

#include "pm_qc_pmic.h"
#include "pm_rpm_proc_npa_device_cfg.h"
#include "spmi_1_0_lite.h"
#include "bare_metal_info.h"
#include "pm_version.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "pm_target_information.h"
#include "CoreVerify.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "PlatformInfo.h"


/*===========================================================================

                 LOCAL CONSTANT AND MACRO DEFINITIONS

===========================================================================*/

/*===========================================================================

                LOCAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

                VARIABLES DEFINITIONS

===========================================================================*/
static uint8 pm_chip_major_revision;
static sleep_register_type* sleep_enter_settings = NULL;
static sleep_register_type* sleep_exit_settings = NULL;

/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/

void pm_npa_rpm_ddr_self_refresh_enter (void)
{
  //pm_vreg_smps_config (PM_SMPS_10, PM_VREG_SMPS_MODE__PFM); //SMPS 4B
}

void pm_npa_rpm_ddr_self_refresh_exit (void)
{
  /* Put back SMPS_4B to PWM */
  //pm_vreg_smps_config (PM_SMPS_10, PM_VREG_SMPS_MODE__PWM);//SMPS 4B
}

void pm_rpm_sleep_init(void)
{
    DALSYS_PROPERTY_HANDLE_DECLARE(hProp);
    DALSYSPropertyVar prop;
    pm_chip_major_revision = pm_get_pmic_revision(0);

    if(DAL_SUCCESS == DALSYS_GetDALPropertyHandleStr("/rpm/pmic/target",hProp))
    {
        if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp, NULL, PM_PROP_SLEEP_ENTER_INFO, &prop))
        {
            sleep_enter_settings = (sleep_register_type*)prop.Val.pStruct;
        }

        if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp, NULL, PM_PROP_SLEEP_EXIT_INFO, &prop))
        {
            sleep_exit_settings = (sleep_register_type*)prop.Val.pStruct;
        }
    }
}

/*===========================================================================

FUNCTION pm_npa_rpm_enter_sleep

DESCRIPTION
    This function calls the Driver layer PMIC calls to set the Rails to the
    state prior to entering sleep.

INPUT PARAMETERS
  NONE

RETURN VALUE
  NONE

DEPENDENCIES
  PMIC APIs should be available.

SIDE EFFECTS
  Will over-write any client configuration requests. Any additions to the below
  function should be cautiously be added.

===========================================================================*/
void pm_npa_rpm_enter_sleep (void)
{
    unsigned i = 0;
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    uint8            data = 0;

    if(sleep_enter_settings != NULL)
    {
        while(sleep_enter_settings[i].regAddr != 0xFFFF)
        {
            pm_spmi_lite_write_byte(sleep_enter_settings[i].slaveID,
                                    sleep_enter_settings[i].regAddr,
                                    sleep_enter_settings[i].data, 0);
            i++;
        }

        //For PMIC 2.0 the SW Workaround #3 � Force
        //VMUX to LDO6 (CR-0000163990) has been moved to MPM
        if (pm_chip_major_revision == 1)
        {
            pm_spmi_lite_write_byte( 0, 0x5040, 0x02 , 1);
        }
    }

    /* Read the value of XO_ADJ_FINE */
    err = pm_spmi_lite_read_byte(0, 0x505C, &data, 0);

    /* Copy value of  XO_ADJ_FINE to CLK_DIST_SPARE1 */
    err |= pm_spmi_lite_write_byte(0, 0x5951, data, 0);

    /* Calculate ( XO_ADJ_FINE+2) & Copy value to CLK_DIST_SPARE2 */
    err |= pm_spmi_lite_write_byte(0, 0x5952, data+2, 0);
}

/*===========================================================================

FUNCTION pm_npa_rpm_exit_sleep

DESCRIPTION
    This function calls the Driver layer PMIC calls to set the Rails back to
    the prior state of entering sleep.

INPUT PARAMETERS
  NONE

RETURN VALUE
  NONE

DEPENDENCIES
  PMIC APIs should be available.

SIDE EFFECTS
  Will over-write any client configuration requests. Any additions to the below
  function should be cautiously be added.

===========================================================================*/
void pm_npa_rpm_exit_sleep (void)
{
  unsigned i = 0;

    if(sleep_exit_settings != NULL)
    {
        //For PMIC 2.0 the SW Workaround #3
        // Force VMUX back to Auto (CR-0000163990) has been moved to MPM
        if (pm_chip_major_revision == 1)
        {
          pm_spmi_lite_write_byte( 0, 0x5040, 0x00, 1);
        }

        while(sleep_exit_settings[i].regAddr != 0xFFFF)
        {

            pm_spmi_lite_write_byte(sleep_exit_settings[i].slaveID,
                                    sleep_exit_settings[i].regAddr,
                                    sleep_exit_settings[i].data, 0);
            i++;
        }
    }

    //Temp fix for SPMI INT MISS
    //INT_INT_RESEND_ALL => 0x540 (register)
    //INT_RESEND_ALL => 0x1  (field)
    pm_spmi_lite_write_byte(0 , 0x540, 0x01, 1);
    //SPMI_INT_LATCHED_CLR => 0x614 (register)
    //SPMI_INT_LATCHED_CLR =>0x1 (field)
    pm_spmi_lite_write_byte(0 , 0x614, 0x01, 1);
}

void pm_rpm_platform_init(void)
{
    // Keeping this dummy as it might be required to add any platform specific changes. So retaining this API
    PlatformInfoPlatformType platform;
    pm_npa_ldo_data_type	 *ldo_data = NULL;
  
    platform = PlatformInfo_GetPlatform();

    if (PLATFORMINFO_TYPE_SBC == platform)
    {
        ldo_data = pm_rpm_ldo_get_resource_data(RPM_LDO_A_REQ);
        CORE_VERIFY_PTR(ldo_data);
        ldo_data->railInfo[16].MaxVoltage = 3300;
    }
}

