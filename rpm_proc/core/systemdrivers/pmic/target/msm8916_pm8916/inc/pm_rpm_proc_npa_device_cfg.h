#ifndef PM_RPM_PROC_NPA_DEVICE_CFG_H
#define PM_RPM_PROC_NPA_DEVICE_CFG_H
/*===========================================================================


                  P M    N P A    D E V I C E    H E A D E R    F I L E

DESCRIPTION
  This file contains prototype definitions npa device layer

Copyright (c) 2011 by Qualcomm Technologies, Incorporated.  All Rights Reserved.
===========================================================================*/

/*===========================================================================

                      EDIT HISTORY FOR FILE

This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/target/msm8916_pm8916/inc/pm_rpm_proc_npa_device_cfg.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/11/13   rk      Added sleep init prototype to be called from pm_init()
09/11/13   rk      Code refactoring.
10/13/11   jtn     Add file for 8960
04/15/11   umr     Allow OEMs to Validate rails requests. 

===========================================================================*/
/*===========================================================================

                        INCLUDE FILES

===========================================================================*/
#include "pm_npa_device.h"


/*===========================================================================

                        DEFINITIONS

===========================================================================*/

/*===========================================================================

                        GENERIC FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

FUNCTION pm_rpm_sleep_init

DESCRIPTION
    Initializes the sleep settings LUT.

INPUT PARAMETERS
  NONE

RETURN VALUE
  NONE

DEPENDENCIES
  Sleep settings should be available in config.

SIDE EFFECTS
  None

===========================================================================*/

void pm_rpm_sleep_init(void);

/*===========================================================================

FUNCTION pm_npa_rpm_enter_sleep

DESCRIPTION
    This function calls the Driver layer PMIC calls to set the Rails to the
    state prior to entering sleep.

INPUT PARAMETERS
  NONE

RETURN VALUE
  NONE

DEPENDENCIES
  PMIC APIs should be available.

SIDE EFFECTS
  Will over-write any client configuration requests. Any additions to the below
  function should be cautiously be added.

===========================================================================*/
void 
pm_npa_rpm_enter_sleep (void);

/*===========================================================================

FUNCTION pm_npa_rpm_exit_sleep

DESCRIPTION
    This function calls the Driver layer PMIC calls to set the Rails back to
    the prior state of entering sleep.

INPUT PARAMETERS
  NONE

RETURN VALUE
  NONE

DEPENDENCIES
  PMIC APIs should be available.

SIDE EFFECTS
  Will over-write any client configuration requests. Any additions to the below
  function should be cautiously be added.

===========================================================================*/
void 
pm_npa_rpm_exit_sleep (void);

/** 
 * @name pm_rpm_platform_init 
 *  
 * @brief This function initialize the platform specific 
 *        settings.
 * 
 * @param None
 *
 * @return None 
 *  
 * @sideeffects None
 *
 */
void pm_rpm_platform_init(void);



#endif /* PM_RPM_PROC_NPA_DEVICE_CFG_H */
