/*! \file pm_pwr_alg.c 
*  \n
*  \brief  
*  \n  
*  \n &copy; Copyright 2012-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/drivers/pwr/src/pm_pwr_alg.c#1 $

when        who     what, where, why
--------    ---     ----------------------------------------------------------
12/06/12   hw      Rearchitecturing module driver to peripheral driver
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pwr_alg.h"

/*===========================================================================

                     FUNCTION IMPLEMENTATION 

===========================================================================*/

pm_err_flag_type pm_pwr_pull_down_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, pm_on_off_type on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    
    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (on_off >= PM_INVALID)
    {
        err_flag = PM_ERR_FLAG__MACRO_NOT_RECOGNIZED;
    }
    else
    {
        pm_register_address_type          reg = 0xFF;
        pm_register_data_type             data = 0xFF;
    
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->PD_CTL);
    
        data = (PM_OFF == on_off) ? 0 : 0x80;

        err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x80, data, 0);
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_PULL_DOWN_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_sw_mode_status_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, pm_sw_mode_type *sw_mode)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
                                                          
    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (sw_mode == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0xFF;
            
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->MODE_CTL);

        err_flag = comm_ptr->ReadByte(comm_ptr, reg, &data, 0);
        
        if (err_flag == PM_ERR_FLAG__SUCCESS)
        {
            /* BYPASS supercedes NPM & PFM */
            /* Only valid for LDO's */
            if (data & 0x20)
            {
                *sw_mode = PM_SW_MODE_BYPASS;
            }
            /* NPM supercedes AUTO & PFM */
            else if (data & 0x80)
            {
                *sw_mode = PM_SW_MODE_NPM;
            }
            /* AUTO supercedes PFM */
            /* only valid for SMPS */
            else if (data & 0x40)
            {
                *sw_mode = PM_SW_MODE_AUTO;
            }
            else /* IT MIGHT BE PFM DEPENDING ON pc_mode bits */
            {
                *sw_mode = PM_SW_MODE_LPM;
            }
        }
        else
        {
            *sw_mode = PM_SW_MODE_INVALID;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_PWR_MODE_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_sw_mode_status_raw_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, uint8 *mode_ctl)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
                                                          
    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (mode_ctl == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0xFF;
            
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->MODE_CTL);

        err_flag = comm_ptr->ReadByte(comm_ptr, reg, &data, 0);
        
        if (err_flag == PM_ERR_FLAG__SUCCESS)
        {
            *mode_ctl = data;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_PWR_MODE_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_pin_ctrl_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, uint8 select_pin)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index < pwr_data->num_of_peripherals)
    {
        pm_register_address_type           reg = 0x0;

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->EN_CTL);

        err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x0F, (pm_register_data_type)select_pin, 1); // bit<0:3>
    }
    else
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_PIN_CTRL_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_pin_ctrl_mode_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, uint8 select_pin)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index < pwr_data->num_of_peripherals)
    {
        pm_register_address_type           reg = 0x0;

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->MODE_CTL);

        err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x1F, (pm_register_data_type)select_pin, 0); // bit<0:4>
    }
    else
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_PIN_CTRL_MODE_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_sw_enable_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, pm_on_off_type on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (on_off >= PM_INVALID)
    {
        err_flag = PM_ERR_FLAG__MACRO_NOT_RECOGNIZED;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
            
        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->EN_CTL);

        if (PM_OFF == on_off)
        {
            err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x80, 0, 0);
        }
        else
        {
            err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x80, 0x80, 0);
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_SW_EN_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_sw_enable_status_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, pm_on_off_type *on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (on_off == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0;

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->EN_CTL);

        err_flag = comm_ptr->ReadByte(comm_ptr, reg, &data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            *on_off = PM_INVALID;
            return err_flag;
        }

        if (data & 0x80)
        {
            *on_off = PM_ON;
        }
        else
        {
            *on_off = PM_OFF;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_SW_EN_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_sw_enable_vreg_ok_status_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, boolean *vreg_ok)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index >= pwr_data->num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (vreg_ok == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type           reg = 0x0;
        pm_register_data_type              data = 0;

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->STATUS);

        err_flag = comm_ptr->ReadByte(comm_ptr, reg, &data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            *vreg_ok = FALSE;
            return err_flag;
        }

        if (data & 0x80)
        {
            *vreg_ok = TRUE;
        }
        else
        {
            *vreg_ok = FALSE;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_SW_EN_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_volt_level_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8 peripheral_index, pm_volt_level_type  volt_level)
{
    pm_err_flag_type                  err_flag = PM_ERR_FLAG__SUCCESS;
    uint32                            vmin = 0;  // microvolt
    uint32                            vStep = 0; // microvolt
    uint32                            range = 0; // 0 to 4
    pm_register_data_type             vset = 0;
    pm_register_address_type          reg = 0;
    uint8                             mask = 0xFF;
    pm_device_info_type               pmic_device_info;

    err_flag = pm_get_pmic_info(0, &pmic_device_info);
    if (PM_ERR_FLAG__SUCCESS == err_flag)
    {
        /* In PM8916 version 1.0  some of the regulators (L4 to L18 and S4) are not regulating properly. So avoiding to program the VSET for those LDOs. Instead it is configured in PBS */
        if ((PMIC_IS_PM8916 == pm_get_pmic_model(0)) && (1 == pmic_device_info.nPmicAllLayerRevision) && (0 == pmic_device_info.nPmicMetalRevision) && (peripheral_index >= 3) && (peripheral_index <= 17))
        {
            err_flag = PM_ERR_FLAG__SUCCESS ;
            return err_flag;
        }
    }
    else
    {
        return err_flag;
    }

    if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED ;
    }
    else
    {
        range = pwr_data->pwr_specific_info[peripheral_index].pwr_range;
           //If the range read failed in init , return error
        if (range == PM_VOLT_INVALID_RANGE )
        {
          //Think about proper error code, this error is due 
          //to SPMI read failure but that happened at init
           err_flag = PM_ERR_FLAG__VLEVEL_OUT_OF_RANGE;
           return err_flag;
        } 

        if ((volt_level <= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMax) && 
            (volt_level >= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin) )
        {
            vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin;
            vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].VStep;
        }
       
        if (vStep > 0)
        {
            //calculate VSET
            vset = (pm_register_data_type)((volt_level - vmin)/vStep);

             // Set VSET
             reg =  (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->VOLTAGE_CTRL2);

             if ((1 == range) && (PM_HW_MODULE_ULT_BUCK == pwr_data->pwr_specific_info[peripheral_index].periph_type))
             {
                 mask = 0x1F; // since bit<5> and bit<6> implies range which shouldn�t be changed, bit <7> is unused.
             }
             // Set the vset then
             err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, mask, vset , 0) ; // 0:7
        }
        else
        {
            err_flag = PM_ERR_FLAG__VLEVEL_OUT_OF_RANGE;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_VOLT_LVL_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_volt_level_status_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8  peripheral_index, pm_volt_level_type *volt_level)
{
    pm_err_flag_type                 err_flag = PM_ERR_FLAG__SUCCESS;
    uint32                           vmin = 0; // microvolt
    uint32                           vStep = 0; //microvolt
    uint32                           range = 0; // 0 to 4
    pm_register_data_type            vset = 0;
    pm_register_address_type         reg = 0;
    pm_register_data_type            reg_data[2];

    if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (volt_level == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        // get the voltage level LUT
        reg =  (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->VOLTAGE_CTRL2);

        err_flag = comm_ptr->ReadByteArray(comm_ptr, reg, 1, reg_data, 0);

        if ( err_flag == PM_ERR_FLAG__SUCCESS )
        {
            range = pwr_data->pwr_specific_info[peripheral_index].pwr_range;
            vset = reg_data[0];
            if ((1 == range) && (PM_HW_MODULE_ULT_BUCK == pwr_data->pwr_specific_info[peripheral_index].periph_type))
            {
                vset &= 0x1F;
            }
            vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin; // microvolt
            vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].VStep; //microvolt

            *volt_level = vmin + vset * vStep;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_VOLT_LVL_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_volt_level_stepper_done_status_alg(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8  peripheral_index, boolean *stepper_done)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;

    if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (stepper_done == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        pm_register_address_type          reg = 0;
        pm_register_data_type             data = 0;

        // get the voltage level LUT
        reg =  (pm_register_address_type)(pwr_data->pwr_specific_info[peripheral_index].periph_base_address + pwr_data->pwr_reg_table->STATUS);

        err_flag = comm_ptr->ReadByte(comm_ptr, reg, &data, 0);
        if (err_flag != PM_ERR_FLAG__SUCCESS)
        {
            *stepper_done = FALSE;
            return err_flag;
        }

        if (data & 0x01)
        {
            *stepper_done = TRUE;
        }
        else
        {
            *stepper_done = FALSE;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_VOLT_LVL_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_pwr_volt_calculate_vset(pm_pwr_data_type *pwr_data, pmiC_IComm *comm_ptr, uint8  peripheral_index, pm_volt_level_type volt_level, uint32 *vset)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint32           vmin = 0; // microvolt
    uint32           vStep = 0; //microvolt
    uint32           range = 0; //microvolt

    if (peripheral_index >= (pwr_data->num_of_peripherals))
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (vset == NULL)
    {
        err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
        range = pwr_data->pwr_specific_info[peripheral_index].pwr_range;
        //If the range read failed in init , return error
        if (range == PM_VOLT_INVALID_RANGE)
        {
            //Think about proper error code, this error is due 
            //to SPMI read failure but that happened at init
            err_flag = PM_ERR_FLAG__VLEVEL_OUT_OF_RANGE;
            return err_flag;
        }

        if ((volt_level <= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMax) && 
            (volt_level >= pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin))
        {
            vmin = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].RangeMin;
            vStep = pwr_data->pwr_specific_info[peripheral_index].pwr_vset[range].VStep;

        }

        if (vStep > 0)
        {
            //calculate VSET
            *vset = (pm_register_data_type)((volt_level - vmin)/vStep);
            if ((1 == range) && (PM_HW_MODULE_ULT_BUCK == pwr_data->pwr_specific_info[peripheral_index].periph_type))
            {
                *vset |= 0x60;
            }
        }
        else
        {
            err_flag = PM_ERR_FLAG__VLEVEL_OUT_OF_RANGE;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_VOLT_LVL_ERROR, peripheral_index, err_flag);
    }

    return err_flag;
}

