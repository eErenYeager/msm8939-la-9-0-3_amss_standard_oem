/*! \file pm_ldo.c
*  \n
*  \brief Implementation file for LDO public APIs.
*  \n
*  &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/drivers/ldo/src/pm_ldo.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
09/18/13   sv     init version.(CR-549436)
===========================================================================*/
/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "pm_ldo.h"
#include "pm_ldo_driver.h"

/*===========================================================================

                     LOCAL FUNCTION PROTOTYPE

===========================================================================*/

static pm_err_flag_type pm_ldo_sw_mode_exit_bypass(pmiC_IComm *comm_ptr, pm_register_address_type reg);

/*===========================================================================

                     API IMPLEMENTATION

===========================================================================*/

pm_err_flag_type pm_ldo_sw_mode(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_sw_mode_type mode)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_ldo_data_type *ldo_ptr = pm_ldo_get_data(pmic_chip);
    pm_device_info_type               pmic_device_info;

    err_flag = pm_get_pmic_info(0, &pmic_device_info);
    if (PM_ERR_FLAG__SUCCESS == err_flag)
    {
        /* In PM8916 version 1.0 and 1.1, MODE_CTL operation is not allowed for PMOS regulators (L4 to L18 and S4). So avoiding to program the MODE_CTL for those LDOs. */
        if ((PMIC_IS_PM8916 == pm_get_pmic_model(0)) && (1 == pmic_device_info.nPmicAllLayerRevision) && (ldo_peripheral_index >= 3) && (ldo_peripheral_index <= 17))
        {
            return err_flag;
        }
    }
    else
    {
        return err_flag;
    }

    if ((ldo_ptr == NULL) || (ldo_ptr->periph_exists == FALSE))
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if(ldo_peripheral_index >= ldo_ptr->pm_pwr_data.num_of_peripherals)
    {
        err_flag = PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    else if (mode >= PM_SW_MODE_INVALID)
    {
        err_flag = PM_ERR_FLAG__MACRO_NOT_RECOGNIZED;
    }
    else
    {
        pm_register_address_type          reg = 0;
        pmiC_IComm                        *comm_ptr = ldo_ptr->comm_ptr;
        pm_pwr_data_type                  *pwr_data = &(ldo_ptr->pm_pwr_data);

        reg = (pm_register_address_type)(pwr_data->pwr_specific_info[ldo_peripheral_index].periph_base_address + pwr_data->pwr_reg_table->MODE_CTL);

        switch(mode)
        {
            case PM_SW_MODE_LPM: /* Low power mode */
            {
                /* the LDO is already in Bypass mode and will be changed to LPM */
                if (ldo_ptr->in_bypass[ldo_peripheral_index])
                {
                    /* exit bypass properly */
                    err_flag = pm_ldo_sw_mode_exit_bypass(comm_ptr, reg);
                    if (err_flag == PM_ERR_FLAG__SUCCESS)
                    {
                        /* update the bypass LUT */
                        ldo_ptr->in_bypass[ldo_peripheral_index] = 0;
                    }
                }
            
                err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x80, 0, 0);
            }
            break;
			
            case PM_SW_MODE_NPM: /* normal power mode */
            {
                /* the LDO is already in Bypass mode and will be changed to NPM */
                if (ldo_ptr->in_bypass[ldo_peripheral_index])
                {
                    /* exit bypass properly */
                    err_flag = pm_ldo_sw_mode_exit_bypass(comm_ptr, reg);
                    if (err_flag == PM_ERR_FLAG__SUCCESS)
                    {
                        /* update the bypass LUT */
                        ldo_ptr->in_bypass[ldo_peripheral_index] = 0;
                    }
                }
                err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x80, 0x80, 0);
            }
            break;
			
            case PM_SW_MODE_BYPASS: /* bypass mode */
            {
                if (!ldo_ptr->in_bypass[ldo_peripheral_index])
                {
                    err_flag = comm_ptr->WriteByteMask(comm_ptr, reg, 0x20, 0x20, 0);
                    if (err_flag == PM_ERR_FLAG__SUCCESS)
                    {
                        /* update the bypass LUT */
                        ldo_ptr->in_bypass[ldo_peripheral_index] = 1;
                    }
                }
            }
            break;
			
            default:
                err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
            break;
        }
    }

    if (PM_ERR_FLAG__SUCCESS != err_flag)
    {
        PM_SWEVENT(PMIC_DRV_PWR_MODE_ERROR, ldo_peripheral_index, err_flag);
    }

    return err_flag;
}

pm_err_flag_type pm_ldo_sw_enable(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type on_off)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_ldo_data_type *ldo_ptr = pm_ldo_get_data(pmic_chip);

    if ((ldo_ptr == NULL) || (ldo_ptr->periph_exists == FALSE))
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        err_flag = pm_pwr_sw_enable_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, ldo_peripheral_index, on_off);
    }
    return err_flag;
}

pm_err_flag_type pm_ldo_sw_enable_status(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type *on_off)
{
    pm_err_flag_type         err_flag = PM_ERR_FLAG__SUCCESS;
    pm_ldo_data_type *ldo_ptr = pm_ldo_get_data(pmic_chip);

    if ((ldo_ptr == NULL) || (ldo_ptr->periph_exists == FALSE))
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        err_flag = pm_pwr_sw_enable_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, ldo_peripheral_index, on_off);
    }
    return err_flag;
}

pm_err_flag_type pm_ldo_vreg_ok_status(uint8 pmic_chip, uint8 ldo_peripheral_index, boolean* vreg_ok)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_ldo_data_type *ldo_ptr = pm_ldo_get_data(pmic_chip);

    if ((ldo_ptr == NULL) || (ldo_ptr->periph_exists == FALSE))
    {
        err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        err_flag = pm_pwr_sw_enable_vreg_ok_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, ldo_peripheral_index, vreg_ok);
    }
    return err_flag;
}

pm_err_flag_type pm_ldo_calculate_vset(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_volt_level_type volt_level, uint32 *vset)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_ldo_data_type *ldo_ptr = pm_ldo_get_data(pmic_chip);

    if ((ldo_ptr == NULL) || (ldo_ptr->periph_exists == FALSE))
    {
      err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
        err_flag = pm_pwr_volt_calculate_vset(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, ldo_peripheral_index, volt_level, vset);
    }
    return err_flag;
}

/*===========================================================================

                     INTERNAL DRIVER FUNCTIONS

===========================================================================*/

pm_err_flag_type pm_ldo_sw_mode_exit_bypass(pmiC_IComm *comm_ptr, pm_register_address_type reg)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pmiC_IComm* temp_comm_ptr = NULL;

    /* the transaction manager will not be used since we need single step register writes. */
    if (comm_ptr->pInnerComm != NULL) /* the comm_ptr is a transcomm */
    {
        temp_comm_ptr = comm_ptr->pInnerComm;
    }
    else /* the comm_ptr is a regular comm */
    {
        temp_comm_ptr = comm_ptr;
    }

    CORE_VERIFY_PTR(temp_comm_ptr);

    /* step1: set BYPASS_ACT (bit 6) */
    err_flag = temp_comm_ptr->WriteByteMask(temp_comm_ptr, reg, 0x40, 0x40, 0);

    /* step2: wait 100uS */
    DALSYS_BusyWait(100);

    /* step3: clear BYPASS_ACT and BYPASS_EN (bit 6 and bit 5) */
    err_flag |= temp_comm_ptr->WriteByteMask(temp_comm_ptr, reg, 0x60, 0x00, 0);

    return err_flag;
}
