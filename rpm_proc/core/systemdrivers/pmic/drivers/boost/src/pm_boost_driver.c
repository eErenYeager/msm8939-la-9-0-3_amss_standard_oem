/*! \file pm_boost_driver.c 
*  \n
*  \brief This file contains BOOST peripheral driver initialization during which the driver
*         driver data is stored.
*  \n  
*  \n &copy; Copyright 2012-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/25/13   aks     Code Refactoring: Removing PMIC_SubRsc from pwr algs
04/12/13   hs      Code refactoring.
1/29/13    aks     Adding support for 5v boost as a separate driver
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_boost_driver.h"
#include "pm_target_information.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the BOOST driver data */
static pm_boost_data_type pm_boost_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                     INTERNAL DRIVER FUNCTIONS 

===========================================================================*/

void pm_boost_driver_init(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
    pm_boost_data_type *boost_ptr = NULL;
    uint16 boost_index = 0;
    pm_register_address_type base_address = 0;
    pm_register_address_type periph_offset = 0;
    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);
    uint32 prop_id_arr[] = {PM_PROP_BOOSTA_NUM, PM_PROP_BOOSTB_NUM};

    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);

    boost_ptr = &pm_boost_data_arr[pmic_index];
    
    if (boost_ptr->periph_exists == FALSE)
    {    
        boost_ptr->periph_exists = TRUE;
		
        /* Assign Comm ptr */
        boost_ptr->comm_ptr = comm_ptr;

        /* BOOST Register Info - Obtaining Data through dal config */
        boost_ptr->pm_pwr_data.pwr_reg_table = (pm_pwr_register_info_type*)pm_target_information_get_common_info(PM_PROP_BOOST_REG);

        CORE_VERIFY_PTR(boost_ptr->pm_pwr_data.pwr_reg_table);

        /* BOOST Num of peripherals - Obtaining Data through dal config */
        CORE_VERIFY(pmic_index < (sizeof(prop_id_arr)/sizeof(prop_id_arr[0])));

        boost_ptr->pm_pwr_data.num_of_peripherals = pm_target_information_get_count_info(prop_id_arr[pmic_index]);

        /* Num of peripherals cannot be 0 if this driver init gets called */
        CORE_VERIFY(boost_ptr->pm_pwr_data.num_of_peripherals != 0);

        /* BOOST pwr rail specific info pointer malloc to save all the peripheral's base address, Type, Range and Vset */
        pm_malloc(sizeof(pm_pwr_specific_info_type)*(boost_ptr->pm_pwr_data.num_of_peripherals), (void**)&(boost_ptr->pm_pwr_data.pwr_specific_info));

        CORE_VERIFY_PTR(boost_ptr->pm_pwr_data.pwr_specific_info);

        /* Save first BOOST peripheral's base address */
        boost_ptr->pm_pwr_data.pwr_specific_info[0].periph_base_address = peripheral_info->base_address;
    }
    
    if (boost_ptr->periph_exists == TRUE)
    {
        base_address = boost_ptr->pm_pwr_data.pwr_specific_info[0].periph_base_address;
        periph_offset = boost_ptr->pm_pwr_data.pwr_reg_table->peripheral_offset;
            
        /* Peripheral Baseaddress should be >= first peripheral's base addr */
        CORE_VERIFY(peripheral_info->base_address >= base_address);

        /* Calculate BOOST peripheral index */
        boost_index = ((peripheral_info->base_address - base_address)/periph_offset);

        /* Peripheral Index should be less than number of peripherals */
        CORE_VERIFY(boost_index < (boost_ptr->pm_pwr_data.num_of_peripherals));

        /* Save BOOST's Peripheral Type value */
        boost_ptr->pm_pwr_data.pwr_specific_info[boost_index].periph_type = peripheral_info->peripheral_type;

        /* Save each BOOST peripheral's base address */
        boost_ptr->pm_pwr_data.pwr_specific_info[boost_index].periph_base_address = peripheral_info->base_address;
    }
}

pm_boost_data_type* pm_boost_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_boost_data_arr[pmic_index];
    }

    return NULL;
}

