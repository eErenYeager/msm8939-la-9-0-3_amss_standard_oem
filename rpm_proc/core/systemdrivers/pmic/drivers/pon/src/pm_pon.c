/*! \file pm_pon.c
*  \n
*  \brief This file contains the implementation of the public APIs for PMIC PON peripheral.
*  \n
*  &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/drivers/pon/src/pm_pon.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/27/13   kt      Added PON Reasons and IRQ related APIs  
01/24/13   umr     Add new version support.
11/20/12   umr     Created
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pon.h"
#include "pm_pon_driver.h"

/*===========================================================================

                     STRUCTURE TYPE AND ENUM

===========================================================================*/
                                                       
#define PON_RESET_CFG_WARM_RESET_VAL                      0x01
#define PON_RESET_CFG_IMMEDIATE_XVDD_SHUTDOWN_VAL         0x02
#define PON_RESET_CFG_NORMAL_SHUTDOWN_VAL                 0x04
#define PON_RESET_CFG_D_VDD_SHUTDOWN_VAL                  0x05
#define PON_RESET_CFG_X_VDD_SHUTDOWN_VAL                  0x06
#define PON_RESET_CFG_HARD_RESET_VAL                      0x07

#define PON_RESET_CFG_DVDD_HARD_RESET_VAL                 0x08 
#define PON_RESET_CFG_XVDD_HARD_RESET_VAL                 0x09 
#define PON_RESET_CFG_WARM_RESET_AND_DVDD_SHUTDOWN_VAL    0x0A 
#define PON_RESET_CFG_WARM_RESET_AND_XVDD_SHUTDOWN_VAL    0x0B 
#define PON_RESET_CFG_WARM_RESET_AND_SHUTDOWN_VAL         0x0C 
#define PON_RESET_CFG_WARM_RESET_THEN_HARD_RESET_VAL      0x0D 
#define PON_RESET_CFG_WARM_RESET_THEN_DVDD_HARD_RESET_VAL 0x0E 
#define PON_RESET_CFG_WARM_RESET_THEN_XVDD_HARD_RESET_VAL 0x0F 

/* Max PMIC Watchdog S1 and S2 timer value in seconds */
#define PON_WDOG_MAX_S1_S2_TIMER_VAL           127

/*===========================================================================

                     API IMPLEMENTATION 

===========================================================================*/

pm_err_flag_type pm_pon_wdog_cfg(uint8 pmic_index, uint32 s1_timer, uint32 s2_timer, pm_pon_reset_cfg_type reset_cfg_type)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_pon_data_type *pon_ptr = pm_pon_get_data(pmic_index);
    pm_register_data_type wdog_enable = 0;
    pm_register_data_type reset_val = 0;
    pmiC_IComm *local_comm = NULL;

    if ((pon_ptr == NULL) || (pon_ptr->periph_exists == FALSE))
    { 
       err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if (s1_timer > PON_WDOG_MAX_S1_S2_TIMER_VAL)
    {
       err_flag = PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
    }
    else if (s2_timer > PON_WDOG_MAX_S1_S2_TIMER_VAL)
    {
       err_flag = PM_ERR_FLAG__PAR3_OUT_OF_RANGE;
    }
    else if (reset_cfg_type >= PM_PON_RESET_CFG_INVALID)
    {
       err_flag = PM_ERR_FLAG__PAR4_OUT_OF_RANGE;
    }
    else
    {
       local_comm = pon_ptr->comm_ptr;

       err_flag = local_comm->ReadByteMask(local_comm, PMIO_PON_PMIC_WD_RESET_S2_CTL2_ADDR, 0x80, &wdog_enable);

       if (err_flag != PM_ERR_FLAG__SUCCESS)
       {
          return err_flag;
       }

       /* Disable PMIC Wdog if already enabled before configuring the Wdog */
       if (wdog_enable)
       {
          /* Disable PMIC Wdog */
          err_flag = local_comm->WriteByteMask(local_comm, PMIO_PON_PMIC_WD_RESET_S2_CTL2_ADDR, 0x80, 0x00, 0);

          if (err_flag != PM_ERR_FLAG__SUCCESS)
          {
             return err_flag;
          }

          /* Requires 10 sleep clock cycles of delay */
          DALSYS_BusyWait(300);
       }

       /* Configure S1 timer */
       err_flag = local_comm->WriteByteMask(local_comm, PMIO_PON_PMIC_WD_RESET_S1_TIMER_ADDR, 0x7F, (uint8)s1_timer, 0);

       if (err_flag != PM_ERR_FLAG__SUCCESS)
       {
          return err_flag;
       }

       /* Configure S2 timer */
       err_flag = local_comm->WriteByteMask(local_comm, PMIO_PON_PMIC_WD_RESET_S2_TIMER_ADDR, 0x7F, (uint8)s2_timer, 0);

       if (err_flag != PM_ERR_FLAG__SUCCESS)
       {
          return err_flag;
       }

       switch (reset_cfg_type)
       {
          case PM_PON_RESET_CFG_WARM_RESET:
             reset_val = PON_RESET_CFG_WARM_RESET_VAL;
             break;

          case PM_PON_RESET_CFG_IMMEDIATE_X_VDD_COIN_CELL_REMOVE_SHUTDOWN:
             reset_val = PON_RESET_CFG_IMMEDIATE_XVDD_SHUTDOWN_VAL;
             break;

          case PM_PON_RESET_CFG_NORMAL_SHUTDOWN:
             reset_val = PON_RESET_CFG_NORMAL_SHUTDOWN_VAL;
             break;

          case PM_PON_RESET_CFG_D_VDD_BATT_REMOVE_SHUTDOWN:
             reset_val = PON_RESET_CFG_D_VDD_SHUTDOWN_VAL;
             break;

          case PM_PON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_SHUTDOWN:
             reset_val = PON_RESET_CFG_X_VDD_SHUTDOWN_VAL;
             break;

          case PM_PON_RESET_CFG_HARD_RESET:
             reset_val = PON_RESET_CFG_HARD_RESET_VAL;
             break;

          case PM_PON_RESET_CFG_D_VDD_BATT_REMOVE_HARD_RESET:
             reset_val = PON_RESET_CFG_DVDD_HARD_RESET_VAL;
             break;

          case PM_PON_RESET_CFG_X_VDD_COIN_CELL_REMOVE_HARD_RESET:
             reset_val = PON_RESET_CFG_XVDD_HARD_RESET_VAL;
             break;

          case PM_PON_RESET_CFG_WARM_RESET_AND_D_VDD_BATT_REMOVE_SHUTDOWN:
             reset_val = PON_RESET_CFG_WARM_RESET_AND_DVDD_SHUTDOWN_VAL;
             break;

          case PM_PON_RESET_CFG_WARM_RESET_AND_X_VDD_COIN_CELL_REMOVE_SHUTDOWN:
             reset_val = PON_RESET_CFG_WARM_RESET_AND_XVDD_SHUTDOWN_VAL;
             break;

          case PM_PON_RESET_CFG_WARM_RESET_AND_SHUTDOWN:
             reset_val = PON_RESET_CFG_WARM_RESET_AND_SHUTDOWN_VAL;
             break;

          case PM_PON_RESET_CFG_WARM_RESET_THEN_HARD_RESET:
             reset_val = PON_RESET_CFG_WARM_RESET_THEN_HARD_RESET_VAL;
             break;

          case PM_PON_RESET_CFG_WARM_RESET_THEN_D_VDD_BATT_REMOVE_HARD_RESET:
             reset_val = PON_RESET_CFG_WARM_RESET_THEN_DVDD_HARD_RESET_VAL;
             break;

          case PM_PON_RESET_CFG_WARM_RESET_THEN_X_VDD_COIN_CELL_REMOVE_HARD_RESET:
             reset_val = PON_RESET_CFG_WARM_RESET_THEN_XVDD_HARD_RESET_VAL;
             break;

          default:
             /* We should never get here as the check is already made */
             return PM_ERR_FLAG__PAR5_OUT_OF_RANGE;
       }

       /* Configure reset value */
       err_flag = local_comm->WriteByteMask(local_comm, PMIO_PON_PMIC_WD_RESET_S2_CTL_ADDR, 0x0F, reset_val, 0);

       if (err_flag != PM_ERR_FLAG__SUCCESS)
       {
          return err_flag;
       }

       /* Enable PMIC Wdog back if it was already enabled */
       if (wdog_enable)
       {
          /* Enable PMIC Wdog */
          err_flag = local_comm->WriteByteMask(local_comm, PMIO_PON_PMIC_WD_RESET_S2_CTL2_ADDR, 0x80, 0xFF, 0);

          if (err_flag != PM_ERR_FLAG__SUCCESS)
          {
             return err_flag;
          }
       }
    }

    return err_flag;
}

pm_err_flag_type pm_pon_wdog_enable(uint8 pmic_index, pm_on_off_type enable)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_pon_data_type *pon_ptr = pm_pon_get_data(pmic_index);
    pm_register_data_type wdog_enable = 0;
    pmiC_IComm *local_comm = NULL;

    if ((pon_ptr == NULL) || (pon_ptr->periph_exists == FALSE))
    { 
       err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
       local_comm = pon_ptr->comm_ptr;

       wdog_enable = (enable == PM_ON) ? 0xFF : 0;

       /* Enable or Disable PMIC Wdog */
       err_flag = local_comm->WriteByteMask(local_comm, PMIO_PON_PMIC_WD_RESET_S2_CTL2_ADDR, 0x80, wdog_enable, 0);
    }

    return err_flag;
}

pm_err_flag_type pm_pon_wdog_pet(uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_pon_data_type *pon_ptr = pm_pon_get_data(pmic_index);
    pmiC_IComm *local_comm = NULL;

    if ((pon_ptr == NULL) || (pon_ptr->periph_exists == FALSE))
    { 
       err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
       local_comm = pon_ptr->comm_ptr;

       /* Writing 0x1 to the wdog_pet addr clears the PMIC Watchdog timer */
       err_flag = local_comm->WriteByte(local_comm, PMIO_PON_PMIC_WD_RESET_PET_ADDR, 0x01, 0);

       /* Requires 5 sleep clock cycles of delay */
       DALSYS_BusyWait(150);
    }

    return err_flag;
}

pm_err_flag_type pm_pon_qdss_set_flag(uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_pon_data_type *pon_ptr = pm_pon_get_data(pmic_index);
    pmiC_IComm *local_comm = NULL;

    if ((pon_ptr == NULL) || (pon_ptr->periph_exists == FALSE))
    { 
       err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
       local_comm = pon_ptr->comm_ptr;

       /* Writing 0x02 to the DVDD_SPARE reg bit<1> to set the QDSS debug flag */
       err_flag = local_comm->WriteByteMask(local_comm, PMIO_PON_DVDD_RB_SPARE_ADDR, 0x02, 0xFF, 0);
    }

    return err_flag;
}

pm_err_flag_type pm_pon_qdss_clear_flag(uint8 pmic_index)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_pon_data_type *pon_ptr = pm_pon_get_data(pmic_index);
    pmiC_IComm *local_comm = NULL;

    if ((pon_ptr == NULL) || (pon_ptr->periph_exists == FALSE))
    { 
       err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else
    {
       local_comm = pon_ptr->comm_ptr;

       /* Writing 0x00 to the DVDD_SPARE reg bit<1> to clear the QDSS debug flag */
       err_flag = local_comm->WriteByteMask(local_comm, PMIO_PON_DVDD_RB_SPARE_ADDR, 0x02, 0x00, 0);
    }

    return err_flag;
}

pm_err_flag_type pm_pon_qdss_get_flag(uint8 pmic_index, boolean *status)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_pon_data_type *pon_ptr = pm_pon_get_data(pmic_index);
    pm_register_data_type data = 0;
    pmiC_IComm *local_comm = NULL;

    if ((pon_ptr == NULL) || (pon_ptr->periph_exists == FALSE))
    { 
       err_flag = PM_ERR_FLAG__FEATURE_NOT_SUPPORTED;
    }
    else if (status == NULL)
    {
       err_flag = PM_ERR_FLAG__INVALID_POINTER;
    }
    else
    {
       local_comm = pon_ptr->comm_ptr;

       /* Read the DVDD_SPARE reg bit<1> to get the QDSS debug flag status */
       err_flag = local_comm->ReadByteMask(local_comm, PMIO_PON_DVDD_RB_SPARE_ADDR, 0x02, &data);

       *status = data ? TRUE : FALSE;
    }

    return err_flag;
}

