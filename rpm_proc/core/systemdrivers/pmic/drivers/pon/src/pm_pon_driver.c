/*! \file pm_pon_driver.c
*  \n
*  \brief This file contains PON peripheral driver initialization during which the driver
*         driver data is stored.
*  \n
*  &copy; Copyright 2014 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/drivers/pon/src/pm_pon_driver.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
01/20/14   kt      Created.
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_pon_driver.h"
#include "CoreVerify.h"

/*===========================================================================

                        STATIC VARIABLES 

===========================================================================*/

/* Static global variable to store the RTC data */
static pm_pon_data_type pm_pon_data_arr[PM_MAX_NUM_DEVICES];

/*===========================================================================

                        FUNCTION DEFINITIONS

===========================================================================*/

void pm_pon_driver_init(pmiC_IComm *comm_ptr)
{
    DeviceIndex pmic_index = PM_COMM_DEVIDX(comm_ptr);

    CORE_VERIFY(pmic_index < PM_MAX_NUM_DEVICES);

    if (pmic_index == 0)
    {
       pm_pon_data_arr[pmic_index].periph_exists = TRUE;
       pm_pon_data_arr[pmic_index].comm_ptr = comm_ptr;
    }
}

pm_pon_data_type* pm_pon_get_data(uint8 pmic_index)
{
    if(pmic_index < PM_MAX_NUM_DEVICES)
    {
        return &pm_pon_data_arr[pmic_index];
    }

    return NULL;
}
