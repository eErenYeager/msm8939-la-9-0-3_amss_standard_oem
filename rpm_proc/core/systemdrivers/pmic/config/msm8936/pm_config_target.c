/*! \file
 *
 *  \brief  pm_target_config.c ----This file contains customizable target specific driver settings & PMIC registers.
 *  \details This file contains customizable target specific
 * driver settings & PMIC registers. This file is generated from database functional
 * configuration information that is maintained for each of the targets.
 *
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Resource Setting Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Software Register Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Processor Allocation Information Version: VU.Please Provide Valid Label - Not Approved
 *    This file contains code for Target specific settings and modes.
 *
 *  &copy; Copyright 2010 - 2013 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/config/msm8936/pm_config_target.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/10/14   mr      Revert RF_CLK1 and RF_CLK2 Always ON WA (CR-736518)
09/16/14   rk      NPA PAM settings and TCRSS change for DPM2.0  (WTR4905 RF Card) (CR-723681)
09/15/14   rk      RF_CLK1 and RF_CLK2 to be Always ON except  in system sleep (CR-724326)
09/12/14   rk      Fix the headroom issue with S3 BUCK on PM8916 pmic (CR-723620)
07/01/14   xmp     Set S3 frequency to 1.6M instead 3.2M (CR-686563)
06/27/14   rk      Vdd_Min not happening on 8936 due to wrong LDO parameters fetched (CR -686692)
06/09/14   mr      Keep L7 always ON and change L11 Min mode to IPEAK (CR-677315)
05/29/14   rk      8916: LDO7 to be set to LPM mode (CR - 671171)
03/05/14   akt     Make L2 always on in RPM (CR-626650)
10/09/13   rk      renaming the ult buck range and volt struct names.
06/04/13   aks     Remove range related hacks
05/29/13   aks     Clk driver input check (do not disable critical clocks)
04/23/13   hs      Fixed the naming convention in \config.
04/16/13   hs      Added voltage and range look up tables for L1.
04/12/13   hs      Code refactoring.
04/11/13   ps      Updated LDO NMOS tables
04/10/13   aks     Making s3 access allowed as per PDM spec
02/27/13   hs      Code refactoring.
08/07/12   hs      Added support for 5V boost.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES

===========================================================================*/
#include "pmapp_npa.h"
#include "pm_npa.h"
#include "pm_target_information.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "pm_rpm_smps_trans_apply.h"
#include "pm_rpm_clk_buffer_trans_apply.h"
#include "pm_config_target.h"
#include "pm_config.h"


extern pm_pwr_volt_info_type ult_buck_volt_1[];
extern pm_pwr_volt_info_type ult_buck_volt_2[];
extern pm_pwr_volt_info_type nmos_volt[];
extern pm_pwr_volt_info_type ult_nmos_volt[];

extern pm_pwr_settling_time_info_type smps_settling_time[];
extern pm_pwr_settling_time_info_type ldo_settling_time[];

pm_rpm_ldo_rail_info_type ldo_rail_a[NUM_OF_LDO_A] =
{
    {5, 63,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 1000, 1288}, // LDO1   N300_Stepper
    {5, 63,   0, PM_ACCESS_ALLOWED, PM_ALWAYS_ON,       PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 1200, 1300}, // LDO2     N600_Stepper
    {5, 63,   0, PM_ACCESS_ALLOWED, PM_ALWAYS_ON,       PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 750,  1288}, // LDO3     N1200_Stepper
    {5, 150,  0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 1800, 2100}, // LDO4     LV_P300
    {5, 250,  0, PM_ACCESS_ALLOWED, PM_ALWAYS_ON,       PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 1800, 1800}, // LDO5     LV_P300
    {5, 250,  0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 1800, 1800}, // LDO6     LV_P300
    {5, 250,  0, PM_ACCESS_ALLOWED, PM_ALWAYS_ON,       PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 1800, 1900}, // LDO7     LV_P150
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 2900, 2900}, // LDO8     P600
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 3000, 3300}, // LDO9     P600
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 2800, 2800}, // LDO10    P150
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 2950, 2950}, // LDO11    P600
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 1800, 2950}, // LDO12    P150
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 3075, 3075}, // LDO13    P50
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 1800, 3300}, // LDO14    P50
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 1800, 3300}, // LDO15    P50
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__IPEAK, PM_NPA_BYPASS_DISALLOWED, 1800, 3300}, // LDO16    P50
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 2850, 3000}, // LDO17    P600
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 2700, 2700}, // LDO18    P150
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 1740, 2150}, // LDO19    CLK LD - - LN_LDO  Ned to check the curent raing
    {5, 50,   0, PM_ACCESS_ALLOWED, PM_NONE,            PM_NPA_SW_MODE_LDO__NPM,   PM_NPA_BYPASS_DISALLOWED, 1740, 2150}, // LDO20    CLK LDO - LN_LDO  Ned to check the curent raing

};

pm_rpm_smps_rail_info_type smps_rail_a[NUM_OF_SMPS_A] =
{
    {300, 0, PM_ACCESS_ALLOWED,    PM_ALWAYS_ON,   PM_NPA_SW_MODE_SMPS__NPM, PM_CLK_3p2_MHz, PM_CLK_3p2_MHz,  500, 1288}, //HFS1
    {300, 1, PM_ACCESS_DISALLOWED, PM_NONE,        PM_NPA_SW_MODE_SMPS__NPM, PM_CLK_3p2_MHz, PM_CLK_3p2_MHz,  900, 1350}, //FTS2
    {300, 0, PM_ACCESS_ALLOWED,    PM_ALWAYS_ON,   PM_NPA_SW_MODE_SMPS__NPM, PM_CLK_1p6_MHz, PM_CLK_1p6_MHz, 1250, 1350}, //HFS3
    {300, 0, PM_ACCESS_ALLOWED,    PM_ALWAYS_ON,   PM_NPA_SW_MODE_SMPS__NPM, PM_CLK_1p6_MHz, PM_CLK_1p6_MHz, 1850, 2150}  //HFS4
};

pm_rpm_clk_info_type clk_info_a[PM_MAX_NUM_CLKS - 1]=
{
    //{0, 0}, //XO *managed by SBL settings
    {0, 1}, //BB_CLK1 *PBS puts in PIN control
    {1, 0}, //BB_CLK2
    {1, 0}, //EMPTY
    {1, 0}, //RF_CLK1
    {1, 0}, //RF_CLK2
    {1, 0}, //RF_CLK3
    {1, 0}, //DIFF_CLK1
    {1, 0}, //EMPTY
    {1, 0}, //CLK_DIST *not supported
    {0, 1}, //SLEEP_CLK
    {0, 1}, //DIV_CLK1
    {0, 1}, //DIV_CLK2
    {0, 1}, //DIV_CLK3

};


pm_pwr_resource_info_type mx_rail[1] =
{
    {RPM_LDO_A_REQ, PM_HW_MODULE_ULT_LDO, 3, ult_nmos_volt, ldo_settling_time}
};
pm_pwr_resource_info_type cx_rail[1] =
{
    {RPM_SMPS_A_REQ, PM_HW_MODULE_ULT_BUCK, 2, ult_buck_volt_1, smps_settling_time }
};
#if 0
sleep_register_type sleep_enter_info[] =
{
};

sleep_register_type sleep_exit_info[] =
{
};
#endif
