/*! \file
 *  
 *  \brief  pm_target_config.h ----This file contains target specific information.
 *  \details This file contains customizable target specific 
 * driver settings & PMIC registers. This file is generated from database functional
 * configuration information that is maintained for each of the targets.
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation Resource Setting Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Software Register Information Version: VU.Please Provide Valid Label - Not Approved
 *    PMIC code generation Processor Allocation Information Version: VU.Please Provide Valid Label - Not Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *  &copy; Copyright 2010 - 2013 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                        EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/config/msm8936/pm_config_target.h#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/23/13   hs      Fixed the naming convention in \config.
03/25/13   hs      Created.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/

#define NUM_OF_PMICS                  1

#define NUM_OF_LDO_A                  20
#define NUM_OF_SMPS_A                 4
//#define NUM_OF_VS_A                   0
//#define NUM_OF_BOOST_A                0

#define NUM_OF_SMPS_B                 0
#define NUM_OF_BOOST_A                 0
#define NUM_OF_BOOST_B                 0





