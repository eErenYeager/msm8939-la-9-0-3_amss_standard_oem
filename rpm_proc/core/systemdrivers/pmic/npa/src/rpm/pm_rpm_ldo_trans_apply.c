/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

PMIC LDO Translation and Apply (Aggregate)Functionality

GENERAL DESCRIPTION
This file contains translation and aggregate functions for NPA Device layer

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2012           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/npa/src/rpm/pm_rpm_ldo_trans_apply.c#1 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/02/14   rk      PM8916: SW workaround for LDO11 damage issue with faulty uSD cards (CR - 686702)
03/04/14   rk      NULL Check for the APIs not supported
02/28/14   rk      Disable LDO byapass for LDO as it is not supported and increase the hr to 100 mv for 1.0 and 1.1 parts
01/13/14   rk      Updated to Allow voltage votes to be always aggregated
09/10/13   rk      Adding settling time for regulators.
09/10/13   rk      PMIC SWEVENT revise.
09/10/13   rk     Code Refactoring: Removing PMIC_SubRsc from pwr algs   
09/10/13   rk      Fixed the naming convention in \config.
09/10/13   rk      Code refactoring.
09/03/12   rk      Removed pin controlled functionality.
02/27/13   hs      Code refactoring.
02/01/12   hs      Added support for input validation. 
01/22/13   dy      Merge code for common code base
07/20/12   hs      Added settling time.
04/16/12   hs      Removed pm_vreg_internal.h.
04/12/12   hs      Fixed the launching of StartTransaction.
                   Fixed the internal resource indexing.
01/26/12   wra     Created.
===========================================================================*/
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "npa.h"
#include "rpmserver.h"
#include "pm_npa.h"

#include "pm_target_information.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "pm_npa_device_ldo.h"
#include "pm_version.h"
#include "CoreVerify.h"
#include "Chipinfo.h"


/* Minimim wait time to poll for VREG_OK after parallel settling is done */
#define PM_RPM_MIN_WAIT_TIME_POST_SETTLE  25 //uS

static pm_model_type pm_chip_model_id = PMIC_IS_UNKNOWN;

/* MSM or MDM chip family */
static ChipInfoFamilyType chip_family = CHIPINFO_FAMILY_UNKNOWN;

static pm_npa_ldo_data_type RPM_LDO_A_REQ_data;
static pm_npa_ldo_data_type RPM_LDO_B_REQ_data;

void pm_rpm_ldo_translation(rpm_translation_info *pm_rpm_ldo_translation_info);
void pm_rpm_ldo_apply(rpm_application_info *pm_rpm_ldo_application_info);
void pm_rpm_ldo_pre_dependency_analysis(rpm_application_info *info, pm_npa_ldo_int_rep *previous_aggregation);
void pm_rpm_ldo_post_dependency_analysis(rpm_application_info *info, pm_npa_ldo_int_rep *previous_aggregation);
int pm_rpm_ldo_aggregate(rpm_application_info *info);
void pm_rpm_ldo_execute_driver(rpm_application_info *info, pm_npa_ldo_int_rep *previous_aggregation);
void pm_rpm_ldo_settle(rpm_application_info *info, pm_npa_ldo_int_rep *previous_aggregation);

void pm_rpm_ldo_translation(rpm_translation_info *info)
{
    unsigned type, length, *value;

    ((pm_npa_ldo_int_rep *)(info->dest_buffer))->resource_id = info->id;
    ((pm_npa_ldo_int_rep *)(info->dest_buffer))->device_index = ((pm_npa_ldo_data_type*)info->cb_data)->resourceType == RPM_LDO_A_REQ ? 0: 1;
    ((pm_npa_ldo_int_rep *)(info->dest_buffer))->reserve1 = 0;

    while(kvp_get(info->new_kvps, &type, &length, (const char **)&value))
    {
        // Need size to match.
        if(sizeof(npa_resource_state) != length)
            continue;

        switch(type)
        {
        case PM_NPA_KEY_SOFTWARE_ENABLE:
            {
                ((pm_npa_ldo_int_rep *)(info->dest_buffer))->sw_en = *value;

                break;
            }
        case PM_NPA_KEY_LDO_SOFTWARE_MODE:
            {
                ((pm_npa_ldo_int_rep *)(info->dest_buffer))->ldo_sw_mode = *value;
                break;
            }
        case PM_NPA_KEY_PIN_CTRL_ENABLE:
            {
                ((pm_npa_ldo_int_rep *)(info->dest_buffer))->pc_en = *value;
                break;
            }
        case PM_NPA_KEY_PIN_CTRL_POWER_MODE:
            {
                ((pm_npa_ldo_int_rep *)(info->dest_buffer))->pc_mode = *value;
                break;
            }
        case PM_NPA_KEY_MICRO_VOLT:
            {
                ((pm_npa_ldo_int_rep *)(info->dest_buffer))->bypass_uv = *value;
                ((pm_npa_ldo_int_rep *)(info->dest_buffer))->regulated_uvol = *value;
                break;
            }
        case PM_NPA_KEY_CURRENT:
            {
                ((pm_npa_ldo_int_rep *)(info->dest_buffer))->ip = *value;
                break;
            }
        case PM_NPA_KEY_HEAD_ROOM:
            {
                ((pm_npa_ldo_int_rep *)(info->dest_buffer))->noise_hr = *value;
                break;
            }
        case PM_NPA_KEY_BYPASS_ALLOWED_KEY:
            {
                ((pm_npa_ldo_int_rep *)(info->dest_buffer))->byp_allowed = *value;
                break;
            }
        default:
            {
                //should never go here
            }
        }
    }
}

void pm_rpm_ldo_apply(rpm_application_info *info)
{
    int change_detected = 0;
    pm_npa_ldo_int_rep     previous_aggregation = *((pm_npa_ldo_int_rep*)(info->current_aggregation));
    uint64                 settling_time = 0;
    pm_npa_ldo_data_type  *ldo_data = (pm_npa_ldo_data_type  *)info->cb_data;
    uint8                  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;

    PM_OFFTARGET_SWEVENT(PMIC_LDO_START_APPLY, info->id, (((pm_npa_ldo_data_type*)info->cb_data)->resourceType == RPM_LDO_A_REQ ? 0: 1), 0 , NULL);
    PM_DEBUG_SWEVENT(PMIC_LDO_START_APPLY, info->id, (((pm_npa_ldo_data_type*)info->cb_data)->resourceType == RPM_LDO_A_REQ ? 0: 1));

    if(0 == info->settling_time) //  this is a new request
    {
    // Aggregate the results, but don't call driver
    change_detected = pm_rpm_ldo_aggregate(info);

    if(change_detected != 0)
    {
            pm_rpm_ldo_dependency_execute(info, &previous_aggregation);
            pm_rpm_ldo_settle(info, &previous_aggregation);
        }
        else
        {
            PM_OFFTARGET_SWEVENT(PMIC_LDO_NO_CHANGE, info->id, (((pm_npa_ldo_data_type*)info->cb_data)->resourceType == RPM_LDO_A_REQ ? 0: 1), 0, NULL);
            PM_DEBUG_SWEVENT(PMIC_LDO_NO_CHANGE, info->id, (((pm_npa_ldo_data_type*)info->cb_data)->resourceType == RPM_LDO_A_REQ ? 0: 1));
    }
    }
    else // this is an old request to check if the rail has settled
    {
        //take the current system time
        settling_time = time_service_now(); // in time tick

        if(settling_time < info->settling_time) // still need more time to settle
        {
            //get the remaining settling time
            settling_time = info->settling_time - settling_time; // in time tick.

            //convert from time tick to micro-second
            settling_time = pm_convert_timetick_to_time(settling_time); // in micro second

            if(settling_time < PM_RPM_SETTTLING_TIME_THRESHOLD) // if the remaining settling time is small enough, wait internally
    {
                // update the internal settling time
                ldo_data->settlingTime[internal_resource_index] = settling_time; 

                //settling
                pm_rpm_ldo_settle(info, &previous_aggregation);

                // reset the settling time
                info->settling_time = 0;
    }
        }
        else // settling time passed, the rail should already settled
        {
            // update the internal setting time to a minimum value to make sure vreg_ok is set
            // and rail is settled
            ldo_data->settlingTime[internal_resource_index] = PM_RPM_MIN_WAIT_TIME_POST_SETTLE;

            //settling
            pm_rpm_ldo_settle(info, &previous_aggregation);

            //reset the settling time
            info->settling_time = 0;

        }//end if(settling_time < info->settling_time)
    } //end if(0 == info->settling_time)
}

void pm_rpm_ldo_dependency_execute(rpm_application_info *info, pm_npa_ldo_int_rep *previous_aggregation)
{
        // Check the dependencies
    pm_rpm_ldo_pre_dependency_analysis(info, previous_aggregation);

    // Execute the driver calls once all dependencies have been executed  
    pm_rpm_ldo_execute_driver(info, previous_aggregation);

    // Check the dependencies
    pm_rpm_ldo_post_dependency_analysis(info, previous_aggregation);
}

int pm_rpm_ldo_aggregate(rpm_application_info *info)
{
    int change_detected = 0;
    unsigned volt_adj = 0; // for fuzz check

    // For each client aggregate the correct values.
    pm_npa_ldo_int_rep aggregatedIntRep = {0,0,0,0,0,0,0};

    // get the resource type from the info->cb_data
    pm_npa_ldo_data_type* ldo_data = (pm_npa_ldo_data_type*)info->cb_data;

    //pmiC_TargetSpecificSetting* ts = pmiC_TargetSpecificSetting_GetSingleton();
    unsigned safety_hr = ldo_data->railInfo[info->id-1].SafetyHeadRoom;
    unsigned numberOfClients = rpm_get_num_clients(info->resource_handle);
    unsigned clientCount = 0;
    unsigned clientType;
    unsigned *settingValue;
    void* state = NULL;
    pm_npa_ldo_int_rep* clientRequest;

    if(ldo_data->railInfo[info->id - 1].AccessAllowed)
    {
        aggregatedIntRep.resource_id = info->id;
        aggregatedIntRep.device_index = ldo_data->resourceType == RPM_LDO_A_REQ ? 0: 1;


        for(clientCount = 0; clientCount < numberOfClients; clientCount++)
        {
            rpm_get_current_request(info->resource_handle, clientCount, &clientType, (const void **)&state);        

            // EE Client and Internal LDO clients
            if( (clientType == 0) ||(clientType == PM_RPM_NPA_CLIENT_LDO_A_REQ) || (clientType == PM_RPM_NPA_CLIENT_LDO_B_REQ) )
            {
                
                if(info->client == clientCount)
                {
                    clientRequest = (pm_npa_ldo_int_rep *)info->new_state;
                }
                else
                {
                    clientRequest = (pm_npa_ldo_int_rep *)state;
                }

                if(clientRequest != NULL)
                {
                    PM_OFFTARGET_SWEVENT(PMIC_LDO_EE_REQUEST, PM_NPA_VREG_LDO, info->id, aggregatedIntRep.device_index, clientRequest);

                    // Aggregate Software Enable
                    aggregatedIntRep.sw_en          = MAX(aggregatedIntRep.sw_en,       clientRequest->sw_en);

                    // Aggregate Software Mode Enable
                    aggregatedIntRep.ldo_sw_mode        = MAX(aggregatedIntRep.ldo_sw_mode,       clientRequest->ldo_sw_mode);

                    // Aggregate ORed values of Pin Control Enable
                    aggregatedIntRep.pc_en     = aggregatedIntRep.pc_en | clientRequest->pc_en;

                    // Aggregate ORed values of Pin Control Power Mode
                    aggregatedIntRep.pc_mode = aggregatedIntRep.pc_mode | clientRequest->pc_mode;

                    // Aggregate Highest Bypass Voltage
                    aggregatedIntRep.bypass_uv = MAX(aggregatedIntRep.bypass_uv, clientRequest->bypass_uv);                
                    // Aggregate Highest Regulate Voltage
                    aggregatedIntRep.regulated_uvol = MAX(aggregatedIntRep.regulated_uvol, 
                        clientRequest->bypass_uv + (MAX(safety_hr, clientRequest->noise_hr) * 1000));

                    // Aggregate Summation of Current
                    aggregatedIntRep.ip             = aggregatedIntRep.ip + clientRequest->ip;

                    // Aggregate Headroom Voltage - I don't think we need to aggregate this. It is part of the
                    // regulated voltage aggregation.
                    aggregatedIntRep.noise_hr = MAX(aggregatedIntRep.noise_hr, clientRequest->noise_hr);

                    //// Aggregate Highest Bypass Voltage
                    //aggregatedIntRep.bypass_uv = MAX(aggregatedIntRep.bypass_uv, clientRequest->bypass_uv);                

                    // Aggregate Bypass Allowed
                    if (pm_chip_model_id == PMIC_IS_PM8019)
                    {
                        // Temporary workaround 
                        // per Arvindh, no bypass for bringup. This will be removed.
                        aggregatedIntRep.byp_allowed = PM_NPA_BYPASS_DISALLOWED;
                    }
                    else
                    {
                        aggregatedIntRep.byp_allowed = MAX(aggregatedIntRep.byp_allowed, clientRequest->byp_allowed);
                    }
                    
                }
            }
            else
            {
                if(info->client == clientCount)
                {
                    settingValue = (unsigned *)info->new_state;
                }
                else
                {
                  settingValue = state;
                }

                if(clientType == PM_RPM_NPA_INT_CLIENT_VOLTAGE_DEPENDENT) // Internal client that registers voltage vote with another resource
                { aggregatedIntRep.bypass_uv      = MAX(aggregatedIntRep.bypass_uv,       *settingValue); }
                else if(clientType == PM_RPM_NPA_INT_CLIENT_SW_ENABLE_DEPENDENT) // Internal client that registers voltage vote with another resource
                { aggregatedIntRep.sw_en          = MAX(aggregatedIntRep.sw_en,       *settingValue); }
                else if(clientType == PM_RPM_NPA_INT_CLIENT_SW_MODE_DEPENDENT) // Internal client that registers SW Mode vote with another resource
                { aggregatedIntRep.ldo_sw_mode    = MAX(aggregatedIntRep.ldo_sw_mode,       *settingValue); }
                else if(clientType == PM_RPM_NPA_INT_CLIENT_PIN_CONTROL_ENABLE_DEPENDENT) // Internal client that registers pc_en vote with another resource
                { aggregatedIntRep.pc_en     = aggregatedIntRep.pc_en | *settingValue; }
                else if(clientType == PM_RPM_NPA_INT_CLIENT_PIN_CONTROL_MODE_DEPENDENT) // Internal client that registers pc_mode vote with another resource
                { aggregatedIntRep.pc_mode = aggregatedIntRep.pc_mode | *settingValue; }
                else if(clientType == PM_RPM_NPA_INT_CLIENT_CURRENT_DEPENDENT) // Internal client that registers peak current vote with another resource
                { aggregatedIntRep.ip             = aggregatedIntRep.ip + *settingValue; }
                else if(clientType == PM_RPM_NPA_INT_CLIENT_HEADROOM_DEPENDENT) // Internal client that registers headroom voltage vote with another resource
                { aggregatedIntRep.noise_hr = MAX(aggregatedIntRep.noise_hr, *settingValue); }
            }
        }

        // input checking after aggregation.
        if(ldo_data->railInfo[info->id - 1].BypassDisallowed)
        {
          aggregatedIntRep.byp_allowed= PM_NPA_BYPASS_DISALLOWED;
        }
        if(ldo_data->railInfo[info->id - 1].AlwaysOn)
        {
        aggregatedIntRep.sw_en = 1;
        }
        if(aggregatedIntRep.ldo_sw_mode < ldo_data->railInfo[info->id - 1].MinPwrMode)
        {
            aggregatedIntRep.ldo_sw_mode = ldo_data->railInfo[info->id - 1].MinPwrMode;
        }

        if( (aggregatedIntRep.bypass_uv < ldo_data->railInfo[info->id - 1].MinVoltage*1000) )
        {
            volt_adj = ldo_data->railInfo[info->id - 1].MinVoltage*1000 - aggregatedIntRep.bypass_uv;

            aggregatedIntRep.bypass_uv = ldo_data->railInfo[info->id - 1].MinVoltage*1000;
            aggregatedIntRep.regulated_uvol = aggregatedIntRep.regulated_uvol + volt_adj;
        }
        else if(aggregatedIntRep.bypass_uv > ldo_data->railInfo[info->id - 1].MaxVoltage*1000)
        {
            volt_adj = aggregatedIntRep.bypass_uv - ldo_data->railInfo[info->id - 1].MaxVoltage*1000;

            aggregatedIntRep.bypass_uv = ldo_data->railInfo[info->id - 1].MaxVoltage*1000;
            aggregatedIntRep.regulated_uvol = aggregatedIntRep.regulated_uvol - volt_adj;
        }

        change_detected = pm_rpm_int_copy( &aggregatedIntRep, (pm_npa_ldo_int_rep *)info->current_aggregation, sizeof(pm_npa_ldo_int_rep));
    } // end access_allowed

    return change_detected;
}

void pm_rpm_ldo_pre_dependency_analysis(rpm_application_info *info, pm_npa_ldo_int_rep *previous_aggregation)
{
    pm_pwr_resource_dependency_info     *depInfo = NULL;
    pm_npa_ldo_int_rep  *cur_data = (pm_npa_ldo_int_rep*)info->current_aggregation;
    pm_npa_ldo_data_type     *cb_data = (pm_npa_ldo_data_type*)info->cb_data;

    PM_OFFTARGET_SWEVENT(PMIC_LDO_START_PRE_DEP, info->id, (cb_data->resourceType == RPM_LDO_A_REQ ? 0: 1), 0 , NULL);
    PM_DEBUG_SWEVENT(PMIC_LDO_START_PRE_DEP, info->id, cb_data->resourceType == RPM_LDO_A_REQ ? 0: 1);

    if((previous_aggregation->sw_en < cur_data->sw_en) || (cur_data->pc_en))
    {
        cur_data->is_en_transition = TRUE;
    }
    else
    {
        cur_data->is_en_transition = FALSE;
    }

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_LDO_A_REQ)
    {
        depInfo = &RPM_LDO_A_REQ_data.depInfo[info->id];
    }
    else if(cb_data->resourceType == RPM_LDO_B_REQ)
    {
        depInfo = &RPM_LDO_B_REQ_data.depInfo[info->id];
    }

    CORE_VERIFY_PTR(depInfo);
    if(depInfo->parent_source_dependency != NULL)
    {
        // Detect if this is a pre-operation change on the parent
        // These conditions are
        // If the required headroom is increased
        // If the voltage setting is increased
        // If the LDO goes from a disabled to an enabled state
        // If the LDO Pin Control goes from disabled to an enabled state
        // If the Bypass enable transitions from enable to disable
        if( (cur_data->regulated_uvol > previous_aggregation->regulated_uvol) || 
            (cur_data->bypass_uv > previous_aggregation->bypass_uv) ||
            (cur_data->is_en_transition == TRUE) ||
            (cur_data->ip > previous_aggregation->ip) ||
            (cur_data->byp_allowed > previous_aggregation->byp_allowed))
        {

            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_ldo_int_rep), cur_data);

        }
    }

    //Decode and execute preconditions
    //if(depInfo->pre_operational_dependencies != NULL)
    //{
    //    pm_rpm_execute_operational_conditions(depInfo->pre_operational_dependencies, PM_PWR_TIMING_TYPE__PRE);
    //}    

    PM_OFFTARGET_SWEVENT(PMIC_LDO_END_PRE_DEP, info->id, (cb_data->resourceType == RPM_LDO_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_LDO_END_PRE_DEP, info->id, cb_data->resourceType == RPM_LDO_A_REQ ? 0: 1);
}

void pm_rpm_ldo_post_dependency_analysis(rpm_application_info *info, pm_npa_ldo_int_rep *previous_aggregation)
{
    pm_pwr_resource_dependency_info           *depInfo = NULL;
    pm_npa_ldo_int_rep  *new_int_rep = (pm_npa_ldo_int_rep *)info->new_state;
    pm_npa_ldo_int_rep  *cur_data = (pm_npa_ldo_int_rep*)info->current_aggregation;
    pm_npa_ldo_data_type     *cb_data = (pm_npa_ldo_data_type*)info->cb_data;
    unsigned             change_detected = 0;

    PM_OFFTARGET_SWEVENT(PMIC_LDO_START_POST_DEP, info->id, (cb_data->resourceType == RPM_LDO_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_LDO_START_POST_DEP, info->id, cb_data->resourceType == RPM_LDO_A_REQ ? 0: 1);

    // Pin Control can cause voltage droop anytime, so don't allow bypass
    if(new_int_rep != NULL && new_int_rep->pc_en)
    {
        cur_data->is_en_transition = TRUE;
    }
    else
    {
        cur_data->is_en_transition = FALSE;
    }

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_LDO_A_REQ)
    {
        depInfo = &RPM_LDO_A_REQ_data.depInfo[info->id];
    }
    else if(cb_data->resourceType == RPM_LDO_B_REQ)
    {
        depInfo = &RPM_LDO_B_REQ_data.depInfo[info->id];
    }

	CORE_VERIFY_PTR(depInfo);
	
    if (depInfo->parent_source_dependency != NULL)
    {
        // Detect if this is a post-operation change on the parent
        // These conditions are
        // If the required headroom is decreased
        // If the voltage setting is decreased
        // If the LDO goes from a enabled to an disabled state
        // If the LDO Pin Control goes from enabled to an disabled state
        // If the Bypass enable transitions from enable to disable
        if( (cur_data->regulated_uvol <= previous_aggregation->regulated_uvol) || // "=" is to allow SMPS to turn
            (cur_data->bypass_uv <= previous_aggregation->bypass_uv) ||
            (cur_data->sw_en != previous_aggregation->sw_en) || 
            (cur_data->pc_en < previous_aggregation->pc_en) )
        {
            change_detected = 1;
        }

        if(1 == change_detected )
        {
            //save the LDO ID
            cur_data->resource_id = info->id;

            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_ldo_int_rep), cur_data);
        }
    }

    //Decode and execute preconditions
    //if(depInfo->post_operational_dependencies != NULL)
    //{
    //    pm_rpm_execute_operational_conditions(depInfo->post_operational_dependencies, PM_PWR_TIMING_TYPE__PRE);
    //}
    
    PM_OFFTARGET_SWEVENT(PMIC_LDO_END_POST_DEP, info->id, (cb_data->resourceType == RPM_LDO_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_LDO_END_POST_DEP, info->id, cb_data->resourceType == RPM_LDO_A_REQ ? 0: 1);
}

void pm_rpm_ldo_execute_driver(rpm_application_info *info, pm_npa_ldo_int_rep *previous_aggregation)
{
    pm_npa_ldo_int_rep *ldo_int_rep =(pm_npa_ldo_int_rep *)info->current_aggregation;
    pm_npa_ldo_data_type *ldo_data = (pm_npa_ldo_data_type *)info->cb_data;
    pm_ldo_data_type* ldo_ptr = NULL;
    uint8  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;
    unsigned lp_hp_current_threshold = 0;
    uint64 settling_time_vregOK = 0;
    uint64 settling_time_vregStepper = 0;
    uint64 settling_time = 0;
    uint8 pmic_index = (ldo_data->resourceType == RPM_LDO_A_REQ ? 0: 1);

    PM_OFFTARGET_SWEVENT(PMIC_LDO_AGG, PM_NPA_VREG_LDO, info->id, 
                         (((pm_npa_ldo_data_type*)info->cb_data)->resourceType == RPM_LDO_A_REQ ? 0: 1), info->current_aggregation);
    PM_DEBUG_SWEVENT(PMIC_LDO_AGG1, ((pm_npa_ldo_int_rep*)info->current_aggregation)->resource_id, 
            ((pm_npa_ldo_int_rep*)info->current_aggregation)->device_index, 
            ((pm_npa_ldo_int_rep*)info->current_aggregation)->sw_en,
            ((pm_npa_ldo_int_rep*)info->current_aggregation)->ldo_sw_mode);
    PM_DEBUG_SWEVENT(PMIC_LDO_AGG2, ((pm_npa_ldo_int_rep*)info->current_aggregation)->pc_en,
            ((pm_npa_ldo_int_rep*)info->current_aggregation)->pc_mode,
            ((pm_npa_ldo_int_rep*)info->current_aggregation)->is_en_transition,
            ((pm_npa_ldo_int_rep*)info->current_aggregation)->ip);
    PM_DEBUG_SWEVENT(PMIC_LDO_AGG3, ((pm_npa_ldo_int_rep*)info->current_aggregation)->regulated_uvol,
            ((pm_npa_ldo_int_rep*)info->current_aggregation)->bypass_uv,
            ((pm_npa_ldo_int_rep*)info->current_aggregation)->noise_hr,
            ((pm_npa_ldo_int_rep*)info->current_aggregation)->byp_allowed);

    //Get the PmicResource from the call back data.
    if(ldo_data->resourceType == RPM_LDO_A_REQ)
    {
        ldo_ptr = ldo_data->ldoDriverData;
    }
    else
    {
        // We should never get here
        PM_SWEVENT(PMIC_RPM_ERROR, 2, 1); //LDO ID is 2, 1 means NULL Call back data
        return;
    }

    if(ldo_ptr != NULL)
    {
        if(ldo_data->inBypass[internal_resource_index] == 0)//LDO is not in bypass
        {
            /* Set Power Mode */
            if(ldo_int_rep->ldo_sw_mode == PM_NPA_SW_MODE_LDO__IPEAK)
            {
                if( (ldo_int_rep->ldo_sw_mode != previous_aggregation->ldo_sw_mode) || (ldo_int_rep->ip != previous_aggregation->ip) )
                {
                    lp_hp_current_threshold = ldo_data->railInfo[internal_resource_index].LpHpCurrentThreshold;

                    if(ldo_int_rep->ip >= lp_hp_current_threshold)
                    {    
                        PM_OFFTARGET_SWEVENT(PMIC_LDO_POWER_MODE, 0, 0, 0, NULL);
                        PM_DEBUG_SWEVENT(PMIC_LDO_POWER_MODE, 0); // Mode numbers defined by pmic_parser.py

                        pm_ldo_sw_mode(pmic_index, internal_resource_index, PM_SW_MODE_NPM);
                    }
                    else
                    {
                        PM_OFFTARGET_SWEVENT(PMIC_LDO_POWER_MODE, 1, 0, 0, NULL);
                        PM_DEBUG_SWEVENT(PMIC_LDO_POWER_MODE, 1); // Mode numbers defined by pmic_parser.py

                        pm_ldo_sw_mode(pmic_index, internal_resource_index, PM_SW_MODE_LPM);
                    }
                }
            }
            else
            {
                if(ldo_int_rep->ldo_sw_mode != previous_aggregation->ldo_sw_mode) 
                {
                    PM_OFFTARGET_SWEVENT(PMIC_LDO_POWER_MODE, 2, 0, 0, NULL);
                    PM_DEBUG_SWEVENT(PMIC_LDO_POWER_MODE, 2); // Mode numbers defined by pmic_parser.py

                    pm_ldo_sw_mode(pmic_index, internal_resource_index, PM_SW_MODE_NPM);
                }
            }
        }

            /* Set the Voltage */
            if( (ldo_int_rep->bypass_uv != previous_aggregation->bypass_uv) && (ldo_int_rep->bypass_uv != 0) )
            {
            pm_pwr_volt_level_alg(&ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, internal_resource_index, 
                                                      (pm_volt_level_type)ldo_int_rep->bypass_uv);

                // voltage increases, adding settling time.
                if(ldo_int_rep->bypass_uv > previous_aggregation->bypass_uv)
                {
                   //calculate the settling time.
                    if(ldo_data->settlingTimeInfo->stepper_uv != 0)//the rail has a stepper
                    {
                        settling_time_vregStepper = ((ldo_int_rep->bypass_uv - previous_aggregation->bypass_uv)/ldo_data->settlingTimeInfo->stepper_uv)*ldo_data->settlingTimeInfo->stepper_us;
                    }
                    else // the rail doesn't have a stepper
                    {
                        settling_time_vregStepper = ldo_data->settlingTimeInfo->stepper_us;
            }
        }
            }
        
        /* Set PIN Control Enable */
        if(ldo_int_rep->pc_en != previous_aggregation->pc_en)
        {
            pm_pwr_pin_ctrl_alg(&ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, internal_resource_index, (uint8)ldo_int_rep->pc_en);    
        }

        /* Set PIN Control Power Mode */
        if(ldo_int_rep->pc_mode != previous_aggregation->pc_mode)
        {
            pm_pwr_pin_ctrl_mode_alg(&ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, internal_resource_index, (uint8)ldo_int_rep->pc_mode);

        }
        
        /* State set */
        if(ldo_int_rep->sw_en != previous_aggregation->sw_en)
        {
            // check the current state and compare to desired state
            // if we are transitioning from disable to enable state via SW mode
            // then we need  we need to check that other sub regulated LDOs on this SMPS
            // are not in Bypass.The power on current draw will cause the bypassed LDO voltage to droop.
            // General rule for enabling another sub regulated LDO on the same SMPS is 50mV head room.
            // If LDO is pin control enabled then other sub regulated LDOs should be taken out of bypass

            // the LDO is asked to turn off and is in bypass mode, then we need to first take it out of bypass, then turn it off
            // so the next time when it's askedt o turn on, it won't go to bypass directly
            if ( (ldo_int_rep->sw_en == 0) && (ldo_data->inBypass[internal_resource_index] == 1) )
            {
                pm_ldo_sw_mode(pmic_index, internal_resource_index, PM_SW_MODE_NPM);

                ldo_data->inBypass[internal_resource_index] = 0;
            }

            pm_pwr_sw_enable_alg(&ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, internal_resource_index, ( pm_on_off_type)ldo_int_rep->sw_en);
            
            if(ldo_int_rep->sw_en > previous_aggregation->sw_en)
            {
                //calculate the settling time.
                settling_time_vregOK = ldo_data->settlingTimeInfo->vreg_ok_us;
            }
        }

        // -------------- Settling time ------------------

        if(ldo_int_rep->sw_en > previous_aggregation->sw_en) // for rail turning on, inform the RPM server
        {
            //take the current system time
            settling_time = time_service_now() + pm_convert_time_to_timetick(settling_time_vregOK);

            // feedback to RPM server by modifying info
            info->settling_time = settling_time;

            /* Work-around for MSM8916/MSM8936 LDO11 voltage over-shoot issue seen */
            /* Disable LDO11 if it's not settled after making sure we internally settle for safe time (~300us) */
            if((internal_resource_index == 10) && 
               ((chip_family == CHIPINFO_FAMILY_MSM8916) || (chip_family == CHIPINFO_FAMILY_MSM8936) || (chip_family == CHIPINFO_FAMILY_MSM8929)))
            {
                /* Settle for around 300 micro secs internally */

                /* Busywait for 200 micro secs */
                DALSYS_BusyWait(200);

                /* Poll wait (VREG_OK) for 5*10 = 50 micro secs */
                ldo_data->settlingTime[internal_resource_index] = 5;

                /* Disable parallel settling for LDO11 */
                info->settling_time = 0;
            }
        }
        else if(ldo_int_rep->bypass_uv > previous_aggregation->bypass_uv) // voltage changes
        {
            if(settling_time_vregStepper > PM_RPM_SETTTLING_TIME_THRESHOLD) // if the settling time for the voltage change is greater than xxus, inform RPM server
            {
                //take the current system time
                settling_time = time_service_now() + pm_convert_time_to_timetick(settling_time_vregStepper);

                // feedback to RPM server by modifying info
                info->settling_time = settling_time;
            }
            else //otherwise wait internally
            {
                // update the interal setting time
                ldo_data->settlingTime[internal_resource_index] = settling_time_vregStepper;
            }
        }

        // -------------- Settling time ------------------

        ldo_int_rep->is_en_transition = 0; // This might need to get saved somehow.
    }
}

void pm_rpm_ldo_bypass_cb(uint32 device_index, uint32 resource_id, pm_on_off_type on_off)
{
    pm_npa_ldo_int_rep *ldo_int_rep = NULL;
    rpm_resource_type resource_type = RPM_LDO_A_REQ;
    pm_npa_ldo_data_type  *ldo_data = NULL;
    pm_sw_mode_type mode = PM_SW_MODE_NPM;

    resource_type = (device_index == 0 ? RPM_LDO_A_REQ : RPM_LDO_B_REQ);
    ldo_data = (device_index == 0 ? &RPM_LDO_A_REQ_data : &RPM_LDO_B_REQ_data);

    rpm_get_aggregated_request_buffer(resource_type, resource_id, (const void **)&ldo_int_rep);
    if ((ldo_data->inBypass[resource_id-1] != on_off) && /* the LDO's current bypass status needs to be changed. */
        (((pm_npa_ldo_int_rep *)ldo_int_rep)->sw_en > 0))/* the LDO currently is ON */
    {
        if((pm_on_off_type)on_off == PM_ON)
        {
            mode = PM_SW_MODE_BYPASS  ;
            PM_OFFTARGET_SWEVENT(PMIC_LDO_BYPASS_INFO, resource_id, device_index, 1, NULL);
            ldo_data->inBypass[resource_id-1] = PM_ON;
        }   
        else
        {
            PM_OFFTARGET_SWEVENT(PMIC_LDO_BYPASS_INFO, resource_id, device_index, 0, NULL);
            ldo_data->inBypass[resource_id-1] = PM_OFF;
        } 
        
        pm_ldo_sw_mode(device_index, (uint8)resource_id-1, mode);
    }    

}

void pm_rpm_ldo_register_resources(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    pm_npa_ldo_data_type *ldo_data = NULL;
    uint32 dep_prop_id_arr[] = {PM_PROP_LDOA_DEP, PM_PROP_LDOB_DEP};
    uint32 rail_prop_id_arr[] = {PM_PROP_LDOA_RAIL, PM_PROP_LDOB_RAIL};
    uint32 pmic_index = (resourceType == RPM_LDO_A_REQ ? 0: 1);
    /* Backed out CR 589852
    uint32 ldo_index = 0;
    pm_ldo_data_type* ldo_ptr = NULL;
    uint8 range = 0; */

    CORE_VERIFY(pmic_index < (sizeof(dep_prop_id_arr)/sizeof(dep_prop_id_arr[0])));
    CORE_VERIFY(pmic_index < (sizeof(rail_prop_id_arr)/sizeof(rail_prop_id_arr[0])));

    if(num_npa_resources > 0)
    {
        pm_chip_model_id = pm_get_pmic_model(0);

        chip_family = Chipinfo_GetFamily();

        ldo_data = pm_rpm_ldo_get_resource_data(resourceType);
        if (ldo_data != NULL)
        {
            ldo_data->ldoDriverData = pm_ldo_get_data(pmic_index);

            CORE_VERIFY_PTR(ldo_data->ldoDriverData);


            ldo_data->resourceType = resourceType;
            ldo_data->depInfo = (pm_pwr_resource_dependency_info*)pm_target_information_get_specific_info(dep_prop_id_arr[pmic_index]);
            ldo_data->railInfo = (pm_rpm_ldo_rail_info_type*)pm_target_information_get_specific_info(rail_prop_id_arr[pmic_index]);
            pm_malloc(sizeof(boolean)*num_npa_resources, (void**)&(ldo_data->inBypass) );
            DALSYS_memset(ldo_data->inBypass, 0, sizeof(boolean)*num_npa_resources);
            ldo_data->settlingTimeInfo = (pm_pwr_settling_time_info_type*)pm_target_information_get_common_info(PM_PROP_LDO_SETTLING_TIME);
            pm_malloc(sizeof(uint32)*num_npa_resources, (void**)&(ldo_data->settlingTime) );
            DALSYS_memset(ldo_data->settlingTime, 0, sizeof(uint32)*num_npa_resources);

            rpm_register_resource(resourceType, num_npa_resources + 1, sizeof(pm_npa_ldo_int_rep), pm_rpm_ldo_translation, pm_rpm_ldo_apply, (void *)ldo_data);
        }

    }  
}

void pm_rpm_ldo_register_resource_dependencies(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    uint8 count = 0;
    pm_pwr_resource *dependency = NULL;
    pm_npa_ldo_int_rep *current_agg = NULL;
    pm_ldo_data_type* ldo_ptr = NULL;
    pm_volt_level_type vol = 0;
    pm_on_off_type en_status = PM_OFF;
    pm_pwr_resource_dependency_info    *depInfo = NULL;

    if(num_npa_resources > 0)
    {
        if(RPM_LDO_A_REQ == resourceType)
        {
            ldo_ptr = (pm_ldo_data_type*)RPM_LDO_A_REQ_data.ldoDriverData;
            depInfo = RPM_LDO_A_REQ_data.depInfo;
        }
        else if(RPM_LDO_B_REQ == resourceType)
        {
            ldo_ptr = (pm_ldo_data_type*)RPM_LDO_B_REQ_data.ldoDriverData;
            depInfo = RPM_LDO_B_REQ_data.depInfo;
        }
        CORE_VERIFY_PTR(ldo_ptr);

	for(count = 1; count < num_npa_resources + 1; count++)
        {
           rpm_get_aggregated_request_buffer(resourceType, count, (const void**)&current_agg);
            if(current_agg != NULL)
            {
                DALSYS_memset(current_agg, 0, sizeof(pm_npa_ldo_int_rep));

                pm_pwr_volt_level_status_alg(&ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, count-1, &vol);
                current_agg->bypass_uv = vol;
                pm_pwr_sw_enable_status_alg(&ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, count-1, &en_status);
                current_agg->sw_en = en_status;
                current_agg->ldo_sw_mode = 0x0F; // initialize it to some invalid setting in order to make sure that the first vote will be granted.

                if(RPM_LDO_A_REQ == resourceType)
                {
                    current_agg->device_index = PmDeviceIndex1;
                }
                else if(RPM_LDO_B_REQ == resourceType)
                {
                    current_agg->device_index = PmDeviceIndex2;
                }

                current_agg->resource_id = count; // 1 based.
            }
        }

        if(depInfo)
        {
            for(count = 1; count < num_npa_resources + 1; count++)
            {
                if(depInfo[count].parent_source_dependency != NULL)
                {
                    dependency = depInfo[count].parent_source_dependency;

                    // create the internal client
                    rpm_register_internal_client_type(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_LDO_DEPENDENT, sizeof(pm_npa_ldo_int_rep));
                    
                    depInfo[count].parent_source_dependency_client_handle = 
                                 rpm_create_client(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_LDO_DEPENDENT);                   
                }

                //pm_rpm_register_operational_dependency_int_client(&depInfo[count]);
            }
        }
    }  
}


pm_npa_ldo_data_type* pm_rpm_ldo_get_resource_data(rpm_resource_type resource)
{
    if(RPM_LDO_A_REQ == resource)
    {
        return &RPM_LDO_A_REQ_data;
    }
    else if(RPM_LDO_B_REQ == resource)
    {
        return &RPM_LDO_B_REQ_data;
    }
    else
    {
        return NULL;
    }
}

void pm_rpm_ldo_settle(rpm_application_info *info, pm_npa_ldo_int_rep *previous_aggregation)
{
    pm_npa_ldo_int_rep *ldo_int_rep =(pm_npa_ldo_int_rep *)info->current_aggregation;
    pm_npa_ldo_data_type *ldo_data = (pm_npa_ldo_data_type *)info->cb_data;
    uint8  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;
    uint32 settling_time = ldo_data->settlingTime[internal_resource_index];
    pm_ldo_data_type* ldo_ptr = NULL;
    uint8 pmic_index = (ldo_data->resourceType == RPM_LDO_A_REQ ? 0: 1);
    boolean vreg_status = FALSE;

    if( settling_time != 0)
    {
        PM_OFFTARGET_SWEVENT(PMIC_LDO_START_SETTLING, info->id, (ldo_data->resourceType == RPM_LDO_A_REQ ? 0: 1), settling_time, NULL);
        PM_DEBUG_SWEVENT(PMIC_LDO_START_SETTLING, info->id, (ldo_data->resourceType == RPM_LDO_A_REQ ? 0: 1), settling_time);

        //Get the PmicResource from the call back data.
        if( (ldo_data->resourceType == RPM_LDO_A_REQ) || (ldo_data->resourceType == RPM_LDO_B_REQ) )
        {
            ldo_ptr = ldo_data->ldoDriverData;
		}

        CORE_VERIFY_PTR(ldo_ptr);
            
        if( (PM_HW_MODULE_LDO_N600_STEPPER == ldo_ptr->periph_subtype[internal_resource_index]) ||
            (PM_HW_MODULE_LDO_N1200_STEPPER == ldo_ptr->periph_subtype[internal_resource_index]) ||
            (PM_HW_MODULE_LDO_N900_STEPPER == ldo_ptr->periph_subtype[internal_resource_index]) ||
            (PM_HW_MODULE_LDO_N300_STEPPER == ldo_ptr->periph_subtype[internal_resource_index]) )
            {
                 // poll STEPPER_DONE
                pm_rpm_check_stepper_settle_status(settling_time, &ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, internal_resource_index);

            // poll VREG_OK
            vreg_status = pm_rpm_check_vreg_settle_status(settling_time, &ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, internal_resource_index);
           }
            else
            {
                // poll VREG_OK
            vreg_status = pm_rpm_check_vreg_settle_status(settling_time, &ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, internal_resource_index);
        }

        /* Work-around for MSM8916/MSM8936 LDO11 voltage over-shoot issue seen */
        /* Disable LDO11 if it's not settled after making sure we internally settle for safe time (~300us) */
        if((internal_resource_index == 10) && (ldo_int_rep->sw_en == 1) &&
           ((chip_family == CHIPINFO_FAMILY_MSM8916) || (chip_family == CHIPINFO_FAMILY_MSM8936) || (chip_family == CHIPINFO_FAMILY_MSM8929)))
        {
           /* Check if VREG_OK is not set and disable the rail after settling is done */
           if(vreg_status == FALSE)
           {
              /* Make sure the LDO is taken out of bypass before disabling the rail */
              if (ldo_data->inBypass[internal_resource_index] == 1)
              {
                 pm_ldo_sw_mode(pmic_index, internal_resource_index, PM_SW_MODE_NPM);

                 ldo_data->inBypass[internal_resource_index] = 0;
              }

              /* Disable the LDO since VREG_OK is not set */
              pm_pwr_sw_enable_alg(&ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, internal_resource_index, PM_OFF);

              /* Update the current aggregation sw_en bit */
              ldo_int_rep->sw_en = 0;
            }
        }

        // reset the settling time
        ldo_data->settlingTime[internal_resource_index] = 0;

        PM_OFFTARGET_SWEVENT(PMIC_LDO_END_SETTLING, info->id, (ldo_data->resourceType == RPM_LDO_A_REQ ? 0: 1), settling_time, NULL);
        PM_DEBUG_SWEVENT(PMIC_LDO_END_SETTLING, info->id, (ldo_data->resourceType == RPM_LDO_A_REQ ? 0: 1));
    }
}

