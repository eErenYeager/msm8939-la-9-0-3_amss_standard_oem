/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

PMIC RPM Railway Functionality

GENERAL DESCRIPTION
This file contains utility functions for trans apply aggregation layer

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2012           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/npa/src/rpm/pm_rpm_railway.c#1 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
02/28/14   rk      Disable LDO byapass for Mx as it is not supported and increase the hr to 100 mv for 1.0 and 1.1 parts
02/21/14   sv      Updated generic api to obtain vset value for regulators. (CR-549436)
09/11/13   rk     Disable BYPASS for LDO3 for 8x26.  
09/11/13   rk      Added the alg for settling time. 
09/11/13   rk     Added api for railway to get vset and range info
09/11/13   rk     Code Refactoring: Removing PMIC_SubRsc from pwr algs 
09/11/13   rk      Fixed the naming convention in \config.
09/11/13   rk      Code refactoring.
09/11/13   rk      Add support for SW mode.
05/21/13   hs      Added support for FTS mode transition.
05/16/13   hs      Added the alg Vstepper sequencing change 
                   needed for Auto configured FTS2. 
02/27/13   hs      Code refactoring.
11/11/12   hs      Created.
===========================================================================*/
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "pmapp_npa.h"
#include "pm_target_information.h"
#include "pm_smps_driver.h"
#include "pm_ldo_driver.h"
#include "pm_version.h"
#include "spmi_1_0_lite.h" 
#include "pm_version.h"
#include "pm_rpm_smps_trans_apply.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "pm_pwr_alg.h"

#define PM_RAILWAY_SETTLING_TIME_PERIODIC         5 //uS
#define PM_RAILWAY_FTS_DELAY                      50 //uS
#define PM_RAILWAY_LDO_SAFETY_HEADROOM            50000 //uV
#define PM_RAILWAY_FTS_SETTLING_TIME              350 //uS
#define PM_RAILWAY_HFS_SETTLING_TIME              500 //uS
#define PM_RAILWAY_ULT_SETTLING_TIME              500 //uS
#define PM_RAILWAY_LDO_SETTLING_TIME              500 //us


// Function Pointers for Virtual Methods
typedef unsigned (*pm_func_railway_set_voltage)(pm_railway_type_info_type rail, unsigned voltage_uv);
typedef unsigned (*pm_func_railway_set_mode)(pm_railway_type_info_type rail, pm_sw_mode_type  mode);

typedef struct 
{
    unsigned volt_uv:28;
    unsigned sw_en:1;
    unsigned sw_mode:3;
    unsigned byp_allowed:1;
    unsigned regulated_uvol:28;
}pm_railway_state_info_type;

typedef struct 
{
    // Function Pointers for Virtual Methods
    pm_func_railway_set_voltage      set_voltage;
    pm_func_railway_set_mode         set_mode;
   
   /********************************************
             DATA MEMBER DECLARATION
   *********************************************/
   pm_pwr_resource_info_type*        resource;
   void*                             rpm_cb_data;
   pm_railway_state_info_type        pre_state;
   uint32                            settling_time;
}pm_railway_type;

static pm_railway_type pmRailway[PM_RAILWAY_INVALID];

static pm_npa_smps_int_rep  smps_cur_data;
static pm_npa_ldo_int_rep   ldo_cur_data;

static pm_npa_smps_int_rep  smps_shadow_data;
static pm_npa_ldo_int_rep   ldo_shadow_data;
static rpm_application_info rpmInfo;


static void pm_rpm_railway_init_i(pm_railway_type_info_type rail);
static unsigned pm_railway_ldo_set_voltage_i(pm_railway_type_info_type rail, unsigned voltage_uv);
static unsigned pm_railway_ldo_set_mode_i(pm_railway_type_info_type rail, pm_sw_mode_type  mode);
static unsigned pm_railway_smps_set_voltage_i(pm_railway_type_info_type rail, unsigned voltage_uv);
static unsigned pm_railway_smps_set_mode_i(pm_railway_type_info_type rail, pm_sw_mode_type  mode);

void pm_rpm_railway_init(void)
{
    pmRailway[PM_RAILWAY_MX].resource = (pm_pwr_resource_info_type*)pm_target_information_get_specific_info(PM_PROP_MX);
    pm_rpm_railway_init_i(PM_RAILWAY_MX);

    pmRailway[PM_RAILWAY_CX].resource = (pm_pwr_resource_info_type*)pm_target_information_get_specific_info(PM_PROP_CX);
    pm_rpm_railway_init_i(PM_RAILWAY_CX);

    smps_cur_data.global_byp_en = 1;
}

void pm_rpm_railway_init_i(pm_railway_type_info_type rail)
{
    pm_volt_level_type vol = 0;
    pm_on_off_type en_status;
    pm_sw_mode_type mode_status;
    uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
    switch(pmRailway[rail].resource->resource_type)
    {
        case RPM_LDO_A_REQ:
        case RPM_LDO_B_REQ:
        {
            pm_ldo_data_type* ldo_ptr = NULL; 

            pmRailway[rail].set_voltage = pm_railway_ldo_set_voltage_i;
            pmRailway[rail].set_mode = pm_railway_ldo_set_mode_i;
            pmRailway[rail].rpm_cb_data = pm_rpm_ldo_get_resource_data(pmRailway[rail].resource->resource_type);
            ldo_ptr = ((pm_npa_ldo_data_type*)pmRailway[rail].rpm_cb_data)->ldoDriverData;

            pm_pwr_sw_enable_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, &en_status);
            pmRailway[rail].pre_state.sw_en = en_status;

            pm_pwr_sw_mode_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, &mode_status);
            pmRailway[rail].pre_state.sw_mode = mode_status;

            if(en_status) // only update the voltage when the rail is on, otherwise, put the voltage to 0.
            {
                pm_pwr_volt_level_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, &vol);
                pmRailway[rail].pre_state.volt_uv = vol;
                pmRailway[rail].pre_state.regulated_uvol = vol + PM_RAILWAY_LDO_SAFETY_HEADROOM;
            }
            else
            {
                pmRailway[rail].pre_state.volt_uv = 0;
                pmRailway[rail].pre_state.regulated_uvol = 0;
            }
#ifdef PMIC_FIXED_SETTLING_TIME
            pmRailway[rail].settling_time = PM_RAILWAY_LDO_SETTLING_TIME;
#endif
        }
        break;
        case RPM_SMPS_A_REQ:
        case RPM_SMPS_B_REQ:
        {
            pm_smps_data_type* smps_ptr = NULL; 

            pmRailway[rail].set_voltage = pm_railway_smps_set_voltage_i;
            pmRailway[rail].set_mode = pm_railway_smps_set_mode_i;
            pmRailway[rail].rpm_cb_data = pm_rpm_smps_get_resource_data(pmRailway[rail].resource->resource_type);
            smps_ptr = ((pm_npa_smps_data_type*)pmRailway[rail].rpm_cb_data)->smpsDriverData;

            pm_pwr_sw_enable_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, &en_status);
            pmRailway[rail].pre_state.sw_en = en_status;

            pm_pwr_sw_mode_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, &mode_status);
            pmRailway[rail].pre_state.sw_mode = mode_status;

            if(en_status) // only update the voltage when the rail is on, otherwise, put the voltage to 0.
            {
                pm_pwr_volt_level_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, &vol);
                pmRailway[rail].pre_state.volt_uv = vol;
            }
            else
            {
                pmRailway[rail].pre_state.volt_uv = 0;
            }
#ifdef PMIC_FIXED_SETTLING_TIME
            if(PM_HW_MODULE_FTS == pmRailway[rail].resource->peripheral_type) //FTS
            {
                pmRailway[rail].settling_time = PM_RAILWAY_FTS_SETTLING_TIME;
            }
            else //ULT
            {
                pmRailway[rail].settling_time = PM_RAILWAY_ULT_SETTLING_TIME;
            }
#endif
        }
        break;
        default:
        break;
    }
}

unsigned pm_railway_set_mode(pm_railway_type_info_type rail, pm_sw_mode_type  mode)
{
    if(rail<PM_RAILWAY_INVALID)
    {
        return pmRailway[rail].set_mode(rail, mode);
    }
    else
    {
        return 1; // error flag
    }
}

unsigned pm_railway_set_voltage(pm_railway_type_info_type rail, unsigned voltage_uv)
{
    if(rail<PM_RAILWAY_INVALID)
    {
        return pmRailway[rail].set_voltage(rail, voltage_uv);
    }
    else
    {
        return 1; // error flag
    }
}

pm_err_flag_type pm_railway_calculate_vset(pm_railway_type_info_type rail , unsigned voltage_uv ,uint32*  vset)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    if(rail<PM_RAILWAY_INVALID)
    {
        uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
        switch(pmRailway[rail].resource->resource_type)
        {
            case RPM_LDO_A_REQ:
            case RPM_LDO_B_REQ:
            {
                pm_ldo_data_type* ldo_ptr = NULL; 
                pmRailway[rail].rpm_cb_data = pm_rpm_ldo_get_resource_data(pmRailway[rail].resource->resource_type);
                ldo_ptr = ((pm_npa_ldo_data_type*)pmRailway[rail].rpm_cb_data)->ldoDriverData;
                err_flag = pm_pwr_volt_calculate_vset(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, voltage_uv,vset);
            }
            break;
            case RPM_SMPS_A_REQ:
            case RPM_SMPS_B_REQ:
            {
                pm_smps_data_type* smps_ptr = NULL; 
                pmRailway[rail].rpm_cb_data = pm_rpm_smps_get_resource_data(pmRailway[rail].resource->resource_type);
                smps_ptr = ((pm_npa_smps_data_type*)pmRailway[rail].rpm_cb_data)->smpsDriverData;
                err_flag = pm_pwr_volt_calculate_vset(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, voltage_uv,vset);
            }
            break;
        }
    }
    else
    {
        err_flag = PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }

    return err_flag;
}

unsigned pm_railway_smps_set_voltage_i(pm_railway_type_info_type rail, unsigned voltage_uv)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
#ifndef PMIC_FIXED_SETTLING_TIME
    pm_pwr_settling_time_info_type* settling_time_info = (pm_pwr_settling_time_info_type*)(pmRailway[rail].resource->data2);
#endif
    pm_smps_data_type* smps_ptr = ((pm_npa_smps_data_type*)pmRailway[rail].rpm_cb_data)->smpsDriverData;

    uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
    uint8 is_in_auto_mode = 0;
    uint8 pmic_chip = (pmRailway[rail].resource->resource_type == RPM_SMPS_A_REQ ? 0: 1);

    // construct the current_aggregation.
    if(voltage_uv > 0) //turning on the rail
    {
        smps_cur_data.sw_en = 1;
        smps_cur_data.uvol = voltage_uv;
    }
    else //turning off the rail
    {
        smps_cur_data.sw_en = 0;
        smps_cur_data.uvol = 0;
    }

    // construct the shadow
    smps_shadow_data.sw_en = pmRailway[rail].pre_state.sw_en;
    smps_shadow_data.uvol = pmRailway[rail].pre_state.volt_uv;

    rpmInfo.current_aggregation = &smps_cur_data;

    //construct the cb_data
    rpmInfo.cb_data = pmRailway[rail].rpm_cb_data;

    rpmInfo.id = pmRailway[rail].resource->resource_index;

#ifndef PMIC_FIXED_SETTLING_TIME
    //calculate the settling time
    if(smps_cur_data.sw_en > smps_shadow_data.sw_en)// the rail just turning on
    {
        pmRailway[rail].settling_time = settling_time_info->vreg_ok_us;
    }
    else if(smps_cur_data.uvol != smps_shadow_data.uvol )//the voltage is changing
    {
        if(smps_cur_data.uvol > smps_shadow_data.uvol )//the voltage is increasing
        {
            pmRailway[rail].settling_time = ((smps_cur_data.uvol - smps_shadow_data.uvol)/settling_time_info->stepper_uv)*settling_time_info->stepper_us;
        }
        else //the voltage is decreasing
        {
            pmRailway[rail].settling_time = ((smps_shadow_data.uvol - smps_cur_data.uvol)/settling_time_info->stepper_uv)*settling_time_info->stepper_us;
        }
    }
#endif

    //Vstepper sequencing change needed for Auto configured FTS2 -- part1
    if( (smps_cur_data.uvol != smps_shadow_data.uvol ) &&                   // voltage change
        (PM_HW_MODULE_FTS == pmRailway[rail].resource->peripheral_type) &&  // FTS
        (PM_SW_MODE_AUTO == pmRailway[rail].pre_state.sw_mode) )            // auto mode
    {
        //set the flag
        is_in_auto_mode = 1;

        // configure for NPM mode
        err_flag |= pm_smps_sw_mode(pmic_chip, resource_index, PM_SW_MODE_NPM);
    }

    // pass over to the driver.
    pm_rpm_smps_dependency_execute(&rpmInfo, &smps_shadow_data );

    //settling
#ifdef PMIC_FIXED_SETTLING_TIME
    DALSYS_BusyWait(pmRailway[rail].settling_time);
#else
    if(pmRailway[rail].settling_time > 0) // needs to settling
    {
        if(smps_cur_data.sw_en > smps_shadow_data.sw_en) // rail turning on
        {
            // poll VREG_OK
            pm_rpm_check_vreg_settle_status(pmRailway[rail].settling_time, &smps_ptr->pm_pwr_data, smps_ptr->comm_ptr, resource_index);
        }
        else // rail voltage changing
        {
            // poll STEPPER_DONE
            pm_rpm_check_stepper_settle_status(pmRailway[rail].settling_time, &smps_ptr->pm_pwr_data, smps_ptr->comm_ptr, resource_index);
        }
    }
#endif

    //Vstepper sequencing change needed for Auto configured FTS2 -- part2
    if( 1 == is_in_auto_mode)
    {
        //After stepping time is complete, change back to Auto.
        err_flag |= pm_smps_sw_mode(pmic_chip, resource_index, PM_SW_MODE_AUTO);
    }

    // update the previous state
    pmRailway[rail].pre_state.sw_en = smps_cur_data.sw_en;
    pmRailway[rail].pre_state.volt_uv = smps_cur_data.uvol;

    return 0;
}

unsigned pm_railway_smps_set_mode_i(pm_railway_type_info_type rail, pm_sw_mode_type  mode)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_sw_mode_type  sw_mode = mode;
    uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
    uint8 pmic_chip = (pmRailway[rail].resource->resource_type == RPM_SMPS_A_REQ ? 0: 1);

    if(sw_mode == PM_SW_MODE_AUTO) // Enable automode
    {
        err_flag |= pm_smps_sw_mode(pmic_chip, resource_index, PM_SW_MODE_AUTO);
    }
    else if (sw_mode == PM_SW_MODE_NPM)//disable automode
    {
        err_flag |= pm_smps_sw_mode(pmic_chip, resource_index, PM_SW_MODE_NPM);
    }
    else // un-supported modes
    {
        return PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
    }
    //update the mode
    pmRailway[rail].pre_state.sw_mode = sw_mode;

    return (unsigned)err_flag;
}

unsigned pm_railway_ldo_set_voltage_i(pm_railway_type_info_type rail, unsigned voltage_uv)
{
#ifndef PMIC_FIXED_SETTLING_TIME
    pm_pwr_settling_time_info_type* settling_time_info = (pm_pwr_settling_time_info_type*)(pmRailway[rail].resource->data2);
    pm_ldo_data_type* ldo_ptr = ((pm_npa_ldo_data_type*)pmRailway[rail].rpm_cb_data)->ldoDriverData;
    uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
#endif
    pm_rpm_ldo_rail_info_type *ldo_a_rail_info = (pm_rpm_ldo_rail_info_type *)(((pm_npa_ldo_data_type*)pmRailway[rail].rpm_cb_data)->railInfo);
    
    // construct the current_aggregation.
    if(voltage_uv > 0) //turning on the rail
    {
        ldo_cur_data.sw_en = 1;
        ldo_cur_data.bypass_uv = voltage_uv;
        ldo_cur_data.regulated_uvol = voltage_uv + (ldo_a_rail_info[2].SafetyHeadRoom)*1000 ;
    }
    else //turning off the rail
    {
        ldo_cur_data.sw_en = 0;
        ldo_cur_data.bypass_uv = 0;
        ldo_cur_data.regulated_uvol = 0;
    }
    
    ldo_cur_data.resource_id = pmRailway[rail].resource->resource_index;
    ldo_cur_data.device_index = pmRailway[rail].resource->resource_type == RPM_LDO_A_REQ ? 0:1;
    //disable LDO3 BYPASS 
    ldo_cur_data.byp_allowed = PM_NPA_BYPASS_DISALLOWED;
    // construct the shadow
    ldo_shadow_data.sw_en = pmRailway[rail].pre_state.sw_en;
    ldo_shadow_data.bypass_uv = pmRailway[rail].pre_state.volt_uv;
    ldo_shadow_data.regulated_uvol = pmRailway[rail].pre_state.regulated_uvol;
    ldo_shadow_data.byp_allowed = pmRailway[rail].pre_state.byp_allowed;

    rpmInfo.current_aggregation = &ldo_cur_data;
    rpmInfo.new_state = NULL;
    //construct the cb_data
    rpmInfo.cb_data = pmRailway[rail].rpm_cb_data;

    rpmInfo.id = pmRailway[rail].resource->resource_index;

#ifndef PMIC_FIXED_SETTLING_TIME
    //calculate the settling time
    if(ldo_cur_data.sw_en > ldo_shadow_data.sw_en)  // the rail just turning on
    {
        pmRailway[rail].settling_time = settling_time_info->vreg_ok_us;
    }
    else if(ldo_cur_data.bypass_uv != ldo_shadow_data.bypass_uv )  //the voltage is changing
    {
        if(ldo_cur_data.bypass_uv > ldo_shadow_data.bypass_uv )  //the voltage is increasing
        {
            pmRailway[rail].settling_time = ((ldo_cur_data.bypass_uv - ldo_shadow_data.bypass_uv)/settling_time_info->stepper_uv)*settling_time_info->stepper_us;
        }
        else //the voltage is decreasing
        {
            pmRailway[rail].settling_time = ((ldo_shadow_data.bypass_uv - ldo_cur_data.bypass_uv)/settling_time_info->stepper_uv)*settling_time_info->stepper_us;
        }
    }
#endif

    // need to update shadow and pass over to the driver.
    pm_rpm_ldo_dependency_execute(&rpmInfo, &ldo_shadow_data);

    //settling
#ifdef PMIC_FIXED_SETTLING_TIME
    DALSYS_BusyWait(pmRailway[rail].settling_time);
#else
    if(pmRailway[rail].settling_time > 0) // needs to settling
    {
        if(ldo_cur_data.sw_en > ldo_shadow_data.sw_en) // rail turning on
        {
            // poll VREG_OK
            pm_rpm_check_vreg_settle_status(pmRailway[rail].settling_time, &ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, resource_index);
        }
        else // rail voltage changing
        {
            // poll STEPPER_DONE
            //note: all core rails of LDO type should have STEPPER enabled in the SBL.
            pm_rpm_check_stepper_settle_status(pmRailway[rail].settling_time, &ldo_ptr->pm_pwr_data, ldo_ptr->comm_ptr, resource_index);
        }
    }

    //reset the settlling time
    pmRailway[rail].settling_time = 0;
#endif

    // update the previous state
    pmRailway[rail].pre_state.sw_en = ldo_cur_data.sw_en;
    pmRailway[rail].pre_state.volt_uv = ldo_cur_data.bypass_uv;
    pmRailway[rail].pre_state.regulated_uvol = ldo_cur_data.regulated_uvol;
    pmRailway[rail].pre_state.byp_allowed = ldo_cur_data.byp_allowed;

    return 0;
}

unsigned pm_railway_ldo_set_mode_i(pm_railway_type_info_type rail, pm_sw_mode_type  mode)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_sw_mode_type  sw_mode = mode;
    uint8 resource_index = pmRailway[rail].resource->resource_index - 1;
    uint8 pmic_chip = (pmRailway[rail].resource->resource_type == RPM_LDO_A_REQ ? 0: 1);
    
    if(sw_mode == PM_SW_MODE_NPM) // Enable NPM
    {
        if(pmRailway[rail].pre_state.sw_mode != PM_SW_MODE_NPM)
        {
            err_flag |= pm_ldo_sw_mode(pmic_chip, resource_index, PM_SW_MODE_NPM);
        }
    }
    else // un-supported modes
    {
        return PM_ERR_FLAG__PAR2_OUT_OF_RANGE;
    }
        
    //update the mode
    pmRailway[rail].pre_state.sw_mode = sw_mode;
    
    return (unsigned)err_flag;
}

pm_err_flag_type pm_railway_get_voltage(pm_railway_type_info_type rail , pm_volt_level_type* voltage_uv)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    uint8 resource_index;
    
    if(voltage_uv == NULL)
    {
        return PM_ERR_FLAG__INVALID_POINTER;
    }
	
    if(rail > PM_RAILWAY_INVALID)
    {
        return PM_ERR_FLAG__PAR1_OUT_OF_RANGE;
    }
    
    /* Get rail resource index. */
    resource_index = pmRailway[rail].resource->resource_index - 1;
    
    /* Get voltage */
    switch(pmRailway[rail].resource->resource_type)
    {
        case RPM_LDO_A_REQ:
        case RPM_LDO_B_REQ:
        {
            pm_ldo_data_type* ldo_ptr = ((pm_npa_ldo_data_type*)pmRailway[rail].rpm_cb_data)->ldoDriverData;
            err_flag = pm_pwr_volt_level_status_alg(&(ldo_ptr->pm_pwr_data), ldo_ptr->comm_ptr, resource_index, voltage_uv);
        }
        break;
        case RPM_SMPS_A_REQ:
        case RPM_SMPS_B_REQ:
        {
            pm_smps_data_type* smps_ptr = ((pm_npa_smps_data_type*)pmRailway[rail].rpm_cb_data)->smpsDriverData;
            err_flag = pm_pwr_volt_level_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, resource_index, voltage_uv);
        }
        break;
    }    
    return err_flag;
}

