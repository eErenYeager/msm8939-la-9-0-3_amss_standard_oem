/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

PMIC Voltage Switcher Translation and Apply (Aggregate) Functionality

GENERAL DESCRIPTION
This file contains translation and aggregate functions for NPA Device layer

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2012           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/npa/src/rpm/pm_rpm_vs_trans_apply.c#1 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/11/13   hs      Adding settling time for regulators.
05/17/13   hs      PMIC SWEVENT revise.
04/25/13   aks     Code Refactoring: Removing PMIC_SubRsc from pwr algs 
04/12/13   hs      Code refactoring.
02/27/13   hs      Code refactoring.
02/01/12   hs      Added support for input validation. 
07/20/12   hs      Added settling time.
04/12/12   hs      Fixed the launching of StartTransaction.
                   Fixed the internal resource indexing.
01/26/12   wra     Created.
===========================================================================*/
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "npa.h"
#include "rpmserver.h"
#include "pm_npa.h"

#include "pm_target_information.h"
#include "pm_rpm_vs_trans_apply.h"
#include "pm_npa_device_vs.h"
#include "CoreVerify.h"

static pm_npa_vs_data_type RPM_VS_A_REQ_data;
static pm_npa_vs_data_type RPM_VS_B_REQ_data;

void pm_rpm_vs_translation(rpm_translation_info *info);
void pm_rpm_vs_apply(rpm_application_info *info);
void pm_rpm_vs_pre_dependency_analysis(rpm_application_info *info, pm_npa_vs_int_rep *previous_aggregation);
void pm_rpm_vs_post_dependency_analysis(rpm_application_info *info, pm_npa_vs_int_rep *previous_aggregation);
int pm_rpm_vs_aggregate(rpm_application_info *info);
void pm_rpm_vs_execute_driver(rpm_application_info *info, pm_npa_vs_int_rep *previous_aggregation);
pm_npa_vs_data_type* pm_rpm_vs_get_resource_data(rpm_resource_type resource);
void pm_rpm_vs_settle(rpm_application_info *info);

void pm_rpm_vs_translation(rpm_translation_info *info)
{
    unsigned type, length, *value;

    ((pm_npa_vs_int_rep *)(info->dest_buffer))->reserve1 = 0;

    while(kvp_get(info->new_kvps, &type, &length, (const char **)&value))
    {
        // Need size to match.
        if(sizeof(npa_resource_state) != length)
            continue;

        switch(type)
        {
        case PM_NPA_KEY_SOFTWARE_ENABLE:
            {
                ((pm_npa_vs_int_rep *)(info->dest_buffer))->sw_en = *value;

                break;
            }
        case PM_NPA_KEY_PIN_CTRL_ENABLE:
            {
                ((pm_npa_vs_int_rep *)(info->dest_buffer))->pc_en = *value;
                break;
            }
        case PM_NPA_KEY_CURRENT:
            {
                ((pm_npa_vs_int_rep *)(info->dest_buffer))->ip = *value;
                break;
            }
        default:
            {
                //should never go here
            }
        }
    }
}

void pm_rpm_vs_apply(rpm_application_info *info)
{
    int                  change_detected = 0;
    pm_npa_vs_int_rep    previous_aggregation = *((pm_npa_vs_int_rep*)(info->current_aggregation));
    uint64               settling_time = 0;
    pm_npa_vs_data_type *vs_data = (pm_npa_vs_data_type*)info->cb_data;
    uint8                internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;

    PM_OFFTARGET_SWEVENT(PMIC_VS_START_APPLY, info->id, (((pm_npa_vs_data_type*)info->cb_data)->resourceType == RPM_VS_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_VS_START_APPLY, info->id, (((pm_npa_vs_data_type*)info->cb_data)->resourceType == RPM_VS_A_REQ ? 0: 1));

    if(0 == info->settling_time) // this is a new request
    {
        // Aggregate the results, but don't call driver
        change_detected = pm_rpm_vs_aggregate(info);
    
        if( change_detected != 0 )
        {
            // Check the dependencies
            pm_rpm_vs_pre_dependency_analysis(info, &previous_aggregation);
    
            // Execute the driver calls once all dependencies have been executed  
            pm_rpm_vs_execute_driver(info, &previous_aggregation);
    
            // Check the dependencies
            pm_rpm_vs_post_dependency_analysis(info, &previous_aggregation);
        }
        else
        {
            PM_OFFTARGET_SWEVENT(PMIC_VS_NO_CHANGE, info->id, (((pm_npa_vs_data_type*)info->cb_data)->resourceType == RPM_VS_A_REQ ? 0: 1), 0, NULL);
            PM_DEBUG_SWEVENT(PMIC_VS_NO_CHANGE, info->id, (((pm_npa_vs_data_type*)info->cb_data)->resourceType == RPM_VS_A_REQ ? 0: 1));
        }
    }
    else // this is an old request to check if the rail has settled
    {
        //take the current system time
        settling_time = time_service_now(); //in time tick

        if(settling_time < info->settling_time) // still need more time to settle
        {
            //get the remaining settling time
            settling_time = info->settling_time - settling_time;

            //convert from time tick to micro-second
            settling_time = pm_convert_timetick_to_time(settling_time); // in micro second

            if(settling_time < PM_RPM_SETTTLING_TIME_THRESHOLD) // if the remaining settling time is small enough, wait internally
            {
                // update the interal setting time
                vs_data->settlingTime[internal_resource_index] = settling_time;

                //settling
                pm_rpm_vs_settle(info);

                // reset the settling time
                info->settling_time = 0;
            }
        }
        else // settling time passed, the rail should already settled
        {
            //reset the settling time
            info->settling_time = 0;
        } //end if(settling_time < info->settling_time)
    } //end if(0 == info->settling_time) 
}

int pm_rpm_vs_aggregate(rpm_application_info *info)
{
    int change_detected = 0;
    // get the resource type from the info->cb_data
    pm_npa_vs_data_type* vs_data = (pm_npa_vs_data_type*)info->cb_data;

    unsigned numberOfClients = rpm_get_num_clients(info->resource_handle);
    unsigned clientCount = 0; 
    unsigned clientType;
    pm_npa_vs_int_rep* clientRequest;
    unsigned *settingValue;
    void* state = NULL;

    // For each client aggregate the correct values.
    pm_npa_vs_int_rep aggregatedIntRep = {0,0,0,0};

    if(vs_data->railInfo[info->id -1].AccessAllowed)
    {
        for(clientCount = 0; clientCount < numberOfClients; clientCount++)
        {
            rpm_get_current_request(info->resource_handle, clientCount, &clientType, (const void **)&state);        
            
            // EE Client and Internal VS clients
            if ( (clientType == 0) || (clientType == PM_RPM_NPA_CLIENT_VS_A_REQ) || (clientType == PM_RPM_NPA_CLIENT_VS_B_REQ) )
            {
                if(info->client == clientCount)
                {
                    clientRequest = (pm_npa_vs_int_rep*)info->new_state;   
                }
                else
                {
                    clientRequest = (pm_npa_vs_int_rep*)state;     
                }            
                
                if(clientRequest != NULL)
                {
                    PM_OFFTARGET_SWEVENT(PMIC_VS_EE_REQUEST, PM_NPA_VREG_VS, info->id, 
                                          (vs_data->resourceType == RPM_VS_A_REQ ? 0: 1), clientRequest);

                    // Aggregate Software Enable
                    aggregatedIntRep.sw_en          = MAX(aggregatedIntRep.sw_en,       clientRequest->sw_en);

                    // Aggregate ORed values of Pin Control Enable
                    aggregatedIntRep.pc_en          = aggregatedIntRep.pc_en | clientRequest->pc_en;

                    // Aggregate Summation of Current
                    aggregatedIntRep.ip             = aggregatedIntRep.ip + clientRequest->ip;
                }
            }
            else
            {
                if(info->client == clientCount)
                {
                    settingValue = (unsigned *)info->new_state;
                }
                else
                {
                  settingValue = state;
                }

                if(clientType == PM_RPM_NPA_INT_CLIENT_SW_ENABLE_DEPENDENT) // Internal client that registers voltage vote with another resource
                { aggregatedIntRep.sw_en          = MAX(aggregatedIntRep.sw_en,       *settingValue); }
                else if(clientType == PM_RPM_NPA_INT_CLIENT_PIN_CONTROL_ENABLE_DEPENDENT) // Internal client that registers pc_en vote with another resource
                { aggregatedIntRep.pc_en     = aggregatedIntRep.pc_en | *settingValue; }
                else if(clientType == PM_RPM_NPA_INT_CLIENT_CURRENT_DEPENDENT) // Internal client that registers peak current vote with another resource
                { aggregatedIntRep.ip             = aggregatedIntRep.ip + *settingValue; }   
            }
        }

        // input checking
        if(vs_data->railInfo[info->id -1].AlwaysOn)
        {
            aggregatedIntRep.sw_en = 1;
        }

        change_detected = pm_rpm_int_copy( &aggregatedIntRep, (pm_npa_vs_int_rep *)info->current_aggregation, sizeof(pm_npa_vs_int_rep));
    }// end access_allowed

    return change_detected;
}

void pm_rpm_vs_pre_dependency_analysis(rpm_application_info *info, pm_npa_vs_int_rep *previous_aggregation)
{
    pm_pwr_resource_dependency_info      *depInfo = NULL;

    pm_npa_vs_int_rep  *cur_data = (pm_npa_vs_int_rep*)info->current_aggregation;
    pm_npa_vs_data_type     *cb_data = (pm_npa_vs_data_type*)info->cb_data;

    PM_OFFTARGET_SWEVENT(PMIC_VS_START_PRE_DEP, info->id, (cb_data->resourceType == RPM_VS_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_VS_START_PRE_DEP, info->id, cb_data->resourceType == RPM_VS_A_REQ ? 0: 1);

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_VS_A_REQ)
    {
        depInfo = &RPM_VS_A_REQ_data.depInfo[info->id];
    }
    else if(cb_data->resourceType == RPM_VS_B_REQ)
    {
        depInfo = &RPM_VS_B_REQ_data.depInfo[info->id];
    }

    CORE_VERIFY_PTR(depInfo);
    if(depInfo->parent_source_dependency != NULL)
    {
        // Detect if this is a pre-operation change on the parent
        // These conditions are
        // If the VS goes from a disabled to an enabled state
        // If the VS Pin Control goes from disabled to an enabled state
        if( (cur_data->sw_en > previous_aggregation->sw_en) ||
            (cur_data->pc_en > previous_aggregation->pc_en) )
        {
            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_vs_int_rep), info->current_aggregation);
        }   
    }

    //Decode and execute preconditions
    //if(depInfo->pre_operational_dependencies != NULL)
    //{
    //    pm_rpm_execute_operational_conditions(depInfo->pre_operational_dependencies, PM_PWR_TIMING_TYPE__PRE);
    //} 

    PM_OFFTARGET_SWEVENT(PMIC_VS_END_PRE_DEP, info->id, (cb_data->resourceType == RPM_VS_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_VS_END_PRE_DEP, info->id, cb_data->resourceType == RPM_VS_A_REQ ? 0: 1);
}

void pm_rpm_vs_post_dependency_analysis(rpm_application_info *info, pm_npa_vs_int_rep *previous_aggregation)
{
    pm_pwr_resource_dependency_info      *depInfo = NULL;
    pm_npa_vs_int_rep  *cur_data = (pm_npa_vs_int_rep*)info->current_aggregation;
    pm_npa_vs_data_type     *cb_data = (pm_npa_vs_data_type*)info->cb_data;
    unsigned            change_detected = 0;

    PM_OFFTARGET_SWEVENT(PMIC_VS_START_POST_DEP, info->id, (cb_data->resourceType == RPM_VS_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_VS_START_POST_DEP, info->id, cb_data->resourceType == RPM_VS_A_REQ ? 0: 1);

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_VS_A_REQ)
    {
        depInfo = &RPM_VS_A_REQ_data.depInfo[info->id];
    }
    else if(cb_data->resourceType == RPM_VS_B_REQ)
    {
        depInfo = &RPM_VS_B_REQ_data.depInfo[info->id];
    }

    CORE_VERIFY_PTR(depInfo);
    if(depInfo->parent_source_dependency != NULL)
    {
        // Detect if this is a post-operation change on the parent
        // These conditions are
        // If the VS goes from a enabled to an disabled state
        // If the VS Pin Control goes from enabled to an disabled state
        if( (cur_data->sw_en < previous_aggregation->sw_en) ||
            (cur_data->pc_en < previous_aggregation->pc_en))
        {
            change_detected = 1;
        }

        if( 1 == change_detected)
        {
            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_vs_int_rep), info->current_aggregation);
        }  
    }

    //Decode and execute postconditions
    //if(depInfo->post_operational_dependencies != NULL)
    //{
    //    pm_rpm_execute_operational_conditions(depInfo->post_operational_dependencies, PM_PWR_TIMING_TYPE__POST);
    //}  

    PM_OFFTARGET_SWEVENT(PMIC_VS_END_POST_DEP, info->id, (cb_data->resourceType == RPM_VS_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_VS_END_POST_DEP, info->id, cb_data->resourceType == RPM_VS_A_REQ ? 0: 1);
}

void pm_rpm_vs_execute_driver(rpm_application_info *info, pm_npa_vs_int_rep *previous_aggregation)
{
    pm_npa_vs_int_rep *vs_int_rep =(pm_npa_vs_int_rep *)info->current_aggregation;
    pm_npa_vs_data_type *vs_data = (pm_npa_vs_data_type *)info->cb_data;
    pm_vs_data_type* vs_ptr = NULL;
    uint8  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;
    uint64  settling_time_vregOK = 0;
    uint64  settling_time = 0;

    PM_OFFTARGET_SWEVENT(PMIC_VS_AGG1, PM_NPA_VREG_VS, info->id, 
                            (((pm_npa_vs_data_type*)info->cb_data)->resourceType == RPM_VS_A_REQ ? 0: 1), info->current_aggregation);
    PM_DEBUG_SWEVENT(PMIC_VS_AGG1, info->id, (((pm_npa_vs_data_type*)info->cb_data)->resourceType == RPM_VS_A_REQ ? 0: 1), 
                     ((pm_npa_vs_int_rep*)info->current_aggregation)->sw_en,
                     ((pm_npa_vs_int_rep*)info->current_aggregation)->pc_en, ((pm_npa_vs_int_rep*)info->current_aggregation)->ip);

    //Get the vs_ptr from the call back data.
    if(vs_data->resourceType == RPM_VS_A_REQ)
    {
        vs_ptr = vs_data->vsDriverData;
    }
    else
    {
       // We should never get here
       PM_SWEVENT(PMIC_RPM_ERROR, PM_RPM_ERR__NULL_CALLBACK); 
       return;
    }   

    if(vs_ptr != NULL)
    {
        /* Set PIN Control Enable */
        if(vs_int_rep->pc_en != previous_aggregation->pc_en)
        {
           pm_pwr_pin_ctrl_alg(&vs_ptr->pm_pwr_data, vs_ptr->comm_ptr, internal_resource_index, vs_int_rep->pc_en);    
        }

        /* State set */
        if(vs_int_rep->sw_en != previous_aggregation->sw_en)
        {
            pm_pwr_sw_enable_alg(&vs_ptr->pm_pwr_data, vs_ptr->comm_ptr, internal_resource_index, (pm_on_off_type)vs_int_rep->sw_en);

            // -------------- Settling time ------------------
            
            //rail turns on, adding settling time.
            if(vs_int_rep->sw_en > previous_aggregation->sw_en)
            {
                //calculate the settling time.
                settling_time_vregOK = vs_data->settlingTimeInfo->vreg_ok_us;

                //take the current system time
                settling_time = time_service_now() + pm_convert_time_to_timetick(settling_time_vregOK);
    
                // feedback to RPM server by modifying info
                info->settling_time = settling_time;
            }

            // -------------- Settling time ------------------
        }
    }
}

void pm_rpm_vs_register_resources(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    pm_npa_vs_data_type *vs_data = NULL;
    uint32 dep_prop_id_arr[] = {PM_PROP_VSA_DEP, PM_PROP_VSB_DEP};
    uint32 rail_prop_id_arr[] = {PM_PROP_VSA_RAIL, PM_PROP_VSB_RAIL};
    uint8 pmic_index = (resourceType == RPM_VS_A_REQ ? 0: 1);

    CORE_VERIFY(pmic_index < (sizeof(dep_prop_id_arr)/sizeof(dep_prop_id_arr[0])));
    CORE_VERIFY(pmic_index < (sizeof(rail_prop_id_arr)/sizeof(rail_prop_id_arr[0])));

    if(num_npa_resources > 0)
    {
        vs_data = pm_rpm_vs_get_resource_data(resourceType);
        if(vs_data != NULL)
        {
            vs_data->vsDriverData = pm_vs_get_data(pmic_index);

            CORE_VERIFY_PTR(vs_data->vsDriverData);
                    
            vs_data->resourceType = resourceType;
            vs_data->depInfo = (pm_pwr_resource_dependency_info*)pm_target_information_get_specific_info(dep_prop_id_arr[pmic_index]);
            vs_data->railInfo = (pm_rpm_vs_rail_info_type*)pm_target_information_get_specific_info(rail_prop_id_arr[pmic_index]);
            vs_data->settlingTimeInfo = (pm_pwr_settling_time_info_type*)pm_target_information_get_common_info(PM_PROP_VS_SETTLING_TIME);
            pm_malloc(sizeof(uint32)*num_npa_resources, (void**)&(vs_data->settlingTime) );
            DALSYS_memset(vs_data->settlingTime, 0, sizeof(uint32)*num_npa_resources);

            rpm_register_resource(resourceType, num_npa_resources + 1, sizeof(pm_npa_vs_int_rep), pm_rpm_vs_translation, pm_rpm_vs_apply, (void *)vs_data);        
        }
    }  
}

void pm_rpm_vs_register_resource_dependencies(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    uint8 count = 0;
    pm_pwr_resource *dependency = NULL;
    pm_npa_vs_int_rep *current_agg = NULL;
    pm_pwr_resource_dependency_info    *depInfo = NULL;

    if(num_npa_resources > 0)
    {
        for(count = 1; count < num_npa_resources + 1; count++)
        {
            rpm_get_aggregated_request_buffer(resourceType, count, (const void**)&current_agg);
            if(current_agg != NULL)
            {
                DALSYS_memset(current_agg, 0, sizeof(pm_npa_vs_int_rep));
            }
        }

        if(resourceType == RPM_VS_A_REQ)
        {
            depInfo = RPM_VS_A_REQ_data.depInfo;
        }
        else if(resourceType == RPM_VS_B_REQ)
        {
            depInfo = RPM_VS_B_REQ_data.depInfo;
        }

        if(depInfo)
        {
            for(count = 1; count < num_npa_resources + 1; count++)
            {
                if(depInfo[count].parent_source_dependency != NULL)
                {
                    dependency = depInfo[count].parent_source_dependency;

                    // create the internal client
                    rpm_register_internal_client_type(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_VS_DEPENDENT, sizeof(pm_npa_vs_int_rep));
                    
                    depInfo[count].parent_source_dependency_client_handle = 
                                 rpm_create_client(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_VS_DEPENDENT);                   
                }

                //pm_rpm_register_operational_dependency_int_client(&depInfo[count]);
            }
        }
    }  
}

pm_npa_vs_data_type* pm_rpm_vs_get_resource_data(rpm_resource_type resource)
{
    if(RPM_VS_A_REQ == resource)
    {
        return &RPM_VS_A_REQ_data;
    }
    else if(RPM_VS_B_REQ == resource)
    {
        return &RPM_VS_B_REQ_data;
    }
    else
    {
        return NULL;
    }
}

void pm_rpm_vs_settle(rpm_application_info *info)
{
    pm_npa_vs_data_type *vs_data = (pm_npa_vs_data_type *)info->cb_data;
    uint8  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;
    uint32 settling_time = vs_data->settlingTime[internal_resource_index];
    pm_vs_data_type* vs_ptr = NULL;

    // for voltage change, do wait here
    if( settling_time != 0)
    {
        PM_OFFTARGET_SWEVENT(PMIC_VS_START_SETTLING, info->id, (vs_data->resourceType == RPM_VS_A_REQ ? 0: 1), settling_time, NULL);
        PM_DEBUG_SWEVENT(PMIC_VS_START_SETTLING, info->id, (vs_data->resourceType == RPM_VS_A_REQ ? 0: 1), settling_time);
 
        //Get the PmicResource from the call back data.
        if( (vs_data->resourceType == RPM_VS_A_REQ) || (vs_data->resourceType == RPM_VS_B_REQ) )
        {
            vs_ptr = vs_data->vsDriverData;
		}
        CORE_VERIFY_PTR(vs_ptr);  

        // poll VREG_OK
        pm_rpm_check_vreg_settle_status(settling_time, &vs_ptr->pm_pwr_data, vs_ptr->comm_ptr, internal_resource_index);

        // reset the settling time
        vs_data->settlingTime[internal_resource_index] = 0;

        PM_OFFTARGET_SWEVENT(PMIC_VS_END_SETTLING, info->id, (vs_data->resourceType == RPM_VS_A_REQ ? 0: 1), settling_time, NULL);
        PM_DEBUG_SWEVENT(PMIC_VS_END_SETTLING, info->id, (vs_data->resourceType == RPM_VS_A_REQ ? 0: 1));
    }
}

