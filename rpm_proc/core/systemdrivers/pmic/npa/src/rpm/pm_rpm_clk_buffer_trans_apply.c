/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

PMIC Clock Buffer Translation and Apply (Aggregate)Functionality

GENERAL DESCRIPTION
This file contains translation and aggregate functions for NPA Device layer

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2012           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/npa/src/rpm/pm_rpm_clk_buffer_trans_apply.c#1 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
01/13/14   rk      Handle always on flag in ClkBuffers (CR-574742)
05/29/13   aks     Clk driver input check (do not disable critical clocks) 
05/17/13   hs      PMIC SWEVENT revise.
04/12/13   hs      Code refactoring.
02/27/13   hs      Code refactoring.
04/12/12   hs      Fixed the launching of StartTransaction.
                   Fixed the internal resource indexing.
04/09/12   hs      Added missing implementation.
01/26/12   wra     Created.
===========================================================================*/
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "npa.h"
#include "npa_resource.h"
#include "rpmserver.h"

#include "pm_target_information.h"
#include "pm_rpm_clk_buffer_trans_apply.h"
#include "pm_npa_device_clk_buff.h"
#include "CoreVerify.h"

static pm_npa_clk_buffer_data_type RPM_CLK_BUFFER_A_REQ_data;
static pm_npa_clk_buffer_data_type RPM_CLK_BUFFER_B_REQ_data;

void pm_rpm_clk_buffer_translation(rpm_translation_info *pm_rpm_clk_buffer_translation_info);
void pm_rpm_clk_buffer_apply(rpm_application_info *pm_rpm_clk_buffer_application_info);
void pm_rpm_clk_buffer_pre_dependency_analysis(rpm_application_info *info, pm_npa_clk_buffer_int_rep *previous_aggregation);
void pm_rpm_clk_buffer_post_dependency_analysis(rpm_application_info *info, pm_npa_clk_buffer_int_rep *previous_aggregation);
int pm_rpm_clk_buffer_aggregate(rpm_application_info *info);
void pm_rpm_clk_buffer_execute_driver(rpm_application_info *info, pm_npa_clk_buffer_int_rep *previous_aggregation);
pm_npa_clk_buffer_data_type* pm_rpm_clk_buffer_get_resource_data(rpm_resource_type resource);

void pm_rpm_clk_buffer_translation(rpm_translation_info *info)
{ 
    unsigned type, length, *value;

    ((pm_npa_clk_buffer_int_rep *)(info->dest_buffer))->reserved = 0;

    while(kvp_get(info->new_kvps, &type, &length, (const char **)&value))
    {
        // Need size to match.
        if(sizeof(npa_resource_state) != length)
            continue;

        switch(type)
        {
        case PM_NPA_KEY_SOFTWARE_ENABLE:
            {
                ((pm_npa_clk_buffer_int_rep *)(info->dest_buffer))->sw_enable = (uint8)*value;

                break;
            }
        case PM_NPA_KEY_PIN_CTRL_CLK_BUFFER_ENABLE_KEY:
            {
                ((pm_npa_clk_buffer_int_rep *)(info->dest_buffer))->pc_enable = (uint8)*value;
                break;
            }
        default:
            {
                //should never go here
            }
        }
    }
}

void pm_rpm_clk_buffer_apply(rpm_application_info *info)
{
    int change_detected = 0;
    pm_npa_clk_buffer_int_rep previous_aggregation = *((pm_npa_clk_buffer_int_rep*)(info->current_aggregation));

    PM_OFFTARGET_SWEVENT(PMIC_CLK_BUF_START_APPLY, info->id, 
                     (((pm_npa_clk_buffer_data_type*)info->cb_data)->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_CLK_BUF_START_APPLY, info->id, 
                     (((pm_npa_clk_buffer_data_type*)info->cb_data)->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1));

    // Aggregate the results, but don't call driver
    change_detected = pm_rpm_clk_buffer_aggregate(info);

    if( change_detected != 0 )
    {
        // Check the dependencies
        pm_rpm_clk_buffer_pre_dependency_analysis(info, &previous_aggregation);
        
        // Execute the driver calls once all dependencies have been executed  
        pm_rpm_clk_buffer_execute_driver(info, &previous_aggregation);

        pm_rpm_clk_buffer_post_dependency_analysis(info, &previous_aggregation);
    }
    else
    {
        PM_OFFTARGET_SWEVENT(PMIC_CLK_BUF_NO_CHANGE, info->id, 
                         (((pm_npa_clk_buffer_data_type*)info->cb_data)->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1), 0, NULL);
        PM_DEBUG_SWEVENT(PMIC_CLK_BUF_NO_CHANGE, info->id, 
                         (((pm_npa_clk_buffer_data_type*)info->cb_data)->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1));
    }
}

int pm_rpm_clk_buffer_aggregate(rpm_application_info *info)
{
    int change_detected = 0;
    // get the resource type from the info->cb_data
    pm_npa_clk_buffer_data_type* resourceInfo = (pm_npa_clk_buffer_data_type*)info->cb_data;

    unsigned numberOfClients = rpm_get_num_clients(info->resource_handle);
    unsigned clientCount = 0;

    // For each client aggregate the correct values.
    pm_npa_clk_buffer_int_rep aggregatedIntRep = {0,0};
	  //if access not allowed , just return
	  if(resourceInfo->clkInfo[info->id - 1].AccessAllowed == 0)
		  return change_detected;

    for(clientCount = 0; clientCount < numberOfClients; clientCount++)
    {
        unsigned clientType;
        unsigned *settingValue;
        pm_npa_clk_buffer_int_rep* clientRequest;     
        void* state = NULL;
        rpm_get_current_request(info->resource_handle, clientCount, &clientType, (const void **)&state);   

        // EE Client and Internal CLK clients
        if((clientType == 0) || (clientType == PM_RPM_NPA_CLIENT_CLK_BUFFER_A_REQ ) || (clientType == PM_RPM_NPA_CLIENT_CLK_BUFFER_B_REQ)) 
        {
            //clientRequest = (pm_npa_clk_buffer_int_rep*)state;
            if(info->client == clientCount)
            {
                clientRequest = (pm_npa_clk_buffer_int_rep*)info->new_state;
            }
            else
            {
                clientRequest = (pm_npa_clk_buffer_int_rep*)state;
            }

            if(clientRequest != NULL)
            {
                PM_OFFTARGET_SWEVENT(PMIC_CLK_EE_REQUEST, PM_NPA_CLK_BUFF, info->id, (resourceInfo->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1), clientRequest);

                // Aggregate Software Enable
                aggregatedIntRep.sw_enable          = MAX(aggregatedIntRep.sw_enable,       clientRequest->sw_enable);

                // Aggregate ORed values of Pin Control Enable
                aggregatedIntRep.pc_enable     = aggregatedIntRep.pc_enable | clientRequest->pc_enable;
            }
        }
        else
        {
            if(info->client == clientCount)
            {
                settingValue = (unsigned *)info->new_state;
            }
            else
            {
              settingValue = state;
            }

            if(clientType == PM_RPM_NPA_INT_CLIENT_SW_ENABLE_DEPENDENT) // Internal client that registers voltage vote with another resource
            { aggregatedIntRep.sw_enable          = MAX(aggregatedIntRep.sw_enable,       *settingValue); }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_CLK_PIN_CONTROL_ENABLE_DEPENDENT) // Internal client that registers clock buffer pc_en vote with another resource
            { aggregatedIntRep.pc_enable     = aggregatedIntRep.pc_enable | *settingValue; }    
        }
    }

    // input checking after aggregation.
    if(0 == aggregatedIntRep.sw_enable)
    {
        if(1 == resourceInfo->clkInfo[info->id - 1].AlwaysOn)
        {
            // if the clock is voted to be off, but it is designed to be always on, keep it on
            aggregatedIntRep.sw_enable = 1;
        }
    }

    change_detected = pm_rpm_int_copy( &aggregatedIntRep, (pm_npa_clk_buffer_int_rep *)info->current_aggregation, sizeof(pm_npa_clk_buffer_int_rep));

    return change_detected;
}

void pm_rpm_clk_buffer_pre_dependency_analysis(rpm_application_info *info, pm_npa_clk_buffer_int_rep *previous_aggregation)
{
    pm_pwr_resource_dependency_info      *depInfo = NULL;
    pm_npa_clk_buffer_int_rep  *cur_data = (pm_npa_clk_buffer_int_rep*)info->current_aggregation;
    pm_npa_clk_buffer_data_type     *cb_data = (pm_npa_clk_buffer_data_type*)info->cb_data;

    PM_OFFTARGET_SWEVENT(PMIC_CLK_BUF_START_PRE_DEP, info->id, (cb_data->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_CLK_BUF_START_PRE_DEP, info->id, cb_data->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1);

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_CLK_BUFFER_A_REQ)
    {
        depInfo = &RPM_CLK_BUFFER_A_REQ_data.depInfo[info->id];
    }
    else if(cb_data->resourceType == RPM_CLK_BUFFER_B_REQ)
    {
        depInfo = &RPM_CLK_BUFFER_B_REQ_data.depInfo[info->id];
    }

    CORE_VERIFY_PTR(depInfo);
	if(depInfo->parent_source_dependency != NULL)
    {
        // Detect if this is a pre-operation change on the parent
        // These conditions are
        // If the CLK_BUFF goes from a disabled to an enabled state
        // If the CLK_BUFF Pin Control goes from disabled to an enabled state
        if((cur_data->sw_enable > previous_aggregation->sw_enable) || 
            (cur_data->pc_enable > previous_aggregation->pc_enable))
        {
            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_clk_buffer_int_rep), info->current_aggregation);
        }   
    }

    //Decode and execute preconditions
    //if(depInfo->pre_operational_dependencies != NULL)
    //{
    //    pm_rpm_execute_operational_conditions(depInfo->pre_operational_dependencies, PM_PWR_TIMING_TYPE__PRE);
    //} 

    PM_OFFTARGET_SWEVENT(PMIC_CLK_BUF_END_PRE_DEP, info->id, (cb_data->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_CLK_BUF_END_PRE_DEP, info->id, cb_data->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1);
}

void pm_rpm_clk_buffer_post_dependency_analysis(rpm_application_info *info, pm_npa_clk_buffer_int_rep *previous_aggregation)
{
    pm_pwr_resource_dependency_info      *depInfo = NULL;
    pm_npa_clk_buffer_int_rep  *cur_data = (pm_npa_clk_buffer_int_rep*)info->current_aggregation;
    pm_npa_clk_buffer_data_type     *cb_data = (pm_npa_clk_buffer_data_type*)info->cb_data;
    unsigned                    change_detected = 0;

    PM_OFFTARGET_SWEVENT(PMIC_CLK_BUF_START_POST_DEP, info->id, (cb_data->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_CLK_BUF_START_POST_DEP, info->id, cb_data->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1);

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_CLK_BUFFER_A_REQ)
    {
        depInfo = &RPM_CLK_BUFFER_A_REQ_data.depInfo[info->id];
    }
    else if(cb_data->resourceType == RPM_CLK_BUFFER_B_REQ)
    {
        depInfo = &RPM_CLK_BUFFER_B_REQ_data.depInfo[info->id];
    }
 
    CORE_VERIFY_PTR(depInfo);	
    if(depInfo->parent_source_dependency != NULL)
    {
        // Detect if this is a post-operation change on the parent
        // These conditions are
        // If the CLK_BUFF goes from a enabled to an disabled state
        // If the CLK_BUFF Pin Control goes from enabled to an disabled state
        if((cur_data->sw_enable < previous_aggregation->sw_enable) || 
            (cur_data->pc_enable < previous_aggregation->pc_enable))
        {
            change_detected = 1;
        }

        if(1 == change_detected)
        {
            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_clk_buffer_int_rep), info->current_aggregation);
        }  
    }

    //Decode and execute postconditions
    //if(depInfo->post_operational_dependencies != NULL)
    //{
    //    pm_rpm_execute_operational_conditions(depInfo->post_operational_dependencies, PM_PWR_TIMING_TYPE__POST);
    //}  

    PM_OFFTARGET_SWEVENT(PMIC_CLK_BUF_END_POST_DEP, info->id, (cb_data->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_CLK_BUF_END_POST_DEP, info->id, cb_data->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1);
}

void pm_rpm_clk_buffer_execute_driver(rpm_application_info *info, pm_npa_clk_buffer_int_rep *previous_aggregation)
{
    pm_npa_clk_buffer_int_rep *clk_buffer_int_rep =(pm_npa_clk_buffer_int_rep *)info->current_aggregation;
    pm_npa_clk_buffer_data_type *clk_buffer_data = (pm_npa_clk_buffer_data_type *)info->cb_data;
        pm_clk_data_type* clk_ptr = NULL;
    uint8  internal_resource_index = (uint8)(info->id );
    uint8 pmic_index = (clk_buffer_data->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1);

    PM_OFFTARGET_SWEVENT(PMIC_CLK_BUF_AGG1, PM_NPA_CLK_BUFF, info->id, 
                         (((pm_npa_clk_buffer_data_type*)info->cb_data)->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1), info->current_aggregation);
    PM_DEBUG_SWEVENT(PMIC_CLK_BUF_AGG1, info->id, (((pm_npa_clk_buffer_data_type*)info->cb_data)->resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1),
                 ((pm_npa_clk_buffer_int_rep*)(info->current_aggregation))->sw_enable,
                 ((pm_npa_clk_buffer_int_rep*)(info->current_aggregation))->pc_enable);
        

    //Get the PmicResource from the call back data.
    if(clk_buffer_data->resourceType == RPM_CLK_BUFFER_A_REQ)
    {
        clk_ptr = clk_buffer_data->clkBuffDriverData;
    }
    else
    {
        // We should never get here
       PM_SWEVENT(PMIC_RPM_ERROR, 1, 1); //CLK ID is 1, 1 means NULL Call back data
        return;
    }
    
    if(clk_ptr != NULL)
    {
        /* State set */
        if(clk_buffer_int_rep->sw_enable != previous_aggregation->sw_enable)
        {
            pm_clk_sw_enable(pmic_index, clk_ptr->clk_common[internal_resource_index], (pm_on_off_type)(clk_buffer_int_rep->sw_enable));
        }

        /* Set PIN Control */
        if(clk_buffer_int_rep->pc_enable != previous_aggregation->pc_enable)
        {
            pm_clk_pin_ctrled(pmic_index, clk_ptr->clk_common[internal_resource_index], (pm_on_off_type)(clk_buffer_int_rep->pc_enable));
        }
    }
}

void pm_rpm_clk_buffer_register_resources(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    pm_npa_clk_buffer_data_type *clk_buffer_data = NULL;
    uint32 dep_prop_id_arr[] = {PM_PROP_CLKA_DEP, PM_PROP_CLKB_DEP};
    uint32 rail_prop_id_arr[] = {PM_PROP_CLKA_INFO, PM_PROP_CLKB_INFO};
    uint8 pmic_index = (resourceType == RPM_CLK_BUFFER_A_REQ ? 0: 1);

    CORE_VERIFY(pmic_index < (sizeof(dep_prop_id_arr)/sizeof(dep_prop_id_arr[0])));
    CORE_VERIFY(pmic_index < (sizeof(rail_prop_id_arr)/sizeof(rail_prop_id_arr[0])));

    if(num_npa_resources > 0)
    {
        clk_buffer_data = pm_rpm_clk_buffer_get_resource_data(resourceType);
        if(clk_buffer_data != NULL)
        {   
            clk_buffer_data->clkBuffDriverData = pm_clk_get_data(pmic_index);

            CORE_VERIFY_PTR(clk_buffer_data->clkBuffDriverData);

            clk_buffer_data->resourceType = resourceType;
            clk_buffer_data->depInfo = (pm_pwr_resource_dependency_info*)pm_target_information_get_specific_info(dep_prop_id_arr[pmic_index]);
            clk_buffer_data->clkInfo = (pm_rpm_clk_info_type*)pm_target_information_get_specific_info(rail_prop_id_arr[pmic_index]);
			rpm_register_resource(resourceType, num_npa_resources + 1, 
								  sizeof(pm_npa_clk_buffer_int_rep), pm_rpm_clk_buffer_translation, 
								  pm_rpm_clk_buffer_apply, (void *)clk_buffer_data);
        }
    }
}

void pm_rpm_clk_buffer_register_resource_dependencies(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    uint8 count = 0;
    pm_pwr_resource *dependency = NULL;
    pm_pwr_resource_dependency_info *depInfo = NULL;
    pm_npa_clk_buffer_int_rep *current_agg = NULL;

    if(num_npa_resources > 0)
    {
        for(count = 1; count < num_npa_resources+1; count++)
        {
            rpm_get_aggregated_request_buffer(resourceType, count, (const void**)&current_agg);
            if(current_agg != NULL)
            {
                DALSYS_memset(current_agg, 0, sizeof(pm_npa_clk_buffer_int_rep));
            }
        }

        if(resourceType == RPM_CLK_BUFFER_A_REQ)
        {
            depInfo = RPM_CLK_BUFFER_A_REQ_data.depInfo;
        }
        else if(resourceType == RPM_CLK_BUFFER_B_REQ)
        {
            depInfo = RPM_CLK_BUFFER_B_REQ_data.depInfo;
        }

        if(depInfo)
        {
            for(count = 1; count < num_npa_resources + 1; count++)
            {
                if(depInfo[count].parent_source_dependency != NULL)
                {
                    dependency = depInfo[count].parent_source_dependency;

                    // create the internal client
                    rpm_register_internal_client_type(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_CLK_DEPENDENT, sizeof(pm_npa_clk_buffer_int_rep));
                    
                    depInfo[count].parent_source_dependency_client_handle = 
                                 rpm_create_client(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_CLK_DEPENDENT);                   
                }

                //pm_rpm_register_operational_dependency_int_client(&depInfo[count]);
            }
        }
    } 
}

pm_npa_clk_buffer_data_type* pm_rpm_clk_buffer_get_resource_data(rpm_resource_type resource)
{
    if(RPM_CLK_BUFFER_A_REQ == resource)
    {
        return &RPM_CLK_BUFFER_A_REQ_data;
    }
    else if(RPM_CLK_BUFFER_B_REQ == resource)
    {
        return &RPM_CLK_BUFFER_B_REQ_data;
    }
    else
    {
        return NULL;
    }
}

