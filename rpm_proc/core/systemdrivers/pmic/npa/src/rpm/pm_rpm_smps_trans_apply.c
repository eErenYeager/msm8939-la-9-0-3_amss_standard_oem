/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

PMIC Fast Transient Switched (FTS) Voltage Regulator Translation and 
Apply (Aggregate)Functionality

GENERAL DESCRIPTION
This file contains translation and aggregate functions for NPA Device layer

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2012           by QUALCOMM Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/npa/src/rpm/pm_rpm_smps_trans_apply.c#1 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
05/09/13   hs      Parent does not inherit “pin control mode” from child
02/27/13   hs      Code refactoring.
02/01/12   hs      Added support for input validation. 
01/29/13   aks     Adding support for Boost as separate peripheral 
08/07/12   hs      Added support for 5V boost.
07/20/12   hs      Added settling time.
04/12/12   hs      Fixed the launching of StartTransaction.
                   Fixed the internal resource indexing.
01/26/12   wra     Created.
===========================================================================*/
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "npa.h"
#include "rpmserver.h"
#include "pm_npa.h"
#include "comm_manager.h"

#include "pm_rpm_smps_trans_apply.h"
#include "pm_npa_device_smps.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "pm_rpm_vs_trans_apply.h"
#include "pm_target_information.h"
#include "CoreVerify.h"

static pm_npa_smps_data_type RPM_SMPS_A_REQ_data;
static pm_npa_smps_data_type RPM_SMPS_B_REQ_data;

// internal function prototypes
void pm_rpm_smps_translation(rpm_translation_info *info);
void pm_rpm_smps_apply(rpm_application_info *info);
void pm_rpm_smps_pre_dependency_analysis(rpm_application_info *info, pm_npa_smps_int_rep* previous_aggregation);
void pm_rpm_smps_post_dependency_analysis(rpm_application_info *info, pm_npa_smps_int_rep* previous_aggregation);
int pm_rpm_smps_aggregate(rpm_application_info *info);
void pm_rpm_smps_execute_driver(rpm_application_info *info, pm_npa_smps_int_rep* previous_aggregation);
void pm_rpm_smps_check_ldo_bypass(rpm_application_info* info, boolean out_bypass);
void pm_rpm_smps_settle(rpm_application_info *info, pm_npa_smps_int_rep* previous_aggregation);

void pm_rpm_smps_translation(rpm_translation_info *info)
{
    unsigned type, length, *value;
    pm_npa_smps_int_rep *cache;
    pm_npa_smps_int_rep SAWCache;
    pm_npa_smps_int_rep previous_aggregation;
    rpm_application_info executeInfo;
    pm_npa_smps_data_type* resourceInfo = (pm_npa_smps_data_type*)info->cb_data;
    unsigned isSawCtrl = resourceInfo->railInfo[info->id-1].IsSawControlled;

    if(isSawCtrl)
    {
        DALSYS_memset(&SAWCache, 0, sizeof(pm_npa_smps_int_rep));
        cache = &SAWCache;
    }
    else
    {
        cache = (pm_npa_smps_int_rep *)(info->dest_buffer);
    }

    cache->global_byp_en = 1;
    cache->reserve1 = 0;

    while(kvp_get(info->new_kvps, &type, &length, (const char **)&value))
    {
        // Need size to match.
        if(sizeof(npa_resource_state) != length)
            continue;

        switch(type)
        {
        case PM_NPA_KEY_SOFTWARE_ENABLE:
            {
                cache->sw_en = *value;

                break;
            }
        case PM_NPA_KEY_SMPS_SOFTWARE_MODE:
            {
                cache->smps_sw_mode = *value;
                break;
            }
        case PM_NPA_KEY_PIN_CTRL_ENABLE:
            {
                cache->pc_en = *value;
                break;
            }
        case PM_NPA_KEY_PIN_CTRL_POWER_MODE:
            {
                cache->pc_mode = *value;
                break;
            }
        case PM_NPA_KEY_MICRO_VOLT:
            {
                cache->uvol = *value;
                break;
            }
        case PM_NPA_KEY_CURRENT:
            {
                cache->ip = *value;
                break;
            }
        case PM_NPA_KEY_FREQUENCY:
            {
                cache->freq = *value;
                break;
            }
        case PM_NPA_KEY_FREQUENCY_REASON:
            {
                cache->freq_reason = *value;
                break;
            }
        case PM_NPA_KEY_FOLLOW_QUIET_MODE:
            {
                cache->quiet_mode = *value;
                break;
            }
        case PM_NPA_KEY_HEAD_ROOM:
            {
                cache->hr = *value;
                break;
            }
        case PM_NPA_KEY_BYPASS_ALLOWED_KEY:
            {
                cache->byp_allowed = *value;
                break;
            }
       default:
            {
                //should never go here
            }
        }
    }

    if(isSawCtrl)
    {
        executeInfo.cb_data = info->cb_data;
        executeInfo.client  = info->client;
        executeInfo.client_type = info->client_type;
        executeInfo.current_aggregation = cache;
        executeInfo.id = info->id;

        previous_aggregation = *cache;

        previous_aggregation.sw_en = !cache->sw_en; // to make sure this field will be excuted
        previous_aggregation.uvol = cache->uvol - 1; // to make sure this field will be excuted

        pm_rpm_smps_dependency_execute(&executeInfo, &previous_aggregation);
    }
}

void pm_rpm_smps_apply(rpm_application_info *info)
{
    int                    change_detected = 0;
    pm_npa_smps_int_rep    previous_aggregation = *((pm_npa_smps_int_rep*)(info->current_aggregation));
    uint64                 settling_time = 0;
    pm_npa_smps_data_type *smps_data = (pm_npa_smps_data_type *)info->cb_data;
    uint8  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;
    unsigned isSawCtrl = smps_data->railInfo[internal_resource_index].IsSawControlled;

    if(!isSawCtrl)
    {
        PM_OFFTARGET_SWEVENT(PMIC_SMPS_START_APPLY, info->id, (smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1), 0, NULL);
        PM_DEBUG_SWEVENT(PMIC_SMPS_START_APPLY, info->id, smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1);

        if(0 == info->settling_time) // this is a new request
    {
        // Aggregate the results, but don't call driver
        change_detected = pm_rpm_smps_aggregate(info);

        if(change_detected != 0 )
        {
                pm_rpm_smps_dependency_execute(info, &previous_aggregation);
                pm_rpm_smps_settle(info, &previous_aggregation);
        }
            else
            {
                PM_OFFTARGET_SWEVENT(PMIC_SMPS_NO_CHANGE, info->id, (smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1), 0, NULL);
                PM_DEBUG_SWEVENT(PMIC_SMPS_NO_CHANGE, info->id, smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1);
            }
        }
        else // this is an old request to check if the rail has settled
        {
            //take the current system time
            settling_time = time_service_now(); // in time tick

            if(settling_time < info->settling_time) // still need more time to settle
        {
                //get the remaining settling time
                settling_time = info->settling_time - settling_time; // in time tick
    
                //convert from time tick to micro-second
                settling_time = pm_convert_timetick_to_time(settling_time); // in micro second
    
               if(settling_time < PM_RPM_SETTTLING_TIME_THRESHOLD) // if the remaining settling time is small enough, wait internally
                {
                    // update the interal setting time
                    smps_data->settlingTime[internal_resource_index] = settling_time;
    
                    //settling
                    pm_rpm_smps_settle(info, &previous_aggregation);
    
                    // reset the settling time
                    info->settling_time = 0;
        }
    }
            else // settling time passed, the rail should already settled
            {
                //reset the settling time
                info->settling_time = 0;
        }
        } //end if(0 == info->settling_time)
    }//end if(!isSawCtrl)
}

void pm_rpm_smps_dependency_execute(rpm_application_info *info, pm_npa_smps_int_rep* previous_aggregation)
{
    // Check the pre-operational dependencies
    pm_rpm_smps_pre_dependency_analysis(info, previous_aggregation);

    // Execute the driver calls once all dependencies have been executed    
    pm_rpm_smps_execute_driver(info, previous_aggregation);

    // Check the post operational dependencies
    pm_rpm_smps_post_dependency_analysis(info, previous_aggregation);
}

int pm_rpm_smps_aggregate(rpm_application_info *info)
{
    int change_detected = 0;
    unsigned max_ldo_regulated_uvol = 0; 
    unsigned max_ldo_bypass_uvol = 0;
    //unsigned byp_regulate_uvol = 0;
    pm_npa_smps_data_type* smps_data = (pm_npa_smps_data_type*)info->cb_data;
    unsigned clientType;
    unsigned *settingValue;
    pm_npa_smps_int_rep* smpsClientRequest;
    pm_npa_ldo_int_rep* ldoClientRequest;
    pm_npa_vs_int_rep* vsClientRequest;
    void* state = NULL;
    unsigned clientCount = 0;

    unsigned numberOfClients = rpm_get_num_clients(info->resource_handle);

    // For each client aggregate the correct values.
    pm_npa_smps_int_rep aggregatedIntRep = {0,0,0,0,0,0,0,0,0,0,0};
    aggregatedIntRep.global_byp_en = 1;

    for(clientCount = 0; clientCount < numberOfClients; clientCount++)
    {
        rpm_get_current_request(info->resource_handle, clientCount, &clientType, (const void **)&state);        

         // EE Client and Internal SMPS clients
        if( (clientType == 0) || (clientType == PM_RPM_NPA_CLIENT_SMPS_A_REQ) || (clientType == PM_RPM_NPA_CLIENT_SMPS_B_REQ) )
        {
            if(smps_data->railInfo[info->id -1].AccessAllowed)
            {
               if(info->client == clientCount)
                {		
                    smpsClientRequest = (pm_npa_smps_int_rep*)info->new_state;
                }
                else
                {
                    smpsClientRequest = (pm_npa_smps_int_rep*)state;
                }
                
                if(smpsClientRequest != NULL)
                {
                    PM_OFFTARGET_SWEVENT(PMIC_SMPS_EE_REQUEST, PM_NPA_VREG_SMPS, info->id, 
                                         smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1, smpsClientRequest); 

                    // Aggregate Software Enable
                    aggregatedIntRep.sw_en = MAX(aggregatedIntRep.sw_en, smpsClientRequest->sw_en);
                    
                    // Aggregate Software Mode Enable
                    aggregatedIntRep.smps_sw_mode = MAX(aggregatedIntRep.smps_sw_mode, 
                                                        smpsClientRequest->smps_sw_mode);

                    // Aggregate ORed values of Pin Control Enable
                    aggregatedIntRep.pc_en = aggregatedIntRep.pc_en | smpsClientRequest->pc_en;

                    // Aggregate ORed values of Pin Control Power Mode
                    aggregatedIntRep.pc_mode = aggregatedIntRep.pc_mode | smpsClientRequest->pc_mode;

                    // Aggregate Maximum Voltage
                    aggregatedIntRep.uvol = MAX(aggregatedIntRep.uvol, smpsClientRequest->uvol);

                    // Aggregate Summation of Current
                    aggregatedIntRep.ip = aggregatedIntRep.ip + smpsClientRequest->ip;

                    // The following line will only be true if there is a frequency reason 
                    // other than "PM_NPA_FREQ_REASON_NONE"
                    if(smpsClientRequest->freq_reason > aggregatedIntRep.freq_reason)
                    {
                        // If the frequency reason takes priority then set the frequency
                        // to what ever the client needs.
                        aggregatedIntRep.freq_reason = smpsClientRequest->freq_reason;
                        aggregatedIntRep.freq = smpsClientRequest->freq;            
                    }
                    else
                    {
                        // If the frequency reason is NONE, then a simple max frequency aggregation works
                        if(aggregatedIntRep.freq_reason == PM_NPA_FREQ_REASON_NONE)
                        {
                            // Aggregate Maximum Frequency, the enum sequence is from the lowest to highest since the HW is mapped so
                            if(smpsClientRequest->freq != 0 )
                            {
                                if( aggregatedIntRep.freq == 0 )// if the freq is 0, this means it is the first aggregation.
                                {
                                    aggregatedIntRep.freq  = smpsClientRequest->freq;
                                }
                                else
                                {
                                    aggregatedIntRep.freq  = MIN(aggregatedIntRep.freq,       smpsClientRequest->freq);
                                }
                            }
                        }
                    }

                    // Aggregate Quiet Mode
                    aggregatedIntRep.quiet_mode = MAX(aggregatedIntRep.quiet_mode, smpsClientRequest->quiet_mode);

                    // Aggregate Headroom Voltage
                    aggregatedIntRep.hr = MAX(aggregatedIntRep.hr, smpsClientRequest->hr);

                    // Aggregate Bypass Allowed
                    aggregatedIntRep.byp_allowed = MAX(aggregatedIntRep.byp_allowed, smpsClientRequest->byp_allowed);
                }
            }
        }           
        else if(clientType == PM_RPM_NPA_INT_CLIENT_LDO_DEPENDENT)
        {   
            if(info->client == clientCount)
            {		
                ldoClientRequest = (pm_npa_ldo_int_rep*)info->new_state;
            } 
            else
            {            
                ldoClientRequest = (pm_npa_ldo_int_rep*)state;
            }            

            // A valid internal LDO client should have resource_id greater than zero
            if(ldoClientRequest->resource_id > 0) 
            {
                PM_OFFTARGET_SWEVENT(PMIC_SMPS_INTERNAL_REQUEST, PM_NPA_VREG_LDO, ldoClientRequest->resource_id, 
                                     ldoClientRequest->device_index, ldoClientRequest);

                // Aggregate Software Enable - Child needs power
                aggregatedIntRep.sw_en = MAX(aggregatedIntRep.sw_en, ldoClientRequest->sw_en);

                // TODO: Does it make sense to aggregate pass the pin control to the parent?
                // Aggregate ORed values of Pin Control Enable
                aggregatedIntRep.pc_en = aggregatedIntRep.pc_en | ldoClientRequest->pc_en;

                // ldoClientRequest->regulated_uvol has headroom accounted for.
                max_ldo_regulated_uvol = MAX(max_ldo_regulated_uvol, ldoClientRequest->regulated_uvol);
                
                if((ldoClientRequest->noise_hr > 0) || (ldoClientRequest->byp_allowed == PM_NPA_BYPASS_DISALLOWED))
                {
                    max_ldo_bypass_uvol = MAX(max_ldo_bypass_uvol, ldoClientRequest->regulated_uvol);
                }
                else
                {
                    max_ldo_bypass_uvol = MAX(max_ldo_bypass_uvol, ldoClientRequest->bypass_uv);
                }

                // Check for LDO going from disable to enable
                /*SW_Enable - Do not enable LDO when other LDOs on the same SMPS are in Bypass mode. The power on current draw will cause the bypassed LDO voltage to droop.
                    ○ General rule for enabling another sub regulated LDO on the same SMPS is 50mV head room but in this case we will use the safety headroom of the LDO.
                    ○ If PC control enable is active for an LDO with sub regulated LDOs in bypass on the same SMPS then the other LDOs should be taken out of bypass and given 50mV head room*/
                // This transition information is sent from each LDO in the "is_en_transition"
                if(ldoClientRequest->is_en_transition == 1)
                {
                    aggregatedIntRep.global_byp_en = FALSE;
                }

                // Aggregate Summation of Current
                aggregatedIntRep.ip             = aggregatedIntRep.ip + ldoClientRequest->ip;                 
            } //resource_id>0
        }
        else if(clientType == PM_RPM_NPA_INT_CLIENT_VS_DEPENDENT)
        {
            if(info->client == clientCount)
            {		
                vsClientRequest = (pm_npa_vs_int_rep*)info->new_state;
            }
            else
            {
                vsClientRequest = (pm_npa_vs_int_rep*)state;
            }

            PM_OFFTARGET_SWEVENT(PMIC_SMPS_INTERNAL_REQUEST, PM_NPA_VREG_VS, info->id, smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1, vsClientRequest); 

            // Aggregate Software Enable - Child needs power
            aggregatedIntRep.sw_en = MAX(aggregatedIntRep.sw_en, vsClientRequest->sw_en);

            // TODO: ask Chris about how to associate VS setting with its parent, ie. SMPS.
            aggregatedIntRep.pc_en = aggregatedIntRep.pc_en | vsClientRequest->pc_en;

            // Aggregate Summation of Current
            aggregatedIntRep.ip    = aggregatedIntRep.ip + vsClientRequest->ip; 
        }
        else  // This is for Operational dependency
        {
            if(info->client == clientCount)
            {
                settingValue = (unsigned *)info->new_state;
            }
            else
            {
              settingValue = state;
            }
            
            if(clientType == PM_RPM_NPA_INT_CLIENT_VOLTAGE_DEPENDENT) // Internal client that registers voltage vote with another resource
            { aggregatedIntRep.uvol                  = MAX(aggregatedIntRep.uvol,       *settingValue); }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_SW_ENABLE_DEPENDENT) // Internal client that registers voltage vote with another resource
            { aggregatedIntRep.sw_en          = MAX(aggregatedIntRep.sw_en,       *settingValue); }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_SW_MODE_DEPENDENT) // Internal client that registers SW Mode vote with another resource
            { aggregatedIntRep.smps_sw_mode        = MAX(aggregatedIntRep.smps_sw_mode,       *settingValue); }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_PIN_CONTROL_ENABLE_DEPENDENT) // Internal client that registers pc_en vote with another resource
            { aggregatedIntRep.pc_en     = aggregatedIntRep.pc_en | *settingValue; }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_PIN_CONTROL_MODE_DEPENDENT) // Internal client that registers pc_mode vote with another resource
            { aggregatedIntRep.pc_mode = aggregatedIntRep.pc_mode | *settingValue; }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_CURRENT_DEPENDENT) // Internal client that registers peak current vote with another resource
            { aggregatedIntRep.ip             = aggregatedIntRep.ip + *settingValue; }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_FREQ_DEPENDENT) // Internal client that registers voltage vote with another resource
            { aggregatedIntRep.freq           = MAX(aggregatedIntRep.freq,       *settingValue); }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_FREQ_REASON_DEPENDENT) // Internal client that registers voltage vote with another resource
            { aggregatedIntRep.freq_reason    =  aggregatedIntRep.freq_reason | *settingValue; }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_QUIET_MODE_DEPENDENT) // Internal client that registers voltage vote with another resource
            { aggregatedIntRep.quiet_mode   = MAX(aggregatedIntRep.quiet_mode, *settingValue); }
            else if(clientType == PM_RPM_NPA_INT_CLIENT_HEADROOM_DEPENDENT) // Internal client that registers headroom voltage vote with another resource
            { aggregatedIntRep.hr = MAX(aggregatedIntRep.hr, *settingValue); }    
        }
    }
    
    // Check if there is a possible bypass condition    
    // global_byp_en is disabled due LDO to disable->enable state transition
    // Aggregate to highest voltage non bypass voltage
    if(aggregatedIntRep.global_byp_en == 0)
    {
        aggregatedIntRep.uvol = MAX(aggregatedIntRep.uvol, max_ldo_regulated_uvol);
    }
    else
    {
        aggregatedIntRep.uvol = MAX(aggregatedIntRep.uvol, max_ldo_bypass_uvol);
    }

    // input checking after aggregation.
    if(smps_data->railInfo[info->id - 1].AlwaysOn)
    {
        aggregatedIntRep.sw_en = 1;
    }
    if(aggregatedIntRep.smps_sw_mode < smps_data->railInfo[info->id - 1].MinPwrMode)
    {
        aggregatedIntRep.smps_sw_mode = smps_data->railInfo[info->id - 1].MinPwrMode;
    }

    if( (aggregatedIntRep.uvol < smps_data->railInfo[info->id - 1].MinVoltage*1000) && (aggregatedIntRep.uvol > 0) )
    {
        aggregatedIntRep.uvol = smps_data->railInfo[info->id - 1].MinVoltage*1000;
    }
    else if(aggregatedIntRep.uvol > smps_data->railInfo[info->id - 1].MaxVoltage*1000)
    {
        aggregatedIntRep.uvol = smps_data->railInfo[info->id - 1].MaxVoltage*1000;
    }

    //NOTE: the enum values for frequency are revered, i.e. PM_CLK_9p6_MHz is smaller than PM_CLK_6p4_MHz
    if(aggregatedIntRep.freq > smps_data->railInfo[info->id - 1].MinFreq)
    {
        aggregatedIntRep.freq = smps_data->railInfo[info->id - 1].MinFreq;
    }
    else if(aggregatedIntRep.freq < smps_data->railInfo[info->id - 1].MaxFreq)
    {
        aggregatedIntRep.freq = smps_data->railInfo[info->id - 1].MaxFreq;
    }

    change_detected = pm_rpm_int_copy( &aggregatedIntRep, (pm_npa_smps_int_rep *)info->current_aggregation, sizeof(pm_npa_smps_int_rep));

    return change_detected;
}

void pm_rpm_smps_pre_dependency_analysis(rpm_application_info *info, pm_npa_smps_int_rep* previous_aggregation)
{
    pm_pwr_resource_dependency_info      *depInfo = NULL;
    pm_npa_smps_int_rep  *cur_data = (pm_npa_smps_int_rep*)info->current_aggregation;
    pm_npa_smps_data_type     *cb_data = (pm_npa_smps_data_type*)info->cb_data;

    PM_OFFTARGET_SWEVENT(PMIC_SMPS_START_PRE_DEP, info->id, (cb_data->resourceType == RPM_SMPS_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_SMPS_START_PRE_DEP, info->id, cb_data->resourceType == RPM_SMPS_A_REQ ? 0: 1);

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_SMPS_A_REQ)
    {
        depInfo = &RPM_SMPS_A_REQ_data.depInfo[info->id];
    }
    else if(cb_data->resourceType == RPM_SMPS_B_REQ)
    {
        depInfo = &RPM_SMPS_B_REQ_data.depInfo[info->id];
    }

    CORE_VERIFY_PTR(depInfo);
    if(depInfo->child_resource_dependents != NULL)
    {
        // take all the children LDOs out of bypass if needed.
        if( (cur_data->global_byp_en == 0) ||         // a child LDO just about to turn on
            (cur_data->sw_en != previous_aggregation->sw_en)|| //the parent is about to be from on->off or off->on
            (cur_data->uvol != previous_aggregation->uvol) )    // the parent's voltage is about to increase.
        {
            pm_rpm_smps_check_ldo_bypass(info, TRUE);
        }
    }

    if(depInfo->parent_source_dependency != NULL)
    {
        // Detect if this is a pre-operation change on the parent
        // These conditions are
        // If the required headroom is increased
        // If the voltage setting is increased
        // If the SMPS goes from a disabled to an enabled state
        // If the SMPS Pin Control goes from disabled to an enabled state
        if( (cur_data->hr > previous_aggregation->hr) || 
            (cur_data->uvol > previous_aggregation->uvol) || 
            (cur_data->sw_en > previous_aggregation->sw_en) || 
            (cur_data->pc_en > previous_aggregation->pc_en))
        {
            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_smps_int_rep), info->current_aggregation);
        }   
    }

    //Decode and execute preconditions
    //if(depInfo->pre_operational_dependencies != NULL)
    //{
    //    pm_rpm_execute_operational_conditions(depInfo->pre_operational_dependencies, PM_PWR_TIMING_TYPE__PRE);
    //}
    PM_OFFTARGET_SWEVENT(PMIC_SMPS_END_PRE_DEP, info->id, (cb_data->resourceType == RPM_SMPS_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_SMPS_END_PRE_DEP, info->id, cb_data->resourceType == RPM_SMPS_A_REQ ? 0: 1);
}

void pm_rpm_smps_post_dependency_analysis(rpm_application_info *info, pm_npa_smps_int_rep* previous_aggregation)
{
    pm_pwr_resource_dependency_info      *depInfo = NULL;
    pm_npa_smps_int_rep  *cur_data = (pm_npa_smps_int_rep*)info->current_aggregation;
    pm_npa_smps_data_type     *cb_data = (pm_npa_smps_data_type*)info->cb_data;
    unsigned             change_detected = 0;

    PM_OFFTARGET_SWEVENT(PMIC_SMPS_START_POST_DEP, info->id, (cb_data->resourceType == RPM_SMPS_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_SMPS_START_POST_DEP, info->id, cb_data->resourceType == RPM_SMPS_A_REQ ? 0: 1);

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_SMPS_A_REQ)
    {
        depInfo = &RPM_SMPS_A_REQ_data.depInfo[info->id];
    }
    else if(cb_data->resourceType == RPM_SMPS_B_REQ)
    {
        depInfo = &RPM_SMPS_B_REQ_data.depInfo[info->id];
    }

    CORE_VERIFY_PTR(depInfo);
    if(depInfo->child_resource_dependents != NULL)
    {
        if(cur_data->global_byp_en == 1)
        {
            pm_rpm_smps_check_ldo_bypass(info, FALSE);
        }
    }

    if(depInfo->parent_source_dependency != NULL)
    {
        // Detect if this is a post-operation change on the parent
        // These conditions are
        // If the required headroom is decreased
        // If the voltage setting is decreased
        // If the SMPS goes from a enabled to an disabled state
        // If the SMPS Pin Control goes from enabled to an disabled state
        if( (cur_data->hr < previous_aggregation->hr) || 
            (cur_data->uvol < previous_aggregation->uvol) || 
            (cur_data->sw_en < previous_aggregation->sw_en) || 
            (cur_data->pc_en < previous_aggregation->pc_en) )
        {
            change_detected = 1;
        }

        if(1== change_detected )
        {
            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_smps_int_rep), info->current_aggregation);
        }  
    }

    PM_OFFTARGET_SWEVENT(PMIC_SMPS_END_POST_DEP, info->id, (cb_data->resourceType == RPM_SMPS_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_SMPS_END_POST_DEP, info->id, cb_data->resourceType == RPM_SMPS_A_REQ ? 0: 1);
}

void pm_rpm_smps_execute_driver(rpm_application_info *info, pm_npa_smps_int_rep* previous_aggregation)
{
    pm_npa_smps_int_rep *smps_int_rep =(pm_npa_smps_int_rep *)info->current_aggregation;
    pm_npa_smps_data_type *smps_data = (pm_npa_smps_data_type *)info->cb_data;
    pm_smps_data_type* smps_ptr = NULL;
    uint8  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;

    uint64 settling_time_vregOK = 0;
    uint64 settling_time_vregStepper = 0;
    uint64 settling_time = 0;
    uint8 pmic_index = (smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1);

    PM_OFFTARGET_SWEVENT(PMIC_SMPS_AGG,PM_NPA_VREG_SMPS, info->id, 
                         (((pm_npa_smps_data_type*)info->cb_data)->resourceType == RPM_SMPS_A_REQ ? 0: 1), info->current_aggregation);
    PM_DEBUG_SWEVENT(PMIC_SMPS_AGG1, info->id, (((pm_npa_smps_data_type*)info->cb_data)->resourceType == RPM_SMPS_A_REQ ? 0: 1), 
                ((pm_npa_smps_int_rep*)info->current_aggregation)->sw_en,
                ((pm_npa_smps_int_rep*)info->current_aggregation)->smps_sw_mode);
    PM_DEBUG_SWEVENT(PMIC_SMPS_AGG2, ((pm_npa_smps_int_rep*)info->current_aggregation)->pc_en,
                ((pm_npa_smps_int_rep*)info->current_aggregation)->pc_mode,
                ((pm_npa_smps_int_rep*)info->current_aggregation)->global_byp_en,
                ((pm_npa_smps_int_rep*)info->current_aggregation)->uvol);
    PM_DEBUG_SWEVENT(PMIC_SMPS_AGG3, ((pm_npa_smps_int_rep*)info->current_aggregation)->ip,
                ((pm_npa_smps_int_rep*)info->current_aggregation)->freq,
                ((pm_npa_smps_int_rep*)info->current_aggregation)->freq_reason,
                ((pm_npa_smps_int_rep*)info->current_aggregation)->quiet_mode);
    PM_DEBUG_SWEVENT(PMIC_SMPS_AGG4, ((pm_npa_smps_int_rep*)info->current_aggregation)->byp_allowed,
                ((pm_npa_smps_int_rep*)info->current_aggregation)->hr);

    //Get the PmicResource from the call back data.
    if(smps_data->resourceType == RPM_SMPS_A_REQ)
    {
        smps_ptr = smps_data->smpsDriverData;
    }
    else if(smps_data->resourceType == RPM_SMPS_B_REQ)
    {
        smps_ptr = smps_data->smpsDriverData;
    }
    else
    {
        // We should never get here
        PM_SWEVENT(PMIC_RPM_ERROR, PM_RPM_ERR__NULL_CALLBACK); 
        return;
    }

    if(smps_ptr != NULL)
    {
            /* Set Power Mode */
        if( (smps_int_rep->smps_sw_mode == PM_NPA_SW_MODE_SMPS__AUTO) && (smps_int_rep->smps_sw_mode != previous_aggregation->smps_sw_mode) )
            {
            PM_OFFTARGET_SWEVENT(PMIC_SMPS_POWER_MODE, 0, 0, 0, NULL);
            PM_DEBUG_SWEVENT(PMIC_SMPS_POWER_MODE, 0); // Mode numbers defined by pmic_parser.py
            pm_smps_sw_mode(pmic_index, internal_resource_index, PM_SW_MODE_AUTO);
            }
            else if(smps_int_rep->smps_sw_mode == PM_NPA_SW_MODE_SMPS__IPEAK)
            {
            if( (smps_int_rep->smps_sw_mode != previous_aggregation->smps_sw_mode) || (smps_int_rep->ip != previous_aggregation->ip) )
                {
                unsigned lp_hp_current_threshold;
                lp_hp_current_threshold = smps_data->railInfo[internal_resource_index].LpHpCurrentThreshold;

                    if(smps_int_rep->ip >= lp_hp_current_threshold)
                    {
                    PM_OFFTARGET_SWEVENT(PMIC_SMPS_POWER_MODE, 1, 0, 0, NULL);
                    PM_DEBUG_SWEVENT(PMIC_SMPS_POWER_MODE, 1); // Mode numbers defined by pmic_parser.py
                    pm_smps_sw_mode(pmic_index, internal_resource_index, PM_SW_MODE_NPM);
                    }
                    else
                    {
                    PM_OFFTARGET_SWEVENT(PMIC_SMPS_POWER_MODE, 2, 0, 0, NULL);
                    PM_DEBUG_SWEVENT(PMIC_SMPS_POWER_MODE, 2); // Mode numbers defined by pmic_parser.py
                    pm_smps_sw_mode(pmic_index, internal_resource_index, PM_SW_MODE_LPM);
                    }
                }
            }
            else
            {
            if(smps_int_rep->smps_sw_mode != previous_aggregation->smps_sw_mode) 
                {
                PM_OFFTARGET_SWEVENT(PMIC_SMPS_POWER_MODE, 3, 0, 0, NULL);
                PM_DEBUG_SWEVENT(PMIC_SMPS_POWER_MODE, 3); // Mode numbers defined by pmic_parser.py
                pm_smps_sw_mode(pmic_index, internal_resource_index, PM_SW_MODE_NPM);
                }
            }   

        /* Set PIN Control Enable */
        if(smps_int_rep->pc_en != previous_aggregation->pc_en)
        {
            pm_pwr_pin_ctrl_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, internal_resource_index, (uint8)smps_int_rep->pc_en);    
        }

        /* Set PIN Control Power Mode */
        if(smps_int_rep->pc_mode != previous_aggregation->pc_mode)
        {
            pm_pwr_pin_ctrl_mode_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, internal_resource_index, (uint8)smps_int_rep->pc_mode);
        }

            /* Set the switch Frequency */
        if(smps_int_rep->freq != previous_aggregation->freq)
            {
            pm_smps_switching_freq(pmic_index, internal_resource_index, (pm_smps_switching_freq_type)smps_int_rep->freq);
            }

        if(smps_int_rep->quiet_mode != previous_aggregation->quiet_mode)
        {
            pm_smps_quiet_mode(pmic_index, internal_resource_index, (pm_quiet_mode_type)smps_int_rep->quiet_mode, 250);//TODO need to update the structure in order to set the QMODE_PS_VRST.
        }

            /* Set the Voltage */
        if( (smps_int_rep->uvol != previous_aggregation->uvol) && (smps_int_rep->uvol != 0) )
            {
            pm_pwr_volt_level_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, internal_resource_index, (pm_volt_level_type)smps_int_rep->uvol);

            // voltage increases, adding settling time.
            if(smps_int_rep->uvol > previous_aggregation->uvol)
            {
                    //calculate the settling time.
                    settling_time_vregStepper = ((smps_int_rep->uvol - previous_aggregation->uvol)/((smps_data->settlingTimeInfo[internal_resource_index]).stepper_uv))*((smps_data->settlingTimeInfo[internal_resource_index]).stepper_us);
            }
            }

            /* State set */
        if(smps_int_rep->sw_en != previous_aggregation->sw_en)
            {
            pm_pwr_sw_enable_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, internal_resource_index,(pm_on_off_type)smps_int_rep->sw_en);

            //rail turns on, adding settling time.
            if(smps_int_rep->sw_en > previous_aggregation->sw_en)
            {
                //calculate the settling time.
                settling_time_vregOK = smps_data->settlingTimeInfo->vreg_ok_us;
            }
            }

        // -------------- Settling time ------------------

        if(smps_int_rep->sw_en > previous_aggregation->sw_en)// for rail turning on, inform the RPM server
        {
            //take the current system time
            settling_time = time_service_now() + pm_convert_time_to_timetick(settling_time_vregOK);

            // feedback to RPM server by modifying info
            info->settling_time = settling_time;
        }
        else  if(smps_int_rep->uvol > previous_aggregation->uvol)// voltage changes
        {
            if(settling_time_vregStepper > PM_RPM_SETTTLING_TIME_THRESHOLD) // if the settling time for the voltage change is greater than 100us, inform RPM server
            {
                 //take the current system time
                settling_time = time_service_now() + pm_convert_time_to_timetick(settling_time_vregStepper);

                // feedback to RPM server by modifying info
                info->settling_time = settling_time;
           }
            else //otherwise wait internally
            {
                // update the interal setting time
                smps_data->settlingTime[internal_resource_index] = settling_time_vregStepper;
            }
        }
            // -------------- Settling time ------------------
    }
}

void pm_rpm_smps_register_resources(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    pm_npa_smps_data_type *smps_data = NULL;
    uint32 dep_prop_id_arr[] = {PM_PROP_SMPSA_DEP, PM_PROP_SMPSB_DEP};
    uint32 rail_prop_id_arr[] = {PM_PROP_SMPSA_RAIL, PM_PROP_SMPSB_RAIL};
    uint8 pmic_index = (resourceType == RPM_SMPS_A_REQ ? 0: 1);
    /* Backed out CR 589852
    uint32 smps_index = 0;
    pm_smps_data_type *smps_ptr = NULL;
    uint8 range = 0; */

    CORE_VERIFY(pmic_index < (sizeof(dep_prop_id_arr)/sizeof(dep_prop_id_arr[0])));
    CORE_VERIFY(pmic_index < (sizeof(rail_prop_id_arr)/sizeof(rail_prop_id_arr[0])));

    if(num_npa_resources > 0)
    {
        smps_data = pm_rpm_smps_get_resource_data(resourceType);
        if(smps_data != NULL)
        {
            smps_data->smpsDriverData = pm_smps_get_data(pmic_index);

            CORE_VERIFY_PTR(smps_data->smpsDriverData);

            /* Backed out CR 589852
            smps_ptr = smps_data->smpsDriverData;
            CORE_VERIFY_PTR(smps_ptr); */
            smps_data->resourceType = resourceType;
            smps_data->depInfo = (pm_pwr_resource_dependency_info*)pm_target_information_get_specific_info(dep_prop_id_arr[pmic_index]);
            smps_data->railInfo = (pm_rpm_smps_rail_info_type*)pm_target_information_get_specific_info(rail_prop_id_arr[pmic_index]);
            //verify if the range can meet the SMPS's min, max voltage limits
            /* Backed out CR 589852
            for( smps_index = 0; smps_index < num_npa_resources ; smps_index++)
            { 
                 range = smps_ptr->pm_pwr_data.pwr_specific_info[smps_index].pwr_range;
                 CORE_VERIFY(((smps_data->railInfo[smps_index].MinVoltage)*1000 >= smps_ptr->pm_pwr_data.pwr_specific_info[smps_index].pwr_vset[range].RangeMin ) && 
                  ((smps_data->railInfo[smps_index].MaxVoltage)*1000 <= smps_ptr->pm_pwr_data.pwr_specific_info[smps_index].pwr_vset[range].RangeMax));
            } */
            smps_data->settlingTimeInfo = (pm_pwr_settling_time_info_type*)pm_target_information_get_common_info(PM_PROP_SMPS_SETTLING_TIME);
            pm_malloc(sizeof(uint32)*num_npa_resources, (void**)&(smps_data->settlingTime) );
            DALSYS_memset(smps_data->settlingTime, 0, sizeof(uint32)*num_npa_resources);

            rpm_register_resource(resourceType, num_npa_resources + 1, sizeof(pm_npa_smps_int_rep), pm_rpm_smps_translation, pm_rpm_smps_apply, (void *)smps_data);
        }
    } 
}

void pm_rpm_smps_register_resource_dependencies(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    uint8 count = 0;
    pm_pwr_resource *dependency = NULL;
    pm_pwr_resource_dependency_info *depInfo = NULL;
    pm_npa_smps_int_rep *current_agg = NULL;
    pm_smps_data_type* smps_ptr = NULL;
    pm_volt_level_type vol = 0;
    pm_on_off_type en_status = PM_OFF;

    if(num_npa_resources > 0)
    {
        if(RPM_SMPS_A_REQ == resourceType)
        {
            depInfo = RPM_SMPS_A_REQ_data.depInfo;
        }
        else if(RPM_SMPS_B_REQ == resourceType)
        {
            depInfo = RPM_SMPS_B_REQ_data.depInfo;
        }
		
        for(count = 1; count < num_npa_resources + 1; count++)
        {
            rpm_get_aggregated_request_buffer(resourceType, count, (const void**)&current_agg);
            if(current_agg != NULL)
            {
                if(RPM_SMPS_A_REQ == resourceType)
                {
                    smps_ptr = RPM_SMPS_A_REQ_data.smpsDriverData;
                }
                else if(RPM_SMPS_B_REQ == resourceType)
                {
                    smps_ptr = RPM_SMPS_B_REQ_data.smpsDriverData;
                }
                CORE_VERIFY_PTR(smps_ptr);
                

                DALSYS_memset(current_agg, 0, sizeof(pm_npa_smps_int_rep));
                current_agg->global_byp_en = 1;

                pm_pwr_volt_level_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, count-1, &vol);
                current_agg->uvol = vol;
                pm_pwr_sw_enable_status_alg(&(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, count-1, &en_status);
                current_agg->sw_en = en_status;
                current_agg->smps_sw_mode = 0x0F; // initialize it to some invalid setting in order to make sure that the first vote will be granted.
            }
        }

        if(depInfo)
        {
            for(count = 1; count < num_npa_resources + 1; count++)
            {
                if(depInfo[count].parent_source_dependency != NULL)
                {
                    dependency = depInfo[count].parent_source_dependency;

                    // create the internal client
                    rpm_register_internal_client_type(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_SMPS_DEPENDENT, sizeof(pm_npa_smps_int_rep));
                    
                    depInfo[count].parent_source_dependency_client_handle = 
                        rpm_create_client(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_SMPS_DEPENDENT);                   
                }

                //pm_rpm_register_operational_dependency_int_client(&depInfo[count]);
            }
        }
    }  
}

void pm_rpm_smps_check_ldo_bypass(rpm_application_info* info, boolean out_bypass)
{
    // LDO bypass callback variables
    unsigned clientType;
    void* state = NULL;
    pm_npa_ldo_int_rep* ldoClientRequest;
    pm_npa_smps_int_rep* smpsCurrentAgg = (pm_npa_smps_int_rep*)info->current_aggregation;
    unsigned numberOfClients = 0;
    unsigned clientCount = 0;
    unsigned numberofMasters = 0;

    numberOfClients = rpm_get_num_clients(info->resource_handle);
    numberofMasters = rpm_get_num_ees();

    for(clientCount = numberofMasters; clientCount < numberOfClients; clientCount++)
    {
        // Get the current state for the child resource
        rpm_get_current_request(info->resource_handle, clientCount, &clientType, (const void **)&state); 

        if(clientType == PM_RPM_NPA_INT_CLIENT_LDO_DEPENDENT)              
        {                
            if(info->client == clientCount)
            {		
                ldoClientRequest = (pm_npa_ldo_int_rep*)info->new_state;
            } 
            else
            {            
                ldoClientRequest = (pm_npa_ldo_int_rep*)state;
            }            

            // A valid internal LDO client should have resource_id greater than zero
            if(ldoClientRequest->resource_id > 0) 
            {
                if(TRUE == out_bypass)//check if out of bypass is needed
                {
                    //if( (smpsCurrentAgg->global_byp_en == 0) || 
                    //    (smpsCurrentAgg->uvol != ldoClientRequest->bypass_uv) ||
                    //    (PM_NPA_BYPASS_DISALLOWED == ldoClientRequest->byp_allowed) )
                    {
                        pm_rpm_ldo_bypass_cb(ldoClientRequest->device_index, ldoClientRequest->resource_id, (pm_on_off_type)0);
                    }
                }
                else // check if in bypass is needed
                {
                    if(/* (smpsCurrentAgg->global_byp_en == 1) && */
                        (smpsCurrentAgg->uvol == ldoClientRequest->bypass_uv) &&
                        (PM_NPA_BYPASS_ALLOWED == ldoClientRequest->byp_allowed) )
                    {
                        pm_rpm_ldo_bypass_cb(ldoClientRequest->device_index, ldoClientRequest->resource_id, (pm_on_off_type)1);
                    }
                }
            }
        }
    }
}

pm_npa_smps_data_type* pm_rpm_smps_get_resource_data(rpm_resource_type resource)
{
    if(RPM_SMPS_A_REQ == resource)
    {
        return &RPM_SMPS_A_REQ_data;
    }
    else if(RPM_SMPS_B_REQ == resource)
    {
        return &RPM_SMPS_B_REQ_data;
    }
    else
    {
        return NULL;
    }
}

void pm_rpm_smps_settle(rpm_application_info *info, pm_npa_smps_int_rep* previous_aggregation)
{
    pm_npa_smps_int_rep *smps_int_rep =(pm_npa_smps_int_rep *)info->current_aggregation;
    pm_npa_smps_data_type *smps_data = (pm_npa_smps_data_type*)info->cb_data;
    uint8  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;
    uint32 settling_time = smps_data->settlingTime[internal_resource_index];
    pm_smps_data_type* smps_ptr = NULL;

    // for voltage change, do wait here
    if( settling_time != 0)
    {
        PM_OFFTARGET_SWEVENT(PMIC_SMPS_START_SETTLING, info->id, (smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1), settling_time, NULL);
        PM_DEBUG_SWEVENT(PMIC_SMPS_START_SETTLING, info->id, (smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1), settling_time);

        //Get the PmicResource from the call back data.
        if( (smps_data->resourceType == RPM_SMPS_A_REQ) || (smps_data->resourceType == RPM_SMPS_B_REQ) )
        {
            smps_ptr = smps_data->smpsDriverData;
		}
        CORE_VERIFY_PTR(smps_ptr);
		if(smps_int_rep->sw_en > previous_aggregation->sw_en) // rail is just turned on
        {
            // poll VREG_OK
            pm_rpm_check_vreg_settle_status(settling_time, &(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, internal_resource_index);
        }
        else // voltage change
        {
            /* FTS STEPPER_DONE status is not not reliable for FTS, so we will do busywait instead */
            /* Will need to add revision detection later when the HW has this bug fixed */
            if((smps_ptr->pm_pwr_data.pwr_specific_info != NULL) && 
               (PM_HW_MODULE_FTS == smps_ptr->pm_pwr_data.pwr_specific_info[internal_resource_index].periph_type))
            {
                DALSYS_BusyWait(settling_time);
            }
            else
            {
                // poll STEPPER_DONE
                pm_rpm_check_stepper_settle_status(settling_time, &(smps_ptr->pm_pwr_data), smps_ptr->comm_ptr, internal_resource_index);
            }
        }

        // reset the settling time
        smps_data->settlingTime[internal_resource_index] = 0;

        PM_OFFTARGET_SWEVENT(PMIC_SMPS_END_SETTLING, info->id, (smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1), settling_time, NULL);
        PM_DEBUG_SWEVENT(PMIC_SMPS_END_SETTLING, info->id, (smps_data->resourceType == RPM_SMPS_A_REQ ? 0: 1));
    }
}

