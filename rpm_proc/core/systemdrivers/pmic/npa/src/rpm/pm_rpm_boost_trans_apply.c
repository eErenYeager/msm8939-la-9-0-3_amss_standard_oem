/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

PMIC BOOST Translation and Apply (Aggregate)Functionality

GENERAL DESCRIPTION
This file contains translation and aggregate functions for NPA Device layer

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2013           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
06/11/13   hs      Adding settling time for regulators.
05/17/13   hs      PMIC SWEVENT revise.
04/25/13   aks     Code Refactoring: Removing PMIC_SubRsc from pwr algs   
04/12/13   hs       Code refactoring.
02/27/13   hs       Code refactoring.
01/29/13   aks     Adding support for 5v boost as a separate driver
===========================================================================*/
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "npa.h"
#include "rpmserver.h"
#include "pm_npa.h"
#include "comm_manager.h"

#include "pm_target_information.h"
#include "pm_rpm_boost_trans_apply.h"
#include "pm_npa_device_boost.h"
#include "CoreVerify.h"

static pm_npa_boost_data_type RPM_BOOST_A_REQ_data;
static pm_npa_boost_data_type RPM_BOOST_B_REQ_data;

// internal function prototypes
void pm_rpm_boost_translation(rpm_translation_info *info);
void pm_rpm_boost_apply(rpm_application_info *info);
void pm_rpm_boost_pre_dependency_analysis(rpm_application_info *info, pm_npa_boost_int_rep* previous_aggregation);
void pm_rpm_boost_post_dependency_analysis(rpm_application_info *info, pm_npa_boost_int_rep* previous_aggregation);
int pm_rpm_boost_aggregate(rpm_application_info *info);
void pm_rpm_boost_execute_driver(rpm_application_info *info, pm_npa_boost_int_rep* previous_aggregation);
void pm_rpm_boost_settle(rpm_application_info *info);

void pm_rpm_boost_translation(rpm_translation_info *info)
{
    unsigned type, length, *value;
    pm_npa_boost_int_rep *cache;

    cache = (pm_npa_boost_int_rep *)(info->dest_buffer);
    
    while(kvp_get(info->new_kvps, &type, &length, (const char **)&value))
    {
        // Need size to match.
        if(sizeof(npa_resource_state) != length)
            continue;

        switch(type)
        {
        case PM_NPA_KEY_SOFTWARE_ENABLE:
            {
                cache->sw_en = *value;
                break;
            }
        default:
            {
                //should never go here
            }
        }
    }
}

void pm_rpm_boost_apply(rpm_application_info *info)
{
    int                      change_detected = 0;
    pm_npa_boost_int_rep     previous_aggregation = *((pm_npa_boost_int_rep*)(info->current_aggregation));
    uint64                   settling_time = 0;
    pm_npa_boost_data_type  *boost_data = (pm_npa_boost_data_type *)info->cb_data;
    uint8                    internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;

    PM_OFFTARGET_SWEVENT(PMIC_BOOST_START_APPLY, info->id, (((pm_npa_boost_data_type*)info->cb_data)->resourceType == RPM_BOOST_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_BOOST_START_APPLY, info->id, (((pm_npa_boost_data_type*)info->cb_data)->resourceType == RPM_BOOST_A_REQ ? 0: 1));

    if(0 == info->settling_time) // this is a new request
    {
        // Aggregate the results, but don't call driver
        change_detected = pm_rpm_boost_aggregate(info);
    
        if(change_detected != 0 )
        {
            pm_rpm_boost_dependency_execute(info, &previous_aggregation);
        }
        else
        {
            PM_OFFTARGET_SWEVENT(PMIC_BOOST_NO_CHANGE, info->id, (((pm_npa_boost_data_type*)info->cb_data)->resourceType == RPM_BOOST_A_REQ ? 0: 1), 0, NULL);
            PM_DEBUG_SWEVENT(PMIC_BOOST_NO_CHANGE, info->id, (((pm_npa_boost_data_type*)info->cb_data)->resourceType == RPM_BOOST_A_REQ ? 0: 1));
        }
    }
    else // this is an old request to check if the rail has settled
    {
        //take the current system time
        settling_time = time_service_now(); //in time tick

        if(settling_time < info->settling_time) // still need more time to settle
        {
            //get the remaining settling time
            settling_time = info->settling_time - settling_time;

            //convert from time tick to micro-second
            settling_time = pm_convert_timetick_to_time(settling_time); // in micro second

            if(settling_time < PM_RPM_SETTTLING_TIME_THRESHOLD) // if the remaining settling time is small enough, wait internally
            {
                // update the interal settling time
                boost_data->settlingTime[internal_resource_index] = settling_time;

                //settling
                pm_rpm_boost_settle(info);

                // reset the settling time
                info->settling_time = 0;
            }
        }
        else // settling time passed, the rail should already settled
        {
            //reset the settling time
            info->settling_time = 0;
        } //end if(settling_time < info->settling_time)
    } //end if(0 == info->settling_time)
}

void pm_rpm_boost_dependency_execute(rpm_application_info *info, pm_npa_boost_int_rep* previous_aggregation)
{
    // Check the pre-operational dependencies
    pm_rpm_boost_pre_dependency_analysis(info, previous_aggregation);

    // Execute the driver calls once all dependencies have been executed    
    pm_rpm_boost_execute_driver(info, previous_aggregation);

    // Check the post operational dependencies
    pm_rpm_boost_post_dependency_analysis(info, previous_aggregation);
}

int pm_rpm_boost_aggregate(rpm_application_info *info)
{
    int change_detected = 0;
    pm_npa_boost_data_type* boost_data = (pm_npa_boost_data_type*)info->cb_data;
    unsigned clientType;
    unsigned *settingValue;
    pm_npa_boost_int_rep* boostClientRequest;
    pm_npa_vs_int_rep* vsClientRequest;
    void* state = NULL;
    unsigned clientCount = 0;

    unsigned numberOfClients = rpm_get_num_clients(info->resource_handle);

    // For each client aggregate the correct values.
    pm_npa_boost_int_rep aggregatedIntRep = {0};
    
    for(clientCount = 0; clientCount < numberOfClients; clientCount++)
    {
        rpm_get_current_request(info->resource_handle, clientCount, &clientType, (const void **)&state);        

         // EE Client and Internal BOOST clients
        if( (clientType == 0) || (clientType == PM_RPM_NPA_CLIENT_BOOST_A_REQ) )
        {
            if(boost_data->railInfo[info->id -1].AccessAllowed)
            {
                if(info->client == clientCount)
                {       
                    boostClientRequest = (pm_npa_boost_int_rep*)info->new_state;
                }
                else
                {
                    boostClientRequest = (pm_npa_boost_int_rep*)state;
                }
                
                if(boostClientRequest != NULL)
                {
                    PM_OFFTARGET_SWEVENT(PMIC_BOOST_EE_REQUEST, PM_NPA_VREG_BOOST, info->id, 
                                         (boost_data->resourceType == RPM_BOOST_A_REQ ? 0: 1), boostClientRequest);

                    // Aggregate Software Enable
                    aggregatedIntRep.sw_en = MAX(aggregatedIntRep.sw_en, boostClientRequest->sw_en);
                  
                }
            }
        }           
        else if(clientType == PM_RPM_NPA_INT_CLIENT_VS_DEPENDENT)
        {
            if(info->client == clientCount)
            {       
                vsClientRequest = (pm_npa_vs_int_rep*)info->new_state;
            }
            else
            {
                vsClientRequest = (pm_npa_vs_int_rep*)state;
            }
            if( NULL != vsClientRequest)
            {
                PM_OFFTARGET_SWEVENT(PMIC_BOOST_INTERNAL_REQUEST, PM_NPA_VREG_VS, info->id, 
                                         (boost_data->resourceType == RPM_BOOST_A_REQ ? 0: 1), vsClientRequest);

                // Aggregate Software Enable - Child needs power
                aggregatedIntRep.sw_en = MAX(aggregatedIntRep.sw_en, vsClientRequest->sw_en);

            }
        }
        else  // This is for Operational dependency
        {
            if(info->client == clientCount)
            {
                settingValue = (unsigned *)info->new_state;
            }
            else
            {
              settingValue = state;
            }
            
            if(clientType == PM_RPM_NPA_INT_CLIENT_SW_ENABLE_DEPENDENT) // Internal client that registers voltage vote with another resource
            { aggregatedIntRep.sw_en          = MAX(aggregatedIntRep.sw_en,       *settingValue); }
              
        }
    }
    
    // input checking
    if(boost_data->railInfo[info->id -1].AlwaysOn)
    {
        aggregatedIntRep.sw_en = 1;
    }
    
    change_detected = pm_rpm_int_copy(&aggregatedIntRep, (pm_npa_boost_int_rep *)info->current_aggregation, sizeof(pm_npa_boost_int_rep));

    return change_detected;
}

void pm_rpm_boost_pre_dependency_analysis(rpm_application_info *info, pm_npa_boost_int_rep* previous_aggregation)
{
    pm_pwr_resource_dependency_info      *depInfo = NULL;
    pm_npa_boost_int_rep  *cur_data = (pm_npa_boost_int_rep*)info->current_aggregation;
    pm_npa_boost_data_type     *cb_data = (pm_npa_boost_data_type*)info->cb_data;

    PM_OFFTARGET_SWEVENT(PMIC_BOOST_START_PRE_DEP,info->id, (cb_data->resourceType == RPM_BOOST_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_BOOST_START_PRE_DEP, info->id, cb_data->resourceType == RPM_BOOST_A_REQ ? 0: 1);

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_BOOST_A_REQ)
    {
        depInfo = &RPM_BOOST_A_REQ_data.depInfo[info->id];
    }
    
   //AKS: Need to think about removing this code since
    //BOOST does not have any parent depedndencies
    CORE_VERIFY_PTR(depInfo);
   if (depInfo->parent_source_dependency != NULL)
   {
        // Detect if this is a pre-operation change on the parent
        // These conditions are
        // If the BOOST goes from a disabled to an enabled state
    
        if( cur_data->sw_en > previous_aggregation->sw_en )
        {
            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_boost_int_rep), info->current_aggregation);
        } 
    }
    

    PM_OFFTARGET_SWEVENT(PMIC_BOOST_END_PRE_DEP, info->id, (cb_data->resourceType == RPM_BOOST_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_BOOST_END_PRE_DEP, info->id, cb_data->resourceType == RPM_BOOST_A_REQ ? 0: 1);
}

void pm_rpm_boost_post_dependency_analysis(rpm_application_info *info, pm_npa_boost_int_rep* previous_aggregation)
{
    pm_pwr_resource_dependency_info      *depInfo = NULL;
    pm_npa_boost_int_rep  *cur_data = (pm_npa_boost_int_rep*)info->current_aggregation;
    pm_npa_boost_data_type     *cb_data = (pm_npa_boost_data_type*)info->cb_data;
    unsigned             change_detected = 0;

    PM_OFFTARGET_SWEVENT(PMIC_BOOST_START_POST_DEP, info->id, (cb_data->resourceType == RPM_BOOST_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_BOOST_START_POST_DEP, info->id, cb_data->resourceType == RPM_BOOST_A_REQ ? 0: 1);

    // Get the resource dependency information
    if(cb_data->resourceType == RPM_BOOST_A_REQ)
    {
        depInfo = &RPM_BOOST_A_REQ_data.depInfo[info->id];
    }
    //AKS: Need to think about removing this code since
    // BOOST does not have parent source dependency
    CORE_VERIFY_PTR(depInfo);
    if( depInfo->parent_source_dependency != NULL )
    {
        // Detect if this is a post-operation change on the parent
        // These conditions are
        // If the BOOST goes from a enabled to an disabled state
        if( cur_data->sw_en < previous_aggregation->sw_en )
        {
            change_detected = 1;
        }

        if(1== change_detected )
        {
            rpm_issue_request(depInfo->parent_source_dependency->resource_type, 
                depInfo->parent_source_dependency->internal_resource_index, 
                depInfo->parent_source_dependency_client_handle,
                sizeof(pm_npa_boost_int_rep), info->current_aggregation);
        }  
    }
    
    PM_OFFTARGET_SWEVENT(PMIC_BOOST_END_POST_DEP, info->id, (cb_data->resourceType == RPM_BOOST_A_REQ ? 0: 1), 0, NULL);
    PM_DEBUG_SWEVENT(PMIC_BOOST_END_POST_DEP, info->id, cb_data->resourceType == RPM_BOOST_A_REQ ? 0: 1);
}

void pm_rpm_boost_execute_driver(rpm_application_info *info, pm_npa_boost_int_rep* previous_aggregation)
{
    pm_npa_boost_int_rep *boost_int_rep =(pm_npa_boost_int_rep *)info->current_aggregation;
    pm_npa_boost_data_type *boost_data = (pm_npa_boost_data_type *)info->cb_data;
    pm_boost_data_type* boost_ptr = NULL;
    uint8  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;
    uint64  settling_time_vregOK = 0;
    uint64  settling_time = 0;

    PM_OFFTARGET_SWEVENT(PMIC_BOOST_AGG1, PM_NPA_VREG_BOOST, info->id, 
                            (((pm_npa_boost_data_type*)info->cb_data)->resourceType == RPM_BOOST_A_REQ ? 0: 1), info->current_aggregation);
    PM_DEBUG_SWEVENT(PMIC_BOOST_AGG1, info->id, (((pm_npa_boost_data_type*)info->cb_data)->resourceType == RPM_BOOST_A_REQ ? 0: 1), 
                     ((pm_npa_boost_int_rep*)info->current_aggregation)->sw_en);

    //Get the PmicResource from the call back data.
    if(boost_data->resourceType == RPM_BOOST_A_REQ)
    {
        boost_ptr = boost_data->boostDriverData;
    }
    else
    {
        // We should never get here
        PM_SWEVENT(PMIC_RPM_ERROR, 5, 1); //BOOST ID is 5, 1 means NULL Call back data
        return;
    }

    if(boost_ptr != NULL)
    {            
      /* State set */
      pm_pwr_sw_enable_alg(&boost_ptr->pm_pwr_data, boost_ptr->comm_ptr, internal_resource_index, (pm_on_off_type)boost_int_rep->sw_en);
      // -------------- Settling time ------------------
 
      //rail turns on, adding settling time.
      if(boost_int_rep->sw_en > previous_aggregation->sw_en)
      {
          //calculate the settling time.
          settling_time_vregOK = boost_data->settlingTimeInfo->vreg_ok_us;

          //take the current system time
          settling_time = time_service_now() + pm_convert_time_to_timetick(settling_time_vregOK);

          // feedback to RPM server by modifying info
          info->settling_time = settling_time;
      }

      // -------------- Settling time ------------------
    }
}

void pm_rpm_boost_register_resources(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    pm_npa_boost_data_type *boost_data = NULL;
    uint32 dep_prop_id_arr[] = {PM_PROP_BOOSTA_DEP, PM_PROP_BOOSTB_DEP};
    uint32 rail_prop_id_arr[] = {PM_PROP_BOOSTA_RAIL, PM_PROP_BOOSTB_RAIL};
    uint8 pmic_index = (resourceType == RPM_BOOST_A_REQ ? 0: 1);

    CORE_VERIFY(pmic_index < (sizeof(dep_prop_id_arr)/sizeof(dep_prop_id_arr[0])));
    CORE_VERIFY(pmic_index < (sizeof(rail_prop_id_arr)/sizeof(rail_prop_id_arr[0])));

    if(num_npa_resources > 0 )
    {
        boost_data = pm_rpm_boost_get_resource_data(resourceType);
        if(boost_data != NULL)
        {
            boost_data->boostDriverData = pm_boost_get_data(pmic_index);

            CORE_VERIFY_PTR(boost_data->boostDriverData);

            boost_data->resourceType = resourceType;
            boost_data->depInfo = (pm_pwr_resource_dependency_info*)pm_target_information_get_specific_info(dep_prop_id_arr[pmic_index]);
            boost_data->railInfo = (pm_rpm_boost_rail_info_type*)pm_target_information_get_specific_info(rail_prop_id_arr[pmic_index]);
            boost_data->settlingTimeInfo = (pm_pwr_settling_time_info_type*)pm_target_information_get_common_info(PM_PROP_BOOST_SETTLING_TIME);
            pm_malloc(sizeof(uint32)*num_npa_resources, (void**)&(boost_data->settlingTime) );
            DALSYS_memset(boost_data->settlingTime, 0, sizeof(uint32)*num_npa_resources);

            rpm_register_resource(resourceType, num_npa_resources + 1 , sizeof(pm_npa_boost_int_rep), pm_rpm_boost_translation, pm_rpm_boost_apply, (void *)boost_data);
        }
    } 

   // pm_rpm_boost_read_trim(resourceType, num_npa_resources);
}

void pm_rpm_boost_register_resource_dependencies(rpm_resource_type resourceType, uint32 num_npa_resources)
{
    uint8 count = 0;
    pm_pwr_resource *dependency = NULL;
    pm_pwr_resource_dependency_info *depInfo = NULL;
    pm_npa_boost_int_rep *current_agg = NULL;
    pm_boost_data_type* boost_ptr = NULL;
    pm_on_off_type en_status = PM_OFF;

    if(num_npa_resources > 0)
    {
        if(RPM_BOOST_A_REQ == resourceType)
        {
            depInfo = RPM_BOOST_A_REQ_data.depInfo;
        }
        else if(RPM_BOOST_B_REQ == resourceType)
        {
            depInfo = RPM_BOOST_B_REQ_data.depInfo;
        }
       
        for(count = 1; count < num_npa_resources + 1; count++)
        {
            rpm_get_aggregated_request_buffer(resourceType, count, (const void**)&current_agg);
            if(current_agg != NULL)
            {
                if(RPM_BOOST_A_REQ == resourceType)
                {
                    boost_ptr = (pm_boost_data_type*)RPM_BOOST_A_REQ_data.boostDriverData;
                }
                else if(RPM_BOOST_B_REQ == resourceType)
                {
                    boost_ptr = (pm_boost_data_type*)RPM_BOOST_B_REQ_data.boostDriverData;
                }
                CORE_VERIFY_PTR(boost_ptr);
                DALSYS_memset(current_agg, 0, sizeof(pm_npa_boost_int_rep));
                pm_pwr_sw_enable_status_alg(&boost_ptr->pm_pwr_data, boost_ptr->comm_ptr, count-1, &en_status);
                current_agg->sw_en = en_status;
            }
        }

        if(depInfo)
        {
            for(count = 1; count < num_npa_resources + 1; count++)
            {
                if(depInfo[count].parent_source_dependency != NULL)
                {
                    dependency = depInfo[count].parent_source_dependency;

                    // create the internal client
                    rpm_register_internal_client_type(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_BOOST_DEPENDENT, sizeof(pm_npa_boost_int_rep));
                    
                    depInfo[count].parent_source_dependency_client_handle = 
                        rpm_create_client(dependency->resource_type, dependency->internal_resource_index, PM_RPM_NPA_INT_CLIENT_BOOST_DEPENDENT);                   
                }

                //pm_rpm_register_operational_dependency_int_client(&depInfo[count]);
            }
        }
    }  
}


pm_npa_boost_data_type* pm_rpm_boost_get_resource_data(rpm_resource_type resource)
{
    if(RPM_BOOST_A_REQ == resource)
    {
        return &RPM_BOOST_A_REQ_data;
    }
    if(RPM_BOOST_B_REQ == resource)
    {
        return &RPM_BOOST_B_REQ_data;
    }
    else
    {
        return NULL;
    }
}

void pm_rpm_boost_settle(rpm_application_info *info)
{
    pm_npa_boost_data_type *boost_data = (pm_npa_boost_data_type *)info->cb_data;
    uint8  internal_resource_index = (uint8)(info->id - 1); // RPM resource starts from 1; our internal resource index starts from 0;
    uint32 settling_time = boost_data->settlingTime[internal_resource_index];
    pm_boost_data_type* boost_ptr = NULL;

    // for voltage change, do wait here
    if( settling_time != 0)
    {
        PM_OFFTARGET_SWEVENT(PMIC_BOOST_START_SETTLING, (boost_data->resourceType == RPM_BOOST_A_REQ ? 0: 1), settling_time, 0, NULL);
        PM_DEBUG_SWEVENT(PMIC_BOOST_START_SETTLING, info->id, (boost_data->resourceType == RPM_BOOST_A_REQ ? 0: 1), settling_time);

        //Get the PmicResource from the call back data.
        if( (boost_data->resourceType == RPM_BOOST_A_REQ) || (boost_data->resourceType == RPM_BOOST_B_REQ) )
        {
            boost_ptr = boost_data->boostDriverData;
		}
		CORE_VERIFY_PTR(boost_ptr);  

        // poll VREG_OK
        pm_rpm_check_vreg_settle_status(settling_time, &boost_ptr->pm_pwr_data, boost_ptr->comm_ptr, internal_resource_index);

        // reset the settling time
        boost_data->settlingTime[internal_resource_index] = 0;

        PM_OFFTARGET_SWEVENT(PMIC_BOOST_END_SETTLING, (boost_data->resourceType == RPM_BOOST_A_REQ ? 0: 1), settling_time, 0, NULL);
        PM_DEBUG_SWEVENT(PMIC_BOOST_END_SETTLING, info->id, (boost_data->resourceType == RPM_BOOST_A_REQ ? 0: 1));
    }
}




