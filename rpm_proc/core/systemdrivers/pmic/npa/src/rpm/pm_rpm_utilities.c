/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

PMIC RPM Utilities Functionality

GENERAL DESCRIPTION
This file contains utility functions for trans apply aggregation layer

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2012           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/npa/src/rpm/pm_rpm_utilities.c#1 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
07/02/14   rk      PM8916: SW workaround for LDO11 damage issue with faulty uSD cards (CR - 686702)
09/11/13   rk      Code refactoring.
09/03/12   rk      Removed code related to VS and BOOST.
02/27/13   hs      Code refactoring.
01/29/13   aks     Adding support for Boost as separate peripheral
05/18/12   wra     Created.
===========================================================================*/
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "rpmserver.h"
#include "pm_rpm_utilities.h"
#include "CoreVerify.h"
#include "timetick.h"

/* Settling errors log info */
#define PM_SETTLE_ERR_LOG_ARRAY_SIZE 5

typedef struct
{
    uint64 time_stamp;
    uint32 requested_time_us;
    uint32 actual_time_us;
    uint8 pmic_index;
    uint8 periph_type;
    uint8 periph_index;
}pm_settle_info_type;

pm_settle_info_type pm_settle_vreg_ok_err_info[PM_SETTLE_ERR_LOG_ARRAY_SIZE] = {0};

/*===========================================================================

                 FUNCTION IMPLEMENTATION 

===========================================================================*/

unsigned pm_rpm_retrieve_operand_value( pm_pwr_resource_operand * operand) 
{
    unsigned leftValue;
    //Get the internal rep information for left and right operands
    if(operand->operand_type == PM_PWR_OPERAND__RESOURCE)
    {
        leftValue = pm_rpm_get_component_resource_value(operand->resource->resource_type, 
            operand->resource->internal_resource_index, operand->resource_key); 
    }
    else if(operand->operand_type == PM_PWR_OPERAND__STATIC)
    {
        // It is very unlikely this will get called
        leftValue = operand->static_value;
    }
    else
    {
        // TODO: fill in the LUT functionality
        // Has to be a LUT
        leftValue = 0;
    }    
    return leftValue;
}

boolean pm_rpm_review_condition(pm_pwr_resource_operation *condition)
{
    boolean condtion_is_true = 0;
    pm_pwr_resource_operand *leftOp;
    unsigned                 leftValue;
    pm_pwr_resource_operand *rightOp;
    unsigned                 rightValue;

    leftOp = condition->left_operand;
    rightOp = condition->right_operand;

    leftValue = pm_rpm_retrieve_operand_value(leftOp);
    rightValue = pm_rpm_retrieve_operand_value(rightOp);

    switch((pm_pwr_resource_operation_type)condition->operation_type)
    {
        case PM_RESOURCE_OPERATION__GT:
        {
            condtion_is_true = leftValue > rightValue;
            break;
        }
        case PM_RESOURCE_OPERATION__GT_EQ:
        {
            condtion_is_true = leftValue >= rightValue;
            break;
        }
        case PM_RESOURCE_OPERATION__LT:
        {
            condtion_is_true = leftValue < rightValue;
            break;
        }
        case PM_RESOURCE_OPERATION__LT_EQ:
        {
            condtion_is_true = leftValue <= rightValue;
            break;
        }
        case PM_RESOURCE_OPERATION__EQ_EQ:
        {
            condtion_is_true = leftValue == rightValue;
            break;
        }
        default:
        {
            condtion_is_true = 0;
            break;
        }
    }

    return condtion_is_true;
}

unsigned pm_rpm_get_component_resource_value(rpm_resource_type resource_type, unsigned resource_index,
                          unsigned npa_key)
{
    unsigned value = 0;
    void* state = NULL;
    rpm_get_aggregated_request_buffer(resource_type, resource_index, (const void**)&state);

    switch(resource_type)
    {
        case RPM_CLK_BUFFER_A_REQ:
        case RPM_CLK_BUFFER_B_REQ:
        {
            switch(npa_key)
            {
                case PM_NPA_KEY_SOFTWARE_ENABLE:
                {
                    value = ((pm_npa_clk_buffer_int_rep *)state)->sw_enable;
                    break;
                }
                case PM_NPA_KEY_PIN_CTRL_CLK_BUFFER_ENABLE_KEY:
                {
                    value = ((pm_npa_clk_buffer_int_rep *)state)->pc_enable;
                    break;
                }
                default:
                {
                    value = 0;
                    break;
                }
            }
            break;
        }
        case RPM_VS_A_REQ:
        case RPM_VS_B_REQ:
        {
            switch(npa_key)
            {
                case PM_NPA_KEY_SOFTWARE_ENABLE:
                {
                    value = ((pm_npa_vs_int_rep *)state)->sw_en;
                    break;
                }
                case PM_NPA_KEY_PIN_CTRL_ENABLE:
                {
                    value = ((pm_npa_vs_int_rep *)state)->pc_en;
                    break;
                }
                case PM_NPA_KEY_CURRENT:
                {
                    value = ((pm_npa_vs_int_rep *)state)->ip;
                    break;
                }
                default:
                {
                    value = 0;
                    break;
                }
            }
            break;
        }
        case RPM_LDO_A_REQ:
        case RPM_LDO_B_REQ:
        {
            switch(npa_key)
            {
                case PM_NPA_KEY_SOFTWARE_ENABLE:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->sw_en;
                    break;
                }
                case PM_NPA_KEY_LDO_SOFTWARE_MODE:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->ldo_sw_mode;
                    break;
                }
                case PM_NPA_KEY_PIN_CTRL_ENABLE:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->pc_en;
                    break;
                }
                case PM_NPA_KEY_PIN_CTRL_POWER_MODE:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->pc_mode;
                    break;
                }
                case PM_NPA_KEY_CURRENT:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->ip;
                    break;
                }
                case PM_NPA_KEY_MICRO_VOLT: 
                {
                    value = ((pm_npa_ldo_int_rep *)state)->regulated_uvol;
                    break;
                }
                case PM_NPA_KEY_HEAD_ROOM:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->noise_hr;
                    break;
                }
                case PM_NPA_KEY_BYPASS_ALLOWED_KEY:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->byp_allowed;
                    break;
                }
                default:
                {
                    value = 0;
                    break;
                }
            }
            break;
        }
        case RPM_SMPS_A_REQ:
        case RPM_SMPS_B_REQ:
        {
            switch(npa_key)
            {
                case PM_NPA_KEY_SOFTWARE_ENABLE:
                {
                    value = ((pm_npa_smps_int_rep *)state)->sw_en;
                    break;
                }
                case PM_NPA_KEY_SMPS_SOFTWARE_MODE:
                {
                    value = ((pm_npa_smps_int_rep *)state)->smps_sw_mode;
                    break;
                }
                case PM_NPA_KEY_PIN_CTRL_ENABLE:
                {
                    value = ((pm_npa_smps_int_rep *)state)->pc_en;
                    break;
                }
                case PM_NPA_KEY_PIN_CTRL_POWER_MODE:
                {
                    value = ((pm_npa_smps_int_rep *)state)->pc_mode;
                    break;
                }
                case PM_NPA_KEY_CURRENT:
                {
                    value = ((pm_npa_smps_int_rep *)state)->ip;
                    break;
                }
                case PM_NPA_KEY_MICRO_VOLT: 
                {
                    value = ((pm_npa_smps_int_rep *)state)->uvol;
                    break;
                }
                case PM_NPA_KEY_FREQUENCY:
                {
                    value = ((pm_npa_smps_int_rep *)state)->freq;
                    break;
                }
                case PM_NPA_KEY_FREQUENCY_REASON:
                {
                    value = ((pm_npa_smps_int_rep *)state)->freq_reason;
                    break;
                }
                case PM_NPA_KEY_FOLLOW_QUIET_MODE: 
                {
                    value = ((pm_npa_smps_int_rep *)state)->quiet_mode;
                    break;
                }
                case PM_NPA_KEY_HEAD_ROOM:
                {
                    value = ((pm_npa_smps_int_rep *)state)->hr;
                    break;
                }
                case PM_NPA_KEY_BYPASS_ALLOWED_KEY:
                {
                    value = ((pm_npa_ldo_int_rep *)state)->byp_allowed;
                    break;
                }
                default:
                {
                    value = 0;
                    break;
                }
            }
            break;
        }
    }
    return value;
}

/*
void pm_rpm_register_operational_dependency_int_client(pm_pwr_resource_dependency_info *depInfo)
{
   unsigned conditionCount = 0;
   unsigned keyCount = 0;
   unsigned resultCount = 0;
   pm_pwr_resource_operation *pre_results;
   pm_pwr_resource_operation *post_results;

   if(depInfo)
   {
       pm_pwr_resource_condition_result **pre_conditionResults = depInfo->pre_operational_dependencies;
       pm_pwr_resource_condition_result **post_conditionResults = depInfo->post_operational_dependencies;

       //register pre first
       if( (pre_conditionResults != NULL) && (post_conditionResults != NULL) )
       {
           //We assume the size for pre and post is the same
           // Create internal clients for dependencies
           for (conditionCount = 0; conditionCount < NPA_ARRAY_SIZE(pre_conditionResults); conditionCount++)
           {
               pre_results = pre_conditionResults[conditionCount]->results;
               post_results = post_conditionResults[conditionCount]->results;

               for(resultCount = 0; resultCount < pre_conditionResults[conditionCount]->result_count; resultCount++)
               {
                   for(keyCount = 0; keyCount < 13; keyCount++)
                   {
                       // if the left operand key matches then create the internal client
                       if(pre_results[resultCount].left_operand->resource_key == keys_to_dependent_map[keyCount].npa_key)
                       {
                           // create the internal client for the pre
                           rpm_register_internal_client_type(pre_results[resultCount].left_operand->resource->resource_type, 
                               pre_results[resultCount].left_operand->resource->internal_resource_index, keys_to_dependent_map[keyCount].int_client_type, 
                               keys_to_dependent_map[keyCount].int_client_size);

                           pre_results[resultCount].left_operand_client_index = 
                               rpm_create_client(pre_results[resultCount].left_operand->resource->resource_type, 
                               pre_results[resultCount].left_operand->resource->internal_resource_index, keys_to_dependent_map[keyCount].int_client_type); 

                           // assign the same client type to post
                           post_results[resultCount].left_operand_client_index = pre_results[resultCount].left_operand_client_index; 

                           break;
                       }
                   }
               }
           }
       }
   }
}
*/

/*
void pm_rpm_execute_operational_conditions( pm_pwr_resource_condition_result ** preConditions, pm_pwr_timing_type timing) 
{
    unsigned conditionCount = 0;
    unsigned resultCount = 0;
    unsigned settingValue = 0;
    boolean condtion_is_true = 0;

    pm_pwr_resource_operation *condition;
    pm_pwr_resource_operation *results;
    pm_pwr_resource_operand *rightOp;
    pm_pwr_resource_operand *leftOp;  

    if(preConditions != NULL)
    {
        for (conditionCount = 0; conditionCount < NPA_ARRAY_SIZE(preConditions); conditionCount++)
        {
            condition = preConditions[conditionCount]->condition;
            results = preConditions[conditionCount]->results;

            // Determine if the condition is true
            condtion_is_true = pm_rpm_review_condition(condition);
            if(condtion_is_true == TRUE)
            {
                for(resultCount = 0; resultCount < preConditions[conditionCount]->result_count; resultCount++)
                {
                    // if the result is a pre-condition then execute
                    if(((pm_pwr_timing_type)results[resultCount].timingType) == timing)
                    {
                        rightOp = results[resultCount].right_operand;
                        leftOp = results[resultCount].left_operand;
                        settingValue = pm_rpm_retrieve_operand_value(rightOp);

                        // issue the request
                        rpm_issue_request(leftOp->resource->resource_type, 
                            leftOp->resource->internal_resource_index, 
                            results[resultCount].left_operand_client_index,
                            sizeof(unsigned), (void*)&settingValue);
                    }
                }
            }
        }
    }
}
*/

int pm_rpm_int_copy(void *source, void *destination, size_t num)
{
    int change_detected = 0;

    change_detected = memcmp(destination, source, num);

    if(change_detected != 0)
    {
        DALSYS_memcpy(destination, source, num);
    }

    return change_detected;
}

boolean pm_rpm_check_vreg_settle_status(uint32 settling_time_us, pm_pwr_data_type *pwr_res, pmiC_IComm *comm_ptr, uint8 resource_index)
{
    boolean  vreg_status = FALSE;
    uint32   time_passed = 0;
    static uint8 pm_settle_vreg_ok_err_index = 0;

    pm_pwr_sw_enable_vreg_ok_status_alg(pwr_res, comm_ptr, resource_index, &vreg_status);

    while(!vreg_status)
    {
        //time guard
        if(time_passed >= settling_time_us*10)
        {
            if(pm_settle_vreg_ok_err_index >= PM_SETTLE_ERR_LOG_ARRAY_SIZE)
            {
                // reset the settling err index
                pm_settle_vreg_ok_err_index = 0;
            }

            // saving the settling info for the error cases
            pm_settle_vreg_ok_err_info[pm_settle_vreg_ok_err_index].time_stamp = timetick_get64();  
            pm_settle_vreg_ok_err_info[pm_settle_vreg_ok_err_index].requested_time_us = settling_time_us;  
            pm_settle_vreg_ok_err_info[pm_settle_vreg_ok_err_index].actual_time_us = time_passed;  
            pm_settle_vreg_ok_err_info[pm_settle_vreg_ok_err_index].pmic_index = comm_ptr->mpCommInfo->mDeviceInfo->mDeviceIndex;  
            pm_settle_vreg_ok_err_info[pm_settle_vreg_ok_err_index].periph_type = pwr_res->pwr_specific_info->periph_type;
            pm_settle_vreg_ok_err_info[pm_settle_vreg_ok_err_index].periph_index = resource_index;  

            // increment the settling err index
            pm_settle_vreg_ok_err_index++;
            
            PM_SWEVENT(PMIC_RPM_ERROR, PM_RPM_ERR__SETTLING_TIMEOUT);
            break;
        }

        DALSYS_BusyWait(PM_RPM_SETTLING_TIME_PERIODIC);//us

        pm_pwr_sw_enable_vreg_ok_status_alg(pwr_res, comm_ptr, resource_index, &vreg_status);
            
        //keep track of time
        time_passed += PM_RPM_SETTLING_TIME_PERIODIC;
    }

    PM_SWEVENT(PMIC_SETTLING_TIME, settling_time_us, time_passed); 
    
    return vreg_status; 
}

boolean pm_rpm_check_stepper_settle_status(uint32 settling_time_us, pm_pwr_data_type *pwr_res, pmiC_IComm *comm_ptr, uint8 resource_index)
{
    boolean  stepper_status = FALSE;
    uint32   time_passed = 0;

    //wait for the calculated settling time
    DALSYS_BusyWait(settling_time_us);

    time_passed = settling_time_us;

    pm_pwr_volt_level_stepper_done_status_alg(pwr_res, comm_ptr, resource_index, &stepper_status);

    while(!stepper_status)
    {
        //time guard
        if(time_passed >= settling_time_us*10)
        {
            PM_SWEVENT(PMIC_RPM_ERROR, PM_RPM_ERR__SETTLING_TIMEOUT);
            break;
        }

        DALSYS_BusyWait(PM_RPM_SETTLING_TIME_PERIODIC);//us

        pm_pwr_volt_level_stepper_done_status_alg(pwr_res, comm_ptr, resource_index, &stepper_status);
            
        //keep track of time
        time_passed += PM_RPM_SETTLING_TIME_PERIODIC;
    }

    PM_SWEVENT(PMIC_SETTLING_TIME, settling_time_us, time_passed);  
    
    return stepper_status;  
}

