/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

P M    NPA    D E V I C E    S E R V I C E S

GENERAL DESCRIPTION
This file contains initialization functions for NPA Device layer

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2010           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/npa/src/rpm/pm_rpm_npa_device.c#1 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
09/10/13   rk      Removed VS and BOOST related code.
04/23/13   hs      Fixed the naming convention in \config.
04/12/13   hs      Code refactoring.
02/27/13   hs      Code refactoring.
01/29/13   aks     Adding support for Boost as separate peripheral 
08/07/12   hs      Added support for 5V boost.
02/28/12   wra     Redone for Badger
===========================================================================*/
/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "npa.h"
#include "rpmserver.h"
#include "npa_resource.h"
#include "assert.h"
#include "err.h"
#include <string.h>
#include "DALDeviceId.h"


#include "pm_qc_pmic.h"
#include "pmapp_npa.h"

#include "pm_target_information.h"
#include "pm_npa.h"
#include "pm_rpm_npa_device.h"
#include "pm_npa_device.h"

#include "pm_rpm_clk_buffer_trans_apply.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "pm_rpm_smps_trans_apply.h"
#include "pm_rpm_boost_trans_apply.h"
#include "pm_rpm_vs_trans_apply.h"

#ifdef PMIC_OFFTARGET_TESTING
    #undef CORE_VERIFY
    #define CORE_VERIFY(x)  (x)
#endif

/*===========================================================================

                  LOCAL CONSTANT AND MACRO DEFINITIONS

===========================================================================*/

/*===========================================================================

                         LOCAL FUNCTION PROTOTYPES

===========================================================================*/

/*===========================================================================

                           VARIABLES DEFINITIONS

===========================================================================*/


/*===========================================================================

                            FUNCTION DEFINITIONS

===========================================================================*/

void pm_rpm_device_init (void)
{
    uint32 num_of_smpsa = 0;
    uint32 num_of_ldoa = 0;
    uint32 num_of_clka = 0;
    uint32 num_of_vsa = 0;
    uint32 num_of_smpsb = 0;
    uint32 num_of_boosta = 0;
    uint32 num_of_boostb = 0;

    num_of_ldoa = pm_target_information_get_count_info(PM_PROP_LDOA_NUM);
    num_of_smpsa = pm_target_information_get_count_info(PM_PROP_SMPSA_NUM);
    num_of_boosta = pm_target_information_get_count_info(PM_PROP_BOOSTA_NUM);
    num_of_vsa = pm_target_information_get_count_info(PM_PROP_VSA_NUM);
    num_of_clka = (PM_MAX_NUM_CLKS-1);
    num_of_smpsb = pm_target_information_get_count_info(PM_PROP_SMPSB_NUM);
    num_of_boostb = pm_target_information_get_count_info(PM_PROP_BOOSTB_NUM);


    //* Initialize all Device A Resources Types */
    pm_rpm_smps_register_resources(RPM_SMPS_A_REQ, num_of_smpsa);        
    pm_rpm_boost_register_resources(RPM_BOOST_A_REQ, num_of_boosta); 
    pm_rpm_ldo_register_resources(RPM_LDO_A_REQ, num_of_ldoa); 
    pm_rpm_clk_buffer_register_resources(RPM_CLK_BUFFER_A_REQ, num_of_clka);
    pm_rpm_vs_register_resources(RPM_VS_A_REQ, num_of_vsa);
    //pm_rpm_boost_byp_register_resources(RPM_BOOST_BYP_A_REQ, num_of_boost_bypa); 

    //* Initialize all Device B Resources Types */
    pm_rpm_smps_register_resources(RPM_SMPS_B_REQ, num_of_smpsb); 
    pm_rpm_boost_register_resources(RPM_BOOST_B_REQ, num_of_boostb); 
    //pm_rpm_boost_byp_register_resources(RPM_BOOST_BYP_B_REQ, num_of_boost_bypb); 

    //* Initialize all Device A Resources Types dependencies */
    pm_rpm_smps_register_resource_dependencies(RPM_SMPS_A_REQ, num_of_smpsa);        
    pm_rpm_boost_register_resource_dependencies(RPM_BOOST_A_REQ, num_of_boosta);
    pm_rpm_ldo_register_resource_dependencies(RPM_LDO_A_REQ, num_of_ldoa); 
    pm_rpm_clk_buffer_register_resource_dependencies(RPM_CLK_BUFFER_A_REQ, num_of_clka);
    pm_rpm_vs_register_resource_dependencies(RPM_VS_A_REQ, num_of_vsa);
    //pm_rpm_boost_byp_register_resource_dependencies(RPM_BOOST_BYP_A_REQ, num_of_boost_bypa);

    //* Initialize all Device B Resources Types dependencies*/
    pm_rpm_smps_register_resource_dependencies(RPM_SMPS_B_REQ, num_of_smpsb); 
    pm_rpm_boost_register_resource_dependencies(RPM_BOOST_B_REQ, num_of_boostb); 
    //pm_rpm_boost_byp_register_resource_dependencies(RPM_BOOST_BYP_B_REQ, num_of_boost_bypb);         
}

/*===========================================================================

FUNCTION pm_rpm_proc_npa_device_init

DESCRIPTION
This function initializes the NPA RPM Device layer for PMIC.

Does the following:
* Initializes the PMIC NPA Device Node and resources.

INPUT PARAMETERS
None.

RETURN VALUE
None.

DEPENDENCIES
NPA Framework should be available in the build being compiled for

SIDE EFFECTS
NONE.

===========================================================================*/
void pm_rpm_proc_npa_device_init (void)
{                 
    /* Initialize Local RPM Clk regime client node and Resource */
    pm_rpm_device_init ();
} /* end of pm_rpm_proc_npa_device_init() */

