#ifndef PM_RPM_PROC_NPA_H
#define PM_RPM_PROC_NPA_H

/*! \file
 *  
 *  \brief  pm_rpm_proc_npa.h ----This file contains prototype definitions processor npa layer
 *  \details This file contains prototype definitions processor npa layer
 *          which consists mainly of the initialization function prototype
 *  
 *    PMIC code generation Version: 1.0.0.0
 *    PMIC code generation NPA Client Version: MSM8974 branch 1.0 ver 8_21_2012 - Not Approved
 *    PMIC code generation NPA Device Setting Value Version: MSM8974 branch 1.0 ver 8_21_2012 - Not Approved
 *    This file contains code for Target specific settings and modes.
 *  
 *    &copy; Copyright (c) 2010-2012 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module over time.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/npa/inc/pm_rpm_npa.h#1 $ 
when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/02/14   rk      PM8916: SW workaround for LDO11 damage issue with faulty uSD cards (CR - 686702)
06/11/13   hs      Adding settling time for regulators.
04/23/13   hs       Fixed the naming convention in \config.
04/12/13   hs       Code refactoring.
1/29/13    aks      Adding support for 5v boost as a separate driver
===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "comdef.h"
#include "rpm.h"
#include "npa.h"
#include "npa_resource.h"

/*===========================================================================

                        TYPE DEFINITIONS

===========================================================================*/

typedef struct
{
   rpm_resource_type resource_type;
   unsigned resource_id;
   unsigned client_type;
   unsigned internal_client_index;
}pm_rpm_local_resource_client_dependency;

typedef struct
{
   unsigned                                 dependency_count;
   pm_rpm_local_resource_client_dependency *dependencies;
}pm_rpm_resource_client_dependency_info;

#ifdef _WIN32
#pragma pack(push,1) // Save previous, and turn on 1 byte alignment
#endif
typedef struct
{
   unsigned sw_en:1; // [Disable (default), Enable] ? max aggregation (left to right)
   unsigned ldo_sw_mode:4; // [IPEAK (default), NPM] - max aggregation (left to right)
   unsigned pc_en:4; // [NONE, EN1, EN2, EN3, EN4] - ORed value of list
   unsigned pc_mode:5; // [NONE, EN1, EN2, EN3, EN4, SLEEPB] - ORed value of list
   unsigned resource_id:8; // This is needed for the LDO bypass call back from SMPS
   unsigned device_index:2; // This is needed for the LDO bypass call back from SMPS
   unsigned is_en_transition:1; // If((old sw_en == disable) && (new sw_en == enable) || new pc_en == enable then ldo_en_trans = true else ldo_en_trans = false
   unsigned byp_allowed:1; // [Allowed, Disallowed] - max aggregation (left to right)
   unsigned reserve1:6;
   // 32 bit boundary
   unsigned regulated_uvol:32; // uV
   // 32 bit boundary
   unsigned ip:13; // mA > 8 Amps
   unsigned noise_hr:19; // uV
   // 32 bit boundary
   unsigned bypass_uv:32; // bypass voltage
}pm_npa_ldo_int_rep;

typedef struct
{
   unsigned sw_en:1; // [Disable (default), Enable] ? max aggregation (left to right)
   unsigned smps_sw_mode:4; // [AUTO(default), IPEAK, NPM] - max aggregation (left to right)
   unsigned pc_en:4; // [NONE, EN1, EN2, EN3, EN4] - ORed value of list
   unsigned pc_mode:5; // [NONE, EN1, EN2, EN3, EN4, SLEEPB] - ORed value of list
   unsigned global_byp_en:1; // Keeps whether the child LDOs are allowed to go into bypass
   unsigned hr:17; // uV
   // 32 bit boundary
   unsigned uvol:32; // uV
   // 32 bit boundary
   unsigned ip:13; // mA > 8 Amps
   unsigned freq:5; // Frequency
   unsigned freq_reason:4; // ORed technologies from pm_npa_freq_reason_type
   unsigned quiet_mode:2; // [None, Quiet, Super Quiet] ? max aggregation (left to right)
   unsigned byp_allowed:1; // [Allowed, Disallowed] - max aggregation (left to right)
   // Level4 (Normal), Level5 (Turbo), Level6 (Super Turbo)]
   unsigned reserve1:7;
   // 32 bit boundary
}pm_npa_smps_int_rep;

typedef struct
{
   unsigned sw_en:1; // [Disable (default), Enable] ? max aggregation (left to right)
   unsigned pc_en:4; // [NONE, EN1, EN2, EN3, EN4] - ORed value of list
   // 32 bit boundary
   unsigned ip:13; // mA > 8 Amps
   unsigned reserve1:14;
    // 32 bit boundary
} pm_npa_vs_int_rep;

typedef struct
{
   unsigned sw_en:1; // [Disable (default), Enable] ? max aggregation (left to right)
   unsigned reserve1:31;
   // 32 bit boundary
   unsigned start_address:32; // uV
}pm_npa_pbs_int_rep;

typedef struct
{
   unsigned sw_enable:1; // Software Enable
   unsigned pc_enable:4; // Pin Control Enable
   unsigned reserved:27;
    // 32 bit boundary
}pm_npa_clk_buffer_int_rep;

typedef struct
{
   unsigned sw_en:1; // Software Enable
   unsigned reserved:31; //32-bit boundary
}pm_npa_boost_int_rep;

typedef struct
{
   unsigned sw_en:1; // Software Enable
   unsigned byp_allowed:1; // [Allowed, Disallowed] - max aggregation (left to right)
   unsigned reserved:30; //32-bit boundary
}pm_npa_boost_byp_int_rep;

#ifdef _WIN32
#pragma pack(pop) // Revert alignment to what it was previously
#endif

/*===========================================================================

                        DEFINITIONS

===========================================================================*/

#define PM_RPM_SETTLING_TIME_PERIODIC         25 //uS
#define PM_RPM_SETTTLING_TIME_THRESHOLD       50 //uS

typedef enum
{
   // Begin Internal regular client types
   PM_RPM_NPA_CLIENT_SMPS_A_REQ = 1, // This must be assigned to 1 to avoid the "invalid client type" error in the RPM code.
   PM_RPM_NPA_CLIENT_BOOST_A_REQ,
   PM_RPM_NPA_CLIENT_LDO_A_REQ,
   PM_RPM_NPA_CLIENT_NCP_A_REQ,
   PM_RPM_NPA_CLIENT_VS_A_REQ,
   PM_RPM_NPA_CLIENT_CLK_BUFFER_A_REQ,
   PM_RPM_NPA_CLIENT_BOOST_BYP_A_REQ,
   PM_RPM_NPA_CLIENT_SMPS_B_REQ,
   PM_RPM_NPA_CLIENT_BOOST_B_REQ,
   PM_RPM_NPA_CLIENT_LDO_B_REQ,
   PM_RPM_NPA_CLIENT_NCP_B_REQ,
   PM_RPM_NPA_CLIENT_VS_B_REQ,
   PM_RPM_NPA_CLIENT_CLK_BUFFER_B_REQ,
   PM_RPM_NPA_CLIENT_BOOST_BYP_B_REQ,
   
   // Begin Source Dependency based client types
   PM_RPM_NPA_INT_CLIENT_SMPS_DEPENDENT, // Internal client for SMPS sources
   PM_RPM_NPA_INT_CLIENT_BOOST_DEPENDENT, // Internal client for BOOST sources
   PM_RPM_NPA_INT_CLIENT_LDO_DEPENDENT, // Internal client for LDO sources
   PM_RPM_NPA_INT_CLIENT_VS_DEPENDENT, // Internal client for VS sources
   PM_RPM_NPA_INT_CLIENT_CLK_DEPENDENT, // Internal client for CLK sources
   PM_RPM_NPA_INT_CLIENT_BOOST_BYP_DEPENDENT, // Internal client for BOOST_BYP sources
   
   // Begin Operational Dependency based Client Types
   PM_RPM_NPA_INT_CLIENT_SW_ENABLE_DEPENDENT, // Internal client that registers voltage vote with another resource
   PM_RPM_NPA_INT_CLIENT_SW_MODE_DEPENDENT, // Internal client that registers SW Mode vote with another resource
   PM_RPM_NPA_INT_CLIENT_PIN_CONTROL_ENABLE_DEPENDENT, // Internal client that registers pc_en vote with another resource
   PM_RPM_NPA_INT_CLIENT_CLK_PIN_CONTROL_ENABLE_DEPENDENT, // Internal client that registers clock buffer pc_en vote with another resource
   PM_RPM_NPA_INT_CLIENT_PIN_CONTROL_MODE_DEPENDENT, // Internal client that registers pc_mode vote with another resource
   PM_RPM_NPA_INT_CLIENT_VOLTAGE_DEPENDENT, // Internal client that registers voltage vote with another resource
   PM_RPM_NPA_INT_CLIENT_CURRENT_DEPENDENT, // Internal client that registers peak current vote with another resource
   PM_RPM_NPA_INT_CLIENT_FREQ_DEPENDENT, // Internal client that registers voltage vote with another resource
   PM_RPM_NPA_INT_CLIENT_FREQ_REASON_DEPENDENT, // Internal client that registers voltage vote with another resource
   PM_RPM_NPA_INT_CLIENT_QUIET_MODE_DEPENDENT, // Internal client that registers voltage vote with another resource
   PM_RPM_NPA_INT_CLIENT_HEADROOM_DEPENDENT, // Internal client that registers headroom voltage vote with another resource
   PM_RPM_NPA_INT_CLIENT_BYPASS_EN_DEPENDENT, // Internal client that registers bypass enabled vote with another resource
   PM_RPM_NPA_INT_CLIENT_BYPASS_ALLOWED_DEPENDENT, // Internal client that registers bypass allowed vote with another resource
   PM_RPM_NPA_INT_CLIENT_CORNER_LEVEL_DEPENDENT, // Internal client that registers corner level vote with another resource
}pm_rpm_nonstandard_internal_client_types;

typedef struct
{
   unsigned npa_key;
   pm_rpm_nonstandard_internal_client_types int_client_type;
   unsigned int_client_size;
}pm_rpm_keys_to_dependent_map;

extern pm_rpm_keys_to_dependent_map keys_to_dependent_map[14];
extern npa_client_handle handle_rpm_init;

/*===========================================================================

                FUNCTION DEFINITIONS

===========================================================================*/

/*===========================================================================

FUNCTION pm_rpm_proc_npa_init

DESCRIPTION
This function initializes the NPA for rpm.

It does the following:
1)  It initializes the PMIC NPA software driver for nodes and resources.

INPUT PARAMETERS
None.

RETURN VALUE
None.

DEPENDENCIES
None.

SIDE EFFECTS
None.

===========================================================================*/
void pm_rpm_proc_npa_init(void);

/*===========================================================================

FUNCTION pm_rpm_railway_init

DESCRIPTION
This function initializes the PMIC interface for rpm railway.

It does the following:
1)  This function initializes the PMIC interface for rpm railway.

INPUT PARAMETERS
None.

RETURN VALUE
None.

DEPENDENCIES
None.

SIDE EFFECTS
None.

===========================================================================*/
void pm_rpm_railway_init(void);

//void pm_rpm_register_nonstandard_internal_client_types();

npa_resource_state pm_rpm_npa_resource_set (npa_resource      *resource,
                                            npa_client_handle  client,
                                            npa_resource_state mode_id);

npa_resource_state pm_rpm_npa_process_rsrc (npa_resource *resource,
                                            pm_rpm_local_resource_client_dependency *dep_info,
                                            npa_resource_state mode_id,
                                            uint32 index);


#endif //PM_RPM_PROC_NPA_H

