#ifndef PM_RPM_UTILITIES_H
#define PM_RPM_UTILITIES_H
 
/*! \file
 *  
 *  \brief  pm_rpm_utilities.h ----This file contains prototype definitions processor npa layer
 *  \details This file contains prototype definitions processor npa layer
 *          which consists mainly of the initialization function prototype
 *  
 *  
 *    &copy; Copyright 2012 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module over time.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/npa/inc/pm_rpm_utilities.h#1 $ 


when       who     what, where, why
--------   ---     ----------------------------------------------------------
07/02/14   rk      PM8916: SW workaround for LDO11 damage issue with faulty uSD cards (CR - 686702)
09/10/13   rk      Fixed the naming convention in \config.
09/10/13   rk      Code refactoring.

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "pm_rpm_npa.h"
#include "npa.h"
#include "pm_npa.h"
#include "pm_pwr_alg.h"
#include "com_dtypes.h"

enum
{
    PM_RPM_ERR__SUCCESS,
    PM_RPM_ERR__NULL_CALLBACK,
    PM_RPM_ERR__SETTLING_TIMEOUT,
    PM_RPM_ERR__NULL_POINTER
    
};
/*===========================================================================

FUNCTION pm_rpm_utilities_init

DESCRIPTION
    This function initializes the NPA for rpm.

    It does the following:
    1)  It initializes the PMIC NPA software driver for nodes and resources.

INPUT PARAMETERS
  None.

RETURN VALUE
  None.

DEPENDENCIES
  None.

SIDE EFFECTS
  None.

===========================================================================*/

//void pm_rpm_register_nonstandard_internal_client_types(void);

unsigned pm_rpm_retrieve_operand_value( pm_pwr_resource_operand * operand);

///**
// * @brief Returns value of the RPM internal representation field for any NPA resource type and associated key
// * @param npa_key The key that indicates which field in the internal representation should be returned
// * @param resource_type The resource type is used to cast the aggregated void* state
// * @param state Holds a pointer to the current state that will be looked into.
// * 
// * @return unsigned The value of the field in the internal representation that is needed.
// */
unsigned pm_rpm_get_component_resource_value(rpm_resource_type resource_type, unsigned resource_index,
                          unsigned npa_key);

///**
// * @brief function evaluates a condition and determines if condition is true or false
// * @param condition This is the condition to be evaluated as true or false
// * 
// * @return bool Returns true if the condition is true, otherwise returns false.
// */
boolean pm_rpm_review_condition(pm_pwr_resource_operation *condition);

/**
 * @brief Use this function to create operation condition internal clients
 * @param conditionResults The condition & result sets that need internal clients
 * 
 * @return void nothing
 */
void pm_rpm_register_operational_dependency_int_client(pm_pwr_resource_dependency_info *depInfo);

//void pm_rpm_execute_operational_conditions( pm_pwr_resource_condition_result ** preConditions, pm_pwr_timing_type timing);

/**
 * @brief This function is used to do memcpy and memcmp.
 * @param source Pointer to the source of data to be copied, type-casted to a pointer of type const void*.
 * @param destination Pointer to the destination array where the content is to be copied, type-casted to 
          a pointer of type void*.
 * @param num Number of bytes to copy.
 * 
 * @return Returns an integral value indicating the relationship between the content of the memory blocks:
   A zero value indicates that the contents of both memory blocks are equal.
   A value greater than zero indicates that the first byte that does not match in both memory blocks has a 
   greater value in ptr1 than in ptr2 as if evaluated as unsigned char values; And a value less than zero 
   indicates the opposite.
 */
int pm_rpm_int_copy(void *source, void *destination, size_t num);

/**
 * @brief This function is used to check if a regulator has settled by reading the real time status of VREG_OK register.
 * @param settling_time_us The time (in uS) that the regulator is supposed to take to settle.
 * @param vreg_ok_status Function pointer for checking the VREG_OK status
 * @param pwr_res Pointer to the power resource. 
 * @param resource Pointer to the driver resource. 
 * @param resource_index Resource index.
 * 
 * @return returns the final VREG_OK status.
 * @note vreg_ok_status function pointer needs to be initialized when this function gets called.
 */
boolean pm_rpm_check_vreg_settle_status(uint32 settling_time_us, pm_pwr_data_type *pwr_res, pmiC_IComm *comm_ptr, uint8 resource_index);


/**
 * @brief This function is used to check if a regulator has settled by reading the real time status of STEPPER_DONE register.
 * @param settling_time_us The time (in uS) that the regulator is supposed to take to settle.
 * @param stepper_done_status Function pointer for checking the STEPPER_DONE status
 * @param pwr_res Pointer to the power resource.
 * @param resource Pointer to the driver resource. 
 * @param resource_index Resource index.
 * 
 * @return returns the final STEPPER_DONE status.
 * @note stepper_done_status function pointer needs to be initialized when this function gets called.
 */
boolean pm_rpm_check_stepper_settle_status(uint32 settling_time_us, pm_pwr_data_type *pwr_res, pmiC_IComm *comm_ptr, uint8 resource_index);

#endif //PM_RPM_UTILITIES_H


