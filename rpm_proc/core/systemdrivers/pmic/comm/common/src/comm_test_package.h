#ifndef COMMTESTPACKAGE_H
#define COMMTESTPACKAGE_H

/*! \file
 *  \n
 *  \brief  CommTestPackage.h 
 *  \details  
 *  \n &copy; Copyright 2010 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/comm/common/src/comm_test_package.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
========================================================================== */

/*! \file 
*  \n
*  \brief  COMMTESTPACKAGE.h ---- 
*  \details This header file contains class declaration for CommTestPackage
*  \n
*  \n &copy; 2009 Qualcomm Technologies Incorporated, All Rights Reserved
*/

//namespace PmicObjects
//{
//    namespace COMM
//    {
        /*! \class CommTestPackage CommTestPackage.h 
        *  \brief This is the class contains device specific information which
        *   will help the CommManager create test communications when drivers
        *   are being tested.
        *  \details During normal operation the PMIC system is initialized
        *   using from the top down in this order
        *       1. Processor created
        *       2. Communication channels initialized (I2C,SSBI,...)
        *       3. PMIC devices created
        *       4. PMIC specific drivers created
        *  When drivers are tested we need to bypass step 1-3. The
        *  test driver must provide the information instead using the CommTestPackage.
        *
        *  <b>Handles the following</b>
        *  \n None
        *
        *  <b>Listens for the following events</b>
        *  \n None
        *
        *  <b>Generates the following events</b>
        *  \n None
        */
        //class CommTestPackage
        //{
        //public:
        //    CommTestPackage(PmicDevices::DeviceType deviceType, uint32 busIndex, CommType commType);

        //    CommType GetCommType();
        //    void SetCommType(CommType commType);

        //    IComm* GetIComm();
        //    void SetIComm(IComm *pComm);
        //    uint8 GetBusIndex();
        //    PmicDevices::DeviceType GetDeviceType();
        //    PmicDevices::pm_model_type mPmicModel;
        //    uint8 mPmicVariant;
        //    uint8 mDeviceIndex;

        //private:
        //    IComm* mpComm;
        //    uint32 mBusIndex;
        //    CommType mCommType;
        //    PmicDevices::DeviceType mDeviceType;

        //private:
        //    friend class CommManager;
        //    //friend class ISL9519;
        //    //friend class PmicDevice;

        //};
//    } /* namespace COMM */
//} /* namespace PmicObjects */

#endif // COMMTESTPACKAGE_H
