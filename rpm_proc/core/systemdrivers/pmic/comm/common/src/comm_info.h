#ifndef _COMM_INFO_H_
#define _COMM_INFO_H_

/*! \file comm_info.h
 *  \n
 *  \brief Comm Info related structure type and function prototypes
 *  \n 
 *  \n &copy; Copyright 2010-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/comm/common/src/comm_info.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------    
09/12/13   rk      Code refactoring.    
03/20/12   hs      Added a member vailable for DAL SPMI handle.      
08/19/11   jtn     Refactor spelling error
03/15/11   APU     Removing unused include file.
12/08/10   wra     RPM memory reduction. 
                   Removing unused Variables and methods
========================================================================== */
/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "comm_type.h"
#include "device_info.h" 

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/
/** 
  @struct pmiC_CommInfo
  @brief  This struct is used to store the PMIC Comm 
          information.
*/
typedef struct
{
   pmiC_DeviceInfo *mDeviceInfo;
   uint32 mBusIndex;
   CommType mCommType;
   boolean mDeviceInitialized;
}pmiC_CommInfo;

/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/

void pmiC_CommInfo_Init(pmiC_CommInfo *me, pmiC_DeviceInfo *deviceInfo, CommType commType, uint8 slaveIdIndex);
void pmiC_CommInfo_InitCopy(pmiC_CommInfo *me, const pmiC_CommInfo *rhs);

#endif /* _COMM_INFO_H_ */

