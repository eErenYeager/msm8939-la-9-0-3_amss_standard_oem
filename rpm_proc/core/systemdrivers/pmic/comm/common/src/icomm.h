#ifndef ICOMM_H
#define ICOMM_H

/*! \file
 *  \n
 *  \brief  icomm.h 
 *  \details  
 *  \n &copy; Copyright 2010 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/comm/common/src/icomm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/26/12   hs      Added TransactionManager as a friend class.
12/08/10   wra     Re-writing CommManager to reduce memory foot print. Added 
                     member variables for device information.
                     pm_model_type mPmicModel;
                     uint8 mPmicVariant;
                     PmicObjects::PmicDevices::DeviceType mDeviceType;
========================================================================== */

/*! \file 
*  \n
*  \brief  ICOMM.h ---- 
*  \details This header file contains class declarations base
*    class for communication operations.
*  \n
*  \n &copy; 2009 Qualcomm Technologies Incorporated, All Rights Reserved
*/

#include "pm_qc_pmic.h"
#include "bare_metal_info.h"
#include "comm_type.h"
#include "device_info.h"
#include "comm_info.h"

#define PM_COMM_DEVIDX(comm_ptr) (comm_ptr->mpCommInfo->mDeviceInfo->mDeviceIndex)


typedef enum 
{
    NoOperation,
    StartOperation,
    EndOperation
}TransactionOperation;

// Function Pointers for Virtual Methods
typedef pm_err_flag_type (*pmiC_func_IComm_ReadByte)(void* me, pm_register_address_type addr, pm_register_data_type* data, uint8 priority);
typedef pm_err_flag_type (*pmiC_func_IComm_ReadByteMask)(void* me, pm_register_address_type addr, pm_register_mask_type mask, pm_register_data_type* data);
typedef pm_err_flag_type (*pmiC_func_IComm_WriteByte)(void* me, pm_register_address_type addr, pm_register_data_type data, uint8 priority);
typedef pm_err_flag_type (*pmiC_func_IComm_WriteByteMask)(void* me, pm_register_address_type addr, pm_register_mask_type mask, pm_register_data_type data, uint8 priority);
typedef pm_err_flag_type (*pmiC_func_IComm_BankedReadModifyWrite)(void* me, uint8 bank_number, pm_register_address_type address, pm_register_mask_type mask, pm_register_data_type data);
typedef pm_err_flag_type (*pmiC_func_IComm_WriteByteArray)(void* me, pm_register_address_type address, unsigned int numBYtes, pm_register_data_type* data, uint8 priority);
typedef pm_err_flag_type (*pmiC_func_IComm_ReadByteArray)(void* me, pm_register_address_type address, unsigned int numBYtes, pm_register_data_type* data, uint8 priority);
typedef pm_err_flag_type (*pmiC_func_IComm_ReadBytesAtomic)(void* me, pm_register_address_type* addressBuf, unsigned int numBytes, pm_register_data_type* dataBuf, uint8 priority);

typedef struct pmiC_IComm_STRUCT
{
   // Function Pointers for Virtual Methods
   pmiC_func_IComm_ReadByte ReadByte;
   pmiC_func_IComm_ReadByteMask ReadByteMask;
   pmiC_func_IComm_WriteByte WriteByte;
   pmiC_func_IComm_WriteByteMask WriteByteMask;
   pmiC_func_IComm_BankedReadModifyWrite BankedReadModifyWrite;
   pmiC_func_IComm_WriteByteArray WriteByteArray;
   pmiC_func_IComm_ReadByteArray ReadByteArray;
   pmiC_func_IComm_ReadBytesAtomic ReadBytesAtomic;
   
   
   /********************************************
             DATA MEMBER DECLARATION
   *********************************************/
   CommType commType;
   pmiC_CommInfo* mpCommInfo;
   struct pmiC_IComm_STRUCT* pInnerComm;
}pmiC_IComm;
/*************************************
NOTE: Initializer IMPLEMENTATION
**************************************/
void pmiC_IComm_Init(pmiC_IComm *me, pmiC_IComm  **innerComm, CommType  type);
/*************************************
NOTE: Regular METHOD IMPLEMENTATION
**************************************/
pm_err_flag_type pmiC_IComm_BankedRead(pmiC_IComm *me, uint8 bank_number, pm_register_address_type address, pm_register_data_type* data);

#endif // ICOMM_H
