/*! \file 
*  \n
*  \brief  pmiC_IComm.cpp ---- pmiC_IComm RELATED DEFINITION
*  \n
*  \n This header file contains class implementation of the pmiC_IComm
*  \n messaging interface
*  \n
*  \n &copy; Copyright 2009 Qualcomm Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/comm/common/src/icomm.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
08/19/11   jtn     Fix Klocwork warnings
11/9/2009  wra     File created

========================================================================== */

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/
#include "icomm.h"    /* pmiC_IComm class definition file   */

/*************************************
NOTE: VIRTUAL METHOD PROTOTYPES
**************************************/
static pm_err_flag_type pmiC_IComm_ReadByteMask(void *me, pm_register_address_type  addr, pm_register_mask_type  mask, pm_register_data_type*  data);

void pmiC_IComm_Init(pmiC_IComm *me, pmiC_IComm **innerComm , CommType type) // = FIRST_INVALID_COMM_INTERFACE
{
    // Initialize function pointers if necessary
    me->ReadByteMask = pmiC_IComm_ReadByteMask;

    if(innerComm != NULL)
    {
        me->pInnerComm = (pmiC_IComm*)*innerComm;
    }
    else
    {
        me->pInnerComm = NULL;
    }
    me->commType = type;
    me->mpCommInfo = NULL;
    
}

/*************************************
NOTE: VIRTUAL METHOD IMPLEMENTATION
**************************************/
pm_err_flag_type pmiC_IComm_ReadByteMask(void *me, pm_register_address_type addr, pm_register_mask_type mask, pm_register_data_type* data)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;
    err = ((pmiC_IComm*)me)->ReadByte(me, addr, data, 0);

    if(err == PM_ERR_FLAG__SUCCESS)
    {
        *data = *data & mask;
    }
    return err;
}

/*************************************
NOTE: Regular METHOD IMPLEMENTATION
**************************************/
pm_err_flag_type pmiC_IComm_BankedRead 
(
     pmiC_IComm *me, 
     uint8 bank_number ,
     pm_register_address_type address ,
     pm_register_data_type* data
 )
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;

    if ( bank_number > 7 )
    {
        err = PM_ERR_FLAG__PAR1_OUT_OF_RANGE ;
    }
    /* check for out-of-bounds index */
    else if ( address >= PM_MAX_REGS )
    {
        err = PM_ERR_FLAG__PAR2_OUT_OF_RANGE ;
    }
    else
    {
        /* shift the bank number to line up with it's location in the PMIC register */
        bank_number <<= 4 ;
        
        /* send a command to the PMIC telling it that we want to read the bank */
        err = me->WriteByteMask (me, address , 0xF0 , bank_number, 0) ;
        
        /* read the bank data */
        if ( err == PM_ERR_FLAG__SUCCESS )
        {
            err = me->ReadByte(me, address , data, 0) ;
        }
    }

    return err ;
}
