/*! \file 
*  \n
*  \brief  SpmiLiteCommV1.cpp ---- SPMI Version 2.0 RELATED DEFINITION
*  \n
*  \n This header file contains class implementation of the SPMI Version 
*  \n 2.0 messaging interface
*  \n
*  \n &copy; Copyright 2012 Qualcomm Technologies Incorporated, All Rights Reserved
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/comm/spmi_lite_comm/src/spmi_1_0_lite_comm.c#1 $

when        who     what, where, why
--------    ---     ----------------------------------------------------------
03/27/12    hs      File created

========================================================================== */

/*===========================================================================

INCLUDE FILES FOR MODULE

===========================================================================*/

#include "spmi_1_0_lite_comm.h"
#include "spmi_1_0_lite.h"

/*************************************
NOTE: VIRTUAL METHOD PROTOTYPES
**************************************/
static pm_err_flag_type pmiC_SpmiLiteCommV1_ReadByte(void *me, 
                                                     pm_register_address_type  addr, 
                                                     pm_register_data_type*  data, 
                                                     uint8  priority);
static pm_err_flag_type pmiC_SpmiLiteCommV1_WriteByte(void *me, 
                                                      pm_register_address_type  addr, 
                                                      pm_register_data_type  data, 
                                                      uint8  priority);
static pm_err_flag_type pmiC_SpmiLiteCommV1_WriteByteMask(void *me, 
                                                          pm_register_address_type  addr, 
                                                          pm_register_mask_type  mask, 
                                                          pm_register_data_type  data, 
                                                          uint8  priority);
static pm_err_flag_type pmiC_SpmiLiteCommV1_BankedReadModifyWrite(void *me, 
                                                                  uint8  bank_number, 
                                                                  pm_register_address_type  address, 
                                                                  pm_register_mask_type  mask, 
                                                                  pm_register_data_type  data);
static pm_err_flag_type pmiC_SpmiLiteCommV1_WriteByteArray(void *me, 
                                                           pm_register_address_type  address, 
                                                           unsigned  int numBytes, 
                                                           pm_register_data_type*  data,
                                                           uint8  priority);
static pm_err_flag_type pmiC_SpmiLiteCommV1_ReadByteArray(void *me, 
                                                          pm_register_address_type  address, 
                                                          unsigned  int numBytes, 
                                                          pm_register_data_type*  data, 
                                                          uint8  priority);
static pm_err_flag_type pmiC_SpmiLiteCommV1_ReadBytesAtomic(void *me, 
                                                            pm_register_address_type*  addressBuf, 
                                                            unsigned  int numBytes, 
                                                            pm_register_data_type*  dataBuf, 
                                                            uint8  priority);
/*************************************
NOTE: Initializer IMPLEMENTATION
**************************************/
void pmiC_SpmiLiteCommV1_Init(pmiC_SpmiLiteCommV1 *me, pmiC_IComm  **pInnerComm, uint32  uSlaveId)
{
    pmiC_IComm_Init((pmiC_IComm*)me,  pInnerComm, SPMI_1_0Type);

    ((pmiC_IComm *)me)->ReadByte = pmiC_SpmiLiteCommV1_ReadByte;
    ((pmiC_IComm *)me)->WriteByte = pmiC_SpmiLiteCommV1_WriteByte;
    ((pmiC_IComm *)me)->WriteByteMask = pmiC_SpmiLiteCommV1_WriteByteMask;
    ((pmiC_IComm *)me)->BankedReadModifyWrite = pmiC_SpmiLiteCommV1_BankedReadModifyWrite;
    ((pmiC_IComm *)me)->WriteByteArray = pmiC_SpmiLiteCommV1_WriteByteArray;
    ((pmiC_IComm *)me)->ReadByteArray = pmiC_SpmiLiteCommV1_ReadByteArray;
    ((pmiC_IComm *)me)->ReadBytesAtomic = pmiC_SpmiLiteCommV1_ReadBytesAtomic;

    me->mPeripheralSlaveID = uSlaveId;
   
}

/*************************************
NOTE: VIRTUAL METHOD IMPLEMENTATION
**************************************/
pm_err_flag_type pmiC_SpmiLiteCommV1_ReadByte(void *me, 
                                              pm_register_address_type  addr, 
                                              pm_register_data_type*  data, 
                                              uint8  priority)
{
    return pm_spmi_lite_read_byte( ((pmiC_SpmiLiteCommV1*)me)->mPeripheralSlaveID, addr, data, priority);
}

pm_err_flag_type pmiC_SpmiLiteCommV1_WriteByte(void *me, 
                                               pm_register_address_type  addr, 
                                               pm_register_data_type  data, 
                                               uint8  priority)
{
    return pm_spmi_lite_write_byte( ((pmiC_SpmiLiteCommV1*)me)->mPeripheralSlaveID, addr, data, priority);
}

pm_err_flag_type pmiC_SpmiLiteCommV1_WriteByteMask(void *me, 
                                                   pm_register_address_type  addr, 
                                                   pm_register_mask_type  mask, 
                                                   pm_register_data_type  data, 
                                                   uint8  priority)
{
    return pm_spmi_lite_write_byte_mask( ((pmiC_SpmiLiteCommV1*)me)->mPeripheralSlaveID, addr, mask, data, priority);
}

pm_err_flag_type pmiC_SpmiLiteCommV1_BankedReadModifyWrite(void *me, 
                                                           uint8  bank_number, 
                                                           pm_register_address_type  address, 
                                                           pm_register_mask_type  mask, 
                                                           pm_register_data_type  data)
{
    (void) me;
    (void)bank_number;
    (void)address;
    (void)mask;
    (void)data;
   
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED ;
}

pm_err_flag_type pmiC_SpmiLiteCommV1_WriteByteArray(void *me, 
                                                    pm_register_address_type  address, 
                                                    unsigned  int numBytes, 
                                                    pm_register_data_type*  data, 
                                                    uint8  priority)
{
    return  pm_spmi_lite_write_byte_array( ((pmiC_SpmiLiteCommV1*)me)->mPeripheralSlaveID, address, numBytes, data, priority);
}

pm_err_flag_type pmiC_SpmiLiteCommV1_ReadByteArray(void *me, 
                                                   pm_register_address_type  address,
                                                   unsigned  int numBytes, 
                                                   pm_register_data_type*  data, 
                                                   uint8  priority)
{
    return  pm_spmi_lite_read_byte_array( ((pmiC_SpmiLiteCommV1*)me)->mPeripheralSlaveID, address, numBytes, data, priority);
}

pm_err_flag_type pmiC_SpmiLiteCommV1_ReadBytesAtomic(void *me, 
                                                     pm_register_address_type*  addressBuf, 
                                                     unsigned  int numBytes, 
                                                     pm_register_data_type*  dataBuf, 
                                                     uint8  priority)
{
    (void)me;
    (void)addressBuf;
    (void)numBytes;
    (void)dataBuf;
    (void)priority;
   
    return PM_ERR_FLAG__FEATURE_NOT_SUPPORTED ;
}

