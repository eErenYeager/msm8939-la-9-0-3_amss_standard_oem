#ifndef SPMI_1_0_LITE_COMM__H
#define SPMI_1_0_LITE_COMM__H

/*! \file
 *  \n
 *  \brief  spmi_1_0_lite_comm.h 
 *  \details  
 *  \n &copy; Copyright 2012 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/comm/spmi_lite_comm/src/spmi_1_0_lite_comm.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
03/27/12    hs     Created
========================================================================== */

/*! \file 
*  \n
*  \brief  spmi_1_0_lite_comm.h ---- 
*  \details Comm interface that wraps the spmi_lite so that it can be used by RPM. 
*  \n
*  \n &copy; 2012 Qualcomm Technologies Incorporated, All Rights Reserved
*/

#include "icomm.h"

typedef struct pmiC_SpmiLiteCommV1_STRUCT
{
    pmiC_IComm super; // Inheritance from ICommstructure. This must be the first item in structure
   // Function Pointers for Virtual Methods
   
   
   /********************************************
             DATA MEMBER DECLARATION
   *********************************************/
   uint32 mPeripheralSlaveID;
}pmiC_SpmiLiteCommV1;

/*************************************
NOTE: Initializer IMPLEMENTATION
**************************************/
void pmiC_SpmiLiteCommV1_Init(pmiC_SpmiLiteCommV1 *me, pmiC_IComm  **pInnerComm, uint32  uSlaveId);

#endif // SPMI_1_0_LITE_COMM__H

