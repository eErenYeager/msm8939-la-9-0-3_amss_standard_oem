#ifndef __PM_MPM__IN_H__
#define __PM_MPM_IN_H__

/**
 * @file pm_mpm_internal.h
 *
 * Header file for internal declarations PMIC MPM VDD command driver.
 */
/*
  ====================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================

  $Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/framework/inc/pm_mpm_internal.h#1 $
  $DateTime: 2015/03/13 04:27:04 $
  $Author: pwbldsvc $

  ====================================================================
*/

/*=========================================================================
      Include Files
==========================================================================*/

#include "pm_mpm.h"


/*=========================================================================
      Type Definitions
==========================================================================*/

/**
  * Structure to store the index of the dynamically changing
  * elements for the SPMI sequence.
  */
typedef struct
{
  uint32 sleep_level_index;    /* retention index */
  uint32 active_level_index;   /* active index  */
  uint32 sw_mode_level_index;  /* SW mode before sleep index */
} pm_mpm_cmd_index_type;


/*----------------------------------------------------------------------------
 * Function : pm_mpm_cmd_init
 * -------------------------------------------------------------------------*/
/*!
   Get DAL device configuration. This code runs once.
     @dependencies
     pm_target_information_get_target_info
     @return
     None.
*/
void  pm_mpm_cmd_init( void );



#endif /* __PM_MPM_IN_H__ */

