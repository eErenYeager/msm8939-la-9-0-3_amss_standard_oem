#ifndef PM_UTILS_H
#define PM_UTILS_H

/*! \file
 *  
 *  \brief  pm_malloc.h ----This file contain PMIC wrapper function of DALSYS_Malloc()
 *  \details his file contain PMIC wrapper function of DALSYS_Malloc()
 *  
 *    &copy; Copyright 2012 Qualcomm Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

                EDIT HISTORY FOR MODULE

  This document is created by a code generator, therefore this section will
  not contain comments describing changes made to the module over time.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/framework/inc/pm_utils.h#1 $ 

when       who     what, where, why
--------   ---     ----------------------------------------------------------
06/11/13   hs      Adding settling time for regulators.
06/20/12   hs      Created

===========================================================================*/

/*===========================================================================

                     INCLUDE FILES 

===========================================================================*/
#include "swevent.h"
#include "time_service.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

#if defined( PMIC_NPA_LOGGING)
  #define PM_SWEVENT(...)                SWEVENT(__VA_ARGS__)
#else
  #define PM_SWEVENT(...) 
#endif

#if defined(PMIC_NPA_DEBUG_LOGGING)
  #define PM_DEBUG_SWEVENT(...)          SWEVENT(__VA_ARGS__)   
#else
  #define PM_DEBUG_SWEVENT(...) 
#endif

#if defined(PMIC_OFFTARGET_TESTING)
#include "pm_test_utils.h"
  #define PM_OFFTARGET_SWEVENT(id, d0, d1, d2, d3)         PM_OFFTARGET_SWEVENT_log(id, d0, d1, d2, d3)
#else
  #define PM_OFFTARGET_SWEVENT(id, d0, d1, d2, d3)
#endif // PMIC_OFFTARGET_TESTING

/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/

extern void pm_malloc(uint32 dwSize, void **ppMem);

extern uint64 pm_convert_time_to_timetick(uint64 time_us);

extern uint64 pm_convert_timetick_to_time(uint64 time_tick);

#endif // PM_UTILS_H

