/*! \file
*  \n
*  \brief  pm_mpm_cmd.c
*  \n
*  \n This file  implementes the SPMI shutdown/wakeup command buffer.
*  \n
*  \n &copy; Copyright 2013 Qualcomm Technologies Incorporated, All Rights Reserved
*
*/
/* =======================================================================
Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/framework/src/pm_mpm.c#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
10/10/13   vtw     Created.
========================================================================== */

/*=========================================================================
      Include Files
==========================================================================*/

#include "DALSys.h"
#include "pm_mpm.h"
#include "pm_version.h"
#include "pm_dal_prop_ids.h"
#include "railway.h" /* for railway_get_corner_voltage() */
#include "pm_smps.h"
#include "Chipinfo.h"
#include "pm_mpm_internal.h"

#include "pmapp_npa.h"

#include "pm_target_information.h"

/*=========================================================================
      Definitions
==========================================================================*/

#define CEILING_DIV(a, b)  (((a)+((b)-1))/(b))

/*=========================================================================
      Variables.
==========================================================================*/

/**
 * Config data programs the MPM registers
 * for the SPMI sleep/wakeup command sequence.
 */
static pm_mpm_cfg_type* sleep_cfg = NULL;
static pm_mpm_cfg_type* active_cfg = NULL;

/* SPMI sequence index. */
static pm_mpm_cmd_index_type* cmd_index_ptr;

/* SPMI Delay table from MPM2 HPG
  4bit   # XO cycles     Delay in usec
  0000   0               0
  0001   192             10
  0010   480             25
  0011   960             50
  0100   1920            100
  0101   2880            150
  0110   3840            200
  0111   4800            250
  1000   5760            300
  1001   6720            350
  1010   7680            400
  1011   9600            500
  1100   11520           600
  1101   13440           700
  1110   16320           850
  1111   19200           1000
*/
static uint16 delay_table[16] =
{0, 10, 25, 50, 100, 150, 200, 250, 300, 350, 400, 500, 600, 700, 850, 1000};

/* ============================================================================
**  Function : pm_mpm_GetDelayFromMicrovolts
** ============================================================================
*/
/*!
   Calculate the delay based on active and retention volttage.

   @param  pCfg - pointer to MPM config data buffer that sent to the PMIC.
   @retval None
   Note: This function is copied from MPM driver code.

*/
static uint16 pm_mpm_get_delay_from_microvolts
(
  uint32 off_microvolts,
  uint32 on_microvolts
)
{
  uint32 desired_delay = 0;
  uint32 max = 16;
  uint32 min = 0;
  uint32 i = 0;
  uint32 mid = max / 2;

  // PM8026 step FTS2 and LDO @1.5mV/usec
  desired_delay = (uint32)CEILING_DIV((on_microvolts - off_microvolts), 1500);
  // Add 50uS of additional delay, per pmic systems
  desired_delay += 50;

  //max of 1000us delay
  if (desired_delay > 1000)
    return 15;

  for (i = 0; i < 4; i++)
  {
    if (delay_table[mid] == desired_delay)
      break;
    if (delay_table[mid] < desired_delay)
    {
      if (i == 3)
      {
        mid++;
        break;
      }
      min = mid;
    }
    else
    {
      if (i == 3)
      {
        break;
      }
      max = mid;
    }
    mid = ((max + min) / 2);
  }

  return mid;

} /* pm_mpm_get_delay_from_microvolts */

/* ============================================================================
**  Function : pm_mpm_cmd_rail_cx
** ============================================================================
*/
/*!
    Program the SPMI sequence cmd for VDD_CX voltage rail.

   @retval PM_ERR_FLAG__SUCCESS on success else PMIC error code.

*/
static pm_err_flag_type pm_mpm_cmd_rail_cx (void)
{
  pm_err_flag_type ret_val;
  uint32 n_value, n_vset;
  uint32 delay;
  uint32 active_mv = 0, sleep_mv = 0;
  uint8 sw_mode = 0;
  pm_mpm_cmd_index_type* cx_index = &cmd_index_ptr[0];

  /* Rail ID support by railway on this target.  */
  int cx_id = rail_id("vddcx");

  /* Sleep voltage from railway. */
  sleep_mv = railway_get_corner_voltage(cx_id, RAILWAY_RETENTION);

  /* Get current active CX voltage before sleep. */
  ret_val = pm_railway_get_voltage(PM_RAILWAY_CX , &active_mv);
  if (! (ret_val == PM_ERR_FLAG__SUCCESS) )
  {
    return ret_val;
  }
  /* Current vset. */
  ret_val = pm_railway_calculate_vset(PM_RAILWAY_CX, active_mv, &n_vset);
  if (! (ret_val == PM_ERR_FLAG__SUCCESS) )
  {
    return ret_val;
  }

  /* Delay for this command. */
  delay = pm_mpm_get_delay_from_microvolts(sleep_mv, active_mv);

  /* Value to turn off CX. */
  ret_val = pm_railway_calculate_vset(PM_RAILWAY_CX, sleep_mv, &n_value);
  if (! (ret_val == PM_ERR_FLAG__SUCCESS) )
  {
    return ret_val;
  }
  sleep_cfg[cx_index->sleep_level_index].data = n_value;

  /*  Turn on CX. */
  active_cfg[cx_index->active_level_index].data = n_vset;
  active_cfg[cx_index->active_level_index].delay = delay;

  /* Restore SW mode.  */
  ret_val = pm_smps_sw_mode_status_raw(0, PM_SMPS_1,  &sw_mode);
  if (! (ret_val == PM_ERR_FLAG__SUCCESS) )
  {
    return ret_val;
  }
  active_cfg[cx_index->sw_mode_level_index].data = sw_mode;

  /* return success */
  return ret_val;

}/* pm_mpm_cmd_rail_cx */


/* ============================================================================
**  Function : pm_mpm_cmd_rail_mx
** ============================================================================
*/
/*!
    Program the SPMI sequence cmd for VDD_MX voltage rail.

   @retval PM_ERR_FLAG__SUCCESS on success else PMIC error code.

*/
static pm_err_flag_type pm_mpm_cmd_rail_mx (void)
{
  pm_err_flag_type ret_val = PM_ERR_FLAG__SUCCESS;
  uint32 n_value, n_vset;
  uint32 active_mv = 0, sleep_mv = 0;
  uint32 mx_delay, cx_delay;
  uint8 sw_mode = 0;
  pm_mpm_cmd_index_type* mx_index = &cmd_index_ptr[1];

  /* Rail ID support by railway on this target.  */
  int mx_id = rail_id("vddmx");

   /* Sleep voltage from railway. */
  sleep_mv = railway_get_corner_voltage(mx_id, RAILWAY_RETENTION);

  /* Get MX on voltage and vset value before sleep. */
  /* Get current active MX voltage before sleep. */
  ret_val = pm_railway_get_voltage(PM_RAILWAY_MX , &active_mv);
  if (! (ret_val == PM_ERR_FLAG__SUCCESS) )
  {
    return ret_val;
  }
  /* Current vset. */
  ret_val = pm_railway_calculate_vset(PM_RAILWAY_MX, active_mv, &n_vset);
  if (! (ret_val == PM_ERR_FLAG__SUCCESS) )
  {
    return ret_val;
  }

  /* Delay for this command. */
  mx_delay = pm_mpm_get_delay_from_microvolts(sleep_mv, active_mv);

  /* Value to turn off MX. */
  ret_val = pm_railway_calculate_vset(PM_RAILWAY_MX, sleep_mv, &n_value);
  if (! (ret_val == PM_ERR_FLAG__SUCCESS) )
  {
    return ret_val;
  }
  sleep_cfg[mx_index->sleep_level_index].data = n_value;
  sleep_cfg[mx_index->sleep_level_index].delay = mx_delay;

  /* turn MX on. */
  active_cfg[mx_index->active_level_index].data = n_vset;

  cx_delay = active_cfg[mx_index->active_level_index].delay;

  /* Program max delay. */
  if (mx_delay > cx_delay)
  {
    active_cfg[mx_index->active_level_index].delay = mx_delay;
  }
  else
  {
    sleep_cfg[mx_index->sleep_level_index].delay = cx_delay;
  }

  /* Restore SW mode.  */
  ret_val = pm_smps_sw_mode_status_raw(0, PM_SMPS_2, &sw_mode);  
  if (! (ret_val == PM_ERR_FLAG__SUCCESS) )
  {
    return ret_val;
  }
  active_cfg[mx_index->sw_mode_level_index].data = sw_mode;

  return ret_val;

} /* pm_mpm_cmd_rail_mx */

/*----------------------------------------------------------------------------
 * Function : pm_mpm_cmd_init
 * -------------------------------------------------------------------------*/
/*!
   Get DAL device configuration. This code runs once.
     @dependencies
     pm_target_information_get_target_info
     @return
    PM_ERR_FLAG__SUCCESS success otherwise PMIC error.
*/
void pm_mpm_cmd_init (void)
{

  /* Init only happen once */
  if ( active_cfg && sleep_cfg )
  {
    return;
  }

  /* Get command device cfgs. */
  active_cfg = (pm_mpm_cfg_type*)pm_target_information_get_specific_info(PM_PROP_MPM_ACTIVE_CMDS);
  sleep_cfg =  (pm_mpm_cfg_type*)pm_target_information_get_specific_info(PM_PROP_MPM_SLEEP_CMDS);


  /* Get command index. */
  cmd_index_ptr = ( pm_mpm_cmd_index_type*)pm_target_information_get_specific_info(PM_PROP_MPM_CMD_INDICES);

} /* pm_mpm_cmd_init */

/*----------------------------------------------------------------------------
 * Function : pm_mpm_rail_cmd
 * -------------------------------------------------------------------------*/
/*!
    Description: Return a pointer to a config data buffer for MPM voltage rail
                 command.
    @param
      sleep_cfg_ptr: the destination buffer which receives
                     the config data for retention voltage
      active_cfg_ptr: the destination buffer which receives
                      the config data for active voltage
    @return
    PM_ERR_FLAG__SUCCESS success otherwise PMIC error.

    @dependencies
    railway_get_corner_voltage()
    pm_railway_calculate_vset()


    @sa None
*/
pm_err_flag_type pm_mpm_cmd_config
(
  pm_mpm_cfg_type ** sleep_cfg_ptr,
  pm_mpm_cfg_type ** active_cfg_ptr
)
{
  pm_err_flag_type ret_val = PM_ERR_FLAG__SUCCESS;

  /* Missing call to pm_init API. */
  if ( !active_cfg || !sleep_cfg )
  {
    return PM_ERR_FLAG__INVALID_POINTER;
  }

  /* CX rail config data for this request. */
  if ((ret_val = pm_mpm_cmd_rail_cx()) != PM_ERR_FLAG__SUCCESS)
  {
    return ret_val;
  }

  /* MX rail config data for this request. */
  if ((ret_val = pm_mpm_cmd_rail_mx()) != PM_ERR_FLAG__SUCCESS)
  {
    return ret_val;
  }

  /* Return pointer to config data. */
  *sleep_cfg_ptr = sleep_cfg;
  *active_cfg_ptr = active_cfg;

  return ret_val;

} /*  pm_mpm_cmd_config */











