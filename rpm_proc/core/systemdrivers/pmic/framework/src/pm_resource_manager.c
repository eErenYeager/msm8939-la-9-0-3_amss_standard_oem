/*! \file pm_resource_manager.c
 *
 *  \brief  This file contains the initialization functions for PMIC resource manager.
 *
 *  &copy; Copyright 2011-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/framework/src/pm_resource_manager.c#1 $

when       who     what, where, why
--------   ---     ---------------s-------------------------------------------
09/10/13   rk      Code refactoring.
09/03/12   rk      Removed VS related code.
02/27/13   hs      Code refactoring.

===========================================================================*/

/*===========================================================================

                            INCLUDE FILES

===========================================================================*/

#include "pm_resource_manager.h"
#include "pm_target_information.h"

#include "pm_processor.h"
#include "comm_manager.h"
#include "DALSys.h"

/* PMIC Driver includes */
#include "pm_smps_driver.h"
#include "pm_ldo_driver.h"
#include "pm_vs_driver.h"
#include "pm_boost_driver.h"
#include "pm_clk_driver.h"
#include "pm_rtc_driver.h"
#include "pm_pon_driver.h"

#include "hw_module_type.h"

/*===========================================================================

                        FUNCTION PROTOTYPES

===========================================================================*/

/*! \brief This function returns the parameter specified pmic resource.
 *
 *  \param[in] comm_ptr The comm object of the pmic resource.
 *
 *  \param[in] peripheral_info The peripheral info of the pmic resource.
 */
static void pm_resource_manager_create_pmic_resource(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info);

void pm_resource_manager_init(void)
{
    peripheral_info_type peripheral_info;

    uint32 device_index = 0;
    uint32 sid_index = 0;
    uint32 peripheral_index = 0;
    uint32 number_of_pmics = 0;

    pmiC_CommManager *comm_manager = pmiC_CommManager_GetSingleton();
    pmiC_IComm *comm_ptr = NULL;

    peripheral_info.analog_major_version = 0;
    peripheral_info.analog_minor_version = 0;
    peripheral_info.digital_major_version = 0;
    peripheral_info.digital_minor_version = 0;
    peripheral_info.peripheral_type = 0;
    peripheral_info.peripheral_subtype = 0;
    peripheral_info.base_address = 0;

    number_of_pmics = pm_target_information_get_count_info((uint32)PM_PROP_PMIC_NUM);

    /* Num of PMICs cannot be 0 */
    CORE_VERIFY(number_of_pmics != 0);

    CORE_VERIFY(number_of_pmics <= PM_MAX_NUM_DEVICES);

    /*  Go through all Comm buses and read peripheral info needed for driver initializations */
    for(device_index = 0; device_index < number_of_pmics; device_index++)
    {
       for(sid_index = 0; sid_index < 2; sid_index++)
        {
           comm_ptr = pmiC_CommManager_GetComm(comm_manager, device_index, sid_index);

           if(comm_ptr != NULL)
            {
               // Go through each of the 256 base addresses and get peripheral info
               for(peripheral_index = 0; peripheral_index < 256; peripheral_index++)
                {
                   peripheral_info.base_address = peripheral_index * 0x0100;
                    //read revision information here
                   if(pm_processor_resolve_peripheral_version(comm_ptr, &peripheral_info) == PM_ERR_FLAG__SUCCESS)
                    {
                       pm_resource_manager_create_pmic_resource(comm_ptr, &peripheral_info);
                    }
                }
            }
        }
    }
}

void pm_resource_manager_create_pmic_resource(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
    switch((pm_hw_module_type)peripheral_info->peripheral_type)
	{
        case PM_HW_MODULE_FTS:
        case PM_HW_MODULE_HF_BUCK:
        case PM_HW_MODULE_ULT_BUCK:
            switch((pm_hw_module_ult_buck_subtype)peripheral_info->peripheral_subtype)
            {
                case PM_HW_MODULE_ULT_BUCK_CTL1:
                case PM_HW_MODULE_ULT_BUCK_CTL4:
                    pm_smps_driver_init(comm_ptr, peripheral_info);
                    break;
                default:
                    break;
            }
            break;
        case PM_HW_MODULE_LDO:
        case PM_HW_MODULE_ULT_LDO:
            pm_ldo_driver_init(comm_ptr, peripheral_info);
		    break;
        case PM_HW_MODULE_VS:
            pm_vs_driver_init(comm_ptr, peripheral_info);
            break;
        case PM_HW_MODULE_BOOST:
            pm_boost_driver_init(comm_ptr, peripheral_info);
            break;
        case PM_HW_MODULE_CLOCK:
            pm_clk_driver_init(comm_ptr, peripheral_info);
            break;
        case PM_HW_MODULE_RTC:
            pm_rtc_driver_init(comm_ptr, peripheral_info);
            break;
        case PM_HW_MODULE_PON:
            pm_pon_driver_init(comm_ptr);
            break;
        default:
            break;
    }
}

