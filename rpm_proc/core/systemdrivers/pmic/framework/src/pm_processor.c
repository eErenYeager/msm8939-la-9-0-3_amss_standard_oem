/*! \file pm_processor.c
 *
 *  \brief This file contains processor specific initialization functions.
 *    This file contains code for Target specific settings and modes.
 *
 *  &copy; Copyright 2010-2013 QUALCOMM Technologies Incorporated, All Rights Reserved
 */

/*===========================================================================

EDIT HISTORY FOR MODULE

This document is created by a code generator, therefore this section will
not contain comments describing changes made to the module.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/framework/src/pm_processor.c#1 $

when       who     what, where, why
--------   ---     ------------------------------------------------
09/10/13   rk      Code refactoring.
02/27/13   hs      Code refactoring.

===========================================================================*/

/*===========================================================================

INCLUDE FILES

===========================================================================*/
#include "pm_processor.h"
#include "pm_resource_manager.h"
#include "pm_target_information.h"
#include "spmi_1_0_lite_comm.h"
#include "comm_manager.h"
#include "comm_type.h"
#include "hw_module_type.h"
#include "CoreVerify.h"


#ifdef PMIC_OFFTARGET_TESTING
#include "shadow_register_only.h"
#ifdef BAT_BOARD_USB
#include "batboardbase.h"
#endif // BAT_BOARD_USB
#endif // PMIC_OFFTARGET_TESTING

/* PMIC Device Info */
static pmiC_DeviceInfo device_info[PM_MAX_NUM_DEVICES];

/* PMIC Comm Info, Each PMIC device has 2 slave ids */
static pmiC_CommInfo comm_info[PM_MAX_NUM_DEVICES * 2];

static uint32 number_of_pmics = 0;


/*! \brief Internal function. This function initialized the Comm.
 *  \param[in] None.
 *
 *  \return None.
 *
 *  <b>Dependencies</b>
 *  \li pmiC_CommManager_CreateSingleton() .
 */
static void pm_processor_initialize_comms(void);

void pm_processor_init()
{
    number_of_pmics = pm_target_information_get_count_info((uint32)PM_PROP_PMIC_NUM);

    /* Num of PMICs cannot be 0 */
    CORE_VERIFY(number_of_pmics != 0);

    pmiC_CommManager_CreateSingleton(number_of_pmics);

    // Initialize Comms. These may already be initialized if this is an off-target test.
    if(!pmiC_CommManager_GetSingleton()->IsInitialized)
    {
        pm_processor_initialize_comms();
    }
}

void pm_processor_initialize_comms( )
{
    pmiC_CommManager *comm_manager = pmiC_CommManager_GetSingleton();

    /* Num of PMICs cannot be greater than MAX value */
    CORE_VERIFY(number_of_pmics <= PM_MAX_NUM_DEVICES);

    /* Each PMIC device has 2 slave ids */

    /* Primary PMIC Comm Initialization */
    pmiC_DeviceInfo_Init(&device_info[0], QcPmicDevice, PmDeviceIndex1);

    /* Primary PMIC Comm Initialization for slave id index 0 */
    pmiC_CommInfo_Init(&comm_info[0], &device_info[0], SPMI_1_0Type, 0);
    pmiC_CommManager_AddCommInterface(comm_manager, &comm_info[0]);

    /* Primary PMIC Comm Initialization for slave id index 1 */
    pmiC_CommInfo_Init(&comm_info[1], &device_info[0], SPMI_1_0Type, 1);
    pmiC_CommManager_AddCommInterface(comm_manager, &comm_info[1]);

    if ( number_of_pmics > 1 )
    {
        /* Secondary PMIC Comm Initialization */
        pmiC_DeviceInfo_Init(&device_info[1], QcPmicDevice, PmDeviceIndex2);

        /* Secondary PMIC Comm Initialization for slave id index 0 */
        pmiC_CommInfo_Init(&comm_info[2], &device_info[1], SPMI_1_0Type, 0);
        pmiC_CommManager_AddCommInterface(comm_manager, &comm_info[2]);

        /* Secondary PMIC Comm Initialization for slave id index 1 */
        pmiC_CommInfo_Init(&comm_info[3], &device_info[1], SPMI_1_0Type, 1);
        pmiC_CommManager_AddCommInterface(comm_manager, &comm_info[3]);
    }

    comm_manager->IsInitialized = TRUE;
}

pm_err_flag_type pm_processor_create_comm_channel(pmiC_CommInfo *comm_info, pmiC_IComm **current_comm, pmiC_IComm **new_comm)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    CommType comm_type = comm_info->mCommType;
#ifdef PMIC_OFFTARGET_TESTING
#ifdef BAT_BOARD_USB
    int status = 0;
#endif // BAT_BOARD_USB
#endif // PMIC_OFFTARGET_TESTING

    switch(comm_type)
    {
        case SPMI_1_0Type:
        {
            pm_malloc(sizeof(pmiC_SpmiLiteCommV1), (void**)new_comm);
            pmiC_SpmiLiteCommV1_Init( (pmiC_SpmiLiteCommV1*)*new_comm, current_comm, comm_info->mBusIndex);
            break;
        }
#ifdef PMIC_OFFTARGET_TESTING
#ifdef BAT_BOARD_USB
        case BatBoardBaseType:
        {
            // The code should always go into this if statement beccause the
            // ShadowRegisterOnlyType is used for testing and the Register Buffer Size needs
            // to be initialized by the test driver.
            //*new_comm = CreateInstance_BatBoardComm_BBC(&status);
            if(comm_info->mDeviceInfo->mMaxRegisterSize > 0)
            {
                pm_malloc(sizeof(pmiC_BatBoardBase), (void**)new_comm);
                pmiC_BatBoardBase_Init(*new_comm, current_comm, comm_info->mDeviceInfo->mMaxRegisterSize, &status, FALSE, NULL, NULL, NULL);
            }
            break ;
        }
#endif // BAT_BOARD_USB
        case ShadowRegisterOnlyType:
        {
            // The code should always go into this if statement because the
            // ShadowRegisterOnlyType is used for testing and the Register Buffer Size needs
            // to be initialized by the test driver.
            if(comm_info->mDeviceInfo->mMaxRegisterSize > 0)
            {
                pm_malloc(sizeof(pmiC_ShadowRegisterOnly), (void**)new_comm);
                pmiC_ShadowRegisterOnly_Init( (pmiC_ShadowRegisterOnly*)*new_comm, current_comm, comm_info, NULL, NULL);
            }
            break ;
        }
#endif // PMIC_OFFTARGET_TESTING
        default :
        {
            err_flag = PM_ERR_FLAG__COMM_TYPE_NOT_RECOGNIZED;
            break;
        }
    } /* switch */

    return err_flag;
}

pm_err_flag_type pm_processor_resolve_peripheral_version(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
    pm_err_flag_type err = PM_ERR_FLAG__SUCCESS;

    switch(comm_ptr->mpCommInfo->mDeviceInfo->mDeviceType)
    {
        case QcPmicDevice:
        {
            err = pm_target_information_read_peripheral_rev(comm_ptr, peripheral_info);
            break;
        }
        default:
        {
            err = PM_ERR_FLAG__DEV_MISMATCH;
        }
    }

    return err;
}

