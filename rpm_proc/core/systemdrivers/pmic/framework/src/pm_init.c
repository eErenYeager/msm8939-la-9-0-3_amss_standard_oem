/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

P M    RPM    P R O C

GENERAL DESCRIPTION
This file contains initialization functions for NPA Device layer

EXTERNALIZED FUNCTIONS
None.

INITIALIZATION AND SEQUENCING REQUIREMENTS
None.

Copyright (c) 2010-2011           by Qualcomm Technologies, Inc.  All Rights Reserved.
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================

EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/framework/src/pm_init.c#1 $  

when       who     what, where, why
--------   ---     ---------------------------------------------------------- 
01/02/14   rk      Remove RPM INIT Client
09/10/13   rk      Fixed the naming convention in \config.
09/10/13   rk      Code refactoring.
02/27/13   hs      Code refactoring.
05/10/11   jtn     Fix RPM init bug for 8960
07/01/10   umr     Created.
===========================================================================*/
#include "pm_processor.h"
#include "pm_target_information.h"
#include "pm_rpm_npa.h"
#include "pmapp_npa.h"
#include "pm_resource_manager.h"
#include "pm_rpm_proc_npa_device_cfg.h"

npa_client_handle handle_rpm_init = NULL;

#ifdef PM_IMAGE_RPM_PROC
void pm_init( void )
#else
void pm_rpm_init( void )
#endif
{
    pm_version_detect();

    pm_target_information_init();

    pm_processor_init();

    pm_resource_manager_init();

    pm_rpm_proc_npa_init ();

    pm_rpm_railway_init();

    pm_rpm_platform_init();
    //This function may be no-op on targets that do not support
    //sleep yet
    pm_rpm_sleep_init();

    return ;
}
