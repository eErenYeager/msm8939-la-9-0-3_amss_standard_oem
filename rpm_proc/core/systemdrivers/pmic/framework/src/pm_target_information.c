/*! \file
*  
*  \brief  target_initialize_app_settings.cpp ----This file contains target specific PMIC settings.
*  \details This file contains target specific PMIC settings common across processors.
*           the target.
*  
*   
*  &copy; Copyright 2012 Qualcomm Technologies Incorporated, All Rights Reserved
*/

/*===========================================================================

EDIT HISTORY FOR MODULE

$Header: //components/rel/rpm.bf/2.0.c9/core/systemdrivers/pmic/framework/src/pm_target_information.c#1 $ 

when       who     	what, where, why
--------   ---    	----------------------------------------------------------
11/25/13    rk          Enable pmic in RPM  framework 
10/09/13   rk           renaming the ult buck range and volt struct names.
09/10/13    rk          Adding settling time for regulators.
09/10/13    rk         Remove range related hacks
09/10/13    rk         Clk driver input check (do not disable critical clocks)
09/10/13    rk          Added Peripheral Probe fix.
09/10/13    rk         Clk driver code re-architecture
09/10/13    rk          Fixed the naming convention in \config.
09/10/13    rk          Added workaround for 8110 L1.
09/10/13    rk          Code refactoring.
09/03/12    rk          Removed VS and BOOST related code.
02/27/13    hs          Code refactoring.
08/07/12    hs          Added support for 5V boost.
04/16/12    hs          Removed irq files.
04/03/10    wra	        Creating file for MSM8960
========================================================================== */

/*===========================================================================

INCLUDE FILES 

===========================================================================*/
#include "customer.h"                  /* FEATURE definitions    */
#include "pm_target_information.h"
#include "DALSys.h"
#include "hw_module_type.h"
#include "pm_version.h"
#include "comm_manager.h"
#include "pm_rpm_npa_device.h"

#include "pm_smps.h"
#include "pm_ldo.h"
#include "pm_vs.h"

#include "Chipinfo.h"
#include "image_layout.h"

#include "pm_clk_driver.h"
#include "pm_rpm_ldo_trans_apply.h"
#include "pm_rpm_smps_trans_apply.h"
#include "pm_rpm_vs_trans_apply.h"
#include "pm_rpm_boost_trans_apply.h"
#include "pm_rpm_clk_buffer_trans_apply.h"


static DALSYS_PROPERTY_HANDLE_DECLARE(hProp_common);
static DALSYS_PROPERTY_HANDLE_DECLARE(hProp_target);
static DALSYSPropertyVar prop_common;                /* Union to retreive property values */
static DALSYSPropertyVar prop_target;                /* Union to retreive property values */
pm_pwr_resource_info_type pamInfo ;


void pm_target_information_init(void)
{
    pm_model_type pm_chip_model_id_1st = pm_get_pmic_model(0);
    pm_model_type pm_chip_model_id_2nd = pm_get_pmic_model(1);

    DALSYS_GetDALPropertyHandleStr("/rpm/pmic/target",hProp_target);

    DALSYS_GetDALPropertyHandleStr("/rpm/pmic/common",hProp_common);
}


pm_err_flag_type pm_target_information_read_peripheral_rev(pmiC_IComm *comm_ptr, peripheral_info_type *peripheral_info)
{
    pm_err_flag_type err_flag = PM_ERR_FLAG__SUCCESS;
    pm_register_address_type base_address = (pm_register_address_type)peripheral_info->base_address;
    uint32 digital_minor_index = 0;
    uint32 digital_major_index = 1;
    uint32 analog_minor_index = 2;
    uint32 analog_major_index = 3;
    uint32 peripheral_type_index = 4;
    uint32 peripheral_subtype_index = 5;
    uint32 peripheral_dummy_index = 6;
    const uint32 num_of_bytes = 7;
    pm_register_data_type temp_peripheral_info[7] = {0};

    if(!comm_ptr)
    {
        return PM_ERR_FLAG__COMM_TYPE_NOT_RECOGNIZED;
    }

    // Reset the peripheral info
    peripheral_info->peripheral_type = 0;
    peripheral_info->peripheral_subtype = 0;
    peripheral_info->analog_major_version = 0;
    peripheral_info->analog_minor_version = 0;
    peripheral_info->digital_major_version = 0;
    peripheral_info->digital_minor_version = 0;

    /* Burst read the peripheral info */
    comm_ptr->ReadByteArray(comm_ptr, base_address, num_of_bytes, temp_peripheral_info, 0);

    /* When we burst read multiple bytes from a non-existent peripheral, the data returned
       should be same on all the bytes so we need to do the below check and return error */
    if(temp_peripheral_info[peripheral_subtype_index] == temp_peripheral_info[peripheral_dummy_index]) 
    {
        return PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }

    /* Valid peripheral type can never be 0 */   
    if(temp_peripheral_info[peripheral_type_index] == 0) 
    {
        return PM_ERR_FLAG__INVALID_RESOURCE_ACCESS_ATTEMPTED;
    }
    
    peripheral_info->peripheral_type = temp_peripheral_info[peripheral_type_index];    
    peripheral_info->peripheral_subtype = temp_peripheral_info[peripheral_subtype_index];
    peripheral_info->digital_major_version = temp_peripheral_info[digital_major_index];
    peripheral_info->digital_minor_version = temp_peripheral_info[digital_minor_index];
    peripheral_info->analog_major_version = temp_peripheral_info[analog_major_index];
    peripheral_info->analog_minor_version = temp_peripheral_info[analog_minor_index];
    
    return err_flag;
}

void* pm_target_information_get_common_info(uint32 prop_id)
{
  void *info = NULL;

  if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp_common, NULL, prop_id, &prop_common))
  {
      info = (void*)prop_common.Val.pStruct;
  }

  return info;
}

void* pm_target_information_get_specific_info(uint32 prop_id)
{
  void *info = NULL;

  if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp_target, NULL, prop_id, &prop_target))
  {
      info = (void*)prop_target.Val.pStruct;
  }

  return info;
}

uint32 pm_target_information_get_count_info(uint32 prop_id)
{
  uint32 count = 0;;

  if(DAL_SUCCESS == DALSYS_GetPropertyValue(hProp_target, NULL, prop_id, &prop_target))
  {
      count = (uint8)prop_target.Val.dwVal;
  }

  return count;
}

