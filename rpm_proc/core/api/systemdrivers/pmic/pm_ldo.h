#ifndef PM_LDO__H
#define PM_LDO__H

/*! \file pm_ldo.h
 *  \n
 *  \brief This header file contains enums and API definitions for PMIC LDO
 *         power rail driver.
 *  \n
 *  \n &copy; Copyright 2009-2014 QUALCOMM Technologies Incorporated, All Rights Reserved
 */
/* ======================================================================= */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/2.0.c9/core/api/systemdrivers/pmic/pm_ldo.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
02/21/14   sv      Added generic api to obtain vset value for regulators. (CR-549436)
09/03/12   rk      Remove unused LDO and pin control functionality.
12/06/12   hw      Rearchitecturing module driver to peripheral driver
=============================================================================*/

/*===========================================================================

                 HEADER FILE INCLUDE

===========================================================================*/
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/

/*!
 *  \brief 
 *  LDO peripheral index. This enum type contains all ldo regulators that you may need. 
 */
enum
{
  PM_LDO_1,
  PM_LDO_2,
  PM_LDO_3,
  PM_LDO_4,
  PM_LDO_5,
  PM_LDO_6,
  PM_LDO_7,
  PM_LDO_8,
  PM_LDO_9,
  PM_LDO_10,
  PM_LDO_11,
  PM_LDO_12,
  PM_LDO_13,
  PM_LDO_14,
  PM_LDO_15,
  PM_LDO_16,
  PM_LDO_17,
  PM_LDO_18,
  PM_LDO_19,
  PM_LDO_20,
  PM_LDO_21,
  PM_LDO_22,
  PM_LDO_23,
  PM_LDO_24,
  PM_LDO_25,
  PM_LDO_26,
  PM_LDO_27,
  PM_LDO_28,
  PM_LDO_29,
  PM_LDO_30,
  PM_LDO_31,
  PM_LDO_32,
  PM_LDO_33,
  PM_LDO_INVALID
};

/*===========================================================================

                        API PROTOTYPE

===========================================================================*/

/**
 * @name 
 *     pm_ldo_sw_mode
 *
 * @description
 *     Switch between NPM, LPM, and other modes of a regulator.
 * 
 * @param 
 *     sw_mode - Select the different mode of a regulator. Example: NPM, LPM, BYPASS
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_sw_mode
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_sw_mode_type sw_mode);

/**
 * @name 
 *     pm_ldo_sw_enable
 *
 * @description
 *     enable or disable a regulator or voltage switcher.
 * 
 * @param 
 *     enableDisableCmd - turn on and off the regulator.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_sw_enable
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type on_off);

/**
 * @name 
 *     pm_ldo_sw_enable_status
 *
 * @description
 *     returns the software enable status of LDOs.
 * 
 * @param 
 *     on_off - pointer to the on/off status of the regulator.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_sw_enable_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type* on_off);

/**
 * @name 
 *     pm_ldo_vreg_ok_status
 *
 * @description
 *     returns the VREG_OK status of LDOs.
 * 
 * @param 
 *     on_off - pointer to return the VREG_OK status.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_vreg_ok_status
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_on_off_type* on_off);

/**
 * @name 
 *     pm_ldo_calculate_vset
 *
 * @description
 *    get vset for LDO.
 * 
 * @param 
 *     input volt_level - voltage level  .
       output  vset  - deduced voltage register value using
 *     input volt_level
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_ldo_calculate_vset
(uint8 pmic_chip, uint8 ldo_peripheral_index, pm_volt_level_type volt_level, uint32* vset); 

#endif /* PM_LDO__H */

