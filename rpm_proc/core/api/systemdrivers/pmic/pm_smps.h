#ifndef PM_SMPS__H
#define PM_SMPS__H
/*! \file 
 *  \n
 *  \brief
 *  \n
 *  \n This header file contains enums and API definitions for PMIC voltage
 *  regulator driver.
 *  \n
 *  \n &copy; Copyright 2012 QUALCOMM Technologies Incorporated, All Rights Reserved
 */
/* ======================================================================= */

/* =======================================================================
                             Edit History
  This section contains comments describing changes made to this file.
  Notice that changes are listed in reverse chronological order.

  $Header: //components/rel/rpm.bf/2.0.c9/core/api/systemdrivers/pmic/pm_smps.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
05/07/14   rk      PMIC API to read the volt level of SMPS
09/03/12   rk      Remove unused SMPS and pin control and Quiet Mode functionality.
02/04/13   hs      pm_smps_sw_mode_status_lite.
12/06/12   hw      Rearchitecturing module driver to peripheral driver
=============================================================================*/
/*===========================================================================

                        HEADER FILES

===========================================================================*/

#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "com_dtypes.h"

/*===========================================================================

                        TYPE DEFINITIONS 

===========================================================================*/
/* SMPS peripheral index. This enum type contains all ldo regulators that you may need. */
enum
{
  PM_SMPS_1,
  PM_SMPS_2,
  PM_SMPS_3,
  PM_SMPS_4,
  PM_SMPS_5,    
  PM_SMPS_6,
  PM_SMPS_7,
  PM_SMPS_8,
  PM_SMPS_9,
  PM_SMPS_10,  
  PM_SMPS_INVALID
};

typedef enum
{
  PM_INDUCTOR_ILIM__1000MA  = 0,
  PM_INDUCTOR_ILIM__1500MA  = 0x01,
  PM_INDUCTOR_ILIM__2000MA  = 0x02,
  PM_INDUCTOR_ILIM__2500MA  = 0x03,
  PM_INDUCTOR_ILIM__3000MA  = 0x04,
  PM_INDUCTOR_ILIM__3500MA  = 0x05,
  PM_INDUCTOR_ILIM__4000MA  = 0x06,
  PM_INDUCTOR_ILIM__4500MA  = 0x07,
  PM_INDUCTOR_ILIM__INVALID
} pm_smps_inductor_ilim_type ;

typedef enum 
{
   PM_CLK_TCXO,
   PM_CLK_RC,
   PM_CLK_SOURCE_INVALID
}pm_clk_src_type;

/*
 * enumeration assumes input clock freq of 19.2MHz
 * clock frequency = (input clock freq) / ((CLK_PREDIV+1)(CLK_DIV + 1))
 * this enumeration is 5 bit long
 */
typedef enum 
{
    PM_CLK_19p2_MHz     = 0,
    PM_CLK_9p6_MHz      = 1,
    PM_CLK_6p4_MHz      = 2,
    PM_CLK_4p8_MHz      = 3,
    PM_CLK_3p84_MHz     = 4,
    PM_CLK_3p2_MHz      = 5,
    PM_CLK_2p74_MHz     = 6,
    PM_CLK_2p4_MHz      = 7,
    PM_CLK_2p13_MHz     = 8,
    PM_CLK_1p92_MHz     = 9,
    PM_CLK_1p75_MHz     = 10,
    PM_CLK_1p6_MHz      = 11,
    PM_CLK_1p48_MHz     = 12,
    PM_CLK_1p37_MHz     = 13,
    PM_CLK_1p28_MHz     = 14,
    PM_CLK_1p2_MHz      = 15,
    PM_CLK_1p13_MHz     = 16,
    PM_CLK_1p07_MHz     = 17,
    PM_CLK_1p01_MHz     = 18,
    PM_CLK_960_KHz      = 19,
    PM_CLK_914_KHz      = 20,
    PM_CLK_873_KHz      = 21,
    PM_CLK_835_KHz      = 22,
    PM_CLK_800_KHz      = 23,
    PM_CLK_768_KHz      = 24,
    PM_CLK_738_KHz      = 25,
    PM_CLK_711_KHz      = 26,
    PM_CLK_686_KHz      = 27,
    PM_CLK_662_KHz      = 28,
    PM_CLK_640_KHz      = 29,
    PM_CLK_619_KHz      = 30,
    PM_CLK_600_KHz      = 31,
    PM_SWITCHING_FREQ_INVALID,
	PM_SWITCHING_FREQ_FREQ_NONE
}pm_smps_switching_freq_type;
/* Quiet Mode */
typedef enum
{
    PM_QUIET_MODE__DISABLE, // default
    PM_QUIET_MODE__QUIET,
    PM_QUIET_MODE__SUPER_QUIET,
    PM_QUIET_MODE__INVALID
}pm_quiet_mode_type;
/*===========================================================================

                        API PROTOTYPE

===========================================================================*/
/**
 * @name 
 *     pm_smps_pull_down
 *
 * @description
 *     Allows you to enable disable active pulldown.
 * 
 * @param 
 *     on_off - turn on and off active pulldown
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_pull_down
(uint8 pmic_chip, uint8 smps_peripheral_index, pm_on_off_type on_off);

/**
 * @name 
 *     pm_smps_sw_mode
 *
 * @description
 *     Switch between HPM, LPM, and other modes of a regulator.
 * 
 * @param 
 *     sw_mode - Select the different mode of a regulator. Example, HPM, LPM
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_sw_mode
(uint8 pmic_chip, uint8 smps_peripheral_index, pm_sw_mode_type sw_mode);

/**
 * @name 
 *     pm_smps_sw_mode_status
 *
 * @description
 *     obtain the status of a mode ( HPM, LPM etc ) at an instance.
 *     Note, the mode of a regulator changes dyanmically.
 * 
 * @param 
 *     mode - Select the different mode of a regulator. Example, HPM, LPM
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_sw_mode_status
(uint8 pmic_chip, uint8 smps_peripheral_index, pm_sw_mode_type* sw_mode);

/**
 * @name 
 *     pm_smps_volt_level_status
 *
 * @description
 *     obtain the volt level status of any SMPS
 * 
 * @param 
 *     volt_level - pointer to the volt_level variable
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 *
 * Example: pm_smps_volt_level_status (0, 3, volt_level)
 */
pm_err_flag_type pm_smps_volt_level_status(uint8 pmic_chip, uint8 smps_peripheral_index, 
                                        pm_volt_level_type  *volt_level);

/**
 * @name 
 *     pm_smps_sw_mode_status_raw
 *
 * @description
 *     obtain the register value [MODE_CTL] at an
 *     instance. Note, the mode of a regulator changes
 *     dyanmically.
 * 
 * @param 
 *     mode - obtain the mode_ctl register value of a regulator.
 *     Example, NPM, AUTO
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_sw_mode_status_raw
(uint8 pmic_chip, uint8 smps_peripheral_index, pm_sw_mode_type* sw_mode);



/**
 * @name 
 *     pm_smps_pin_ctrled
 *
 * @description
 *     select the pin ( connected to external signal ) that you would like to use
 *     to effect the state ( on/off ) and mode ( HPM, LPM etc ) of a regulator
 * 
 * @param 
 *     pinCtrl - Select a pin
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_pin_ctrled
(uint8 pmic_chip, uint8 smps_peripheral_index, uint8 select_pin);

/**
 * @name 
 *     pm_smps_volt_level
 *
 * @description
 *     set the voltage of a regulator.
 *     Note, differnt type ( LDO, HFS etc ) may have different programmable voltage steps.
 *     Only support the correct programmable steps. Not rounding voltages if the voltage selected
 *     is not part of the programmable steps.
 * 
 * @param 
 *     voltLevel - select the voltage.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_volt_level
(uint8 pmic_chip, uint8 smps_peripheral_index, pm_volt_level_type volt_level);

/**
 * @name 
 *     pm_smps_sw_enable
 *
 * @description
 *     enable or disable a regulator or voltage switcher.
 * 
 * @param 
 *     enableDisableCmd - turn on and off the regulator.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_sw_enable
(uint8 pmic_chip, uint8  smps_peripheral_index, pm_on_off_type  on_off);

/**
 * @name 
 *     pm_smps_clk_source
 *
 * @description
 *    select a desire clock source for SMPS.
 * 
 * @param 
 *     clk_source - There is TCXO, RC etc.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_clk_source
(uint8 pmic_chip, pm_clk_src_type  clk_source);

/**
 * @name 
 *     pm_smps_switching_freq
 *
 * @description
 *    select a desire frequency for the SMPS. Each SMPS can operate under different frequency.
 * 
 * @param 
 *     switching_freq - Select frequency such as 19.2MHz, 3.2MHz etc.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_switching_freq
(uint8 pmic_chip, uint8 smps_peripheral_index, pm_smps_switching_freq_type switching_freq);

/**
 * @name 
 *     pm_smps_inductor_ilim
 *
 * @description
 *    select the current limit for the inductor of a selected SMPS.
 * 
 * @param 
 *     ilim_level - Select current limit level such as 300mA, 1500mA or 3100mA.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_inductor_ilim 
(uint8 pmic_chip, uint8 smps_peripheral_index, pm_smps_inductor_ilim_type ilim_level);

/**
 * @name 
 *     pm_smps_inductor_ilim
 *
 * @description
 *    obtain the current limit for the inductor of a selected SMPS by reading the SPMI register.
 * 
 * @param 
 *     *ilim_level - Provides the current limit with register read inside the API.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_inductor_ilim_status
( uint8 pmic_chip, uint8 smps_peripheral_index, pm_smps_inductor_ilim_type* ilim_level );

/**
 * @name 
 *     pm_smps_quiet_mode
 *
 * @description
 *    Set quiet mode for SMPS.
 * 
 * @param 
 *     quiet_mode - Enable/Disable quiet mode as well as selecting different types of quiet mode.
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_quiet_mode
(uint8 pmic_chip, uint8 smps_peripheral_index, pm_quiet_mode_type quiet_mode, uint16 voltage_mv);
/**
 * @name 
 *     pm_smps_calculate_vset
 *
 * @description
 *    get vset for SMPS.
 * 
 * @param 
 *     input volt_level - voltage level  .
 *     output  vset  - deduced voltage register value using
 *     input volt_level
 * 
 * @return 
 *     error flag type - pm_err_flag_type
 */
pm_err_flag_type pm_smps_calculate_vset
(uint8 pmic_chip, uint8 smps_peripheral_index, pm_volt_level_type  volt_level , 
 uint32* vset) ;


#endif /* PM_SMPS__H */

