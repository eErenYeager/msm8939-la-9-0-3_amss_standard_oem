#ifndef __PM_MPM_H__
#define __PM_MPM_H__

/**
 * @file pm_mpm.h
 *
 * Header file for PMIC MPM VDD command driver.
 */
/*
  ====================================================================

  Copyright (c) 2013 Qualcomm Technologies Incorporated.  All Rights Reserved.
  QUALCOMM Proprietary and Confidential.

  ====================================================================

  $Header: //components/rel/rpm.bf/2.0.c9/core/api/systemdrivers/pmic/pm_mpm.h#1 $
  $DateTime: 2015/03/13 04:27:04 $
  $Author: pwbldsvc $

  ====================================================================
*/

/*----------------------------------------------------------------------------
 *  Include Files
 * -------------------------------------------------------------------------*/

#include "com_dtypes.h"
#include "pm_err_flags.h"

/*----------------------------------------------------------------------------
 * Type Definitions
 * -------------------------------------------------------------------------*/

/**
 * MPM command configuration.
 */
typedef struct
{
  uint32 slave_id;    /* Slave ID */
  uint32 slave_addr;  /* SPMI address  */
  uint32 data;        /* Data */
  uint32 delay;       /* Delay */
} pm_mpm_cfg_type;

/*----------------------------------------------------------------------------
 * Function : pm_mpm_cmd_config
 * -------------------------------------------------------------------------*/
/*!
    Description: Return a pointer to a config data buffer for MPM voltage rail
                 command.
    @param
      sleep_cfg_ptr: the destination buffer which receives
                     the config data for retention voltage
      active_cfg_ptr: the destination buffer which receives
                      the config data for active voltage
    @return
    PM_ERR_FLAG__SUCCESS success otherwise PMIC error.

    @dependencies
    railway_get_corner_voltage()
    pm_railway_calculate_vset()

    @sa None
*/
pm_err_flag_type pm_mpm_cmd_config
(
  pm_mpm_cfg_type ** sleep_cfg_ptr,
  pm_mpm_cfg_type ** active_cfg_ptr
);

#endif /* __PM_MPM_H__ */

