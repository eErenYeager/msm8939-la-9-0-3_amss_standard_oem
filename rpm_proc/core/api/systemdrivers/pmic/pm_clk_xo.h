#ifndef PM_CLK_XO__H
#define PM_CLK_XO__H

/*! \file
*  \n
*  \brief  pm_clk_xo.h PMIC XO CORE RELATED DECLARATION 
*  \details  This header file contains functions and variable declarations 
*  to support Qualcomm PMIC XO CORE module. 
*  \n &copy; Copyright 2013 QUALCOMM Technologies Incorporated, All Rights Reserved
*/

/* =======================================================================
                                Edit History
This section contains comments describing changes made to this file.
Notice that changes are listed in reverse chronological order.

$Header: //components/rel/rpm.bf/2.0.c9/core/api/systemdrivers/pmic/pm_clk_xo.h#1 $

when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/29/13   kt      Initial version. 
========================================================================== */
#include "com_dtypes.h"
#include "pm_err_flags.h"
#include "pm_resources_and_types.h"
#include "pm_clk.h"

/*===========================================================================

                 API PROTOTYPES

===========================================================================*/
/**
 * @brief Configures the TCXO Warm-up delay time. The delay time
 *        is the number of sleep (32KHz) clock cycles to wait
 *        for warm up time of the XO clock.
 * @param pmic_chip: Selects the device in which the buffers 
 *                   being controlled are located. Device index
 *                   starts with zero
 * @param clk_type: Selects which clock type to use. Refer to 
 *                  pm_clk.h for further info on this enum
 * @param timer_value: Number of sleep (32KHz) clock cycles to wait 
 *                     Value cannot be greater than 2047
 *
 * @note Usage Example: 
 *             pm_clk_xo_set_warmup_time(0, PM_CLK_XO, 1000);
 * 
 * @return pm_err_flag_type PM_ERR_FLAG__SUCCESS if successful
 */
pm_err_flag_type pm_clk_xo_set_warmup_time(uint8 pmic_chip, pm_clk_type clk_type, uint32 timer_value);

#endif /* PM_CLK_XO__H */
