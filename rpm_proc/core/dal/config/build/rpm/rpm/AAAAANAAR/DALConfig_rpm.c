#include "DALStdDef.h" 
#include "DALSysTypes.h" 
#include "dalconfig.h" 

#ifndef DAL_CONFIG_IMAGE_RPM 
#define DAL_CONFIG_IMAGE_RPM 
#endif 
extern void * icb_rpm_system;
extern void * SourceConfig;
extern void * RPMClockConfig;
extern void * SystemNOCClockConfig;
extern void * SystemMMNOCClockConfig;
extern void * PCNOClockConfig;
extern void * BIMCClockConfig;
extern void * BIMCGPUClockConfig;
extern void * APSSTCUASYNCClockConfig;
extern void * APSSAXIClockConfig;
extern void * Q6TBUClockConfig;
extern void * QDSSATClockConfig;
extern void * QDSSTraceClockConfig;
extern void * QDSSSTMClockConfig;
extern void * QDSSTSCTRDiv2ClockConfig;
extern void * RBCPRClockConfig;
extern void * SPMIAHBClockConfig;
extern void * SPMISERClockConfig;
extern void * ClockLogDefaultConfig;
extern void * clk_reg;
extern void * fts_volt;
extern void * hfs_volt;
extern void * n600_volt;
extern void * n1200_volt;
extern void * boost_volt;
extern void * fts_range;
extern void * hfs_range;
extern void * boost_range;
extern void * smps_reg;
extern void * ldo_reg;
extern void * nmos_volt;
extern void * ult_nmos_volt;
extern void * pmos_volt;
extern void * ult_pmos_volt;
extern void * nmos_range;
extern void * ult_nmos_range;
extern void * pmos_range;
extern void * ult_pmos_range;
extern void * ult_buck_range_1;
extern void * ult_buck_volt_1;
extern void * ult_buck_range_2;
extern void * ult_buck_volt_2;
extern void * clk_ldo_range;
extern void * clk_ldo_volt;
extern void * ldo_settling_time;
extern void * smps_settling_time;
extern void * clk_sleep_reg;
extern void * clk_xo_reg;
extern void * clk_common;
extern void * mx_rail;
extern void * cx_rail;
extern void * ldo_dep_a;
extern void * smps_dep_a;
extern void * clk_dep_a;
extern void * ldo_rail_a;
extern void * smps_rail_a;
extern void * clk_info_a;

const void * DALPROP_StructPtrs[58] =  {
	&icb_rpm_system,
	&SourceConfig,
	&RPMClockConfig,
	&SystemNOCClockConfig,
	&SystemMMNOCClockConfig,
	&PCNOClockConfig,
	&BIMCClockConfig,
	&BIMCGPUClockConfig,
	&APSSTCUASYNCClockConfig,
	&APSSAXIClockConfig,
	&Q6TBUClockConfig,
	&QDSSATClockConfig,
	&QDSSTraceClockConfig,
	&QDSSSTMClockConfig,
	&QDSSTSCTRDiv2ClockConfig,
	&RBCPRClockConfig,
	&SPMIAHBClockConfig,
	&SPMISERClockConfig,
	&ClockLogDefaultConfig,
	&clk_reg,
	&fts_volt,
	&hfs_volt,
	&n600_volt,
	&n1200_volt,
	&boost_volt,
	&fts_range,
	&hfs_range,
	&boost_range,
	&smps_reg,
	&ldo_reg,
	&nmos_volt,
	&ult_nmos_volt,
	&pmos_volt,
	&ult_pmos_volt,
	&nmos_range,
	&ult_nmos_range,
	&pmos_range,
	&ult_pmos_range,
	&ult_buck_range_1,
	&ult_buck_volt_1,
	&ult_buck_range_2,
	&ult_buck_volt_2,
	&clk_ldo_range,
	&clk_ldo_volt,
	&ldo_settling_time,
	&smps_settling_time,
	&clk_sleep_reg,
	&clk_xo_reg,
	&clk_common,
	&mx_rail,
	&cx_rail,
	&ldo_dep_a,
	&smps_dep_a,
	&clk_dep_a,
	&ldo_rail_a,
	&smps_rail_a,
	&clk_info_a,
	 NULL }; 

const uint32 DALPROP_PropBin[] = {

			0x00000538, 0x00000028, 0x000001dc, 0x000001dc, 0x000001dc, 
			0x00000002, 0x02000145, 0x00000228, 0x0200009b, 0x00000510, 
			0x74737973, 0x70006d65, 0x5f63696d, 0x5f627261, 0x65736162, 
			0x6464615f, 0x776f0072, 0x0072656e, 0x65746e69, 0x70757272, 
			0x6d730074, 0x6e695f64, 0x655f7274, 0x6c62616e, 0x43006465, 
			0x6b636f6c, 0x72756f53, 0x00736563, 0x5f636367, 0x5f6d7072, 
			0x636f7270, 0x6c63665f, 0x6367006b, 0x79735f63, 0x6f6e5f73, 
			0x78615f63, 0x6c635f69, 0x6367006b, 0x79735f63, 0x6d6d5f73, 
			0x636f6e5f, 0x6978615f, 0x6b6c635f, 0x63636700, 0x6e63705f, 
			0x615f636f, 0x635f6268, 0x67006b6c, 0x625f6363, 0x5f636d69, 
			0x006b6c63, 0x5f636367, 0x636d6962, 0x7570675f, 0x6b6c635f, 
			0x63636700, 0x7370615f, 0x63745f73, 0x73615f75, 0x5f636e79, 
			0x006b6c63, 0x5f636367, 0x73737061, 0x6978615f, 0x6b6c635f, 
			0x63636700, 0x73736d5f, 0x5f36715f, 0x636d6962, 0x6978615f, 
			0x6b6c635f, 0x63636700, 0x7364715f, 0x74615f73, 0x6b6c635f, 
			0x63636700, 0x7364715f, 0x72745f73, 0x63656361, 0x6e696b6c, 
			0x6b6c635f, 0x63636700, 0x7364715f, 0x74735f73, 0x6c635f6d, 
			0x6367006b, 0x64715f63, 0x745f7373, 0x72746373, 0x7669645f, 
			0x6c635f32, 0x6367006b, 0x62725f63, 0x5f727063, 0x006b6c63, 
			0x5f636367, 0x696d7073, 0x6268615f, 0x6b6c635f, 0x63636700, 
			0x6d70735f, 0x65735f69, 0x6c635f72, 0x6c43006b, 0x4c6b636f, 
			0x6544676f, 0x6c756166, 0x44007374, 0x55414645, 0x465f544c, 
			0x55514552, 0x59434e45, 0x49545100, 0x5f52454d, 0x425f4341, 
			0x00455341, 0x4d495451, 0x425f5245, 0x00455341, 0x00000012, 
			0x00000000, 0x00000000, 0xff00ff00, 0x00000002, 0x00000007, 
			0x02000000, 0x00000002, 0x0000001a, 0x00000004, 0x00000002, 
			0x00000020, 0x00000030, 0xff00ff00, 0x00000008, 0x0000002a, 
			0x00010106, 0x01000100, 0xff00ff00, 0x00000012, 0x0000003b, 
			0x00000001, 0x00000012, 0x00000048, 0x00000002, 0x00000012, 
			0x0000005a, 0x00000003, 0x00000012, 0x0000006e, 0x00000004, 
			0x00000012, 0x00000085, 0x00000005, 0x00000012, 0x00000097, 
			0x00000006, 0x00000012, 0x000000a4, 0x00000007, 0x00000012, 
			0x000000b5, 0x00000008, 0x00000012, 0x000000cc, 0x00000009, 
			0x00000012, 0x000000dd, 0x0000000a, 0x00000012, 0x000000f5, 
			0x0000000b, 0x00000012, 0x00000105, 0x0000000c, 0x00000012, 
			0x0000011d, 0x0000000d, 0x00000012, 0x0000012e, 0x0000000e, 
			0x00000012, 0x00000146, 0x0000000f, 0x00000012, 0x00000154, 
			0x00000010, 0x00000012, 0x00000165, 0x00000011, 0x00000012, 
			0x00000176, 0x00000012, 0xff00ff00, 0x00000012, 0x00000001, 
			0x00000013, 0x00000012, 0x00000006, 0x00000014, 0x00000012, 
			0x00000007, 0x00000015, 0x00000012, 0x0000000a, 0x00000016, 
			0x00000012, 0x0000000b, 0x00000017, 0x00000012, 0x00000008, 
			0x00000018, 0x00000012, 0x0000000e, 0x00000019, 0x00000012, 
			0x0000000f, 0x0000001a, 0x00000012, 0x00000010, 0x0000001b, 
			0x00000012, 0x00000002, 0x0000001c, 0x00000012, 0x00000003, 
			0x0000001d, 0x00000012, 0x00000009, 0x0000001e, 0x00000012, 
			0x00000029, 0x0000001f, 0x00000012, 0x0000000c, 0x00000020, 
			0x00000012, 0x00000027, 0x00000021, 0x00000012, 0x00000011, 
			0x00000022, 0x00000012, 0x00000028, 0x00000023, 0x00000012, 
			0x00000014, 0x00000024, 0x00000012, 0x00000026, 0x00000025, 
			0x00000012, 0x00000020, 0x00000026, 0x00000012, 0x00000021, 
			0x00000027, 0x00000012, 0x00000022, 0x00000028, 0x00000012, 
			0x00000023, 0x00000029, 0x00000012, 0x00000024, 0x0000002a, 
			0x00000012, 0x00000025, 0x0000002b, 0x00000012, 0x00000016, 
			0x0000002c, 0x00000012, 0x00000017, 0x0000002d, 0x00000012, 
			0x0000001a, 0x0000002e, 0x00000012, 0x0000001b, 0x0000002f, 
			0x00000012, 0x0000001c, 0x00000030, 0xff00ff00, 0x00000002, 
			0x00000065, 0x00000001, 0x00000002, 0x0000006a, 0x00000000, 
			0x00000002, 0x0000007f, 0x00000000, 0x00000002, 0x00000066, 
			0x00000004, 0x00000002, 0x00000068, 0x00000014, 0x00000012, 
			0x0000006c, 0x00000031, 0x00000012, 0x0000006d, 0x00000032, 
			0x00000012, 0x0000006f, 0x00000033, 0x00000012, 0x00000070, 
			0x00000034, 0x00000012, 0x00000073, 0x00000035, 0x00000012, 
			0x00000077, 0x00000036, 0x00000012, 0x00000078, 0x00000037, 
			0x00000012, 0x0000007c, 0x00000038, 0xff00ff00, 0x00000002, 
			0x00000187, 0x0124f800, 0x00000002, 0x00000199, 0x00082000, 
			0x00000002, 0x000001a8, 0x00084000, 0xff00ff00 };



 const StringDevice driver_list[] = {
			{"/dev/icb/rpm",2545934574u, 476, NULL, 0, NULL },
			{"DALDEVICEID_SPMI_DEVICE",3290583706u, 492, NULL, 0, NULL },
			{"/core/mproc/smd",1450287328u, 532, NULL, 0, NULL },
			{"/rpm/pmic/common",2887753651u, 772, NULL, 0, NULL },
			{"/rpm/pmic/target",3536625265u, 1136, NULL, 0, NULL }
};


 const DALProps DALPROP_PropsInfo = {(const byte*)DALPROP_PropBin, DALPROP_StructPtrs , 5, driver_list};
