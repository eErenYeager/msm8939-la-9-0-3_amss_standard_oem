/*=========================================================================
                       QDSS e-fuse check

GENERAL DESCRIPTION 
   The interface to check efuses associated with the QDSS for accessing its 
   trace functionality is implemented. The fuses that are checked include 
   DBGEN, SPIDEN, NIDEN and SPNIDEN. The functionality controlled by the fuses 
   are as follows. DBGEN controls non-secure invasive debugging, SPIDEN controls
   secured invasive debugging, NIDEN controls non-secure non-invasive debugging 
   and SPNIDEN controls secure non-invasive debugging.

   According to the ARM Coresight architecture, in all cases, the fuse 
   setting for invasive debugging overrides settings for non-invasive debugging.
    
 
EXTERNALIZED FUNCTIONS
   qdss_fuse_trace_access
 
 
INITIALIZATION AND SEQUENCING REQUIREMENTS
   Requires System FPB clock to access the efuse registers.
   

      Copyright (c) 2012 by Qualcomm Technologies, Inc.  All Rights Reserved.
==========================================================================*/

/*========================================================================== 
 $Header: //components/rel/rpm.bf/2.0.c9/core/debugtrace/common/src/qdss_fuse_check.c#1 $
==========================================================================*/  



#include "qdss_fuse_check.h"
#include "halhwio_qdss.h"
#include "qdss_fuse_regs.h"

#define TRUE  1
#define FALSE 0

/*
Input: None
   
Description: 
   Checks the efuse associated with QDSS and returns a boolean on whether trace
   access is possible or not regardless of the specific security mode of 
   debugging(NS=0/1). The function checks the following OEM fuses: 
   DAP_SPNIDEN_DISABLE, DAP_SPIDEN_DISABLE, DAP_NIDEN_DISABLE,DAP_DBGEN_DISABLE,
   DAP_DEVICEEN_DISABLE, ALL_DEBUG_DISABLE

Return:
   0 - Trace access not possible
   1 - Trace access is possible
*/


unsigned int qdss_fuse_trace_access(void)
{
   unsigned int status=FALSE;
   
   status =           !(HWIO_INF(OEM_CONFIG1,DAP_SPNIDEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG1,DAP_SPIDEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG1,DAP_NIDEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG1,DAP_DBGEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG1,DAP_DEVICEEN_DISABLE));
   status = status && !(HWIO_INF(OEM_CONFIG1,ALL_DEBUG_DISABLE));
   				
   return status;
}
