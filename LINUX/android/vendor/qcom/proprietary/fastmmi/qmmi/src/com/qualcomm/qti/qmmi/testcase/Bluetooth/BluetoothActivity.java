/*
 * Copyright (c) 2017, Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.qmmi.testcase.Bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.qualcomm.qti.qmmi.R;
import com.qualcomm.qti.qmmi.bean.DeviceInfo;
import com.qualcomm.qti.qmmi.bean.TestCase;
import com.qualcomm.qti.qmmi.framework.BaseActivity;
import com.qualcomm.qti.qmmi.utils.LogUtils;
import com.qualcomm.qti.qmmi.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BluetoothActivity extends BaseActivity {
    LayoutInflater mInflater = null;
    BluetoothAdapter mBluetoothAdapter = null;
    List<DeviceInfo> mDeviceList = new ArrayList<DeviceInfo>();
    Set<BluetoothDevice> bondedDevices;
    private final static int MIN_COUNT = 1;
    IntentFilter filter;
    ListView mListView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.logi("Bluetooth Activity run");
        mInflater = LayoutInflater.from(this);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mListView = (ListView) findViewById(R.id.devices_list);
        mListView.setAdapter(mAdapter);
        //startScanAdapterUpdate();

        filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);

        if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
            LogUtils.logi("BluetoothAdapter state is on, start to scan devices.");
            scanDevice();
        } else {
            LogUtils.logi("BluetoothAdapter state is off, start to turn on.");
            if (mBluetoothAdapter.getState() != BluetoothAdapter.STATE_TURNING_ON) {
                LogUtils.logi("BluetoothAdapter state is STATE_TURNING_ON, start to turn on.");
                mBluetoothAdapter.enable();
            }
        }
    }

    private void scanDevice() {
        updateView(getString(R.string.bluetooth_scan_start));
        setProgressBarIndeterminateVisibility(true);
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
        mBluetoothAdapter.startDiscovery();
    }

    private void cancelScan() {
        setProgressBarIndeterminateVisibility(false);
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
    }

    public void updateAdapter() {
        mAdapter.notifyDataSetChanged();
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            LogUtils.logi("onReceive get intent action:" + action);
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            Short rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                LogUtils.logi("onReceive ACTION_FOUND add device to device list :: device name ::" + device.getName()
                        + "device address" + device.getAddress());
                mDeviceList.add(new DeviceInfo(device.getName(), device.getAddress(), rssi));
                mTestCase.addTestData(device.getName() == null ? device.getAddress() : device.getName(), "address:" + device.getAddress());
                mTestCase.addTestData(device.getName() == null ? device.getAddress() : device.getName(), "rssi:" + rssi);
                updateAdapter();
                if (mDeviceList.size() >= MIN_COUNT) {
                    updateResult(TestCase.STATE_PASS);
                    updateView(getString(R.string.bluetooth_found_device));
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setProgressBarIndeterminateVisibility(false);
                LogUtils.logi("onReceive ACTION_DISCOVERY_FINISHED");
                if (mDeviceList.size() < MIN_COUNT) {
                    updateResult(TestCase.STATE_FAIL);
                    updateView(getString(R.string.bluetooth_scan_null));
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                LogUtils.logi("onReceive ACTION_DISCOVERY_STARTED");
                //startScanAdapterUpdate();
            } else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                LogUtils.logi("onReceive ACTION_STATE_CHANGED");
                if (BluetoothAdapter.STATE_ON == intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0)) {
                    scanDevice();
                    if (BluetoothAdapter.STATE_OFF == intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0))
                        mBluetoothAdapter.enable();
                } else if (BluetoothAdapter.STATE_TURNING_ON == intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, 0)) {
                    updateView(getString(R.string.bluetooth_turning_on));
                    setProgressBarIndeterminateVisibility(true);
                }
            }
        }
    };

    public void updateView(String message) {
        // TODO Auto-generated method stub
        Bundle bundle = new Bundle();
        bundle.putString(Utils.BUNDLE_KEY_CASE_NAME, mTestCase.getName());
        bundle.putString(Utils.BUNDLE_KEY_MESSAGE, message);
        super.updateView(bundle);
    }

    BaseAdapter mAdapter = new BaseAdapter() {
        public Object getItem(int arg0) {
            return null;
        }

        public long getItemId(int arg0) {
            return 0;
        }

        public View getView(int index, View convertView, ViewGroup parent) {
            if (convertView == null){
                convertView = mInflater.inflate(R.layout.bluetooth_item, null);
            }

            ImageView image = (ImageView) convertView.findViewById(R.id.bluetooth_image);
            TextView name = (TextView) convertView.findViewById(R.id.bluetooth_text);
            TextView rssi = (TextView) convertView.findViewById(R.id.bluetooth_rssi);
            name.setText(mDeviceList.get(index).getName() == null ? mDeviceList.get(index).getAddress() : mDeviceList.get(index).getName());
            rssi.setText(Short.toString(mDeviceList.get(index).getRssi()));
            image.setImageResource(android.R.drawable.stat_sys_data_bluetooth);
            return convertView;
        }

        public int getCount() {
            if (mDeviceList != null) {
                return mDeviceList.size();
            } else {
                return 0;
            }
        }
    };

    private void startScanAdapterUpdate() {
        /**
        mDeviceList.clear();
        bondedDevices = mBluetoothAdapter.getBondedDevices();
        for (BluetoothDevice device : bondedDevices) {
            DeviceInfo deviceInfo = new DeviceInfo(device.getName(),
                    device.getAddress());
            mDeviceList.add(deviceInfo);
        }
        updateAdapter();**/
    }

    protected void onResume() {
        super.onResume();
        registerReceiver(mReceiver, filter);
        updateAdapter();
    }

    @Override
    protected void onPause() {

        unregisterReceiver(mReceiver);
        super.onPause();
    }

    @Override
    public void finish() {
        cancelScan();
        super.finish();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.bluetooth_act;
    }
}