/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.qmmi.testcase.Gps;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.GnssStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.qualcomm.qti.qmmi.R;
import com.qualcomm.qti.qmmi.bean.TestCase;
import com.qualcomm.qti.qmmi.framework.BaseService;
import com.qualcomm.qti.qmmi.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

public class GpsService extends BaseService {
    private LocationManager mLocationManager = null;
    private TestCase mTestCase;

    LocationListener mLocationListener = new LocationListener() {

        public void onLocationChanged(Location location) {
            LogUtils.logi("onLocationChanged callback ");
        }

        public void onProviderDisabled(String provider) {
            LogUtils.logi("onProviderDisabled callback ");
        }

        public void onProviderEnabled(String provider) {
            LogUtils.logi("onProviderEnabled callback ");
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            LogUtils.logi("onStatusChanged callback ");
        }
    };


    GnssStatus.Callback gnssStatusCallback = new GnssStatus.Callback() {
        @Override
        public void onSatelliteStatusChanged(GnssStatus status) {
            LogUtils.logi("status satelie count: " + status.getSatelliteCount());
            StringBuffer sb = new StringBuffer();
            sb.append(getApplication().getResources().getString(R.string.get_satelite_count) + status.getSatelliteCount() + "\n");
            for (int i = 0; i < status.getSatelliteCount(); ++i) {
                status.getAzimuthDegrees(i);
                status.getCn0DbHz(i);
                status.getConstellationType(i);
                status.getElevationDegrees(i);
                status.getSvid(i);
                status.hasAlmanacData(i);
                status.hasEphemerisData(i);
                status.usedInFix(i);
                sb.append("Svid(" + status.getSvid(i) + "): " + status.getCn0DbHz(i) + "dB\n");
                LogUtils.logi("status : " + status.getAzimuthDegrees(i) + "," + status.getCn0DbHz(i) + "," + status.getConstellationType(i) + "," + status.getElevationDegrees(i) + "," + status.hasAlmanacData(i));

                mTestCase.addTestData(String.valueOf(status.getSvid(i)), "cn0DbHz:" + status.getCn0DbHz(i));
            }

            if (status.getSatelliteCount() > 0) {
                updateResultForCase(mTestCase.getName(), TestCase.STATE_PASS);
            } else {
                updateResultForCase(mTestCase.getName(),TestCase.STATE_FAIL);
            }

            updateView(mTestCase.getName(), sb.toString());
        }
    };

    @Override
    public void register() {

    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (mLocationManager != null) {
            LogUtils.loge("Get mLocationManager failed ");
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public int stop(TestCase testCase) {

        if (mLocationManager != null) {
            mLocationManager.unregisterGnssStatusCallback(gnssStatusCallback);
            mLocationManager.removeUpdates(mLocationListener);
        }
        return 0;
    }

    @Override
    public int run(TestCase testCase) {
        LogUtils.logi("gps service run");
        mTestCase = testCase;

        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return Service.START_NOT_STICKY;
        }


        if (mLocationManager != null) {
            mLocationManager.registerGnssStatusCallback(gnssStatusCallback);
            Criteria criteria;
            criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(Criteria.POWER_LOW);

            String provider = mLocationManager.getBestProvider(criteria, true);
            if (provider != null) {
                mLocationManager.requestLocationUpdates(provider, 500, 0, mLocationListener);
            }
        }

        return 0;
    }

}
