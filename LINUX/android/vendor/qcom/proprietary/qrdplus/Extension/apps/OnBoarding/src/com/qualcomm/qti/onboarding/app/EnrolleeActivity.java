/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.net.wifi.aware.PeerHandle;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.qualcomm.qti.onboarding.R;
import com.qualcomm.qti.onboarding.dpp.ConnectionConfig;
import com.qualcomm.qti.onboarding.dpp.DppCallback;
import com.qualcomm.qti.onboarding.dpp.DppConfig;
import com.qualcomm.qti.onboarding.dpp.DppManager;
import com.qualcomm.qti.onboarding.dpp.MockDppManager;
import com.qualcomm.qti.onboarding.dpp.WifiManagerProxy;
import com.qualcomm.qti.onboarding.nan.Device;
import com.qualcomm.qti.onboarding.nan.MockNanManager;
import com.qualcomm.qti.onboarding.nan.NanCallback;
import com.qualcomm.qti.onboarding.nan.NanManager;
import com.qualcomm.qti.onboarding.nan.WifiAwareManagerProxy;

public class EnrolleeActivity extends Activity {
    private static final String TAG = "Enrollee";

    private NanManager nanManager;
    private DppManager dppManager;

    private TextView titleView;
    private ProgressBar progressBar;
    private TextView textView;
    private Button positiveButton;
    private Button backButton;

    private Device configurator;
    private String ssid;
    private ConnectionConfig config = new ConnectionConfig();
    private @Nullable BroadcastReceiver receiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrollee);

        titleView = findViewById(R.id.title);
        progressBar = findViewById(R.id.progress);
        textView = findViewById(R.id.ap_name);
        positiveButton = findViewById(R.id.positive);
        backButton = findViewById(R.id.back);

        publish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        final NanCallback nanCallback = new NanCallback() {
            @Nullable
            @Override
            public String onAttach(boolean success) {
                // TODO
                DppConfig config = new DppConfig();
                int id = dppManager.generateBootstrap(config);
                if (id == -1) {
                    Log.e(TAG, "generateBootstrap");
                }
                String uri = dppManager.getUri(id);
                if (uri == null) {
                    Log.e(TAG, "getUri");
                }
                Device device = new Device(Build.MODEL, uri, null);
                return device.toJsonString();
            }

            @Nullable
            @Override
            public String onDiscover(PeerHandle peerHandle, byte[] message) {
                return null;
            }

            @Override
            public void onReceive(PeerHandle peerHandle, byte[] message) {
                configurator = Device.parse(message, null);
                if (configurator != null) {
                    if (startDpp() && nanManager != null) {
                        nanManager.unregister(EnrolleeActivity.this);
                        nanManager = null;
                    }
                }
            }

            @Override
            public void onMessageSent(int messageId, boolean success) {}
        };
        if (MainActivity.mock) {
            dppManager = new MockDppManager(this);
            nanManager = new MockNanManager(this, nanCallback);
        } else {
            dppManager = new WifiManagerProxy(this);
            nanManager = new WifiAwareManagerProxy(nanCallback);
        }
        nanManager.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
    }

    private boolean startDpp() {
        if (dppManager.addBootstrapNan(configurator.uri) == -1) {
            Log.e(TAG, "addBootstrapNan");
            return false;
        }
        dppManager.registerListener(new DppCallback() {
            @Override
            public void onConfReceived(int conf_mask) {
                super.onConfReceived(conf_mask);
                confirm();
            }

            @Override
            public void onConfObjSSID(String ssid) {
                super.onConfObjSSID(ssid);
                EnrolleeActivity.this.ssid = ssid;
            }

            @Override
            public void onConfObjConnector(@NonNull String connector) {
                super.onConfObjConnector(connector);
                config.setConnector(connector);
            }

            @Override
            public void onConfObjCSignKey(@NonNull String key) {
                super.onConfObjCSignKey(key);
                config.setCSign(key);
            }

            @Override
            public void onConfObjNetAccessKey(@NonNull String key) {
                super.onConfObjNetAccessKey(key);
                config.setNetAccessKey(key);
            }
        });
        return dppManager.listen(null, DppConfig.ROLE_ENROLLEE) != -1;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (nanManager != null) {
            nanManager.unregister(this);
            nanManager = null;
        }
    }

    private void publish() {
        titleView.setText(R.string.waiting_to_be_discovered);
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        positiveButton.setVisibility(View.INVISIBLE);
        backButton.setVisibility(View.VISIBLE);
        backButton.setText(android.R.string.cancel);
        backButton.setOnClickListener(view -> finish());
    }

    private void confirm() {
        titleView.setText(String.format(getString(R.string.confirm), configurator.name));
        textView.setVisibility(View.VISIBLE);
        textView.setText(ssid);
        progressBar.setVisibility(View.GONE);
        positiveButton.setText(R.string.accept);
        positiveButton.setVisibility(View.VISIBLE);
        positiveButton.setOnClickListener(view -> connect());
        backButton.setText(R.string.ignore);
    }

    private void success() {
        titleView.setText(R.string.connection_successful);
        progressBar.setVisibility(View.GONE);
        positiveButton.setText(android.R.string.ok);
        positiveButton.setOnClickListener(view -> finish());
        backButton.setVisibility(View.GONE);
    }

    private void connect() {
        titleView.setText(R.string.connecting);
        progressBar.setVisibility(View.VISIBLE);
        positiveButton.setVisibility(View.INVISIBLE);
        backButton.setText(android.R.string.cancel);

        receiver = ConfigureActivity.registerReceiver(this, ssid, this::success);
    }
}
