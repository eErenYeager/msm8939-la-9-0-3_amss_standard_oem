/*
* Copyright (c) 2017 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Confidential and Proprietary - Qualcomm Technologies, Inc.
*/
package com.qualcomm.qti.smartassistant.asr.baidu;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.AlarmClock;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.widget.Toast;

import com.baidu.duer.dcs.api.DcsSdkBuilder;
import com.baidu.duer.dcs.api.IConnectionStatusListener;
import com.baidu.duer.dcs.api.IDcsSdk;
import com.baidu.duer.dcs.api.IDialogStateListener;
import com.baidu.duer.dcs.devicemodule.alarms.AlarmsDeviceModule;
import com.baidu.duer.dcs.devicemodule.alarms.message.SetAlarmPayload;
import com.baidu.duer.dcs.devicemodule.alarms.message.SetTimerPayload;
import com.baidu.duer.dcs.devicemodule.alarms.message.ShowAlarmsPayload;
import com.baidu.duer.dcs.devicemodule.alarms.message.ShowTimersPayload;
import com.baidu.duer.dcs.devicemodule.applauncher.AppLauncherDeviceModule;
import com.baidu.duer.dcs.devicemodule.applauncher.AppLauncherImpl;
import com.baidu.duer.dcs.devicemodule.applauncher.message.LaunchAppPayload;
import com.baidu.duer.dcs.devicemodule.location.ILocation;
import com.baidu.duer.dcs.devicemodule.location.LocationImpl;
import com.baidu.duer.dcs.devicemodule.phonecall.PhoneCallDeviceModule;
import com.baidu.duer.dcs.devicemodule.phonecall.message.CandidateCallee;
import com.baidu.duer.dcs.devicemodule.phonecall.message.PhonecallByNamePayload;
import com.baidu.duer.dcs.devicemodule.phonecall.message.PhonecallByNumberPayload;
import com.baidu.duer.dcs.devicemodule.phonecall.message.SelectCalleePayload;

import com.baidu.duer.dcs.devicemodule.screen.ScreenDeviceModule;
import com.baidu.duer.dcs.devicemodule.screen.message.HtmlPayload;
import com.baidu.duer.dcs.devicemodule.screen.message.RenderCardPayload;
import com.baidu.duer.dcs.devicemodule.screen.message.RenderHintPayload;
import com.baidu.duer.dcs.devicemodule.screen.message.RenderVoiceInputTextPayload;
import com.baidu.duer.dcs.devicemodule.sms.SmsDeviceModule;
import com.baidu.duer.dcs.devicemodule.sms.message.CandidateRecipient;
import com.baidu.duer.dcs.devicemodule.sms.message.SelectRecipientPayload;
import com.baidu.duer.dcs.devicemodule.sms.message.SendSmsByNamePayload;
import com.baidu.duer.dcs.devicemodule.sms.message.SendSmsByNumberPayload;
import com.baidu.duer.dcs.framework.DcsSdkImpl;
import com.baidu.duer.dcs.framework.ILoginListener;
import com.baidu.duer.dcs.framework.IMessageSender;
import com.baidu.duer.dcs.framework.InternalApi;
import com.baidu.duer.dcs.framework.internalapi.DcsConfig;
import com.baidu.duer.dcs.framework.internalapi.IErrorListener;
import com.baidu.duer.dcs.framework.location.Location;
import com.baidu.duer.dcs.framework.upload.contact.UploadPreference;
import com.baidu.duer.dcs.oauth.api.grant.BaiduOauthImplicitGrantIml;
import com.baidu.duer.dcs.systeminterface.BaseAudioRecorder;
import com.baidu.duer.dcs.systeminterface.IOauth;
import com.baidu.duer.dcs.util.CommonUtil;
import com.baidu.duer.dcs.util.LogUtil;
import com.baidu.duer.dcs.util.NetWorkUtil;
import com.qualcomm.qti.smartassistant.R;
import com.qualcomm.qti.smartassistant.asr.CandidateInfo;
import com.qualcomm.qti.smartassistant.asr.IVoiceSdk;
import com.qualcomm.qti.smartassistant.asr.IVoiceSdkListener;
import com.qualcomm.qti.smartassistant.service.device.SmsSender;
import com.qualcomm.qti.smartassistant.utils.LogUtils;

import com.qualcomm.qti.dueros.DuerosAccount;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

public class DuerVoiceSdkImpl implements IVoiceSdk {
    public static final String TAG = "DuerVoiceSdkImpl";
    private static final String CLIENT_ID = DuerosAccount.getClientId();
    private static final String CLIENT_SECRET = DuerosAccount.getClientSecret();
    private List<IVoiceSdkListener> mVoiceSdkListeners;
    private Context mContext;
    private IDcsSdk dcsSdk;
    private ILocation location;
    private DuerAudioRecordImpl mAudioRecorder;
    private ScreenDeviceModule screenDeviceModule;
    private PhoneCallDeviceModule phoneCallDeviceModule;
    private SmsDeviceModule smsDeviceModule;
    private AppLauncherDeviceModule appLauncherDeviceModule;
    private AlarmsDeviceModule alarmsDeviceModule;

    private PhoneCallDeviceModule.IPhoneCallListener phoneCallListener;
    private SmsDeviceModule.ISmsListener smsListener;
    private AppLauncherDeviceModule.IAppLauncherListener appLauncherListener;
    private AlarmsDeviceModule.IAlarmDirectiveListener alarmListener;
    private IDialogStateListener dialogStateListener;
    private boolean isStopListenReceiving;

    private QueryContactsTask queryContactsTask;

    private static final Map<String, Integer> calendar = new ArrayMap<>();
    static {
        calendar.put("MON", Calendar.MONDAY);
        calendar.put("TUE", Calendar.TUESDAY);
        calendar.put("WED", Calendar.WEDNESDAY);
        calendar.put("THU", Calendar.THURSDAY);
        calendar.put("FRI", Calendar.FRIDAY);
        calendar.put("SAT", Calendar.SATURDAY);
        calendar.put("SUN", Calendar.SUNDAY);
    }

    private IConnectionStatusListener connectionStatusListener = new IConnectionStatusListener() {
        @Override
        public void onConnectStatus(ConnectionStatus connectionStatus) {
            LogUtil.d(TAG, "onConnectionStatusChange: " + connectionStatus);
        }
    };

    private IErrorListener errorListener = new IErrorListener() {
        @Override
        public void onErrorCode(ErrorCode errorCode) {
            LogUtil.d(TAG, "onErrorCode:" + errorCode);
            if (errorCode == ErrorCode.VOICE_REQUEST_FAILED) {
//                Toast.makeText(mContext,
//                        mContext.getResources().getString(R.string.voice_err_msg),
//                        Toast.LENGTH_SHORT)
//                        .show();
            }
        }
    };

    private ScreenDeviceModule.IScreenListener screenListener = new ScreenDeviceModule.IScreenListener() {
        @Override
        public void onRenderVoiceInputText(RenderVoiceInputTextPayload payload) {
            handleRenderVoiceInputText(payload.text);
            if (payload.type == RenderVoiceInputTextPayload.Type.FINAL) {
                LogUtil.d(TAG,"ASR-FINAL-RESULT:" + payload.text + "," + System.currentTimeMillis());
            }
        }

        @Override
        public void onHtmlPayload(HtmlPayload htmlPayload, int id) {
            for (IVoiceSdkListener listener: mVoiceSdkListeners){
                listener.onHtmlPayload(htmlPayload.getUrl());
            }
        }

        @Override
        public void onRenderCard(RenderCardPayload renderCardPayload, int id) {

        }

        @Override
        public void onRenderHint(RenderHintPayload renderHintPayload, int id) {

        }
    };

    protected Location.LocationHandler locationHandler = new Location.LocationHandler() {
        @Override
        public double getLongitude() {
            double lon = location.getLocationInfo().longitude;
            if(lon > -0.000001 && lon < 0.000001){
                lon = 116.404;
            }
            return lon;
        }

        @Override
        public double getLatitude() {
            double lat = location.getLocationInfo().latitude;
            if(lat > -0.000001 && lat < 0.000001){
                lat = 39.915;
            }
            return lat;
        }

        @Override
        public String getCity() {
            String city = location.getLocationInfo().city;
            if(TextUtils.isEmpty(city)){
                city = "北京";
            }
            return city;
        }

        @Override
        public Location.EGeoCoordinateSystem getGeoCoordinateSystem() {
            return Location.EGeoCoordinateSystem.WGS84;
        }
    };

    public DuerVoiceSdkImpl(Context context){
        mContext = context;
        mVoiceSdkListeners = new ArrayList<>();
    }

    @Override
    public void initializeSdk(Context context) {
        initSdk(context);
        // 设置各种监听器
        dcsSdk.addConnectionStatusListener(connectionStatusListener);
        // 错误
        getInternalApi().addErrorListener(errorListener);
        // 需要定位后赋值，目前是写死的北京的
        getInternalApi().setLocationHandler(locationHandler);
        // 对话
        initVoiceRequestListener();
    }

    @Override
    public void addVoiceSdkListener(IVoiceSdkListener listener) {
        if (!mVoiceSdkListeners.contains(listener)){
            mVoiceSdkListeners.add(listener);
        }
    }

    @Override
    public void removeVoiceSdkListener(IVoiceSdkListener listener) {
        mVoiceSdkListeners.remove(listener);
    }

    @Override
    public void startRecognition(boolean captureAvailable, int captureSession,
                                 int soundModelHandle) {
        if (!NetWorkUtil.isNetworkConnected(mContext)) {
            Toast.makeText(mContext,
                    mContext.getResources().getString(R.string.err_net_msg),
                    Toast.LENGTH_SHORT).show();
            for (IVoiceSdkListener listener : mVoiceSdkListeners){
                listener.onNetworkException();
            }
            LogUtils.e(TAG, "startRecognition network exception");
            return;
        }
        if (CommonUtil.isFastDoubleClick()) {
            return;
        }
        if (isStopListenReceiving) {
            endVoiceRequest();
            return;
        }
        mAudioRecorder.setAudioRecordMode(captureAvailable ?
                DuerAudioRecordImpl.AudioRecordMode.ONE_SHOT :
                DuerAudioRecordImpl.AudioRecordMode.NORMAL,
                captureSession, soundModelHandle);
        beginVoiceRequest();
    }

    @Override
    public void stopRecognition() {
        dcsSdk.getVoiceRequest().endVoiceRequest();
        isStopListenReceiving = false;
    }

    @Override
    public void updateContacts(String contacts) {
        final String trimContacts = contacts.trim();
        String lastUpMd5 = UploadPreference.getLastUploadPhoneContacts(mContext);
        if (trimContacts.equals(lastUpMd5)){
            LogUtils.e(TAG, "updateContacts rejected since the same");
        }else {
            ((DcsSdkImpl) dcsSdk).getUpload().uploadPhoneContacts(mContext, trimContacts);
        }
    }

    @Override
    public void releaseSdk() {
        if (screenDeviceModule != null) {
            screenDeviceModule.removeScreenListener(screenListener);
        }
        if (phoneCallDeviceModule != null){
            phoneCallDeviceModule.removePhoneCallListener(phoneCallListener);
        }
        if (smsDeviceModule != null){
            smsDeviceModule.removeSmsListener(smsListener);
        }
        if (appLauncherDeviceModule != null){
            appLauncherDeviceModule.removeAppLauncherListener(appLauncherListener);
        }
        dcsSdk.getVoiceRequest().removeDialogStateListener(dialogStateListener);
        dcsSdk.removeConnectionStatusListener(connectionStatusListener);
        getInternalApi().removeErrorListener(errorListener);
        getInternalApi().setLocationHandler(null);
        dcsSdk.release();
    }

    private void beginVoiceRequest(){
        if (!isStopListenReceiving){
            isStopListenReceiving = true;
            handleRenderVoiceInputText("");
            handleStartVoiceInput();

            dcsSdk.getVoiceRequest().beginVoiceRequest(getAsrType() == DcsConfig.ASR_TYPE_AUTO);
        }
    }

    private void endVoiceRequest(){
        dcsSdk.getVoiceRequest().endVoiceRequest();
        handleStopVoiceInput();
        isStopListenReceiving = false;
    }

    private void handleStartVoiceInput(){
        for (IVoiceSdkListener listener: mVoiceSdkListeners){
            listener.onVoiceStart();
        }
        LogUtils.d(TAG, "handleStartVoiceInput");
    }

    private void handleStopVoiceInput(){
        for (IVoiceSdkListener listener: mVoiceSdkListeners){
            listener.onVoiceStop();
        }
        LogUtils.d(TAG, "handleStopVoiceInput");
    }

    private void handleRenderVoiceInputText(String text) {
        for (IVoiceSdkListener listener: mVoiceSdkListeners){
            listener.onRenderVoiceInputText(text);
        }
    }

    private InternalApi getInternalApi() {
        return ((DcsSdkImpl) dcsSdk).getInternalApi();
    }

    private int getAsrType() {
        return DcsConfig.ASR_MODE_ONLINE;
    }

    private void initSdk(Context context) {
        // 第一步初始化sdk
        mAudioRecorder = new DuerAudioRecordImpl();
        mAudioRecorder.addRecorderListener(new BaseAudioRecorder.SimpleRecorderListener() {
            @Override
            public void onData(byte[] data) {
//                int volume = calculateVolume(data);
//                Log.d(TAG, "onVolumeChange:" + volume);
            }
        });
        IOauth oauth = new BaiduOauthImplicitGrantIml(CLIENT_ID, (Activity) context);

        // 定位
        location = new LocationImpl(mContext);
        location.requestLocation(false);
        // 构造dcs sdk
        DcsSdkBuilder builder = new DcsSdkBuilder();
        dcsSdk = builder.withClientId(CLIENT_ID)
                .withOauth(oauth)
                .withAudioRecorder(mAudioRecorder)
                // 设置音乐播放器的实现，MediaPlayerImpl为内部sdk的实现
                // .withMediaPlayer(new MediaPlayerImpl())
                .build();

        // ！！！！临时配置需要在run之前设置！！！！
        // 临时配置开始
        // 暂时没有定的API接口，可以通过getInternalApi设置后使用
        getInternalApi().setDebug(true);
        getInternalApi().setAsrMode(getAsrType());
        // 临时配置结束

        // 第二步：可以按需添加内置端能力和用户自定义端能力（需要继承BaseDeviceModule）
        // 屏幕展示
        IMessageSender messageSender = getInternalApi().getMessageSender();
        screenDeviceModule = new ScreenDeviceModule(messageSender);
        screenDeviceModule.addScreenListener(screenListener);
        dcsSdk.putDeviceModule(screenDeviceModule);
        // 打电话
        phoneCallDeviceModule = new PhoneCallDeviceModule(messageSender);
        initPhoneCallListener();
        phoneCallDeviceModule.addPhoneCallListener(phoneCallListener);
        dcsSdk.putDeviceModule(phoneCallDeviceModule);
        // 发短信
        smsDeviceModule = new SmsDeviceModule(messageSender);
        initSmsListener();
        smsDeviceModule.addSmsListener(smsListener);
        dcsSdk.putDeviceModule(smsDeviceModule);
        // AppLauncher
        appLauncherDeviceModule = new AppLauncherDeviceModule(messageSender,new AppLauncherImpl(context));
        initAppLauncherListener();
        appLauncherDeviceModule.addAppLauncherListener(appLauncherListener);
        dcsSdk.putDeviceModule(appLauncherDeviceModule);
        // 本地闹钟
        alarmsDeviceModule = new AlarmsDeviceModule(messageSender);
        initAlarmListener();
        alarmsDeviceModule.addAlarmListener(alarmListener);
        dcsSdk.putDeviceModule(alarmsDeviceModule);

        // 第三步，将sdk跑起来
        ((DcsSdkImpl) dcsSdk).getInternalApi().login(new ILoginListener() {
            @Override
            public void onSucceed(String accessToken) {
                dcsSdk.run();
            }

            @Override
            public void onFailed(String errorMessage) {

            }

            @Override
            public void onCancel() {

            }
        });
    }

    private void initVoiceRequestListener() {
        // 添加会话状态监听
        dialogStateListener = new IDialogStateListener() {
            @Override
            public void onDialogStateChanged(DialogState dialogState) {
                switch (dialogState) {
                    case IDLE:
                        isStopListenReceiving = false;
                        handleStopVoiceInput();
                        break;
                    case LISTENING:
                        isStopListenReceiving = true;
                        handleStartVoiceInput();
                        break;
                    case SPEAKING:
//                        voiceButton.setText(getResources().getString(R.string.speaking));
                        break;
                    case THINKING:
//                        voiceButton.setText(getResources().getString(R.string.think));
                        break;
                    default:
                        break;
                }
            }
        };
        dcsSdk.getVoiceRequest().addDialogStateListener(dialogStateListener);
    }

    private void initPhoneCallListener() {
        // 打电话指令监听
        phoneCallListener = new PhoneCallDeviceModule.IPhoneCallListener() {
            @Override
            public void onPhoneCallByName(PhonecallByNamePayload payload) {
                List<String> contacts = new ArrayList<>();
                for (CandidateCallee callee: payload.getCandidateCallees()){
                    contacts.add(callee.getContactName());
                }
                String[] callees =  contacts.toArray(new String[contacts.size()]);
                queryContactsTask = new QueryContactsTask("");
                queryContactsTask.execute(callees);
            }

            @Override
            public void onSelectCallee(SelectCalleePayload payload) {
                Toast.makeText(mContext, "打电话指令（选择联系人）", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onPhoneCallByNumber(PhonecallByNumberPayload payload) {
                Uri uri = Uri.parse("tel:"+payload.getCallee().getPhoneNumber());
                Intent intent = new Intent(Intent.ACTION_CALL, uri);
                mContext.startActivity(intent);
            }
        };
    }

    private void initSmsListener() {
        // 发短信指令监听
        smsListener = new SmsDeviceModule.ISmsListener() {
            @Override
            public void onSendSmsByName(SendSmsByNamePayload payload) {
                List<String> contacts = new ArrayList<>();
                for (CandidateRecipient callee: payload.getCandidateRecipients()){
                    contacts.add(callee.getContactName());
                }
                String[] callees =  contacts.toArray(new String[contacts.size()]);
                queryContactsTask = new QueryContactsTask(payload.getMessageContent());
                queryContactsTask.execute(callees);
            }

            @Override
            public void onSelectRecipient(SelectRecipientPayload payload) {
                Toast.makeText(mContext, "发短信指令（选择联系人）", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSendSmsByNumber(SendSmsByNumberPayload payload) {
                SmsSender smsSender = new SmsSender(mContext);
                smsSender.sendMessage(payload.getRecipient().getPhoneNumber(),
                        payload.getMessageContent());
            }
        };
    }

    private void initAppLauncherListener() {
        // 打开应用指令监听
        appLauncherListener = new AppLauncherDeviceModule.IAppLauncherListener() {
            @Override
            public void onLaunchApp(LaunchAppPayload payload) {
                // 优先打开deepLink，然后是packageName、appName
                if (!TextUtils.isEmpty(payload.getDeepLink())) {
                    appLauncherDeviceModule.getAppLauncher().launchAppByDeepLink(mContext,
                            payload.getDeepLink());
                } else if (!TextUtils.isEmpty(payload.getPackageName())) {
                    appLauncherDeviceModule.getAppLauncher().launchAppByPackageName(mContext,
                            payload.getPackageName());
                } else if (!TextUtils.isEmpty(payload.getAppName())) {
                    appLauncherDeviceModule.getAppLauncher().launchAppByName(mContext,
                            payload.getAppName());
                }
            }
        };
    }

    private void initAlarmListener() {
        // 闹钟、定制器指令监听
        alarmListener = new AlarmsDeviceModule.IAlarmDirectiveListener() {
            @Override
            public void onSetAlarmDirectiveReceived(SetAlarmPayload setAlarmPayload) {
                //Toast.makeText(mContext, "设置闹钟指令", Toast.LENGTH_LONG).show();
                ArrayList<Integer> alarmDays = new ArrayList<>();
                for (String s : setAlarmPayload.getDays()) {
                    alarmDays.add(calendar.get(s));
                }
                Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
                intent.putExtra(AlarmClock.EXTRA_MESSAGE, setAlarmPayload.getMessage());
                intent.putExtra(AlarmClock.EXTRA_DAYS, alarmDays);
                intent.putExtra(AlarmClock.EXTRA_HOUR, setAlarmPayload.getHour());
                intent.putExtra(AlarmClock.EXTRA_MINUTES, setAlarmPayload.getMinutes());
                if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                    mContext.startActivity(intent);
                }
            }

            @Override
            public void onShowAlarmsDirectiveReceived(ShowAlarmsPayload showAlarmsPayload) {
                //Toast.makeText(mContext, "查看闹钟指令", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSetTimerDirectiveReceived(SetTimerPayload setTimerPayload) {
                //Toast.makeText(mContext, "设置定时器指令", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onShowTimersDirectiveReceived(ShowTimersPayload showTimersPayload) {
                //Toast.makeText(mContext, "查看定时器指令", Toast.LENGTH_LONG).show();
            }
        };
    }


    private class QueryContactsTask extends AsyncTask<String, Void, Integer> {
        private final String message;
        private final List<CandidateInfo> mCandidateInfos;
        private String logs = "";

        public QueryContactsTask(String message){
            this.message = message;
            mCandidateInfos = new ArrayList<>();
        }

        @Override
        protected Integer doInBackground(String... strings) {
            LogUtils.v(TAG, "QueryContactsTask.doInBackground "+strings.length);

            for (String name: strings){
                ContentResolver cr = mContext.getContentResolver();
                //根据传入的姓名查询通讯录，这个姓名就是上一个activity传过来的参数
                Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,null,
                        ContactsContract.PhoneLookup.DISPLAY_NAME + "=?",
                        new String[]{name},null);

                while(cursor.moveToNext()) {
                    int nameFieldColumnIndex = cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
                    String contact = cursor.getString(nameFieldColumnIndex);
                    //根据id获取手机号码，手机号码表和用户资料表根据id关联
                    String ContactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    Cursor phone = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID +
                                    "=" + ContactId, null, null);
                    //获取用户的第一个手机号，因为用户有可能不只有一个手机号
                    CandidateInfo candidateInfo = new CandidateInfo(name, ContactId);
                    while(phone.moveToNext()) {
                        String phoneNumber = phone.getString(phone.
                                getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String phoneType = phone.getString(phone.
                                getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                        CharSequence phoneTypeLabel = ContactsContract.CommonDataKinds.Phone.
                                getTypeLabel(mContext.getResources(),
                                Integer.parseInt(phoneType),"");
                        candidateInfo.addContactPhoneInfo(new CandidateInfo.Phone(phoneNumber,
                                phoneTypeLabel));
                        logs += (contact +":"+ phoneNumber +" "+ phoneTypeLabel +"\n");
                    }
                    mCandidateInfos.add(candidateInfo);
                }
                cursor.close();
            }
            LogUtils.d(TAG, "QueryContactsTask.doInBackground result = "+logs);
            return 0;
        }

        @Override
        protected void onPostExecute(Integer returnStatus) {
            LogUtils.v(TAG, "QueryContactsTask.onPostExecute returnStatus = "+returnStatus);
            if (returnStatus == 0) {
                if (TextUtils.isEmpty(message)){
                    for (IVoiceSdkListener listener: mVoiceSdkListeners){
                        listener.onPhoneCallByName(mCandidateInfos);
                    }
                }else {
                    for (IVoiceSdkListener listener: mVoiceSdkListeners){
                        listener.onSendSmsByName(mCandidateInfos, message);
                    }
                }
            }
        }
    }
}
