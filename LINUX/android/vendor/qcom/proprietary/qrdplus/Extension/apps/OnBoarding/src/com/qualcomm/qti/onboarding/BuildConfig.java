/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding;

public final class BuildConfig {
    public static final String BUILD_TYPE = "debug";
}
