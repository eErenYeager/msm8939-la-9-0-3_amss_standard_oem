/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.dpp;

import android.support.annotation.NonNull;

public interface DppManager {
    /**
     * Register an listener for DPP event callbacks.
     * @param listener callback
     */
    void registerListener(DppCallback listener);

    /**
     * Add the DPP bootstrap info obtained from QR code.
     * @param uri string encoded in QR code
     * @return Handle to stored info, -1 on failure
     */
    int addBootstrapQrCode(String uri);

    /**
     * Add the DPP bootstrap info obtained from NAN discovery.
     * @param uri the info obtained
     * @return Handle to stored info, -1 on failure
     */
    int addBootstrapNan(String uri);

    int generateBootstrap(DppConfig config);
    String getUri(int id);
    int removeBootstrap(int id);
    int listen(String frequency, int role);
    int addConfigurator(String curve, String key, int expiry);
    int removeConfigurator(int id);
    String getBootstrapCurveName(int id);
    int startAuth(DppConfig config);

    /**
     * Try to connect to a network.
     * @param config config for DPP connection
     * @return true if operation succeeded
     */
    boolean connect(@NonNull ConnectionConfig config);
}
