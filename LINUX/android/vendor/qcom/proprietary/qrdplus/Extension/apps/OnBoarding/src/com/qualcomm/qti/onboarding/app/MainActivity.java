/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.app;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.qualcomm.qti.onboarding.BuildConfig;
import com.qualcomm.qti.onboarding.R;

public class MainActivity extends Activity {
    static final boolean mock = BuildConfig.BUILD_TYPE.equals("debugMock");

    private static final String TAG = "MainActivity";
    private static final String ACTION = "com.google.zxing.client.android.SCAN";
    static final String RESULT = "SCAN_RESULT";

    private static final int QRCODE = 1;
    private static final int REQUEST_PERM = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            setupButtons();
        } else {
            requestPermissions(new String[] { Manifest.permission.ACCESS_COARSE_LOCATION },
                    REQUEST_PERM);
        }
    }

    private void setupButtons() {
        findViewById(R.id.configure_button).setOnClickListener(view -> {
            if (mock) {
                Toast.makeText(this, "skip qr code scan", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, ConfigureActivity.class);
                String result = "QR-Code";
                intent.putExtra(RESULT, result);
                startActivity(intent);
            } else {
                Intent intent = new Intent();
                intent.setAction(ACTION);
                try {
                    startActivityForResult(intent, QRCODE);
                } catch (ActivityNotFoundException e) {
                    Log.e(TAG, "no QR Code scanner found");
                    Toast.makeText(MainActivity.this, R.string.qr_code_scanner_not_found,
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.configurator_button).setOnClickListener(view -> {
            if (!supportWifiAware()) {
                Toast.makeText(MainActivity.this, R.string.nan_not_supported,
                        Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(MainActivity.this, ConfiguratorActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.enrollee_button).setOnClickListener(view -> {
            if (!supportWifiAware()) {
                Toast.makeText(MainActivity.this, R.string.nan_not_supported,
                        Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(MainActivity.this, EnrolleeActivity.class);
                startActivity(intent);
            }
        });
    }

    private boolean supportWifiAware() {
        if (mock) {
            Log.v(TAG, "supportWifiAware: mock");
            return true;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            Log.w(TAG, "supportWifiAware api=" + Build.VERSION.SDK_INT);
            return false;
        } else {
            boolean ok = getPackageManager().hasSystemFeature(PackageManager.FEATURE_WIFI_AWARE);
            if (!ok) {
                Log.w(TAG, "supportWifiAware false");
            }
            return ok;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERM: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupButtons();
                } else {
                    finish();
                }
                break;
            }
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == QRCODE && resultCode == RESULT_OK) {
            Intent intent = new Intent(this, ConfigureActivity.class);
            String result = data.getStringExtra(RESULT);
            intent.putExtra(RESULT, result);
            startActivity(intent);
        }
    }
}
