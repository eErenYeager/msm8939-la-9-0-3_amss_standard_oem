/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.nan;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.aware.PeerHandle;
import android.support.annotation.NonNull;
import android.util.Log;

public class MockNanManager implements NanManager {
    private static final String TAG = "MockNanManager";

    private @NonNull final Activity activity;
    private @NonNull final NanCallback callback;
    private boolean active;
    private Thread thread;

    public MockNanManager(final @NonNull Activity activity, final @NonNull NanCallback callback) {
        this.activity = activity;
        active = false;
        this.callback = callback;
    }

    @Override
    public void register(@NonNull Context context) {
        active = true;
        thread = new Thread(() -> {
            if (!active) return;
            Log.d(TAG, "onAttach");
            final String info = callback.onAttach(true);

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!active) return;
            activity.runOnUiThread(() -> {
                if (info == null) {
                    // Configurator
                    Log.d(TAG, "onDiscover1");
                    Device device = new Device("Alice", "enro1", null);
                    callback.onDiscover(null, device.toJsonString().getBytes());
                    Log.d(TAG, "onDiscover2");
                    device = new Device("Bob", "enro2", null);
                    callback.onDiscover(null, device.toJsonString().getBytes());
                } else {
                    // Enrollee
                    Log.d(TAG, "onReceive");
                    callback.onReceive(null, "Configurator1 uri-conf1".getBytes());
                }
            });
        });
        thread.start();
    }

    @Override
    public void unregister(@NonNull Context context) {
        active = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread = null;
    }

    @Override
    public void sendMessage(PeerHandle peerHandle, int messageId, byte[] message) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        activity.runOnUiThread(() -> {
            Log.d(TAG, "sendMessage success");
            callback.onMessageSent(messageId, true);
        });
    }
}
