/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.dpp;

import java.lang.reflect.Field;

public class DppConfig {
    public static final int ROLE_CONFIGURATOR = 0;
    public static final int ROLE_ENROLLEE = 1;
    public static final int TYPE_QR_CODE = 0;
    public static final int TYPE_NAN = 1;

    public int peerBootstrapId;
    public int ownBootstrapId;
    public int dppRole;
    public String ssid;
    public String passphrase;
    public boolean isDpp;
    public int confId;
    public int bootstrapType;
    public String frequency;
    public String macAddr;
    public String info;
    public String curve;
    public int expiry;
    public String key;

    public Object toWifiDppConfig(final Class<?> cls) {
        try {
            Object o = cls.newInstance();

            Field field = cls.getField("peer_bootstrap_id");
            field.set(o, peerBootstrapId);
            field = cls.getField("own_bootstrap_id");
            field.set(o, ownBootstrapId);
            field = cls.getField("dpp_role");
            field.set(o, dppRole);
            field = cls.getField("ssid");
            field.set(o, ssid);
            field = cls.getField("passphrase");
            field.set(o, passphrase);
            field = cls.getField("isDpp");
            field.set(o, isDpp ? 1 : 0);
            field = cls.getField("conf_id");
            field.set(o, confId);
            field = cls.getField("bootstrap_type");
            field.set(o, bootstrapType);
            field = cls.getField("frequency");
            field.set(o, frequency);
            field = cls.getField("mac_addr");
            field.set(o, macAddr);
            field = cls.getField("info");
            field.set(o, info);
            field = cls.getField("curve");
            field.set(o, curve);
            field = cls.getField("expiry");
            field.set(o, expiry);
            field = cls.getField("key");
            field.set(o, key);

            return o;
        } catch (InstantiationException | IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }
}
