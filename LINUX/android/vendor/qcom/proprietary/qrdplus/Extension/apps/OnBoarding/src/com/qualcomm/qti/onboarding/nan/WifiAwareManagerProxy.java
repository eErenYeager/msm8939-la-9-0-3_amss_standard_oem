/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.nan;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.net.wifi.aware.AttachCallback;
import android.net.wifi.aware.DiscoverySessionCallback;
import android.net.wifi.aware.PeerHandle;
import android.net.wifi.aware.PublishConfig;
import android.net.wifi.aware.PublishDiscoverySession;
import android.net.wifi.aware.SubscribeConfig;
import android.net.wifi.aware.SubscribeDiscoverySession;
import android.net.wifi.aware.WifiAwareManager;
import android.net.wifi.aware.WifiAwareSession;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

@TargetApi(26)
public class WifiAwareManagerProxy implements NanManager {
    private static final String TAG = "StateManager";
    private static final String SERVICE = "dppBootstrap";

    private final @NonNull NanCallback callback;
    private BroadcastReceiver receiver;
    private WifiAwareManager manager;
    private WifiAwareSession session;
    private PublishDiscoverySession publishSession;
    private SubscribeDiscoverySession subscribeSession;

    /**
     * Instantiates a WifiAwareManagerProxy
     * @param callback used to receive events
     */
    public WifiAwareManagerProxy(final @NonNull NanCallback callback) {
        this.callback = callback;
    }

    /**
     * Registers for Wi-Fi Aware state change. It will try to obtain a WifiAwareSession when
     * possible.
     * @param context application context
     */
    @Override
    public void register(@NonNull Context context) {
        Log.d(TAG, "register");
        if (!enableWifi(context)) {
            Log.e(TAG, "turn on wifi");
            return;
        }

        manager = context.getSystemService(WifiAwareManager.class);
        boolean available = manager.isAvailable();
        Log.d(TAG, "WifiAwareManager state=" + available);
        if (available && session == null) {
            attach();
        }

        IntentFilter filter = new IntentFilter(WifiAwareManager.ACTION_WIFI_AWARE_STATE_CHANGED);
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (manager.isAvailable()) {
                    Log.d(TAG, "WifiAwareManager available");
                    if (session == null) {
                        attach();
                    }
                } else {
                    Log.d(TAG, "WifiAwareManager unavailable");
                    if (session != null) {
                        session.close();
                        session = null;
                    }
                }
            }
        };
        context.registerReceiver(receiver, filter);
    }

    /**
     * Unregisters for Wi-Fi Aware state change, and closes all sessions.
     * @param context application context
     */
    @Override
    public void unregister(@NonNull Context context) {
        Log.d(TAG, "unregister");
        context.unregisterReceiver(receiver);
        if (publishSession != null) {
            publishSession.close();
            publishSession = null;
        }
        if (subscribeSession != null) {
            subscribeSession.close();
            subscribeSession = null;
        }
        if (session != null) {
            session.close();
            session = null;
        }
        receiver = null;
        manager = null;
    }

    /**
     * Calls WifiAwareManager.attach()
     */
    private void attach() {
        manager.attach(new AttachCallback() {
            @Override
            public void onAttached(WifiAwareSession s) {
                Log.d(TAG, "session attached");
                session = s;
                String info = callback.onAttach(true);
                if (info != null) {
                    publish(info);
                } else {
                    subscribe();
                }
            }

            @Override
            public void onAttachFailed() {
                Log.w(TAG, "attach failed");
                callback.onAttach(false);
            }
        }, null);
    }

    /**
     * Calls WifiAwareSession.subscribe()
     */
    private void subscribe() {
        SubscribeConfig config = new SubscribeConfig.Builder().setServiceName(SERVICE).build();
        session.subscribe(config, new DiscoverySessionCallback() {
            @Override
            public void onSubscribeStarted(SubscribeDiscoverySession session) {
                Log.d(TAG, "subscribe started");
                subscribeSession = session;
            }

            @Override
            public void onServiceDiscovered(PeerHandle peerHandle, byte[] serviceSpecificInfo,
                    List<byte[]> matchFilter) {
                Log.d(TAG, "service discovered");
                String msg = callback.onDiscover(peerHandle, serviceSpecificInfo);
                if (msg != null) {
                    subscribeSession.sendMessage(peerHandle, 0, msg.getBytes());
                }
            }
        }, null);
    }

    /**
     * Calls WifiAwareSession.publish()
     * @param info service specific info
     */
    private void publish(@NonNull String info) {
        PublishConfig config = new PublishConfig.Builder()
                .setServiceName(SERVICE)
                .setServiceSpecificInfo(info.getBytes())
                .build();
        session.publish(config, new DiscoverySessionCallback() {
            @Override
            public void onPublishStarted(PublishDiscoverySession session) {
                Log.d(TAG, "publish started");
                publishSession = session;
            }

            @Override
            public void onMessageReceived(PeerHandle peerHandle, byte[] message) {
                Log.d(TAG, "message received (publisher)");
                callback.onReceive(peerHandle, message);
            }
        }, null);
    }

    private boolean enableWifi(Context context) {
        WifiManager wifiManager = context.getSystemService(WifiManager.class);
        return wifiManager.isWifiEnabled() || wifiManager.setWifiEnabled(true);
    }

    @Override
    public void sendMessage(PeerHandle peerHandle, int messageId, byte[] message) {
        if (publishSession == null) {
            Log.e(TAG, "sendMessage without publish session");
            return;
        }
        publishSession.sendMessage(peerHandle, messageId, message);
    }
}
