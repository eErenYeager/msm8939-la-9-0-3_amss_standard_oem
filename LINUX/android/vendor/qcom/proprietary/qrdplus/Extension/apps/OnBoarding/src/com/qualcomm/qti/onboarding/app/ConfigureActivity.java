/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.qualcomm.qti.onboarding.R;
import com.qualcomm.qti.onboarding.dpp.ConnectionConfig;
import com.qualcomm.qti.onboarding.dpp.DppCallback;
import com.qualcomm.qti.onboarding.dpp.DppConfig;
import com.qualcomm.qti.onboarding.dpp.DppManager;
import com.qualcomm.qti.onboarding.dpp.MockDppManager;
import com.qualcomm.qti.onboarding.dpp.WifiManagerProxy;

public class ConfigureActivity extends Activity {
    private static final String TAG = "Configure";

    private static final int IDLE = 0;
    private static final int CONNECTING = 1;
    private static final int SUCCESS = 2;

    private DppManager dppManager;
    private DppConfig dppConfig;
    private ConnectionConfig connectionConfig;

    private @Nullable BroadcastReceiver receiver;
    private int status = IDLE;

    private TextView titleView;
    private EditText ssidView;
    private ProgressBar progressBar;
    private Button positiveButton;
    private Button backButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure);

        titleView = findViewById(R.id.title);
        ssidView = findViewById(R.id.ssid);
        progressBar = findViewById(R.id.progress);
        positiveButton = findViewById(R.id.positive);
        backButton = findViewById(R.id.back);

        specifySSID();

        if (MainActivity.mock) {
            dppManager = new MockDppManager(this);
        } else {
            dppManager = new WifiManagerProxy(this);
        }
        dppConfig = new DppConfig();
        String uri = getIntent().getStringExtra(MainActivity.RESULT);
        Log.d(TAG, "qr code: " + uri);
        dppConfig.peerBootstrapId = dppManager.addBootstrapQrCode(uri);
        if (dppConfig.peerBootstrapId == -1) {
            Log.e(TAG, "addBootstrapQrCode");
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
    }

    private void specifySSID() {
        titleView.setText(R.string.ap_found);
        ssidView.setVisibility(View.VISIBLE);
        ssidView.setEnabled(true);
        ssidView.setOnEditorActionListener((textView, i, keyEvent) -> {
            positiveButton.callOnClick();
            return true;
        });
        progressBar.setVisibility(View.GONE);
        positiveButton.setVisibility(View.VISIBLE);
        positiveButton.setText(R.string.configure);
        positiveButton.setOnClickListener(view -> {
            if (TextUtils.isEmpty(ssidView.getText())) {
                ssidView.setError(getString(R.string.ssid_prompt));
            } else {
                configure();
            }
        });
        backButton.setText(android.R.string.cancel);
        backButton.setOnClickListener(view -> finish());
    }

    private void configure() {
        titleView.setText(R.string.configuring_ap);
        ssidView.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        positiveButton.setVisibility(View.INVISIBLE);
        backButton.setVisibility(View.VISIBLE);

        prepareDppConfig();
        startAuth();
    }

    private void prepareDppConfig() {
        dppConfig.confId = dppManager.addConfigurator(null, null, 0);
        if (dppConfig.confId == -1) {
            Log.e(TAG, "addConfigurator");
            finish();
        }
        dppConfig.dppRole = DppConfig.ROLE_CONFIGURATOR;
        dppConfig.ssid = ssidView.getText().toString();
        dppConfig.isDpp = true;
        dppConfig.expiry = 10000;

        // TODO: freq, curve?
    }

    private void startAuth() {
        connectionConfig = new ConnectionConfig();
        dppManager.registerListener(new DppCallback() {
            @Override
            public void onConfSent() {
                super.onConfSent();
                tryConnect();
            }

            @Override
            public void onConfObjConnector(@NonNull String connector) {
                super.onConfObjConnector(connector);
                connectionConfig.setConnector(connector);
                tryConnect();
            }

            @Override
            public void onConfObjNetAccessKey(@NonNull String key) {
                super.onConfObjNetAccessKey(key);
                connectionConfig.setNetAccessKey(key);
                tryConnect();
            }

            @Override
            public void onConfObjCSignKey(@NonNull String key) {
                super.onConfObjCSignKey(key);
                connectionConfig.setCSign(key);
                tryConnect();
            }
        });
        dppManager.startAuth(dppConfig);
    }

    private void tryConnect() {
        if (status == IDLE && connectionConfig.isValid()) {
            if (MainActivity.mock) {
                Log.d(TAG, "mock successful connection");
                status = SUCCESS;
                success();
            } else {
                receiver = registerReceiver(this, dppConfig.ssid, () -> {
                    status = SUCCESS;
                    success();
                });
                if (dppManager.connect(connectionConfig)) {
                    status = CONNECTING;
                }
            }
        } else {
            Log.d(TAG, "not connecting, status=" + status);
        }
    }

    private void success() {
        titleView.setText(R.string.configuration_successful);
        progressBar.setVisibility(View.GONE);
        positiveButton.setVisibility(View.VISIBLE);
        positiveButton.setText(android.R.string.ok);
        positiveButton.setOnClickListener(view -> finish());
        backButton.setVisibility(View.INVISIBLE);

        Storage.save(this, dppConfig.ssid, connectionConfig);
    }

    static BroadcastReceiver registerReceiver(final Context context, final String ssid,
            Runnable runnable) {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String bssid = intent.getStringExtra(WifiManager.EXTRA_BSSID);
                if (bssid != null && bssid.equals(ssid)) {
                    runnable.run();
                } else {
                    Log.d(TAG, "onReceive bssid=" + nullableString(bssid)
                            + " ssid=" + nullableString(ssid));
                }
            }
        };
        context.registerReceiver(receiver,
                new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
        return receiver;
    }

    @NonNull
    private static String nullableString(@Nullable String s) {
        return s == null ? "(null)" : s;
    }
}
