/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.qualcomm.qti.onboarding.dpp.ConnectionConfig;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Stores all OnBoarded APs
 */
class Storage {
    private static final String TAG = "Storage";
    /**
     * Name of shared preferences file
     */
    private static final String NAME = "ap";
    /**
     * Json key of the AP object
     */
    private static final String KEY = "config";

    @Nullable
    private static JSONObject load(@NonNull SharedPreferences preferences) {
        String s = preferences.getString(KEY, null);
        if (s == null) {
            Log.d(TAG, "no config found");
            return null;
        }
        try {
            return new JSONObject(s);
        } catch (JSONException e) {
            Log.d(TAG, "not a json object: " + s);
        }
        return null;
    }

    @Nullable
    static JSONObject load(@NonNull Context context) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return load(preferences);
    }

    static void save(@NonNull Context context, @NonNull String ssid, @NonNull ConnectionConfig config) {
        SharedPreferences preferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        JSONObject object = load(preferences);
        if (object == null) {
            object = new JSONObject();
        }
        SharedPreferences.Editor editor = preferences.edit();
        try {
            object.put(ssid, config.toJson());
            editor.putString(KEY, object.toString());
            editor.apply();
        } catch (JSONException e) {
            Log.e(TAG, "save ssid/config");
        }
    }
}
