/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.nan;

import android.net.wifi.aware.PeerHandle;
import android.support.annotation.Nullable;

/**
 * Wi-Fi Aware events callbacks
 */
public interface NanCallback {
    /**
     * Notifies WifiAwareManager.attach() result
     *
     * @param success whether or not a WifiAwareSession is successfully obtained
     * @return A non-null String to publish, null to subscribe
     */
    @Nullable
    String onAttach(boolean success);

    /**
     * (Subscriber) Notifies DiscoverySessionCallback.onServiceDiscovered() result
     *
     * @param peerHandle Publisher's handle
     * @param message    message from Publisher
     * @return A String to be sent as a message to Publisher. null indicates an error state,
     * or returned from Publisher side.
     */
    @Nullable
    String onDiscover(PeerHandle peerHandle, byte[] message);

    /**
     * (Publisher) Notifies DiscoverySessionCallback.onMessageReceived() result
     *
     * @param peerHandle Subscriber's handle
     * @param message    message from Subscriber
     */
    void onReceive(PeerHandle peerHandle, byte[] message);

    /**
     * (Subscriber) Notifies DiscoverySessionCallback.onMessageSend{Succeeded,Failed}() result
     * @param messageId message ID
     * @param success successful or not
     */
    void onMessageSent(int messageId, boolean success);
}
