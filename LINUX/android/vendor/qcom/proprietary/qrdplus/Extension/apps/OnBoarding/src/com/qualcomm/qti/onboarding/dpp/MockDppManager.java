/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.dpp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class MockDppManager implements DppManager {
    private final @NonNull Activity activity;
    private @Nullable DppCallback callback;

    public MockDppManager(final @NonNull Activity activity) {
        this.activity = activity;
    }

    @Override
    public void registerListener(DppCallback listener) {
        this.callback = listener;
    }

    @Override
    public int addBootstrapQrCode(String uri) {
        return 0;
    }

    @Override
    public int addBootstrapNan(String uri) {
        return 0;
    }

    @Override
    public int generateBootstrap(DppConfig config) {
        return 0;
    }

    @Override
    public String getUri(int id) {
        return "uri-" + id;
    }

    @Override
    public int removeBootstrap(int id) {
        return 0;
    }

    @Override
    public int listen(String frequency, int role) {
        new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (callback != null) {
                activity.runOnUiThread(() -> callback.onConfReceived(0));
            }
        }).start();
        return 0;
    }

    @Override
    public int addConfigurator(String curve, String key, int expiry) {
        return 0;
    }

    @Override
    public int removeConfigurator(int id) {
        return 0;
    }

    @Override
    public String getBootstrapCurveName(int id) {
        return "curve";
    }

    @Override
    public int startAuth(DppConfig config) {
        new Thread(() -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (callback != null) {
                activity.runOnUiThread(() -> {
                    callback.onConfObjConnector("connector");
                    callback.onConfObjNetAccessKey("net-access-key");
                    callback.onConfObjCSignKey("c-sign-key");
                    callback.onConfSent();
                });
            }
        }).start();
        return 0;
    }

    @Override
    public boolean connect(@NonNull ConnectionConfig config) {
        return true;
    }
}
