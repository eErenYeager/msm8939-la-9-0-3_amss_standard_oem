/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.dpp;

import android.net.wifi.WifiConfiguration;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

public class ConnectionConfig {
    private static final String TAG = "ConnectionConfig";

    private String connector;
    private String netAccessKey;
    private int netAccessKeyExpiry;
    private String cSign;
    private int cSignExpiry;

    public void setConnector(@NonNull String connector) {
        this.connector = connector;
    }

    public void setNetAccessKey(@NonNull String netAccessKey) {
        this.netAccessKey = netAccessKey;
    }

    public void setNetAccessKeyExpiry(int netAccessKeyExpiry) {
        this.netAccessKeyExpiry = netAccessKeyExpiry;
    }

    public void setCSign(@NonNull String cSign) {
        this.cSign = cSign;
    }

    public void setCSignExpiry(int cSignExpiry) {
        this.cSignExpiry = cSignExpiry;
    }

    @Nullable WifiConfiguration toWifiConfiguration() {
        WifiConfiguration config = new WifiConfiguration();
        final Class<?> cls = config.getClass();
        try {
            Field field = cls.getField("dppConnector");
            field.set(config, connector);
            field = cls.getField("dppNetAccessKey");
            field.set(config, netAccessKey);
            field = cls.getField("dppNetAccessKeyExpiry");
            field.set(config, netAccessKeyExpiry);
            field = cls.getField("dppCsign");
            field.set(config, cSign);
            field = cls.getField("dppCsignExpiry");
            field.set(config, cSignExpiry);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        }
        return config;
    }

    // names for json (de)serialization
    private static final String CONN = "conn";
    private static final String KEY = "key";
    private static final String KEY_EXP = "key_exp";
    private static final String SIGN = "sign";
    private static final String SIGN_EXP = "sign_exp";

    public JSONObject toJson() {
        JSONObject object = new JSONObject();
        try {
            object.put(CONN, connector);
            object.put(KEY, netAccessKey);
            object.put(KEY_EXP, netAccessKeyExpiry);
            object.put(SIGN, cSign);
            object.put(SIGN_EXP, cSignExpiry);
        } catch (JSONException e) {
            Log.e(TAG, "toJsonString");
        }
        return object;
    }

    @Nullable
    public static ConnectionConfig parse(JSONObject object) {
        ConnectionConfig config = new ConnectionConfig();
        try {
            config.connector = object.getString(CONN);
            config.netAccessKey = object.getString(KEY);
            config.netAccessKeyExpiry = object.getInt(KEY_EXP);
            config.cSign = object.getString(SIGN);
            config.cSignExpiry = object.getInt(SIGN_EXP);
            return config;
        } catch (JSONException e) {
            Log.e(TAG, "parse");
        }
        return null;
    }

    @Nullable
    public static ConnectionConfig parse(@NonNull String str) {
        try {
            JSONObject object = new JSONObject(str);
            return parse(object);
        } catch (JSONException e) {
            Log.e(TAG, "parse");
        }
        return null;
    }

    public boolean isValid() {
        return connector != null && netAccessKey != null && cSign != null;
    }
}
