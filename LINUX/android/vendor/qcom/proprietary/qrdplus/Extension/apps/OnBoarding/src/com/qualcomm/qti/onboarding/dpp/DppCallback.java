/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.dpp;

import android.support.annotation.NonNull;
import android.util.Log;

// Corresponding to android.net.wifi.WifiManager.DppCallback
public class DppCallback {
    private static final String TAG = "DppCallback";

    public void onAuthSucceeded() {
        Log.d(TAG, "onAuthSucceeded");
    }

    public void onAuthFailed(int reason) {
        Log.e(TAG, "onAuthFailed " + reason);
    }

    public void onConfSent() {
        Log.d(TAG, "onConfSent");
    }

    public void onConfFailed(int reason) {
        Log.e(TAG, "onConfFailed " + reason);
    }

    public void onConfReceived(int conf_mask) {
        Log.d(TAG, "onConfReceived " + conf_mask);
    }

    public void onConfObjSSID(String ssid) {
        Log.d(TAG, "onConfObjSSID " + ssid);
    }

    public void onConfObjConnector(@NonNull String connector) {
        Log.d(TAG, "onConfObjConnector " + connector);
    }

    public void onConfObjCSignKey(@NonNull String key) {
        Log.d(TAG, "onConfObjCSignKey " + key);
    }

    public void onConfObjNetAccessKey(@NonNull String key) {
        Log.d(TAG, "onConfObjNetAccessKey " + key);
    }

    public void onConfObjNetworkKey(String key) {
        Log.d(TAG, "onConfObjNetworkKey " + key);
    }
}
