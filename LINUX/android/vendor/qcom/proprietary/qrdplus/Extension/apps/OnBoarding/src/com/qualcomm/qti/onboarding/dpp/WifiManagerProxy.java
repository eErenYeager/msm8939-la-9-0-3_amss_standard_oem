/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.dpp;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.annotation.NonNull;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * A proxy to actual DPP implementation.
 */
public class WifiManagerProxy implements DppManager {
    private static final String TAG = "WifiManagerProxy";

    private final @NonNull WifiManager manager;
    private Class<?> configClass;

    /**
     * Constructor
     * @param context application context
     */
    public WifiManagerProxy(@NonNull Context context) {
        manager = context.getSystemService(WifiManager.class);
        try {
            configClass = Class.forName("android.net.wifi.WifiDppConfig");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void registerListener(DppCallback listener) {
    }

    @Override
    public int addBootstrapQrCode(String uri) {
        try {
            Method method = WifiManager.class.getMethod("dppAddBootstrapQrCode", String.class);
            return (Integer) method.invoke(manager, uri);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int addBootstrapNan(String uri) {
        try {
            Method method = WifiManager.class.getMethod("dppAddBootstrapNan", String.class);
            return (Integer) method.invoke(manager, uri);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int generateBootstrap(DppConfig config) {
        try {
            Method method = WifiManager.class.getMethod("dppBootstrapGenerate", configClass);
            return (Integer) method.invoke(manager, config.toWifiDppConfig(configClass));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public String getUri(int id) {
        try {
            Method method = WifiManager.class.getMethod("dppGetUri", Integer.class);
            return (String) method.invoke(manager, id);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int removeBootstrap(int id) {
        try {
            Method method = WifiManager.class.getMethod("dppBootstrapRemove", Integer.class);
            return (Integer) method.invoke(manager, id);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int listen(String frequency, int role) {
        try {
            Method method = WifiManager.class.getMethod("dppListen", String.class, Integer.class);
            return (Integer) method.invoke(manager, frequency, role);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int addConfigurator(String curve, String key, int expiry) {
        try {
            Method method = WifiManager.class.getMethod("dppConfiguratorAdd", String.class,
                    String.class, Integer.class);
            return (Integer) method.invoke(manager, curve, key, expiry);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int removeConfigurator(int id) {
        try {
            Method method = WifiManager.class.getMethod("dppConfiguratorRemove", Integer.class);
            return (Integer) method.invoke(manager, id);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public String getBootstrapCurveName(int id) {
        try {
            Method method = WifiManager.class.getMethod("dppGetBootstrapCurveName", Integer.class);
            return (String) method.invoke(manager, id);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int startAuth(DppConfig config) {
        try {
            Method method = WifiManager.class.getMethod("dppStartAuth", configClass);
            return (Integer) method.invoke(manager, config.toWifiDppConfig(configClass));
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public boolean connect(@NonNull ConnectionConfig config) {
        WifiConfiguration wifiConfiguration = config.toWifiConfiguration();
        if (wifiConfiguration == null) {
            Log.e(TAG, "toWifiConfiguration");
            return false;
        }
        // WifiConfiguration.KeyMgmt.DPP
        wifiConfiguration.allowedKeyManagement.set(10);

        int id = manager.addNetwork(wifiConfiguration);
        if (id == -1) {
            Log.e(TAG, "addNetwork");
            return false;
        }
        boolean ok = manager.enableNetwork(id, false);
        if (!ok) {
            Log.e(TAG, "enableNetwork");
            return false;
        }
        ok = manager.reconnect();
        if (!ok) {
            Log.e(TAG, "reconnect");
        }
        return ok;
    }
}
