/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.nan;

import android.content.Context;
import android.net.wifi.aware.PeerHandle;
import android.support.annotation.NonNull;

public interface NanManager {
    void register(@NonNull Context context);
    void unregister(@NonNull Context context);
    void sendMessage(PeerHandle peerHandle, int messageId, byte[] message);
}
