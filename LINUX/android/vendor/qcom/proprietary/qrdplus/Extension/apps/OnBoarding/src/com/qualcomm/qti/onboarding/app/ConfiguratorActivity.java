/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.wifi.aware.PeerHandle;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.qualcomm.qti.onboarding.R;
import com.qualcomm.qti.onboarding.dpp.ConnectionConfig;
import com.qualcomm.qti.onboarding.dpp.DppCallback;
import com.qualcomm.qti.onboarding.dpp.DppConfig;
import com.qualcomm.qti.onboarding.dpp.DppManager;
import com.qualcomm.qti.onboarding.dpp.MockDppManager;
import com.qualcomm.qti.onboarding.dpp.WifiManagerProxy;
import com.qualcomm.qti.onboarding.nan.Device;
import com.qualcomm.qti.onboarding.nan.MockNanManager;
import com.qualcomm.qti.onboarding.nan.NanCallback;
import com.qualcomm.qti.onboarding.nan.NanManager;
import com.qualcomm.qti.onboarding.nan.WifiAwareManagerProxy;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@TargetApi(26)
public class ConfiguratorActivity extends Activity {
    private static final String TAG = "Configurator";

    private NanManager nanManager;
    private DppManager dppManager;

    private TextView titleView;
    private ProgressBar progressBar;
    private ListView listView;
    private Button positiveButton;
    private Button backButton;

    private Drawable checkDrawable;

    private ArrayList<Device> devices;
    private int deviceSelection;
    private int apSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurator);

        titleView = findViewById(R.id.title);
        progressBar = findViewById(R.id.progress);
        listView = findViewById(R.id.list);
        positiveButton = findViewById(R.id.positive);
        backButton = findViewById(R.id.back);

        checkDrawable = getDrawable(R.drawable.ic_check);
        if (checkDrawable != null) {
            checkDrawable.setBounds(0, 0, checkDrawable.getIntrinsicWidth(),
                    checkDrawable.getIntrinsicHeight());
        }

        devices = new ArrayList<>();
        final DeviceAdapter adapter = new DeviceAdapter(this, R.layout.view_device, devices);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((adapterView, view, pos, id) -> {
            deviceSelection = pos;
            adapter.notifyDataSetChanged();
        });
        deviceSelection = -1;
        apSelection = -1;

        search();
    }

    @Override
    protected void onStart() {
        super.onStart();
        final NanCallback nanCallback = new NanCallback() {
            @Override
            public @Nullable
            String onAttach(boolean success) {
                return null;
            }

            @Override
            public @Nullable
            String onDiscover(PeerHandle peerHandle, byte[] message) {
                Device device = Device.parse(message, peerHandle);
                if (device != null) {
                    if (devices.isEmpty()) {
                        deviceSelection = 0;
                    }
                    devices.add(device);
                }
                showDevices(message, peerHandle);
                return generateBootstrap(message);
            }

            @Override
            public void onReceive(PeerHandle peerHandle, byte[] message) {}

            @Override
            public void onMessageSent(int messageId, boolean success) {
                if (success) {
                    configSent();
                } else {
                    Toast.makeText(ConfiguratorActivity.this, R.string.configurator_failed,
                            Toast.LENGTH_SHORT).show();
                    selectAp();
                }
            }
        };
        if (MainActivity.mock) {
            dppManager = new MockDppManager(this);
            nanManager = new MockNanManager(this, nanCallback);
        } else {
            dppManager = new WifiManagerProxy(this);
            nanManager = new WifiAwareManagerProxy(nanCallback);
        }
        nanManager.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        nanManager.unregister(this);
        nanManager = null;
    }

    /**
     * Show "searching device" UI
     */
    private void search() {
        titleView.setText(R.string.searching_device);
        progressBar.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        positiveButton.setText(R.string.select_device);
        positiveButton.setEnabled(false);
        backButton.setText(android.R.string.cancel);
        backButton.setOnClickListener(view -> finish());
    }

    /**
     * Generate own bootstrap info according to the message received from Enrollee.
     * @param message msg received from Enrollee
     * @return bootstrap info, or null on error
     */
    private @Nullable String generateBootstrap(byte[] message) {
        int peerId = dppManager.addBootstrapNan(new String(message));
        if (peerId == -1) {
            Log.e(TAG, "addBootstrapNan");
            return null;
        }
        String curve = dppManager.getBootstrapCurveName(peerId);
        if (curve == null) {
            Log.e(TAG, "getBootstrapCurveName");
            return null;
        }
        // TODO
        DppConfig config = new DppConfig();
        int id = dppManager.generateBootstrap(config);
        if (id == -1) {
            Log.e(TAG, "generateBootstrap");
            return null;
        }
        String uri = dppManager.getUri(id);
        if (uri == null) {
            Log.e(TAG, "getUri");
            if (dppManager.removeBootstrap(id) == -1) {
                Log.e(TAG, "removeBootstrap");
            }
            return null;
        }
        Device device = new Device(Build.DEVICE, uri, null);
        return device.toJsonString();
    }

    /**
     * Show device Selection UI.
     * @param message message received from discovery callback
     */
    private void showDevices(byte[] message, PeerHandle peerHandle) {
        titleView.setText(R.string.nan_device_found);
        progressBar.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        positiveButton.setEnabled(true);
        positiveButton.setOnClickListener(view -> selectAp());
        backButton.setEnabled(true);
    }

    private void selectAp() {
        JSONObject object = Storage.load(this);
        final ArrayList<AccessPoint> aps = AccessPoint.parseAPs(object);

        titleView.setText(R.string.nan_select_ap);
        positiveButton.setOnClickListener(view -> {
            if (apSelection < 0 || apSelection >= aps.size()) {
                Log.e(TAG, "ap selection out of range: " + apSelection + "/" + aps.size());
            } else {
                startConfig(aps.get(apSelection));
            }
        });

        ApAdapter adapter = new ApAdapter(this, R.layout.view_device, aps);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener((adapterView, view, pos, id) -> {
            apSelection = pos;
            adapter.notifyDataSetChanged();
        });
    }

    private void startConfig(@NonNull AccessPoint ap) {
        titleView.setText(R.string.sending_config);
        progressBar.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        positiveButton.setVisibility(View.INVISIBLE);

        if (deviceSelection >= 0 && deviceSelection < devices.size()) {
            int id = dppManager.addConfigurator(null, null, 0);
            if (id == -1) {
                Log.e(TAG, "addConfigurator");
                return;
            }
            // TODO
            dppManager.registerListener(new DppCallback());
            dppManager.startAuth(new DppConfig());
        } else {
            Log.e(TAG, "device selection out of range: " + deviceSelection + "/" + devices.size());
        }
    }

    private void configSent() {
        titleView.setText(R.string.configuration_successful);
        progressBar.setVisibility(View.INVISIBLE);
        positiveButton.setVisibility(View.VISIBLE);
        positiveButton.setText(android.R.string.ok);
        positiveButton.setOnClickListener(v -> finish());
        backButton.setVisibility(View.INVISIBLE);
    }

    private View setupView(int position, @Nullable View convertView, @NonNull ViewGroup parent,
            @Nullable String text, int selection) {
        if (convertView == null) {
            convertView = getLayoutInflater().inflate(R.layout.view_device, parent, false);
        }
        TextView tv = (TextView) convertView;
        if (text != null) tv.setText(text);

        if (position == selection) {
            tv.setCompoundDrawables(null, null, checkDrawable, null);
        } else {
            tv.setCompoundDrawables(null, null, null, null);
        }
        return convertView;
    }

    private class DeviceAdapter extends ArrayAdapter<Device> {
        DeviceAdapter(@NonNull Context context, @LayoutRes int resource,
                @NonNull List<Device> objects) {
            super(context, resource, objects);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            Device device = getItem(position);
            return setupView(position, convertView, parent, device == null ? null : device.name,
                    deviceSelection);
        }
    }

    private class ApAdapter extends ArrayAdapter<AccessPoint> {
        ApAdapter(@NonNull Context context, int resource, @NonNull List<AccessPoint> objects) {
            super(context, resource, objects);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            AccessPoint ap = getItem(position);
            return setupView(position, convertView, parent, ap == null ? null : ap.ssid,
                    apSelection);
        }
    }

    private static class AccessPoint {
        @NonNull String ssid;
        @NonNull ConnectionConfig config;

        AccessPoint(@NonNull String ssid, @NonNull ConnectionConfig config) {
            this.ssid = ssid;
            this.config = config;
        }

        /**
         * Parse serialized AP information in the form:
         * {
         *     ssid1: config1,
         *     ssid2: config2,
         *     ...
         * }
         */
        static ArrayList<AccessPoint> parseAPs(JSONObject object) {
            ArrayList<AccessPoint> aps = new ArrayList<>();
            if (object == null) return aps;

            Iterator<String> keys = object.keys();
            while (keys.hasNext()) {
                String ssid = keys.next();
                try {
                    JSONObject obj = object.getJSONObject(ssid);
                    ConnectionConfig config = ConnectionConfig.parse(obj);
                    if (config != null) {
                        aps.add(new AccessPoint(ssid, config));
                    }
                } catch (JSONException ignored) {
                }
            }
            return aps;
        }
    }
}
