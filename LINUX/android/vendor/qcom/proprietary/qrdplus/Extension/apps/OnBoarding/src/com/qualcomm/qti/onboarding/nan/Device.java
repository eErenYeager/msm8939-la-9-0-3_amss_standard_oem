/*
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
package com.qualcomm.qti.onboarding.nan;

import android.net.wifi.aware.PeerHandle;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A discovered NAN Configurator/Enrollee device.
 */
public class Device {
    private static final String TAG = "Device";

    public String name;
    public String uri;
    @Nullable public PeerHandle peerHandle;

    public Device(String name, String uri, @Nullable PeerHandle peerHandle) {
        this.name = name;
        this.uri = uri;
        this.peerHandle = peerHandle;
    }

    public static @Nullable Device parse(byte[] message, @Nullable PeerHandle peerHandle) {
        String str = new String(message);
        try {
            JSONObject object = new JSONObject(str);
            String name = object.getString("name");
            String uri = object.getString("uri");
            return new Device(name, uri, peerHandle);
        } catch (JSONException e) {
            return null;
        }
    }

    public String toJsonString() {
        JSONObject object = new JSONObject();
        try {
            object.put("name", name);
            object.put("uri", uri);
        } catch (JSONException e) {
            Log.e(TAG, "toJsonString");
        }
        return object.toString();
    }
}
