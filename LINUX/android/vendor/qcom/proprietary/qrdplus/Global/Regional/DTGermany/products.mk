PRODUCT_PACKAGES += \
    DTGermanyContactsRes \
    DTGermanyDeskClockRes \
    DTGermanyFrameworksRes \
    DTGermanyMmsRes \
    DTGermanySettingsRes \
    DTGermanySettingsProviderRes \
    DTGermanyNetworkSettingRes \
    DTGermanyTeleServiceRes \
    DTGermanyBrowserRes \
    DTGermanySoundRecorderRes
