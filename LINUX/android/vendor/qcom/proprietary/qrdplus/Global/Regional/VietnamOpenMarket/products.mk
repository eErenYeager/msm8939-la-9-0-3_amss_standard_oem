# Add Cherry Mobile Apps
PRODUCT_PACKAGES += \
    VietnamOpenMarketBrowserRes \
    VietnamOpenMarketFrameworksRes \
    VietnamOpenMarketContactsRes \
    VietnamOpenMarketMmsRes \
    VietnamOpenMarketSettingsProviderRes \
    VietnamOpenMarketCellBroadcastReceiverRes \
    VietnamOpenMarketFM2Res \
    VietnamOpenMarketLatinIMERes \
    VietnamOpenMarketSoundRecorderRes \
    VietnamOpenMarketTeleServiceRes \
    VietnamOpenMarketSettingsRes \
    VietnamOpenMarketNetworkSettingRes \
    VietnamOpenMarketPhoneFeaturesRes
