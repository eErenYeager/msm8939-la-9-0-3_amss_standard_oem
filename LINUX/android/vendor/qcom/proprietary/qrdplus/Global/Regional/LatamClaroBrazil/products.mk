# Add LatamClaroBrazil Apps
PRODUCT_PACKAGES += \
    LatamClaroBrazilMmsRes \
    LatamClaroBrazilFrameworksRes \
    LatamClaroBrazilStkRes \
    LatamClaroBrazilBrowserRes \
    LatamClaroBrazilLauncherRes \
    LatamClaroBrazilSimContactsRes \
    LatamClaroBrazilTeleServiceRes \
    LatamClaroBrazilConfigurationClientRes \
    LatamClaroBrazilOmaDownloadRes \
    LatamClaroBrazilTrebuchetRes \
    LatamClaroBrazilEmailRes \
    LatamClaroBrazilSettingsRes
