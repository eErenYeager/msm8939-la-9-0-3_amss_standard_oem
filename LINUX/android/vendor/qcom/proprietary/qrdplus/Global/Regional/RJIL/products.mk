# Add RJIL Apps
PRODUCT_PACKAGES += \
    mcfg_sw.mbn \
    RJILTeleServiceRes \
    RJILSettingsRes \
    RJILSystemUIRes \
    RJILContactsRes \
    RJILProfileMgrRes \
    RJILLauncher3Res
