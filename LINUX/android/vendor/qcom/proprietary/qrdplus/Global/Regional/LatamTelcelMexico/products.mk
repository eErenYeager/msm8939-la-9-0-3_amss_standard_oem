# Add LatamTelcelMexico Apps
PRODUCT_PACKAGES += \
    LatamTelcelMexicoFrameworksRes \
    LatamTelcelMexicoEmailRes \
    LatamTelcelMexicoSettingsRes \
    LatamTelcelMexicoBrowserRes \
    LatamTelcelMexicoDialerRes \
    LatamTelcelMexicoTeleServiceRes \
    LatamTelcelMexicoCalendarRes \
    LatamTelcelMexicoSettingsProviderRes \
    LatamTelcelMexicoMmsRes \
    LatamTelcelMexicoConfigurationClientRes \
    LatamTelcelMexicoOmaDownloadRes \
    LatamTelcelMexicoStkRes \
    LatamTelcelMexicoTelephonyCarrierPackRes \
    LatamTelcelMexicoTrebuchetRes \
    LatamTelcelMexicoLauncherRes \
    LatamTelcelMexicoTelResourcesRes \
    LatamTelcelMexicoZeroBalanceHelperRes

#Product properties
PRODUCT_BRAND := Telcel
PRODUCT_LOCALES := en_US es_US

