/*
 *  Copyright (c) 2017 Qualcomm Technologies, Inc.
 *  All Rights Reserved.
 *  Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

package com.qualcomm.qti.backup_wrapper;

public class ContactsWrapper {
    
    public interface ContactMethodsColumns {
        public static final String MOBILE_EMAIL_TYPE_NAME = "_AUTO_CELL";
    }
}