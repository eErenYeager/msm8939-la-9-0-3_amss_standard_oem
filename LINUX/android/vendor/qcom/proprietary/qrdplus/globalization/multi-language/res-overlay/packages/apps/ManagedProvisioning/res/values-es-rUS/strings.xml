<?xml version="1.0" encoding="utf-8"?>
<!--
/*
 * Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
-->
<!--
/**
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
-->
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
  <!-- General provisioning strings used for both device owner and work profile provisioning -->
  <!-- Title of the provisioning error dialog. [CHAR LIMIT=45] -->
  <string name="provisioning_error_title">"¡Uy!"</string>
  <!-- Work profile provisioning flow UI. -->
  <!-- User consent -->
  <!-- Title of all work profile setup screens. [CHAR LIMIT=NONE] -->
  <string name="setup_work_profile">"Configura tu perfil"</string>
  <!-- The text that asks for user consent to create a work profile [CHAR LIMIT=NONE] -->
  <string name="company_controls_workspace">"Tu organización controla este perfil y lo mantiene seguro. Tú controlas todo lo demás en el dispositivo."</string>
  <!-- The text that asks for user consent to set up a work device [CHAR LIMIT=NONE] -->
  <string name="company_controls_device">"Tu organización controlará este dispositivo y lo mantendrá protegido."</string>
  <!-- String shown above the icon of the app that will control the work profile after it is set up. [CHAR LIMIT=NONE]-->
  <string name="the_following_is_your_mdm">"La siguiente aplicación necesitará acceso a este perfil:"</string>
  <!-- String shown above the icon of the app that will control the work device after it is set up. [CHAR LIMIT=NONE]-->
  <string name="the_following_is_your_mdm_for_device">"La siguiente aplicación administrará tu dispositivo:"</string>
  <!-- String for positive button on user consent dialog for creating a profile [CHAR LIMIT=NONE] -->
  <string name="set_up">"Configurar"</string>
  <!-- Progress text shown when setting up work profile [CHAR LIMIT=NONE] -->
  <string name="setting_up_workspace">"Configurando tu perfil de trabajo\u2026"</string>
  <!-- Text in a pop-up that asks the user to confirm that they allow the mobile device management app to take control of the profile they are creating. [CHAR LIMIT=NONE]-->
  <string name="admin_has_ability_to_monitor_profile">"El administrador puede supervisar y administrar la configuración, el acceso corporativo, las aplicaciones, los permisos y los datos asociados con el perfil, incluidas la actividad de red y la información de ubicación del dispositivo."</string>
  <!-- Text in a pop-up that asks the user to confirm that they allow the mobile device management app to take control of the device. [CHAR LIMIT=NONE]-->
  <string name="admin_has_ability_to_monitor_device">"El administrador puede supervisar y administrar la configuración, el acceso corporativo, las aplicaciones, los permisos y los datos asociados a este dispositivo, incluidas la actividad de red y la información de ubicación del dispositivo."</string>
  <!-- Text in a pop-up that asks the user to confirm that theft protection will be disabled. [CHAR LIMIT=NONE]-->
  <string name="theft_protection_disabled_warning">"Si continúas, no se habilitarán las funciones de protección contra robo. Para activar la protección, cancela esta acción ahora y configura el dispositivo como personal."</string>
  <!-- String telling the user how to get more information about their work profile-->
  <string name="contact_your_admin_for_more_info">"Para obtener más información, como las políticas de privacidad de la organización, comunícate con el administrador."</string>
  <!-- Text on the link that will forward the user to a website with more information about work profiles [CHAR LIMIT=30]-->
  <string name="learn_more_link">"Más información"</string>
  <!-- Text on negative button in a popup shown to the user to confirm that they want to start setting up a work profile [CHAR LIMIT=15]-->
  <string name="cancel_setup">"CANCELAR"</string>
  <!-- Text on positive button in a popup shown to the user to confirm that they want to start setting up a work profile [CHAR LIMIT=15]-->
  <string name="ok_setup">"Aceptar"</string>
  <!-- Default profile name used for the creation of work profiles. [CHAR LIMIT=NONE] -->
  <string name="default_managed_profile_name">"Perfil de trabajo"</string>
  <!-- Title on the dialog that prompts the user to confirm that they really want to delete their existing work profile-->
  <string name="delete_profile_title">"¿Eliminar perfil de trabajo?"</string>
  <!-- Opening string on the dialog that prompts the user to confirm that they really want to delete
       their existing work profile. The administration app icon and name appear after the final
       colon. [CHAR LIMIT=NONE] -->
  <string name="opening_paragraph_delete_profile_unknown_company">"Ya existe un perfil de trabajo que administra:"</string>
    <!-- Opening string on the dialog that prompts the user to confirm that they really want to delete
         their existing work profile. The administration app icon and name appear after the final
         colon, the %s is replaced by the domain name of the organization that owns the work
         profile. [CHAR LIMIT=NONE] -->
  <string name="opening_paragraph_delete_profile_known_company">"La siguiente aplicación administra el perfil de trabajo de %s:"</string>
  <!-- String on the dialog that links through to more information about the profile management application. -->
  <string name="read_more_delete_profile">"Antes de continuar, "<a href="#read_this_link">"lee esta información"</a>"."</string>
  <!-- String on the dialog that prompts the user to confirm that they really want to delete their existing work profile-->
  <string name="sure_you_want_to_delete_profile">"Si continúas, se eliminarán todas las aplicaciones y los datos en este perfil."</string>
  <!-- String on the button that triggers the deletion of a work profile.-->
  <string name="delete_profile">"BORRAR"</string>
  <!-- String on the button that cancels out of the deletion of a work profile.-->
  <string name="cancel_delete_profile">"CANCELAR"</string>
  <!-- String shown on prompt for user to encrypt and reboot the device, in case of profile setup [CHAR LIMIT=NONE]-->
  <string name="encrypt_device_text_for_profile_owner_setup">"Para continuar con la configuración del perfil de trabajo, deberás encriptar el dispositivo. Esto podría tardar un poco."</string>
  <!-- String shown on prompt for user to encrypt and reboot the device, in case of device setup [CHAR LIMIT=NONE]-->
  <string name="encrypt_device_text_for_device_owner_setup">"Para continuar con la configuración del dispositivo, deberás encriptarlo. Este proceso podría tardar un poco."</string>
  <!-- String shown on button for user to cancel setup when asked to encrypt device. [CHAR LIMIT=20]-->
  <string name="encrypt_device_cancel">"CANCELAR"</string>
  <!-- String shown on button for user to continue to settings to encrypt the device. [CHAR LIMIT=20]-->
  <string name="encrypt_device_launch_settings">"ENCRIPTAR"</string>
  <!-- Title for reminder notification to resume provisioning after encryption [CHAR LIMIT=30] -->
  <string name="continue_provisioning_notify_title">"Encriptación completa"</string>
  <!-- Body for reminder notification to resume provisioning after encryption [CHAR LIMIT=NONE] -->
  <string name="continue_provisioning_notify_text">"Toca para continuar configurando tu perfil de trabajo."</string>
  <!-- Explains the failure and what to do to next. [CHAR LIMIT=NONE] -->
  <string name="managed_provisioning_error_text">"No se pudo configurar el perfil de trabajo. Comunícate con el Departamento de TI o vuelve a intentarlo más tarde."</string>
  <!-- Error string displayed if this device doesn't support work profiles. -->
  <string name="managed_provisioning_not_supported">"El dispositivo no admite perfiles de trabajo."</string>
  <!-- Error string displayed if the user that initiated the provisioning is not the user owner. -->
  <string name="user_is_not_owner">"El usuario principal del dispositivo es quien configura los perfiles de trabajo."</string>
  <!-- Error string displayed if provisioning was initiated on a device with a Device Owner -->
  <string name="device_owner_exists">"Los perfiles de trabajo no se pueden configurar en un dispositivo administrado."</string>
  <!-- Error string displayed if maximum user limit is reached -->
  <string name="maximum_user_limit_reached">"No se puede crear el perfil de trabajo porque alcanzaste el número máximo de usuarios en el dispositivo. Quita al menos un usuario y vuelve a intentarlo."</string>
  <!-- Error string displayed if the selected launcher doesn't support work profiles. -->
  <string name="managed_provisioning_not_supported_by_launcher">"Esta aplicación de launcher no admite tu perfil de trabajo. Deberás utilizar un launcher compatible."</string>
  <!-- Button text for the button that cancels provisioning  -->
  <string name="cancel_provisioning">"CANCELAR"</string>
  <!-- Button text for the button that opens the launcher picker  -->
  <string name="pick_launcher">"ACEPTAR"</string>
  <!-- Device owner provisioning flow UI. -->
  <!-- Default username for the user of the owned device. [CHAR LIMIT=45] -->
  <string name="default_owned_device_username">"Usuario de dispositivo de trabajo"</string>
  <!-- Title of the work device setup screen. [CHAR LIMIT=NONE] -->
  <string name="setup_work_device">"Configurar el dispositivo"</string>
  <!-- Progress text indicating that the setup data is processed. [CHAR LIMIT=45] -->
  <string name="progress_data_process">"Procesando los datos de configuración\u2026"</string>
  <!-- Progress text indicating that wifi is set up. [CHAR LIMIT=45] -->
  <string name="progress_connect_to_wifi">"Conectando a Wi-Fi\u2026"</string>
  <!-- Progress text indicating that the device admin package is being downloaded. [CHAR LIMIT=45] -->
  <string name="progress_download">"Descargando la aplicación de administración\u2026"</string>
  <!-- Progress text indicating that the device admin package is being installed. [CHAR LIMIT=45] -->
  <string name="progress_install">"Instalando la aplicación de administración\u2026"</string>
  <!-- Progress text indicating that the device admin package is set as ownder. [CHAR LIMIT=45] -->
  <string name="progress_set_owner">"Configurando propietario del dispositivo\u2026"</string>
  <!-- Message of the cancel dialog. [CHAR LIMIT=NONE] -->
  <string name="device_owner_cancel_message">"¿Quieres detener la configuración y borrar los datos del dispositivo?"</string>
  <!-- Cancel button text of the cancel dialog. [CHAR LIMIT=45] -->
  <string name="device_owner_cancel_cancel">"CANCELAR"</string>
  <!-- OK button text of the error dialog. [CHAR LIMIT=45] -->
  <string name="device_owner_error_ok">"Aceptar"</string>
  <!-- Reset button text of the error dialog. [CHAR LIMIT=45] -->
  <string name="device_owner_error_reset">"REESTABLECER"</string>
  <!-- Message of the error dialog in case of an unspecified error. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_general">"No se pudo configurar el dispositivo. Comunícate con el Departamento de TI."</string>
  <!-- Message of the error dialog when already provisioned. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_already_provisioned">"Este dispositivo ya está configurado."</string>
  <!-- Message of the error dialog when a secondary user has already been provisioned. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_already_provisioned_user">"Este dispositivo ya está configurado para este usuario."</string>
  <!-- Message of the error dialog when setting up wifi failed. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_wifi">"No se pudo conectar a la red Wi-Fi."</string>
  <!-- Message of the error dialog when passing factory reset protection fails. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_frp">"El dispositivo está bloqueado a causa de la protección contra restablecimiento de configuración de fábrica. Comunícate con el departamento de TI."</string>
  <!-- Message informing the user that the factory reset protection partition is being erased [CHAR LIMIT=30] -->
  <string name="frp_clear_progress_title">"Borrando"</string>
  <!-- Progress screen text shown while erasing the factory reset protection partition [CHAR LIMIT=75] -->
  <string name="frp_clear_progress_text">"Espera un momento\u2026"</string>
  <!-- Message of the error dialog when data integrity check fails. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_hash_mismatch">"No se pudo usar la aplicación de administración debido a un error de suma de comprobación. Comunícate con el Departamento de TI."</string>
  <!-- Message of the error dialog when device owner apk could not be downloaded. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_download_failed">"No se pudo descargar la aplicación de administración."</string>
  <!-- Message of the error dialog when package to install is invalid. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_package_invalid">"No se puede usar la aplicación de administración. Hay componentes que faltan o que están dañados. Comunícate con el Departamento de TI."</string>
  <!-- Message of the error dialog when the name of the admin package to be installed is invalid. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_package_name_invalid">"No se puede instalar la aplicación de administración. El nombre del paquete no es válido. Comunícate con el Departamento de TI."</string>
  <!-- Message of the error dialog when package could not be installed. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_installation_failed">"No se pudo instalar la aplicación de administración."</string>
  <!-- Message of the error dialog when device admin package is not installed. [CHAR LIMIT=NONE] -->
  <string name="device_owner_error_package_not_installed">"La aplicación de administración no está instalada en el dispositivo."</string>
  <!-- Message of the cancel dialog. [CHAR LIMIT=NONE] -->
  <string name="profile_owner_cancel_message">"¿Deseas detener la configuración?"</string>
  <!-- Cancel button text of the cancel dialog. [CHAR LIMIT=45] -->
  <string name="profile_owner_cancel_cancel">"NO"</string>
  <!-- OK button text of the error dialog. [CHAR LIMIT=45] -->
  <string name="profile_owner_cancel_ok">"SÍ"</string>
  <!-- Message of the cancel progress dialog. [CHAR LIMIT=45] -->
  <string name="profile_owner_cancelling">"Cancelando\u2026"</string>
  <!-- Title of the work profile setup cancel dialog. [CHAR LIMIT=45] -->
  <string name="work_profile_setup_later_title">"¿Deseas detener la configuración del perfil?"</string>
  <!-- Message of the work profile setup cancel dialog. [CHAR LIMIT=NONE] -->
  <string name="work_profile_setup_later_message">"Puedes configurar tu perfil de trabajo más tarde desde la aplicación de administración del dispositivo que utiliza tu organización."</string>
  <!-- Cancel button text of the work profile setup cancel dialog. [CHAR LIMIT=45] -->
  <string name="work_profile_setup_continue">"CONTINUAR"</string>
  <!-- OK button text of the work profile setup cancel dialog. [CHAR LIMIT=45] -->
  <string name="work_profile_setup_stop">"DETENER"</string>
  <!-- CertService -->
  <!-- Title of a status bar notification for a service which installs CA certificates to secondary users. [CHAR LIMIT=45] -->
  <string name="provisioning">"Aprovisionando"</string>
  <!-- Content of a status bar notification for a service which installs CA certificates to secondary users. [CHAR LIMIT=45] -->
  <string name="copying_certs">"Configuración de certificados de CA"</string>
  <!-- Accessibility Descriptions -->
  <!-- Accessibility description for the profile setup screen showing organization info. [CHAR LIMIT=NONE] -->
  <string name="setup_profile_start_setup">"Configurar tu perfil. Comenzar la configuración"</string>
  <!-- Accessibility description for the profile setup screen showing encryption info. [CHAR LIMIT=NONE] -->
  <string name="setup_profile_encryption">"Configurar tu perfil. Encriptación"</string>
  <!-- Accessibility description for the profile setup screen showing progress. [CHAR LIMIT=NONE] -->
  <string name="setup_profile_progress">"Configurar tu perfil. Mostrando el progreso"</string>
  <!-- Accessibility description for the device setup screen showing company info. [CHAR LIMIT=NONE] -->
  <string name="setup_device_start_setup">"Configurar el dispositivo. Comenzar la configuración"</string>
  <!-- Accessibility description for the device setup screen showing encryption info. [CHAR LIMIT=NONE] -->
  <string name="setup_device_encryption">"Configurar el dispositivo. Encriptación"</string>
  <!-- Accessibility description for the device setup screen showing progress. [CHAR LIMIT=NONE] -->
  <string name="setup_device_progress">"Configurar el dispositivo. Mostrando el progreso"</string>
  <!-- Accessibility description for the learn more link in user consent dialog. [CHAR LIMIT=NONE] -->
  <string name="learn_more_label">"Botón Más información"</string>
  <!-- Accessibility description for the company icon. [CHAR LIMIT=NONE] -->
  <string name="mdm_icon_label">"Ícono de <xliff:g id="ICON_LABEL">%1$s</xliff:g>"</string></resources>