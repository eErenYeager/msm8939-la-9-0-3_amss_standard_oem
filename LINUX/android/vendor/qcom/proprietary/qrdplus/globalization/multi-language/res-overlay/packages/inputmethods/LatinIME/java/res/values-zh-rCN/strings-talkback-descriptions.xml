<?xml version="1.0" encoding="utf-8"?>
<!--
/*
 * Copyright (c) 2015-2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 *
 * Not a Contribution.
 * Apache license notifications and license are retained
 * for attribution purposes only.
 */
-->
<!--
/*
**
** Copyright 2015, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->
<resources xmlns:xliff="urn:oasis:names:tc:xliff:document:1.2">
    <!-- Spoken description to let the user know that when typing in a password, they can plug in a headset in to hear spoken descriptions of the keys they type. [CHAR LIMIT=NONE] -->
    <string name="spoken_use_headphones">"插入耳机可听到输入密码时的按键提示音。"</string>
    <!-- Spoken description for the currently entered text -->
    <string name="spoken_current_text_is">"当前文字为%s"</string>
    <!-- Spoken description when there is no text entered -->
    <string name="spoken_no_text_entered">"未输入文字"</string>
    <!-- Spoken description to let the user know what auto-correction will be performed when a key is pressed. An auto-correction replaces a single word with one or more words. -->
    <string name="spoken_auto_correct">"按<xliff:g id="KEY_NAME">%1$s</xliff:g>键可将<xliff:g id="ORIGINAL_WORD">%2$s</xliff:g>更正为<xliff:g id="CORRECTED_WORD">%3$s</xliff:g>"</string>
    <!-- Spoken description used during obscured (e.g. password) entry to let the user know that auto-correction will be performed when a key is pressed. -->
    <string name="spoken_auto_correct_obscured">"按<xliff:g id="KEY_NAME">%1$s</xliff:g>键可进行自动更正"</string>
    <!-- Spoken description of a suggestion when nothing is specified and the field is blank. -->
    <string name="spoken_empty_suggestion">"没有任何建议"</string>
    <!-- Spoken description for unknown keyboard keys. -->
    <string name="spoken_description_unknown">"未知字符"</string>
    <!-- Spoken description for the "Shift" keyboard key when "Shift" is off. -->
    <string name="spoken_description_shift">"Shift"</string>
    <!-- Spoken description for the "Shift" keyboard key in symbols mode. -->
    <string name="spoken_description_symbols_shift">"更多符号"</string>
    <!-- Spoken description for the "Shift" keyboard key when "Shift" is on. -->
    <string name="spoken_description_shift_shifted">"Shift键"</string>
    <!-- Spoken description for the "Shift" keyboard key in 2nd symbols (a.k.a. symbols shift) mode. -->
    <string name="spoken_description_symbols_shift_shifted">"符号"</string>
    <!-- Spoken description for the "Shift" keyboard key when "Caps lock" is on. -->
    <string name="spoken_description_caps_lock">"Shift键"</string>
    <!-- Spoken description for the "Delete" keyboard key. -->
    <string name="spoken_description_delete">"删除"</string>
    <!-- Spoken description for the "To Symbol" keyboard key. -->
    <string name="spoken_description_to_symbol">"符号"</string>
    <!-- Spoken description for the "To Alpha" keyboard key. -->
    <string name="spoken_description_to_alpha">"字母"</string>
    <!-- Spoken description for the "To Numbers" keyboard key. -->
    <string name="spoken_description_to_numeric">"数字"</string>
    <!-- Spoken description for the "Settings" keyboard key. -->
    <string name="spoken_description_settings">"设置"</string>
    <!-- Spoken description for the "Tab" keyboard key. -->
    <string name="spoken_description_tab">"Tab"</string>
    <!-- Spoken description for the "Space" keyboard key. -->
    <string name="spoken_description_space">"空格"</string>
    <!-- Spoken description for the "Mic" keyboard key. -->
    <string name="spoken_description_mic">"语音输入"</string>
    <!-- Spoken description for the "Emoji" keyboard key. -->
    <string name="spoken_description_emoji">"表情符号"</string>
    <!-- Spoken description for the "Return" keyboard key. -->
    <string name="spoken_description_return">"回车"</string>
    <!-- Spoken description for the "Search" keyboard key. -->
    <string name="spoken_description_search">"搜索"</string>
    <!-- Spoken description for the "U+2022" (BULLET) keyboard key. -->
    <string name="spoken_description_dot">"点"</string>
    <!-- Spoken description for the "Switch language" keyboard key. -->
    <string name="spoken_description_language_switch">"切换语言"</string>
    <!-- Spoken description for the "Next" action keyboard key. -->
    <string name="spoken_description_action_next">"下一个"</string>
    <!-- Spoken description for the "Previous" action keyboard key. -->
    <string name="spoken_description_action_previous">"上一个"</string>
    <!-- Spoken feedback after turning "Shift" mode on. -->
    <string name="spoken_description_shiftmode_on">"已开启Shift模式"</string>
    <!-- Spoken feedback after turning "Caps lock" mode on. -->
    <string name="spoken_description_shiftmode_locked">"已锁定大写模式"</string>
    <!-- Spoken feedback after changing to the symbols keyboard. -->
    <string name="spoken_description_mode_symbol">"符号模式"</string>
    <!-- Spoken feedback after changing to the 2nd symbols (a.k.a. symbols shift) keyboard. -->
    <string name="spoken_description_mode_symbol_shift">"更多符号模式"</string>
    <!-- Spoken feedback after changing to the alphanumeric keyboard. -->
    <string name="spoken_description_mode_alpha">"字母模式"</string>
    <!-- Spoken feedback after changing to the phone dialer keyboard. -->
    <string name="spoken_description_mode_phone">"电话模式"</string>
    <!-- Spoken feedback after changing to the shifted phone dialer (symbols) keyboard. -->
    <string name="spoken_description_mode_phone_shift">"电话符号模式"</string>
    <!-- Spoken feedback when the keyboard is hidden. -->
    <string name="announce_keyboard_hidden">"键盘已隐藏"</string>
    <!-- Spoken feedback when the keyboard mode changes. -->
    <string name="announce_keyboard_mode">"当前显示的是<xliff:g id="KEYBOARD_MODE">%s</xliff:g>键盘"</string>
    <!-- Description of the keyboard mode for entering dates. -->
    <string name="keyboard_mode_date">"日期"</string>
    <!-- Description of the keyboard mode for entering dates and times. -->
    <string name="keyboard_mode_date_time">"日期和时间"</string>
    <!-- Description of the keyboard mode for entering email addresses. -->
    <string name="keyboard_mode_email">"电子邮件地址"</string>
    <!-- Description of the keyboard mode for entering text messages. -->
    <string name="keyboard_mode_im">"消息"</string>
    <!-- Description of the keyboard mode for entering numbers. -->
    <string name="keyboard_mode_number">"数字"</string>
    <!-- Description of the keyboard mode for entering phone numbers. -->
    <string name="keyboard_mode_phone">"电话号码"</string>
    <!-- Description of the keyboard mode for entering generic text. -->
    <string name="keyboard_mode_text">"文字"</string>
    <!-- Description of the keyboard mode for entering times. -->
    <string name="keyboard_mode_time">"时间"</string>
    <!-- Description of the keyboard mode for entering URLs. -->
    <string name="keyboard_mode_url">"网址"</string>
    <!-- Description of the emoji category icon of Recents. -->
    <string name="spoken_descrption_emoji_category_recents">"最近用过"</string>
    <!-- Description of the emoji category icon of People. -->
    <string name="spoken_descrption_emoji_category_people">"人物"</string>
    <!-- Description of the emoji category icon of Objects. -->
    <string name="spoken_descrption_emoji_category_objects">"物件"</string>
    <!-- Description of the emoji category icon of Nature. -->
    <string name="spoken_descrption_emoji_category_nature">"自然"</string>
    <!-- Description of the emoji category icon of Places. -->
    <string name="spoken_descrption_emoji_category_places">"地点"</string>
    <!-- Description of the emoji category icon of Symbols. -->
    <string name="spoken_descrption_emoji_category_symbols">"符号"</string>
    <!-- Description of the emoji category icon of Flags. -->
    <string name="spoken_descrption_emoji_category_flags">"旗"</string>
    <!-- Description of the emoji category icon of Smiley & People. -->
    <string name="spoken_descrption_emoji_category_eight_smiley_people">"笑脸和人物"</string>
    <!-- Description of the emoji category icon of Animals & Nature. -->
    <string name="spoken_descrption_emoji_category_eight_animals_nature">"动物和自然"</string>
    <!-- Description of the emoji category icon of Food & Drink. -->
    <string name="spoken_descrption_emoji_category_eight_food_drink">"食物和饮料"</string>
    <!-- Description of the emoji category icon of Travel & Places. -->
    <string name="spoken_descrption_emoji_category_eight_travel_places">"旅行景点"</string>
    <!-- Description of the emoji category icon of Activity. -->
    <string name="spoken_descrption_emoji_category_eight_activity">"活动"</string>
    <!-- Description of the emoji category icon of Emoticons. -->
    <string name="spoken_descrption_emoji_category_emoticons">"表情图标"</string>
    <!-- Description of an upper case letter of LOWER_LETTER. -->
    <string name="spoken_description_upper_case">"拉丁文大写字母<xliff:g id="LOWER_LETTER">%s</xliff:g>"</string>
    <!-- Spoken description for Unicode code point U+0049: "I" LATIN CAPITAL LETTER I
         Note that depending on locale, the lower-case of this letter is U+0069 or U+0131. -->
    <string name="spoken_letter_0049">"拉丁文大写字母I"</string>
    <!-- Spoken description for Unicode code point U+0130: "İ" LATIN CAPITAL LETTER I WITH DOT ABOVE
         Note that depending on locale, the lower-case of this letter is U+0069 or U+0131. -->
    <string name="spoken_letter_0130">"带上点的拉丁文大写字母I"</string>
    <!-- Spoken description for unknown symbol code point. -->
    <string name="spoken_symbol_unknown">"未知符号"</string>
    <!-- Spoken description for unknown emoji code point. -->
    <string name="spoken_emoji_unknown">"未知表情符号"</string>
    <!-- Spoken description for emoticons ":-!". -->
    <string name="spoken_emoticon_3A_2D_21_20">"无聊"</string>
    <!-- Spoken description for emoticons ":-$". -->
    <string name="spoken_emoticon_3A_2D_24_20">"尴尬"</string>
    <!-- Spoken description for emoticons "B-)". -->
    <string name="spoken_emoticon_42_2D_29_20">"戴墨镜"</string>
    <!-- Spoken description for emoticons ":O". -->
    <string name="spoken_emoticon_3A_4F_20">"惊讶"</string>
    <!-- Spoken description for emoticons ":-*". -->
    <string name="spoken_emoticon_3A_2D_2A_20">"亲吻"</string>
    <!-- Spoken description for emoticons ":-[". -->
    <string name="spoken_emoticon_3A_2D_5B_20">"皱眉"</string>
    <!-- Spoken descriptions when opening a more keys keyboard that has alternative characters. -->
    <string name="spoken_open_more_keys_keyboard">"有可用的替代字符"</string>
    <!-- Spoken descriptions when closing a more keys keyboard that has alternative characters. -->
    <string name="spoken_close_more_keys_keyboard">"已关闭替代字符"</string>
    <!-- Spoken descriptions when opening a more suggestions panel that has alternative suggested words. -->
    <string name="spoken_open_more_suggestions">"有可用的其他建议字词"</string>
    <!-- Spoken descriptions when closing a more suggestions panel that has alternative suggested words. -->
    <string name="spoken_close_more_suggestions">"已关闭其他建议字词"</string></resources>