
#
# DO NOT EDIT
#
# AUTO GENERATED FILE FROM CONTEXTUAL ANALYSIS AND EXTRACTED DWARF STRUCTURE DATA
#

# Copyright (c) 2017 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.

import mpgen_mpdb

class Entry():
	def __init__(self, mp_type, name):
		self.mp_type = mp_type
		self.name = name
		return

class Catalog(list):
	def __init__(self):
		list.__init__(self)
		return

	def ro_symbol(self, symbol_name):
		el = Entry(mpgen_mpdb.RD_ONLY, symbol_name)
		self.append(el)
		return

	def write_once_known(self, symbol_name, value):
		el = Entry(mpgen_mpdb.WR_ONCE_KNOWN, symbol_name)
		self.append(el)
		return

	def write_once_unknown(self, symbol_name):
		el = Entry(mpgen_mpdb.WR_ONCE_UNKNOWN, symbol_name)
		self.append(el)
		return

	def auth_writer(self, symbol_name, writer):
		el = Entry(mpgen_mpdb.WR_AUTH_WRITER, symbol_name)
		el.writer = writer
		self.append(el)
		return

	def kconfig(self, name):
		el = Entry(mpgen_mpdb.KCONFIG, name)
		self.append(el)
		return

def get_catalog():
	cat = Catalog()
	cat.ro_symbol('linux_banner')
	cat.ro_symbol('linux_proc_banner')

	# cat.write_once_known('selinux_enforcing', '1')
	cat.write_once_known('linux_proc_banner', '1')

	# cat.write_once_unknown('selinux_enforcing')
	# cat.write_once_unknown('security_ops') this symbol is missing in 845
	cat.write_once_unknown('selinux_hooks')

	cat.auth_writer('selinux_enforcing', 'selinux')

	cat.kconfig('CONFIG_ARM64_ICACHE_DISABLE')
	cat.kconfig('CONFIG_ARM64_DCACHE_DISABLE')
	cat.kconfig('CONFIG_FTRACE')
	cat.kconfig('CONFIG_KPROBES')
	cat.kconfig('CONFIG_ARM64')
	cat.kconfig('CONFIG_SYSTEM_TRUSTED_KEYS')
	cat.kconfig('CONFIG_DM_VERITY')
	cat.kconfig('CONFIG_DM_ANDROID_VERITY')
	cat.kconfig('CONFIG_DM_VERITY_FEC')
	cat.kconfig('CONFIG_DM_VERITY_AVB')
	cat.kconfig('CONFIG_IMA')
	cat.kconfig('CONFIG_SECURITY')
	cat.kconfig('CONFIG_INTEGRITY')

	cat.kconfig('CONFIG_SECURITYFS')
	cat.kconfig('CONFIG_EVM')
	cat.kconfig('CONFIG_MODULES')
	cat.kconfig('CONFIG_')

	return cat

