l1_file_name = /local/mnt/workspace/lnxbuild/project/trees_in_use/free_tree_platform_manifest_LA.BR.1.2.9.1_rb1_msm8916_32_HY11_commander_34389959/checkout/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Encryption/Unified/default/l1_key.bin
l2_file_name = /local/mnt/workspace/lnxbuild/project/trees_in_use/free_tree_platform_manifest_LA.BR.1.2.9.1_rb1_msm8916_32_HY11_commander_34389959/checkout/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Encryption/Unified/default/l2_key.bin
l3_file_name = /local/mnt/workspace/lnxbuild/project/trees_in_use/free_tree_platform_manifest_LA.BR.1.2.9.1_rb1_msm8916_32_HY11_commander_34389959/checkout/vendor/qcom/proprietary/common/scripts/SecImage/resources/data_prov_assets/Encryption/Unified/default/l3_key.bin
Clear L1 key, clear L2 key, and clear L3 keys were provided locally.
image is stored at /local/mnt/workspace/lnxbuild/project/trees_in_use/free_tree_platform_manifest_LA.BR.1.2.9.1_rb1_msm8916_32_HY11_commander_34389959/checkout/vendor/qcom/proprietary/common/scripts/SecImage/signed/integrity_check/default/appsbl/emmc_appsboot.mbn
Clear L1 key, clear L2 key, and clear L3 keys were provided locally.
Image /local/mnt/workspace/lnxbuild/project/trees_in_use/free_tree_platform_manifest_LA.BR.1.2.9.1_rb1_msm8916_32_HY11_commander_34389959/checkout/vendor/qcom/proprietary/common/scripts/SecImage/signed/integrity_check/default/appsbl/emmc_appsboot.mbn is not signed
Image /local/mnt/workspace/lnxbuild/project/trees_in_use/free_tree_platform_manifest_LA.BR.1.2.9.1_rb1_msm8916_32_HY11_commander_34389959/checkout/vendor/qcom/proprietary/common/scripts/SecImage/signed/integrity_check/default/appsbl/emmc_appsboot.mbn is not encrypted

Base Properties: 
| Integrity Check                 | True  |
| Signed                          | False |
| Encrypted                       | False |
| Size of signature               | 256   |
| Size of one cert                | 2048  |
| Num of certs in cert chain      | 3     |
| Number of root certs            | 1     |
| Hash Page Segments as segments  | False |
| Cert chain size                 | 6144  |

ELF Properties: 
Elf Header: 
| Magic                      | ELF                           |
| Class                      | ELF32                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | Advanced RISC Machines ARM     |
| Version                    | 0x1                            |
| Entry address              | 0x8f600000                     |
| Program headers offset     | 0x00000034                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x05000200                     |
| ELF header size            | 52                             |
| Program headers size       | 32                             |
| Number of program headers  | 6                              |
| Section headers size       | 40                             |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Elf Program Headers: 
| S.No |    Type    | Offset | VirtAddr | PhysAddr | FileSize | MemSize | Flags | Align |
|------|------------|--------|----------|----------|----------|---------|-------|-------|
|  1   |    LOAD    |0x02000 |0x00000000|0x8f5ff000| 0x000f4  | 0x000f4 |  0x4  | 0x1000|
|  2   |    LOAD    |0x03000 |0x8f600000|0x8f600000| 0x4664c  | 0x4664c |  0x5  | 0x1000|
|  3   |    LOAD    |0x4964c |0x8f64664c|0x8f64664c| 0x31efc  | 0x31efc |  0x6  | 0x1000|
|  4   |    LOAD    |0x7e000 |0x8f67c000|0x8f67c000| 0x00000  | 0x0e838 |  0x6  | 0x4000|
|  5   | 1685382481 |0x02000 |0x00000000|0x00000000| 0x00000  | 0x00000 |  0x6  | 0x0   |
|  6   | 1879048193 |0x5b414 |0x8f658414|0x8f658414| 0x00028  | 0x00028 |  0x4  | 0x4   |

Hash Segment Properties: 
| Header Size  | 40B  |

Header: 
| cert_chain_ptr  | 0x8f68b128  |
| cert_chain_size | 0x00000000  |
| code_size       | 0x00000100  |
| flash_parti_ver | 0x00000003  |
| image_dest_ptr  | 0x8f68b028  |
| image_id        | 0x00000000  |
| image_size      | 0x00000100  |
| image_src       | 0x00000000  |
| sig_ptr         | 0x8f68b128  |
| sig_size        | 0x00000000  |

SecElf Properties: 
| image_type        | 0     |
| max_elf_segments  | 100   |
| testsig_serialnum | None  |

