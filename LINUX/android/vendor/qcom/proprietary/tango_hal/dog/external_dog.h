/*============================================================================
Copyright (c) 2015-2017 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
============================================================================*/
#ifndef _EXTERNAL_DOG_H
#define _EXTERNAL_DOG_H
#include "AEEStdDef.h"
#ifndef __QAIC_HEADER
#define __QAIC_HEADER(ff) ff
#endif //__QAIC_HEADER

#ifndef __QAIC_HEADER_EXPORT
#define __QAIC_HEADER_EXPORT
#endif // __QAIC_HEADER_EXPORT

#ifndef __QAIC_HEADER_ATTRIBUTE
#define __QAIC_HEADER_ATTRIBUTE
#endif // __QAIC_HEADER_ATTRIBUTE

#ifndef __QAIC_IMPL
#define __QAIC_IMPL(ff) ff
#endif //__QAIC_IMPL

#ifndef __QAIC_IMPL_EXPORT
#define __QAIC_IMPL_EXPORT
#endif // __QAIC_IMPL_EXPORT

#ifndef __QAIC_IMPL_ATTRIBUTE
#define __QAIC_IMPL_ATTRIBUTE
#endif // __QAIC_IMPL_ATTRIBUTE
#ifdef __cplusplus
extern "C" {
#endif
typedef struct external_dog_Position external_dog_Position;
struct external_dog_Position {
   float _position[2];
};
typedef struct external_dog_Descriptor external_dog_Descriptor;
struct external_dog_Descriptor {
   uint64 _descriptor[8];
};
typedef struct _external_dog_PositionSequence__seq_external_dog_Position _external_dog_PositionSequence__seq_external_dog_Position;
typedef _external_dog_PositionSequence__seq_external_dog_Position external_dog_PositionSequence;
struct _external_dog_PositionSequence__seq_external_dog_Position {
   external_dog_Position* data;
   int dataLen;
};
typedef struct _external_dog_DescriptorSequence__seq_external_dog_Descriptor _external_dog_DescriptorSequence__seq_external_dog_Descriptor;
typedef _external_dog_DescriptorSequence__seq_external_dog_Descriptor external_dog_DescriptorSequence;
struct _external_dog_DescriptorSequence__seq_external_dog_Descriptor {
   external_dog_Descriptor* data;
   int dataLen;
};
__QAIC_HEADER_EXPORT uint32 __QAIC_HEADER(external_dog_init_with_lift)(uint32 width, uint32 height, const float* camera_calibration, int camera_calibrationLen, uint32* context) __QAIC_HEADER_ATTRIBUTE;
__QAIC_HEADER_EXPORT uint32 __QAIC_HEADER(external_dog_init_with_lift_attributes)(uint32 width, uint32 height, const float* camera_calibration, int camera_calibrationLen, float laplaceThreshold, float edgeScoreThreshold, uint32 speedMode, uint32 dcvsDisable, uint32* context) __QAIC_HEADER_ATTRIBUTE;
__QAIC_HEADER_EXPORT uint32 __QAIC_HEADER(external_dog_run_with_lift)(uint32 context, uint32 width, uint32 height, uint32 stride, const unsigned char* pixels, int pixelsLen, uint32* feature_count, external_dog_Position* positions, int positionsLen, float* scores, int scoresLen, float* scales, int scalesLen, float* edge_scores, int edge_scoresLen, uint32* is_minimum, int is_minimumLen, uint32* descriptor_count, external_dog_Descriptor* descriptors, int descriptorsLen, float* angles, int anglesLen) __QAIC_HEADER_ATTRIBUTE;
__QAIC_HEADER_EXPORT uint32 __QAIC_HEADER(external_dog_lift_deinit)(uint32 context) __QAIC_HEADER_ATTRIBUTE;
#ifdef __cplusplus
}
#endif
#endif //_EXTERNAL_DOG_H
