/* ---------------------------------------------------------------------------
Copyright (c) 2017 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
--------------------------------------------------------------------------- */
#ifndef VENDOR_QTI_HARDWARE_RADIO_UIM_REMOTE_CLIENT_V1_0_H
#define VENDOR_QTI_HARDWARE_RADIO_UIM_REMOTE_CLIENT_V1_0_H

#include <vendor/qti/hardware/radio/uim_remote_client/1.0/IUimRemoteServiceClient.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <ril.h>
#include <utils/Mutex.h>

extern "C" {
  #include "qcril_uim_remote.h"
}

using ::android::hardware::hidl_death_recipient;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::sp;
using ::android::wp;
using ::android::Mutex;

namespace vendor {
namespace qti {
namespace hardware {
namespace radio {
namespace uim_remote_client {
namespace V1_0 {
namespace implementation {

class UimRemoteClientIndicationDeathRecipient;
class UimRemoteClientResponseDeathRecipient;

class UimRemoteClientImpl : public IUimRemoteServiceClient {
  sp<IUimRemoteServiceClientResponse> mResponseCb;
  sp<IUimRemoteServiceClientIndication> mIndicationCb;
  sp<UimRemoteClientIndicationDeathRecipient> mIndicationDeathRecipient;
  sp<UimRemoteClientResponseDeathRecipient> mResponseDeathRecipient;
  int mInstanceId;
  Mutex mIndicationLock;
  Mutex mResponseLock;


  // Methods from IUimRemoteServiceClientResponse
  Return<void> setCallback(const sp<IUimRemoteServiceClientResponse>& responseCallback, const sp<IUimRemoteServiceClientIndication>& indicationCallback);
  Return<void> UimRemoteServiceClientEventReq(int32_t token, const UimRemoteEventReqType& eventReq);
  Return<void> UimRemoteServiceClientApduReq(int32_t token, UimRemoteClientApduStatus apduStatus, const hidl_vec<uint8_t>& apduResponse);

public:
  UimRemoteClientImpl();
  void resetIndicationCallback();
  void resetResponseCallback();
  void setInstanceId(int instanceId);
  void uimRemoteClientEventResponse(int32_t token, qcril_uim_remote_event_resp_type eventResp);
  void uimRemoteClientApduResponse(int32_t token, qcril_uim_remote_apdu_resp_type apduResp);
  void uimRemoteClientSendApduInd(uint32_t apduLen, uint8_t * apduPtr);
  void uimRemoteClientConnectInd();
  void uimRemoteClientDisconnectInd();
  void uimRemoteClientPowerUpInd(bool hasTimeOut,
  int32_t timeOut,
  bool hasVoltageClass,
  qcril_uim_remote_voltage_class_type voltageClass);
  void uimRemoteClientPowerDownInd(bool hasMode, qcril_uim_remote_power_down_mode mode);
  void uimRemoteClientResetInd();
};

class UimRemoteClientIndicationDeathRecipient : public hidl_death_recipient {
public:
  UimRemoteClientIndicationDeathRecipient(const sp<UimRemoteClientImpl> uimRemoteClientImpl) : mUimRemoteClientImpl(uimRemoteClientImpl) {}
  virtual void serviceDied(uint64_t /*cookie*/, const wp<::android::hidl::base::V1_0::IBase>& /*who*/)
  {
    mUimRemoteClientImpl->resetIndicationCallback();
  }
  sp<UimRemoteClientImpl> mUimRemoteClientImpl;
};

class UimRemoteClientResponseDeathRecipient : public hidl_death_recipient {
public:
  UimRemoteClientResponseDeathRecipient(const sp<UimRemoteClientImpl> uimRemoteClientImpl) : mUimRemoteClientImpl(uimRemoteClientImpl) {}
  virtual void serviceDied(uint64_t /*cookie*/, const wp<::android::hidl::base::V1_0::IBase>& /*who*/)
  {
    mUimRemoteClientImpl->resetResponseCallback();
  }
  sp<UimRemoteClientImpl> mUimRemoteClientImpl;
};

}  // namespace implementation
}  // namespace V1_0
}  // namespace uim_remote_client
}  // namespace radio
}  // namespace hardware
}  // namespace qti
}  // namespace vendor

#endif  // VENDOR_QTI_HARDWARE_RADIO_UIM_REMOTE_CLIENT_V1_0_H
