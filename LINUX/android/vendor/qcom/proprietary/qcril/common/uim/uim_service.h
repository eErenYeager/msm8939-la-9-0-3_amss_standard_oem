/******************************************************************************
@file    uim_service.h
@brief   qcril uim service

DESCRIPTION
Implements the server side of the IUim interface.
Handles RIL requests and responses and indications to be received
and sent to client respectively

---------------------------------------------------------------------------

Copyright (c) 2017 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
---------------------------------------------------------------------------
******************************************************************************/
#ifndef VENDOR_QTI_HARDWARE_RADIO_UIM_V1_0_H
#define VENDOR_QTI_HARDWARE_RADIO_UIM_V1_0_H

#include <vendor/qti/hardware/radio/uim/1.0/IUim.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <ril.h>
#include <utils/Mutex.h>

using ::android::hardware::hidl_death_recipient;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::sp;
using ::android::wp;
using ::android::Mutex;

namespace vendor {
namespace qti {
namespace hardware {
namespace radio {
namespace uim {
namespace V1_0 {
namespace implementation {

class UimIndicationDeathRecipient;
class UimResponseDeathRecipient;

class UimImpl : public IUim {
  sp<IUimResponse> mResponseCb;
  sp<IUimIndication> mIndicationCb;
  sp<UimIndicationDeathRecipient> mIndicationDeathRecipient;
  sp<UimResponseDeathRecipient> mResponseDeathRecipient;
  int mInstanceId;
  Mutex mIndicationLock;
  Mutex mResponseLock;

  // Methods from IUimResponse
  Return<void> setCallback(const sp<IUimResponse>& responseCallback, const sp<IUimIndication>& indicationCallback);
  Return<void> UimRemoteSimlockRequest(int32_t token, UimRemoteSimlockOperationType simlockOp, const hidl_vec<uint8_t>& simlockData);

public:
  UimImpl();
  void resetIndicationCallback();
  void resetResponseCallback();
  void setInstanceId(int instanceId);
  void uimRemoteSimlockResponse(int token, UimRemoteSimlockResponseType resp, UimRemoteSimlockOperationType op_type,
    uint8_t * rsp_ptr, uint32_t rsp_len, UimRemoteSimlockVersion version, UimRemoteSimlockStatus status);
};

class UimIndicationDeathRecipient : public hidl_death_recipient {
public:
  UimIndicationDeathRecipient(const sp<UimImpl> uimImpl) : mUimImpl(uimImpl) {}
  virtual void serviceDied(uint64_t /*cookie*/, const wp<::android::hidl::base::V1_0::IBase>& /*who*/)
  {
    mUimImpl->resetIndicationCallback();
  }
  sp<UimImpl> mUimImpl;
};

class UimResponseDeathRecipient : public hidl_death_recipient {
public:
  UimResponseDeathRecipient(const sp<UimImpl> uimImpl) : mUimImpl(uimImpl) {}
  virtual void serviceDied(uint64_t /*cookie*/, const wp<::android::hidl::base::V1_0::IBase>& /*who*/)
  {
    mUimImpl->resetResponseCallback();
  }
  sp<UimImpl> mUimImpl;
};

}  // namespace implementation
}  // namespace V1_0
}  // namespace uim
}  // namespace radio
}  // namespace hardware
}  // namespace qti
}  // namespace vendor

#endif  // VENDOR_QTI_HARDWARE_RADIO_UIM_V1_0_H
