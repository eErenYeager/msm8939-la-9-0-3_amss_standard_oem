/******************************************************************************
  @file    qcril_qmi_qtiRadioService_service.c
  @brief   qcril qmi - qtiRadioService_service

  DESCRIPTION
    Implements the server side of the IQtiqtiRadioService interface. Handles RIL
    requests and responses and indications to be received and sent to client
    respectively

  ---------------------------------------------------------------------------

  Copyright (c) 2017 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
  ---------------------------------------------------------------------------
******************************************************************************/

#include "qcril_qmi_qti_radio_service.h"

extern "C" {
    #include "qcril_log.h"
}

using namespace vendor::qti::hardware::radio::qtiradio::V1_0::implementation;
using ::android::hardware::Void;

namespace vendor {
namespace qti {
namespace hardware {
namespace radio {
namespace qtiradio {
namespace V1_0 {
namespace implementation {

static sp<QtiRadioImpl> qtiRadioService = NULL;

QtiRadioImpl::QtiRadioImpl() {
    mResponseCb = NULL;
    mIndicationCb = NULL;
    mIndicationDeathRecipient = NULL;
    mResponseDeathRecipient = NULL;
}

//===========================================================================
// QtiRadioImpl::setCallback
//
//===========================================================================
//===========================================================================
/*!
    @brief
    Registers the callbacks for IQtiRadioResponse and IQtiRadioIndication
    passed by the client as a parameter

    @return
    None
*/
/*=========================================================================*/
Return<void> QtiRadioImpl::setCallback(const sp<IQtiRadioResponse>& responseCallback,
        const sp<IQtiRadioIndication>& indicationCallback) {
    QCRIL_LOG_INFO("QtiRadioImpl::setCallback");
    mIndicationDeathRecipient = new QtiRadioIndicationDeathRecipient(this);
    mResponseDeathRecipient = new QtiRadioResponseDeathRecipient(this);
   {
        Mutex::Autolock lock(mIndicationLock);
        mIndicationCb = indicationCallback;
        mIndicationCb->linkToDeath(mIndicationDeathRecipient, 0);
    }
    {
        Mutex::Autolock lock(mResponseLock);
        mResponseCb = responseCallback;
        mResponseCb->linkToDeath(mResponseDeathRecipient, 0);
    }
    return Void();
}


//===========================================================================
// QtiRadioImpl::getAtr
//
//===========================================================================
//===========================================================================
/*!
    @brief
    Processes the getAtr request sent by client

    @return
    None
*/
/*=========================================================================*/
Return<void> QtiRadioImpl::getAtr(int32_t serial) {

    qcril_request_params_type param;
    qcril_request_return_type ret_ptr;
    int slot = mInstanceId;
    QCRIL_LOG_ERROR("QtiRadioImpl::getAtr serial=%d mInstanceId = %d" ,serial, mInstanceId);
    RIL_Token token = qcril_qmi_qti_convert_radio_token_to_ril_token(serial/*request token*/);
    param.instance_id = mInstanceId;
    param.t           = (void *)token;
    param.modem_id    = QCRIL_DEFAULT_MODEM_ID;
    param.data = &slot;
    qcril_uim_request_get_atr(&param, &ret_ptr);
    return Void();
}

void QtiRadioImpl::getAtrResponse(RIL_Token token, RIL_Errno error, char *buf, int bufLen) {

    if (mResponseCb != NULL) {
        QtiRadioResponseInfo responseInfo;
        hidl_string data;
        QCRIL_LOG_ERROR("QtiRadioImpl::getAtrResponse ");
        int serial = qcril_qmi_qti_free_and_convert_ril_token_to_radio_token(token);
        responseInfo.serial = serial;
        responseInfo.type = QtiRadioResponseType::SOLICITED;
        responseInfo.error = (QtiRadioError) error;
        if (buf != NULL) {
            data.setToExternal(buf, bufLen);
        }
        mResponseCb->getAtrResponse(responseInfo, data);
    } else {
        QCRIL_LOG_ERROR("QtiRadioImpl::getAtrResponse mResponseCb NULL");
    }
}

void QtiRadioImpl::sendQtiIndication() {
}

/*
 * Resets indication callback upon client's death
 */
void QtiRadioImpl::resetIndicationCallback() {
    QCRIL_LOG_ERROR("QtiRadioImpl::resetIndicationCallback Client died");
    Mutex::Autolock lock(mIndicationLock);
    mIndicationDeathRecipient = NULL;
    mIndicationCb = NULL;
}

/*
 * Resets response callback upon client's death
 */
void QtiRadioImpl::resetResponseCallback() {
    QCRIL_LOG_ERROR("QtiRadioImpl::resetResponsCallback Client died");
    Mutex::Autolock lock(mResponseLock);
    mResponseDeathRecipient = NULL;
    mResponseCb = NULL;
}

/*
 * Sets instance id
 */
void QtiRadioImpl::setInstanceId(qcril_instance_id_e_type instanceId) {
    mInstanceId = instanceId;
}



/*
 * Register qtiRadioService service with Service Manager
 */
extern "C" void qcril_qmi_qti_radio_service_init() {
    android::status_t ret = android::OK;
    qcril_instance_id_e_type instanceId = qmi_ril_get_process_instance_id();
    QCRIL_LOG_ERROR("qcril_qmi_qti_radio_service_init");
    qtiRadioService = new QtiRadioImpl();
    qtiRadioService->setInstanceId(instanceId);
    std::string serviceName = "slot";

    QCRIL_LOG_INFO("qcril_qmi_qti_radio_service_init adding for slot %d", instanceId);
    ret = qtiRadioService->registerAsService(serviceName + std::to_string(instanceId + 1));
    QCRIL_LOG_INFO("qtiRadioServiceRegisterService instanceId=%d status=%d", instanceId, ret);
    //return ret == android::OK;
}

extern "C" void qtiGetAtrResponse(RIL_Token token, RIL_Errno error, char *buf, int bufLen) {
    qtiRadioService->getAtrResponse(token, error, buf, bufLen);
}

RIL_Token qcril_qmi_qti_convert_radio_token_to_ril_token(uint32_t oem_token)
{
    RIL_Token ret = qcril_malloc(sizeof(uint32_t));
    if (NULL != ret)
    {
        uint32_t *tmp = (uint32_t*) ret;
        *tmp = oem_token ^ 0xc0000000;
    }
    return ret;
} // qcril_qmi_oem_convert_oem_token_to_ril_token

uint32_t qcril_qmi_qti_free_and_convert_ril_token_to_radio_token(RIL_Token ril_token)
{
    uint32_t ret = 0xFFFFFFFF;
    if (ril_token)
    {
        ret = (*((uint32_t *) ril_token)) ^ 0xc0000000;
        QCRIL_LOG_INFO("oem token: %d", ret);
        qcril_free((void*) ril_token);
    }
    else
    {
        QCRIL_LOG_ERROR("ril_token is NULL");
    }

  return ret;
} // qcril_qmi_oem_free_and_convert_ril_token_to_oem_token

} // namespace implementation
} // namespace V1_0
} // namespace qtiradio
} // namespace radio
} // namespace hardware
} // namespace qti
} //namespace vendor
