/******************************************************************************
 * @file    IUimRemoteSimlockService.aidl
 * @brief   This interface provides the APIs for remote simlock
 *
 * @version 00.00.01
 *
 * ---------------------------------------------------------------------------
 * Copyright (c) 2017 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 * ---------------------------------------------------------------------------
 *
 ******************************************************************************/

package com.qualcomm.qti.remoteSimlock;

import com.qualcomm.qti.remoteSimlock.IUimRemoteSimlockServiceCallback;

interface IUimRemoteSimlockService {
    /**
     * registerCallback will be used by a client to register a callback to be
     * notified asynchronously
     *
     * @param cb
     *    Defines the callback interface
     *
     * @return
     *    UIM_REMOTE_SIMLOCK_SUCCESS       = 0;
     *    UIM_REMOTE_SIMLOCK_ERROR         = 1;
     */
    int registerCallback(in IUimRemoteSimlockServiceCallback cb);

    /**
     * deregisterCallback will be used by a client to deregister a callback to
     * be notified asynchronously
     *
     * @param cb
     *    Defines the callback interface
     *
     * @return
     *    UIM_REMOTE_SIMLOCK_SUCCESS       = 0;
     *    UIM_REMOTE_SIMLOCK_ERROR         = 1;
     */
    int deregisterCallback(in IUimRemoteSimlockServiceCallback cb);

    /**
     * uimRemoteSimlockProcessSimlockData Send lock/unlock blob to SIMLock engine
     *
     * @param token
     *
     * @param simlockData
     *    The byte array representing simlock blob for lock and unlock.
     *
     * @return immediate return error.
     *    UIM_REMOTE_SIMLOCK_SUCCESS       = 0;
     *    UIM_REMOTE_SIMLOCK_ERROR         = 1;
     */
    int uimRemoteSimlockProcessSimlockData(in int token, in byte[] simlockData);

    /**
     * uimRemoteSimlockGetSharedKey to get encrypted symmetric key from SIMLock engine
     *
     * @param token
     *
     * @return immediate return error.
     *    UIM_REMOTE_SIMLOCK_SUCCESS       = 0;
     *    UIM_REMOTE_SIMLOCK_ERROR         = 1;
     */
    int uimRemoteSimlockGetSharedKey(in int token);

    /**
     * uimRemoteSimlockGenerateHMAC request to SIMLock engine to generate and return
     * HMAC on the input data
     *
     * @param token
     *
     * @parma data
     *    Input data for which HMAC needs to be generated
     *
     * @return immediate return error.
     *    UIM_REMOTE_SIMLOCK_SUCCESS       = 0;
     *    UIM_REMOTE_SIMLOCK_ERROR         = 1;
     */
    int uimRemoteSimlockGenerateHMAC(in int token, in byte[] data);

    /**
     * uimRemoteSimlockGetVersion to get highest supported major/minor version of
     * incoming blob that is supported by SIMLock engine
     *
     * @param token
     *
     * @return immediate return error.
     *    UIM_REMOTE_SIMLOCK_SUCCESS       = 0;
     *    UIM_REMOTE_SIMLOCK_ERROR         = 1;
     */
    int uimRemoteSimlockGetVersion(in int token);

    /**
     * uimRemoteSimlockGetSimlockStatus to get current SIMLock status
     *
     * @param token
     *
     * @return immediate return error.
     *    UIM_REMOTE_SIMLOCK_SUCCESS       = 0;
     *    UIM_REMOTE_SIMLOCK_ERROR         = 1;
     */
    int uimRemoteSimlockGetSimlockStatus(in int token);
}
