/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
  Copyright (c) 2015-2017 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
#define LOG_TAG "LocSvc_HIDL_RilInfoMonitor_jni"
#define LOG_NDEBUG 0

#include "JNIHelp.h"
#include "jni.h"
#include "utils_jni.h"
#include <utils/Log.h>

#ifdef LOC_HIDL_VERSION_1_1
#include <vendor/qti/gnss/1.1/ILocHidlGnss.h>
#include <vendor/qti/gnss/1.1/ILocHidlRilInfoMonitor.h>
#else
#include <vendor/qti/gnss/1.0/ILocHidlGnss.h>
#include <vendor/qti/gnss/1.0/ILocHidlRilInfoMonitor.h>
#endif

#ifdef LOC_HIDL_VERSION_1_1
using ::vendor::qti::gnss::V1_1::ILocHidlGnss;
using ::vendor::qti::gnss::V1_1::ILocHidlRilInfoMonitor;
#else
using ::vendor::qti::gnss::V1_0::ILocHidlGnss;
using ::vendor::qti::gnss::V1_0::ILocHidlRilInfoMonitor;
#endif
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

static jobject jRilInfoMonitor = NULL;

/* ===================================================================
 *   HIDL Interface
 * ===================================================================*/
static sp<ILocHidlGnss> gnssVendorHal = nullptr;
static sp<ILocHidlRilInfoMonitor> gnssRilInfoMonitorIface = nullptr;

static void classInit(JNIEnv* /*env*/, jclass /*clazz*/) {

    ALOGD("%s", __FUNCTION__);

    // HIDL Service
    gnssVendorHal = ILocHidlGnss::getService(GNSS_VENDOR_SERVICE_INSTANCE);
    if (gnssVendorHal == nullptr) {
        ALOGE("Unable to get GnssVendor service\n");
        return;
    }
    ALOGD("Got GnssVendor Service.");

#ifdef LOC_HIDL_VERSION_1_1
    auto ril = gnssVendorHal->getExtensionLocHidlRilInfoMonitor_1_1();
#else
    auto ril = gnssVendorHal->getExtensionLocHidlRilInfoMonitor();
#endif
    if (!ril.isOk()) {
        ALOGE("Unable to get a handle to RilInfoMonitor extension !");
    } else {
        gnssRilInfoMonitorIface = ril;
        ALOGD("Got RilInfoMonitor Extension.");
    }
}

static void instanceInit(JNIEnv *env, jobject obj) {

    ALOGD("%s", __FUNCTION__);

    if (NULL == jRilInfoMonitor) {
        jRilInfoMonitor = env->NewGlobalRef(obj);
    }

    if (gnssRilInfoMonitorIface == nullptr) {
        ALOGE("gnssRilInfoMonitorIface null !");
        return;
    }
    TO_HIDL_SERVICE();
    auto r = gnssRilInfoMonitorIface->init();
    if (!r.isOk()) {
        ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
    }
}

static void cinfoInject(JNIEnv* /*env*/, jobject /*obj*/,
                        jint cid, jint lac, jint mnc,
                        jint mcc, jboolean roaming) {

    ALOGD("%s", __FUNCTION__);

    if (gnssRilInfoMonitorIface == nullptr) {
        ALOGE("gnssRilInfoMonitorIface null !");
        return;
    }
    TO_HIDL_SERVICE();
    auto r = gnssRilInfoMonitorIface->cinfoInject(cid, lac, mnc, mcc, roaming);
    if (!r.isOk()) {
        ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
    }
}

static void oosInform(JNIEnv* /*env*/, jobject /*obj*/) {

    ALOGD("%s", __FUNCTION__);

    if (gnssRilInfoMonitorIface == nullptr) {
        ALOGE("gnssRilInfoMonitorIface null !");
        return;
    }
    TO_HIDL_SERVICE();
    auto r = gnssRilInfoMonitorIface->oosInform();
    if (!r.isOk()) {
        ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
    }
}

static void niSuplInit(JNIEnv* env, jobject /*obj*/,
                       jbyteArray data, jint length) {

    ALOGD("%s", __FUNCTION__);

    if (gnssRilInfoMonitorIface == nullptr) {
        ALOGE("gnssRilInfoMonitorIface null !");
        return;
    }
    if (length < 1) {
        ALOGE("Invalid length %d", length);
        return;
    }

    char* buf = new char[length + 1];
    if (buf == nullptr) {
        ALOGE("new allocation failed, fatal error.");
        return;
    }
    memset(buf, 0, length+1);
    jbyte* bytes = (jbyte *)env->GetPrimitiveArrayCritical(data, 0);
    for (int i=0; i < length; i++) {
        buf[i] = (char)bytes[i];
    }
    env->ReleasePrimitiveArrayCritical(data, bytes, JNI_ABORT);

    TO_HIDL_SERVICE();
    auto r = gnssRilInfoMonitorIface->niSuplInit(buf);
    if (!r.isOk()) {
        ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
    }
    delete[] buf;
}

static void chargerStatusInject(JNIEnv* /*env*/, jobject /*obj*/,
                                jint status) {

    ALOGD("%s", __FUNCTION__);

    if (gnssRilInfoMonitorIface == nullptr) {
        ALOGE("gnssRilInfoMonitorIface null !");
        return;
    }
    TO_HIDL_SERVICE();
    auto r = gnssRilInfoMonitorIface->chargerStatusInject(status);
    if (!r.isOk()) {
        ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
    }
}

static JNINativeMethod sMethods[] = {
     /* name, signature, funcPtr */
    {"native_rm_class_init", "()V", (void *)classInit},
    {"native_rm_init", "()V", (void *)instanceInit},
    {"native_rm_cinfo_inject", "(IIIIZ)V", (void *)cinfoInject},
    {"native_rm_oos_inform", "()V", (void *)oosInform},
    {"native_rm_ni_supl_init", "([BI)V", (void *)niSuplInit},
};

int register_RilInfoMonitor(JNIEnv* env) {
    return jniRegisterNativeMethods(env, "com/qualcomm/location/RilInfoMonitor",
                                    sMethods, NELEM(sMethods));
}
