/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
  Copyright (c) 2015-2017 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
#define LOG_TAG "LocSvc_HIDL_IzatProvider_jni"
#define LOG_NDEBUG 0

#include <string>
#include <jni.h>
#include <JNIHelp.h>
#include "utils_jni.h"
#include <android_runtime/AndroidRuntime.h>
#include <utils/Log.h>
#include <map>
#ifdef LOC_HIDL_VERSION_1_1
#include <vendor/qti/gnss/1.1/ILocHidlGnss.h>
#include <vendor/qti/gnss/1.1/ILocHidlIzatProvider.h>
#include <vendor/qti/gnss/1.1/ILocHidlIzatProviderCallback.h>
#else
#include <vendor/qti/gnss/1.0/ILocHidlGnss.h>
#include <vendor/qti/gnss/1.0/ILocHidlIzatProvider.h>
#include <vendor/qti/gnss/1.0/ILocHidlIzatProviderCallback.h>
#endif

#ifdef LOC_HIDL_VERSION_1_1
using ::vendor::qti::gnss::V1_1::ILocHidlGnss;
using ::vendor::qti::gnss::V1_1::ILocHidlIzatProvider;
using ::vendor::qti::gnss::V1_1::ILocHidlIzatProviderCallback;
#else
using ::vendor::qti::gnss::V1_0::ILocHidlGnss;
using ::vendor::qti::gnss::V1_0::ILocHidlIzatProvider;
using ::vendor::qti::gnss::V1_0::ILocHidlIzatProviderCallback;
#endif
using ::vendor::qti::gnss::V1_0::LocHidlIzatStreamType;
using ::vendor::qti::gnss::V1_0::LocHidlIzatLocation;
using ::vendor::qti::gnss::V1_0::LocHidlIzatProviderStatus;
using ::vendor::qti::gnss::V1_0::LocHidlIzatRequest;
using ::vendor::qti::gnss::V1_0::LocHidlNetworkPositionSourceType;
using ::vendor::qti::gnss::V1_0::LocHidlIzatHorizontalAccuracy;

using ::android::hardware::hidl_vec;
using ::android::hardware::hidl_string;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

using namespace android;
using namespace std;


class IzatProviderGlue;
static IzatProviderGlue* networkProvider;
static IzatProviderGlue* fusedProvider;

static jmethodID method_onLocationChanged;
static jmethodID method_onStatusChanged;

inline static IzatProviderGlue**
providerTypeToNativeObj(jint providerType) {
    switch (static_cast<LocHidlIzatStreamType>(providerType)) {
    case LocHidlIzatStreamType::FUSED:
        return &fusedProvider;
    case LocHidlIzatStreamType::NETWORK:
        return &networkProvider;
    default:
        return NULL;
    }
}

inline static int providerStatusLookUp(const LocHidlIzatProviderStatus status) {
    switch (status) {
    case LocHidlIzatProviderStatus::GNSS_STATUS_SESSION_BEGIN:
        return 4; // GNSS_SESSION_BEGIN
    case LocHidlIzatProviderStatus::GNSS_STATUS_SESSION_END:
        return 5; // GNSS_SESSION_END
    case LocHidlIzatProviderStatus::GNSS_STATUS_ENGINE_ON:
        return 6; // GNSS_ENGINE_ON
    case LocHidlIzatProviderStatus::GNSS_STATUS_ENGINE_OFF:
        return 7; // GNSS_ENGINE_OFF
    default:
        return -1;
    }
}

static IzatProviderGlue* createGnssIzatRequest(LocHidlIzatRequest& request, jint providerType,
                                               jint numUpdates, jlong interval,
                                               jfloat smallestDisplacement, jint accuracy) {
    IzatProviderGlue** provider = providerTypeToNativeObj(providerType);

    if (provider && *provider != NULL) {
        request.provider = static_cast<LocHidlIzatStreamType>(providerType);
        request.numUpdates = numUpdates;
        request.timeIntervalBetweenFixes = interval;
        request.smallestDistanceBetweenFixes = smallestDisplacement;
        request.suggestedHorizontalAccuracy = static_cast<LocHidlIzatHorizontalAccuracy>(accuracy);

        ALOGD ("Provider Type: %d; Num Updates : %d; Interval : %lld"
               "Displacement : %f; Accuracy : %d", request.provider,
               request.numUpdates, request.timeIntervalBetweenFixes,
               request.smallestDistanceBetweenFixes, request.suggestedHorizontalAccuracy);

        return *provider;
    }
    return NULL;
}

/* ===========================================================
 *   HIDL Callbacks : ILocHidlIzatProviderCallback.hal
 * ===========================================================*/
class IzatProviderGlue : public ILocHidlIzatProviderCallback {
    LocHidlIzatStreamType mStreamType;
    const jobject mJavaProvider;
    sp<ILocHidlIzatProvider> mNativeProvider;
    bool mEnabled;
public:
    inline IzatProviderGlue(LocHidlIzatStreamType streamType, jobject javaProvider,
                                sp<ILocHidlIzatProvider>& nativeProvider) :
        mStreamType(streamType), mJavaProvider(javaProvider),
        mNativeProvider(nativeProvider), mEnabled(false) {
        if (mNativeProvider != NULL) {
            TO_HIDL_SERVICE();
            auto r = mNativeProvider->init(this);
            if (!r.isOk() || (r == false)) {
                ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
            }
        }
    }
    inline ~IzatProviderGlue() {
        JNIEnv* env = android::AndroidRuntime::getJNIEnv();
        if (env != NULL && mJavaProvider != NULL) {
            env->DeleteGlobalRef(mJavaProvider);
        }
        if (mNativeProvider != NULL) {
            TO_HIDL_SERVICE();
            auto r = mNativeProvider->deinit();
            if (!r.isOk()) {
                ALOGW("mNativeProvider->deinit() of stream type %d failed", mStreamType);
            }
        }
    }
    inline sp<ILocHidlIzatProvider>& getNativeProvider() {
        return mNativeProvider;
    }
    inline void setEnabled(bool isEnabled) {
        mEnabled = isEnabled;
    }

    Return<void> onLocationChanged(const LocHidlIzatLocation& location) override {
        FROM_HIDL_SERVICE();
        if (location.hasLatitude && location.hasLongitude && location.hasHorizontalAccuracy &&
            mEnabled && mJavaProvider && method_onLocationChanged) {

            JNIEnv* env = android::AndroidRuntime::getJNIEnv();
            if (env != NULL) {
                jlong utcTime = location.utcTimestampInMsec;
                jlong elapsedRealTimeNanos = location.elapsedRealTimeInNanoSecs;
                jdouble latitude = location.latitude;
                jdouble longitude = location.longitude;
                jboolean hasAltitude = location.hasAltitudeWrtEllipsoid;
                jdouble altitude = location.altitudeWrtEllipsoid;
                jboolean hasVerticalUnc = location.hasVertUnc;
                jfloat verticalUnc = location.vertUnc;
                jboolean hasSpeed = location.hasSpeed;
                jfloat speed = location.speed;
                jboolean hasSpeedUnc = location.hasSpeedUnc;
                jfloat speedUnc = location.speedUnc;
                jboolean hasBearing = location.hasBearing;
                jfloat bearing = location.bearing;
                jboolean hasBearingUnc = location.hasBearingUnc;
                jfloat bearingUnc = location.bearingUnc;
                jboolean hasAccuracy = location.hasHorizontalAccuracy;
                jfloat accuracy = location.horizontalAccuracy;
                jshort positionSource = -1;

                switch (location.networkPositionSource) {
                case LocHidlNetworkPositionSourceType::WIFI:
                    positionSource = 1;
                    break;
                case LocHidlNetworkPositionSourceType::CELL:
                    positionSource = 0;
                    break;
                }

                env->CallVoidMethod(mJavaProvider, method_onLocationChanged,
                                    utcTime, elapsedRealTimeNanos, latitude,
                                    longitude, hasAltitude, altitude, hasVerticalUnc,
                                    verticalUnc, hasSpeed, speed,
                                    hasSpeedUnc, speedUnc,
                                    hasBearing, bearing,
                                    hasBearingUnc, bearingUnc,
                                    hasAccuracy, accuracy, positionSource);
            }
        }
        return Void();
    }

    Return<void> onStatusChanged(LocHidlIzatProviderStatus status) {
        FROM_HIDL_SERVICE();
        JNIEnv* env = android::AndroidRuntime::getJNIEnv();
        jint javaStatus = providerStatusLookUp(status);
        if (mStreamType == LocHidlIzatStreamType::FUSED && mJavaProvider &&
            method_onStatusChanged && env && javaStatus != -1) {
            env->CallVoidMethod(mJavaProvider, method_onStatusChanged, javaStatus);
        }
        return Void();
    }
};

/* ===================================================================
 *   HIDL Interface
 * ===================================================================*/
static sp<ILocHidlGnss> vendorHal = nullptr;
static sp<ILocHidlIzatProvider> networkProviderIface = nullptr;
static sp<ILocHidlIzatProvider> fusedProviderIface = nullptr;

// Java to native calls
static void onJavaClassLoad(JNIEnv* env, jclass clazz) {
    method_onLocationChanged = env->GetMethodID(clazz,
                                                "onLocationChanged", "(JJDDZDZFZFZFZFZFZFS)V");
    method_onStatusChanged = env->GetMethodID(clazz,
                                              "onStatusChanged", "(I)V");
    // Setting the values of LocHidlIzatStreamType::NETWORK / LocHidlIzatStreamType::FUSED
    env->SetStaticIntField(clazz,
                           env->GetStaticFieldID(clazz, "IZAT_STREAM_NETWORK", "I"),
                           static_cast<int>(LocHidlIzatStreamType::NETWORK));
    env->SetStaticIntField(clazz,
                           env->GetStaticFieldID(clazz, "IZAT_STREAM_FUSED", "I"),
                           static_cast<int>(LocHidlIzatStreamType::FUSED));
    env->SetStaticIntField(clazz,
                           env->GetStaticFieldID(clazz, "IZAT_HORIZONTAL_FINE", "I"),
                           static_cast<int>(LocHidlIzatHorizontalAccuracy::FINE));
    env->SetStaticIntField(clazz,
                           env->GetStaticFieldID(clazz, "IZAT_HORIZONTAL_BLOCK", "I"),
                           static_cast<int>(LocHidlIzatHorizontalAccuracy::BLOCK));
    env->SetStaticIntField(clazz,
                           env->GetStaticFieldID(clazz, "IZAT_HORIZONTAL_NONE", "I"),
                           static_cast<int>(LocHidlIzatHorizontalAccuracy::NONE));
}

static void onJavaStaticLoad(JNIEnv* env, jclass clazz) {
    // HIDL Service
    vendorHal = ILocHidlGnss::getService(GNSS_VENDOR_SERVICE_INSTANCE);
    if (vendorHal == nullptr) {
        ALOGE("Unable to get GnssVendor service\n");
        return;
    }
    ALOGD("Got GnssVendor Service.");

#ifdef LOC_HIDL_VERSION_1_1
    auto izat = vendorHal->getExtensionLocHidlIzatNetworkProvider_1_1();
#else
    auto izat = vendorHal->getExtensionLocHidlIzatNetworkProvider();
#endif
    if (!izat.isOk()) {
        ALOGE("Unable to get a handle to IzatProvider extension !");
    } else {
        networkProviderIface = izat;
        ALOGD("Got IzatProvider Extension.");
    }

#ifdef LOC_HIDL_VERSION_1_1
    izat = vendorHal->getExtensionLocHidlIzatFusedProvider_1_1();
#else
    izat = vendorHal->getExtensionLocHidlIzatFusedProvider();
#endif
    if (!izat.isOk()) {
        ALOGE("Unable to get a handle to IzatProvider extension !");
    } else {
        fusedProviderIface = izat;
        ALOGD("Got IzatProvider Extension.");
    }
}

static void onJavaInstanceInit(JNIEnv *env, jobject obj, int providerType) {
    if (networkProviderIface == nullptr || fusedProviderIface == nullptr) {
        ALOGE("networkProviderIface or fusedProviderIface is null");
        return;
    }

    IzatProviderGlue** provider = providerTypeToNativeObj(providerType);
    if (provider && *provider == NULL) {
        auto providerHidl =
                (providerType == static_cast<int>(LocHidlIzatStreamType::FUSED)) ?
                fusedProviderIface : networkProviderIface;
        *provider = new IzatProviderGlue(static_cast<LocHidlIzatStreamType>(providerType),
                                         env->NewGlobalRef(obj),
                                         providerHidl);
    }
}

static void onJavaInstanceDeinit(JNIEnv* /*env*/, jobject /*obj*/, int providerType) {
    IzatProviderGlue** provider = providerTypeToNativeObj(providerType);

    if (provider) {
        if (*provider) {
            delete *provider;
        }
        *provider = NULL;
    }
}

static void onEnable(JNIEnv* /*env*/, jobject /*obj*/, jint providerType) {
    IzatProviderGlue** provider = providerTypeToNativeObj(providerType);

    if (provider && *provider != NULL) {
        auto r = (*provider)->getNativeProvider()->onEnable();
        if (!r.isOk() || (r == false)) {
            ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
        } else {
            (*provider)->setEnabled(true);
        }
    }
}

static void onDisable(JNIEnv* /*env*/, jobject /*obj*/, jint providerType) {
    IzatProviderGlue** provider = providerTypeToNativeObj(providerType);

    if (provider && *provider != NULL) {
        auto r = (*provider)->getNativeProvider()->onDisable();
        if (!r.isOk() || (r == false)) {
            ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
        } else {
            (*provider)->setEnabled(false);
        }
    }
}

static void onAddRequest(JNIEnv* /*env*/, jobject /*obj*/, jint providerType,
                         jint numUpdates, jlong interval,
                         jfloat smallestDisplacement, jint accuracy) {
    LocHidlIzatRequest request = {};
    IzatProviderGlue* provider = createGnssIzatRequest(request, providerType, numUpdates,
                                                       interval, smallestDisplacement,
                                                       accuracy);

    if (provider) {
        TO_HIDL_SERVICE();
        auto r = provider->getNativeProvider()->onAddRequest(request);
        if (!r.isOk() || (r == false)) {
            ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
        }
    }
}

static void onRemoveRequest(JNIEnv* /*env*/, jobject /*obj*/, jint providerType,
                         jint numUpdates, jlong interval,
                         jfloat smallestDisplacement, jint accuracy) {
    LocHidlIzatRequest request = {};
    IzatProviderGlue* provider = createGnssIzatRequest(request, providerType, numUpdates,
                                                       interval, smallestDisplacement,
                                                       accuracy);

    if (provider) {
        TO_HIDL_SERVICE();
        auto r = provider->getNativeProvider()->onRemoveRequest(request);
        if (!r.isOk() || (r == false)) {
            ALOGE("Error invoking HIDL API [%s]", r.description().c_str());
        }
    }
}

static JNINativeMethod sMethods[] = {
    {"nativeOnClassLoad", "()V", (void*)onJavaClassLoad},
    {"nativeOnStaticLoad", "()V", (void*)onJavaStaticLoad},
    {"nativeOnInstanceInit", "(I)V", (void*)onJavaInstanceInit},
    {"nativeOnInstanceDeinit", "(I)V", (void *) onJavaInstanceDeinit},
    {"nativeOnEnableProvider","(I)V", (void*)onEnable},
    {"nativeOnDisableProvider","(I)V", (void*)onDisable},
    {"nativeOnAddRequest","(IIJFI)V", (void*)onAddRequest},
    {"nativeOnRemoveRequest", "(IIJFI)V", (void*)onRemoveRequest}
};


int register_IzatProvider(JNIEnv* env) {
    return jniRegisterNativeMethods(env,
                                    "com/qualcomm/location/izatprovider/IzatProvider",
                                    sMethods, NELEM(sMethods));
}
