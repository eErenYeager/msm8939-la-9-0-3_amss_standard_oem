/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

GENERAL DESCRIPTION
  DataItemBaseExt

  Copyright (c) 2017 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/

#ifndef __IZAT_MANAGER_DATAITEMEBASEEXT_H__
#define __IZAT_MANAGER_DATAITEMEBASEEXT_H__

#include <string>
#include <DataItemId.h>
#include <IDataItemCore.h>
#include <IDataItemSerializer.h>

using namespace std;
using loc_core::IDataItemCore;

namespace izat_manager
{
class IDataItemSerialization;

class DataItemBaseExt : public IDataItemSerialization,
                        public IDataItemCore{
public:
    DataItemBaseExt(DataItemId Id, std::string cardName = "") :
        mId(Id),
        mCardName (cardName)
    {}

    virtual ~DataItemBaseExt() {}

    virtual DataItemId getId () { return mId; }

    virtual void stringify (string & valueStr) {
        (void)valueStr;
        return;
    }

    virtual int32_t copy (IDataItemCore * src, bool *dataItemCopied = nullptr) {
        (void)src;
        (void)dataItemCopied;
        return 1;
    }

    virtual int32 serialize (OutPostcard * out) { (void)out; return 1; };

    virtual int32 deSerialize (InPostcard * in) { (void)in; return 1; };

    virtual const std::string & getCardName () const {
        return mCardName;
    }
    virtual bool hasCardName () const {
        return !mCardName.empty ();
    }

protected:
    /**
     * Data item id.
     */
    DataItemId mId;
    std::string mCardName;
};

}
#endif // #ifndef __IZAT_MANAGER_DATAITEMEBASEEXT_H__
