/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#pragma once

#include <framework/legacy.h>
#include <framework/UnSolicitedMessage.h>
#include <framework/add_message_id.h>

/**
  * QmiSmsSetUpCompleteIndMessage: Sent by the modem endpoint to
  * indicate that the WMS QMI client has been initialized.
  */

class QmiSmsSetUpCompleteIndMessage: public UnSolicitedMessage,
                                     public add_message_id<QmiSmsSetUpCompleteIndMessage>
{
  public:
    static constexpr const char *MESSAGE_NAME = "com.qualcomm.qti.qcril.qmi_sms_setup_complete_ind";

    string dump() {
      return mName + "{}";
    }

    QmiSmsSetUpCompleteIndMessage(qcril_instance_id_e_type instance_id):
        UnSolicitedMessage(get_class_message_id()),
        instance_id(instance_id) {
      mName = MESSAGE_NAME;
    }

    std::shared_ptr<UnSolicitedMessage> clone() {
      return (std::make_shared<QmiSmsSetUpCompleteIndMessage>(instance_id));
    }

    qcril_instance_id_e_type get_instance_id() {
      return instance_id;
    }

    inline ~QmiSmsSetUpCompleteIndMessage() {}

  private:
    qcril_instance_id_e_type instance_id;
};

