/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/

#pragma once
#include "framework/GenericCallback.h"
#include "framework/SolicitedSyncMessage.h"
#include "framework/Message.h"
#include "framework/add_message_id.h"

class UimQmiUimRequestSyncMsg : public SolicitedSyncMessage<int>,
                                public add_message_id<UimQmiUimRequestSyncMsg>
{
  private:
    uint32_t                          mMsgId;
    const void                      * mMsgPtr;
    void                            * mRspData;

  public:
    static constexpr const char *MESSAGE_NAME = "com.qualcomm.qti.qcril.uim.qmi_uim_client_sync_request";
    UimQmiUimRequestSyncMsg() = delete;
    inline ~UimQmiUimRequestSyncMsg()
    {
      mMsgPtr = nullptr;
      mRspData = nullptr;
    }

    UimQmiUimRequestSyncMsg(uint32_t msg_id, const void *dataPtr, void  *rsp_data):
                            SolicitedSyncMessage<int>(get_class_message_id())
    {
      mName = MESSAGE_NAME;
      mMsgId = msg_id;
      mMsgPtr = dataPtr;
      mRspData = rsp_data;
    }

    inline const void * get_message(void)
    {
      return mMsgPtr;
    }

    inline uint32_t get_msg_id(void)
    {
      return mMsgId;
    }

    inline void *get_rsp_data()
    {
      return mRspData;
    }

    inline string dump()
    {
      return mName + "{ " + std::to_string(mMsgId) + "}";
    }
};
