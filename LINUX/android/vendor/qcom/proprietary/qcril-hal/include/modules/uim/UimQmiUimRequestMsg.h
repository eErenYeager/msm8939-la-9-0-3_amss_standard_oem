/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/

#pragma once
#include "framework/GenericCallback.h"
#include "framework/SolicitedMessage.h"
#include "framework/Message.h"
#include "framework/add_message_id.h"
#include "modules/uim/qcril_uim_srvc.h"

class UimQmiUimRequestMsg : public SolicitedMessage<qmi_uim_rsp_data_type>,
                            public add_message_id<UimQmiUimRequestMsg>
                            
{
  private:
    uint32_t                          mMsgId;
    void                            * mMsgPtr;
    void                            * mOrigPtr;

  public:
    static constexpr const char *MESSAGE_NAME = "com.qualcomm.qti.qcril.uim.qmi_uim_request_msg";
    UimQmiUimRequestMsg() = delete;
    ~UimQmiUimRequestMsg();

    UimQmiUimRequestMsg(uint32_t msg_id, void *dataPtr, void *orig_ptr,
                        GenericCallback<qmi_uim_rsp_data_type> *callback);

    inline void * get_message()
    {
      return mMsgPtr;
    }

    inline void * get_orig_ptr(void)
    {
      return mOrigPtr;
    }

    inline uint32_t get_msg_id(void)
    {
      return mMsgId;
    }

    inline string dump()
    {
      return mName + "{ " + std::to_string(mMsgId) + "}";
    }
};
