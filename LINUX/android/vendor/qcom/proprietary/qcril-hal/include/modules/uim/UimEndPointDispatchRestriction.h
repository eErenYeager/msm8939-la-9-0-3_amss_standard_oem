/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#pragma once
#include <queue>
#include <memory>
#include "framework/Restriction.h"
class UimEndPointDispatchRestriction : public Restriction
{
  public:
    static constexpr int MAX_INFLIGHT_UIM_END_POINT_MSG = 10;
    inline UimEndPointDispatchRestriction()
    {
      mName = "UimEndPointDispatchRestriction";
      mInflightMsgCount = 0;
    }

    inline ~UimEndPointDispatchRestriction() {}
    bool isDispatchAllowed(std::shared_ptr<Message> msg);
    void onMessageCallbackCompletion(std::shared_ptr<Message> msg);
    inline string to_string() { return mName; }
  private:
    std::mutex mMutex;
    int mInflightMsgCount;
    std::queue<std::shared_ptr<Message> > mDispatchPendingList;
};

