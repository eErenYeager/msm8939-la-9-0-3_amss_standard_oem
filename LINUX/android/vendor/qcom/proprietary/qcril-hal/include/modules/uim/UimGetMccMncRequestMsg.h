/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#ifndef UIM_GET_MCCMNC_REQUEST_MSG_H
#define UIM_GET_MCCMNC_REQUEST_MSG_H
#include "framework/GenericCallback.h"
#include "framework/SolicitedMessage.h"
#include "framework/Message.h"
#include "framework/add_message_id.h"
#include "modules/uim/qcril_uim_srvc.h"


typedef struct
{
  char       mcc[4];
  char       mnc[4];
  RIL_Errno  err_code;
}qcril_mcc_mnc_info_type;


class UimGetMccMncRequestMsg : public SolicitedMessage<qcril_mcc_mnc_info_type>,
                               public add_message_id<UimGetMccMncRequestMsg>
{
  private:
    char     mAidBuffer[QMI_UIM_MAX_AID_LEN+1];
    uint8_t  mAidLen;

  public:
    static constexpr const char *MESSAGE_NAME = "com.qualcomm.qti.qcril.uim.get_mcc_mnc";
    UimGetMccMncRequestMsg() = delete;
    inline ~UimGetMccMncRequestMsg()
    {
      memset(mAidBuffer, 0x00, sizeof(mAidBuffer));
      mAidLen = 0;
    }

    inline UimGetMccMncRequestMsg(char *aid_ptr, uint8_t aid_len,
                                  GenericCallback<qcril_mcc_mnc_info_type> *callback) :
                                  SolicitedMessage<qcril_mcc_mnc_info_type>(get_class_message_id())
    {
      mName = MESSAGE_NAME;
      memset(mAidBuffer, 0x00, sizeof(mAidBuffer));
      mAidLen = 0;
      /* check for AID len + null termination */
      if (aid_ptr != nullptr && aid_len <= (QMI_UIM_MAX_AID_LEN + 1))
      {
        memcpy(mAidBuffer, aid_ptr, aid_len);
        mAidLen = aid_len;
      }
      setCallback(callback);
    }

    inline char * get_aid(uint8_t *aid_len_ptr)
    {
      *aid_len_ptr = mAidLen;
      return mAidBuffer;
    }

    inline string dump()
    {
      return mName + "{ " + mAidBuffer + "}";
    }
};
#endif
