/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#pragma once
#include "framework/Looper.h"
class QcrilMainLooper : public Looper {
 public:
  void handleMessage(std::shared_ptr<Message> msg);
  void handleMessageSync(std::shared_ptr<Message> msg);
};
