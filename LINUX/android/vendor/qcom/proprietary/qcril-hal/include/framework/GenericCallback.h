/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#pragma once
#include "framework/Dispatcher.h"
#include "framework/Message.h"
template <typename T>
class GenericCallback : public Message::Callback {
 public:
  inline GenericCallback(string str) : Callback(str){};
  inline virtual ~GenericCallback(){};
  virtual void onResponse(std::shared_ptr<Message> solicitedMsg, Status status,
                          std::shared_ptr<T> responseDataPtr) {
    (void)solicitedMsg;
    (void)status;
    (void)responseDataPtr;
  };

  inline void handleAsyncResponse(std::shared_ptr<Message> solicitedMsg,
                                  Message::Callback::Status status,
                                  std::shared_ptr<void> responseDataPtr) {
      onResponse(solicitedMsg, status,
        std::static_pointer_cast<T>(responseDataPtr));
  }
};
