/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#pragma once
#include <deque>
#include <mutex>

#include "framework/Message.h"

using std::deque;

class Message;
class MessageQueue {
 private:
  std::mutex mMutex;
  deque<std::shared_ptr<Message> > mMessages;

 public:
  void enqueue(std::shared_ptr<Message> msg);
  size_t getSize();
  std::string dumpMessageQueue();
  bool isEmpty();
  void clear();
  std::shared_ptr<Message> pop();

  // void dump();
};
