/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#include <mutex>

#define TAG "RILQ-MessageQueue"
#include "framework/Log.h"
#include "framework/MessageQueue.h"

using std::mutex;
using std::lock_guard;

void MessageQueue::enqueue(std::shared_ptr<Message> msg) {
  if(msg == nullptr){
    QCRIL_LOG_DEBUG("null message received .. Not enqueuing");
    return;
  }
  QCRIL_LOG_DEBUG("enqueuing message %p. Id: %s", msg.get(), msg->get_message_name().c_str());
  lock_guard<mutex> lock(mMutex);
  mMessages.push_back(msg);
  mMessages.shrink_to_fit();
}

size_t MessageQueue::getSize() {
  lock_guard<mutex> lock(mMutex);
  return mMessages.size();
}

bool MessageQueue::isEmpty() {
  lock_guard<mutex> lock(mMutex);
  return mMessages.empty();
}

string MessageQueue::dumpMessageQueue() {
  lock_guard<mutex> lock(mMutex);
  string output = "[";
  for (auto& elem : mMessages) {
#ifndef QMI_RIL_UTF
    QCRIL_LOG_DEBUG("dequeued message %p", elem.get());
#endif
    output += elem->to_string();
    output += ", ";
  }
  output += "]";
  return output;
}

void MessageQueue::clear() {
  lock_guard<mutex> lock(mMutex);
  mMessages.clear();
}

std::shared_ptr<Message> MessageQueue::pop() {
  if(getSize() > 0) {
    QCRIL_LOG_DEBUG("pop");
    lock_guard<mutex> lock(mMutex);
    std::shared_ptr<Message> msg = mMessages.front();
    QCRIL_LOG_DEBUG("msg: %p", msg.get());
    mMessages.pop_front();
    mMessages.shrink_to_fit();

    return msg;
  } else {
      return nullptr;
  }
}
