/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#include <string>
#include "modules/sms/QmiSmsSetUpCompleteIndMessage.h"

constexpr const char *QmiSmsSetUpCompleteIndMessage::MESSAGE_NAME;

