/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#include "modules/qmi/QmiSmsSetupRequestCallback.h"
#include "modules/sms/QmiSmsSetUpCompleteIndMessage.h"


#define TAG "QmiSmsSetupRequestCallback"

/*===========================================================================
  FUNCTION  onResponse  -  QmiSmsSetupRequestCallback onResponse function
===========================================================================*/
void QmiSmsSetupRequestCallback::onResponse
(
  std::shared_ptr<Message> solicitedMsg,
  Message::Callback::Status status,
  std::shared_ptr<string> responseDataPtr
)
{
  (void)solicitedMsg;
  (void)status;
  (void)responseDataPtr;

  QCRIL_LOG_INFO( "QmiSmsSetupRequestCallback : %d ", status);
  Log::getInstance().d(
      "[QmiSetupRequestCallback]: Callback executed. client data = " +
      mClientToken);

  if (status == Message::Callback::Status::SUCCESS)
  {
    std::shared_ptr<QmiSmsSetUpCompleteIndMessage> ind_msg_ptr =
         std::make_shared<QmiSmsSetUpCompleteIndMessage>(QCRIL_DEFAULT_INSTANCE_ID);
    if (ind_msg_ptr != nullptr)
    {
        ind_msg_ptr->broadcast();
    }
  }
} /* QmiSmsSetupRequestCallback::onResponse */


/*===========================================================================
  FUNCTION  clone  -  QmiSmsSetupRequestCallback clone function
===========================================================================*/
Message::Callback *QmiSmsSetupRequestCallback::clone()
{
  QmiSmsSetupRequestCallback *clone = new QmiSmsSetupRequestCallback(mClientToken);
  return clone;
} /* QmiSmsSetupRequestCallback::clone */
