/******************************************************************************
@file    uim_service.cpp
@brief   qcril uim service

DESCRIPTION
Implements the server side of the IUim interface.
Handles RIL requests and responses and indications to be received
and sent to client respectively

---------------------------------------------------------------------------

Copyright (c) 2017 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
---------------------------------------------------------------------------
******************************************************************************/

#include <vendor/qti/hardware/radio/uim/1.0/types.h>
#include "uim_service.h"

#include "qcril_log.h"
#include "qcril_uim_security.h"

using namespace vendor::qti::hardware::radio::uim::V1_0::implementation;
using namespace vendor::qti::hardware::radio::uim::V1_0;
using ::android::hardware::Status;

namespace vendor {
namespace qti {
namespace hardware {
namespace radio {
namespace uim {
namespace V1_0 {
namespace implementation {

static sp<UimImpl> uim = NULL;


/*===========================================================================

FUNCTION:  UimImpl::UimImpl

===========================================================================*/
UimImpl::UimImpl()
{
  mResponseCb = NULL;
  mIndicationCb = NULL;
  mIndicationDeathRecipient = NULL;
  mResponseDeathRecipient = NULL;
} /* UimImpl::UimImpl() */


/*===========================================================================

FUNCTION:  UimImpl::setCallback

===========================================================================*/
Return<void> UimImpl::setCallback
(
  const sp<IUimResponse>& responseCallback,
  const sp<IUimIndication>& indicationCallback
)
{
  QCRIL_LOG_INFO("UimImpl::setCallback");
  mIndicationDeathRecipient = new UimIndicationDeathRecipient(this);
  mResponseDeathRecipient = new UimResponseDeathRecipient(this);
  {
    Mutex::Autolock lock(mIndicationLock);
    mIndicationCb = indicationCallback;
    mIndicationCb->linkToDeath(mIndicationDeathRecipient, 0);
  }
  {
    Mutex::Autolock lock(mResponseLock);
    mResponseCb = responseCallback;
    mResponseCb->linkToDeath(mResponseDeathRecipient, 0);
  }
  return Status::ok();
} /* UimImpl::setCallback */


/*===========================================================================

FUNCTION:  UimImpl::resetIndicationCallback

===========================================================================*/
void UimImpl::resetIndicationCallback()
{
  QCRIL_LOG_ERROR("UimImpl::resetIndicationCallback Client died");
  Mutex::Autolock lock(mIndicationLock);
  mIndicationCb->unlinkToDeath(mIndicationDeathRecipient);
  mIndicationCb = NULL;
} /* UimImpl::resetIndicationCallback() */


/*===========================================================================

FUNCTION:  UimImpl::resetResponseCallback

===========================================================================*/
void UimImpl::resetResponseCallback()
{
  QCRIL_LOG_ERROR("UimImpl::resetResponsCallback Client died");
  Mutex::Autolock lock(mResponseLock);
  mResponseCb->unlinkToDeath(mResponseDeathRecipient);
  mResponseCb = NULL;
} /* UimImpl::resetResponseCallback() */


/*===========================================================================

FUNCTION:  UimImpl::setInstanceId

===========================================================================*/
void UimImpl::setInstanceId
(
  int instanceId
)
{
  mInstanceId = instanceId;
} /* UimImpl::setInstanceId */


/*===========================================================================

FUNCTION:  UimImpl::uimRemoteSimlockResponse

===========================================================================*/
void UimImpl::uimRemoteSimlockResponse
(
  int                             token,
  UimRemoteSimlockResponseType    response,
  UimRemoteSimlockOperationType   op_type,
  uint8_t                       * rsp_ptr,
  uint32_t                        rsp_len,
  UimRemoteSimlockVersion         version,
  UimRemoteSimlockStatus          status
)
{
  hidl_vec<uint8_t> simlock_data;

  Mutex::Autolock lock(mResponseLock);
  if (mResponseCb == NULL)
  {
    QCRIL_LOG_ERROR("uimRemoteSimlockResponse responseCb is null");
    return;
  }
  QCRIL_LOG_INFO("uimRemoteSimlockResponse token=%d", token);

  simlock_data.setToExternal(rsp_ptr, rsp_len);

  QCRIL_LOG_INFO("op_type:%d, version : %d, status : %d, len: %d", op_type, version, status, rsp_len);
  if (rsp_ptr != NULL && rsp_len != 0)
  {
    uint32_t i = 0;
    for (i = 0; i < rsp_len; i++)
    {
      QCRIL_LOG_INFO("simlock[%d]: 0x%x", i, rsp_ptr[i]);
    }
  }

  mResponseCb->UimRemoteSimlockResponse(token,
                                        response,
                                        op_type,
                                        simlock_data,
                                        version,
                                        status);
}


/*===========================================================================

FUNCTION:  UimImpl::UimRemoteSimlockRequest

===========================================================================*/
Return<void> UimImpl::UimRemoteSimlockRequest
(
  int32_t token,
  UimRemoteSimlockOperationType simlockOp,
  const hidl_vec<uint8_t>& simlockData
)
{
  int  * tokenPtr  = new int(token);

  qcril_uim_request_remote_sim_lock_unlock(tokenPtr,
  (qcril_uim_remote_simlock_operation_type)simlockOp,
                                           simlockData.data(),
                                           simlockData.size());
  return Status::ok();
} /* UimImpl::UimRemoteSimlockRequest */

} // namespace implementation
} // namespace V1_0
} // namespace uim
} // namespace radio
} // namespace hardware
} // namespace qti
} // namespace vendor



/*===========================================================================

FUNCTION:  qcril_uim_register_service

===========================================================================*/
uint8_t qcril_uim_register_service
(
  int instanceId
)
{
  android::status_t ret = android::OK;
  uim = new UimImpl();
  uim->setInstanceId(instanceId);
  std::string serviceName = "Uim";
  ret = uim->registerAsService(serviceName + std::to_string(instanceId));
  QCRIL_LOG_INFO("qcril_uim_register_service instanceId=%d status=%d", instanceId, ret);

  return ret == android::OK;
} /* qcril_uim_register_service */


/*===========================================================================

FUNCTION:  qcril_uim_remote_simlock_response

===========================================================================*/
void qcril_uim_remote_simlock_response
(
  void                                    * token_ptr,
  qcril_uim_remote_simlock_response_type    response,
  qcril_uim_remote_simlock_operation_type   op_type,
  uint8                                   * simlock_rsp_ptr,
  uint32                                    simlock_rsp_len,
  qcril_uim_remote_simlock_version          version,
  qcril_uim_remote_simlock_status           status
)
{
  UimRemoteSimlockVersion    simlock_version;
  UimRemoteSimlockStatus     simlock_status;

  if (token_ptr == NULL)
  {
    QCRIL_LOG_ERROR("Null token ptr");
    return;
  }

  simlock_version.majorVersion = version.majorVersion;
  simlock_version.minorVersion = version.minorVersion;

  simlock_status.status = (UimRemoteSimlockStatusType)status.status;
  simlock_status.unlockTime = status.unlockTime;

  QCRIL_LOG_INFO("op_type:%d, version : %d, status : %d, len: %d", op_type, version, status, simlock_rsp_len);
  if (simlock_rsp_ptr != NULL && simlock_rsp_len != 0)
  {
    uint32_t i = 0;
    for (i = 0; i < simlock_rsp_len; i++)
    {
      QCRIL_LOG_INFO("simlock[%d]: 0x%x", i, simlock_rsp_ptr[i]);
    }
  }

  uim->uimRemoteSimlockResponse(*((int *)token_ptr),
         (UimRemoteSimlockResponseType)response,
         (UimRemoteSimlockOperationType)op_type,
                           simlock_rsp_ptr,
                           simlock_rsp_len,
                           simlock_version,
                           simlock_status);
} /* qcril_uim_remote_simlock_response */
