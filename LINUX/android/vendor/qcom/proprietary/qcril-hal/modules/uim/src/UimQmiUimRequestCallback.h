/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#pragma once
#include "framework/GenericCallback.h"
#include "framework/Message.h"
#include "modules/uim/qcril_uim_srvc.h"

class UimQmiUimRequestCallback : public GenericCallback<qmi_uim_rsp_data_type> {
 public:
  inline UimQmiUimRequestCallback(string str) : GenericCallback(str) {}
  inline ~UimQmiUimRequestCallback() {}
  Message::Callback *clone();
  void onResponse(std::shared_ptr<Message>               solicitedMsg,
                  Message::Callback::Status              status,
                  std::shared_ptr<qmi_uim_rsp_data_type> responseDataPtr);
  string mClientToken;
};
