/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/
#include "framework/Dispatcher.h"
#include "framework/Log.h"
#include "modules/uim/UimEndPointDispatchRestriction.h"

using std::lock_guard;
using std::mutex;

bool UimEndPointDispatchRestriction::isDispatchAllowed
(
  std::shared_ptr<Message> msg
)
{
  lock_guard<mutex> lock(mMutex);
  string msgStr = msg->to_string();
  bool okToDispatch = false;

  if (mInflightMsgCount < MAX_INFLIGHT_UIM_END_POINT_MSG)
  {
    mInflightMsgCount++;
    okToDispatch = true;
  }
  if (!okToDispatch)
  {
    Log::getInstance().d("[UimEndPointDispatchRestriction]: Can not forward msg = " +
                  msg->to_string() + " until restriction is cleared");
    mDispatchPendingList.push(msg);
  }
  return okToDispatch;
} /* UimEndPointDispatchRestriction::isDispatchAllowed */


void UimEndPointDispatchRestriction::onMessageCallbackCompletion
(
  std::shared_ptr<Message> msg
)
{
  lock_guard<mutex> lock(mMutex);
  string msgStr = msg->to_string();

  if (mInflightMsgCount > 0)
  {
    mInflightMsgCount--;
  }

  if (mInflightMsgCount < MAX_INFLIGHT_UIM_END_POINT_MSG)
  {
    Log::getInstance().d("[UimEndPointDispatchRestriction]: msg = " +
                  msg->to_string() +
                  " callback executed. Other pending similar "
                  "messages can now reevaluate the restriction.");
    /* Re-enqueue the pending mesgs in the same order they came
     * previously.*/
    if (!mDispatchPendingList.empty())
    {
      std::shared_ptr<Message> pendingMsg = mDispatchPendingList.front();
      mDispatchPendingList.pop();
      Log::getInstance().d("[UimEndPointDispatchRestriction]: Requeue msg = " +
                   pendingMsg->dump());
      Dispatcher::getInstance().dispatch(pendingMsg);
    }
  }
} /* UimEndPointDispatchRestriction::onMessageCallbackCompletion */

