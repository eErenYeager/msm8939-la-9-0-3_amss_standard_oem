/* ---------------------------------------------------------------------------
Copyright (c) 2017 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
--------------------------------------------------------------------------- */
#ifndef VENDOR_QTI_VENDOR_RADIO_UIM_REMOTE_SERVER_V1_0_H
#define VENDOR_QTI_VENDOR_RADIO_UIM_REMOTE_SERVER_V1_0_H

#include <vendor/qti/hardware/radio/uim_remote_server/1.0/IUimRemoteServiceServer.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <ril.h>
#include <utils/Mutex.h>
#include "modules/uim/qcril_uim_sap.h"

using ::android::hardware::hidl_death_recipient;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::sp;
using ::android::wp;
using ::android::Mutex;

namespace vendor {
namespace qti {
namespace hardware {
namespace radio {
namespace uim_remote_server {
namespace V1_0 {
namespace implementation {

class UimRemoteServerIndicationDeathRecipient;
class UimRemoteServerResponseDeathRecipient;

class UimRemoteServerImpl : public IUimRemoteServiceServer {
  sp<IUimRemoteServiceServerResponse> mResponseCb;
  sp<IUimRemoteServiceServerIndication> mIndicationCb;
  sp<UimRemoteServerIndicationDeathRecipient> mIndicationDeathRecipient;
  sp<UimRemoteServerResponseDeathRecipient> mResponseDeathRecipient;
  int mInstanceId;
  Mutex mIndicationLock;
  Mutex mResponseLock;

  Return<void> setCallback(const sp<IUimRemoteServiceServerResponse>& responseCallback, const sp<IUimRemoteServiceServerIndication>& indicationCallback);
  Return<void> uimRemoteServiceServerConnectReq(int32_t token, int32_t maxMsgSize);
  Return<void> uimRemoteServiceServerDisconnectReq(int32_t token);
  Return<void> uimRemoteServiceServerApduReq(int32_t token, UimRemoteServiceServerApduType type, const hidl_vec<uint8_t>& command);
  Return<void> uimRemoteServiceServerTransferAtrReq(int32_t token);
  Return<void> uimRemoteServiceServerPowerReq(int32_t token, bool state);
  Return<void> uimRemoteServiceServerResetSimReq(int32_t token);
  Return<void> uimRemoteServiceServerTransferCardReaderStatusReq(int32_t token);
  Return<void> uimRemoteServiceServerSetTransferProtocolReq(int32_t token, UimRemoteServiceServerTransferProtocol transferProtocol);

public:
  UimRemoteServerImpl();
  void resetIndicationCallback();
  void resetResponseCallback();
  void setInstanceId(int instanceId);
  void uimRemoteServerProcessRequestResp(int request_id, int err_code, int token_id, uint8_t *data_ptr, uint32_t data_len);
  void uimRemoteServerProcessIndication(qmi_uim_sap_status_type  sap_state);
};

class UimRemoteServerIndicationDeathRecipient : public hidl_death_recipient {
public:
  UimRemoteServerIndicationDeathRecipient(const sp<UimRemoteServerImpl> uimRemoteServerImpl) : mUimRemoteServerImpl(uimRemoteServerImpl) {}
  virtual void serviceDied(uint64_t /*cookie*/, const wp<::android::hidl::base::V1_0::IBase>& /*who*/)
  {
    mUimRemoteServerImpl->resetIndicationCallback();
  }
  sp<UimRemoteServerImpl> mUimRemoteServerImpl;
};

class UimRemoteServerResponseDeathRecipient : public hidl_death_recipient {
public:
  UimRemoteServerResponseDeathRecipient(const sp<UimRemoteServerImpl> uimRemoteServerImpl) : mUimRemoteServerImpl(uimRemoteServerImpl) {}
  virtual void serviceDied(uint64_t /*cookie*/, const wp<::android::hidl::base::V1_0::IBase>& /*who*/)
  {
    mUimRemoteServerImpl->resetResponseCallback();
  }
  sp<UimRemoteServerImpl> mUimRemoteServerImpl;
};

}  // namespace implementation
}  // namespace V1_0
}  // namespace uim_remote_server
}  // namespace radio
}  // namespace hardware
}  // namespace qti
}  // namespace vendor

#endif  // VENDOR_QTI_VENDOR_RADIO_UIM_REMOTE_SERVER_V1_0_H
