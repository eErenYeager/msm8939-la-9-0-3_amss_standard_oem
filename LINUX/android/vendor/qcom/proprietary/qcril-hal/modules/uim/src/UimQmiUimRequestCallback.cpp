/******************************************************************************
#  Copyright (c) 2017 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/

#include "modules/uim/UimQmiUimRequestMsg.h"
#include "modules/uim/qcril_uim.h"
#include "framework/Log.h"
#include "UimQmiUimResponseMsg.h"
#include "UimQmiUimRequestCallback.h"

#define TAG "UimQmiUimRequestCallback"

/*===========================================================================
  FUNCTION  onResponse - response function
===========================================================================*/
void UimQmiUimRequestCallback::onResponse
(
  std::shared_ptr<Message>                solicitedMsg,
  Message::Callback::Status               status,
  std::shared_ptr<qmi_uim_rsp_data_type>  responseDataPtr
)
{
  qmi_uim_rsp_data_type                 *rsp_data_ptr = nullptr;
  std::shared_ptr<UimQmiUimResponseMsg>  resp_msg_ptr = nullptr;
  std::shared_ptr<UimQmiUimRequestMsg>   req_msg_ptr((
    std::static_pointer_cast<UimQmiUimRequestMsg>(solicitedMsg)));

  Log::getInstance().d(
      "[UimQmiUimRequestCallback]: Callback executed. client data = " +
      mClientToken);

  if (status == Message::Callback::Status::SUCCESS &&
      responseDataPtr != nullptr)
  {
    rsp_data_ptr = responseDataPtr.get();
  }

  resp_msg_ptr = std::make_shared<UimQmiUimResponseMsg>(rsp_data_ptr,
                                                        req_msg_ptr->get_orig_ptr());

  if (resp_msg_ptr != nullptr)
  {
    resp_msg_ptr->dispatch();
  }
} /* UimQmiUimRequestCallback::onResponse */


/*===========================================================================
  FUNCTION  clone - clone function
===========================================================================*/
Message::Callback *UimQmiUimRequestCallback::clone()
{
  UimQmiUimRequestCallback *clone = new UimQmiUimRequestCallback(mClientToken);
  return clone;
} /* UimQmiUimRequestCallback::clone */
