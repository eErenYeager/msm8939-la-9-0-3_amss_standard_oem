/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 *  Copyright (c) 2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.*
 *  Qualcomm Technologies Proprietary and Confidential.                      *
 *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

#ifndef _EXPORT_PPLLIB
#define _EXPORT_PPLLIB


/**************************** library Constatns *************/
#define MAX_SERIES_PACKET_SIZE          0x4000      /* Use it for packet_size argument in GetPPLAllocationSizes */
#define MAX_NUM_OF_SERIES_PACKETS       50          /* Use it for maxpackets argument in GetPPLAllocationSizes */
#define SERIES_START_SEARCH_NUMBER      1
#define MAX_NUMBER_OF_MICS              6
#define MAX_NUMBER_OF_TX                2
#define SIGNAL_LENGTH                   512
#define PRIME_NUM                       83


/**************************** PPL ERROR CODES *****************************/

#define ERR_NOERROR                 0
//#define ERR_INVALID_CALIB_PACKET    1           /* Removed */
/* Fatal errors. Execution stops on any of these errors. */
#define ERR_INVALID_SER_PACKET      2           /* Show stopper. */
#define ERR_INIT                    15          /* Init Workspace Error. Show stopper. */

/* Validation Report Errors. Reported per frame only in Validation proccess. */
#define ERR_AC_PWR_LVL_LOW          3
#define ERR_LF_PWR_LVL_LOW          4
#define ERR_CANT_FIND_SER           5
#define ERR_CANT_FIND_POS           6
#define ERR_DUPL_SAME_SER           8
#define ERR_MIC_BLOCK               9
#define ERR_PEN_TILT_ERR            10
#define ERR_CANT_FIND_BMM_SER       11
#define ERR_PEN_POS_OUT_REGION      12
#define ERR_PEN_MAX_Z               13
#define ERR_NO_PENDOWN              14

/* End of check cycle Error Codes. Reported only after all series were checked. */
#define ERR_DUPL_POS_SERIES         7           /* Duplicated position series - none or one of the found series is valid */
#define ERR_FATAL_DUPL_POS_SERIES   17           /* Duplicated position series - more than one found series is valid */
#define ERR_FULL_CYCLE_NO_SERIES    16          /* All Series are searched. No match found. */
#define ERR_FULL_CYCLE_BAD_VALID    18          /* All Series are searched. Match found. Validation failed. - can be multiple pen with same series.*/

typedef enum{
    SERIES_SEARCH = 0,
    SERIES_VALIDATION =1,
    SEARCH_COMPLETED = 2
}ProcessStep;

/************************* PPL FEEDBACK STRUCTURE *************************/

typedef struct
{
    int32_t ConfidenceLevel;                               // confidence level - in Q15 : 100% = 2^15*100, 0% = 0
    int32_t Result;                                        // Result - validation complete - 1, not complete - 0
    ProcessStep Process;                                // Machine status (see enum declaration above)
    int32_t MicBlocked;                                    // binary map of blocked microphones: 1 - mic 0 blocked, 2 - mic 1 blocked, 4 - mic 2 blocked, 8 - mic 3 blocked etc...
    int32_t PenType;                                       // Type of pen used
    int32_t Power[3];                                      // mean Power values of the pen's transducers and LF
    int32_t Peak2RMS[MAX_NUMBER_OF_MICS][3];               // mean Peak to RMS values of the pen's transducers and LF
    EPoint *out_points;                                 // pointer to output points structure of GetPoints (same as in run-time library)
    int32_t SeriesFound;                                   // Series found flag
    int32_t PositionFound;                                 // Pen position found flag
    int32_t CurrentSeriesNumber;                           // number of series under test
    int32_t FinalSeriesNumber;                             // final series number - this number valid when Result is equal 1
    int32_t CurrentFrameStatus;                            // Shows Current frame status - for automatic database validation
}PPLFeedbackStruct;


#ifdef __cplusplus
extern "C" {
#endif

#if defined(_MSC_VER)
#if defined(EPDSP_EXPORTS)
#define EPDSP_API __declspec(dllexport)
#else
#define EPDSP_API __declspec(dllimport)
#endif
#elif defined(__GNUC__)
#define EPDSP_API __attribute__((visibility("default")))
#else
#define EPDSP_API
#endif

/* Alocates memory for PPL library, packets and EPOS library*/
/* packet_size and maxpackets are supplied from the defines above, all of the rest of the arguments are exactly as in the GetAllocationSizes of eposexports.h*/
void EPDSP_API GetPPLAllocationSizes (int32_t *OutPointMaxCountPerPen, int32_t *OutMaxPens, int32_t packet_size, int32_t maxpackets, int32_t *OutSizeOfWorkspace);
/* Initializes the PPL Library */
/* all of the arguments are exactly as in the ResetDSP of eposexports.h*/
int32_t EPDSP_API ResetPPL(EPoint *InPointBuffer, void *InWorkspace);
/* Loads the series packets - this function should be activated in loop (on all packets), where "InConfiguration" is a packet buffer */
/*Returns 0 in case of success, -1 - memory alocation error, -2 - packet size error, -3 - maximal number of packets exceeded, Input parameters are exactly the same as in LoadCoeffs of eposexports.h*/
int32_t EPDSP_API LoadSeries(void *InWorkspace, void *InConfiguration, int32_t InConfigLength);
/* run this function on frames, the progress information and results are in PPLFeedback*/
int32_t EPDSP_API FindSeries(int32_t *pPacket, void *InWorkspace, PPLFeedbackStruct *PPLFeedback, int32_t X, int32_t Y, int32_t Radius);
/* Same as in run-time */
void EPDSP_API GetPPLVersion (char *OutDSPVersionString, unsigned char *OutDSPVersion);
/* Same as in run-time */
void EPDSP_API ReleasePPL(void *InWorkspace, void *OutConfiguration);
/* Load Calibration Packets - same as LoadCoeffs */
int32_t EPDSP_API LoadPPLCoeffs(void *InWorkspace, void *InConfiguration, int32_t InConfigLength);

#ifdef __cplusplus
}
#endif

#endif //_EXPORT_EPDSP
