#ifndef DPENCALIB_H
#define DPENCALIB_H

/*
 * Copyright (c) 2014 Qualcomm Technologies, Inc.  All Rights Reserved.
 * Qualcomm Technologies Proprietary and Confidential.
 */

/*
 * Return codes for dpencalib library.
 */
typedef enum
{
    DPENCALIB_SUCCESS        = 0, /* Success. */
    DPENCALIB_INVALID_ARG    = 1, /* Invalid argument to function. */
    DPENCALIB_INVALID_PACKET = 2, /* Invalid calibration packet. */
    DPENCALIB_NO_CALIB       = 3, /* Missing calibration data. */
    DPENCALIB_NO_FRAMES      = 4, /* Not enough frames for calibration. */
    DPENCALIB_NO_SPACE       = 5, /* Not enough space to hold calibration packet. */
    DPENCALIB_CALIB_FAILED   = 6, /* Calibration failed. */
    DPENCALIB_NO_STATS       = 7, /* Not enough data for statistics. */
} DPC_ERR;

/*
 * Acoustic frame.
 */
struct dpencalib_frame
{
    int channel;            /* Channel number: 0..Nchannels-1. */
    int timestamp;          /* Number of frames since sampling began. */
    const short *samples;   /* Pointer to an array of 512 signed 16bit samples. */
};

/*
 * Feedback status IDs.
 */
typedef enum
{
    DPCSTATUS_OK            = 0, /* All Ok. */
    DPCSTATUS_SPUR_ACOUSTIC = 1, /* Spurious acoustic tone. */
    DPCSTATUS_SPUR_EM       = 2, /* Spurious electromagnetic tone. */
    DPCSTATUS_NO_PEN        = 3, /* No pen detected. */
    DPCSTATUS_NOT_PEN_DOWN  = 4, /* Pen down switch is not pressed. */
    DPCSTATUS_LOW_SIGNAL    = 5, /* Low signal power. */
    DPCSTATUS_LOW_QUALITY   = 6, /* Low signal quality. */
    DPCSTATUS_MULTIPATH     = 7, /* Multipath detected. */
    DPCSTATUS_NOT_ON_POINT  = 8, /* Pen is too far from expected point. */
} DPC_STATUS;

struct dpencalib_status
{
    DPC_STATUS status; /* The current status. */
    int channel;       /* The channel number where the noise was found if any. */
    int point_number;  /* The index of the current point (0..Npoints-1),
                          or -1 if no more frames are needed. */
    int x, y;          /* The coordinates where the pen should be (0.01mm). */
};

struct dpencalib_noise
{
    double power_mean;          /* Mean signal power in dB FS. */
    double power_std;           /* Std. of signal power in dB FS. */
    double spur_power_mean;     /* Mean power of spur (dB). */
    double spur_power_std;      /* Std. of spur power (dB). */
    double spur_frequency_mean; /* Frequency of spur (Hz). */
    double spur_frequency_std;  /* Std. of spur frequency (Hz). */
    double peak2rms_mean[2];    /* Mean of peak to RMS ratio. */
    double peak2rms_std[2];     /* Std. of peak to RMS ratio. */
    double multipath_mean[2];   /* Mean of multipath ratio. */
    double multipath_std[2];    /* Std. of multipath ratio. */
};

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_MSC_VER)
#if defined(DPEN_EXPORTS)
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT __declspec(dllimport)
#endif
#elif defined(__GNUC__)
#define DLL_EXPORT __attribute__((visibility("default")))
#else
#define DLL_EXPORT
#endif

/*
 * Initialize the dpencalib library.
 * workspace is a buffer allocated by the caller.
 * To get the size of the buffer, call dpencalib_get_size().
 *
 * Return value:
 *    DPENCALIB_INVALID_ARG - if any of the arguments is NULL.
 *    DPENCALIB_SUCCESS - otherwise.
 */
DPC_ERR DLL_EXPORT dpencalib_init(void *workspace);

/*
 * Returns the required workspace size in bytes.
 *
 * Return value:
 *    DPENCALIB_INVALID_ARG - if any of the arguments is NULL.
 *    DPENCALIB_SUCCESS - otherwise.
 */
DPC_ERR DLL_EXPORT dpencalib_get_size(unsigned int *size);

/*
 * Returns the version of the library, e.g. version 2.0.4 is returned as
 *    *major = 2; *minor = 0; *subminor = 4;
 *
 * Return value:
 *    DPENCALIB_INVALID_ARG - if any of the arguments is NULL.
 *    DPENCALIB_SUCCESS - otherwise.
 */
DPC_ERR DLL_EXPORT dpencalib_version(int *major, int *minor, int *subminor);

/*
 * Used for debugging. Do not call.
 */
DPC_ERR DLL_EXPORT dpencalib_dump(int enable);

/*
 * Load a calibration packet into the workspace.
 * Should be called after dpencalib_init() and before
 * dpencalib_process_frame().
 *
 * workspace is a pointer to the workspace.
 * calib_packet is a pointer to the calibration packet's data.
 * length is the size of the calibration packet in bytes.
 *
 * Return value:
 *    DPENCALIB_INVALID_ARG - if any of the arguments is NULL or length < 0.
 *    DPENCALIB_INVALID_PACKET - if the calibration packet contains invalid data.
 *    DPENCALIB_SUCCESS - otherwise.
 */
DPC_ERR DLL_EXPORT dpencalib_calib(void *workspace,
    const void *calib_packet, int length);

/*
 * Process an acoustic frame.
 *
 * workspace is a pointer to the workspace.
 * frame is a pointer to a structure describing the frame.
 *
 * Return value:
 *    DPENCALIB_INVALID_ARG - if any of the arguments is NULL.
 *    DPENCALIB_NO_CALIB - if some calibration data is missing or invalid.
 *    DPENCALIB_SUCCESS - otherwise.
 */
DPC_ERR DLL_EXPORT dpencalib_process_frame(void *workspace,
    const struct dpencalib_frame *frame);

/*
 * Return the calibration packet containing the calibration parameters
 * computed by the library.
 * Should be called after all acoustic frames are processed.
 * Call dpencalib_get_status() to determine whether there is
 * enough data to begin calibration.
 *
 * workspace is a pointer to the workspace.
 * calib_packet is a pointer to the buffer to which the
 *     calibration packet is written to.
 * length is an in/out parameter. On entry, it contains the size of the
 *     buffer. On return, it is set to the number of bytes written.
 *     If the buffer is too small, nothing is written but length
 *     is still set to the number of bytes that would have been written.
 *
 * Return value:
 *    DPENCALIB_INVALID_ARG - if any of the arguments is NULL.
 *    DPENCALIB_NO_FRAMES - if not enough acoustic frames for calibration.
 *    DPENCALIB_NO_SPACE - if length is too small to hold the calibration packet.
 *    DPENCALIB_CALIB_FAILED - if the calibration failed for some reason.
 *    DPENCALIB_SUCCESS - otherwise.
 */
DPC_ERR DLL_EXPORT dpencalib_get_calib_packet(void *workspace,
    void *calib_packet, int *length);

/*
 * Return the mean and standard deviation of various quality paramters.
 * The mean and standard deviation are computed over a period of 1 second.
 *
 * workspace is a pointer to the workspace.
 * noise is a pointer to the structure which holds the results.
 * channel is the channel number for which to return the statistics.
 *
 * Return value:
 *    DPENCALIB_INVALID_ARG - if any of the arguments is NULL
 *                            or invalid channel number.
 *    DPENCALIB_NO_CALIB - if some calibration data is missing or invalid.
 *    DPENCALIB_NO_STATS - if not enough data for computing statistics.
 *    DPENCALIB_SUCCESS - otherwise.
 */
DPC_ERR DLL_EXPORT dpencalib_noise_statistics(void *workspace,
    struct dpencalib_noise *noise, int channel);

/*
 * Get the current status.
 *
 * The coordinate of the current point is returned in status->x, status->y
 * and is in units of 0.01mm.
 * Note that the coordinate returned is where the pen SHOULD be, not where
 * it currently is.
 * When no more data is needed (after the last point) status->point_number
 * is set to -1.
 *
 * workspace is a pointer to the workspace.
 * status is a pointer to the structure which holds the results.
 *
 * Return value:
 *    DPENCALIB_INVALID_ARG - if any of the arguments is NULL.
 *    DPENCALIB_SUCCESS - otherwise.
 */
DPC_ERR DLL_EXPORT dpencalib_get_status(void *workspace,
    struct dpencalib_status *status);

#ifdef __cplusplus
}
#endif

#endif
