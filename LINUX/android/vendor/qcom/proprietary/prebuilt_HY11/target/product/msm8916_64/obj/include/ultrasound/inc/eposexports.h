/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 *  Copyright (c) 2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.*
 *  Qualcomm Technologies Proprietary and Confidential.                      *
 *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

#ifndef _EXPORT_EPDSP
#define _EXPORT_EPDSP

#include <stdint.h>

#define NO_COMMAND              0
#define COMMAND_OUT_SETGAIN 10
#define COMMAND_OUT_SPEAKER     30
#define COMMAND_OUT_TEMPERATURE 40

#define SPEAKER_ON              1
#define SPEAKER_OFF             0

#define MAX_DUMP_DATA_SIZE 25

/****************************** COMMAND IN *****************************/
//Acoustics
#define COMMAND_AGC_only    0x210
#define COMMAND_Spur_Light  0x220
#define COMMAND_Spur_Remover    0x230
#define COMMAND_Get_Spur_Frequency  0x240
#define COMMAND_IIR_High_Pass   0x250
#define COMMAND_TIMEBASELOCK    0x260
//Pen Interference
#define COMMAND_Pen_Interference_X_length   0x0310
#define COMMAND_Pen_Interference_Y_length   0x0320
//Template related
#define COMMAND_Wiener_filter   0x0410
//Post Processing
#define COMMAND_Motion_Filter   0x0510
#define COMMAND_Smoother    0x0520
#define COMMAND_Top_Linearity   0x0530
#define COMMAND_Bilinear    0x0540
#define COMMAND_Parabolic   0x0550
//Triangulation
#define COMMAND_Large_area_wrap 0x0610
//Temperature
#define COMMAND_SpeakerOnOff    0x0700
//Debug
#define COMMAND_Traces  0xF010
#define COMMAND_Scope_Debugger  0xF020
#define COMMAND_Packet_Logger   0xF030

#ifndef COMMAND_RotationAngle
#define COMMAND_RotationAngle   0x0701
#endif
#define COMMAND_SnifferMode     0x0660

/****************************** PACKET HEADERS ****************************/
#define  IR_PACKET_TYPE_A  64
#define  IR_PACKET_TYPE_B  65
#define  IR_PACKET_TYPE_C  66

#define  TEMPERATURE_PACKET  0xF4

#define TRACE_ERROR 0
#define TRACE_WARN 1
#define TRACE_INFO 2
#define TRACE_DEBUG 3

#define COORD_VALID     (1<<0)
#define COORD_PEN_DOWN  (1<<1)
#define COORD_SW2       (1<<2)
#define COORD_SW1       (1<<3)
#define COORD_SW0       (1<<4)
#define COORD_BAT       (1<<15)



typedef struct
{
    uint32_t BaseID;
    uint32_t TerminalID;
}EPSource;



typedef struct
{
    EPSource        Source;
    int32_t    X;                          //X coordinate in mm*100
    int32_t    Y;                          //Y coordinate in mm*100
    int32_t    Z;                          //Z coordinate in mm*100
    int32_t    P;                          //Pressure
    int32_t    Type;                       //Coordinate type
    int32_t    TiltX;                      //TiltX +-128
    int32_t    TiltY;                      //TiltY +-128

}EPoint;

typedef enum
{
    POINT_BaseID=0,                     //base id
    POINT_TerminalID=1,                 //terminal id
    POINT_X=2,                          //X coordinate in mm*100
    POINT_Y=3,                          //Y coordinate in mm*100
    POINT_Z=4,                          //Z coordinate in mm*100
    POINT_Pressure=5,                   //Pressure
    POINT_Valid=6,                      //Is coordinate valid?
    POINT_TiltX=7,                      //TiltX +-128
    POINT_TiltY=8,                      //TiltY +-128
    POINT_TiltZ=9,                      //TiltZ +-128
    POINT_WorkingPlane=10,              //Working Plane
    POINT_AcousticFrameCounter=11,      //Acoustic frame counter
    POINT_SpeedOfSound=12,              //Speed Of Sound current Speed OfSound in m/sec Q15
    POINT_GdopX=13,                     //Coordinate GDOPS
    POINT_GdopY=14,                     //Coordinate GDOPS
    POINT_GdopZ=15,                     //Coordinate GDOPS
    POINT_UserData1=16,                 //User data with buffer
    POINT_ADC0=17,                      //ADC0
    POINT_ADC1=18,                      //ADC1
    POINT_PenDown=19,                   // Pen down state, 1=pen down, 0=pen up
    POINT_SideSwitch=20,                // Side switch state, 1=pressed, 0=depressed
    POINT_SW0=21,                       // Switch0 state, 1=pressed, 0=depressed
    POINT_SW1=22,                       // Switch1 state, 1=pressed, 0=depressed
    POINT_Battery=23,                   // Low pen battery status, 1=low battery, 0=battery Ok
    POINT_RotationOffsetX=24,           // Rotation Offset in  mm*100 X
    POINT_RotationOffsetY=25,           // Rotation Offset in  mm*100 Y
    POINT_RotationOffsetZ=26,           // Rotation Offset in  mm*100 Z
    POINT_RotationDirectionX=27,        // Rotation direction in Q15 X
    POINT_RotationDirectionY=28,        // Rotation direction in Q15 Y
    POINT_RotationDirectionZ=29,        // Rotation direction in Q15 Z
    POINT_RotationAngle=30,             // Rotation angle degrees in Q15
    POINT_OffScreenZ=31,                // OffScreenZ in  mm*100
    POINT_BatteryLevel=32,              //Battery level
    POINT_PrimarySwitch=33,             // Primary switch
    POINT_SecondarySwitch=34,           // Secondary switch
    POINT_TertiarySwitch=35,            // Tertiary switch
    POINT_BatteryCharging=36,           // Pen battery is charging
}EPointType;

typedef struct
{
    int32_t ActiveState;
    int32_t ActivePenNumber;
    int32_t CommandOut[4];
}FeedbackInfo;

typedef struct
{
    int32_t                ReceivedGain;
    int32_t                GainChangeCounter;
    int32_t                RequestedDeltaGain;
    int32_t                RequestedGain;
    int32_t                CalculatedPower;
    int32_t                GainRequestReady;
    int32_t                Reset;
}AGC_struct;

typedef struct
{
    int32_t DumpID;
    int32_t Length;
    int32_t PacketNumber;
    int32_t Data[MAX_DUMP_DATA_SIZE];
}DumpPacket;

typedef int32_t (*WriteDumpCallback)(DumpPacket *Dump, int32_t Length);
typedef void (*TraceCallback)(int trace_level, const char *fmt, ...);

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_MSC_VER)
#if defined(EPDSP_EXPORTS)
#define EPDSP_API __declspec(dllexport)
#else
#define EPDSP_API __declspec(dllimport)
#endif
#elif defined(__GNUC__)
#define EPDSP_API __attribute__((visibility("default")))
#else
#define EPDSP_API
#endif

void EPDSP_API GetDSPVersion (char *OutDSPVersionString, unsigned char *OutDSPVersion);
void EPDSP_API GetAllocationSizes (int32_t *OutPointMaxCountPerPen, int32_t *OutMaxPens, int32_t *OutSizeOfWorkspace);
int32_t EPDSP_API InitDSP(EPoint *InPointBuffer, void *InWorkspace, void *InConfiguration, int32_t ConfigureationLength);
int32_t EPDSP_API GetPoints(int32_t *pPacket, void *InWorkspace, FeedbackInfo *OutFeedback);
void EPDSP_API SetRotationAxis(void *InWorkspace,int32_t origin[3],int32_t direction[3],int32_t OffScreenZ);
void EPDSP_API ReleaseDSP(void *InWorkspace, void *OutConfiguration);
void EPDSP_API AGC(int16_t *inPtr16,int32_t *input512,AGC_struct *pAGC,int32_t ChannelNumber,int32_t EnableEvents);
void EPDSP_API InitAGC(AGC_struct *pAGC);
void EPDSP_API UpdateAGC(AGC_struct *pAGC,int16_t Gain);
int32_t EPDSP_API Command (void *InWorkspace, int32_t *CommandBuffer);
void EPDSP_API InitDumpCallbacks(WriteDumpCallback WriteDumpFunc);
int32_t EPDSP_API ResetDSP(EPoint *InPointBuffer, void *InWorkspace);
int32_t EPDSP_API LoadCoeffs(void *InWorkspace, void *InConfiguration, int32_t InConfigLength);
int EPDSP_API GetPersistentData(void *InWorkspace, int32_t *Buffer, int Length);
void EPDSP_API SetDSPTraceCallback(TraceCallback callback);
int32_t EPDSP_API QueryEPoint(void *InWorkspace, EPointType eptype,int32_t pointnum,int32_t *result,int32_t buflen);

#ifdef __cplusplus
}
#endif

#endif //_EXPORT_EPDSP
