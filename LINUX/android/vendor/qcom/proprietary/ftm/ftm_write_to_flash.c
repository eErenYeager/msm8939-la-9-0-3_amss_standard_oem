/*
*Copyright (c) 2017 Qualcomm Technologies, Inc.
*
*All Rights Reserved.
*Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

#ifdef WIN_AP_HOST

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <mtd/mtd-user.h>
#include "comdef.h"
#include "diagcmd.h"

#include "ftm_wlan.h"
#include "ftm_dbg.h"

#define MAC_XTAL_LENGTH 7
#define MAC_LENGTH_POS 103
#define MAC_POS 105
#define BT_TLV1_RESP_LEN 84
#define BT_RESP_LEN 100
#define FLASH_SECTOR_SIZE 0x10000

uint16_t TLV2_Specific_byte;
unsigned char BTsetmacResponse[] = {
    0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x38, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x09, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xC6, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,
    0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

/*===========================================================================
FUNCTION bt_setmac_flash_write

DESCRIPTION
    Write MAC and XTAL to flash region

DEPENDENCIES
    NIL

RETURN VALUE
   Returns status success or failure

SIDE EFFECTS
    NONE

===========================================================================*/
int bt_setmac_flash_write(uint8_t *mac, unsigned int len)
{
    int fd;
    int offset;
    erase_info_t eraseInfo;
    mtd_info_t mtdInfo;
    int SectorStart;
    int SectorEnd;
    int TotalSize;
    int SectorOffset;
    char *SectorData;
    int Sector;
    int i;

    if((fd = open("/dev/caldata", O_RDWR)) < 0) {
        perror("Could not open flash. Returning without write\n");
        return -1;
    }
    DPRINTF(FTM_DBG_TRACE,"\nNumber of bytes = %d",len);

    offset = 0x40;
    SectorStart = offset/FLASH_SECTOR_SIZE;
    DPRINTF(FTM_DBG_TRACE,"\nSector start = %d\n",SectorStart);
    SectorEnd = (offset + 0x40)/FLASH_SECTOR_SIZE;
    DPRINTF(FTM_DBG_TRACE,"\nSector end = %d\n",SectorEnd);
    SectorOffset = (SectorStart*FLASH_SECTOR_SIZE);
    DPRINTF(FTM_DBG_TRACE,"\nSectorOffset = %d\n",SectorOffset);
    offset = offset - SectorOffset;
    DPRINTF(FTM_DBG_TRACE,"\nfinal offset = %d\n",offset);
    TotalSize = FLASH_SECTOR_SIZE;
    DPRINTF(FTM_DBG_TRACE,"\nTotal size = %d\n",TotalSize);

    SectorData = (char *) malloc(TotalSize);
    if (!SectorData) {
        printf("malloc fail.\n");
        return -1;
    }
    DPRINTF(FTM_DBG_TRACE,"\nSeek pointer to %d\n",SectorOffset);
    lseek(fd, SectorOffset, SEEK_SET);
    if (read(fd, SectorData, TotalSize) < 1) {
        printf("backup flash fail.\n");
        free(SectorData);
        return -1;
    }
    printf("flash backup to RAM success.\n");

    /*clear RAM content with 0x00 from the bt offset
     *memset(&SectorData[offset], 0x00, 0x40);
     *apply the bt data to buffer
     */
    for(i=0;i<len;i++) {
       SectorData[offset+i]=mac[i];
       DPRINTF(FTM_DBG_TRACE,"mac = 0x%x SectorData = 0x%x",mac[i],SectorData[offset+i]);
    }
    DPRINTF(FTM_DBG_TRACE,"\n");
    if (ioctl(fd, MEMGETINFO, &mtdInfo) == 0){
        if ( FLASH_SECTOR_SIZE != mtdInfo.erasesize ) {
            DPRINTF(FTM_DBG_TRACE,"NOR flash sector is not 64KB ! Not support now.\n");
            return -1;
        }
        eraseInfo.start = SectorStart;
        eraseInfo.length = FLASH_SECTOR_SIZE;
        ioctl(fd, MEMUNLOCK, &eraseInfo);
        if (ioctl(fd, MEMERASE, &eraseInfo) < 0) {
            DPRINTF(FTM_DBG_TRACE,"flash erase error.. Returning\n");
            free(SectorData);
            return -1;
        }
        lseek(fd, SectorStart, SEEK_SET);
        if (write(fd, &SectorData[SectorStart], FLASH_SECTOR_SIZE) < 1) {
            DPRINTF(FTM_DBG_TRACE,"flash write error. Returning\n");
            free(SectorData);
            return -1;
        }
        free(SectorData);
        close(fd);
        DPRINTF(FTM_DBG_TRACE, "flash write ok ! SectorOffset=0x%X, offset=0x%X\n", SectorOffset, offset);
        return 1;
    }
    lseek(fd, SectorStart, SEEK_SET);
    if (write(fd, &SectorData[SectorStart], FLASH_SECTOR_SIZE) < 1) {
        DPRINTF(FTM_DBG_TRACE,"flash write error. Returning\n");
        free(SectorData);
        return -1;
    }
    free(SectorData);
    close(fd);
    DPRINTF(FTM_DBG_TRACE,"flash write ok ! SectorOffset=0x%X, offset=0x%X\n", SectorOffset, offset);
    return 1;
}


/*===========================================================================
FUNCTION win_bt_mac_flash_write

DESCRIPTION
    Call bt_setmac_flash_write function and populate response to Qdart

DEPENDENCIES
    NIL

RETURN VALUE
   Returns resp to qdart

SIDE EFFECTS
    NONE

===========================================================================*/
ftm_wlan_rsp_pkt_type *win_bt_mac_flash_write(ftm_wlan_req_pkt_type *wlan_ftm_pkt, int pkt_len)
{
    int i;
    int status;
    ftm_wlan_rsp_pkt_type *rsp;
    unsigned char BtDiagMAC[MAC_XTAL_LENGTH];
    unsigned int dataLen = 0;
    uint8_t *input_msg = (uint8_t*)wlan_ftm_pkt;
    TLV2_Specific_byte = wlan_ftm_pkt->cmd.common_ops.rsvd;

    dataLen = input_msg[MAC_LENGTH_POS];;
    for(i=0;i<dataLen;i++)
        BtDiagMAC[i]= input_msg[MAC_POS+i];
    rsp = (ftm_wlan_rsp_pkt_type*)diagpkt_subsys_alloc(DIAG_SUBSYS_FTM,
                                                      FTM_WLAN_CMD_CODE,
                                                      (sizeof(rsp->common_header) +
                                                       sizeof(rsp->cmd.common_ops)+ BT_TLV1_RESP_LEN ));
    rsp->common_header.cmd_rsp_pkt_size = BT_RESP_LEN;
    rsp->common_header.cmd_data_len = 0;
    rsp->cmd.win_resp.data_len = BT_TLV1_RESP_LEN;
    rsp->cmd.win_resp.win_cmd_specific = TLV2_Specific_byte;

    status = bt_setmac_flash_write(BtDiagMAC, dataLen);
    if (status > 0) {
        memcpy(rsp->cmd.win_resp.data, BTsetmacResponse, BT_TLV1_RESP_LEN);
        DPRINTF(FTM_DBG_TRACE,"Response sent to Qdart\n");
        /*print_uchar_array((uint8_t*)(rsp->cmd.win_resp.data), BT_TLV1_RESP_LEN);*/
        return rsp;
    }
    else
        return rsp;
}
#endif

